package org.jit8.site.travel.constants;

import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.jit8.user.developer.common.constants.CommonDeveloperConstant;


public class KylinboyTravelDeveloperConstant extends CommonDeveloperConstant{

	private static final long serialVersionUID = -2922906669417770496L;
	
	public static final String _KYLINBOY_CODE_SEPERATOR = "_";
	public static final String _KYLINBOY_CODE_SEPERATOR_O = "o";
	public static final long _KYLINBOY_USER_ID_LONG = 100001l;//开发者ID
	public static final String _KYLINBOY_USER_ID = "100001";//开发者ID
	public static final String _KYLINBOY_CODE_PARAM = "{opration}";
	
	/**
	 * 用于url中code两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_CODE_SEPERATOR_PRIFIX = _KYLINBOY_CODE_SEPERATOR_O + _KYLINBOY_CODE_SEPERATOR;
	public static final String _KYLINBOY_CODE_SEPERATOR_SUFFIX = _KYLINBOY_CODE_SEPERATOR + _KYLINBOY_CODE_SEPERATOR_O;
	
	/**
	 * 用于url中userId两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_USER_SEPERATOR_PRIFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR_U + CommonDeveloperConstant._COMMON_USER_SEPERATOR;
	public static final String _KYLINBOY_USER_SEPERATOR_SUFFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR + CommonDeveloperConstant._COMMON_USER_SEPERATOR_U;
	
	/**
	 * 用于url中userId+标识组合
	 */
	public static final String _KYLINBOY_USER_ID_AND_SEPERATOR = _KYLINBOY_USER_SEPERATOR_PRIFIX + _KYLINBOY_USER_ID + _KYLINBOY_USER_SEPERATOR_SUFFIX;
	
	/**
	 * 用于url中code+标识组合
	 */
	public static final String _KYLINBOY_CODE_AND_SEPERATOR = _KYLINBOY_CODE_PARAM + _KYLINBOY_CODE_SEPERATOR_SUFFIX;
	
	
	/**
	 * 用于url中code+userId+标识组合
	 */
	public static final String _KYLINBOY_CODE_PARAM_AND_USER_ID = _KYLINBOY_CODE_AND_SEPERATOR + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	/** JourneyController mapping begin */
	
	/**行程管理模块*/
	
	public static final String KYLINBOY_JOURNEY_MODULE_ID = _KYLINBOY_USER_ID + "journey_manage";
	public static final String KYLINBOY_JOURNEY_MODULE = "journey_manage";
	public static final String KYLINBOY_JOURNEY_MODULE_NAME = "行程管理";
	public static final String KYLINBOY_JOURNEY_MODULE_CODE = _KYLINBOY_USER_ID + KYLINBOY_JOURNEY_MODULE;
	
	public static final String KYLINBOY_JOURNEY_ADD_UI_ID = _KYLINBOY_USER_ID + "journey_add_ui";
	public static final String KYLINBOY_JOURNEY_ADD_UI = "journey_add_ui";
	public static final String KYLINBOY_JOURNEY_ADD_UI_NAME = "行程添加页面";
	public static final String KYLINBOY_JOURNEY_ADD_UI_DESC = "行程添加页面";
	public static final String KYLINBOY_JOURNEY_ADD_UI_URL = "/admin/main/journey/";
	public static final String KYLINBOY_JOURNEY_ADD_UI_MAPPING = KYLINBOY_JOURNEY_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_JOURNEY_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	/** JourneyController mapping end */
	
	
	
	@Override
	public Long getDeveloperUserId() {
		return _KYLINBOY_USER_ID_LONG;
	}
}
