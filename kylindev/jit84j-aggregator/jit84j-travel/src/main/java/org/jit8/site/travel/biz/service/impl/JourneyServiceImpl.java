package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.JourneyService;
import org.jit8.site.travel.persist.domain.Journey;
import org.jit8.site.travel.persiste.dao.JourneyDao;
import org.springframework.stereotype.Service;

@Service
public class JourneyServiceImpl extends GenericServiceImpl<Journey, Long> implements JourneyService {

	@Resource
	private JourneyDao journeyDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = journeyDao;
	}


	@Override
	public List<Journey> getJourneyList(boolean disable, boolean deleted) {
		return journeyDao.getJourneyList(disable, deleted);
	}


	@Override
	public Journey findByCode(String code) {
		return journeyDao.findByCode( code);
	}


	@Override
	public List<Journey> findByIds(List<Long> idList) {
		return journeyDao.findByIds( idList);
	}
	
}
