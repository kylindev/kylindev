package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.MemberInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface MemberInfoBusiness {

	public List<MemberInfo> getMemberInfoList();
	
	public MemberInfo persist(MemberInfo memberInfo);
	
	public MemberInfo getMemberInfoById(Long id);
	
	public MemberInfo deleteMemberInfoById(Long id);
	
	public Page<MemberInfo> findAll(Pageable pageable);
	
	public MemberInfo findByCode(String code);
	
	public void removeById(Long id);
	
	public List<MemberInfo> findByIds(List<Long> idList);
}
