package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.MemberService;
import org.jit8.site.travel.persist.domain.Member;
import org.jit8.site.travel.persiste.dao.MemberDao;
import org.springframework.stereotype.Service;

@Service
public class MemberServiceImpl extends GenericServiceImpl<Member, Long> implements MemberService {

	@Resource
	private MemberDao memberDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = memberDao;
	}


	@Override
	public List<Member> getMemberList(boolean disable, boolean deleted) {
		return memberDao.getMemberList(disable, deleted);
	}


	@Override
	public Member findByCode(String code) {
		return memberDao.findByCode( code);
	}


	@Override
	public List<Member> findByIds(List<Long> idList) {
		return memberDao.findByIds( idList);
	}
	
}
