package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.Division;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface DivisionBusiness {

	public List<Division> getDivisionList();
	
	public Division persist(Division division);
	
	public Division getDivisionById(Long id);
	
	public Division deleteDivisionById(Long id);
	
	public Page<Division> findAll(Pageable pageable);
	
	public Division findByCode(String code);
	
	public void removeById(Long id);
	
	public List<Division> findByIds(List<Long> idList);
}
