package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.DivisionBusiness;
import org.jit8.site.travel.biz.service.DivisionService;
import org.jit8.site.travel.persist.domain.Division;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("divisionBusiness")
public class DivisionBusinessImpl implements DivisionBusiness{

	@Resource
	private DivisionService divisionService;
	

	@Override
	public List<Division> getDivisionList() {
		return divisionService.getDivisionList(false, false);
	}
	
	@Transactional
	@Override
	public Division persist(Division division){
		Long id = division.getId();
		Division exist = null;
		if(id != null && id>0){
			 exist = divisionService.findOne(division.getId());
			 exist.setSequence(division.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=division;
		}
		
		return divisionService.save(exist);
	}

	@Override
	public Division getDivisionById(Long id) {
		return divisionService.findOne(id);
	}

	@Override
	public Division deleteDivisionById(Long id) {
		Division old = divisionService.findOne(id);
		old.setDeleted(true);
		return divisionService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			divisionService.delete(id);
		}
	}

	@Override
	public Page<Division> findAll(Pageable pageable) {
		return divisionService.findAll(pageable);
	}

	@Override
	public Division findByCode(String code) {
		return divisionService.findByCode( code);
	}

	@Override
	public List<Division> findByIds(List<Long> idList) {
		return divisionService.findByIds(idList);
	}

}
