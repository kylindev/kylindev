package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.Division;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DivisionRepository extends GenericJpaRepository<Division, Long>{


	@Query("from Division o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<Division> getDivisionList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from Division o where o.code=:code")
	public Division findByCode(@Param("code")String code);
	
	@Query("from Division o where o.uniqueIdentifier in :idList")
	public List<Division> findByIds(@Param("idList")List<Long> idList);
	
}
