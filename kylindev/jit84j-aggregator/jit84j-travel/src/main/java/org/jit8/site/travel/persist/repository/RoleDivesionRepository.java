package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.RoleDivesion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RoleDivesionRepository extends GenericJpaRepository<RoleDivesion, Long>{

	@Query("from RoleDivesion o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<RoleDivesion> getRoleDivesionList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from RoleDivesion o where o.division.code=:code")
	public RoleDivesion findByCode(@Param("code")String code);
	
	@Query("from RoleDivesion o where o.uniqueIdentifier in :idList")
	public List<RoleDivesion> findByIds(@Param("idList")List<Long> idList);
}
