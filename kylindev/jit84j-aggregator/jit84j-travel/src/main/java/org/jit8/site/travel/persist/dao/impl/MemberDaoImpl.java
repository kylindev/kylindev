package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.domain.Member;
import org.jit8.site.travel.persist.repository.MemberRepository;
import org.jit8.site.travel.persiste.dao.MemberDao;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDaoImpl extends GenericDaoImpl<Member, Long> implements MemberDao {

	@Resource
	private MemberRepository memberRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = memberRepository;
	}

	@Override
	public List<Member> getMemberList(boolean disable, boolean deleted) {
		return memberRepository.getMemberList(disable, deleted);
	}
	
	@Override
	public Member findByCode(String code) {
		return memberRepository.findByUserName( code);
	}

	@Override
	public List<Member> findByIds(List<Long> idList) {
		return memberRepository.findByIds(idList);
	}
}
