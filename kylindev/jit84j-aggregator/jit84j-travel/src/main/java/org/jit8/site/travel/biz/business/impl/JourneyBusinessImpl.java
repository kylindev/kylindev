package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.JourneyBusiness;
import org.jit8.site.travel.biz.service.JourneyService;
import org.jit8.site.travel.persist.domain.Journey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("journeyBusiness")
public class JourneyBusinessImpl implements JourneyBusiness{

	@Resource
	private JourneyService journeyService;
	

	@Override
	public List<Journey> getJourneyList() {
		return journeyService.getJourneyList(false, false);
	}
	
	@Transactional
	@Override
	public Journey persist(Journey journey){
		Long id = journey.getId();
		Journey exist = null;
		if(id != null && id>0){
			 exist = journeyService.findOne(journey.getId());
			 exist.setDescription(journey.getDescription());
			 exist.setImagePath(journey.getImagePath());
			 exist.setSequence(journey.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=journey;
		}
		
		return journeyService.save(exist);
	}

	@Override
	public Journey getJourneyById(Long id) {
		return journeyService.findOne(id);
	}

	@Override
	public Journey deleteJourneyById(Long id) {
		Journey old = journeyService.findOne(id);
		old.setDeleted(true);
		return journeyService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			journeyService.delete(id);
		}
	}

	@Override
	public Page<Journey> findAll(Pageable pageable) {
		return journeyService.findAll(pageable);
	}

	@Override
	public Journey findByCode(String code) {
		return journeyService.findByCode( code);
	}

	@Override
	public List<Journey> findByIds(List<Long> idList) {
		return journeyService.findByIds(idList);
	}

}
