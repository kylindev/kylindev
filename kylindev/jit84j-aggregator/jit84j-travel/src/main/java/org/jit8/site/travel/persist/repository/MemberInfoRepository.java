package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.MemberInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MemberInfoRepository extends GenericJpaRepository<MemberInfo, Long>{


	@Query("from MemberInfo o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<MemberInfo> getMemberInfoList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from MemberInfo o where o.userName=:userName")
	public MemberInfo findByUserName(@Param("userName")String userName);
	
	@Query("from MemberInfo o where o.uniqueIdentifier in :idList")
	public List<MemberInfo> findByIds(@Param("idList")List<Long> idList);
}
