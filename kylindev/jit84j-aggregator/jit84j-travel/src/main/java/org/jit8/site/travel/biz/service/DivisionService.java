package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.Division;


public interface DivisionService extends GenericService<Division, Long>{

	public List<Division> getDivisionList(boolean disable, boolean deleted);
	
	public Division findByCode(String code);
	
	public List<Division> findByIds(List<Long> idList);
}
