package org.jit8.site.travel.persiste.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.Division;

public interface DivisionDao extends GenericDao<Division, Long> {

	public List<Division> getDivisionList(boolean disable, boolean deleted) ;
	
	public Division findByCode(String code);
	
	public List<Division> findByIds(List<Long> idList);
}
