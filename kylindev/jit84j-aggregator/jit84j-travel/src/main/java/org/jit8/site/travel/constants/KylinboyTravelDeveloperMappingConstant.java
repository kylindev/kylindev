package org.jit8.site.travel.constants;

import java.util.List;

import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public class KylinboyTravelDeveloperMappingConstant extends KylinboyTravelDeveloperConstant{

	private static final long serialVersionUID = 8267163625961520713L;
	
	/**
	 * 获取系统预定义的URL列表
	 * @return
	 */
	@Override
	public List<UrlStaticMapping> getUrlStaticMappingModuleList(){
		
		/**行程管理模块 begin */
		UrlStaticMapping jouneyModule = addModule(KYLINBOY_JOURNEY_MODULE_ID,KYLINBOY_JOURNEY_MODULE_NAME,KYLINBOY_JOURNEY_MODULE_CODE);
		addUrlStaticMapping(
				jouneyModule,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_ID,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_NAME,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_DESC,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_URL,
				KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING
		);
		
		/** 行程管理模块  end */


		return urlStaticMappingModuleList;
		
	}
	
	
}