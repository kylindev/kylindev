package org.jit8.site.travel.persiste.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.RoleDivesion;

public interface RoleDivesionDao extends GenericDao<RoleDivesion, Long> {

	public List<RoleDivesion> getRoleDivesionList(boolean disable, boolean deleted) ;
	
	public RoleDivesion findByCode(String code);
	
	public List<RoleDivesion> findByIds(List<Long> idList);
}
