package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.DivisionService;
import org.jit8.site.travel.persist.domain.Division;
import org.jit8.site.travel.persiste.dao.DivisionDao;
import org.springframework.stereotype.Service;

@Service
public class DivisionServiceImpl extends GenericServiceImpl<Division, Long> implements DivisionService {

	@Resource
	private DivisionDao divisionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = divisionDao;
	}


	@Override
	public List<Division> getDivisionList(boolean disable, boolean deleted) {
		return divisionDao.getDivisionList(disable, deleted);
	}


	@Override
	public Division findByCode(String code) {
		return divisionDao.findByCode( code);
	}


	@Override
	public List<Division> findByIds(List<Long> idList) {
		return divisionDao.findByIds( idList);
	}
	
}
