package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.Member;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MemberRepository extends GenericJpaRepository<Member, Long>{


	@Query("from Member o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<Member> getMemberList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from Member o where o.userName=:userName")
	public Member findByUserName(@Param("userName")String userName);
	
	@Query("from Member o where o.uniqueIdentifier in :idList")
	public List<Member> findByIds(@Param("idList")List<Long> idList);
}
