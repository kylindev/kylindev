<#-- 切换语言begin -->
<#macro languageBar>  
<ul class="list-inline">
	  <li>
	  	<a class="text-primary" href="javascript:void(0)" onclick="changeLanguage('en')">EN</a>
	  </li>
	  <li>
	    <a class="text-primary" href="javascript:void(0)" onclick="changeLanguage('zh')">中文</a>
	  </li>
</ul>
<script type="text/javascript">
function changeLanguage(language)
{
     $.ajax({
       type: "GET",
       url: "${KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING}/"+language,
       data: "lang="+language,
       dataType:"json",
       async: true,
       error: function(data, error) {alert(data.responseText+"change lang error!");},
       success: function(data)
       {
            window.location.reload();
       }
	}); 
}
</script>
</#macro> 
<#-- 切换语言end -->
