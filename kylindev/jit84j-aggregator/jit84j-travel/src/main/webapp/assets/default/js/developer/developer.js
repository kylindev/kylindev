$( document ).ready( function() {
	function getDeveloperList(){
		var _id = "#listDeveloperForPermission";
		var _action = "#listDeveloperForPermissionAction";
		var _developerId = $("#developerId").val();
		var request={
				"id":_developerId,
				"seperator":"seperator"
		}
		var action = $(_action).val();
		
			$.ajax({
				url: action,
				data: request,
				type: "POST",
				dataType : "text",
				contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
				success: function(data) {
						$(_id).html(data);
				},
				error: function() {
					alert( "Sorry, there was a problem!" );
				},
				complete: function() {
					//alert('complete');
				}
			});
	}
	 getDeveloperList();
} );
