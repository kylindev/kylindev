//切换按钮，异步获取内容并填充
function switchSetting(id,navigationId,url,containerId){
	var listItem = $(".list-group-item");
	var _id="left_navigation_"+navigationId;
	if(listItem.length>0){
		$(listItem).each(function(){
			if(_id==$(this).attr("id")){
				$(this).addClass("active");
			}else{
				$(this).removeClass("active");
			}
		});
	}
	var request={'id':id};
	getContent(url,request,containerId);
}

function getContent(url,request,containerId){
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			$(containerId).html(data);
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
//异步提交表单，表单区域要提交的内容必须有data='data'属性，将返回结果页面填充入指定容器
function submit(formId,backDataContainerId){
	alert("submit:" + formId);
		var request = new Object();
		var _fom = $(formId);
		//var _input = formId + " input[data$='data']";
		if(_fom.length>0){
			var _input= _fom.find("input[data$='data']");
			if(_input.length>0){
	  			_input.each(function(){
	  				
	  				if($(this).attr("type")=="checkbox" && $(this).attr("data")!='data-check-value'){
	  					if($(this).attr('checked')==undefined){
	  						request[$(this).attr("name")]='false';
	  					}else{
	  						request[$(this).attr("name")]='true';
	  					}
	  				}else{
	  					request[$(this).attr("name")]=$(this).attr("value");
	  				}
	  				
	  			});
			}
			
			var _input= _fom.find("input[data$='data-check-value']");
			if(_input.length>0){
	  			_input.each(function(){
	  				if($(this).attr("type")=="checkbox" && $(this).attr("data")!='data'){
	  					if($(this).attr('checked')!=undefined){
	  						request[$(this).attr("name")]=$(this).attr("value");
	  					}
	  				}
	  			});
			}
			
			
			var _select = formId+" select[data$='data']";
			if($(_select).length>0){
				$(_select).each(function(){
					request[$(this).attr("name")]=$(this).attr("value");
				});
			}
			
			var _select = formId+" textarea[data$='data']";
			if($(_select).length>0){
				$(_select).each(function(){
					request[$(this).attr("name")]=$(this).attr("value");
				});
			}
			var action = $(formId).attr("action");
			$.ajax({
				url: action,
				data: request,
				type: "POST",
				dataType : "text",
				contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
				success: function(data) {
					if($(backDataContainerId).length>0){
							$(backDataContainerId).html(data);
					}
						
				},
				error: function() {
					alert( "Sorry, there was a problem!" );
				},
				complete: function() {
					//alert('complete');
				}
			});
		}
}

function cleanForm(formId) {
	//alert("clean:" + formId);
	var _input = formId + " input[data$='data']";
	if ($(_input).length > 0) {
		$(_input).each(function() {
			if ($(this).attr("type") == "checkbox") {
				$(this).attr("checked", false);
			} else {
				$(this).attr("value", "");
			}
		});
	}
}

function removeItem(id,action,backDataContainerId){
	if(id==''){
		alert("没有id");
		return;
	}
	var request = {'id':id};
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			//alert("移除成功");
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
//全选操作
function selectAll(id,prefix){
	var _id = "#" + id;
	var _all = $(_id);
	var _input = "input[id ^='"+prefix+"']";
	if(_all.attr("checked")){ 
		$(_input).each(function(){this.checked=true;}); 
	}else{
		$(_input).each(function(){this.checked=false;}); 
	}
}

function joinCheckboxValue(prefix){
	
	var _input = "input[id ^='"+prefix+"']:checked";
	var _all = prefix+ "all";
	var requestData="";
	if($(_input).length>0){
		$(_input).each(function(){
		if(($(this).attr("id")).indexOf(_all)<0){
			requestData += $(this).val()+",";
		}
		
	}); 
	
	if(requestData.lastIndexOf(",")>0){
			requestData = requestData.trim();
			requestData = requestData.substring(0,requestData.lastIndexOf(","));
		}
	}
	if(""==requestData){
		return "";
	}
	return requestData;
}

function submitOpration(url,request,backDataContainerId){
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
				$(backDataContainerId).html(data);
		}
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
		}
	}); 
}

function submitForm(formId,checkBoxPrefix,checkBoxName){
  	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	if(checkBoxPrefix !='' && checkBoxPrefix != undefined && checkBoxName != '' && checkBoxName != undefined){
		var checkboxValues = joinCheckboxValue(checkBoxPrefix);
		request[checkBoxName]=checkboxValues;
	}
	var _formId="#"+formId;
	var action = $(_formId).attr("action");
	$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "json",
			success: function(data) {
				alert(data.msg);
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
	});
}	