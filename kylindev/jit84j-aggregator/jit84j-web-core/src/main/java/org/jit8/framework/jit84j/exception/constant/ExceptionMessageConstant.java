package org.jit8.framework.jit84j.exception.constant;


public class ExceptionMessageConstant {

	public static final String ERROR_CODE_403 = "org.jit8.framework.error.page.403";
	public static final String ERROR_CODE_404 = "org.jit8.framework.error.page.404";
	public static final String ERROR_CODE_405 = "org.jit8.framework.error.page.405";
	public static final String ERROR_CODE_406 = "org.jit8.framework.error.page.406";
	public static final String ERROR_CODE_500 = "org.jit8.framework.error.page.500";
	
}
