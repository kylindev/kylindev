package org.jit8.framework.jit84j.exception.resolver;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.exception.ApplicationException;
import org.jit8.framework.jit84j.exception.SystemException;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

public class CustomSimpleMappingExceptionResolver extends
		SimpleMappingExceptionResolver {
	private static final Logger logger = LoggerFactory.getLogger(CustomSimpleMappingExceptionResolver.class);
	
	@Override
	protected ModelAndView doResolveException(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex) {
		// Expose ModelAndView for chosen error view.
		String viewName = determineViewName(ex, request);
		if (viewName != null) {// JSP格式返回
			if (!(request.getHeader("accept").indexOf("application/json") > -1 || (request
					.getHeader("X-Requested-With") != null && request
					.getHeader("X-Requested-With").indexOf("XMLHttpRequest") > -1))) {
				// 如果不是异步请求
				// Apply HTTP status code for error views, if specified.
				// Only apply it if we're processing a top-level request.
				Integer statusCode = determineStatusCode(request, viewName);
				if (statusCode != null) {
					applyStatusCodeIfPossible(request, response, statusCode);
				}
				handleException(request, ex);
				return getModelAndView(viewName, ex, request);
			} else {
				// JSON格式返回
				try {
					PrintWriter writer = response.getWriter();
					handleException(request, ex);
					writer.write(ex.getMessage());
					writer.flush();
				} catch (IOException e) {
					logger.error(ex.getMessage(), ex);
				}
				return null;

			}
		} else {
			return null;
		}
	}

	/**
	 * @param request
	 * @param ex
	 */
	private void handleException(HttpServletRequest request, Exception ex) {
		if(ex instanceof ApplicationException){
			ApplicationException bex = ((ApplicationException) ex);
			String code = bex.getCode();
			if(StringUtils.isNotEmpty(code)){
				bex.setMessage(MessageUtil.getLocaleMessage(request, code, bex.getParams()));
			}
			logger.error(bex.getMessage(), bex);
		}else if(ex instanceof SystemException){
			SystemException sex = ((SystemException) ex);
			String code = sex.getCode();
			if(StringUtils.isNotEmpty(code)){
				sex.setMessage(MessageUtil.getLocaleMessage(request, code, sex.getParams()));
			}
			logger.error(sex.getMessage(), sex);
		}else if(ex instanceof Exception){
			logger.error(ex.getMessage(), ex);
		}else{
			logger.error(ex.getMessage(), ex);
		}
	}

}
