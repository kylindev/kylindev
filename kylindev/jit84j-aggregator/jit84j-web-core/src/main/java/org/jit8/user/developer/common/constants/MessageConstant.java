package org.jit8.user.developer.common.constants;

public class MessageConstant {

	/**直接访问URL模块 begin*/
	public static String CODE_DIRECTPERMISSION_INVALID_PERMISSION_ID = "org.jit8.100001.userinfo.directPermission.add.permissionId.invalid";
	public static String CODE_DIRECTPERMISSION_ADDED_SUCCESS = "org.jit8.100001.userinfo.directPermission.add.success";
	/**直接访问URL模块 end*/
}
