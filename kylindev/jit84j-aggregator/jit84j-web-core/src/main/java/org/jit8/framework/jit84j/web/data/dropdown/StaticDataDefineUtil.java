package org.jit8.framework.jit84j.web.data.dropdown;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.jit8.framework.jit84j.core.cache.memached.Memached;
import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StaticDataDefineUtil {
	
	private static final Logger LOGGER  = LoggerFactory.getLogger(StaticDataDefineUtil.class);

	
	public static Map<String, MemachedModel> scan(Class targetClazz){
		//Assert.assertNotNull(targetClazz);
		Map<String, MemachedModel> map = new HashMap<String,MemachedModel>();
		Method[] methods = targetClazz.getMethods();
		for(Method m : methods){
			Memached targetMemached = m.getAnnotation(Memached.class);
			if(null != targetMemached){
				MemachedModel memachedModel = new MemachedModel();
				String key = targetMemached.value();
				String name = targetMemached.name();
				
				boolean autoRefresh = targetMemached.autoRefresh();
				int expiresTime = targetMemached.expiresTime();
				long delayRefreshTime = targetMemached.delayRefreshTime();
				long period = targetMemached.period();
				boolean repeated = targetMemached.repeated();
				
				map.put(key, memachedModel);
				memachedModel.setKey(key);
				memachedModel.setName(name);
				memachedModel.setAutoRefresh(autoRefresh);
				memachedModel.setExpiresTime(expiresTime);
				memachedModel.setDelayRefreshTime(delayRefreshTime);
				memachedModel.setPeriod(period);
				memachedModel.setRepeated(repeated);
				
				memachedModel.setTargetClazz(targetClazz);
				memachedModel.setTargetMethod(m);
				memachedModel.setTargetMemached(targetMemached);
			}
		}
		return map;
	}
	
}
