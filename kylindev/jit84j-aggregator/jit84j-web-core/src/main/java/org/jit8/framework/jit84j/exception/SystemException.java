package org.jit8.framework.jit84j.exception;

public class SystemException extends RuntimeException {

	private static final long serialVersionUID = 6915772974801937817L;

	//message code from resource message property files
	private String code;
	//message default value, if code if null
	private String message;
	//message params for resource message property files
	private Object[] params;
	//exception trace
	private Throwable e;

	public SystemException() {
	}

	/**
	 * 
	 * @param code
	 * @param message
	 */
	public SystemException(String code,String message) {
		super(message);
		this.code = code;
		this.message = message;
	}

	/**
	 * 
	 * @param code
	 * @param e
	 */
	public SystemException(String code,Throwable e) {
		super(e);
		this.code = code;
		this.e = e;
	}

	/**
	 * 
	 * @param code
	 * @param message
	 * @param e
	 */
	public SystemException(String code,String message, Throwable e) {
		super(message, e);
		this.code = code;
		this.message = message;
		this.e = e;
	}
	
	public SystemException(String code,String message,Object[] params, Throwable e) {
		super(message, e);
		this.code=code;
		this.message=message;
		this.params=params;
		this.e=e;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public Throwable getE() {
		return e;
	}

	public void setE(Throwable e) {
		this.e = e;
	}
}
