package org.jit8.framework.jit84j.web.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.stereotype.Controller;

@Controller
public class BaseController  {

	
	protected Object getSession(Object key){  
        Subject currentUser = SecurityUtils.getSubject();  
        if(null != currentUser){  
            Session session = currentUser.getSession();  
            System.out.println("Session默认超时时间为[" + session.getTimeout() + "]毫秒");  
            if(null != session){  
                return session.getAttribute(key);  
            }  
        }  
        return null;
    } 
	
	protected User getCurrentUser(){  
        return (User)getSession("currentUser");
    }
	
}
