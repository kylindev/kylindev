package org.jit8.framework.jit84j.exception.constant;

import java.io.Serializable;

import org.jit8.user.developer.common.constants.kylilnboy.KylinboyDeveloperConstant;

public class BusinessExceptionConstant implements Serializable{

	private static final long serialVersionUID = -7167624222901453928L;

	/** permission exception code begin */
	public static final String PERMISSION_CODE_EXISTS = "permission_code_exists_" +KylinboyDeveloperConstant._KYLINBOY_USER_ID;
	public static final String PERMISSION_DEVELOPER_REQUIRED = "permission_developer_required"+KylinboyDeveloperConstant._KYLINBOY_USER_ID;
	
	
	/** permission exception code end */
}
