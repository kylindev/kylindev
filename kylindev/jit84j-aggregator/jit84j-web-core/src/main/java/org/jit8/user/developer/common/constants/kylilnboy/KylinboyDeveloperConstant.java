package org.jit8.user.developer.common.constants.kylilnboy;

import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.jit8.user.developer.common.constants.CommonDeveloperConstant;

public class KylinboyDeveloperConstant extends CommonDeveloperConstant{

	private static final long serialVersionUID = -2922906669417770496L;
	
	public static final String _KYLINBOY_CODE_SEPERATOR = "_";
	public static final String _KYLINBOY_CODE_SEPERATOR_O = "o";
	public static final long _KYLINBOY_USER_ID_LONG = 100001l;//开发者ID
	public static final String _KYLINBOY_USER_ID = "100001";//开发者ID
	public static final String _KYLINBOY_CODE_PARAM = "{opration}";
	
	/**
	 * 用于url中code两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_CODE_SEPERATOR_PRIFIX = _KYLINBOY_CODE_SEPERATOR_O + _KYLINBOY_CODE_SEPERATOR;
	public static final String _KYLINBOY_CODE_SEPERATOR_SUFFIX = _KYLINBOY_CODE_SEPERATOR + _KYLINBOY_CODE_SEPERATOR_O;
	
	/**
	 * 用于url中userId两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_USER_SEPERATOR_PRIFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR_U + CommonDeveloperConstant._COMMON_USER_SEPERATOR;
	public static final String _KYLINBOY_USER_SEPERATOR_SUFFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR + CommonDeveloperConstant._COMMON_USER_SEPERATOR_U;
	
	/**
	 * 用于url中userId+标识组合
	 */
	public static final String _KYLINBOY_USER_ID_AND_SEPERATOR = _KYLINBOY_USER_SEPERATOR_PRIFIX + _KYLINBOY_USER_ID + _KYLINBOY_USER_SEPERATOR_SUFFIX;
	
	/**
	 * 用于url中code+标识组合
	 */
	public static final String _KYLINBOY_CODE_AND_SEPERATOR = _KYLINBOY_CODE_PARAM + _KYLINBOY_CODE_SEPERATOR_SUFFIX;
	
	
	/**
	 * 用于url中code+userId+标识组合
	 */
	public static final String _KYLINBOY_CODE_PARAM_AND_USER_ID = _KYLINBOY_CODE_AND_SEPERATOR + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	/** UserController mapping begin */
	
	/**分配角色*/
	public static final String KYLINBOY_USER_MODULE_ID = _KYLINBOY_USER_ID + "user_manage";
	public static final String KYLINBOY_USER_MODULE = "user_manage";
	public static final String KYLINBOY_USER_MODULE_NAME = "用户管理";
	public static final String KYLINBOY_USER_MODULE_CODE = _KYLINBOY_USER_ID + KYLINBOY_USER_MODULE;
	
	public static final String KYLINBOY_USER_ASIGN_ROLES_ID = _KYLINBOY_USER_ID + "user_asign_roles";
	public static final String KYLINBOY_USER_ASIGN_ROLES = "user_asign_roles";
	public static final String KYLINBOY_USER_ASIGN_ROLES_NAME = "分配角色";
	public static final String KYLINBOY_USER_ASIGN_ROLES_DESC = "分配角色提交";
	public static final String KYLINBOY_USER_ASIGN_ROLES_URL = "/admin/userinfo/asignRolesForUser/";
	public static final String KYLINBOY_USER_ASIGN_ROLES_MAPPING = KYLINBOY_USER_ASIGN_ROLES_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_ASIGN_ROLES + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	/**分配角色UI*/
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI_ID = _KYLINBOY_USER_ID + "user_asign_roles_ui";
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI = "user_asign_roles_ui";
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI_NAME = "分配角色页面";
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI_DESC = "分配角色页面";
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI_URL = "/admin/userinfo/userListForAsignRoleUI/";
	public static final String KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING = KYLINBOY_USER_ASIGN_ROLES_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_ASIGN_ROLES_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**删除角色*/
	public static final String KYLINBOY_USER_REMOVE_ROLES_ID = _KYLINBOY_USER_ID + "user_remove_roles";
	public static final String KYLINBOY_USER_REMOVE_ROLES = "user_remove_roles";
	public static final String KYLINBOY_USER_REMOVE_ROLES_NAME = "删除角色";
	public static final String KYLINBOY_USER_REMOVE_ROLES_DESC = "删除角色";
	public static final String KYLINBOY_USER_REMOVE_ROLES_URL = "/admin/userinfo/removeRolesForUser/";
	public static final String KYLINBOY_USER_REMOVE_ROLES_MAPPING = KYLINBOY_USER_REMOVE_ROLES_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_REMOVE_ROLES + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**添加用户 /admin/userinfo/addUser/*/
	public static final String KYLINBOY_ADD_USER_ID = _KYLINBOY_USER_ID + "add_user";
	public static final String KYLINBOY_ADD_USER = "add_user";
	public static final String KYLINBOY_ADD_USER_NAME = "添加用户";
	public static final String KYLINBOY_ADD_USER_DESC = "添加用户提交";
	public static final String KYLINBOY_ADD_USER_URL = "/admin/userinfo/addUser/";
	public static final String KYLINBOY_ADD_USER_MAPPING = KYLINBOY_ADD_USER_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADD_USER + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**添加用户UI /admin/userinfo/addUserUI/*/
	public static final String KYLINBOY_ADD_USER_UI_ID = _KYLINBOY_USER_ID + "add_user_ui";
	public static final String KYLINBOY_ADD_USER_UI = "add_user_ui";
	public static final String KYLINBOY_ADD_USER_UI_NAME = "添加用户页面";
	public static final String KYLINBOY_ADD_USER_UI_DESC = "添加用户页面";
	public static final String KYLINBOY_ADD_USER_UI_URL = "/admin/userinfo/addUserUI/";
	public static final String KYLINBOY_ADD_USER_UI_MAPPING = KYLINBOY_ADD_USER_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADD_USER_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	/**用户列表UI /admin/userinfo/userListUI/*/
	public static final String KYLINBOY_USER_LIST_UI_ID = _KYLINBOY_USER_ID + "user_list_ui";
	public static final String KYLINBOY_USER_LIST_UI = "user_list_ui";
	public static final String KYLINBOY_USER_LIST_UI_NAME = "用户列表";
	public static final String KYLINBOY_USER_LIST_UI_DESC = "用户列表页面";
	public static final String KYLINBOY_USER_LIST_UI_URL = "/admin/userinfo/userListUI/";
	public static final String KYLINBOY_USER_LIST_UI_MAPPING = KYLINBOY_USER_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/**用户编辑UI /admin/userinfo/userEditUI/*/
	public static final String KYLINBOY_USER_EDIT_UI_ID = _KYLINBOY_USER_ID + "user_edit_ui";
	public static final String KYLINBOY_USER_EDIT_UI = "user_edit_ui";
	public static final String KYLINBOY_USER_EDIT_UI_NAME = "用户编辑页面";
	public static final String KYLINBOY_USER_EDIT_UI_DESC = "用户编辑页面";
	public static final String KYLINBOY_USER_EDIT_UI_URL = "/admin/userinfo/userEditUI/";

	public static final String KYLINBOY_USER_EDIT_UI_MAPPING = KYLINBOY_USER_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	/**用户编辑保存 /admin/userinfo/userEditSave/*/
	public static final String KYLINBOY_USER_EDIT_SAVE_ID = _KYLINBOY_USER_ID + "user_edit_save";
	public static final String KYLINBOY_USER_EDIT_SAVE = "user_edit_save";
	public static final String KYLINBOY_USER_EDIT_SAVE_NAME = "用户编辑保存";
	public static final String KYLINBOY_USER_EDIT_SAVE_DESC = "用户编辑保存提交";
	public static final String KYLINBOY_USER_EDIT_SAVE_URL = "/admin/userinfo/userEditSave/";
	public static final String KYLINBOY_USER_EDIT_SAVE_MAPPING = KYLINBOY_USER_EDIT_SAVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_EDIT_SAVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;


	/**用户删除 /admin/userinfo/userDelete/*/
	public static final String KYLINBOY_USER_DELETE_ID = _KYLINBOY_USER_ID + "user_delete";
	public static final String KYLINBOY_USER_DELETE = "user_delete";
	public static final String KYLINBOY_USER_DELETE_NAME = "删除用户";
	public static final String KYLINBOY_USER_DELETE_DESC = "删除用户";
	public static final String KYLINBOY_USER_DELETE_URL = "/admin/userinfo/userDelete/";
	public static final String KYLINBOY_USER_DELETE_MAPPING = KYLINBOY_USER_DELETE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_DELETE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** UserController mapping end */
	
	/** AdminIndexController mapping begin */
	
	public static final String KYLINBOY_COMMON_MODULE_ID =  _KYLINBOY_USER_ID + "common_manager";
	public static final String KYLINBOY_COMMON_MODULE = "common_manager";
	public static final String KYLINBOY_COMMON_MODULE_NAME = "公用模块";
	public static final String KYLINBOY_COMMON_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_COMMON_MODULE;
	
	/**后台首页*/
	public static final String KYLINBOY_ADMIN_INDEX_ID = _KYLINBOY_USER_ID + "admin_index";
	public static final String KYLINBOY_ADMIN_INDEX = "admin_index";
	public static final String KYLINBOY_ADMIN_INDEX_NAME = "管理后台首页";
	public static final String KYLINBOY_ADMIN_INDEX_DESC = "管理后台首页";
	public static final String KYLINBOY_ADMIN_INDEX_URL = WebConstants._ADMIN_PATH_PREFIX + "/index/";
	public static final String KYLINBOY_ADMIN_INDEX_MAPPING = KYLINBOY_ADMIN_INDEX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADMIN_INDEX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	public static final String KYLINBOY_ADMIN_INDEX_MAPPING_SIMPLE = KYLINBOY_ADMIN_INDEX_URL;//简化版路径
	
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY_ID = _KYLINBOY_USER_ID + "admin_index_summary";
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY = "admin_index_summary";
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY_NAME = "后台总览";
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY_DESC = "后台总览";
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY_URL = WebConstants._ADMIN_PATH_PREFIX + "/index/";
	public static final String KYLINBOY_ADMIN_INDEX_SUMMARY_MAPPING = KYLINBOY_ADMIN_INDEX_SUMMARY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADMIN_INDEX_SUMMARY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** AdminIndexController mapping end */
	
	/** LoginController mapping begin */
	public static final String KYLINBOY_ADMIN_COMMON_MODULE_ID =  _KYLINBOY_USER_ID + "admin_common_manager";
	public static final String KYLINBOY_ADMIN_COMMON_MODULE = "admin_common_manager";
	public static final String KYLINBOY_ADMIN_COMMON_MODULE_NAME = "后台公用模块";
	public static final String KYLINBOY_ADMIN_COMMON_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_ADMIN_COMMON_MODULE;
	
	/**后台登录UI*/
	public static final String KYLINBOY_ADMIN_LOGIN_UI_ID = _KYLINBOY_USER_ID + "admin_login_ui";
	public static final String KYLINBOY_ADMIN_LOGIN_UI = "admin_login_ui";
	public static final String KYLINBOY_ADMIN_LOGIN_UI_NAME = "管理后台登陆页面";
	public static final String KYLINBOY_ADMIN_LOGIN_UI_DESC = "管理后台登陆页面";
	public static final String KYLINBOY_ADMIN_LOGIN_UI_URL = WebConstants._ADMIN_PATH_PREFIX + "/adminlogin/";
	public static final String KYLINBOY_ADMIN_LOGIN_UI_MAPPING = KYLINBOY_ADMIN_LOGIN_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADMIN_LOGIN_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**后台登录*/
	public static final String KYLINBOY_ADMIN_LOGIN_ID = _KYLINBOY_USER_ID + "admin_login";
	public static final String KYLINBOY_ADMIN_LOGIN = "admin_login";
	public static final String KYLINBOY_ADMIN_LOGIN_NAME = "管理后台登陆";
	public static final String KYLINBOY_ADMIN_LOGIN_DESC = "管理后台登陆提交";
	public static final String KYLINBOY_ADMIN_LOGIN_URL = WebConstants._ADMIN_PATH_PREFIX + "/login/";
	public static final String KYLINBOY_ADMIN_LOGIN_MAPPING = KYLINBOY_ADMIN_LOGIN_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ADMIN_LOGIN + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/**前台登录*/
	public static final String KYLINBOY_FRONT_COMMON_MODULE_ID =  _KYLINBOY_USER_ID + "front_common_manager";
	public static final String KYLINBOY_FRONT_COMMON_MODULE = "front_common_manager";
	public static final String KYLINBOY_FRONT_COMMON_MODULE_NAME = "前台公用模块";
	public static final String KYLINBOY_FRONT_COMMON_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_FRONT_COMMON_MODULE;
	
	
	public static final String KYLINBOY_FRONT_LOGIN_ID = _KYLINBOY_USER_ID + "front_login";
	public static final String KYLINBOY_FRONT_LOGIN = "front_login";
	public static final String KYLINBOY_FRONT_LOGIN_NAME = "登陆";
	public static final String KYLINBOY_FRONT_LOGIN_DESC = "登陆提交";
	public static final String KYLINBOY_FRONT_LOGIN_URL = "/login/";
	public static final String KYLINBOY_FRONT_LOGIN_MAPPING = KYLINBOY_FRONT_LOGIN_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FRONT_LOGIN + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**前台登录UI*/
	public static final String KYLINBOY_FRONT_LOGIN_UI_ID = _KYLINBOY_USER_ID + "front_login_ui";
	public static final String KYLINBOY_FRONT_LOGIN_UI = "front_login_ui";
	public static final String KYLINBOY_FRONT_LOGIN_UI_NAME = "登录页面";
	public static final String KYLINBOY_FRONT_LOGIN_UI_DESC = "登录页面";
	public static final String KYLINBOY_FRONT_LOGIN_UI_URL = "/loginUI/";
	public static final String KYLINBOY_FRONT_LOGIN_UI_MAPPING = KYLINBOY_FRONT_LOGIN_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FRONT_LOGIN_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**登出*/
	public static final String KYLINBOY_LOGOUT_ID = _KYLINBOY_USER_ID + "logout";
	public static final String KYLINBOY_LOGOUT = "logout";
	public static final String KYLINBOY_LOGOUT_NAME = "登出系统";
	public static final String KYLINBOY_LOGOUT_DESC = "登出系统";
	public static final String KYLINBOY_LOGOUT_URL = "/logout/";
	public static final String KYLINBOY_LOGOUT_MAPPING = KYLINBOY_LOGOUT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_LOGOUT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**验证码*/
	public static final String KYLINBOY_VALIDATE_CODE_ID = _KYLINBOY_USER_ID + "validate_code";
	public static final String KYLINBOY_VALIDATE_CODE = "validate_code";
	public static final String KYLINBOY_VALIDATE_CODE_NAME = "验证码";
	public static final String KYLINBOY_VALIDATE_CODE_DESC = "验证码";
	public static final String KYLINBOY_VALIDATE_CODE_URL = "/validateCode";
	public static final String KYLINBOY_VALIDATE_CODE_MAPPING = KYLINBOY_VALIDATE_CODE_URL;
	
	/**切换语言*/
	public static final String KYLINBOY_CHANGE_LANGUAGE_ID = _KYLINBOY_USER_ID + "change_language";
	public static final String KYLINBOY_CHANGE_LANGUAGE = "change_language";
	public static final String KYLINBOY_CHANGE_LANGUAGE_NAME = "切换语言";
	public static final String KYLINBOY_CHANGE_LANGUAGE_DESC = "切换语言";
	public static final String KYLINBOY_CHANGE_LANGUAGE_URL = "/changelanguage";
	public static final String KYLINBOY_CHANGE_LANGUAGE_MAPPING = KYLINBOY_CHANGE_LANGUAGE_URL+ _KYLINBOY_USER_ID_AND_SEPERATOR;
	/** LoginController mapping end */
	
	/** IndexController mapping end */
	/**前台首页*/
	public static final String KYLINBOY_FRONT_INDEX_UI_ID = _KYLINBOY_USER_ID + "front_index_ui";
	public static final String KYLINBOY_FRONT_INDEX_UI = "front_index_ui";
	public static final String KYLINBOY_FRONT_INDEX_UI_NAME = "前端首页";
	public static final String KYLINBOY_FRONT_INDEX_UI_DESC = "前端首页路径";
	public static final String KYLINBOY_FRONT_INDEX_UI_URL = "/index/";
	public static final String KYLINBOY_FRONT_INDEX_UI_MAPPING = KYLINBOY_FRONT_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FRONT_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	public static final String KYLINBOY_FRONT_INDEX_ID = _KYLINBOY_USER_ID + "index";
	public static final String KYLINBOY_FRONT_INDEX_MAPPING = "/index";
	
	/** IndexController mapping end */
	
	
	
	
	/** PasswordController mapping end */
	/** 忘记密码 */
	public static final String KYLINBOY_FORGET_PASSWORD_UI_ID = _KYLINBOY_USER_ID + "forget_password_ui";
	public static final String KYLINBOY_FORGET_PASSWORD_UI = "forget_password_ui";
	public static final String KYLINBOY_FORGET_PASSWORD_UI_NAME = "忘记密码";
	public static final String KYLINBOY_FORGET_PASSWORD_UI_DESC = "忘记密码页面";
	public static final String KYLINBOY_FORGET_PASSWORD_UI_URL = "/front/ui/password/";
	public static final String KYLINBOY_FORGET_PASSWORD_UI_MAPPING = KYLINBOY_FORGET_PASSWORD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FORGET_PASSWORD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	public static final String KYLINBOY_FORGET_PASSWORD_ID = _KYLINBOY_USER_ID + "forget_password_2_ui";
	public static final String KYLINBOY_FORGET_PASSWORD = "forget_password_2_ui";
	public static final String KYLINBOY_FORGET_PASSWORD_NAME = "忘记密码";
	public static final String KYLINBOY_FORGET_PASSWORD_DESC = "忘记密码提交";
	public static final String KYLINBOY_FORGET_PASSWORD_URL = "/front/ui/password/";
	public static final String KYLINBOY_FORGET_PASSWORD_MAPPING = KYLINBOY_FORGET_PASSWORD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FORGET_PASSWORD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI_ID = _KYLINBOY_USER_ID + "forget_password_email_ui";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI = "forget_password_email_ui";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI_NAME = "前端重置密码【Email】";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI_DESC = "前端重置密码【Email】邮箱点击返回页面";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI_URL = "/front/ui/password/";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_UI_MAPPING = KYLINBOY_FORGET_PASSWORD_EMAIL_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FORGET_PASSWORD_EMAIL_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO_ID = _KYLINBOY_USER_ID + "forget_password_email_do";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO = "forget_password_email_do";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO_NAME = "重置密码【Email】提交";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO_DESC = "重置密码【Email】提交";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO_URL = "/front/ui/password/";
	public static final String KYLINBOY_FORGET_PASSWORD_EMAIL_DO_MAPPING = KYLINBOY_FORGET_PASSWORD_EMAIL_DO_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FORGET_PASSWORD_EMAIL_DO + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** PasswordController mapping end */
	
	
	
	
	/** DeveloperController mapping end */
	public static final String KYLINBOY_DEVELOPER_MODULE = "developer_manager";
	public static final String KYLINBOY_DEVELOPER_MODULE_NAME = "开发者模块";
	public static final String KYLINBOY_DEVELOPER_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_DEVELOPER_MODULE;
	public static final String KYLINBOY_DEVELOPER_MODULE_ID =  _KYLINBOY_USER_ID + "developer_manager";
	/**申请成为开发者*/
	public static final String KYLINBOY_APPLY_DEVELOPER_UI_ID = _KYLINBOY_USER_ID + "apply_developer_ui";
	public static final String KYLINBOY_APPLY_DEVELOPER_UI = "apply_developer_ui";
	public static final String KYLINBOY_APPLY_DEVELOPER_UI_NAME = "申请成为开发者";
	public static final String KYLINBOY_APPLY_DEVELOPER_UI_DESC = "申请成为开发者";
	public static final String KYLINBOY_APPLY_DEVELOPER_UI_URL = "/admin/userinfo/developer/applyDeveloperUI/";
	public static final String KYLINBOY_APPLY_DEVELOPER_UI_MAPPING = KYLINBOY_APPLY_DEVELOPER_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_APPLY_DEVELOPER_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/**列表开发者*/
	public static final String KYLINBOY_DEVELOPER_LIST_UI_ID = _KYLINBOY_USER_ID + "list_developer_ui";
	public static final String KYLINBOY_DEVELOPER_LIST_UI = "list_developer_ui";
	public static final String KYLINBOY_DEVELOPER_LIST_UI_NAME = "开发者列表";
	public static final String KYLINBOY_DEVELOPER_LIST_UI_DESC = "开发者列表";
	public static final String KYLINBOY_DEVELOPER_LIST_UI_URL = "/admin/userinfo/developer/developerListUI/";
	public static final String KYLINBOY_DEVELOPER_LIST_UI_MAPPING = KYLINBOY_DEVELOPER_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DEVELOPER_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**添加开发者*/
	public static final String KYLINBOY_DEVELOPER_ADD_ID = _KYLINBOY_USER_ID + "developer_add";
	public static final String KYLINBOY_DEVELOPER_ADD = "developer_add";
	public static final String KYLINBOY_DEVELOPER_ADD_NAME = "添加开发者";
	public static final String KYLINBOY_DEVELOPER_ADD_DESC = "添加开发者";
	public static final String KYLINBOY_DEVELOPER_ADD_URL = "/admin/userinfo/developer/addDeveloper/";
	public static final String KYLINBOY_DEVELOPER_ADD_MAPPING = KYLINBOY_DEVELOPER_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DEVELOPER_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**添加开发者UI*/
	public static final String KYLINBOY_DEVELOPER_ADD_UI_ID = _KYLINBOY_USER_ID + "developer_add_ui";
	public static final String KYLINBOY_DEVELOPER_ADD_UI = "developer_add_ui";
	public static final String KYLINBOY_DEVELOPER_ADD_UI_NAME = "添加开发者页面";
	public static final String KYLINBOY_DEVELOPER_ADD_UI_DESC = "添加开发者页面";
	public static final String KYLINBOY_DEVELOPER_ADD_UI_URL = "/admin/userinfo/developer/addDeveloperUI/";
	public static final String KYLINBOY_DEVELOPER_ADD_UI_MAPPING = KYLINBOY_DEVELOPER_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DEVELOPER_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**开发者列表，将该权限分配为该开发者UI*/
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX_ID = _KYLINBOY_USER_ID + "developer_list_ajax";
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX = "developer_list_ajax";
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX_NAME = "开发者列表异步请求";
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX_DESC = "开发者列表异步请求路径";
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX_URL = "/admin/userinfo/developer/developerListUI/";
	public static final String KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING = KYLINBOY_DEVELOPER_LIST_AJAX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DEVELOPER_LIST_AJAX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**开发者列表带查询，将该权限分配为该开发者UI*/
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX_ID = _KYLINBOY_USER_ID + "developer_query_ajax";
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX = "developer_query_ajax";
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX_NAME = "开发者带查询列表异步请求";
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX_DESC = "开发者带查询列表异步请求路径";
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX_URL = "/admin/userinfo/developer/developerListUI/";
	public static final String KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING = KYLINBOY_DEVELOPER_QUERY_AJAX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DEVELOPER_QUERY_AJAX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	
	/**列表Constant model*/
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI_ID = _KYLINBOY_USER_ID + "list_constant_model_ui";
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI = "list_constant_model_ui";
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI_NAME = "Constant model List";
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI_DESC = "Constant model List";
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI_URL = "/admin/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING = KYLINBOY_CONSTANT_MODEL_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	/**列表Constant model 仅有查看功能*/
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_ID = _KYLINBOY_USER_ID + "view_list_constant_model_ui";
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI = "view_list_constant_model_ui";
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_NAME = "View Constant model List";
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_DESC = "View Constant model List";
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_URL = "/admin/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_MAPPING = KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	
	/**添加Constant model*/
	public static final String KYLINBOY_CONSTANT_MODEL_ADD_ID = _KYLINBOY_USER_ID + "constant_model_add";
	public static final String KYLINBOY_CONSTANT_MODEL_ADD = "constant_model_add";
	public static final String KYLINBOY_CONSTANT_MODEL_ADD_NAME = "Constant model add";
	public static final String KYLINBOY_CONSTANT_MODEL_ADD_DESC = "Constant model add";
	public static final String KYLINBOY_CONSTANT_MODEL_ADD_URL = "/admin/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_ADD_MAPPING = KYLINBOY_CONSTANT_MODEL_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**更新Constant model*/
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE_ID = _KYLINBOY_USER_ID + "constant_model_update";
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE = "constant_model_update";
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE_NAME = "Constant model update";
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE_DESC = "Constant model update";
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE_URL = "/admin/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_UPDATE_MAPPING = KYLINBOY_CONSTANT_MODEL_UPDATE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_UPDATE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**删除Constant model*/
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE_ID = _KYLINBOY_USER_ID + "constant_model_remove";
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE = "constant_model_remove";
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE_NAME = "Constant model remove";
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE_DESC = "Constant model remove";
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE_URL = "/admin/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_REMOVE_MAPPING = KYLINBOY_CONSTANT_MODEL_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	/**添加Constant model UI*/
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_ID = _KYLINBOY_USER_ID + "constant_model_show_form";
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI = "constant_model_show_form";
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_NAME = "Constant model show form";
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_DESC = "Constant model show form";
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_URL = "/admin/blank/userinfo/developer/";
	public static final String KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_MAPPING = KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** DeveloperController mapping end */
	
	/** PermissionController mapping begin */
	
	public static final String KYLINBOY_PERMISSION_MODULE_ID =  _KYLINBOY_USER_ID + "permission_manager";
	public static final String KYLINBOY_PERMISSION_MODULE = "permission_manager";
	public static final String KYLINBOY_PERMISSION_MODULE_NAME = "权限模块";
	public static final String KYLINBOY_PERMISSION_MODULE_CODE = _KYLINBOY_USER_ID + KYLINBOY_PERMISSION_MODULE;
	
	
	public static final String KYLINBOY_PERMISSION_LIST_UI_ID = _KYLINBOY_USER_ID + "permission_list_ui";
	public static final String KYLINBOY_PERMISSION_LIST_UI = "permission_list_ui";
	public static final String KYLINBOY_PERMISSION_LIST_UI_NAME= "权限列表";
	public static final String KYLINBOY_PERMISSION_LIST_UI_DESC = "指向权限列表页面";
	public static final String KYLINBOY_PERMISSION_LIST_UI_URL = "/admin/userinfo/permission/permissionListUI/";
	public static final String KYLINBOY_PERMISSION_LIST_UI_MAPPING = KYLINBOY_PERMISSION_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_PERMISSION_LIST_MODIFY_ID = _KYLINBOY_USER_ID + "permission_list_modify";
	public static final String KYLINBOY_PERMISSION_LIST_MODIFY = "permission_list_modify";
	public static final String KYLINBOY_PERMISSION_LIST_NAME = "权限列表修改";
	public static final String KYLINBOY_PERMISSION_LIST_DESC = "权限列表修改提交";
	public static final String KYLINBOY_PERMISSION_LIST_MODIFY_URL = "/admin/userinfo/permission/permissionList/modify/";
	public static final String KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING = KYLINBOY_PERMISSION_LIST_MODIFY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_LIST_MODIFY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_PERMISSION_MODIFY_UI_ID = _KYLINBOY_USER_ID + "permission_modify_ui";
	public static final String KYLINBOY_PERMISSION_MODIFY_UI = "permission_modify_ui";
	public static final String KYLINBOY_PERMISSION_MODIFY_UI_NAME = "权限修改";
	public static final String KYLINBOY_PERMISSION_MODIFY_UI_DESC = "权限修改页面";
	public static final String KYLINBOY_PERMISSION_MODIFY_UI_URL = "/admin/userinfo/permission/modifyPermissionUI";
	public static final String KYLINBOY_PERMISSION_MODIFY_UI_MAPPING = KYLINBOY_PERMISSION_MODIFY_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_MODIFY_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_PERMISSION_ADD_UI_ID = _KYLINBOY_USER_ID + "permission_add_ui";
	public static final String KYLINBOY_PERMISSION_ADD_UI = "permission_add_ui";
	public static final String KYLINBOY_PERMISSION_ADD_UI_NAME = "权限添加页面";
	public static final String KYLINBOY_PERMISSION_ADD_UI_DESC = "权限添加页面";
	public static final String KYLINBOY_PERMISSION_ADD_UI_URL = "/admin/userinfo/permission/addPermissionUI/";
	public static final String KYLINBOY_PERMISSION_ADD_UI_MAPPING = KYLINBOY_PERMISSION_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_PERMISSION_ADD_ID = _KYLINBOY_USER_ID + "permission_add";
	public static final String KYLINBOY_PERMISSION_ADD = "permission_add";
	public static final String KYLINBOY_PERMISSION_ADD_NAME = "权限添加";
	public static final String KYLINBOY_PERMISSION_ADD_DESC = "权限添加提交";
	public static final String KYLINBOY_PERMISSION_ADD_URL = "/admin/userinfo/permission/addPermission/";
	public static final String KYLINBOY_PERMISSION_ADD_MAPPING = KYLINBOY_PERMISSION_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	//权限类别管理首页
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX_ID = _KYLINBOY_USER_ID + "permission_category_index";
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX = "permission_category_index";
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX_NAME= "权限类别管理";
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX_DESC = "指向权限类别页面";
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX_URL = "/admin/userinfo/permission/category/";
	public static final String KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING = KYLINBOY_PERMISSION_CATEGORY_INDEX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_INDEX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	//权限类别添加
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD_ID = _KYLINBOY_USER_ID + "permission_category_add";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD = "permission_category_add";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD_NAME= "权限类别添加";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD_DESC = "权限类别添加";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD_URL = "/admin/userinfo/permission/category/add/";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ADD_MAPPING = KYLINBOY_PERMISSION_CATEGORY_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	//权限类别删除
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ID = _KYLINBOY_USER_ID + "permission_category_remove";
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE = "permission_category_remove";
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_NAME= "权限类别删除";
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_DESC = "权限类别删除";
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_URL = "/admin/userinfo/permission/category/remove/";
	public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING = KYLINBOY_PERMISSION_CATEGORY_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	//为权限类别分配权限
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN_ID = _KYLINBOY_USER_ID + "permission_category_assign";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN = "permission_category_assign";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN_NAME= "为权限类别分配权限";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN_DESC = "为权限类别分配权限";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN_URL = "/admin/userinfo/permission/category/assign/";
	public static final String KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING = KYLINBOY_PERMISSION_CATEGORY_ASSIGN_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_ASSIGN + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	//从权限类别移除权限
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_ID = _KYLINBOY_USER_ID + "permission_category_remove_from";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM = "permission_category_remove_from";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_NAME= "从权限类别移除权限";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_DESC = "从权限类别移除权限";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_URL = "/admin/userinfo/permission/category/remove/from/";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING = KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

		//从所有权限类别移除该权限
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_ID = _KYLINBOY_USER_ID + "permission_category_remove_all_from";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM = "permission_category_remove_all_from";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_NAME= "从所有权限类别移除该权限";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_DESC = "从所有权限类别移除该权限";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_URL = "/admin/userinfo/permission/category/removeAll/from/";
		public static final String KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_MAPPING = KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

		//启用该权限
		public static final String KYLINBOY_PERMISSION_ENABLE_MAPPING_ID = _KYLINBOY_USER_ID + "permission_enable";
		public static final String KYLINBOY_PERMISSION_ENABLE = "permission_enable";
		public static final String KYLINBOY_PERMISSION_ENABLE_NAME= "启用该权限";
		public static final String KYLINBOY_PERMISSION_ENABLE_DESC = "启用该权限";
		public static final String KYLINBOY_PERMISSION_ENABLE_URL = "/admin/userinfo/permission/enable/";
		public static final String KYLINBOY_PERMISSION_ENABLE_MAPPING = KYLINBOY_PERMISSION_ENABLE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_PERMISSION_ENABLE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

				
		
		
	/** PermissionController mapping end */
	/** DirectPermissionController mapping begin */
	
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI_ID = _KYLINBOY_USER_ID + "direct_permission_list_ui";
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI = "direct_permission_list_ui";
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI_NAME = "白名单列表";
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI_DESC = "直接权限列表";
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI_URL = "/admin/userinfo/permission/directPermissionListUI/";
	public static final String KYLINBOY_DIRECT_PERMISSION_LIST_UI_MAPPING = KYLINBOY_DIRECT_PERMISSION_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DIRECT_PERMISSION_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD_ID = _KYLINBOY_USER_ID + "direct_permission_add";
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD = "direct_permission_add";
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD_NAME = "白名单添加";
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD_DESC = "直接权限添加";
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD_URL = "/admin/userinfo/permission/directPermissionAdd/";
	public static final String KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING = KYLINBOY_DIRECT_PERMISSION_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_DIRECT_PERMISSION_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_URL_MAPPING_ID = _KYLINBOY_USER_ID + "url_mapping";
	public static final String KYLINBOY_URL_MAPPING = "url_mapping";
	public static final String KYLINBOY_URL_MAPPING_NAME = "URL映射";
	public static final String KYLINBOY_URL_MAPPING_DESC = "URL映射路径";
	public static final String KYLINBOY_URL_MAPPING_URL = "/admin/userinfo/urlMappingListUI";
	public static final String KYLINBOY_URL_MAPPING_MAPPING = KYLINBOY_URL_MAPPING_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_MAPPING + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_URL_MAPPING_AJAX_ID = _KYLINBOY_USER_ID + "url_mapping_ajax";
	public static final String KYLINBOY_URL_MAPPING_AJAX = "url_mapping_ajax";
	public static final String KYLINBOY_URL_MAPPING_AJAX_NAME = "URL映射";
	public static final String KYLINBOY_URL_MAPPING_AJAX_DESC = "URL映射路径";
	public static final String KYLINBOY_URL_MAPPING_AJAX_URL = "/admin/blank/userinfo/urlMappingListUI";
	public static final String KYLINBOY_URL_MAPPING_AJAX_MAPPING = KYLINBOY_URL_MAPPING_AJAX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_MAPPING_AJAX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX_ID = _KYLINBOY_USER_ID + "url_mapping_filter_ajax";
	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX = "url_mapping_filter_ajax";
	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX_NAME = "URL映射分类";
	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX_DESC = "URL映射路径分类";
	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX_URL = "/admin/blank/userinfo/urlMappingListUI";
	public static final String KYLINBOY_URL_MAPPING_FILTER_AJAX_MAPPING = KYLINBOY_URL_MAPPING_FILTER_AJAX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_MAPPING_FILTER_AJAX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_URL_MAPPING_IMPORT_ID = _KYLINBOY_USER_ID + "url_mapping_import";
	public static final String KYLINBOY_URL_MAPPING_IMPORT = "url_mapping_import";
	public static final String KYLINBOY_URL_MAPPING_IMPORT_NAME = "URL映射导入DB";
	public static final String KYLINBOY_URL_MAPPING_IMPORT_DESC = "URL映射路径导入DB";
	public static final String KYLINBOY_URL_MAPPING_IMPORT_URL = "/admin/userinfo/urlMappingImport";
	public static final String KYLINBOY_URL_MAPPING_IMPORT_MAPPING = KYLINBOY_URL_MAPPING_IMPORT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_MAPPING_IMPORT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ_ID = _KYLINBOY_USER_ID + "url_mapping_persist_by_clazz";
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ = "url_mapping_persist_by_clazz";
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ_NAME = "URL映射导入DB";
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ_DESC = "URL映射路径导入DB";
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ_URL = "/admin/userinfo/urlMappingImport";
	public static final String KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING = KYLINBOY_URL_PERSISTER_BY_CLAZZ_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_PERSISTER_BY_CLAZZ + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_ID = _KYLINBOY_USER_ID + "url_mapping_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST = "url_mapping_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_NAME = "URL映射列表";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_DESC = "URL映射列表";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_MAPPING = KYLINBOY_URL_STATIC_MAPPING_LIST_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_STATIC_MAPPING_LIST + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_ID = _KYLINBOY_USER_ID + "url_static_mapping_module_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST = "url_static_mapping_module_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_NAME = "URL映射分类";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_DESC = "URL映射分类";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_MAPPING = KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_ID = _KYLINBOY_USER_ID + "url_static_mapping_module_for_navigation_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION = "url_static_mapping_module_for_navigation_list";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_NAME = "URL映射分类[For Navigation]";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_DESC = "URL映射分类[For Navigation]";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_MAPPING = KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_ID = _KYLINBOY_USER_ID + "url_mapping_list_for_navigation";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION = "url_mapping_list_for_navigation";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_NAME = "URL映射列表[导航菜单]";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_DESC = "URL映射列表[导航菜单]";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING = KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION_ID = _KYLINBOY_USER_ID + "url_bind_to_permission";
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION = "url_bind_to_permission";
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION_NAME = "URL关联权限";
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION_DESC = "URL关联权限";
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING = KYLINBOY_URL_BIND_TO_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_BIND_TO_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION_ID = _KYLINBOY_USER_ID + "all_url_bind_to_permission";
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION = "all_url_bind_to_permission";
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION_NAME = "所有URL关联权限";
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION_DESC = "所有URL关联权限";
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_ALL_URL_BIND_TO_PERMISSION_MAPPING = KYLINBOY_ALL_URL_BIND_TO_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ALL_URL_BIND_TO_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION_ID = _KYLINBOY_USER_ID + "url_unbind_from_permission";
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION = "url_unbind_from_permission";
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION_NAME = "URL解除权限关联";
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION_DESC = "URL解除权限关联";
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING = KYLINBOY_URL_UNBIND_FROM_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_UNBIND_FROM_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT_ID = _KYLINBOY_USER_ID + "url_init_ui";
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT = "url_init_ui";
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT_NAME = "URL初始化";
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT_DESC = "URL初始化";
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_STATIC_MAPPING_INIT_MAPPING = KYLINBOY_URL_STATIC_MAPPING_INIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_STATIC_MAPPING_INIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION_ID = _KYLINBOY_USER_ID + "url_bind_to_navigation";
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION = "url_bind_to_navigation";
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION_NAME = "URL关联导航菜单";
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION_DESC = "URL关联导航菜单";
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING = KYLINBOY_URL_BIND_TO_NAVIGATION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_BIND_TO_NAVIGATION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION_ID = _KYLINBOY_USER_ID + "url_unbind_from_navigation";
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION = "url_unbind_from_navigation";
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION_NAME = "URL解除导航菜单关联";
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION_DESC = "URL解除导航菜单关联";
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING = KYLINBOY_URL_UNBIND_FROM_NAVIGATION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_URL_UNBIND_FROM_NAVIGATION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** PermissionController mapping begin */
	
	
	
	
	
	/** RoleController mapping begin */
	
	public static final String KYLINBOY_ROLE_MODULE_ID =  _KYLINBOY_USER_ID + "role_manager";
	public static final String KYLINBOY_ROLE_MODULE = "role_manager";
	public static final String KYLINBOY_ROLE_MODULE_NAME = "角色模块";
	public static final String KYLINBOY_ROLE_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_ROLE_MODULE;
	
	
	public static final String KYLINBOY_ROLE_LIST_UI_ID = _KYLINBOY_USER_ID + "role_list_ui";
	public static final String KYLINBOY_ROLE_LIST_UI = "role_list_ui";
	public static final String KYLINBOY_ROLE_LIST_UI_NAME = "角色列表";
	public static final String KYLINBOY_ROLE_LIST_UI_DESC = "角色列表页面";
	public static final String KYLINBOY_ROLE_LIST_UI_URL = "/admin/userinfo/role/roleListUI/";
	public static final String KYLINBOY_ROLE_LIST_UI_MAPPING = KYLINBOY_ROLE_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ROLE_LIST_MODIFY_ID = _KYLINBOY_USER_ID + "role_list_modify";
	public static final String KYLINBOY_ROLE_LIST_MODIFY = "role_list_modify";
	public static final String KYLINBOY_ROLE_LIST_MODIFY_NAME = "角色列表修改";
	public static final String KYLINBOY_ROLE_LIST_MODIFY_DESC = "角色列表个性提交";
	public static final String KYLINBOY_ROLE_LIST_MODIFY_URL = "/admin/userinfo/role/roleList/modify/";
	public static final String KYLINBOY_ROLE_LIST_MODIFY_MAPPING = KYLINBOY_ROLE_LIST_MODIFY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_LIST_MODIFY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ROLE_ADD_UI_ID = _KYLINBOY_USER_ID + "role_add_ui";
	public static final String KYLINBOY_ROLE_ADD_UI = "role_add_ui";
	public static final String KYLINBOY_ROLE_ADD_UI_NAME = "角色添加页面";
	public static final String KYLINBOY_ROLE_ADD_UI_DESC = "角色添加页面";
	public static final String KYLINBOY_ROLE_ADD_UI_URL = "/admin/userinfo/role/addRoleUI/";
	public static final String KYLINBOY_ROLE_ADD_UI_MAPPING = KYLINBOY_ROLE_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI_ID = _KYLINBOY_USER_ID + "asign_permission_ui";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI = "asign_permission_ui";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI_NAME = "角色授权";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI_DESC = "为角色添分配权限";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI_URL = "/admin/userinfo/role/";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_UI_MAPPING = KYLINBOY_ROLE_ASIGN_PERMISSION_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_ASIGN_PERMISSION_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_ID = _KYLINBOY_USER_ID + "asign_role_permission";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION = "asign_role_permission";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_NAME = "角色授权执行";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_DESC = "为角色分配权限执行";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_ROLE_ASIGN_PERMISSION_MAPPING = KYLINBOY_ROLE_ASIGN_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_ASIGN_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION_ID = _KYLINBOY_USER_ID + "remove_role_permission";
	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION = "remove_role_permission";
	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION_NAME = "角色移除权限执行";
	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION_DESC = "为角色移除权限执行";
	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_ROLE_REMOVE_PERMISSION_MAPPING = KYLINBOY_ROLE_REMOVE_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_REMOVE_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_ID = _KYLINBOY_USER_ID + "remove_all_role_permission";
	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION = "remove_all_role_permission";
	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_NAME = "角色移除所有权限执行";
	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_DESC = "为角色移除所有权限执行";
	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_MAPPING = KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ROLE_REMOVE_ALL_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SELECTED_ROLE_UI_ID = _KYLINBOY_USER_ID + "selected_role_ui";
	public static final String KYLINBOY_SELECTED_ROLE_UI = "selected_role_ui";
	public static final String KYLINBOY_SELECTED_ROLE_UI_NAME = "选择角色";
	public static final String KYLINBOY_SELECTED_ROLE_UI_DESC = "选择角色ajax请求";
	public static final String KYLINBOY_SELECTED_ROLE_UI_URL = "/admin/userinfo/selected/";
	public static final String KYLINBOY_SELECTED_ROLE_UI_MAPPING = KYLINBOY_SELECTED_ROLE_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SELECTED_ROLE_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI_ID = _KYLINBOY_USER_ID + "asign_user_permission_ui";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI = "asign_user_permission_ui";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI_NAME = "用户单独授权";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI_DESC = "为用户添分配权限";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI_URL = "/admin/userinfo/role/";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING = KYLINBOY_USER_ASIGN_PERMISSION_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_ASIGN_PERMISSION_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_ASIGN_PERMISSION_ID = _KYLINBOY_USER_ID + "asign_user_permission";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION = "asign_user_permission";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_NAME = "用户单独授权执行";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_DESC = "为用户添分配权限执行";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_USER_ASIGN_PERMISSION_MAPPING = KYLINBOY_USER_ASIGN_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_ASIGN_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_REMOVE_PERMISSION_ID = _KYLINBOY_USER_ID + "remove_user_permission";
	public static final String KYLINBOY_USER_REMOVE_PERMISSION = "remove_user_permission";
	public static final String KYLINBOY_USER_REMOVE_PERMISSION_NAME = "用户移除权限执行";
	public static final String KYLINBOY_USER_REMOVE_PERMISSION_DESC = "为用户移除权限执行";
	public static final String KYLINBOY_USER_REMOVE_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_USER_REMOVE_PERMISSION_MAPPING = KYLINBOY_USER_REMOVE_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_REMOVE_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION_ID = _KYLINBOY_USER_ID + "remove_all_user_permission";
	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION = "remove_all_user_permission";
	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION_NAME = "用户移除所有权限执行";
	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION_DESC = "为用户移除所有权限执行";
	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION_URL = "/admin/userinfo/";
	public static final String KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING = KYLINBOY_USER_REMOVE_ALL_PERMISSION_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_REMOVE_ALL_PERMISSION + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SELECTED_USER_UI_ID = _KYLINBOY_USER_ID + "selected_user_ui";
	public static final String KYLINBOY_SELECTED_USER_UI = "selected_user_ui";
	public static final String KYLINBOY_SELECTED_USER_UI_NAME = "选择用户";
	public static final String KYLINBOY_SELECTED_USER_UI_DESC = "选择用户ajax请求";
	public static final String KYLINBOY_SELECTED_USER_UI_URL = "/admin/userinfo/selected/";
	public static final String KYLINBOY_SELECTED_USER_UI_MAPPING = KYLINBOY_SELECTED_USER_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SELECTED_USER_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FIND_PASSWORD_UI_ID = _KYLINBOY_USER_ID + "find_password_ui";
	public static final String KYLINBOY_FIND_PASSWORD_UI = "find_password_ui";
	public static final String KYLINBOY_FIND_PASSWORD_UI_NAME = "找回密码";
	public static final String KYLINBOY_FIND_PASSWORD_UI_DESC = "找回密码页面";
	public static final String KYLINBOY_FIND_PASSWORD_UI_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_FIND_PASSWORD_UI_MAPPING = KYLINBOY_FIND_PASSWORD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FIND_PASSWORD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FIND_PASSWORD_ID = _KYLINBOY_USER_ID + "find_password";
	public static final String KYLINBOY_FIND_PASSWORD = "find_password";
	public static final String KYLINBOY_FIND_PASSWORD_NAME = "找回密码";
	public static final String KYLINBOY_FIND_PASSWORD_DESC = "找回密码提交";
	public static final String KYLINBOY_FIND_PASSWORD_URL = "/admin/main/userinfo/DO";
	public static final String KYLINBOY_FIND_PASSWORD_MAPPING = KYLINBOY_FIND_PASSWORD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FIND_PASSWORD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_RESET_PASSWORD_UI_ID = _KYLINBOY_USER_ID + "reset_password_ui";
	public static final String KYLINBOY_RESET_PASSWORD_UI = "reset_password_ui";
	public static final String KYLINBOY_RESET_PASSWORD_UI_NAME = "重置密码【系统】";
	public static final String KYLINBOY_RESET_PASSWORD_UI_DESC = "重置密码【系统】页面";
	public static final String KYLINBOY_RESET_PASSWORD_UI_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_UI_MAPPING = KYLINBOY_RESET_PASSWORD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI_ID = _KYLINBOY_USER_ID + "reset_password_email_ui";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI = "reset_password_email_ui";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI_NAME = "重置密码【Email】";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI_DESC = "重置密码【Email】邮箱点击返回页面";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_UI_MAPPING = KYLINBOY_RESET_PASSWORD_EMAIL_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD_EMAIL_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_ID = _KYLINBOY_USER_ID + "reset_password_email_admin_ui";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI = "reset_password_email_admin_ui";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_NAME = "重置密码【Email】";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_DESC = "重置密码【Email】Admin页面";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_MAPPING = KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_RESET_PASSWORD_ID = _KYLINBOY_USER_ID + "reset_password";
	public static final String KYLINBOY_RESET_PASSWORD = "reset_password";
	public static final String KYLINBOY_RESET_PASSWORD_NAME = "重置密码【系统】提交";
	public static final String KYLINBOY_RESET_PASSWORD_DESC = "重置密码【系统】提交";
	public static final String KYLINBOY_RESET_PASSWORD_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_MAPPING = KYLINBOY_RESET_PASSWORD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO_ID = _KYLINBOY_USER_ID + "reset_password_email_do";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO = "reset_password_email_do";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO_NAME = "重置密码【Email】提交";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO_DESC = "重置密码【Email】提交";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_EMAIL_DO_MAPPING = KYLINBOY_RESET_PASSWORD_EMAIL_DO_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD_EMAIL_DO + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_RESET_PASSWORD_AUTO_ID = _KYLINBOY_USER_ID + "reset_password_auto";
	public static final String KYLINBOY_RESET_PASSWORD_AUTO = "reset_password_auto";
	public static final String KYLINBOY_RESET_PASSWORD_AUTO_NAME = "重置密码【系统】提交自动生成";
	public static final String KYLINBOY_RESET_PASSWORD_AUTO_DESC = "重置密码【系统】提交自动生成密码";
	public static final String KYLINBOY_RESET_PASSWORD_AUTO_URL = "/admin/main/userinfo/";
	public static final String KYLINBOY_RESET_PASSWORD_AUTO_MAPPING = KYLINBOY_RESET_PASSWORD_AUTO_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_RESET_PASSWORD_AUTO + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** RoleController mapping end */
	
	/** ExceptionController mapping begin */
	
	public static final String KYLINBOY_ERROR_MODULE_ID = _KYLINBOY_USER_ID + "error_manager";
	public static final String KYLINBOY_ERROR_MODULE = "error_manager";
	public static final String KYLINBOY_ERROR_MODULE_NAME = "错误管理模块";
	public static final String KYLINBOY_ERROR_MODULE_CODE = _KYLINBOY_USER_ID + KYLINBOY_ERROR_MODULE;

	public static final String KYLINBOY_EXCEPTION_403_UI_ID = _KYLINBOY_USER_ID + "error_403_ui";
	public static final String KYLINBOY_EXCEPTION_403_UI = "error_403_ui";
	public static final String KYLINBOY_EXCEPTION_403_UI_NAME = "403错误";
	public static final String KYLINBOY_EXCEPTION_403_UI_DESC = "403错误页面";
	public static final String KYLINBOY_EXCEPTION_403_UI_URL = "/error/";
	public static final String KYLINBOY_EXCEPTION_403_UI_MAPPING = KYLINBOY_EXCEPTION_403_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_EXCEPTION_403_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_EXCEPTION_404_UI_ID = _KYLINBOY_USER_ID + "error_404_ui";
	public static final String KYLINBOY_EXCEPTION_404_UI = "error_404_ui";
	public static final String KYLINBOY_EXCEPTION_404_UI_NAME = "404错误";
	public static final String KYLINBOY_EXCEPTION_404_UI_DESC = "404错误页面";
	public static final String KYLINBOY_EXCEPTION_404_UI_URL = "/error/";
	public static final String KYLINBOY_EXCEPTION_404_UI_MAPPING = KYLINBOY_EXCEPTION_404_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_EXCEPTION_404_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_EXCEPTION_405_UI_ID = _KYLINBOY_USER_ID + "error_405_ui";
	public static final String KYLINBOY_EXCEPTION_405_UI = "error_405_ui";
	public static final String KYLINBOY_EXCEPTION_405_UI_NAME = "405错误";
	public static final String KYLINBOY_EXCEPTION_405_UI_DESC = "405错误页面";
	public static final String KYLINBOY_EXCEPTION_405_UI_URL = "/error/";
	public static final String KYLINBOY_EXCEPTION_405_UI_MAPPING = KYLINBOY_EXCEPTION_405_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_EXCEPTION_405_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_EXCEPTION_406_UI_ID = _KYLINBOY_USER_ID + "error_406_ui";
	public static final String KYLINBOY_EXCEPTION_406_UI = "error_406_ui";
	public static final String KYLINBOY_EXCEPTION_406_UI_NAME = "406错误";
	public static final String KYLINBOY_EXCEPTION_406_UI_DESC = "406错误页面";
	public static final String KYLINBOY_EXCEPTION_406_UI_URL = "/error/";
	public static final String KYLINBOY_EXCEPTION_406_UI_MAPPING = KYLINBOY_EXCEPTION_406_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_EXCEPTION_406_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_EXCEPTION_500_UI_ID = _KYLINBOY_USER_ID + "error_500_ui";
	public static final String KYLINBOY_EXCEPTION_500_UI = "error_500_ui";
	public static final String KYLINBOY_EXCEPTION_500_UI_NAME = "500错误";
	public static final String KYLINBOY_EXCEPTION_500_UI_DESC = "500错误页面";
	public static final String KYLINBOY_EXCEPTION_500_UI_URL = "/error/";
	public static final String KYLINBOY_EXCEPTION_500_UI_MAPPING = KYLINBOY_EXCEPTION_500_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_EXCEPTION_500_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** ExceptionController mapping end */
	
	
	
	/** 菜单管理  NavigationController begin*/
	public static final String KYLINBOY_NAVIGATION_MODULE_ID =  _KYLINBOY_USER_ID + "navigation_manager";
	public static final String KYLINBOY_NAVIGATION_MODULE = "navigation_manager";
	public static final String KYLINBOY_NAVIGATION_MODULE_NAME = "菜单模块";
	public static final String KYLINBOY_NAVIGATION_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_NAVIGATION_MODULE;
	
	
	public static final String KYLINBOY_NAVIGATION_LIST_UI_ID = _KYLINBOY_USER_ID + "navigation_list";
	public static final String KYLINBOY_NAVIGATION_LIST_UI = "navigation_list";
	public static final String KYLINBOY_NAVIGATION_LIST_UI_NAME = "菜单列表";
	public static final String KYLINBOY_NAVIGATION_LIST_UI_DESC = "后台菜单列表";
	public static final String KYLINBOY_NAVIGATION_LIST_UI_URL = "/admin/main/menu/navigation/navigationListUI/";
	public static final String KYLINBOY_NAVIGATION_LIST_UI_MAPPING = KYLINBOY_NAVIGATION_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATION_MANAGER_UI_ID = _KYLINBOY_USER_ID + "navigation_manager_ui";
	public static final String KYLINBOY_NAVIGATION_MANAGER_UI = "navigation_manager_ui";
	public static final String KYLINBOY_NAVIGATION_MANAGER_UI_NAME = "菜单管理";
	public static final String KYLINBOY_NAVIGATION_MANAGER_UI_DESC = "菜单管理页面";
	public static final String KYLINBOY_NAVIGATION_MANAGER_UI_URL = "/admin/main/menu/navigation/manager/";
	public static final String KYLINBOY_NAVIGATION_MANAGER_UI_MAPPING = KYLINBOY_NAVIGATION_MANAGER_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_MANAGER_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	
	public static final String KYLINBOY_NAVIGATION_ADD_UI_ID = _KYLINBOY_USER_ID + "navigation_add_ui";
	public static final String KYLINBOY_NAVIGATION_ADD_UI = "navigation_add_ui";
	public static final String KYLINBOY_NAVIGATION_ADD_UI_NAME = "菜单添加页面";
	public static final String KYLINBOY_NAVIGATION_ADD_UI_DESC = "后台菜单添加页面";
	public static final String KYLINBOY_NAVIGATION_ADD_UI_URL = "/admin/main/menu/navigation/navigationAddUI/";
	public static final String KYLINBOY_NAVIGATION_ADD_UI_MAPPING = KYLINBOY_NAVIGATION_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_NAVIGATION_ADD_ID = _KYLINBOY_USER_ID + "navigation_add";
	public static final String KYLINBOY_NAVIGATION_ADD = "navigation_add";
	public static final String KYLINBOY_NAVIGATION_ADD_NAME = "菜单添加";
	public static final String KYLINBOY_NAVIGATION_ADD_DESC = "执行后台菜单添加";
	public static final String KYLINBOY_NAVIGATION_ADD_URL = "/admin/menu/navigation/navigationAdd/";
	public static final String KYLINBOY_NAVIGATION_ADD_MAPPING = KYLINBOY_NAVIGATION_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATION_MODIFY_UI_ID = _KYLINBOY_USER_ID + "navigation_modify_ui";
	public static final String KYLINBOY_NAVIGATION_MODIFY_UI = "navigation_modify_ui";
	public static final String KYLINBOY_NAVIGATION_MODIFY_UI_NAME = "菜单修改页面";
	public static final String KYLINBOY_NAVIGATION_MODIFY_UI_DESC = "菜单修改页面";
	public static final String KYLINBOY_NAVIGATION_MODIFY_UI_URL = "/admin/main/menu/navigation/navigationModifyUI/";
	public static final String KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING = KYLINBOY_NAVIGATION_MODIFY_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_MODIFY_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_NAVIGATION_MODIFY_ID = _KYLINBOY_USER_ID + "navigation_modify";
	public static final String KYLINBOY_NAVIGATION_MODIFY = "navigation_modify";
	public static final String KYLINBOY_NAVIGATION_MODIFY_NAME = "菜单修改";
	public static final String KYLINBOY_NAVIGATION_MODIFY_DESC = "执行菜单修改";
	public static final String KYLINBOY_NAVIGATION_MODIFY_URL = "/admin/menu/navigation/navigationModify/";
	public static final String KYLINBOY_NAVIGATION_MODIFY_MAPPING = KYLINBOY_NAVIGATION_MODIFY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_MODIFY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY_ID = _KYLINBOY_USER_ID + "navigation_list_modify";
	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY = "navigation_list_modify";
	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY_NAME = "菜单列表修改";
	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY_DESC = "执行菜单列表修改";
	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY_URL = "/admin/menu/navigation/navigationList/modify";
	public static final String KYLINBOY_NAVIGATION_LIST_MODIFY_MAPPING = KYLINBOY_NAVIGATION_LIST_MODIFY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATION_LIST_MODIFY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI_ID = _KYLINBOY_USER_ID + "navigat_menu_front_manager_ui";
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI = "navigat_menu_front_manager_ui";
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI_NAME = "前台菜单管理";
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI_DESC = "前台菜单管理";
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI_URL = "/admin/main/menu/navigatMenu/manager/";
	public static final String KYLINBOY_NAVIGATMENU_MANAGER_UI_MAPPING = KYLINBOY_NAVIGATMENU_MANAGER_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_MANAGER_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI_ID = _KYLINBOY_USER_ID + "navigat_menu_list";
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI = "navigat_menu_list";
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI_NAME = "前台菜单列表";
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI_DESC = "前台菜单列表";
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI_URL = "/admin/main/menu/navigatMenu/navigatMenuListUI/";
	public static final String KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING = KYLINBOY_NAVIGATMENU_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI_ID = _KYLINBOY_USER_ID + "navigat_menu_modify_ui";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI = "navigat_menu_modify_ui";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI_NAME = "前台菜单修改页面";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI_DESC = "前台菜单修改页面";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI_URL = "/admin/main/menu/navigatMenu/navigatMenuModifyUI/";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING = KYLINBOY_NAVIGATMENU_MODIFY_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_MODIFY_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATMENU_MODIFY_ID = _KYLINBOY_USER_ID + "navigat_menu_modify";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY = "navigat_menu_modify";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_NAME = "前台菜单修改提交";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_DESC = "前台菜单修改提交";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_URL = "/admin/menu/navigatMenu/navigatMenuModify/";
	public static final String KYLINBOY_NAVIGATMENU_MODIFY_MAPPING = KYLINBOY_NAVIGATMENU_MODIFY_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_MODIFY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATMENU_ADD_ID = _KYLINBOY_USER_ID + "navigat_menu_add";
	public static final String KYLINBOY_NAVIGATMENU_ADD = "navigat_menu_add";
	public static final String KYLINBOY_NAVIGATMENU_ADD_NAME = "前台菜单添加提交";
	public static final String KYLINBOY_NAVIGATMENU_ADD_DESC = "前台菜单添加提交";
	public static final String KYLINBOY_NAVIGATMENU_ADD_URL = "/admin/blank/menu/navigatMenu/navigatMenuAdd/";
	public static final String KYLINBOY_NAVIGATMENU_ADD_MAPPING = KYLINBOY_NAVIGATMENU_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_NAVIGATMENU_ADD_UI_ID = _KYLINBOY_USER_ID + "navigat_menu_add_ui";
	public static final String KYLINBOY_NAVIGATMENU_ADD_UI = "navigat_menu_add_ui";
	public static final String KYLINBOY_NAVIGATMENU_ADD_UI_NAME = "前台菜单添加页面";
	public static final String KYLINBOY_NAVIGATMENU_ADD_UI_DESC = "前台菜单添加页面";
	public static final String KYLINBOY_NAVIGATMENU_ADD_UI_URL = "/admin/main/menu/navigatMenu/navigatMenuAddUI/";
	public static final String KYLINBOY_NAVIGATMENU_ADD_UI_MAPPING = KYLINBOY_NAVIGATMENU_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;


	public static final String KYLINBOY_NAVIGATMENU_REMOVE_ID = _KYLINBOY_USER_ID + "navigat_menu_remove";
	public static final String KYLINBOY_NAVIGATMENU_REMOVE = "navigat_menu_remove";
	public static final String KYLINBOY_NAVIGATMENU_REMOVE_NAME = "前台菜单删除提交";
	public static final String KYLINBOY_NAVIGATMENU_REMOVE_DESC = "前台菜单删除提交";
	public static final String KYLINBOY_NAVIGATMENU_REMOVE_URL = "/admin/blank/menu/navigatMenu/";
	public static final String KYLINBOY_NAVIGATMENU_REMOVE_MAPPING = KYLINBOY_NAVIGATMENU_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_NAVIGATMENU_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** 菜单管理  NavigationController end*/
	
	/** 与站长关系管理  OwnRelationController begin*/
	public static final String KYLINBOY_OWNRELATION_MODULE_ID =  _KYLINBOY_USER_ID + "ownRelation_manager";
	public static final String KYLINBOY_OWNRELATION_MODULE = "ownRelation_manager";
	public static final String KYLINBOY_OWNRELATION_MODULE_NAME = "站长关系管理模块";
	public static final String KYLINBOY_OWNRELATION_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_OWNRELATION_MODULE;
	
	// /listOwnRelation
	public static final String KYLINBOY_OWNRELATION_LIST_UI_ID = _KYLINBOY_USER_ID + "ownRelation_list_ui";
	public static final String KYLINBOY_OWNRELATION_LIST_UI = "ownRelation_list_ui";
	public static final String KYLINBOY_OWNRELATION_LIST_UI_NAME = "站长关系页面";
	public static final String KYLINBOY_OWNRELATION_LIST_UI_DESC = "站长关系页面";
	public static final String KYLINBOY_OWNRELATION_LIST_UI_URL = "/admin/listOwnRelation";
	public static final String KYLINBOY_OWNRELATION_LIST_UI_MAPPING = KYLINBOY_OWNRELATION_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_OWNRELATION_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN_ID = _KYLINBOY_USER_ID + "ownRelation_list_dropdown";
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN = "ownRelation_list_dropdown";
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN_NAME = "站长关系dropdown";
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN_DESC = "站长关系dropdown";
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN_URL = "/admin/dropdown/listOwnRelation/";
	public static final String KYLINBOY_OWNRELATION_LIST_DROPDOWN_MAPPING = KYLINBOY_OWNRELATION_LIST_DROPDOWN_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_OWNRELATION_LIST_DROPDOWN + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	/** 与站长关系管理  OwnRelationController end*/
	
	
	/**
	 * 站点设置模块 SiteInfoController begin
	 * /admin/main/siteInfo/siteInfoListUI
	 */
	
	public static final String KYLINBOY_SITEINFO_SETTING_MODULE_ID =  _KYLINBOY_USER_ID + "siteInfo_setting";
	public static final String KYLINBOY_SITEINFO_SETTING_MODULE = "siteInfo_setting";
	public static final String KYLINBOY_SITEINFO_SETTING_MODULE_NAME = "站点设置模块";
	public static final String KYLINBOY_SITEINFO_SETTING_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_SITEINFO_SETTING_MODULE;
	
	public static final String KYLINBOY_SITEINFO_LIST_ID = _KYLINBOY_USER_ID + "siteInfo_list";
	public static final String KYLINBOY_SITEINFO_LIST = "siteInfo_list";
	public static final String KYLINBOY_SITEINFO_LIST_NAME = "站点列表";
	public static final String KYLINBOY_SITEINFO_LIST_DESC = "站点列表";
	public static final String KYLINBOY_SITEINFO_LIST_URL = "/admin/main/siteInfo/UI";
	public static final String KYLINBOY_SITEINFO_LIST_MAPPING = KYLINBOY_SITEINFO_LIST_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_LIST + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_SITEINFO_EDIT_UI_ID = _KYLINBOY_USER_ID + "siteInfo_edit_ui";
	public static final String KYLINBOY_SITEINFO_EDIT_UI = "siteInfo_edit_ui";
	public static final String KYLINBOY_SITEINFO_EDIT_UI_NAME = "站点编辑";
	public static final String KYLINBOY_SITEINFO_EDIT_UI_DESC = "站点编辑页面";
	public static final String KYLINBOY_SITEINFO_EDIT_UI_URL = "/admin/main/siteInfo/UI";
	public static final String KYLINBOY_SITEINFO_EDIT_UI_MAPPING = KYLINBOY_SITEINFO_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI_ID = _KYLINBOY_USER_ID + "siteInfo_basic_edit_ui";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI = "siteInfo_basic_edit_ui";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI_NAME = "基本信息设置";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI_DESC = "站点基本信息设置";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI_URL = "/admin/blank/siteInfo/UI";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_UI_MAPPING = KYLINBOY_SITEINFO_BASIC_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_BASIC_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_ID = _KYLINBOY_USER_ID + "siteInfo_basic_edit";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT = "siteInfo_basic_edit";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_NAME = "基本信息设置提交";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_DESC = "站点基本信息设置提交";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_URL = "/admin/blank/siteInfo/DO";
	public static final String KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING = KYLINBOY_SITEINFO_BASIC_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_BASIC_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_ID = _KYLINBOY_USER_ID + "siteInfo_sticky_note_edit_ui";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI = "siteInfo_sticky_note_edit_ui";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_NAME = "随手贴设置";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_DESC = "随手贴设置";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_URL = "/admin/blank/siteInfo/UI";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING = KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_ID = _KYLINBOY_USER_ID + "siteInfo_sticky_note_edit";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT = "siteInfo_sticky_note_edit";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_NAME = "随手贴设置提交";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_DESC = "随手贴设置提交";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_URL = "/admin/blank/siteInfo/DO";
	public static final String KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING = KYLINBOY_SITEINFO_STICKYNOTE_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_STICKYNOTE_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	//FileInfoController 文件信息设置开始
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_ID = _KYLINBOY_USER_ID + "siteInfo_fileInfo_edit_ui";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_UI = "siteInfo_fileInfo_edit_ui";
	public static final String KYLINBOY_SITEINFOFILEINFO_EDIT_UI_NAME = "文件信息设置";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_DESC = "文件信息设置页面";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_URL = "/admin/blank/siteInfo/UI";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_MAPPING = KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_FILEINFO_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_ID = _KYLINBOY_USER_ID + "siteInfo_fileInfo_edit";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT = "siteInfo_fileInfo_edit";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_NAME = "文件信息设置提交";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_DESC = "文件信息设置提交";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_URL = "/admin/blank/siteInfo/DO";
	public static final String KYLINBOY_SITEINFO_FILEINFO_EDIT_MAPPING = KYLINBOY_SITEINFO_FILEINFO_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SITEINFO_FILEINFO_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** 站点设置模块 SiteInfoController end*/
	
	
	/** 缓存模块 MemachedModelController begin*/
	public static final String KYLINBOY_MEMACHED_MODEL_MODULE_ID =  _KYLINBOY_USER_ID + "memached_model_manager";
	public static final String KYLINBOY_MEMACHED_MODEL_MODULE = "memached_model_manager";
	public static final String KYLINBOY_MEMACHED_MODEL_MODULE_NAME = "缓存管理模块";
	public static final String KYLINBOY_MEMACHED_MODEL_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_MEMACHED_MODEL_MODULE;

	
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX_ID = _KYLINBOY_USER_ID + "memached_model_index_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX = "memached_model_index_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX_NAME = "缓存管理";
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX_DESC = "缓存管理页面";
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX_URL = "/admin/main/memcached/UI";
	public static final String KYLINBOY_MEMACHED_MODEL_INDEX_MAPPING = KYLINBOY_MEMACHED_MODEL_INDEX_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_INDEX + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MEMACHED_MODEL_LIST_ID = _KYLINBOY_USER_ID + "memached_model_list_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_LIST = "memached_model_list_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_LIST_NAME = "缓存列表";
	public static final String KYLINBOY_MEMACHED_MODEL_LIST_DESC = "缓存列表页面";
	public static final String KYLINBOY_MEMACHED_MODEL_LIST_URL = "/admin/main/memcached/UI";
	public static final String KYLINBOY_MEMACHED_MODEL_LIST_MAPPING = KYLINBOY_MEMACHED_MODEL_LIST_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_LIST + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT_ID = _KYLINBOY_USER_ID + "object_cache_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT = "object_cache_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT_NAME = "对象缓存【内存】";
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT_DESC = "对象缓存【内存】页面";
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT_URL = "/admin/blank/memcached/UI";
	public static final String KYLINBOY_MEMACHED_MODEL_OBJECT_MAPPING = KYLINBOY_MEMACHED_MODEL_OBJECT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_OBJECT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MEMACHED_MODEL_HTML_ID = _KYLINBOY_USER_ID + "html_cache_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_HTML = "html_cache_ui";
	public static final String KYLINBOY_MEMACHED_MODEL_HTML_NAME = "页面缓存【HTML】";
	public static final String KYLINBOY_MEMACHED_MODEL_HTML_DESC = "页面缓存【HTML】页面";
	public static final String KYLINBOY_MEMACHED_MODEL_HTML_URL = "/admin/main/memcached/UI";
	public static final String KYLINBOY_MEMACHED_MODEL_HTML_MAPPING = KYLINBOY_MEMACHED_MODEL_HTML_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_HTML + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP_ID = _KYLINBOY_USER_ID + "object_set_exp";
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP = "object_set_exp";
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP_NAME = "设置过期时间";
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP_DESC = "设置对象过期时间";
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP_URL = "/admin/blank/memcached/DO";
	public static final String KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING = KYLINBOY_MEMACHED_MODEL_SET_EXP_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_SET_EXP + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ID = _KYLINBOY_USER_ID + "object_refresh";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH = "object_refresh";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_NAME = "刷新缓存";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_DESC = "刷新缓存提交";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_URL = "/admin/blank/memcached/DO";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_MAPPING = KYLINBOY_MEMACHED_MODEL_REFRESH_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_REFRESH + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_ID = _KYLINBOY_USER_ID + "object_refresh_all";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL = "object_refresh_all";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_NAME = "刷新整个缓存";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_DESC = "刷新整个缓存提交";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_URL = "/admin/blank/memcached/DO";
	public static final String KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_MAPPING = KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_REFRESH_ALL + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE_ID = _KYLINBOY_USER_ID + "object_delete";
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE = "object_delete";
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE_NAME = "删除缓存ByKey";
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE_DESC = "删除缓存ByKey提交";
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE_URL = "/admin/blank/memcached/DO";
	public static final String KYLINBOY_MEMACHED_MODEL_DELETE_MAPPING = KYLINBOY_MEMACHED_MODEL_DELETE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MEMACHED_MODEL_DELETE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** 缓存模块 MemachedModelController end*/
	
	/** 文件管理模块 begin*/
	public static final String KYLINBOY_FILE_MODEL_MODULE_ID =  _KYLINBOY_USER_ID + "file_model_manager";
	public static final String KYLINBOY_FILE_MODEL_MODULE = "file_model_manager";
	public static final String KYLINBOY_FILE_MODEL_MODULE_NAME = "文件管理模块";
	public static final String KYLINBOY_FILE_MODEL_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_FILE_MODEL_MODULE;

	/** 文件类型 FileMimeController begin*/
	
	public static final String KYLINBOY_FILEMIME_LIST_UI_ID = _KYLINBOY_USER_ID + "fileMime_list_ui";
	public static final String KYLINBOY_FILEMIME_LIST_UI = "fileMime_list_ui";
	public static final String KYLINBOY_FILEMIME_LIST_UI_NAME = "文件类型列表";
	public static final String KYLINBOY_FILEMIME_LIST_UI_DESC = "文件类型列表";
	public static final String KYLINBOY_FILEMIME_LIST_UI_URL = "/admin/main/file/fileMime/UI";
	public static final String KYLINBOY_FILEMIME_LIST_UI_MAPPING = KYLINBOY_FILEMIME_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEMIME_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEMIME_INDEX_UI_ID = _KYLINBOY_USER_ID + "fileMime_index_ui";
	public static final String KYLINBOY_FILEMIME_INDEX_UI = "fileMime_index_ui";
	public static final String KYLINBOY_FILEMIME_INDEX_UI_NAME = "文件类型首页";
	public static final String KYLINBOY_FILEMIME_INDEX_UI_DESC = "文件类型首页";
	public static final String KYLINBOY_FILEMIME_INDEX_UI_URL = "/admin/main/file/fileMime/UI";
	public static final String KYLINBOY_FILEMIME_INDEX_UI_MAPPING = KYLINBOY_FILEMIME_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEMIME_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_FILEMIME_ADD_UI_ID = _KYLINBOY_USER_ID + "fileMime_add_ui";
	public static final String KYLINBOY_FILEMIME_ADD_UI = "fileMime_add_ui";
	public static final String KYLINBOY_FILEMIME_ADD_UI_NAME = "文件类型添加";
	public static final String KYLINBOY_FILEMIME_ADD_UI_DESC = "文件类型添加页面";
	public static final String KYLINBOY_FILEMIME_ADD_UI_URL = "/admin/blank/file/fileMime/UI";
	public static final String KYLINBOY_FILEMIME_ADD_UI_MAPPING = KYLINBOY_FILEMIME_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEMIME_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_FILEMIME_ADD_ID = _KYLINBOY_USER_ID + "fileMime_add";
	public static final String KYLINBOY_FILEMIME_ADD = "fileMime_add";
	public static final String KYLINBOY_FILEMIME_ADD_NAME = "文件类型添加提交";
	public static final String KYLINBOY_FILEMIME_ADD_DESC = "文件类型添加提交";
	public static final String KYLINBOY_FILEMIME_ADD_URL = "/admin/blank/file/fileMime/DO";
	public static final String KYLINBOY_FILEMIME_ADD_MAPPING = KYLINBOY_FILEMIME_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEMIME_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEMIME_REMOVE_ID = _KYLINBOY_USER_ID + "fileMime_remove";
	public static final String KYLINBOY_FILEMIME_REMOVE = "fileMime_remove";
	public static final String KYLINBOY_FILEMIME_REMOVE_NAME = "文件类型删除提交";
	public static final String KYLINBOY_FILEMIME_REMOVE_DESC = "文件类型删除提交";
	public static final String KYLINBOY_FILEMIME_REMOVE_URL = "/admin/blank/file/fileMime/DO";
	public static final String KYLINBOY_FILEMIME_REMOVE_MAPPING = KYLINBOY_FILEMIME_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEMIME_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** 文件类型 FileMimeController end*/
	
	/** 文件系统分类 FileSystemGalleryController begin*/
	
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_ID = _KYLINBOY_USER_ID + "fileSystemGallery_index_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI = "fileSystemGallery_index_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_NAME = "文件系统分类";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_DESC = "文件系统分类页面";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_URL = "/admin/main/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_ID = _KYLINBOY_USER_ID + "fileSystemGallery_list_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI = "fileSystemGallery_list_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_NAME = "文件系统分类列表";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_DESC = "文件系统分类列表";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_URL = "/admin/blank/file/fileSystemGallery/UI";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_ID = _KYLINBOY_USER_ID + "fileSystemGallery_add_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI = "fileSystemGallery_add_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_NAME = "文件系统分类添加";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_DESC = "文件系统分类添加页面";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_URL = "/admin/blank/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_ID = _KYLINBOY_USER_ID + "fileSystemGallery_add";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD = "fileSystemGallery_add";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_NAME = "文件系统分类添加";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_DESC = "文件系统分类添加提交";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_URL = "/admin/blank/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_ADD_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_ID = _KYLINBOY_USER_ID + "fileSystemGallery_remove";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE = "fileSystemGallery_remove";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_NAME = "文件系统分类删除";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_DESC = "文件系统分类删除提交";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_URL = "/admin/blank/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** 文件系统分类 FileSystemGalleryController end*/
	
	/** 文件系统分类 FileSystemGalleryTypeController end*/
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_ID = _KYLINBOY_USER_ID + "fileSystemGalleryType_define_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI = "fileSystemGalleryType_define_ui";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_NAME = "系统分类类型定义";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_DESC = "文件系统分类定义页面";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_URL = "/admin/blank/file/fileSystemGalleryType/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_ID = _KYLINBOY_USER_ID + "fileSystemGalleryType_add";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD = "fileSystemGalleryType_add";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_NAME = "系统分类类型添加";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_DESC = "系统分类类型添加提交";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_URL = "/admin/blank/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_ID = _KYLINBOY_USER_ID + "fileSystemGalleryType_remove";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE = "fileSystemGalleryType_remove";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_NAME = "系统分类类型删除";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_DESC = "系统分类类型删除提交";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_URL = "/admin/blank/file/fileSystemGallery/";
	public static final String KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_MAPPING = KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEINFO_UPLOAD_ID = _KYLINBOY_USER_ID + "fileInfo_upload";
	public static final String KYLINBOY_FFILEINFO_UPLOAD = "fileInfo_upload";
	public static final String KYLINBOY_FILEINFO_UPLOAD_NAME = "文件上传提交";
	public static final String KYLINBOY_FILEINFO_UPLOAD_DESC = "文件上传提交";
	public static final String KYLINBOY_FILEINFO_UPLOAD_URL = "/admin/blank/file/DO/basicAdd";
	public static final String KYLINBOY_FILEINFO_UPLOAD_MAPPING = KYLINBOY_FILEINFO_UPLOAD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FFILEINFO_UPLOAD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEINFO_UPLOAD_UI_ID = _KYLINBOY_USER_ID + "fileInfo_upload_ui";
	public static final String KYLINBOY_FFILEINFO_UPLOAD_UI = "fileInfo_upload_ui";
	public static final String KYLINBOY_FILEINFO_UPLOAD_UI_NAME = "文件上传页面";
	public static final String KYLINBOY_FILEINFO_UPLOAD_UI_DESC = "文件上传页面";
	public static final String KYLINBOY_FILEINFO_UPLOAD_UI_URL = "/admin/blank/file/UI/basicAdd";
	public static final String KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING = KYLINBOY_FILEINFO_UPLOAD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FFILEINFO_UPLOAD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEINFO_LIST_UI_ID = _KYLINBOY_USER_ID + "fileInfo_list_ui";
	public static final String KYLINBOY_FILEINFO_LIST_UI = "fileInfo_list_ui";
	public static final String KYLINBOY_FILEINFO_LIST_UI_NAME = "文件列表页面";
	public static final String KYLINBOY_FILEINFO_LIST_UI_DESC = "文件列表页面";
	public static final String KYLINBOY_FILEINFO_LIST_UI_URL = "/admin/blank/file/UI/";
	public static final String KYLINBOY_FILEINFO_LIST_UI_MAPPING = KYLINBOY_FILEINFO_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEINFO_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_FILEINFO_INDEX_UI_ID = _KYLINBOY_USER_ID + "fileInfo_index_ui";
	public static final String KYLINBOY_FILEINFO_INDEX_UI = "fileInfo_index_ui";
	public static final String KYLINBOY_FILEINFO_INDEX_UI_NAME = "文件信息管理首页";
	public static final String KYLINBOY_FILEINFO_INDEX_UI_DESC = "文件信息管理首页";
	public static final String KYLINBOY_FILEINFO_INDEX_UI_URL = "/admin/main/file/UI/";
	public static final String KYLINBOY_FILEINFO_INDEX_UI_MAPPING = KYLINBOY_FILEINFO_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEINFO_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	public static final String KYLINBOY_FILEINFO_DELETE_ID = _KYLINBOY_USER_ID + "fileInfo_delete";
	public static final String KYLINBOY_FILEINFO_DELETE = "fileInfo_delete";
	public static final String KYLINBOY_FILEINFO_DELETE_NAME = "文件删除提交";
	public static final String KYLINBOY_FILEINFO_DELETE_DESC = "文件删除提交";
	public static final String KYLINBOY_FILEINFO_DELETE_URL = "/admin/blank/fileInfo/DO";
	public static final String KYLINBOY_FILEINFO_DELETE_MAPPING = KYLINBOY_FILEINFO_DELETE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_FILEINFO_DELETE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	/** 文件系统分类 FileSystemGalleryTypeController end*/
	
	
	/** 文件管理模块  end*/
	
	/** 邮件配置模块  begin*/
	
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI_ID = _KYLINBOY_USER_ID + "mailConfig_index_ui";
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI = "mailConfig_index_ui";
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI_NAME = "邮件服务器配置";
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI_DESC = "邮件服务器配置页面";
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI_URL = "/admin/main/mailConfig/UI";
	public static final String KYLINBOY_MAILCONFIG_INDEX_UI_MAPPING = KYLINBOY_MAILCONFIG_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MAILCONFIG_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI_ID = _KYLINBOY_USER_ID + "mailConfig_edit_ui";
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI = "mailConfig_edit_ui";
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI_NAME = "邮件服务器配置";
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI_DESC = "邮件服务器配置页面";
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI_URL = "/admin/main/mailConfig/UI";
	public static final String KYLINBOY_MAILCONFIG_EDIT_UI_MAPPING = KYLINBOY_MAILCONFIG_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MAILCONFIG_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MAILCONFIG_EDIT_ID = _KYLINBOY_USER_ID + "mailConfig_edit";
	public static final String KYLINBOY_MAILCONFIG_EDIT = "mailConfig_edit";
	public static final String KYLINBOY_MAILCONFIG_EDIT_NAME = "邮件服务器配置提交";
	public static final String KYLINBOY_MAILCONFIG_EDIT_DESC = "邮件服务器配置提交";
	public static final String KYLINBOY_MAILCONFIG_EDIT_URL = "/admin/main/mailConfig/UI";
	public static final String KYLINBOY_MAILCONFIG_EDIT_MAPPING = KYLINBOY_MAILCONFIG_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MAILCONFIG_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** 邮件配置模块  end*/
	
	/** 随手帖管理模块 SpecialAreaController  begin*/
	
	public static final String KYLINBOY_STICKY_NOTE_MODULE_ID =  _KYLINBOY_USER_ID + "sticky_note_manager";
	public static final String KYLINBOY_STICKY_NOTE_MODULE = "sticky_note_manager";
	public static final String KYLINBOY_STICKY_NOTE_MODULE_NAME = "随手贴管理模块";
	public static final String KYLINBOY_STICKY_NOTE_MODULE_CODE =  _KYLINBOY_USER_ID + KYLINBOY_STICKY_NOTE_MODULE;

	/** 特殊区管理 SpecialAreaController  begin*/
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI_ID = _KYLINBOY_USER_ID + "special_area_index_ui";
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI = "special_area_index_ui";
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI_NAME = "特殊区管理";
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI_DESC = "特殊区管理页面";
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_SPECIAL_AREA_INDEX_UI_MAPPING = KYLINBOY_SPECIAL_AREA_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SPECIAL_AREA_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI_ID = _KYLINBOY_USER_ID + "special_area_edit_ui";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI = "special_area_edit_ui";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI_NAME = "特殊区编辑";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI_DESC = "特殊区编辑页面";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_UI_MAPPING = KYLINBOY_SPECIAL_AREA_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SPECIAL_AREA_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_ID = _KYLINBOY_USER_ID + "special_area_edit";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT = "special_area_edit";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_NAME = "特殊区编辑提交";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_DESC = "特殊区编辑提交";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_SPECIAL_AREA_EDIT_MAPPING = KYLINBOY_SPECIAL_AREA_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SPECIAL_AREA_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_SPECIAL_AREA_REMOVE_ID = _KYLINBOY_USER_ID + "special_area_remove";
	public static final String KYLINBOY_SPECIAL_AREA_REMOVE = "special_area_remove";
	public static final String KYLINBOY_SPECIAL_AREA_REMOVE_NAME = "特殊区分类删除";
	public static final String KYLINBOY_SPECIAL_AREA_REMOVE_DESC = "特殊区分类删除提交";
	public static final String KYLINBOY_SPECIAL_AREA_REMOVE_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_SPECIAL_AREA_REMOVE_MAPPING = KYLINBOY_SPECIAL_AREA_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_SPECIAL_AREA_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** 特殊区管理 SpecialAreaController  end*/
	
	/** 随手帖类别管理 StickyNoteCategoryController  begin*/
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_ID = _KYLINBOY_USER_ID + "stickyNote_category_index_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI = "stickyNote_category_index_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_NAME = "随手帖类别管理";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_DESC = "随手帖类别管理页面";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_ID = _KYLINBOY_USER_ID + "stickyNote_category_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI = "stickyNote_category_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_NAME = "随手帖类别管理";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_DESC = "随手帖类别管理页面";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_ID = _KYLINBOY_USER_ID + "stickyNote_category_edit_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI = "stickyNote_category_edit_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_NAME = "类别编辑";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_DESC = "类别编辑页面";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_ID = _KYLINBOY_USER_ID + "special_area_edit";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT = "stickyNote_category_edit";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_NAME = "类别编辑提交";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_DESC = "类别编辑提交";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_ID = _KYLINBOY_USER_ID + "special_area_remove";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE = "stickyNote_category_remove";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_NAME = "类别分类删除";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_DESC = "类别分类删除提交";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	/** 随手帖类别管理 StickyNoteCategoryController  end*/
	
	
	/** 类别与特殊区关系管理 SpecialAreaController  begin*/
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_ID = _KYLINBOY_USER_ID + "category_special_index_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI = "category_special_index_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_NAME = "类别特殊区关系管理";
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_DESC = "类别特殊区关系管理页面";
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_MAPPING = KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI_ID = _KYLINBOY_USER_ID + "category_special_list_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI = "category_special_list_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI_NAME = "类别特殊区关系管理";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI_DESC = "类别特殊区关系管理页面";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_UI_MAPPING = KYLINBOY_CATEGORY_SPECIAL_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_ID = _KYLINBOY_USER_ID + "category_special_asign_list_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI = "category_special_asign_list_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_NAME = "类别特殊区关系列表[asign页面]";
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_DESC = "类别特殊区关系列表[asign页面]";
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING = KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_ID = _KYLINBOY_USER_ID + "category_special_edit_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI = "category_special_edit_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_NAME = "类别特殊区关系编辑";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_DESC = "类别特殊区关系编辑页面";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING = KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_ID = _KYLINBOY_USER_ID + "category_special_edit";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT = "category_special_edit";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_NAME = "类别特殊区关系编辑提交";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_DESC = "类别特殊区关系编辑提交";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING = KYLINBOY_CATEGORY_SPECIAL_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_ID = _KYLINBOY_USER_ID + "category_special_remove";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE = "category_special_remove";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_NAME = "类别特殊区关系删除";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_DESC = "类别特殊区关系删除提交";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_MAPPING = KYLINBOY_CATEGORY_SPECIAL_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_ID = _KYLINBOY_USER_ID + "category_special_remove_from_asign_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI = "category_special_remove_from_asign_ui";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_NAME = "类别特殊区关系删除[asign页面]";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_DESC = "类别特殊区关系删除提交[asign页面]";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_MAPPING = KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_ID = _KYLINBOY_USER_ID + "category_asign_special_ui";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI = "category_asign_special_ui";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_NAME = "类别绑定特殊区";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_DESC = "类别绑定特殊区页面";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING = KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_ID = _KYLINBOY_USER_ID + "category_asign_special";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL = "category_asign_special";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_NAME = "类别绑定特殊区";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_DESC = "类别绑定特殊区提交";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING = KYLINBOY_CATEGORY_ASIGN_SPECIAL_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_ASIGN_SPECIAL + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_ID = _KYLINBOY_USER_ID + "category_remove_from_special";
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL = "category_remove_from_special";
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_NAME = "类别移除特殊区";
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_DESC = "类别移除特殊区提交";
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING = KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_ID = _KYLINBOY_USER_ID + "category_special_list_save";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE = "category_special_list_save";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_NAME = "保存类别特殊区列表";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_DESC = "保存类别特殊区列表";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING = KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;


	/** 类别与特殊区关系管理  SpecialAreaController  end*/
	
	
	
	/** 帖子管理 StickyNoteCategorySpecialController  begin*/
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_ID = _KYLINBOY_USER_ID + "sticky_note_category_special_index_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI = "sticky_note_category_special_index_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_NAME = "随手贴帖子首页";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_DESC = "随手贴帖子首页页面";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_ID = _KYLINBOY_USER_ID + "sticky_note_category_special_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI = "sticky_note_category_special_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_NAME = "随手贴帖子列表";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_DESC = "随手贴帖子列表页面";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_MAPPING = KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/** 帖子添加与编辑 StickyNoteController begin*/
	
	/** 帖子管理 StickyNoteController  end*/
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI_ID = _KYLINBOY_USER_ID + "sticky_note_edit_ui";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI = "sticky_note_edit_ui";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI_NAME = "随手贴帖子编辑";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI_DESC = "随手贴帖子编辑页面";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_UI_MAPPING = KYLINBOY_STICKY_NOTE_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	public static final String KYLINBOY_STICKY_NOTE_EDIT_ID = _KYLINBOY_USER_ID + "sticky_note_edit";
	public static final String KYLINBOY_STICKY_NOTE_EDIT = "sticky_note_edit";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_NAME = "随手贴帖子编辑";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_DESC = "随手贴帖子编辑提交";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_EDIT_MAPPING = KYLINBOY_STICKY_NOTE_EDIT_URL + _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_ID = _KYLINBOY_USER_ID + "sticky_note_checked_category_ui";
	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY = "sticky_note_checked_category_ui";
	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_NAME = "帖子所属类别";
	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_DESC = "帖子所属类别页面";
	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_MAPPING = KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_URL + _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_STICKY_NOTE_LIST_UI_ID = _KYLINBOY_USER_ID + "sticky_note_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_LIST_UI = "sticky_note_list_ui";
	public static final String KYLINBOY_STICKY_NOTE_LIST_UI_NAME = "帖子列表";
	public static final String KYLINBOY_STICKY_NOTE_LIST_UI_DESC = "帖子列表";
	public static final String KYLINBOY_STICKY_NOTE_LIST_UI_URL = "/admin/main/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_LIST_UI_MAPPING = KYLINBOY_STICKY_NOTE_LIST_UI_URL + _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_ID = _KYLINBOY_USER_ID + "sticky_note_list_ajax_ui";
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI = "sticky_note_list_ajax_ui";
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_NAME = "帖子列表[ajax]";
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_DESC = "帖子列表[ajax]";
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_URL = "/admin/blank/UI/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_MAPPING = KYLINBOY_STICKY_NOTE_LIST_AJAX_UI_URL + _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_LIST_AJAX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	public static final String KYLINBOY_STICKY_NOTE_DELERE_ID = _KYLINBOY_USER_ID + "sticky_note_delete";
	public static final String KYLINBOY_STICKY_NOTE_DELERE = "sticky_note_delete";
	public static final String KYLINBOY_STICKY_NOTE_DELERE_NAME = "帖子删除";
	public static final String KYLINBOY_STICKY_NOTE_DELERE_DESC = "帖子删除提交";
	public static final String KYLINBOY_STICKY_NOTE_DELERE_URL = "/admin/blank/DO/stickyNote/";
	public static final String KYLINBOY_STICKY_NOTE_DELERE_MAPPING = KYLINBOY_STICKY_NOTE_DELERE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_STICKY_NOTE_DELERE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** 帖子添加与编辑 StickyNoteController end*/
	
	
	/** 随手帖管理模块   end*/
	
	@Override
	public Long getDeveloperUserId() {
		return _KYLINBOY_USER_ID_LONG;
	}
}
