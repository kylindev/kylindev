package org.jit8.framework.jit84j.web.utils;

import java.io.File;
import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.util.ResourceUtils;

public class MailMessageUtils {

	public MailMessageUtils() {
	}

	public static MailMessage getEmailHtmlStoredRealPath(User user, String rootStoredPath, HttpServletRequest request) {
		MailMessage mailMessage = new MailMessage();
		// 将file存储路径参数化
		// 区别用户目录
		if (user == null) {
			user = Jit8ShareInfoUtils.getUser();
		}
		Long userId = 0l;
		if (null != user) {
			userId = user.getId();
			if (userId == null) {
				userId = 0l;
			}
		}
		rootStoredPath = rootStoredPath + "/" + userId.toString();
		mailMessage.setFileAccessPath(rootStoredPath);
		String realPath = request.getSession().getServletContext().getRealPath(rootStoredPath);
		if (StringUtils.isEmpty(realPath)) {
			realPath = request.getSession().getServletContext().getRealPath("/") + rootStoredPath;
			File file = new File(realPath);
			if (!file.exists()) {
				file.mkdirs();
			}
		}
		mailMessage.setFileRealPath(realPath);
		return mailMessage;
	}
	
	
	public static MailMessage getMailMessage(String path, String templateName, String fileName){
		MailMessage mailMessage = new MailMessage();
		if(StringUtils.isEmpty(path)){
			path = "/template/email";
		}
		
		try {
			String templatePath = path+"/"+templateName;
			File file = ResourceUtils.getFile(templatePath);
			
			System.out.println(file.getAbsolutePath());
			if(file.exists()){
				String realPath = file.getParent();
				mailMessage.setFileRealPath(realPath);
				mailMessage.setFileName(fileName);
				mailMessage.setTemplatePath(templatePath);
				//mailMessage.setfi
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		return mailMessage;
	}

}
