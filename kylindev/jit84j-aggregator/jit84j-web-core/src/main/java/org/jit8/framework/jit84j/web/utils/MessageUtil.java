package org.jit8.framework.jit84j.web.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.support.RequestContextUtils;

/**
 * 国际化工具类
 * @author kylinboy
 *
 */
public class MessageUtil {

	public static String getLocaleMessage(HttpServletRequest request, String code, Object[] args) {
		WebApplicationContext ac = RequestContextUtils.getWebApplicationContext(request);
		return ac.getMessage(code, args, RequestContextUtils.getLocale(request));
	}

	// args可以为null,如果你request也不提供的话可以通过threadLocale获得request
	/**
	 * @return {@link HttpServletRequest}
	 */
	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	}

	public static String getLocaleMessage(String code, Object[] args) {
		return getLocaleMessage(getRequest(), code, args);
	}
}
