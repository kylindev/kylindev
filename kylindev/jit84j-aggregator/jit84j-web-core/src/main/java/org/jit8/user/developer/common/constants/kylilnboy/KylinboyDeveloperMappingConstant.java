package org.jit8.user.developer.common.constants.kylilnboy;

import java.util.List;

import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public class KylinboyDeveloperMappingConstant extends KylinboyDeveloperConstant{

	private static final long serialVersionUID = 8267163625961520713L;
	
	/**
	 * 获取系统预定义的URL列表
	 * @return
	 */
	@Override
	public List<UrlStaticMapping> getUrlStaticMappingModuleList(){
		
		UrlStaticMapping userModule = addModule(KYLINBOY_USER_MODULE_ID,KYLINBOY_USER_MODULE_NAME,KYLINBOY_USER_MODULE_CODE);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING
				);
		
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING
				);
		
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_ID,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_URL,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING
				);	
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_ID,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_URL,
				KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_MAPPING
				);
		//为用户分配权限页面
				addUrlStaticMapping(
						userModule,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING
						);
				//为用户分配权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING
						);	
				
				//为用户移除权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_MAPPING
						);	
				//为用户移除所有权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING
						);	


				
				//选择用户页面
				addUrlStaticMapping(
						userModule,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING
						);
				
				
				
		UrlStaticMapping adminCommonModule = addModule(KYLINBOY_ADMIN_COMMON_MODULE_ID,KYLINBOY_ADMIN_COMMON_MODULE_NAME,KYLINBOY_ADMIN_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX_ID,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX_URL,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING
				);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING
				);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_ID,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_URL,
				KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_MAPPING
				);

		
		UrlStaticMapping commonModule = addModule(KYLINBOY_COMMON_MODULE_ID,KYLINBOY_COMMON_MODULE_NAME,KYLINBOY_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				commonModule,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE_ID,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE_URL,
				KylinboyDeveloperConstant.KYLINBOY_VALIDATE_CODE_MAPPING
				);
		addUrlStaticMapping(
				commonModule,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_ID,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_URL,
				KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING
				);
		
		
		
		UrlStaticMapping frontCommonModule = addModule(KYLINBOY_FRONT_COMMON_MODULE_ID,KYLINBOY_FRONT_COMMON_MODULE_NAME,KYLINBOY_FRONT_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_ID,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_NAME,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_DESC,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_URL,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING
				);
		
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT_ID,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT_NAME,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT_DESC,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT_URL,
				KylinboyDeveloperConstant.KYLINBOY_LOGOUT_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_ID,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING
				);
		
		
		UrlStaticMapping developerModule = addModule(KYLINBOY_DEVELOPER_MODULE_ID,KYLINBOY_DEVELOPER_MODULE_NAME,KYLINBOY_DEVELOPER_MODULE_CODE);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_ID,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_URL,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_ID,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_URL,
				KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING
				);
		/**列表Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING
				);
		
		/**列表Constant model 仅有查看功能*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_MAPPING
				);
		
		/**列表URL定义 */
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_ID,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_URL,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_MAPPING
				);

		/**添加Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_MAPPING
				);
		
		/**更新Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_MAPPING
				);

		/**删除Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_MAPPING
				);


		/**添加Constant model UI*/
		addUrlStaticMapping(
				developerModule,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_ID,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_MAPPING
				);
		
		
		

		
		UrlStaticMapping permissionModule = addModule(KYLINBOY_PERMISSION_MODULE_ID,KYLINBOY_PERMISSION_MODULE_NAME,KYLINBOY_PERMISSION_MODULE_CODE);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_MAPPING
				);
		
		//权限修改页面
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_MAPPING
				);
		
		
		
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_MAPPING
				);
		//权限类别删除
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING
				);
		//为权限类别分配权限
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_ID,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_NAME,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_DESC,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_URL,
				KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING
				);
		
		//从权限类别移除权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_ID,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_NAME,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_DESC,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_URL,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING
						);
				//从所有权限类别移除该权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_ID,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_NAME,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_DESC,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_URL,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_MAPPING
						);
				
				//启用该权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING_ID,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_URL,
						KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING
						);
				
				
		
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_ID,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_NAME,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_DESC,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_URL,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING
				);
		//URL分类
		addUrlStaticMapping(
				permissionModule,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_ID,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_NAME,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_DESC,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_URL,
				KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_MAPPING
				);
		//按类保存URL
		addUrlStaticMapping(
						permissionModule,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_ID,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_NAME,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_DESC,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_URL,
						KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING
						);
		//URL映射列表
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_MAPPING
								);
				
				//URL映射分类
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_MAPPING
								);
				//URL映射分类[For Navigation]
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_MAPPING
								);
				//URL映射列表[For Navigation]
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING
								);

				
				//URL关联权限
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING
								);
				//所有URL关联权限
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_ID,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_URL,
								KylinboyDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_MAPPING
								);
				//URL初始化
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_MAPPING
								);

			
				
				//URL解除权限关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING
								);
				
				
				//URL导航菜单关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING
								);
				//URL解除导航菜单关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_ID,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_NAME,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_DESC,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_URL,
								KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING
								);
				


				
		UrlStaticMapping roleModule = addModule(KYLINBOY_ROLE_MODULE_ID,KYLINBOY_ROLE_MODULE_NAME,KYLINBOY_ROLE_MODULE_CODE);
		addUrlStaticMapping(
				roleModule,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				roleModule,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_ID,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_URL,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_MAPPING
				);
		addUrlStaticMapping(
				roleModule,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ADD_UI_MAPPING
				);
		//为角色添分配权限页面
		addUrlStaticMapping(
				roleModule,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_MAPPING
				);
		//为角色添分配权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_MAPPING
						);
				//角色移除权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_MAPPING
						);
				//角色移除所有权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_ID,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_NAME,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_DESC,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_URL,
						KylinboyDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_MAPPING
						);
				//选择角色ajax请求
				addUrlStaticMapping(
						roleModule,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING
						);

		
		UrlStaticMapping errorModule = addModule(KYLINBOY_ERROR_MODULE_ID,KYLINBOY_ERROR_MODULE_NAME,KYLINBOY_ERROR_MODULE_CODE);
		addUrlStaticMapping(
				errorModule,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_MAPPING
				);
		
		UrlStaticMapping navigationModule = addModule(KYLINBOY_NAVIGATION_MODULE_ID,KYLINBOY_NAVIGATION_MODULE_NAME,KYLINBOY_NAVIGATION_MODULE_CODE);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_MAPPING
				);
		//前台导航菜单管理
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_MAPPING
				);
		//前台导航菜单列表
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING
				);
		//前台菜单修改页面
		addUrlStaticMapping(
				navigationModule,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_ID,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_NAME,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_DESC,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_URL,
				KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING
				);
		//前台菜单修改提交
				addUrlStaticMapping(
						navigationModule,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_ID,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_NAME,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_DESC,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_URL,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_MAPPING
						);
				//前台菜单添加页面
				addUrlStaticMapping(
						navigationModule,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_MAPPING
						);
				
				//前台菜单添加提交
				addUrlStaticMapping(
						navigationModule,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_ID,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_NAME,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_DESC,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_URL,
						KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_MAPPING
						);
				
				/** 站长关系管理模块 begin*/
				UrlStaticMapping ownRelationModule = addModule(KYLINBOY_OWNRELATION_MODULE_ID,KYLINBOY_OWNRELATION_MODULE_NAME,KYLINBOY_OWNRELATION_MODULE_CODE);
				//站长关系管理页面
				addUrlStaticMapping(
						ownRelationModule,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_MAPPING
						);
				/** 站长关系管理模块 end*/
				
				
				/** 站点设置模块 begin*/
				UrlStaticMapping siteInfoSettingModule = addModule(KYLINBOY_SITEINFO_SETTING_MODULE_ID,KYLINBOY_SITEINFO_SETTING_MODULE_NAME,KYLINBOY_SITEINFO_SETTING_MODULE_CODE);
				//站点列表页面
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_MAPPING
						);
				
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_MAPPING
						);
				/**基本信息设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_MAPPING
						);
				/**随手贴设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING
						);
				
				/**基本信息设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING
						);
				/**随手贴设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING
						);

				/**文件上传设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFOFILEINFO_EDIT_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_MAPPING
						);
				/**文件上传设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_ID,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_NAME,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_DESC,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_URL,
						KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_MAPPING
						);


				/** 站点设置模块 end*/
				
				
				/** 缓存模块 MemachedModelController begin*/
				UrlStaticMapping memcacheModelModule = addModule(KYLINBOY_MEMACHED_MODEL_MODULE_ID,KYLINBOY_MEMACHED_MODEL_MODULE_NAME,KYLINBOY_MEMACHED_MODEL_MODULE_CODE);
				//缓存管理页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_MAPPING
						);
				//缓存列表页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_MAPPING
						);

				//对象缓存【内存】页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_MAPPING
						);
				//页面缓存【html】页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_MAPPING
						);

				//设置过期时间
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING
						);

				//刷新缓存
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_MAPPING
						);
				
				//刷新整个缓存
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_MAPPING
						);

				//删除缓存ByKey
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_ID,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_URL,
						KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_MAPPING
						);


				/** 缓存模块 MemachedModelController end*/
				
				
				/** 文件管理模块 begin*/
				
				UrlStaticMapping fileModule = addModule(KYLINBOY_FILE_MODEL_MODULE_ID,KYLINBOY_FILE_MODEL_MODULE_NAME,KYLINBOY_FILE_MODEL_MODULE_CODE);
				/** 文件类型 FileMimeController begin*/
				//文件类型列表
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_MAPPING
						);
				//文件类型首页
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_MAPPING
						);
				//文件类型添加页面
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_MAPPING
						);
				//文件类型添加提交
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_MAPPING
						);
				//文件类型删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_MAPPING
						);



				/** 文件类型 FileMimeController end*/
				
				/** 文件系统分类 FileSystemGalleryController begin*/
				//文件系统分类首页
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING
						);

				//文件类型列表
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING
						);

				//文件类型添加
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING
						);
				
				//文件类型添加提交
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_MAPPING
						);
				//文件类型删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING
						);


				/** 文件系统分类 FileSystemGalleryController end*/
				/** 文件系统分类类型定义 FileSystemGalleryTypeController begin*/
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_MAPPING
						);
				//系统分类类型添加
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_MAPPING
						);
				//系统分类类型删除
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_MAPPING
						);

				/** 文件系统分类类型定义 FileSystemGalleryTypeController end*/
				/** 文件信息处理 FileInfoController begin*/
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_ID,
						KylinboyDeveloperConstant.KYLINBOY_FFILEINFO_UPLOAD,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_MAPPING
						);
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FFILEINFO_UPLOAD_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING
						);
				//文件信息列表
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_MAPPING
						);
				//文件信息管理首页
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_MAPPING
						);
				//文件删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_ID,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_NAME,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_DESC,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_URL,
						KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING
						);
				
				/** 文件信息处理 FileInfoController end*/
				
				/** 文件管理模块  end*/


		return urlStaticMappingModuleList;
		
	}
	
	
}