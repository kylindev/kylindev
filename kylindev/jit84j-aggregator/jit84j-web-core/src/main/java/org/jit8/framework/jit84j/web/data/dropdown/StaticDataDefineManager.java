package org.jit8.framework.jit84j.web.data.dropdown;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import net.rubyeye.xmemcached.MemcachedClient;

import org.jit8.framework.jit84j.core.cache.memached.Memached;
import org.jit8.framework.jit84j.core.cache.memached.MemachedContext;
import org.jit8.framework.jit84j.core.cache.memached.MemachedDefiner;
import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;
import org.jit8.framework.jit84j.core.mail.domain.MailConfigType;
import org.jit8.framework.jit84j.core.web.util.MemcachedUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StaticDataDefineManager implements Serializable{
	
	private static final long serialVersionUID = -484943254842647483L;

	private static final Logger LOGGER  = LoggerFactory.getLogger(StaticDataDefineManager.class);

	//memached key定义规则 ： developerId_key
	
	@Resource
	protected MemachedContext memachedContext;
	
	private static MemcachedClient memcachedClient;
	
	
	
	private static Set<String> directPermissionSet;
	
	private static Map<String,String> navigationTypeMap = new HashMap<String,String>();
	
	@PostConstruct
	protected void init(){
		//initDirectPermission();
		this.memcachedClient = memachedContext.getMemcachedClient();
		StaticDataDefineConstant.setMemachedModelByKey(StaticDataDefineUtil.scan(StaticDataDefineManager.class));
	}
	
	public static Object getData(String key,boolean refresh){
		LOGGER.debug("getData begin :" + key);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		Object object = MemachedDefiner.get(memachedModel,refresh);
		LOGGER.debug("getData end :" + key);
		return object;
	}
	
	public static Object getData(String key){
		return getData(key,false);
	}
	
	public boolean refresh(String key,boolean refresh){
		LOGGER.debug("refresh begin :" + key);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		Object object = MemachedDefiner.get(memachedModel,refresh);
		boolean success=false;
		if(null != object){
			success=true;
		}
		LOGGER.debug("refresh end :" + key);
		return success;
	}
	
	public boolean touch(String key,int exp){
		Object value = MemcachedUtil.get(key, memcachedClient);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		memachedModel.setExpiresTime(exp);
		StaticDataDefineConstant.setMemachedModelByKey(key, memachedModel);
		if(null == value){
			getData(key,true);
		}
		return MemachedDefiner.touch(key, exp);
	}
	public boolean delete(String key){
		return MemachedDefiner.delete(key);
	}
	
	public static void main(String[] args) {
		Map<String ,Long> map = new HashMap<String,Long>();
		map.put("aaa", 22l);
		map.put("aaa", 99l);
		System.out.println(map.get("aaa"));
	}



	/**
	 * @return the memachedContext
	 */
	public MemachedContext getMemachedContext() {
		return memachedContext;
	}



	/**
	 * @return the memcachedClient
	 */
	public static MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}
	
}
