package org.jit8.framework.jit84j.exception;


public class ApplicationException extends Exception {

	private static final long serialVersionUID = -4393859493885154343L;

	//message code from resource message property files
	private String code;
	//message default value, if code if null
	private String message;
	//message params for resource message property files
	private Object[] params;
	//exception trace
	private Throwable e;
	
	private String paramsToString;
	
	public ApplicationException(String code,String message,Object[] params,Throwable e){
		super(message,e);
		this.code=code;
		this.message=message;
		this.params=params;
		this.e=e;
	}
	
	public ApplicationException(String code,String message,Object[] params){
		super(message);
		this.code=code;
		this.message=message;
		this.params=params;
	}
	
	public ApplicationException(String code,String message,Throwable e){
		super(message,e);
		this.code=code;
		this.message=message;
		this.e=e;
	}
	
	public ApplicationException(String code,String message){
		super(message);
		this.code=code;
		this.message=message;
	}
	
	public ApplicationException(String code,Throwable e){
		super(code,e);
		this.code=code;
		this.e = e;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object[] getParams() {
		return params;
	}

	public void setParams(Object[] params) {
		this.params = params;
	}

	public Throwable getE() {
		return e;
	}

	public void setE(Throwable e) {
		this.e = e;
	}

	public String getParamsToString() {
		if(null != this.params){
			StringBuilder sb = new StringBuilder();
			for(Object o : params){
				sb.append(o.toString()).append(",");
			}
			int position = sb.lastIndexOf(",");
			if(position>0){
				sb.deleteCharAt(position);
			}
			this.paramsToString = sb.toString();
		}
		return paramsToString;
	}

	
}
