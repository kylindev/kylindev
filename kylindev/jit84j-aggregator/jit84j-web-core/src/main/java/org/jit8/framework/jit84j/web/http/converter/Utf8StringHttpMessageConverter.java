package org.jit8.framework.jit84j.web.http.converter;

import java.io.IOException;
import java.nio.charset.Charset;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.charset.StringHttpMessageConverter;

public class Utf8StringHttpMessageConverter extends StringHttpMessageConverter {

	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");
	
	public Utf8StringHttpMessageConverter(Charset defaultCharset){
		
	}

}
