package org.jit8.framework.jit84j.exception;

public class BusinessException extends ApplicationException {


	private static final long serialVersionUID = -6465609521417351180L;

	public BusinessException(String code,String message,Object[] params,Throwable e){
		super(code, message, params, e);
	}
	
	public BusinessException(String code,String message,Object[] params){
		super(code, message, params);
	}
	
	public BusinessException(String code,String message,Throwable e){
		super(code, message,  e);
	}
	
	public BusinessException(String code,String message){
		super(code, message);
	}

}
