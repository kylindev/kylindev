package org.jit8.framework.jit84j.web.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.exception.constant.ExceptionMessageConstant;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ExceptionCommonController extends BaseController{
	
	private static final String errorCode = "error_code";
	
	private static final Logger logger = LoggerFactory.getLogger(ExceptionCommonController.class);
	
	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_MAPPING)
	public String resolve403(Model model,HttpServletRequest request,
			HttpServletResponse response){
		String message = MessageUtil.getLocaleMessage(request,
				ExceptionMessageConstant.ERROR_CODE_403, null);
		model.addAttribute(errorCode, message);
		logger.warn(request.getRequestURL().toString());
		if (!isAjax(request)) {
			// 如果不是异步请求
			return "error_page";
		} else {
			// JSON格式返回
			try {
				response.setContentType("application/json;charset=UTF-8");
				PrintWriter writer = response.getWriter();
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		
		return null;
	}

	/**
	 * @param request
	 * @return
	 */
	private boolean isAjax(HttpServletRequest request) {
		return request.getHeader("accept").indexOf("application/json") > -1 || (request
				.getHeader("X-Requested-With") != null && request.getHeader(
				"X-Requested-With").indexOf("XMLHttpRequest") > -1);
	}
	
	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_MAPPING)
	public String resolve404(Model model, HttpServletRequest request,
			HttpServletResponse response) throws BusinessException {
		String message = MessageUtil.getLocaleMessage(request,
				ExceptionMessageConstant.ERROR_CODE_404, null);
		model.addAttribute(errorCode, message);

		logger.warn(request.getRequestURL().toString());
		if (!isAjax(request)) {
			// 如果不是异步请求
			return "error_page";
		} else {
			// JSON格式返回
			try {
				response.setContentType("application/json;charset=UTF-8");
				PrintWriter writer = response.getWriter();
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_MAPPING)
	public String resolve405(Model model,HttpServletRequest request, HttpServletResponse response){
		String message = MessageUtil.getLocaleMessage(request,
				ExceptionMessageConstant.ERROR_CODE_405, null);
		model.addAttribute(errorCode, message);
		if (!isAjax(request)) {
			// 如果不是异步请求
			return "error_page";
		} else {
			// JSON格式返回
			try {
				response.setContentType("application/json;charset=UTF-8");
				PrintWriter writer = response.getWriter();
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_MAPPING)
	public String resolve406(Model model,HttpServletRequest request, HttpServletResponse response){
		String message = MessageUtil.getLocaleMessage(request,
				ExceptionMessageConstant.ERROR_CODE_406, null);
		model.addAttribute(errorCode, message);
		if (!isAjax(request)) {
			// 如果不是异步请求
			return "error_page";
		} else {
			// JSON格式返回
			try {
				response.setContentType("application/json;charset=UTF-8");
				PrintWriter writer = response.getWriter();
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_MAPPING)
	public String resolve500(Model model,HttpServletRequest request, HttpServletResponse response){
		String message = MessageUtil.getLocaleMessage(request,
				ExceptionMessageConstant.ERROR_CODE_500, null);
		model.addAttribute(errorCode, MessageUtil.getLocaleMessage(request, ExceptionMessageConstant.ERROR_CODE_500, null));
		
		if (!isAjax(request)) {
			// 如果不是异步请求
			return "error_500_page";
		} else {
			// JSON格式返回
			try {
				response.setContentType("application/json;charset=UTF-8");
				PrintWriter writer = response.getWriter();
				writer.write(message);
				writer.flush();
			} catch (IOException e) {
				logger.error(e.getMessage(), e);
			}
		}
		return null;
	}
}
