package org.jit8.framework.jit84j.web.constants;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jit8.user.developer.common.constants.CommonDeveloperConstant;
import org.springframework.stereotype.Component;

@Component
public class WebConstants implements Serializable{

	private static final long serialVersionUID = -4799815383252353083L;

	public static final String _CURRENT_USER = "_current_user";
	public static final String _SHAREINFO = "_thread_local_shareInfo";
	
	
	
	
	
	//后台路径 起始符
	public static final String _ADMIN_PATH_PREFIX="/admin";
	public static final String _FRONT_PATH_PREFIX="/home";
	public static final String _AUTHORIZE_PATH_PARAM="authorize_path_param";
	public static final String _AUTHORIZE_PATH_PARAM_DEFAULT="default";
	public static String _AUTHORIZE_SEPORATOR = "---";//权限url截取分隔符
	
	
	
	//web site param data
	public static boolean frontPermissionEnable = false;
	public static boolean adminPermissionEnable = false;//开发环境，设置为false,生产环境设置为true
	
}
