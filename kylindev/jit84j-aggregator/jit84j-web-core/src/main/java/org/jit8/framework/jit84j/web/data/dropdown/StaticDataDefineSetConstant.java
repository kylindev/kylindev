package org.jit8.framework.jit84j.web.data.dropdown;

import java.util.Map;

import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;

public class StaticDataDefineSetConstant extends StaticDataDefineConstant {

	private static final long serialVersionUID = 2406124021010276339L;

	public StaticDataDefineSetConstant() {
	}

	/**
	 * 获取memached中的缓存数据
	 */
	@Override
	public void addKeysInMemcached(){
		addKeyIntoMemcached(StaticDataDefineConstant.OWN_RELATION_LIST,StaticDataDefineConstant.OWN_RELATION_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.NAVIGATION_TOP_LIST,StaticDataDefineConstant.NAVIGATION_TOP_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.NAVIGATMENU_TOP_LIST,StaticDataDefineConstant.NAVIGATMENU_TOP_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.NAVIGATION_TYPE_DROPDOWN,StaticDataDefineConstant.NAVIGATION_TYPE_DROPDOWN_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.DIRECT_PERMISSION_SET,StaticDataDefineConstant.DIRECT_PERMISSION_SET_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.GALLERY_LIST,StaticDataDefineConstant.GALLERY_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN,StaticDataDefineConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN_NAME);
		
	}
	
	

	@Override
	public void addMemachedModelByKey() {
		addMemachedModelByKeyIntoMemcached(StaticDataDefineConstant.OWN_RELATION_LIST, new MemachedModel());
	}

}
