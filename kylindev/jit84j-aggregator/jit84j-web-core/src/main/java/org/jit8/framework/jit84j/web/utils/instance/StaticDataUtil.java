package org.jit8.framework.jit84j.web.utils.instance;

import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineManager;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.persist.domain.menu.Navigation;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class StaticDataUtil{
	
	@Resource
	protected MessageSource messageSource;
	
	@Resource
	protected StaticDataDefineManager staticDataDefineManager;
	
	private static StaticDataDefineManager sdm;
	
	
	@PostConstruct
	public void init(){
		sdm = staticDataDefineManager;
	}
	
	
	public static Navigation getCurrentNavigationByCode(String code){
		
		Map<String,Navigation > map = (Map<String,Navigation >)sdm.getData(StaticDataDefineConstant.NAVIGATION_MAP);
		Navigation navigation = null;
		if(null != map){
			navigation = map.get(code);
		}
		return navigation;
	}
	
	public static List<Navigation> getNavigationTopList(){
		List<Navigation> navigationTopList = (List<Navigation>)sdm.getData(StaticDataDefineConstant.NAVIGATION_TOP_LIST, false);
		return navigationTopList;
	}
	
	public static List<NavigatMenu> getNavigatMenuTopList(){
		List<NavigatMenu> navigatMenuTopList = (List<NavigatMenu>) sdm.getData(StaticDataDefineConstant.NAVIGATMENU_TOP_LIST);
		return navigatMenuTopList;
	}
	
	public static NavigatMenu getCurrentNavigatMenuByCode(String code){
		Map<String,NavigatMenu > map = (Map<String,NavigatMenu >)sdm.getData(StaticDataDefineConstant.NAVIGATMENU_MAP);
		NavigatMenu navigatMenu = null;
		if(null != map){
			navigatMenu = map.get(code);
		}
		return navigatMenu;
	}
}
