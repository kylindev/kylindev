package org.jit8.user.developer.common.constants;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public interface CommonDeveloperInterface extends Serializable{
	
	String IDKEY_PATTERN_DEVELOPER = "(\\d{6,})[a-zA-Z0-9_-]+";

	public List<UrlStaticMapping> getUrlStaticMappingModuleList();
	
	//public List<UrlStaticMapping> getUrlStaticMappingList();
	
	public Map<String, UrlStaticMapping> getUrlStaticMappingMap();
	
	public  UrlStaticMapping getUrlStaticMapping(String idKey);
	
	public Long getDeveloperUserId();
}
