package org.jit8.user.developer.common.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public abstract class CommonDeveloperConstant implements CommonDeveloperInterface{

	private static final long serialVersionUID = 8524018069094172646L;

	public static final String _COMMON_USER_SEPERATOR = "_";//用户分隔符
	public static final String _COMMON_USER_SEPERATOR_U = "u";//标识用户分隔符
	public static final String _COMMON_PARAM_SEPERATOR = "~";//参数分隔符
	
	private  List<String> urlMappingList = new ArrayList<String>();
	
	protected  List<UrlStaticMapping> urlStaticMappingModuleList = new ArrayList<UrlStaticMapping>();
	
	private  List<String> urlStaticMappingCodeList = new ArrayList<String>();
	private  Map<String ,UrlStaticMapping> urlStaticMappingMap = new HashMap<String,UrlStaticMapping>();
	
	
	
	public void add(String urlMapping){
		if(StringUtils.isNotEmpty(urlMapping)  && !urlMappingList.contains(urlMapping)){
			urlMappingList.add(urlMapping);
		}
	}
	
	public List<String> getUrlMappingList() {
		return urlMappingList;
	}
	
	
	public UrlStaticMapping addModule(String idKey,String moduleName,String moduleCode){
		UrlStaticMapping module = null;
		if(StringUtils.isNotEmpty(moduleName) && StringUtils.isNotEmpty(moduleCode)  ){
			if(!urlStaticMappingCodeList.contains(moduleCode)){
				urlStaticMappingCodeList.add(moduleCode);
			    module = new UrlStaticMapping(idKey,moduleName,moduleCode);
				urlStaticMappingModuleList.add(module);
				urlStaticMappingMap.put(moduleCode, module);
			}else{
				module = urlStaticMappingMap.get(moduleCode);
			}
		}
		return module;
	}
	
	@Override
	public List<UrlStaticMapping> getUrlStaticMappingModuleList() {
		return urlStaticMappingModuleList;
	}

	public void setUrlMappingList(List<String> urlMappingList) {
		this.urlMappingList = urlMappingList;
	}
	
	/**
	 * 添加一组url到所属模块
	 * @param module
	 * @param url
	 * @param urlCode
	 * @param urlName
	 * @param urlFullPath
	 * @param urlDesc
	 * @throws RuntimeException
	 */
	protected void addUrlStaticMapping(UrlStaticMapping module,String idKey,String urlCode,String urlName, String urlDesc , String url,String urlFullPath) throws RuntimeException{
		
		if(null == module){
			throw new RuntimeException("module can't be null ");
		}
		
		List<UrlStaticMapping> childUrlList = module.getChildUrlList();
		if(null == childUrlList){
			childUrlList = new ArrayList<UrlStaticMapping>();
			module.setChildUrlList(childUrlList);
		}
		if(StringUtils.isEmpty(urlFullPath) || StringUtils.isEmpty(urlCode)){
			throw new RuntimeException("urlCode， urlFullPath can't be empty ");
		}
		
		
		UrlStaticMapping urlStaticMapping = new UrlStaticMapping(idKey,urlCode, urlName,  urlDesc, url,  urlFullPath);
		boolean exist = false;
		if(childUrlList.size()>0){
			for(UrlStaticMapping urlExist : childUrlList){
				String idKeyExist = urlExist.getIdKey();
				String urlFullPathExist = urlExist.getUrlFullPath();
				String urlCodeExist = urlExist.getUrlCode();
				if((StringUtils.isNotEmpty(idKeyExist) && idKeyExist.equals(idKey)) || (StringUtils.isNotEmpty(urlFullPathExist) && urlFullPathExist.equals(urlFullPath) && StringUtils.isNotEmpty(urlCodeExist) && urlCodeExist.equals(urlCode))){
					exist = true;
					break;
				}
			}
		}
		if(!exist){
			add(urlFullPath);//加入进urlMapping
			childUrlList.add(urlStaticMapping);
			urlStaticMapping.setModule(module);
			urlStaticMappingMap.put(urlStaticMapping.getIdKey(), urlStaticMapping);
		}
		
	}
	
	@Override
	public abstract Long getDeveloperUserId();

	public Map<String, UrlStaticMapping> getUrlStaticMappingMap() {
		return urlStaticMappingMap;
	}

	public void setUrlStaticMappingMap(
			Map<String, UrlStaticMapping> urlStaticMappingMap) {
		this.urlStaticMappingMap = urlStaticMappingMap;
	}
	
	public  UrlStaticMapping getUrlStaticMapping(String idKey) {
		UrlStaticMapping urlStaticMapping = null;
		if(StringUtils.isNotEmpty(idKey)){
			if(null != urlStaticMappingMap){
				urlStaticMapping = urlStaticMappingMap.get(idKey);
			}
		}
		return urlStaticMapping;
	}
	
}
