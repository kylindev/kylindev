/**
 * 
 */
package org.jit8.user.developer.common.constants;

/**
 * URL跳转页面常量类
 * @author hezanyuan
 *
 */
public interface URLConstant {
	
	/** 用户管理 **/
	public static final String _USER_LIST = "user_List";
	public static final String _USER_ADD = "user_Add";
	public static final String _USER_EDIT = "user_edit";
	public static final String _USER_SAVE = "user_save";
	public static final String _USER_DEL = "user_del";
	public static final String _USER_VIEW = "user_view";

}
