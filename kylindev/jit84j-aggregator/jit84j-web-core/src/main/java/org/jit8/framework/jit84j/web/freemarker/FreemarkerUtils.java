package org.jit8.framework.jit84j.web.freemarker;


import java.io.IOException;
import java.io.StringWriter;
import java.util.Map;

import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import freemarker.template.Template;

public class FreemarkerUtils{

	private static final Logger LOGGER = LoggerFactory.getLogger(FreemarkerUtils.class);
	
	public static FreeMarkerConfigurer getFreeMarkerConfigurer(){
		
		FreeMarkerConfigurer freeMarkerConfigurer = (FreeMarkerConfigurer)ApplicationContext.getBean(FreeMarkerConfigurer.class);
		
		return freeMarkerConfigurer;
	}
	
	public static Template getTemplate(String path){
		FreeMarkerConfigurer cfg = getFreeMarkerConfigurer();
		Template temp = null;
		try {
			if(null != cfg){
				temp = cfg.getConfiguration().getTemplate(path);
			}
		} catch (IOException e) {
			LOGGER.error("email.template.load.failure",e);
		}
		return temp;
	}
	
	public static StringBuffer process(String path, Map<String, Object> root) {
		StringWriter out = null;
		StringBuffer result = null;
		try {
			out = new StringWriter();
			Template temp = getTemplate(path);
			temp.process(root, out);
			result = out.getBuffer();
			out.flush(); 
		}catch (Exception e) {
			LOGGER.error("email.template.render.failure", e);
		}finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					LOGGER.error("email.template.close.writer.failure", e);
				}
			}
		}
		return result;
	}
}
