package org.jit8.framework.jit84j.core.init.test;

import org.jit8.framework.jit84j.core.init.test.model.Home;
import org.jit8.framework.jit84j.core.init.test.service.HomeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:META-INF/spring-persistence.xml"})
@Transactional  
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)  
public class TestHomeService {

	@Autowired
	private HomeService homeService;
	@Test
	@Rollback(false)
	public void testSave(){
		homeService.save(new Home("at home"));
		homeService.save(new Home("中国"));
	}
}
