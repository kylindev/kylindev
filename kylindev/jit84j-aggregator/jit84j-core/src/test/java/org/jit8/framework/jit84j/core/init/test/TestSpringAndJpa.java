package org.jit8.framework.jit84j.core.init.test;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:META-INF/spring-persistence.xml"})
public class TestSpringAndJpa {

	@PersistenceContext
	EntityManager em;

	@Test
	@Transactional
	@Rollback(false)
	public void testSpring() {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"classpath:META-INF/spring-persistence.xml");
		DataSource ds = (DataSource) ctx.getBean("dataSource");
		System.out.println(ds); // 不为null,即通过
		
		//em.getTransaction().begin();
		em.persist(new DBConnection("电脑"));
		//em.getTransaction().commit();
		em.close();
	}

}
