package org.jit8.camera.bussiness;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexTest {

	//URL组成：模块路径 code [权限参数个数标识][菜单参数个数标识] 参数 任意字符 
	private static final String URL_SEP =   "u_[0-9]{5,10}_u" ;
		
	public static final String URL_REX_PARAM_SEPORATOR = "~";
	public static final String URL_REX_COMMON_PREFIX="[0-9a-zA-Z/-_]*";
	public static final String URL_REX_SPECIFIC=URL_SEP;//先按目前以_u为分隔截取，后续将改造成code+6位developerId
	public static final String URL_REX_PERMISSION_NAVIGATION = "P([0-9]{1,2})N([0-9]{1,2})";//权限与菜单个数标识
	public static final String URL_REX_PARAM = "[0-9a-zA-Z-_]+";//参数匹配
	public static final String URL_REX_PARAM_SEPORATOR_AND_PARAM = URL_REX_PARAM_SEPORATOR + URL_REX_PARAM;//参数分隔符与参数匹配
	public static final String URL_REX_ANY = ".*";//任意字符匹配
	
	
	//URL定义模式
	//带参数的匹配模式
	public static final String URL_REX_P_N = "(("+ URL_REX_COMMON_PREFIX + URL_SEP + ")"+ URL_REX_PERMISSION_NAVIGATION  + URL_REX_ANY+")";
	//1.先根据以上模式 匹配出 权限有几个参数，菜单有几个参数（其中 菜单参数个数>=权限参数个数）
	//2.再根据参数个数作拼接，组成新的模式，作匹配，
	//3.再匹配，第一个url为 菜单路径，第二个url为 权限路径
	
	//不带参数的匹配模式
	public static final String URL_REX_NO_PARAM = "("+URL_REX_COMMON_PREFIX + URL_REX_SPECIFIC + ")"+URL_REX_ANY;
	
	public static void main(String[] args) {
		String urlRegex1 = "([0-9a-zA-Z/-_]*P([0-9]{1,2}).*)";
		
		String url1 = "/admin/user/wwwwu_100001_uP2N3~232~323~2?oo=00";
		
		Pattern p = Pattern.compile(URL_REX_P_N);
		Matcher m = p.matcher(url1);
		int permissionParamNum = 0;
		int navigationNum = 0;
		if(m.find()){
			String g1 = m.group(1);//全url
			System.out.println("g1 : " +g1);
			String g2 = m.group(2);//主url
			System.out.println("g2 : " +g2);
			String g3 = m.group(3);//permission参数个数
			System.out.println("g3 : " + g3);
			permissionParamNum= Integer.valueOf(g3);
			String g4 = m.group(4);//navigation参数个数
			System.out.println("g4 : " + g4);
			navigationNum = Integer.valueOf(g4);
		}
		String URL_REX_PARAM_ = "(("+ URL_REX_COMMON_PREFIX + URL_SEP +  URL_REX_PERMISSION_NAVIGATION + "(" + URL_REX_PARAM_SEPORATOR_AND_PARAM +"){"+ permissionParamNum +"})("+URL_REX_PARAM_SEPORATOR_AND_PARAM+"){"+ ( navigationNum - permissionParamNum )+"})"+URL_REX_ANY;
		System.out.println(URL_REX_PARAM_);

		Pattern p2 = Pattern.compile(URL_REX_PARAM_);
		Matcher m2 = p2.matcher(url1);
		if(m2.find()){
			
			String g1 = m2.group(1);//navigation 路径
			String g2 = m2.group(2);//permission路径
			
			System.out.println(g1);
			System.out.println(g2);
			
		}
		System.out.println("end");
		
		
		//StringBuilder sb = new StringBuilder("([0-9a-zA-Z/-_]*");
		//sb.append(URL_SEP).append("P([0-9]{1,2})N([0-9]{1,2})(~[0-9a-zA-Z-_]+){").append(g2).append("}).*");
		
		
		
		System.out.println(URL_REX_P_N);
		System.out.println(URL_REX_NO_PARAM);


	}

}
