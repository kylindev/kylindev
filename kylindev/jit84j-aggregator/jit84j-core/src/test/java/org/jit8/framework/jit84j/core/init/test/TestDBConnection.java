package org.jit8.framework.jit84j.core.init.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.Test;

public class TestDBConnection {

	@Test
	public void testJpa() {
		// 未配置spring时这样获取EntityManagerFactory(实体管理器工厂)
		// 根据jpa规范，使用jpa的入口点是Persistence类
		EntityManagerFactory factory = Persistence
				.createEntityManagerFactory("jit84j-zh-persistence");// 传递一个持久化单元名称,这里的名称对应
															// persistence.xml中配置的持久化单元名称
		EntityManager em = factory.createEntityManager();// 得到EntityManager(作用类类于Session)
		em.getTransaction().begin();
		em.persist(new DBConnection("电脑"));
		em.getTransaction().commit();
		em.close();
		factory.close();
	}
	
	

}
