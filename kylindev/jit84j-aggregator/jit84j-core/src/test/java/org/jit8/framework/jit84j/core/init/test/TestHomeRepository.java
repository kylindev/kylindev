package org.jit8.framework.jit84j.core.init.test;

import org.jit8.framework.jit84j.core.init.test.model.Home;
import org.jit8.framework.jit84j.core.init.test.repository.HomeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value={"classpath:META-INF/spring-persistence.xml"})
public class TestHomeRepository {

	@Autowired
	private HomeRepository homeRepository;
	@Test
	@Rollback(false)
	public void testPersist(){
		homeRepository.save(new Home("at home"));
		homeRepository.save(new Home("中国"));
	}
}
