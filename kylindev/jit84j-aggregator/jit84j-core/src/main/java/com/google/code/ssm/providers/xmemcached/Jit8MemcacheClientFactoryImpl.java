package com.google.code.ssm.providers.xmemcached;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Map;

import net.rubyeye.xmemcached.Jit8XMemcachedClientBuilder;
import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientBuilder;
import net.rubyeye.xmemcached.XMemcachedClientBuilder;
import net.rubyeye.xmemcached.command.BinaryCommandFactory;
import net.rubyeye.xmemcached.impl.KetamaMemcachedSessionLocator;
import net.rubyeye.xmemcached.utils.Protocol;

import org.jit8.framework.jit84j.core.cache.memached.IMemcachedClient;

import com.google.code.ssm.providers.CacheClient;
import com.google.code.ssm.providers.CacheConfiguration;
import com.google.code.yanf4j.core.SocketOption;

public class Jit8MemcacheClientFactoryImpl extends MemcacheClientFactoryImpl implements IMemcachedClient{

		private MemcachedClientBuilder builder = null;
		
		private MemcachedClient client = null;
		
	
	 	@Override
	    public CacheClient create(final List<InetSocketAddress> addrs, final CacheConfiguration conf) throws IOException {
	 		
	 		//1.如果设置了 builder 则 builder 设置优先
	 		//2.如果设置了 memcachedClient 原设置不起作用,使用设置CacheConfiguration的设置
	 		
	 		MemcachedClientBuilder builder = this.builder;
	 		Jit8XMemcachedClientBuilder jit8Builder = null;

	 		if(null != builder && builder instanceof Jit8XMemcachedClientBuilder){
            	jit8Builder =  (Jit8XMemcachedClientBuilder)builder;
            	jit8Builder.setAddressList(addrs);
            }
	 		
	        if (conf instanceof XMemcachedConfiguration) {
	            int[] weights = ((XMemcachedConfiguration) conf).getWeights();
	            if (weights != null && weights.length > 0 && null == builder) {
	            	if(null != jit8Builder){
	            		jit8Builder.setWeights(weights);
	            	}else{
	            		builder = new XMemcachedClientBuilder(addrs, weights);
	            	}
	                
	            }
	        }

	        if (builder == null) {
	            builder = new XMemcachedClientBuilder(addrs);
	        }
	        
	        if (conf.isConsistentHashing()) {
	            builder.setSessionLocator(new KetamaMemcachedSessionLocator());
	        }

	        if (conf.isUseBinaryProtocol()) {
	            builder.setCommandFactory(new BinaryCommandFactory());
	        }

	        if (conf instanceof XMemcachedConfiguration) {
	            setProviderBuilderSpecificSettings(builder, (XMemcachedConfiguration) conf);
	        }
	        
	        //用builder重新构建一个MemcachedClient
	        MemcachedClient client = builder.build();
	        //如果传入的client不为null, 则将client值赋给 this.client;
	        
	        //如果设置了conf，则将conf的配置赋给this.client
	        if (conf.getOperationTimeout() != null) {
	            client.setOpTimeout(conf.getOperationTimeout());
	        }

	        if (conf instanceof XMemcachedConfiguration) {
	            setProviderClientSpecificSettings(client, (XMemcachedConfiguration) conf);
	        }
	        
	        this.client=client;//这里将memachedClient的引用更改了
	        
	        return new MemcacheClientWrapper(client);
	    }

	    public void setProviderBuilderSpecificSettings(final MemcachedClientBuilder builder, final XMemcachedConfiguration conf) {
	        if (conf.getConnectionPoolSize() != null) {
	            builder.setConnectionPoolSize(conf.getConnectionPoolSize());
	        }

	        if (conf.getConfiguration() != null) {
	            builder.setConfiguration(conf.getConfiguration());
	        }

	        if (conf.getFailureMode() != null) {
	            builder.setFailureMode(conf.getFailureMode());
	        }

	        if (conf.getSocketOptions() != null) {
	            for (Map.Entry<SocketOption<?>, Object> entry : conf.getSocketOptions().entrySet()) {
	                builder.setSocketOption(entry.getKey(), entry.getValue());
	            }
	        }

	        if (conf.getDefaultTranscoder() != null) {
	            builder.setTranscoder(conf.getDefaultTranscoder());
	        }

	        if (conf.getConnectionTimeout() != null) {
	            builder.setConnectTimeout(conf.getConnectionTimeout());
	        }

	        if (conf.getMaxQueuedNoReplyOperations() != null) {
	            builder.setMaxQueuedNoReplyOperations(conf.getMaxQueuedNoReplyOperations());
	        }

	        if (conf.getEnableHealSession() != null) {
	            builder.setEnableHealSession(conf.getEnableHealSession());
	        }

	        if (conf.getAuthInfoMap() != null) {
	            builder.setAuthInfoMap(conf.getAuthInfoMap());
	        }

	        if (conf.getStateListeners() != null) {
	            builder.setStateListeners(conf.getStateListeners());
	        }
	    }

	    public void setProviderClientSpecificSettings(final MemcachedClient client, final XMemcachedConfiguration conf) {
	        if (conf.getMaxAwayTime() != null) {
	            client.addStateListener(new ReconnectListener(conf.getMaxAwayTime()));
	        }

	        if (conf.getEnableHeartBeat() != null) {
	            client.setEnableHeartBeat(conf.getEnableHeartBeat());
	        }

	        if (conf.getHealSessionInterval() != null) {
	            client.setHealSessionInterval(conf.getHealSessionInterval());
	        }

	        if (conf.getMergeFactor() != null) {
	            client.setMergeFactor(conf.getMergeFactor());
	        }

	        if (conf.getOptimizeGet() != null) {
	            client.setOptimizeGet(conf.getOptimizeGet());
	        }

	        if (conf.getOptimizeMergeBuffer() != null) {
	            client.setOptimizeMergeBuffer(conf.getOptimizeMergeBuffer());
	        }

	        if (conf.getPrimitiveAsString() != null) {
	            client.setPrimitiveAsString(conf.getPrimitiveAsString());
	        }

	        if (conf.getSanitizeKeys() != null) {
	            client.setSanitizeKeys(conf.getSanitizeKeys());
	        }

	    }

		/**
		 * @return the client
		 */
		public MemcachedClient getClient() {
			return client;
		}

		/**
		 * @param builder the builder to set
		 */
		public void setBuilder(MemcachedClientBuilder builder) {
			this.builder = builder;
		}

		/**
		 * @param client the client to set
		 */
		public void setClient(MemcachedClient client) {
			this.client = client;
		}
}
