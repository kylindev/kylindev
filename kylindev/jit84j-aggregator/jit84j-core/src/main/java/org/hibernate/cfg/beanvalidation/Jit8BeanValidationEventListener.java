package org.hibernate.cfg.beanvalidation;

import java.util.Properties;

import javax.validation.ValidatorFactory;

import org.hibernate.event.spi.PreDeleteEvent;
import org.hibernate.event.spi.PreInsertEvent;
import org.hibernate.event.spi.PreUpdateEvent;

public class Jit8BeanValidationEventListener extends BeanValidationEventListener {


	private static final long serialVersionUID = 1L;
	
	private boolean modelValidateEnable;//是否开启模型层校验

	public Jit8BeanValidationEventListener() {
	}

	public Jit8BeanValidationEventListener(ValidatorFactory factory, Properties properties) {
		super(factory, properties);
	}

	public boolean onPreInsert(PreInsertEvent event) {
		if(modelValidateEnable){
			super.onPreInsert(event);
		}
		return false;
	}

	public boolean onPreUpdate(PreUpdateEvent event) {
		if(modelValidateEnable){
			super.onPreUpdate(event);
		}
		return false;
	}

	public boolean onPreDelete(PreDeleteEvent event) {
		if(modelValidateEnable){
			super.onPreDelete(event);
		}
		return false;
	}

	/**
	 * @return the modelValidateEnable
	 */
	public boolean isModelValidateEnable() {
		return modelValidateEnable;
	}

	/**
	 * @param modelValidateEnable the modelValidateEnable to set
	 */
	public void setModelValidateEnable(boolean modelValidateEnable) {
		this.modelValidateEnable = modelValidateEnable;
	}
}
