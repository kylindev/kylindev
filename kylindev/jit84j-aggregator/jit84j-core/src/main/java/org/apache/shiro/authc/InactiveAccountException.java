package org.apache.shiro.authc;

public class InactiveAccountException extends DisabledAccountException {

	private static final long serialVersionUID = -558177555675964121L;

	public InactiveAccountException() {
	}

	public InactiveAccountException(String message) {
		super(message);
	}

	public InactiveAccountException(Throwable cause) {
		super(cause);
	}

	public InactiveAccountException(String message, Throwable cause) {
		super(message, cause);
	}

}
