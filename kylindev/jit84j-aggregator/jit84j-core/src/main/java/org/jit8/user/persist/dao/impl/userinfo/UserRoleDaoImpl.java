package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.UserRoleDao;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.jit8.user.persist.repository.userinfo.UserRoleRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserRoleDaoImpl extends GenericDaoImpl<UserRole, Long> implements UserRoleDao {

	@Resource
	private UserRoleRepository userRoleRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = userRoleRepository;
	}

	@Override
	public List<UserRole> findUserRoleList(Long userId) {
		return userRoleRepository.findUserRoleList(userId);
	}

	@Override
	public void removeByIdList(List<Long> userIdList) {
		userRoleRepository.removeByIdList(userIdList);
	}
}
