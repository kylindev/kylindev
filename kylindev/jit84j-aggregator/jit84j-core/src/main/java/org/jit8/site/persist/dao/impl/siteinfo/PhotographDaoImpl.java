package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.PhotographDao;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.jit8.site.persist.repository.siteinfo.PhotographRepository;
import org.springframework.stereotype.Repository;

@Repository
public class PhotographDaoImpl extends GenericDaoImpl<Photograph, Long> implements PhotographDao {

	@Resource
	private PhotographRepository photographRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = photographRepository;
	}

	@Override
	public List<Photograph> getPhotographList(boolean disable, boolean deleted) {
		return photographRepository.getPhotographList(disable, deleted);
	}
}
