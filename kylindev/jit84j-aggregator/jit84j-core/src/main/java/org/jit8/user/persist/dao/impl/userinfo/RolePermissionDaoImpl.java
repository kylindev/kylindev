package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.RolePermissionDao;
import org.jit8.user.persist.domain.userinfo.RolePermission;
import org.jit8.user.persist.repository.userinfo.RolePermissionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class RolePermissionDaoImpl extends GenericDaoImpl<RolePermission, Long> implements RolePermissionDao {

	@Resource
	private RolePermissionRepository rolePermissionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = rolePermissionRepository;
	}

	@Override
	public List<RolePermission> findByPermissionId(Long permissionId) {
		return rolePermissionRepository.findByPermissionId( permissionId);
	}

}
