package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategorySpecialRepository extends GenericJpaRepository<CategorySpecial, Long>{

	@Query("from CategorySpecial o where o.disable=:disable and o.deleted=:deleted")
	List<CategorySpecial> getCategorySpecialList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from CategorySpecial o where o.name=:name")
	public CategorySpecial findByName(@Param("name")String name);
	
	@Modifying
	@Query("delete from CategorySpecial o where o.specialArea.uniqueIdentifier=:specialId and o.category.uniqueIdentifier in :categoryIdList")
	public void removeBySpecialIdAndCategoryIdList(@Param("specialId")Long specialId, @Param("categoryIdList")List<Long> categoryIdList);

	@Query("from CategorySpecial o where o.uniqueIdentifier in :idList")
	public List<CategorySpecial> findByIdList(@Param("idList")List<Long> idList);
	
	@Query("from CategorySpecial o where o.specialArea.uniqueIdentifier = :specialAreaId")
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(@Param("specialAreaId")Long id);
	
	
	@Query("from CategorySpecial o where o.uniqueIdentifier in :categorySpecialIdList and o.deleted=false")
	public List<CategorySpecial> getByCategorySpecialIdList(
			@Param("categorySpecialIdList")List<Long> categorySpecialIdList);

}
