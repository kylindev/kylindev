package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;

public interface FileSystemGalleryDao extends GenericDao<FileSystemGallery, Long> {

	public List<FileSystemGallery> getFileSystemGalleryList(boolean disable, boolean deleted) ;
	
}
