package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.DirectPermission;

public interface DirectPermissionRepository extends GenericJpaRepository<DirectPermission, Long>{

	public DirectPermission findByType(String type);
}
