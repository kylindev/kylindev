package org.jit8.user.persist.dao.userinfo;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.EmailUrl;

public interface EmailUrlDao extends GenericDao<EmailUrl, Long> {

	public EmailUrl findByUrlCode(String urlCode);
}
