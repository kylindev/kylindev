package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UrlStaticMappingRepository extends GenericJpaRepository<UrlStaticMapping, Long>{

	@Query("from UrlStaticMapping u where u.idKey=:idKey")
	public UrlStaticMapping findByUrlStaticMappingIdKey(@Param("idKey")String idKey);
	
	
	@Query("from UrlStaticMapping u where u.module is null")
	public Page<UrlStaticMapping> findModule(Pageable pageable);
	
	
	/**
	 * 根据module idKey查找URL
	 * @param moduleIdKey
	 * @param pageable
	 * @return
	 */
	@Query("from UrlStaticMapping u where u.module.idKey=:moduleIdKey")
	public Page<UrlStaticMapping> findUrlStaticMapping(@Param("moduleIdKey")String moduleIdKey,Pageable pageable);
	
	
	@Query("from UrlStaticMapping u where u.idKey in :idKeyList")
	public List<UrlStaticMapping> findByUrlStaticMappingIdKeyList(@Param("idKeyList")List<String> idKeyList);
	
	@Query("from UrlStaticMapping u where u.module is not null and u.deleted=false")
	public Page<UrlStaticMapping> findUrlStaticMappingWithoutModule(
			Pageable pageable);
	
	
}
