package org.jit8.site.persist.repository.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.article.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ArticleRepository extends GenericJpaRepository<Article, Long>{

	@Query("from Article o where o.disable=:disable and o.deleted=:deleted")
	List<Article> getArticleList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);
	
	@Query("from Article o where o.disable=:disable and o.deleted=:deleted")
	public Page<Article> getArticleList(@Param("disable")boolean disable, @Param("deleted")boolean deleted,Pageable pageable);

}
