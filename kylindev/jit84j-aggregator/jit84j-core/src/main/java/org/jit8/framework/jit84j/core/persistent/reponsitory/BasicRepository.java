package org.jit8.framework.jit84j.core.persistent.reponsitory;

import java.io.Serializable;

public interface BasicRepository<T, ID extends Serializable> extends GenericJpaRepository<T, ID>{

}
