package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FileInfoDao extends GenericDao<FileInfo, Long> {

	public List<FileInfo> getFileInfoList(boolean disable, boolean deleted) ;
	
	public FileInfo findByIdAndOwner(long id, long owner);
	
	public Page<FileInfo> findAll(FileInfo fileInfo, Pageable pageable);
}
