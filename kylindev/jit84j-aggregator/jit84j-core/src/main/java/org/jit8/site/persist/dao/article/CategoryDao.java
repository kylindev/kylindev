package org.jit8.site.persist.dao.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.article.Category;

public interface CategoryDao extends GenericDao<Category, Long> {

	public List<Category> getCategoryList(boolean disable, boolean deleted) ;
	
	public List<Category> getCategoryListByIds(boolean disable,boolean deleted,List<Long> categoryIdList);
	
	public List<Category> getCategoryListByParentNull();
}
