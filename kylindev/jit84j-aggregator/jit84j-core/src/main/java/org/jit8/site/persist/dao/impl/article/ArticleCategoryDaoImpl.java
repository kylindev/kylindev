package org.jit8.site.persist.dao.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.article.ArticleCategoryDao;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.jit8.site.persist.repository.article.ArticleCategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleCategoryDaoImpl extends GenericDaoImpl<ArticleCategory, Long> implements ArticleCategoryDao {

	@Resource
	private ArticleCategoryRepository articleCategoryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = articleCategoryRepository;
	}

	@Override
	public List<ArticleCategory> getArticleCategoryList(boolean disable, boolean deleted) {
		return articleCategoryRepository.getArticleCategoryList(disable, deleted);
	}

	@Override
	public List<ArticleCategory> getArticleCategoryListByArticleId(
			boolean disable, boolean deleted, Long articleId) {
		return articleCategoryRepository.getArticleCategoryListByArticleId(disable, deleted, articleId);
	}
}
