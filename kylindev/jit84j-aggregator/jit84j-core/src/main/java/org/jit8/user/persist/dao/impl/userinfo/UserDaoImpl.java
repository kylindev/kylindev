package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.UserDao;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.spec.UserSpecifications;
import org.jit8.user.persist.repository.userinfo.UserRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Long> implements UserDao {

	@Resource
	private UserRepository userRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = userRepository;
	}
	@Override
	public User findUserByUsername(String username){
		return userRepository.findUserByUsername(username);
	}

	@Override
	public boolean exitUserByUsername(String username) {
		User user = findUserByUsername(username);
		if(user != null){
			return true;
		}
		return false;
	}
	@Override
	public User findUserByEmail(String email) {
		return userRepository.findUserByEmail(email);
	}
	@Override
	public boolean exitUserByEmail(String email) {
		User user = findUserByEmail(email);
		if(user != null){
			return true;
		}
		return false;
	}
	
	public User findUserByEmailAndPassword(String email, String password) {
		return userRepository.findUserByEmailAndPassword(email,password);
	}
	@Override
	public Page<User> getUserList(Pageable pageable) {
		//List<User> userList = userRepository.findAll(UserSpecifications.eqUsername("kylinboy"));
		return userRepository.getUserList(pageable);
	}
	
	@Override
	public Page<User> getLastUserList(Pageable pageable) {
		return userRepository.getLastUserList(pageable);
	}
	
	@Override
	public List<User> getUserListByUserIdList(List<Long> userIdList) {
		return userRepository.getUserListByUserIdList(userIdList);
	}
	@Override
	public void addUser(User user) {
		
	}
	@Override
	public Page<User> getUserList(User user, Pageable pageable) {
		
		return userRepository.findAll(UserSpecifications.queryByCondition(user), pageable);
	}
	
	@Override
	public Page<User> getUserListByUserIdNotNull(User user, Pageable pageable) {
		
		return userRepository.findAll(UserSpecifications.queryByConditionAndUserId(user), pageable);
	}
}
