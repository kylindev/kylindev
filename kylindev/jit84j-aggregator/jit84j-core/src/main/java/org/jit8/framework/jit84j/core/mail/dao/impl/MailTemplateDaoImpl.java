package org.jit8.framework.jit84j.core.mail.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailTemplateDao;
import org.jit8.framework.jit84j.core.mail.domain.MailTemplate;
import org.jit8.framework.jit84j.core.mail.repository.MailTemplateRepository;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MailTemplateDaoImpl extends GenericDaoImpl<MailTemplate, Long> implements MailTemplateDao {

	@Resource
	private MailTemplateRepository mailTemplateRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = mailTemplateRepository;
	}

	@Override
	public List<MailTemplate> getMailTemplateList(boolean disable, boolean deleted) {
		return mailTemplateRepository.getMailTemplateList(disable, deleted);
	}
}
