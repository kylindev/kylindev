package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StickyNoteCategorySpecialRepository extends GenericJpaRepository<StickyNoteCategorySpecial, Long>{

	@Query("from StickyNoteCategorySpecial o where o.disable=:disable and o.deleted=:deleted")
	List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from StickyNoteCategorySpecial o where o.categorySpecial.uniqueIdentifier in :categorySpecialIdList and o.deleted=false")
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(
			@Param("categorySpecialIdList")List<Long> categorySpecialIdList);
	
}
