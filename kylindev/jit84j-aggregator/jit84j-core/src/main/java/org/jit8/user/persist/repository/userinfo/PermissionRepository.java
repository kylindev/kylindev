package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface PermissionRepository extends GenericJpaRepository<Permission, Long>{

	@Query("from Permission o where o.deleted=false and o.resourceUrl=:resourceUrl")
	public Permission findPermissionByResourceUrl(@Param("resourceUrl")String resourceUrl);
	
	@Query("from Permission o where o.deleted=false and o.resourceUrl in :resourceUrls")
	public List<Permission> findPermissionByResourceUrls(@Param("resourceUrls")List<String> resourceUrls);
	
	@Query("from Permission o where o.deleted=false and o.code=:code")
	public Permission findPermissionByCode(@Param("code")String code);
	
	@Query("from Permission o where o.deleted=false")
	Page<Permission> getPermissionList(Pageable pageable);
	
	@Query("from Permission o where o.deleted=false and o.uniqueIdentifier=:id")
	public Permission fineById(@Param("id")Long id);
	
	public Permission findByFullUrl(String fullUrl);
	
	@Query("from Permission o where o.deleted=false and o.categoryId=:categoryId")
	public List<Permission> findPermissionListByCategoryId(Long categoryId);
	
	@Query("from Permission o where o.deleted=false and o.uniqueIdentifier in (:idList)")
	public List<Permission> findByIdList(@Param("idList")List<Long> idList);
	
}
