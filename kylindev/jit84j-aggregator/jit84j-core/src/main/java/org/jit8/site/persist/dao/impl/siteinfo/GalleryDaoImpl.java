package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.GalleryDao;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.persist.repository.siteinfo.GalleryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class GalleryDaoImpl extends GenericDaoImpl<Gallery, Long> implements GalleryDao {

	@Resource
	private GalleryRepository galleryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = galleryRepository;
	}

	@Override
	public List<Gallery> getGalleryList(boolean disable, boolean deleted) {
		return galleryRepository.getGalleryList(disable, deleted);
	}
}
