package org.jit8.user.persist.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.util.StringUtils;

public class CheckPasswordValidator implements ConstraintValidator<CheckPassword, User> {  
	  
    @Override  
    public void initialize(CheckPassword constraintAnnotation) {  
    }  
  
    @Override  
    public boolean isValid(User user, ConstraintValidatorContext context) {  
        if(user == null) {  
            return true;  
        }  
  
        //没有填密码  
        if(!StringUtils.hasText(user.getPassword())) {  
            context.disableDefaultConstraintViolation();  
            context.buildConstraintViolationWithTemplate("{user.password.empty}")  
                    .addNode("password")  
                    .addConstraintViolation();  
            return false;  
        }  
  
        if(!StringUtils.hasText(user.getConfirmPassword())) {  
            context.disableDefaultConstraintViolation();  
            context.buildConstraintViolationWithTemplate("{user.password.confirm.empty}")  
                    .addNode("confirmPassword")  
                    .addConstraintViolation();  
            return false;  
        }  
  
        //两次密码不一样  
        if (!user.getPassword().trim().equals(user.getConfirmPassword().trim())) {  
            context.disableDefaultConstraintViolation();  
            context.buildConstraintViolationWithTemplate("{user.password.confirm.error}")  
                    .addNode("confirmPassword")  
                    .addConstraintViolation();  
            return false;  
        }  
        return true;  
    }  
}  
