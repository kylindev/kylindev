package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface GalleryRepository extends GenericJpaRepository<Gallery, Long>{

	@Query("from Gallery o where o.disable=:disable and o.deleted=:deleted")
	List<Gallery> getGalleryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

}
