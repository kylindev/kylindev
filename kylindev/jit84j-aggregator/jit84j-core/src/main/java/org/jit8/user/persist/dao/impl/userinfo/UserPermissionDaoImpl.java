package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.UserPermissionDao;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.jit8.user.persist.repository.userinfo.UserPermissionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserPermissionDaoImpl extends GenericDaoImpl<UserPermission, Long> implements UserPermissionDao {

	@Resource
	private UserPermissionRepository userPermissionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = userPermissionRepository;
	}

	@Override
	public List<UserPermission> findByPermissionId(Long permissionId) {
		return userPermissionRepository.findByPermissionId(permissionId);
	}

}
