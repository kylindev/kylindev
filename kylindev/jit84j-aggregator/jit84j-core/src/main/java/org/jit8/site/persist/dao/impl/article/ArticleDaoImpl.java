package org.jit8.site.persist.dao.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.article.ArticleDao;
import org.jit8.site.persist.domain.article.Article;
import org.jit8.site.persist.repository.article.ArticleRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ArticleDaoImpl extends GenericDaoImpl<Article, Long> implements ArticleDao {

	@Resource
	private ArticleRepository articleRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = articleRepository;
	}

	@Override
	public List<Article> getArticleList(boolean disable, boolean deleted) {
		return articleRepository.getArticleList(disable, deleted);
	}

	@Override
	public Page<Article> getArticleList(boolean disable, boolean deleted,
			Pageable pageable) {
		return articleRepository.getArticleList(disable, deleted,pageable);
	}
}
