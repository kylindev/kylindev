package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.persist.domain.siteinfo.FileMime_;
import org.springframework.data.jpa.domain.Specification;



public class FileMimeSpecifications {

	public static Specification<FileMime> queryByCondition(final FileMime fileMime) {
		return new Specification<FileMime>() {
			public Predicate toPredicate(Root<FileMime> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				String mime = StringUtils.trim(fileMime.getMime());
				String name = StringUtils.trim(fileMime.getName());
				String description = StringUtils.trim(fileMime.getDescription());
				String suffix = StringUtils.trim(fileMime.getSuffix());
				
				if(StringUtils.isNotEmpty(mime)){
					predicateList.add(builder.like(root.get(FileMime_.mime ), Specifications.PERCENT + mime + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(name)){
					predicateList.add(builder.like(root.get(FileMime_.name ), Specifications.PERCENT + name + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(suffix)){
					predicateList.add(builder.like(root.get(FileMime_.suffix ), Specifications.PERCENT + suffix + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(description)){
					predicateList.add(builder.like(root.get(FileMime_.description ), Specifications.PERCENT + description + Specifications.PERCENT));
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
