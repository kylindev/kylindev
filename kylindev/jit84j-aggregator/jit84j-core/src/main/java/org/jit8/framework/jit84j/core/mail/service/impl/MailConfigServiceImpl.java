package org.jit8.framework.jit84j.core.mail.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailConfigDao;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.service.MailConfigService;
import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class MailConfigServiceImpl extends GenericServiceImpl<MailConfig, Long> implements MailConfigService {

	@Resource
	private MailConfigDao mailConfigDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = mailConfigDao;
	}


	@Override
	public List<MailConfig> getMailConfigList(boolean disable, boolean deleted) {
		return mailConfigDao.getMailConfigList(disable, deleted);
	}


	@Override
	public MailConfig findByType(String type) {
		return mailConfigDao.findByType(type);
	}
	
}
