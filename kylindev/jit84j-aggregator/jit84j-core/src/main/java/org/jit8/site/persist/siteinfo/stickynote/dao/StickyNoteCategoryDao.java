package org.jit8.site.persist.siteinfo.stickynote.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;

public interface StickyNoteCategoryDao extends GenericDao<StickyNoteCategory, Long> {

	public List<StickyNoteCategory> getStickyNoteCategoryList(boolean disable, boolean deleted) ;
	
	public StickyNoteCategory findByCode(String code);
	
	public List<StickyNoteCategory> findByIds(List<Long> idList);
}
