package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;

public interface SiteInfoDao extends GenericDao<SiteInfo, Long> {

	public List<SiteInfo> getSiteInfoList(boolean disable, boolean deleted) ;
	
	public SiteInfo getSiteInfoByType(String type);
	
}
