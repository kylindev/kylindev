package org.jit8.site.persist.siteinfo.stickynote.repository.spec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial_;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote_;
import org.springframework.data.jpa.domain.Specification;



public class StickyNoteSpecifications {

	public static Specification<StickyNote> queryByCondition(final StickyNote stickyNote) {
		return new Specification<StickyNote>() {
			public Predicate toPredicate(Root<StickyNote> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				predicateList.add(builder.equal(root.get(StickyNote_.deleted ), false));
				
				if(null != stickyNote){
					
					String title = StringUtils.trim(stickyNote.getTitle());
					String content = StringUtils.trim(stickyNote.getContent());
					String summary = StringUtils.trim(stickyNote.getSummary());
					
					String author = StringUtils.trim(stickyNote.getAuthor());
					String imagePath = StringUtils.trim(stickyNote.getImagePath());
					
					if(StringUtils.isNotEmpty(title)){
						predicateList.add(builder.like(root.get(StickyNote_.title), Specifications.PERCENT + title + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(summary)){
						predicateList.add(builder.like(root.get(StickyNote_.summary), Specifications.PERCENT + summary + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(author)){
						predicateList.add(builder.like(root.get(StickyNote_.author), Specifications.PERCENT + author + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(imagePath)){
						predicateList.add(builder.like(root.get(StickyNote_.imagePath), Specifications.PERCENT + imagePath + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(content)){
						predicateList.add(builder.like(root.get(StickyNote_.content), Specifications.PERCENT + content + Specifications.PERCENT));
					}
					
					Integer minLevel = stickyNote.getMinLevel();
					
					Integer maxLevel = stickyNote.getMaxLevel();
					
					Date minPushlishTime = stickyNote.getMinPublishTime();
					
					Date maxPushlishTime = stickyNote.getMaxPublishTime();
					
					if(minLevel != null){
						predicateList.add(builder.ge(root.get(StickyNote_.level), minLevel));
					}
					
					if(null != maxLevel && maxLevel > 0){
						predicateList.add(builder.le(root.get(StickyNote_.level), maxLevel));
					}
					
					if(null != minPushlishTime){
						predicateList.add(builder.greaterThan(root.get(StickyNote_.publishTime), minPushlishTime));
					}
					
					if(null != maxPushlishTime){
						predicateList.add(builder.lessThan(root.get(StickyNote_.publishTime), maxPushlishTime));
					}
					
					List<Long> categorySpecialIdList = stickyNote.getCategorySpecialIdList();
					
					Join<CategorySpecial, StickyNoteCategory> stickyJoin = root.join(StickyNote_.stickyNoteCategorySpecialSet , JoinType.LEFT)
							.join(StickyNoteCategorySpecial_.categorySpecial,JoinType.INNER).join(CategorySpecial_.category,JoinType.INNER);
					if(null != categorySpecialIdList && categorySpecialIdList.size() > 0 ){
						predicateList.add(stickyJoin.get(CategorySpecial_.uniqueIdentifier).in(categorySpecialIdList));
					}
					
					Long specialAreaId = stickyNote.getSpecialAreaId();
					if(null != specialAreaId){
						builder.equal(stickyJoin.get(SpecialArea_.uniqueIdentifier),specialAreaId);
					}
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
