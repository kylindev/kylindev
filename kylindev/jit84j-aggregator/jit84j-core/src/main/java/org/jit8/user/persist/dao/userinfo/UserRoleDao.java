package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.UserRole;

public interface UserRoleDao extends GenericDao<UserRole, Long> {

	public List<UserRole> findUserRoleList(Long userId);
	
	public void removeByIdList(List<Long> userIdList);
}
