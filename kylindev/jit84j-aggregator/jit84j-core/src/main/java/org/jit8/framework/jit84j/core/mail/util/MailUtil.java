package org.jit8.framework.jit84j.core.mail.util;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeUtility;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailAttachment;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.katesoft.scale4j.log.LogFactory;


public class MailUtil {

	private static final com.katesoft.scale4j.log.Logger LOGGER = LogFactory.getLogger(MailUtil.class);
	
	public MailUtil() {
	}
	public static void main(String[] args) throws MessagingException {
		MailMessage mailMessage = new MailMessage();
		MailConfig mailConfig = new MailConfig();
		//mailConfig.setHost("smtp.sohu.com");    
		//mailConfig.setUsername("15915434241@sohu.com");    
		mailConfig.setHost("smtp.163.com");    
		//mailConfig.setHost("smtp.sina.com");    
		mailConfig.setUsername("bruceyang_it@163.com");    
		mailConfig.setPassword("yanglina"); 
		mailMessage.setReciever("460416126@qq.com");
		mailMessage.setCc("381121369@qq.com");
		mailMessage.setBcc("275060435@qq.com");
		List<String> toList = new ArrayList<String>();
		toList.add("381121369@qq.com");
		toList.add("460416126@qq.com");
		//toList.add("275060435@qq.com");
		//mailMessage.setToList(toList);
		
		List<String> ccList = new ArrayList<String>();
		ccList.add("993202834@qq.com");
		ccList.add("934089458@qq.com");
		mailMessage.setCcList(ccList);
		List<String> bccList = new ArrayList<String>();
		bccList.add("739135765@qq.com");
		bccList.add("275060435@qq.com");
		//mailMessage.setBccList(bccList);
		//381121369
		//993202834
		//739135765
		//460416126 
		//934089458
		//mailConfig.setFrom("15915434241@sohu.com");    
		mailConfig.setSender("bruceyang_it@163.com");    
		mailMessage.setSubject("密码重置");  
		mailMessage.setContent("<html><body><h1>明天下午五点，吃烧烤，大家准备参加</h1>"
				+""
				+ "http://www.kylinboy.org/ <br/><img src='http://images2.baihe.com/images/baihe_new/images/skin_index/20131118index_focus_02.jpg' alt='logo...'/> </body></html>");    
	      
		sendMail(mailConfig,mailMessage);
		System.out.println(mailMessage.getLogInfo());
	}
	
	 /**   
	  *用spring mail 发送邮件,依赖jar：spring.jar，activation.jar，mail.jar    
	  */    
	     
	  public static MailMessage sendMail(MailConfig mailConfig,MailMessage mailMessage) throws MessagingException {    
	          JavaMailSenderImpl senderImpl = new JavaMailSenderImpl();    
	          
	          // 建立HTML邮件消息    
	          MimeMessage mimeMessage = senderImpl.createMimeMessage();    
	          // true表示开始附件模式    
	          MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "utf-8");    
	      
	          if(mailConfig.isUseClientFrom()){
	        	  senderImpl.setHost(mailMessage.getHost());    
		          senderImpl.setUsername(mailMessage.getUsername());    
		          senderImpl.setPassword(mailMessage.getPassword()); 
		          int port = mailMessage.getPort();
		          
		          String protocol = mailMessage.getProtocol();
		          
		          senderImpl.setPort(handlePort(port));
		          senderImpl.setProtocol(handleProtocol(protocol));
		          
		          String from = StringUtils.trimToEmpty(mailMessage.getSender());
		          if(StringUtils.isNotEmpty(from)){
		        	  int position = from.indexOf("@");
		        	  if(position > 0){
		        		  String domainName = from.substring(position);
		        		  if(StringUtils.isNotEmpty(domainName)){
		        			 String host = mailMessage.getHost();
		        			 if(host.indexOf(domainName)>0){
		        				 String error = "invalid email sender ["+from+"], donot macth the host["+host+"]";
		        				 LOGGER.error(error);
		        				 throw new MessagingException(error);
		        			 }
		        		  }
		        	  }else{
		        		  String error = "invalid email sender ["+from+"], missing '@' .";
		        		  LOGGER.error(error);
		        		  throw new MessagingException(error);
		        	  }
		          }
		          messageHelper.setFrom(from);
	          }else{
	              // 设定mail server    
		          senderImpl.setHost(mailConfig.getHost());    
		          senderImpl.setUsername(mailConfig.getUsername());    
		          senderImpl.setPassword(mailConfig.getPassword()); 
		          int port = handlePort(mailConfig.getPort());
		          senderImpl.setPort(port);
		          String protocol = handleProtocol(mailConfig.getProtocol());
		          senderImpl.setProtocol(protocol);
		          messageHelper.setFrom(mailConfig.getSender());
		          mailMessage.setSender(mailConfig.getSender());
		          mailMessage.setUsername(mailConfig.getUsername());
		          mailMessage.setPort(port);
		          mailMessage.setProtocol(protocol);
		          mailMessage.setHost(mailConfig.getHost());
		          
	          }
	          
	          
	          
	          // 设置收件人，寄件人    
	          String to = StringUtils.trimToEmpty(mailMessage.getReciever());
	          if(StringUtils.isNotEmpty(to)){
	        	  messageHelper.setTo(to);
	          }
	          
	          String bcc = StringUtils.trimToEmpty(mailMessage.getBcc());
	          if(StringUtils.isNotEmpty(bcc)){
	        	  messageHelper.setBcc(bcc);
	          }
	          
	          String cc = StringUtils.trimToEmpty(mailMessage.getCc());
	          if(StringUtils.isNotEmpty(cc)){
	        	  messageHelper.setCc(cc);
	          }
	          List<String> toList = mailMessage.getToList();
	          if(CollectionUtils.isNotEmpty(toList)){
	        	  if(!toList.contains(to) && StringUtils.isNotEmpty(to)){
	        		  toList.add(to);
	        	  }
	        	  messageHelper.setTo(toList.toArray(new String[toList.size()]));
	          }
	          
	          List<String> ccList = mailMessage.getCcList();
	          if(CollectionUtils.isNotEmpty(ccList) && StringUtils.isNotEmpty(cc)){
	        	  if(!ccList.contains(cc)){
	        		  ccList.add(cc);
	        	  }
	        	  messageHelper.setCc(ccList.toArray(new String[ccList.size()]));
	          }
	          
	          List<String> bccList = mailMessage.getBccList();
	          if(CollectionUtils.isNotEmpty(bccList)){
	        	  if(!bccList.contains(bcc) && StringUtils.isNotEmpty(bcc)){
	        		  bccList.add(bcc);
	        	  }
	        	  messageHelper.setBcc(bccList.toArray(new String[bccList.size()]));
	          }
	          
	          messageHelper.setSubject(mailMessage.getSubject());    
	          // true 表示启动HTML格式的邮件    
	          messageHelper.setText(mailMessage.getContent(), true);  
	      
	          List<MailAttachment> attachments = mailMessage.getAttachments();
	          if(null != attachments && attachments.size()>0){
	        	  for(MailAttachment mailAttachment : attachments){
	        		  File file = mailAttachment.getFile();
	        		  if(null != file){
	        			  try {    
	        	              //附件名有中文可能出现乱码    
	        				  messageHelper.addAttachment(MimeUtility.encodeWord(file.getName()), file);     
	        	          } catch (UnsupportedEncodingException e) {
	        	        	  String error = "encoding fileName error, fileName=" + file.getName();
			        		  LOGGER.error(error);
	        	              throw new MessagingException(error,e);    
	        	          }
	        			 
	        		  }else{
	        			  String path = StringUtils.trimToEmpty(mailAttachment.getPath());
	        			  if(StringUtils.isNotEmpty(path)){
	        				  FileSystemResource file1 = new FileSystemResource(new File(path)); 
	        				  if(null != file1 && file1.exists()){
	        					  String info = "";
	        					  try {
	        						info = "attchment name : " + file1.getFilename() + " size: " + file1.contentLength();
									LOGGER.info(info);
									 messageHelper.addAttachment("logo.jpg", file1);  
	        					  } catch (IOException e) {
	        						  LOGGER.error(e);
	        						  throw new MessagingException(info,e);    
								}
	        					 
	        				  }
	        			  }
	        		  }
	        	  }
	          }
	              
	          // 发送邮件    
	          senderImpl.send(mimeMessage);    
	          
	          StringBuilder sb = new StringBuilder();
	          sb.append("邮件【").append(mimeMessage.getSubject())
	          .append("】由【").append(mailMessage.getSender())
	          .append("】 发送至 【").append(mailMessage.getAllReciever()).append("】 发送成功.....");
	          mailMessage.setLogInfo(sb.toString());
	          LOGGER.info(sb.toString());   
	          
	          return mailMessage;
	      
	      } 
	  
	  
	  private static int handlePort(int port){
		  if(port<=0){
        	  port=MailConfig.DEFAULT_PORT;
          }
		  return port;
	  }
	  
	  private static String handleProtocol(String protocol){
		  if(StringUtils.isEmpty(protocol)){
        	  protocol = MailConfig.DEFAULT_PROTOCOL;
          }
		  return protocol;
	  }
}
