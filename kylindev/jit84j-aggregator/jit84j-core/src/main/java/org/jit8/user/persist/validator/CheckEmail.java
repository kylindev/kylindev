package org.jit8.user.persist.validator;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE})  
@Retention(RetentionPolicy.RUNTIME)  
//指定验证器  
@Constraint(validatedBy = CheckEmailValidator.class)  
@Documented  
public @interface CheckEmail {  
  
    //默认错误消息  
    String message() default "";  
  
    //分组  
    Class<?>[] groups() default { };  
  
    //负载  
    Class<? extends Payload>[] payload() default { };  
  
    //指定多个时使用  
    @Target({ ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER, ElementType.ANNOTATION_TYPE })  
    @Retention(RetentionPolicy.RUNTIME)  
    @Documented  
    @interface List {  
        CheckEmail[] value();  
    }  
}  
