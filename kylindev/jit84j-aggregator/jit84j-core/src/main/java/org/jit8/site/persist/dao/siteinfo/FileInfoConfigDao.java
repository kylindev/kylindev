package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;

public interface FileInfoConfigDao extends GenericDao<FileInfoConfig, Long> {

	public List<FileInfoConfig> getFileInfoConfigList(boolean disable, boolean deleted) ;
}
