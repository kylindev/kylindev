package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.springframework.data.jpa.repository.Query;

public interface StickyNoteConfigRepository extends GenericJpaRepository<StickyNoteConfig, Long>{

	@Query("from StickyNoteConfig o where o.disable=:disable and o.deleted=:deleted")
	public List<StickyNoteConfig> getStickyNoteConfigList(boolean disable, boolean deleted);
}
