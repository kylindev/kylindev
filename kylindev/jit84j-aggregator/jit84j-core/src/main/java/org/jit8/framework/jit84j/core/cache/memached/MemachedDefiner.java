package org.jit8.framework.jit84j.core.cache.memached;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.core.web.util.MemcachedUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class MemachedDefiner {

	@Resource
	protected MemachedContext memachedContext;
	
	private static MemcachedClient memcachedClient;
	
	private static ScheduledExecutorService scheduledThreadPool = Executors.newScheduledThreadPool(MemachedDefinition.SCHEDULED_THREAD_POOL);

	private static final Logger LOGGER  = LoggerFactory.getLogger(MemachedDefiner.class);
	
	
	public MemachedDefiner() {
	}
	
	@SuppressWarnings("static-access")
	@PostConstruct
	protected void init(){
		this.memcachedClient = memachedContext.getMemcachedClient();
	}

	public static MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}
	
	public static void set(MemachedInit memached,final Object value){
		if(null != memcachedClient && value != null){
				putValueIntoMemached(memached, value);
		}
	}

	/**
	 * @param memached
	 * @param value
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws MemcachedException
	 */
	private static void putValueIntoMemached(MemachedInit memached,
			final Object value)  {
		final String key  = memached.value();
		LOGGER.info("get from memcached begin, key=" + key);
		Object object = MemcachedUtil.get(key,memcachedClient);
		LOGGER.info("get from memcached end, key=" + key);
		
		//把当前方法的执行结果放入缓存
		
		boolean autoRefresh = memached.autoRefresh();
		
		final int expiresTime = memached.expiresTime();
		
		final long delayRefreshTime = memached.delayRefreshTime();
		
		final long period = memached.period();
		
		boolean repeated = memached.repeated();
		
		putValueIntoMemcached(value, key, object, autoRefresh, expiresTime,
				delayRefreshTime, period, repeated);
	}

	/**
	 * @param value
	 * @param key
	 * @param object
	 * @param autoRefresh
	 * @param expiresTime
	 * @param delayRefreshTime
	 * @param period
	 * @param repeated
	 */
	private static void putValueIntoMemcached(final Object value,
			final String key, Object object, boolean autoRefresh,
			final int expiresTime, final long delayRefreshTime,
			final long period, boolean repeated) {
		if(null == object){
			LOGGER.info("stored into memcached begin, key=" + key);
			//boolean stored = memcachedClient.add(key, expiresTime, value);
			boolean stored = MemcachedUtil.set(key, expiresTime, value, memcachedClient);
			LOGGER.info("stored into memcached end, key=" + key + " : "+ stored);
			if(repeated){
		    	scheduledThreadPool.scheduleAtFixedRate(new Runnable() {  
		            public void run() {  
            			LOGGER.info("repeated: stored into memcached begin, key=" + key);
            			boolean stored = MemcachedUtil.set(key, expiresTime, value, memcachedClient);
						//boolean stored = memcachedClient.add(key, expiresTime, value);
						LOGGER.info("repeated: stored into memcached end, key=" + key + " : "+ stored + " expiresTime:"+expiresTime + " period:" + period + " delayRefreshTime:" + delayRefreshTime);
		            }  
		        }, delayRefreshTime, period,TimeUnit.MILLISECONDS);  
		    }
		}else{
			if(autoRefresh){
				LOGGER.info("refresh into memcached begin, key=" + key);
				MemcachedUtil.delete(key, memcachedClient);
				//memcachedClient.delete(key);
				//boolean stored = memcachedClient.set(key, expiresTime, value);
				boolean stored = MemcachedUtil.set(key, expiresTime, value, memcachedClient);
				LOGGER.info("refresh into memcached end, key=" + key + " : "+ stored);
			}
		}
	}
	/**
	 * @param memached
	 * @param value
	 * @throws TimeoutException
	 * @throws InterruptedException
	 * @throws MemcachedException
	 */
	private static void putValueIntoMemached(MemachedModel memachedModel,
			final Object value) {
		final String key  = memachedModel.getKey();
		LOGGER.info("get from memcached begin, key=" + key);
		Object object = MemcachedUtil.get(key,memcachedClient);
		LOGGER.info("get from memcached end, key=" + key);
		
		//把当前方法的执行结果放入缓存
		
		boolean autoRefresh = memachedModel.isAutoRefresh();
		
		final int expiresTime = memachedModel.getExpiresTime();
		
		final long delayRefreshTime = memachedModel.getDelayRefreshTime();
		
		final long period = memachedModel.getPeriod();
		
		boolean repeated = memachedModel.isRepeated();
		
		putValueIntoMemcached(value, key, object, autoRefresh, expiresTime,
				delayRefreshTime, period, repeated);
	}
	
	public static Object get(MemachedModel memachedModel,boolean refresh){
		//Assert.assertNotNull(memachedModel);
		String key = memachedModel.getKey();
		if(StringUtils.isNotEmpty(key)){
			try{
				Object value = null;
				if(!refresh){
					value = MemcachedUtil.get(key,memcachedClient);
				}
				if(null == value || refresh){
					LOGGER.info("refresh memcached begin,  key = ", key);
					Method method = memachedModel.getTargetMethod();
					Object targetBean = ApplicationContext.getBean(memachedModel.getTargetClazz());
					memachedModel.setTargetBean(targetBean);
					value = method.invoke(targetBean, null);
					//Memached targetMemached = memachedModel.getTargetMemached();
					putValueIntoMemached(memachedModel, value);
					LOGGER.info("refresh memcached end,  key = ", key);
				}
				
				return value;
			} catch (IllegalArgumentException e) {
				LOGGER.error("IllegalArgumentException:", e);
			} catch (IllegalAccessException e) {
				LOGGER.error("IllegalAccessException:", e);
			} catch (InvocationTargetException e) {
				LOGGER.error("InvocationTargetException:", e);
			} 
		}
		return null;
	}
	
	public static void set(MemachedUpdate memachedUpdate,Object[] values){
		if(null != memcachedClient && null != memachedUpdate &&  !memachedUpdate.targetClazz().isAssignableFrom(MemachedUpdate.class)){
			try {
				final String key = memachedUpdate.value();
				LOGGER.info("update memcached begin, key = " + key);
				
				Class<?> clazz = memachedUpdate.targetClazz();
				
				String methodName = memachedUpdate.targetMethod();
				
				Object bean = ApplicationContext.getBean(clazz);
				
				Method method = clazz.getMethod(methodName,memachedUpdate.targetMethodParams());
				
				boolean autoRefresh = memachedUpdate.autoRefresh();
			        
			    final int expiresTime = memachedUpdate.expiresTime();
			        
			    final long delayRefreshTime = memachedUpdate.delayRefreshTime();
				
				if( null != methodName){
					final Object result = method.invoke(bean,values);
					LOGGER.info("refresh memcached begin, key=" + key + " will delay " + delayRefreshTime + " ms");
					if(autoRefresh){
						scheduledThreadPool.schedule(new Runnable() {  
			                public void run() {  
			                	long begin = System.currentTimeMillis();
			                	MemcachedUtil.refresh(key, expiresTime, result, memcachedClient);
			                	long end = System.currentTimeMillis();
			                	LOGGER.info("refresh memcached end, key=" + key + " in " + (end-begin) + " ms");
			                }  
			            }, delayRefreshTime, TimeUnit.MILLISECONDS);
					}
				}
				LOGGER.info("update memcached end, key=" + key);
			} catch (SecurityException e) {
				LOGGER.error("SecurityException:", e);
			} catch (NoSuchMethodException e) {
				LOGGER.error("NoSuchMethodException:", e);
			} catch (IllegalArgumentException e) {
				LOGGER.error("IllegalArgumentException:", e);
			} catch (IllegalAccessException e) {
				LOGGER.error("IllegalAccessException:", e);
			} catch (InvocationTargetException e) {
				LOGGER.error("InvocationTargetException:", e);
			}
		}
	}
	
	public static boolean touch(String key, int exp){
		if(null != memcachedClient ){
			return MemcachedUtil.touch(key, exp, memcachedClient);
		}
		return false;
	}
	
	public static boolean delete(String key){
		if(null != memcachedClient ){
			return MemcachedUtil.delete(key, memcachedClient);
		}
		return false;
	}

}
