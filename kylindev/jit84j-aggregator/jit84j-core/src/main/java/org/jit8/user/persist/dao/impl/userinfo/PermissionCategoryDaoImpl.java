package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.PermissionCategoryDao;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.jit8.user.persist.repository.userinfo.PermissionCategoryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class PermissionCategoryDaoImpl extends GenericDaoImpl<PmnCategory, Long> implements PermissionCategoryDao {

	@Resource
	private PermissionCategoryRepository permissionCategoryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = permissionCategoryRepository;
	}

	@Override
	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable) {
		
		return permissionCategoryRepository.getPermissionCategoryList( pageable);
	}

	@Override
	public void removeAllPermissionFromCategory(
			Long permissionId) {
		 permissionCategoryRepository.removeAllPermissionFromCategory(permissionId);
	}

	
}
