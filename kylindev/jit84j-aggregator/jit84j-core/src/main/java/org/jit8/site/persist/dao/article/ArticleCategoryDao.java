package org.jit8.site.persist.dao.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.article.ArticleCategory;

public interface ArticleCategoryDao extends GenericDao<ArticleCategory, Long> {

	public List<ArticleCategory> getArticleCategoryList(boolean disable, boolean deleted) ;
	
	
	public List<ArticleCategory> getArticleCategoryListByArticleId(boolean disable,boolean deleted,  Long articleId);
}
