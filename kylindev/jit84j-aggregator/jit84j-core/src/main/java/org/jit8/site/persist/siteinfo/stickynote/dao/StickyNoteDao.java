package org.jit8.site.persist.siteinfo.stickynote.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface StickyNoteDao extends GenericDao<StickyNote, Long> {

	public List<StickyNote> getStickyNoteList(boolean disable, boolean deleted) ;
	
	public Page<StickyNote> findAll(StickyNote stickyNote,
			Pageable pageable);
}
