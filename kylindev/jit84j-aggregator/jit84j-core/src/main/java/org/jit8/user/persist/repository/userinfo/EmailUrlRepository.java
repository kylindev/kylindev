package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface EmailUrlRepository extends GenericJpaRepository<EmailUrl, Long>{

	@Query("from EmailUrl o where o.urlCode=:urlCode and o.deleted=false")
	public EmailUrl findByUrlCode(@Param("urlCode")String urlCode);
}
