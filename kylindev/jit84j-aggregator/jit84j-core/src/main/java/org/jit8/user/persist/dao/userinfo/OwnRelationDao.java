package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.springframework.data.repository.query.Param;

public interface OwnRelationDao extends GenericDao<OwnRelation, Long> {

	List<OwnRelation> getOwnRelationList(boolean disable,boolean deleted);
}
