package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface DeveloperRepository extends GenericJpaRepository<Developer, Long>{

	@Query("from Developer o where o.deleted=false")
	Page<Developer> getDeveloperList(Pageable pageable);
	
	@Query("from Developer o where o.deleted=false and o.userId.userId=:userId")
	public Developer findByUserId(@Param("userId")Long userId);
}
