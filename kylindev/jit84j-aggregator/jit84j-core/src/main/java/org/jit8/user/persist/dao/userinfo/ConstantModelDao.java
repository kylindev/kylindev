package org.jit8.user.persist.dao.userinfo;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ConstantModelDao extends GenericDao<ConstantModel, Long> {

	public Page<ConstantModel> getConstantModelList(Pageable pageable);
	
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,Pageable pageable);
	
	
}
