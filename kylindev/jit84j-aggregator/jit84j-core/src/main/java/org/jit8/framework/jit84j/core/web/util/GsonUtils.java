package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GsonUtils implements Serializable {

	private static final long serialVersionUID = -7975536577655202148L;
	
	private static final Logger logger = LoggerFactory.getLogger(GsonUtils.class);

	public static String getJsonFromObject(Object obj,String dateFormat){
		GsonBuilder gb = new GsonBuilder();
		if(StringUtils.isEmpty(dateFormat)){
			dateFormat = "yyyy-MM-dd HH:mm:ss";
		}
		gb.setDateFormat(dateFormat);
		Gson gson = gb.excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(obj);
		logger.debug(json);
		return json;
	}
	
	public static String getJsonFromMap(Map<String,Object> map){
		return getJsonFromObject(map,null);
	}
	
	public static <T> String getJsonFromList(List<T> list,String dateFormat){
		if(null != list && list.size()>0){
			GsonBuilder gb = new GsonBuilder();
			if(StringUtils.isEmpty(dateFormat)){
				dateFormat = "yyyy-MM-dd HH:mm:ss";
			}
			gb.setDateFormat(dateFormat);
			Gson gson = gb.excludeFieldsWithoutExposeAnnotation().create();
			List<T> dest = new ArrayList<T>(list.size());
			for(T t : list){
				dest.add(t);
			}
			String json = gson.toJson(dest);
			logger.debug(json);
			return json;
		}
		return "";
	}
	
	public static <T> String getJsonFromList(List<T> list){
		return getJsonFromList(list,null);
	}
     
}
