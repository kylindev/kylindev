package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.jit8.site.persist.siteinfo.stickynote.repository.StickyNoteRepository;
import org.jit8.site.persist.siteinfo.stickynote.repository.spec.StickyNoteSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class StickyNoteDaoImpl extends GenericDaoImpl<StickyNote, Long> implements StickyNoteDao {

	@Resource
	private StickyNoteRepository stickyNoteRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = stickyNoteRepository;
	}

	@Override
	public List<StickyNote> getStickyNoteList(boolean disable, boolean deleted) {
		return stickyNoteRepository.getStickyNoteList(disable, deleted);
	}
	
	@Override
	public Page<StickyNote> findAll(StickyNote stickyNote,
			Pageable pageable){
		
		Page<StickyNote> page = stickyNoteRepository.findAll(StickyNoteSpecifications.queryByCondition(stickyNote), pageable);
		
		return page;
	}
			
}
