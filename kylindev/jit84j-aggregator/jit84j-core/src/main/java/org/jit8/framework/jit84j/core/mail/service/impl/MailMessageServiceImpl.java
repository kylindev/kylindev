package org.jit8.framework.jit84j.core.mail.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailMessageDao;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.mail.service.MailMessageService;
import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class MailMessageServiceImpl extends GenericServiceImpl<MailMessage, Long> implements MailMessageService {

	@Resource
	private MailMessageDao mailMessageDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = mailMessageDao;
	}


	@Override
	public List<MailMessage> getMailMessageList(boolean disable, boolean deleted) {
		return mailMessageDao.getMailMessageList(disable, deleted);
	}
	
	
	public boolean sendMailMessage(MailMessage mailMessage){
		
		return false;
	}
	
}
