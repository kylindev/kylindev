package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteCategorySpecialDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.repository.StickyNoteCategorySpecialRepository;
import org.jit8.site.persist.siteinfo.stickynote.repository.spec.StickyNoteCategorySpecialSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class StickyNoteCategorySpecialDaoImpl extends GenericDaoImpl<StickyNoteCategorySpecial, Long> implements StickyNoteCategorySpecialDao {

	@Resource
	private StickyNoteCategorySpecialRepository stickyNoteCategorySpecialRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = stickyNoteCategorySpecialRepository;
	}

	@Override
	public List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList(boolean disable, boolean deleted) {
		return stickyNoteCategorySpecialRepository.getStickyNoteCategorySpecialList(disable, deleted);
	}

	@Override
	public Page<StickyNoteCategorySpecial> findAll(StickyNoteCategorySpecial stickyNoteCategorySpecial, Pageable pageable) {
		return stickyNoteCategorySpecialRepository.findAll(StickyNoteCategorySpecialSpecifications.queryByCondition(stickyNoteCategorySpecial),pageable);
	}

	@Override
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList) {
		return stickyNoteCategorySpecialRepository.getByCategorySpecialIdList(categorySpecialIdList);
	}
	
}
