package org.jit8.framework.jit84j.core.mail.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailConfigDao;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.repository.MailConfigRepository;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MailConfigDaoImpl extends GenericDaoImpl<MailConfig, Long> implements MailConfigDao {

	@Resource
	private MailConfigRepository mailConfigRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = mailConfigRepository;
	}

	@Override
	public List<MailConfig> getMailConfigList(boolean disable, boolean deleted) {
		return mailConfigRepository.getMailConfigList(disable, deleted);
	}

	@Override
	public MailConfig findByType(String type) {
		return mailConfigRepository.findByType(type);
	}
}
