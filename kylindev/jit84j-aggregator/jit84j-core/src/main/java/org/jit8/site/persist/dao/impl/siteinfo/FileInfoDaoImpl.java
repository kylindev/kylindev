package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileInfoDao;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.jit8.site.persist.repository.siteinfo.FileInfoRepository;
import org.jit8.user.persist.domain.userinfo.spec.FileInfoSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class FileInfoDaoImpl extends GenericDaoImpl<FileInfo, Long> implements FileInfoDao {

	@Resource
	private FileInfoRepository fileInfoRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileInfoRepository;
	}

	@Override
	public List<FileInfo> getFileInfoList(boolean disable, boolean deleted) {
		return fileInfoRepository.getFileInfoList(disable, deleted);
	}

	@Override
	public FileInfo findByIdAndOwner(long id, long owner) {
		return fileInfoRepository.findByIdAndOwner(id,owner);
	}

	@Override
	public Page<FileInfo> findAll(FileInfo fileInfo, Pageable pageable) {
		Page<FileInfo> page = fileInfoRepository.findAll(FileInfoSpecifications.queryByCondition(fileInfo), pageable);
		return page;
	}
}
