package org.jit8.framework.jit84j.core.init.test.service;

import org.jit8.framework.jit84j.core.init.test.model.Home;

public interface HomeService {

	public void save(Home home);
}
