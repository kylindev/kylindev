package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PermissionDao extends GenericDao<Permission, Long> {

	public Permission findPermissionByResourceUrl(String resourceUrl);
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls);
	
	public Permission findPermissionByCode(String code);
	
	public Page<Permission> getPermissionList(Pageable pageable);
	
	public Page<Permission> getPermissionList(Permission permission,
			Pageable pageable);
	
	public Permission fineById(Long id);
	
	public List<Permission> findByIdList(List<Long> idList);
	
	public Permission findByFullUrl(String fullUrl);
	
	public List<Permission> findPermissionListByCategoryId(Long categoryId);
}
