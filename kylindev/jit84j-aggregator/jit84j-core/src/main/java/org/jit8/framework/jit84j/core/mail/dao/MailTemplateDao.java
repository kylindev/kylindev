package org.jit8.framework.jit84j.core.mail.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailTemplate;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;

public interface MailTemplateDao extends GenericDao<MailTemplate, Long>{

	List<MailTemplate> getMailTemplateList(boolean disable,boolean deleted);

}
