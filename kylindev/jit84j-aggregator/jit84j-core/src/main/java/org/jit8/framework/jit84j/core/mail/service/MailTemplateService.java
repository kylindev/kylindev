package org.jit8.framework.jit84j.core.mail.service;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailTemplate;
import org.jit8.framework.jit84j.core.service.GenericService;

public interface MailTemplateService extends GenericService<MailTemplate, Long>{

	List<MailTemplate> getMailTemplateList(boolean disable,boolean deleted);

}
