package org.jit8.framework.jit84j.core.persistent.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.MappedSuperclass;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

@MappedSuperclass
@Access(AccessType.FIELD)
public class AbstractBasicPersistentEntity extends AbstractPersistentEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -852596336601478265L;
	
	private Long ownerId;

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}


}
