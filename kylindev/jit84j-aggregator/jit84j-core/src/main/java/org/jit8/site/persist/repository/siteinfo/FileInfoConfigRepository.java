package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;
import org.springframework.data.jpa.repository.Query;

public interface FileInfoConfigRepository extends GenericJpaRepository<FileInfoConfig, Long>{

	@Query("from FileInfoConfig o where o.disable=:disable and o.deleted=:deleted")
	public List<FileInfoConfig> getFileInfoConfigList(boolean disable, boolean deleted);
}
