package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;
import java.util.concurrent.TimeoutException;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.exception.MemcachedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemcachedUtil implements Serializable {


	private static final long serialVersionUID = 4758329199183465849L;
	private static final Logger LOGGER = LoggerFactory.getLogger(MemcachedUtil.class);
	
	public MemcachedUtil() {
	}

	public static boolean set(final String key, final int exp, final Object value,MemcachedClient memcachedClient){
		try {
			LOGGER.info("stored into memcached begin, key=" + key);
			boolean stored = memcachedClient.set(key, exp, value);
			LOGGER.info("stored into memcached end, key=" + key+ " , exp= " + exp + ", stored="+ stored);
			return stored;
		} catch (TimeoutException e) {
			LOGGER.error("TimeoutException:",e);
		} catch (InterruptedException e) {
			LOGGER.error("InterruptedException:",e);
		} catch (MemcachedException e) {
			LOGGER.error("MemcachedException:",e);
		}
		return false;
	}
	
	public static boolean refresh(final String key, final int exp, final Object value,MemcachedClient memcachedClient){
		try {
			LOGGER.info("remove from memcached begin, key=" + key);
			memcachedClient.delete(key);
			LOGGER.info("restored into memcached begin, key=" + key);
			boolean stored = memcachedClient.set(key, exp, value);
			LOGGER.info("restored into memcached end, key=" + key+ " , exp= " + exp + ", stored="+ stored);
			return stored;
		} catch (TimeoutException e) {
			LOGGER.error("TimeoutException:",e);
		} catch (InterruptedException e) {
			LOGGER.error("InterruptedException:",e);
		} catch (MemcachedException e) {
			LOGGER.error("MemcachedException:",e);
		}
		return false;
	}
	
	public static Object get(String key,MemcachedClient memcachedClient){
		try {
			LOGGER.info("get from memcached begin, key=" + key);
			Object value = memcachedClient.get(key);
			LOGGER.info("get from memcached end, key=" + key);
			return value;
		} catch (TimeoutException e) {
			LOGGER.error("TimeoutException:",e);
		} catch (InterruptedException e) {
			LOGGER.error("InterruptedException:",e);
		} catch (MemcachedException e) {
			LOGGER.error("MemcachedException:",e);
		}
		return null;
	}
	
	public static boolean delete(String key,MemcachedClient memcachedClient){
		try {
			Object object = memcachedClient.get(key);
			if(object == null){
				return true;
			}
			boolean deleted = memcachedClient.delete(key);
			return deleted;
		} catch (TimeoutException e) {
			LOGGER.error("TimeoutException:",e);
		} catch (InterruptedException e) {
			LOGGER.error("InterruptedException:",e);
		} catch (MemcachedException e) {
			LOGGER.error("MemcachedException:",e);
		}
		return false;
	}
	
	public static boolean touch(final String key, final int exp,MemcachedClient memcachedClient){
		try {
			LOGGER.info("check exist from memcached begin, key=" + key);
			Object value = memcachedClient.get(key);
			boolean stored = false;
			if(null != value){
				LOGGER.info("set expiration time in memcached begin, key=" + key);
				stored = memcachedClient.touch(key, exp);
				LOGGER.info("set expiration time in memcached end, key=" + key+ " , exp= " + exp + ", stored="+ stored);
				return stored;
			}
			LOGGER.info("check exist from memcached end, key=" + key + ", stored="+ stored);
		} catch (TimeoutException e) {
			LOGGER.error("TimeoutException:",e);
		} catch (InterruptedException e) {
			LOGGER.error("InterruptedException:",e);
		} catch (MemcachedException e) {
			LOGGER.error("MemcachedException:",e);
		}
		return false;
	}
}
