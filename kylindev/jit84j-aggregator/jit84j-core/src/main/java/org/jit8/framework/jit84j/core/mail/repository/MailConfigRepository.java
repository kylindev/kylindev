package org.jit8.framework.jit84j.core.mail.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MailConfigRepository extends GenericJpaRepository<MailConfig, Long>{

	@Query("from MailConfig o where o.disable=:disable and o.deleted=:deleted")
	List<MailConfig> getMailConfigList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);
	
	@Query("from MailConfig o where o.type=:type and o.deleted=false")
	public MailConfig findByType(@Param("type")String type) ;

}
