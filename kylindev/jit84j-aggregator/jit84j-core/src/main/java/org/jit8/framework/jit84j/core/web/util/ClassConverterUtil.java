package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ClassConverterUtil implements Serializable {


	private static final long serialVersionUID = 4490535715747379429L;
	
	public static Long[] getLong(String[] values){
		if(null != values && values.length>0){
			int length = values.length;
			Long[] result = new Long[length];
			for(int i=0;i<length;i++){
				result[i] = Long.valueOf(StringUtils.trim(values[i]));
			}
			return result;
		}
		return null;
	}
}
