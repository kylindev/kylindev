package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserRoleRepository extends GenericJpaRepository<UserRole, Long>{

	@Query("from UserRole o where o.user.uniqueIdentifier=:userId")
	public List<UserRole> findUserRoleList(@Param("userId")Long userId);
	
	@Modifying
	@Query("delete from UserRole o where o.user.uniqueIdentifier in :userIdList")
	public void removeByIdList(@Param("userIdList")List<Long> userIdList);
}
