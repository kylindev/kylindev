package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.RolePermission;

public interface RolePermissionDao extends GenericDao<RolePermission, Long> {

	public List<RolePermission> findByPermissionId(Long permissionId);
}
