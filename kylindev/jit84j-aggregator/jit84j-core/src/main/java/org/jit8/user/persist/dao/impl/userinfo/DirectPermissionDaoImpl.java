package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.DirectPermissionDao;
import org.jit8.user.persist.domain.userinfo.DirectPermission;
import org.jit8.user.persist.repository.userinfo.DirectPermissionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DirectPermissionDaoImpl extends GenericDaoImpl<DirectPermission, Long> implements DirectPermissionDao {

	@Resource
	private DirectPermissionRepository directPermissionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = directPermissionRepository;
	}

	@Override
	public DirectPermission findByType(String type) {
		return directPermissionRepository.findByType(type);
	}

}
