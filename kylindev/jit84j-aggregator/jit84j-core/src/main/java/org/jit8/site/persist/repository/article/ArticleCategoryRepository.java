package org.jit8.site.persist.repository.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ArticleCategoryRepository extends GenericJpaRepository<ArticleCategory, Long>{

	@Query("from ArticleCategory o where o.disable=:disable and o.deleted=:deleted")
	List<ArticleCategory> getArticleCategoryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from ArticleCategory o where o.disable=:disable and o.deleted=:deleted and o.article.uniqueIdentifier=:articleId")
	List<ArticleCategory> getArticleCategoryListByArticleId(@Param("disable")boolean disable,@Param("deleted")boolean deleted, @Param("articleId") Long articleId);
	
	
	
}
