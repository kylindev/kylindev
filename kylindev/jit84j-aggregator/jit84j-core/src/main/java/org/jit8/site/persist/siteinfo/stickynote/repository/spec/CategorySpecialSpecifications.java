package org.jit8.site.persist.siteinfo.stickynote.repository.spec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.framework.jit84j.core.web.util.ClassConverterUtil;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial_;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory_;
import org.springframework.data.jpa.domain.Specification;



public class CategorySpecialSpecifications {

	public static Specification<CategorySpecial> queryByCondition(final CategorySpecial categorySpecial) {
		return new Specification<CategorySpecial>() {
			public Predicate toPredicate(Root<CategorySpecial> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				String name = StringUtils.trim(categorySpecial.getName());
				String imagePath = StringUtils.trim(categorySpecial.getImagePath());
				
				predicateList.add(builder.equal(root.get(CategorySpecial_.deleted ), false));
				
				if(StringUtils.isNotEmpty(name)){
					predicateList.add(builder.like(root.get(CategorySpecial_.name ), Specifications.PERCENT + name + Specifications.PERCENT));
				}
				
				
				if(StringUtils.isNotEmpty(imagePath)){
					predicateList.add(builder.like(root.get(CategorySpecial_.imagePath ), Specifications.PERCENT + imagePath + Specifications.PERCENT));
				}
				
				String categoryIds = StringUtils.trim(categorySpecial.getCategoryIds());
				if(StringUtils.isNotEmpty(categoryIds)){
					String[] categoryIdArray = categoryIds.split(",");
					Long[] categoryIdLong = ClassConverterUtil.getLong(categoryIdArray);
					if(null != categoryIdLong && categoryIdLong.length>0){
						predicateList.add(root.get(CategorySpecial_.category).get(StickyNoteCategory_.uniqueIdentifier).in(Arrays.asList(categoryIdLong)));
					}
				}
				
				String specialAreaIds = StringUtils.trim(categorySpecial.getSpecialAreaIds());
				if(StringUtils.isNotEmpty(specialAreaIds)){
					String[] specialAreaIdArray = specialAreaIds.split(",");
					Long[] specialAreaIdLong = ClassConverterUtil.getLong(specialAreaIdArray);
					if(null != specialAreaIdLong && specialAreaIdLong.length>0){
						predicateList.add(root.get(CategorySpecial_.specialArea).get(SpecialArea_.uniqueIdentifier).in(Arrays.asList(specialAreaIdLong)));
					}
				}
				
				
				SpecialArea specialArea = categorySpecial.getSpecialArea();
				
				if(null != specialArea){
					String specialAreaName = StringUtils.trim(specialArea.getName());
					if(StringUtils.isNotEmpty(specialAreaName)){
						predicateList.add(builder.like(root.get(CategorySpecial_.specialArea ).get(SpecialArea_.name), Specifications.PERCENT + specialAreaName + Specifications.PERCENT));
					}
					
					String specialAreaCode = StringUtils.trim(specialArea.getCode());
					if(StringUtils.isNotEmpty(specialAreaCode)){
						predicateList.add(builder.like(root.get(CategorySpecial_.specialArea ).get(SpecialArea_.code), Specifications.PERCENT + specialAreaCode + Specifications.PERCENT));
					}
					
					Long specialAreaId = specialArea.getId();
					if(null != specialAreaId && specialAreaId>0){
						predicateList.add(builder.equal(root.get(CategorySpecial_.specialArea ).get(SpecialArea_.uniqueIdentifier), specialAreaId));
					}
				}
				
				StickyNoteCategory  stickyNoteCategory  = categorySpecial.getCategory();
				if(null != stickyNoteCategory){
					String stickyNoteCategoryName = StringUtils.trim(stickyNoteCategory.getName());
					if(StringUtils.isNotEmpty(stickyNoteCategoryName)){
						predicateList.add(builder.like(root.get(CategorySpecial_.category ).get(StickyNoteCategory_.name), Specifications.PERCENT + stickyNoteCategoryName + Specifications.PERCENT));
					}
					
					String stickyNoteCategoryCode = StringUtils.trim(stickyNoteCategory.getCode());
					if(StringUtils.isNotEmpty(stickyNoteCategoryCode)){
						predicateList.add(builder.like(root.get(CategorySpecial_.category ).get(StickyNoteCategory_.code), Specifications.PERCENT + stickyNoteCategoryCode + Specifications.PERCENT));
					}
					
					Long stickyNoteCategoryId = stickyNoteCategory.getId();
					if(null != stickyNoteCategoryId && stickyNoteCategoryId>0){
						predicateList.add(builder.equal(root.get(CategorySpecial_.category ).get(StickyNoteCategory_.uniqueIdentifier), stickyNoteCategoryId));
					}
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
