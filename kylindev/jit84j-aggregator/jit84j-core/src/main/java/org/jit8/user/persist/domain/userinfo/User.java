package org.jit8.user.persist.domain.userinfo;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;
import org.jit8.framework.jit84j.core.persistent.model.BasicPersistentEntity;
import org.jit8.user.persist.validator.CheckEmail;
import org.jit8.user.persist.validator.CheckPassword;
import org.jit8.user.persist.validator.CheckUsername;
import org.springframework.stereotype.Component;

@Entity
@Component("user") 
@Access(value = AccessType.FIELD)
@CheckPassword()
@CheckUsername()
@CheckEmail()
public class User extends BasicPersistentEntity {

	
	private static final long serialVersionUID = 118513538847080084L;

	/**
	 * 用户类型：
	 * 1.系统内置  系统内置用户不被查询出来，一般情况下为开发人员的帐号
	 * （内置用户是由系统初始化时配置在里面的，保证系统的正常运行，不可删除，设为禁用状态，不能登录后台与前台
	 * 如果后续添加：只有超级管理员才有添加内置帐户的权限，后续添加的用户可以设为开启状态，可以从后台登录）
	 * 2.注册用户  在用户查询列表展示的就是这部分用户（从前台注册的用户与后台添加的用户）
	 * 
	 */
	public static final String USER_TYPE_SYS_INLAY = "SYS_INLAY";//系统内置
	public static final String USER_TYPE_REGISTER = "REGISTER";//注册用户
	
	private String nickname;
	@Pattern(regexp="[A-Za-z0-9]{6,20}",message="用户名必须为数字0-9,字母a-z或A-Z的组合，6-20位")
	private String username;
	private String password;
	private String phone;
	@Transient
	private String confirmPassword;
	private Long userNum;
	
	@NotEmpty(message="您名字？")
	private String firstname;
	
	@NotEmpty(message="您贵姓？")
	private String lastname;
	
	private String imagePath;//用户头像40X40
	
	private boolean locked;
	
	@ManyToOne
	private UserLastAccessLog userLastAccessLog;
	
	@ManyToOne
	private UserId userId;
	
	//@NotEmpty(message="Email不能为空")
	@Pattern(regexp="^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$",message="格式有误，请输入正确的格式，例如：kylinboy521@163.com")
	private String email;
	
	@NotEmpty(message="您来自何方啊，告诉大家一下呗")
	private String comeLocation;
	
	private String userType;
	
	private boolean actived;//是否被激活
	
	@NotEmpty(message="您是啥头衔啊？")
	private String civilit; //身份称谓
	
	@NotEmpty(message="对大家说句话吧，好不容易注册一次，大家都在期待你的话呢")
	private String manifesto;//注册宣言
	
	@ManyToOne
	private OwnRelation ownRelation;
	
	@Basic(fetch=FetchType.EAGER)
	@OneToMany(mappedBy="user",fetch=FetchType.EAGER,cascade=CascadeType.ALL)
	private List<UserRole> roleList;
	
	
	@Transient
	private List<Role> roles;
	
	@Basic(fetch=FetchType.EAGER)
	@OneToMany(mappedBy="user",cascade={CascadeType.MERGE,CascadeType.REFRESH})
	private List<UserPermission> permissionList;
	
	@Transient
	private List<Long> roleIdList;
	
	@Transient
	private User currentUser;
	
	@Transient
	private Long id;
	
	@Transient
	private String permissionIds;
	
	@Transient
	private Long ownRelationId;
	
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Long getUserNum() {
		if(userId != null){
			this.userNum=this.userId.getUserId();
		}
		return userNum;
	}
	public void setUserNum(Long userNum) {
		if(userId != null && userId.getUserId()<=0){
			this.userId.setUserId(userNum);
		}
		this.userNum = userNum;
	}
	public UserId getUserId() {
		return userId;
	}
	public void setUserId(UserId userId) {
		this.userId = userId;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getConfirmPassword() {
		return confirmPassword;
	}
	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	public OwnRelation getOwnRelation() {
		return ownRelation;
	}
	public void setOwnRelation(OwnRelation ownRelation) {
		this.ownRelation = ownRelation;
	}
	public String getComeLocation() {
		return comeLocation;
	}
	public void setComeLocation(String comeLocation) {
		this.comeLocation = comeLocation;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public UserLastAccessLog getUserLastAccessLog() {
		return userLastAccessLog;
	}
	public void setUserLastAccessLog(UserLastAccessLog userLastAccessLog) {
		this.userLastAccessLog = userLastAccessLog;
	}
	public List<UserRole> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<UserRole> roleList) {
		this.roleList = roleList;
	}
	public List<UserPermission> getPermissionList() {
		return permissionList;
	}
	public void setPermissionList(List<UserPermission> permissionList) {
		this.permissionList = permissionList;
	}
	public List<Role> getRoles() {
		return roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}
	public List<Long> getRoleIdList() {
		return roleIdList;
	}
	public void setRoleIdList(List<Long> roleIdList) {
		this.roleIdList = roleIdList;
	}
	public User getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	/**
	 * @return the permissionIds
	 */
	public String getPermissionIds() {
		return permissionIds;
	}
	/**
	 * @param permissionIds the permissionIds to set
	 */
	public void setPermissionIds(String permissionIds) {
		this.permissionIds = permissionIds;
	}
	public Long getOwnRelationId() {
		return ownRelationId;
	}
	public void setOwnRelationId(Long ownRelationId) {
		this.ownRelationId = ownRelationId;
	}
	/**
	 * @return the userType
	 */
	public String getUserType() {
		return userType;
	}
	/**
	 * @param userType the userType to set
	 */
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * @return the locked
	 */
	public boolean isLocked() {
		return locked;
	}
	/**
	 * @param locked the locked to set
	 */
	public void setLocked(boolean locked) {
		this.locked = locked;
	}
	/**
	 * @return the civilit
	 */
	public String getCivilit() {
		return civilit;
	}
	/**
	 * @param civilit the civilit to set
	 */
	public void setCivilit(String civilit) {
		this.civilit = civilit;
	}
	/**
	 * @return the manifesto
	 */
	public String getManifesto() {
		return manifesto;
	}
	/**
	 * @param manifesto the manifesto to set
	 */
	public void setManifesto(String manifesto) {
		this.manifesto = manifesto;
	}
	/**
	 * @return the actived
	 */
	public boolean isActived() {
		return actived;
	}
	/**
	 * @param actived the actived to set
	 */
	public void setActived(boolean actived) {
		this.actived = actived;
	}
	
}
