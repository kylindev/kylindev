package org.jit8.site.persist.dao.impl.menu;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.menu.NavigatMenuDao;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.persist.repository.menu.NavigatMenuRepository;
import org.springframework.stereotype.Repository;

@Repository
public class NavigatMenuDaoImpl extends GenericDaoImpl<NavigatMenu, Long> implements NavigatMenuDao {

	@Resource
	private NavigatMenuRepository navigatMenuRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = navigatMenuRepository;
	}

	@Override
	public List<NavigatMenu> getNavigatMenuList(boolean disable, boolean deleted) {
		return navigatMenuRepository.getNavigatMenuList(disable, deleted);
	}
	
	@Override
	public List<NavigatMenu> getNavigatMenuListByIds(boolean disable,
			boolean deleted, List<Long> navigatMenuIdList) {
		return navigatMenuRepository.getNavigatMenuListByIds(disable, deleted, navigatMenuIdList);
	}

	@Override
	public List<NavigatMenu> getNavigatMenuListByParentNull() {
		return navigatMenuRepository.getNavigatMenuListByParentNull();
	}

	@Override
	public List<NavigatMenu> getNavigatMenuByCode(boolean disable, boolean deleted,
			String code) {
		
		return navigatMenuRepository.getNavigatMenuByCode(disable, deleted, code);
	}

	@Override
	public List<NavigatMenu> getNavigatMenuByTopCode(boolean disable,
			boolean deleted, String code) {
		return navigatMenuRepository.getNavigatMenuByTopCode(disable, deleted, code);
	}

	@Override
	public NavigatMenu findByCode(String code) {
		return navigatMenuRepository.findByCode( code);
	}
}
