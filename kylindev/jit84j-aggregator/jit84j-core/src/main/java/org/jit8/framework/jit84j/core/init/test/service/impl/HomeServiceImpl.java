package org.jit8.framework.jit84j.core.init.test.service.impl;

import org.jit8.framework.jit84j.core.init.test.model.Home;
import org.jit8.framework.jit84j.core.init.test.repository.HomeRepository;
import org.jit8.framework.jit84j.core.init.test.service.HomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("homeService")
public class HomeServiceImpl implements HomeService {

	@Autowired
	private HomeRepository homeRepository;
	
	
	public void save(Home home) {

		homeRepository.save(home);
	}

}
