package org.jit8.user.persist.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.jit8.user.persist.dao.userinfo.UserDao;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.util.StringUtils;

public class CheckUsernameValidator implements ConstraintValidator<CheckUsername, User> {  
	  
	@Resource
	private UserDao userDao;
	
    @Override  
    public void initialize(CheckUsername constraintAnnotation) {  
    }  
  
    @Override  
    public boolean isValid(User user, ConstraintValidatorContext context) {  
        if(user == null) {  
            return true;  
        }  
  
        //没有填密码  
        if(!StringUtils.hasText(user.getUsername())) {  
            context.disableDefaultConstraintViolation();  
            context.buildConstraintViolationWithTemplate("{user.username.empty}")  
                    .addNode("username")  
                    .addConstraintViolation();  
            return false;  
        }  
  
        if(StringUtils.hasText(user.getUsername())) {  
        	if(null != userDao && userDao.exitUserByUsername(user.getUsername())){
	            context.disableDefaultConstraintViolation();  
	            context.buildConstraintViolationWithTemplate("{user.username.unique.error}")  
	                    .addNode("username")  
	                    .addConstraintViolation();  
	            return false;  
        	}
        }  
        
        return true;  
    }  
}  
