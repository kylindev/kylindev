package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StickyNoteCategoryRepository extends GenericJpaRepository<StickyNoteCategory, Long>{

	@Query("from StickyNoteCategory o where o.disable=:disable and o.deleted=:deleted")
	List<StickyNoteCategory> getStickyNoteCategoryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from StickyNoteCategory o where o.code=:code")
	public StickyNoteCategory findByCode(@Param("code")String code);
	
	@Query("from StickyNoteCategory o where o.uniqueIdentifier in :idList")
	public List<StickyNoteCategory> findByIds(@Param("idList")List<Long> idList);
}
