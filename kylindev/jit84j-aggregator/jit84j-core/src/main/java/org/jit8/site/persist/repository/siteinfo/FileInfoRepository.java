package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FileInfoRepository extends GenericJpaRepository<FileInfo, Long>{

	@Query("from FileInfo o where o.disable=:disable and o.deleted=:deleted")
	List<FileInfo> getFileInfoList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from FileInfo o where o.systemGallery.uniqueIdentifier=:galleryId and o.deleted=false")
	Page<FileInfo> findBySystemGalleryId(@Param("galleryId") long galleryId,Pageable pageable);
	
	@Query("from FileInfo o where o.customGallery.uniqueIdentifier=:galleryId and o.deleted=false")
	Page<FileInfo> findByCustomGalleryId(@Param("galleryId") long galleryId,Pageable pageable);
	
	@Query("from FileInfo o where o.uniqueIdentifier=:id and o.owner=:owner")
	public FileInfo findByIdAndOwner(@Param("id")long id, @Param("owner")long owner);
}
