package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleDao extends GenericDao<Role, Long> {

	List<Role> getRoleListByRoleLevel(int roleLevel);
	
	public List<Role> getRoleListByRoleIdList(List<Long> roleIdList);
	
	public Page<Role> getRoleList(Pageable pageable);
	
	public List<Role> saveRoleList(List<Role> roleList);
	
	public Page<Role> getRoleList(Role role, Pageable pageable);
}
