package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserDao extends GenericDao<User, Long> {

	public User findUserByUsername(String username);
	
	public boolean exitUserByUsername(String username);
	
	public User findUserByEmail(String email);
	
	public boolean exitUserByEmail(String email);
	
	public User findUserByEmailAndPassword(String email, String password) ;
	
	public Page<User> getUserList(Pageable pageable);
	
	public Page<User> getLastUserList(Pageable pageable);
	
	public Page<User> getUserList(User user, Pageable pageable);
	
	public List<User> getUserListByUserIdList(List<Long> userIdList);
	
	public void addUser(User user);
	
	public Page<User> getUserListByUserIdNotNull(User user, Pageable pageable);
}
