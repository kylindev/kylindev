package org.jit8.framework.jit84j.core.cache.memached;

import java.io.Serializable;
import java.lang.reflect.Method;

import javax.persistence.Transient;

/**
 * MemachedModel 用于存储手动刷新的缓存对象
 * 所对应的方法必须无参
 * @author kylinboy
 *
 */
public class MemachedModel implements Serializable {

	private static final long serialVersionUID = -4549723103826239093L;
	
	public MemachedModel() {
	}

	private String key;//缓存的key存储在这里，如果为空则取类的全名_方法名
	private String name;//缓存的显示名称存储在这里
	@Transient
	private Class<?> targetClazz;
	private String targetMethodName;
	@Transient
	private Method targetMethod;
	
	@Transient
	private Memached targetMemached;
	
	private Object targetBean;
	
	private int timeout = MemachedDefinition.TIMEOUT_DEFAULT;
	
	private boolean autoRefresh;//是否自动刷新，若为true，可在页面上点击 刷新 按钮进行刷新
	
	private boolean repeated; //是否重复 用于按间隔重复性更新缓存
	
	private int expiresTime = MemachedDefinition.DEFAULT_EXPIRES_TIME;//过期时间
	
	private long delayRefreshTime = MemachedDefinition.DELAY_REFRESH_TIME;//延迟刷新时间
	
	private long period =MemachedDefinition.REFRESH_PERIOD;//刷新周期 
	
	
	private String keys;
	
	
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Class<?> getTargetClazz() {
		return targetClazz;
	}
	public void setTargetClazz(Class<?> targetClazz) {
		this.targetClazz = targetClazz;
	}
	public String getTargetMethodName() {
		return targetMethodName;
	}
	public void setTargetMethodName(String targetMethodName) {
		this.targetMethodName = targetMethodName;
	}
	public Method getTargetMethod() {
		return targetMethod;
	}
	public void setTargetMethod(Method targetMethod) {
		this.targetMethod = targetMethod;
	}
	public Memached getTargetMemached() {
		return targetMemached;
	}
	public void setTargetMemached(Memached targetMemached) {
		this.targetMemached = targetMemached;
	}
	public Object getTargetBean() {
		return targetBean;
	}
	public void setTargetBean(Object targetBean) {
		this.targetBean = targetBean;
	}
	public int getTimeout() {
		return timeout;
	}
	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
	public boolean isAutoRefresh() {
		return autoRefresh;
	}
	public void setAutoRefresh(boolean autoRefresh) {
		this.autoRefresh = autoRefresh;
	}
	public boolean isRepeated() {
		return repeated;
	}
	public void setRepeated(boolean repeated) {
		this.repeated = repeated;
	}
	public int getExpiresTime() {
		return expiresTime;
	}
	public void setExpiresTime(int expiresTime) {
		this.expiresTime = expiresTime;
	}
	public long getDelayRefreshTime() {
		return delayRefreshTime;
	}
	public void setDelayRefreshTime(long delayRefreshTime) {
		this.delayRefreshTime = delayRefreshTime;
	}
	public long getPeriod() {
		return period;
	}
	public void setPeriod(long period) {
		this.period = period;
	}
	public String getKeys() {
		return keys;
	}
	public void setKeys(String keys) {
		this.keys = keys;
	}
	
	
}
