package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;

public interface FileSystemGalleryTypeDao extends GenericDao<FileSystemGalleryType, Long> {

	public List<FileSystemGalleryType> getFileSystemGalleryTypeList(boolean disable, boolean deleted) ;
	
}
