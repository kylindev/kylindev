package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileCustomGalleryDao;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.jit8.site.persist.repository.siteinfo.FileCustomGalleryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FileCustomGalleryDaoImpl extends GenericDaoImpl<FileCustomGallery, Long> implements FileCustomGalleryDao {

	@Resource
	private FileCustomGalleryRepository fileCustomGalleryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileCustomGalleryRepository;
	}

	@Override
	public List<FileCustomGallery> getFileCustomGalleryList(boolean disable, boolean deleted) {
		return fileCustomGalleryRepository.getFileCustomGalleryList(disable, deleted);
	}

	@Override
	public FileCustomGallery findByCodeAndType(String code, int type) {
		return fileCustomGalleryRepository.findByCodeAndType( code,  type);
	}
}
