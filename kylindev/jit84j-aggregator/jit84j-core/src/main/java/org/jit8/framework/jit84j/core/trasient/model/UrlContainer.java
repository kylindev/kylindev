package org.jit8.framework.jit84j.core.trasient.model;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

public class UrlContainer implements Serializable {

		private static final long serialVersionUID = -741926941447489260L;

		//URL组成：模块路径 code [权限参数个数标识][菜单参数个数标识] 参数 任意字符 
		private static final String URL_SEP =   "u_[0-9]{5,10}_u" ;
		
		
		//	             		匹配说明				通用字符      权限参数标识    菜单参数标识    参数分隔符       权限参数个数       菜单参数个数-权限参数个数     任意字符
		public static String urlRegexExample = "(([0-9a-zA-Z/-_]*P([0-9]{1,2})N([0-9]{1,2})(~[0-9a-zA-Z-_]+){"+2+"})(~[0-9a-zA-Z-_]+){"+ (3-2) +"}).*";

		
		public static final String URL_REX_PARAM_SEPORATOR = "~";
		public static final String URL_REX_COMMON_PREFIX="[0-9a-zA-Z/-_]*";
		public static final String URL_REX_SPECIFIC=URL_SEP;//先按目前以_u为分隔截取，后续将改造成code+6位developerId
		public static final String URL_REX_PERMISSION_NAVIGATION = "P([0-9]{1,2})N([0-9]{1,2})";//权限与菜单个数标识
		public static final String URL_REX_PARAM = "[0-9a-zA-Z-_]+";//参数匹配
		public static final String URL_REX_PARAM_SEPORATOR_AND_PARAM = URL_REX_PARAM_SEPORATOR + URL_REX_PARAM;//参数分隔符与参数匹配
		public static final String URL_REX_ANY = ".*";//任意字符匹配
		
		
		//URL定义模式
		//带参数的匹配模式
		public static final String URL_REX_P_N = "(("+ URL_REX_COMMON_PREFIX + URL_SEP + ")"+ URL_REX_PERMISSION_NAVIGATION  + URL_REX_ANY+")";
		//1.先根据以上模式 匹配出 权限有几个参数，菜单有几个参数（其中 菜单参数个数>=权限参数个数）
		//2.再根据参数个数作拼接，组成新的模式，作匹配，
		//3.再匹配，第一个url为 菜单路径，第二个url为 权限路径
		
		//不带参数的匹配模式
		public static final String URL_REX_NO_PARAM = "("+URL_REX_COMMON_PREFIX + URL_REX_SPECIFIC + ")"+URL_REX_ANY;	
		
		
		public static void main(String[] args) {
			String reg = "((([0-9a-zA-Z/-_]*u_[0-9]{5,10}_uP([0-9]{1,2})N([0-9]{1,2})((~[0-9a-zA-Z-_]+){2}))(~[0-9a-zA-Z-_]+){1})).*";
			String url1 = "/admin/user/wwwwu_100001_uP2N4~232~323~2~5~7?oo=00";
			String url2 = "/admin/user/wwwwu_100001_uP0N4~232~323~2~5?oo=00";//permission参数为0
			String url3 = "/admin/user/wwwwu_100001_uP-1N4~232~323~2~5?oo=00";//给个-1试试
			// /admin/article/o_detailArticle_ou_1000001_uP1N2~14~111112
			System.out.println(getUrlContainerFromUrl(url1).toString());
			System.out.println(getUrlContainerFromUrl(url2).toString());
			System.out.println(getUrlContainerFromUrl(url3).toString());
		}
		
		
		public static UrlContainer getUrlContainerFromUrl(String url){
			UrlContainer urlContainer = new UrlContainer();
			urlContainer.setFullUrl(url);
			Pattern p = Pattern.compile(URL_REX_P_N);
			Matcher m = p.matcher(url);
			int permissionParamNum = 0;
			int navigationParamNum = 0;
			//先检查是否带参url
			if(m.find()){
				String g1 = m.group(1);//全url
				
				String moduleUrl = m.group(2);//主url
				
				urlContainer.setModuleUrl(moduleUrl);
				String g3 = m.group(3);//permission参数个数
				
				permissionParamNum= Integer.valueOf(g3);
				urlContainer.setPermissionParamNum(permissionParamNum);
				String g4 = m.group(4);//navigation参数个数
				
				navigationParamNum = Integer.valueOf(g4);
				urlContainer.setNavigationParamNum(navigationParamNum);
				
				if(permissionParamNum>navigationParamNum){
					String errorCode = "param.number.permission.larger.than.navigation";
					String errorMessage = "permission param number is not bigger than navigation param number, check the url";
					urlContainer.setErrorCode(errorCode);
					urlContainer.setErrorMessage(errorMessage);
					return urlContainer;
				}
				
				String URL_REX_PARAM_ = "(("+ URL_REX_COMMON_PREFIX + URL_SEP +  URL_REX_PERMISSION_NAVIGATION + "(" + URL_REX_PARAM_SEPORATOR_AND_PARAM +"){"+ permissionParamNum +"})("+URL_REX_PARAM_SEPORATOR_AND_PARAM+"){"+ ( navigationParamNum - permissionParamNum )+"})"+URL_REX_ANY;
				
				urlContainer.setUrlRexPermissionNavigation(URL_REX_PARAM_);
				Pattern p2 = Pattern.compile(URL_REX_PARAM_);
				Matcher m2 = p2.matcher(url);
				if(m2.find()){
					String navigationUrl = m2.group(1);//navigation 路径
					
					String permisssionUrl = m2.group(2);//permission路径
					int paramEnd = m2.end(1);//参数结束位置
					int paramStart = m2.end(4);//参数起始位置
					if(paramEnd>paramStart){
						StringBuilder params1 = new StringBuilder(url.substring(paramStart, paramEnd));//params1.toString();
						int first = params1.indexOf(URL_REX_PARAM_SEPORATOR);
						if(first>-1){
							params1.deleteCharAt(first);
						}
						String params = params1.toString();
						if(StringUtils.isNotEmpty(params)){
							String[] paramArray = params.split(URL_REX_PARAM_SEPORATOR);
							urlContainer.setParamList(Arrays.asList(paramArray));
						}
					}
					
					
					
					urlContainer.setPermisssionUrl(permisssionUrl);
					urlContainer.setNavigationUrl(navigationUrl);
				}else{
					String errorCode = "param.number.conflict";
					String errorMessage = "defined param number is not equals actural param number, check the url";
					urlContainer.setErrorCode(errorCode);
					urlContainer.setErrorMessage(errorMessage);
				}
			}else{
				//如果不带参则按标准url进行匹配
				Pattern pStep3 = Pattern.compile(URL_REX_NO_PARAM);
				Matcher mStep3 = pStep3.matcher(url);
				if(mStep3.find()){
					String permisssionUrl = mStep3.group(1);
					urlContainer.setPermisssionUrl(permisssionUrl);
					urlContainer.setNavigationUrl(url);
				}
			}
			
			return urlContainer;
		}
		
		
		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("fullUrl:").append(fullUrl).append(", moduleUrl:").append(moduleUrl)
			.append(", navigationUrl:").append(navigationUrl)
			.append(", navigationParamNum:").append(navigationParamNum)
			.append(", permisssionUrl:").append(permisssionUrl)
			.append(", permissionParamNum:").append(permissionParamNum)
			.append(", urlRexPermissionNavigation").append(urlRexPermissionNavigation)
			.append(", paramList:[").append(paramListToString()).append("]").append(", errorCode:").append(errorCode)
			.append(", errorMessage:").append(errorMessage);
			
			return sb.toString();
		}
		public String paramListToString(){
			if(null != paramList){
				StringBuilder sb = new StringBuilder();
				for(String param : paramList){
					sb.append(param).append(",");
				}
				int lastIndex = sb.lastIndexOf(",");
				if(lastIndex>-1){
					sb.deleteCharAt(lastIndex);
				}
				return sb.toString();
			}
			return "";
			
		}
		
	public UrlContainer() {
	}
	
	
	private int permissionParamNum;//权限路径中参数个数
	private int navigationParamNum;//菜单路径中参数个数 （总参数个数，必须>=permissionParamNum）
	private String permisssionUrl;//权限路径
	private String navigationUrl;//菜单路径
	private String fullUrl;//全路径
	private String moduleUrl;//模块划分url
	private String urlRexPermissionNavigation;//带参数url二次匹配正则表达式
	
	private List<String> paramList;//参数列表
	
	private String errorCode;
	private String errorMessage;

	/**
	 * @return the permissionParamNum
	 */
	public int getPermissionParamNum() {
		return permissionParamNum;
	}
	/**
	 * @param permissionParamNum the permissionParamNum to set
	 */
	public void setPermissionParamNum(int permissionParamNum) {
		this.permissionParamNum = permissionParamNum;
	}
	/**
	 * @return the navigationParamNum
	 */
	public int getNavigationParamNum() {
		return navigationParamNum;
	}
	/**
	 * @param navigationParamNum the navigationParamNum to set
	 */
	public void setNavigationParamNum(int navigationParamNum) {
		this.navigationParamNum = navigationParamNum;
	}
	/**
	 * @return the permisssionUrl
	 */
	public String getPermisssionUrl() {
		return permisssionUrl;
	}
	/**
	 * @param permisssionUrl the permisssionUrl to set
	 */
	public void setPermisssionUrl(String permisssionUrl) {
		this.permisssionUrl = permisssionUrl;
	}
	/**
	 * @return the navigationUrl
	 */
	public String getNavigationUrl() {
		return navigationUrl;
	}
	/**
	 * @param navigationUrl the navigationUrl to set
	 */
	public void setNavigationUrl(String navigationUrl) {
		this.navigationUrl = navigationUrl;
	}
	/**
	 * @return the fullUrl
	 */
	public String getFullUrl() {
		return fullUrl;
	}
	/**
	 * @param fullUrl the fullUrl to set
	 */
	public void setFullUrl(String fullUrl) {
		this.fullUrl = fullUrl;
	}
	/**
	 * @return the moduleUrl
	 */
	public String getModuleUrl() {
		return moduleUrl;
	}
	/**
	 * @param moduleUrl the moduleUrl to set
	 */
	public void setModuleUrl(String moduleUrl) {
		this.moduleUrl = moduleUrl;
	}
	/**
	 * @return the urlRexPermissionNavigation
	 */
	public String getUrlRexPermissionNavigation() {
		return urlRexPermissionNavigation;
	}
	/**
	 * @param urlRexPermissionNavigation the urlRexPermissionNavigation to set
	 */
	public void setUrlRexPermissionNavigation(String urlRexPermissionNavigation) {
		this.urlRexPermissionNavigation = urlRexPermissionNavigation;
	}


	/**
	 * @return the paramList
	 */
	public List<String> getParamList() {
		return paramList;
	}


	/**
	 * @param paramList the paramList to set
	 */
	public void setParamList(List<String> paramList) {
		this.paramList = paramList;
	}
	
	
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}


	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}


	/**
	 * @return the errorMessage
	 */
	public String getErrorMessage() {
		return errorMessage;
	}


	/**
	 * @param errorMessage the errorMessage to set
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	

}
