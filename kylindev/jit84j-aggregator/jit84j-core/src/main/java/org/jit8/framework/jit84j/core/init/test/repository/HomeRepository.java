package org.jit8.framework.jit84j.core.init.test.repository;

import org.jit8.framework.jit84j.core.init.test.model.Home;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HomeRepository extends JpaRepository<Home, Long>{

}
