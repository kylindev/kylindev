package org.jit8.framework.jit84j.core.mail.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailAttachmentDao;
import org.jit8.framework.jit84j.core.mail.domain.MailAttachment;
import org.jit8.framework.jit84j.core.mail.repository.MailAttachmentRepository;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MailAttachmentDaoImpl extends GenericDaoImpl<MailAttachment, Long> implements MailAttachmentDao {

	@Resource
	private MailAttachmentRepository mailAttachmentRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = mailAttachmentRepository;
	}

	@Override
	public List<MailAttachment> getMailAttachmentList(boolean disable, boolean deleted) {
		return mailAttachmentRepository.getMailAttachmentList(disable, deleted);
	}
}
