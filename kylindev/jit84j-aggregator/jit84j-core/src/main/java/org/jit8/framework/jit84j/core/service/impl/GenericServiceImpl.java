package org.jit8.framework.jit84j.core.service.impl;

import java.io.Serializable;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.framework.jit84j.core.service.GenericService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.katesoft.scale4j.persistent.model.unified.AuditBasicEntity;

public class GenericServiceImpl<T, ID extends Serializable> implements GenericService<T, ID>{

	protected GenericDao<T, ID> genericDao;
	
	public List<T> findAll() {
		return genericDao.findAll();
	}

	public List<T> findAll(Sort sort) {
		return genericDao.findAll(sort);
	}
	
	public Page<T> findAll(Pageable pageable){
		return genericDao.findAll(pageable);
	}

	public <S extends T> List<S> save(Iterable<S> entities) {
		setUpdateDate(entities);
		return (List<S>) genericDao.save(entities);
	}

	public void flush() {
		genericDao.flush();
	}

	public T saveAndFlush(T entity) {
		setUpdateDate(entity);
		return (T) genericDao.saveAndFlush(entity);
	}

	public void deleteInBatch(Iterable<T> entities) {
		genericDao.delete(entities);
	}

	public void deleteAllInBatch() {
		genericDao.deleteAllInBatch();
	}

	public <S extends T> S save(S entity) {
		setUpdateDate(entity);
		return (S)genericDao.save(entity);
	}

	public T findOne(ID id) {
		return (T)genericDao.findOne(id);
	}

	public boolean exists(ID id) {
		return genericDao.exists(id);
	}

	public Iterable<T> findAll(Iterable<ID> ids) {
		return genericDao.findAll(ids);
	}

	public long count() {
		return genericDao.count();
	}

	public void delete(ID id) {
		genericDao.delete(id);
	}

	public void delete(T entity) {
		genericDao.delete(entity);
	}

	public void delete(Iterable<? extends T> entities) {
		genericDao.delete(entities);
	}

	public void deleteAll() {
		genericDao.deleteAll();
	}
	
	
	private <S extends T> S setUpdateDate(S entity){
		if(entity instanceof AuditBasicEntity){
			((AuditBasicEntity) entity).setUpdateDate(new Date());
		}
		return entity;
	}
	
	private <S extends T> List<S> setUpdateDate(Iterable<S> entities){
		Date date = new Date();
		Iterator<S> entityIterator = entities.iterator();
		while(entityIterator.hasNext()){
			S entity = entityIterator.next();
			setUpdateDate(entity);
		}
		return (List<S>) entities;
	}

	public Page<T> findByPage(Pageable pageable) {
		return genericDao.findByPage(pageable);
	}

	@Override
	public T findOneByIdAndOwnerId(ID id, ID ownerId) {
		return genericDao.findOneByIdAndOwnerId(id, ownerId);
	}
}
