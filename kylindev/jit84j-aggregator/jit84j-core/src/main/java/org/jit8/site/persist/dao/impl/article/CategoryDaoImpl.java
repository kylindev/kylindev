package org.jit8.site.persist.dao.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.article.CategoryDao;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.persist.repository.article.CategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDaoImpl extends GenericDaoImpl<Category, Long> implements CategoryDao {

	@Resource
	private CategoryRepository categoryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = categoryRepository;
	}

	@Override
	public List<Category> getCategoryList(boolean disable, boolean deleted) {
		return categoryRepository.getCategoryList(disable, deleted);
	}

	@Override
	public List<Category> getCategoryListByIds(boolean disable,
			boolean deleted, List<Long> categoryIdList) {
		return categoryRepository.getCategoryListByIds(disable, deleted, categoryIdList);
	}

	@Override
	public List<Category> getCategoryListByParentNull() {
		return categoryRepository.getCategoryListByParentNull();
	}
}
