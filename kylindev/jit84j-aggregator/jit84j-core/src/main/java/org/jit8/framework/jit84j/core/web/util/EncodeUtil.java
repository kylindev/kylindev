package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.shiro.crypto.hash.SimpleHash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EncodeUtil implements Serializable {

	private static final long serialVersionUID = 3928859081735832665L;

	private static final Logger logger = LoggerFactory.getLogger(EncodeUtil.class);

	public static final String DEFAULT_CONTENT_CHARSET = "utf-8";
	
	public static final String DEFAULT_MD5_ENCRYPT = "md5";
	

	public EncodeUtil() {
	}

	@SuppressWarnings("restriction")
	public static final String encoderByMd5(String str) {
		// 确定计算方法
		MessageDigest md5;
		try {
			md5 = MessageDigest.getInstance("MD5");
			sun.misc.BASE64Encoder base64en = new sun.misc.BASE64Encoder();
			// 加密后的字符串
			String newstr = base64en.encode(md5.digest(str.getBytes("utf-8")));
			return newstr;
		} catch (NoSuchAlgorithmException e) {
			logger.error("NoSuchAlgorithmException : ", e);
		} catch (UnsupportedEncodingException e) {
			logger.error("UnsupportedEncodingException : ", e);
		}
		return str;
	}

	public static final String encoderByMd5With16Hex(String str) {
		char[] temp = encoderByMd5(str).toCharArray();
		String result = "";
		for (int i = 0; i < temp.length; i++) {
			result += Integer.toHexString((0x000000ff & temp[i]) | 0xffffff00).substring(6);
		}
		return result;
	}

	public static String decode(final String content, final String encoding) {
		try {
			return URLDecoder.decode(content, encoding != null ? encoding : DEFAULT_CONTENT_CHARSET);
		} catch (UnsupportedEncodingException problem) {
			throw new IllegalArgumentException(problem);
		}
	}
	
	public static String encode(final String content, final String encoding) {
		try {
			return URLEncoder.encode(content, encoding != null ? encoding : DEFAULT_CONTENT_CHARSET);
		} catch (UnsupportedEncodingException problem) {
			throw new IllegalArgumentException(problem);
		}
	}

	public static String encodeWithByte(final String content, final String encoding) {
		try {
			String encode = URLEncoder.encode(content, encoding != null ? encoding : DEFAULT_CONTENT_CHARSET);
			byte[] charArray = encode.getBytes();
			 StringBuilder sb = new StringBuilder();
			 for(byte c : charArray){
				 sb.append(c).append("-");
			 }
			 if(sb.lastIndexOf("-")>0){
				 sb.deleteCharAt(sb.lastIndexOf("-"));
			 }
			 System.out.println(sb.toString());
			return sb.toString();
		} catch (UnsupportedEncodingException problem) {
			throw new IllegalArgumentException(problem);
		}
	}
	
	public static String decodeWithByte(final String content, final String encoding) {
		try {
			 String[] bs = content.split("-");
			 int length = bs.length;
			 byte[] charArray = new byte[length];
			 StringBuilder sb = new StringBuilder();
			 for(int i=0; i<length;i++){
				 charArray[i] = Byte.parseByte(bs[i]);
			 }
			 String result = new String(charArray);
			 String decode = URLDecoder.decode(result, encoding != null ? encoding : DEFAULT_CONTENT_CHARSET);
			return decode;
		} catch (UnsupportedEncodingException problem) {
			throw new IllegalArgumentException(problem);
		}
	}
	
	public static String customEncrypt(String algorithmName, Object source, Object salt, int hashIterations){
		SimpleHash sh = new SimpleHash(algorithmName,source,salt,hashIterations) ;
		String result = sh.toHex();
		return result;
		
	}

	public static void main(String[] args) {
		System.out.println(encoderByMd5With16Hex("xiaoyanzi"));
		String encode = encodeWithByte("kylinboy521@163.com",DEFAULT_CONTENT_CHARSET);
		
		System.out.println(encode);
		
		String decode = decodeWithByte(encode,DEFAULT_CONTENT_CHARSET);
		
		System.out.println(decode);
		
	}

}
