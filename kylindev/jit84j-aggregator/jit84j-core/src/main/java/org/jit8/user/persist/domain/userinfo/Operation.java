package org.jit8.user.persist.domain.userinfo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import org.jit8.framework.jit84j.core.persistent.model.AbstractBasicPersistentEntityWithAutoGeneratedId;

@Entity
public class Operation extends AbstractBasicPersistentEntityWithAutoGeneratedId {

	private static final long serialVersionUID = -3632519133769026349L;
	
	private String name;
	private String code;
	private String description;
	
	
	@OneToMany
	private List<OpGroupOption> opGroupOptionList;

	public List<OpGroupOption> getOpGroupOptionList() {
		return opGroupOptionList;
	}

	public void setOpGroupOptionList(List<OpGroupOption> opGroupOptionList) {
		this.opGroupOptionList = opGroupOptionList;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
