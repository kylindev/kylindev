package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FileSystemGalleryRepository extends GenericJpaRepository<FileSystemGallery, Long>{

	@Query("from FileSystemGallery o where o.disable=:disable and o.deleted=:deleted")
	List<FileSystemGallery> getFileSystemGalleryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
}
