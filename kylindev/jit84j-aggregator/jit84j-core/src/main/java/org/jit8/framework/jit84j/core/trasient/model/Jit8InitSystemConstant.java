package org.jit8.framework.jit84j.core.trasient.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jit8.framework.jit84j.core.web.util.EncodeUtil;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserRole;

/**
 * 定义系统初始化配置
 * @author kylinboy
 *
 */
public final class Jit8InitSystemConstant implements Serializable {

	private static final long serialVersionUID = -13269791360551773L;

	public Jit8InitSystemConstant() {
	}
	/**
	 * 系统初始化超级用户 username，内置用户
	 * 6e6c5365446c6e587062565764485158627138374a513d3d
	 */
	public static final String USER_USERNAME_SUPER = "super_root";
	
	/**
	 * 系统初始化超级用户 email，内置用户
	 * 7a36727241704b525a654b4e766e4e4d2f444f3436513d3d
	 */
	public static final String USER_EMAIL_SUPER = "super_root@jit8.com";
	/**
	 * 系统初始化超级管理员，内置角色
	 * 6e6c5365446c6e587062565764485158627138374a513d3d
	 */
	public static final String ROLE_CODE_SUPER= "super_root";

	/**
	 * 系统初始化角色最大级别
	 */
	public static final int ROLE_LEVEL_MAX = 9999;
	
	public static User getSuperUser(){
		User user  = new User();
		user.setComeLocation("JIT84J");
		user.setCreateDate(new Date());
		user.setDeleted(true);
		user.setDisable(false);
		user.setEmail(EncodeUtil.encoderByMd5With16Hex(USER_EMAIL_SUPER));
		user.setFirstname("root");
		user.setLastname("super");
		user.setNickname("超级用户");
		user.setPassword("super_root");
		user.setPhone("");
		user.setUsername(EncodeUtil.encoderByMd5With16Hex(USER_USERNAME_SUPER));
		user.setUserType(User.USER_TYPE_SYS_INLAY);
		
		Role role = new Role();
		role.setRoleCode(ROLE_CODE_SUPER);
		role.setRolename("超级管理员");
		role.setRoleLevel(ROLE_LEVEL_MAX);
		role.setDisable(false);
		role.setDeleted(false);
		
		UserRole userRole = new UserRole();
		userRole.setUser(user);
		userRole.setRole(role);
		
		List<UserRole> roleList = new ArrayList<UserRole>();
		roleList.add(userRole);
		user.setRoleList(roleList);
		
		return user;
	}
	
	public static void main(String[] args) {
		User user = getSuperUser();
		System.out.println(user.getUsername());
		System.out.println(user.getEmail());
		
		System.out.println(EncodeUtil.encoderByMd5With16Hex("000000").length());
	}

}
