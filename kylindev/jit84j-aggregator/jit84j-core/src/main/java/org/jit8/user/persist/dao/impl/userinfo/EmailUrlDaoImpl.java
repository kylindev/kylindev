package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.EmailUrlDao;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.repository.userinfo.EmailUrlRepository;
import org.springframework.stereotype.Repository;

@Repository
public class EmailUrlDaoImpl extends GenericDaoImpl<EmailUrl, Long> implements EmailUrlDao {

	@Resource
	private EmailUrlRepository emailUrlRepository;
	
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = emailUrlRepository;
	}

	@Override
	public EmailUrl findByUrlCode(String urlCode) {
		return emailUrlRepository.findByUrlCode(urlCode);
	}


}
