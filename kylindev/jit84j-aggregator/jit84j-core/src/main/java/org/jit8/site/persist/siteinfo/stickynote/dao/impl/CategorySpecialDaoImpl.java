package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.CategorySpecialDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.repository.CategorySpecialRepository;
import org.jit8.site.persist.siteinfo.stickynote.repository.spec.CategorySpecialSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public class CategorySpecialDaoImpl extends GenericDaoImpl<CategorySpecial, Long> implements CategorySpecialDao {

	@Resource
	private CategorySpecialRepository categorySpecialRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = categorySpecialRepository;
	}

	@Override
	public List<CategorySpecial> getCategorySpecialList(boolean disable, boolean deleted) {
		return categorySpecialRepository.getCategorySpecialList(disable, deleted);
	}
	
	@Override
	public CategorySpecial findByName(String code) {
		return categorySpecialRepository.findByName( code);
	}

	@Override
	public void removeBySpecialIdAndCategoryIdList(Long specialId,
			List<Long> categoryIdList) {
		categorySpecialRepository.removeBySpecialIdAndCategoryIdList(specialId, categoryIdList);
	}

	@Override
	public List<CategorySpecial> findByIdList(List<Long> idList) {
		return categorySpecialRepository.findByIdList(idList);
	}

	@Override
	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,
			Pageable pageable) {
		Page<CategorySpecial>  page = categorySpecialRepository.findAll(CategorySpecialSpecifications.queryByCondition(categorySpecial), pageable);
		return page;
	}
	
	@Override
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id){
		return categorySpecialRepository.getCategorySpecialListBySpecialAreaId(id);
		
	}
	
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList){
		return categorySpecialRepository.getByCategorySpecialIdList(categorySpecialIdList);
	}
}
