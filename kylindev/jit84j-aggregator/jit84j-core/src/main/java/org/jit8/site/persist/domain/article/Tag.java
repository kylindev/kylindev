package org.jit8.site.persist.domain.article;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.jit8.framework.jit84j.core.persistent.model.AbstractBasicPersistentEntityWithAutoGeneratedId;

@Entity
public class Tag extends AbstractBasicPersistentEntityWithAutoGeneratedId {

	private static final long serialVersionUID = 1L;

	@OneToMany(mappedBy="tag")
	private List<ArticleTag> articleTagList;
	
	private String name;
	
	private String capitalIndex;
	
	@Transient
	private Long id;
	
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}

	public List<ArticleTag> getArticleTagList() {
		return articleTagList;
	}

	public void setArticleTagList(List<ArticleTag> articleTagList) {
		this.articleTagList = articleTagList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCapitalIndex() {
		return capitalIndex;
	}
	public void setCapitalIndex(String capitalIndex) {
		this.capitalIndex = capitalIndex;
	}


	
}
