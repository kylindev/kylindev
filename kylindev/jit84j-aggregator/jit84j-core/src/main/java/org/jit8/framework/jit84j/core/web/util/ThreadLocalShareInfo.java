package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;

import org.jit8.user.persist.domain.userinfo.User;

public class ThreadLocalShareInfo implements Serializable{

	private static final long serialVersionUID = 8885593341950527705L;
	
	private Long currentUserOid;
	
	private User user;
	
	//http request
	private String jsessionId;
	private String requestPath;
	private String protocol;
	private String requestMethod;
	private String clientIp;
	private String referer;
	private String userAgent;
	private boolean sysSuperRoot;
	
	

	public Long getCurrentUserOid() {
		return currentUserOid;
	}

	public void setCurrentUserOid(Long currentUserOid) {
		this.currentUserOid = currentUserOid;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getJsessionId() {
		return jsessionId;
	}

	public void setJsessionId(String jsessionId) {
		this.jsessionId = jsessionId;
	}

	public String getRequestPath() {
		return requestPath;
	}

	public void setRequestPath(String requestPath) {
		this.requestPath = requestPath;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getUserAgent() {
		return userAgent;
	}

	public void setUserAgent(String userAgent) {
		this.userAgent = userAgent;
	}

	public boolean isSysSuperRoot() {
		return sysSuperRoot;
	}

	public void setSysSuperRoot(boolean sysSuperRoot) {
		this.sysSuperRoot = sysSuperRoot;
	}
}
