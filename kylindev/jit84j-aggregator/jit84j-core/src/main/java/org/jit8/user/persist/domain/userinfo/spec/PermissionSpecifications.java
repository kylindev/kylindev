package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.user.persist.domain.userinfo.Developer_;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.Permission_;
import org.jit8.user.persist.domain.userinfo.UserId_;
import org.springframework.data.jpa.domain.Specification;

public class PermissionSpecifications {

	public static Specification<Permission> queryByCondition(final Permission permission) {
		return new Specification<Permission>() {
			public Predicate toPredicate(Root<Permission> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				List<Predicate> predicateList = new ArrayList<Predicate>();
				String code = permission.getCode();
				if(StringUtils.isNotEmpty(code)){
					predicateList.add(builder.like(root.get(Permission_.code),Specifications.PERCENT+code+Specifications.PERCENT));
				}
				String name = permission.getName();
				if(StringUtils.isNotEmpty(name)){
					predicateList.add(builder.like(root.get(Permission_.name),Specifications.PERCENT+name+Specifications.PERCENT));
				}
				
				String opration = permission.getOpration();
				if(StringUtils.isNotEmpty(opration)){
					predicateList.add(builder.like(root.get(Permission_.opration),Specifications.PERCENT+opration+Specifications.PERCENT));
				}
				
				String fullUrl = permission.getFullUrl();
				if(StringUtils.isNotEmpty(fullUrl)){
					predicateList.add(builder.like(root.get(Permission_.fullUrl),Specifications.PERCENT+fullUrl+Specifications.PERCENT));
				}
				
				String resourceUrl = permission.getResourceUrl();
				if(StringUtils.isNotEmpty(resourceUrl)){
					predicateList.add(builder.like(root.get(Permission_.resourceUrl),resourceUrl));
				}
				
				Long developerId = permission.getDeveloperId();
				if(null != developerId && developerId>0){
					predicateList.add(builder.equal(root.get(Permission_.developer).get(Developer_.userId).get(UserId_.userId),developerId));
				}
				
				Long categoryDropdownValue = permission.getCategoryDropdownValue();
				if(null != categoryDropdownValue && categoryDropdownValue>0){
					if(Permission.PERMISSION_CATEGORY_DROPDWON_UN_CATEGORIED.equals(categoryDropdownValue)){
						predicateList.add(builder.isNull(root.get(Permission_.category)));
					}else{
						predicateList.add(builder.isNotNull(root.get(Permission_.category)));
					}
				}
				//query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				
				return  builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
