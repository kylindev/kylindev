package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.DeveloperDao;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.spec.DeveloperSpecifications;
import org.jit8.user.persist.repository.userinfo.DeveloperRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class DeveloperDaoImpl extends GenericDaoImpl<Developer, Long> implements DeveloperDao {

	@Resource
	private DeveloperRepository developerRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = developerRepository;
	}

	@Override
	public Page<Developer> getDeveloperList(Pageable pageable) {
		return developerRepository.getDeveloperList(pageable);
	}

	@Override
	public Developer findByUserId(Long userId) {
		return developerRepository.findByUserId(userId);
	}

	@Override
	public Page<Developer> getDeveloperList(Developer developer,
			Pageable pageable) {
		
		return developerRepository.findAll(DeveloperSpecifications.queryByCondition(developer), pageable);
	}
}
