package org.jit8.framework.jit84j.core.mail.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Transient;

import org.jit8.framework.jit84j.core.persistent.model.AbstractBasicPersistentEntityWithAutoGeneratedId;

@Entity
public class MailTemplate extends AbstractBasicPersistentEntityWithAutoGeneratedId {

	private static final long serialVersionUID = -3602121831429191221L;

	public static final String TEMPLATE_PATH_FIND_PASSWORD = "find_password.ftl";
	public static final String TEMPLATE_NAME_FIND_PASSWORD = "找回密码";
	
	public static final String TEMPLATE_PATH_RESET_PASSWORD_BY_SYS = "reset_password.ftl";
	public static final String TEMPLATE_NAME_RESET_PASSWORD_BY_SYS = "重置密码";
	
	public static final String TEMPLATE_PATH_RESET_PASSWORD_SUCCESS = "reset_password_success.ftl";
	public static final String TEMPLATE_NAME_RESET_PASSWORD_SUCCESS = "重置密码成功";
	
	public static final String TEMPLATE_PATH_ACTIVE_USER = "active_user.ftl";
	public static final String TEMPLATE_NAME_ACTIVE_USER = "激活用户";
	
	public static final String TEMPLATE_PATH_ACTIVE_USER_SUCCESS = "active_user_success.ftl";
	public static final String TEMPLATE_NAME_ACTIVE_USER_SUCCESS = "激活用户成功";
	
	
	private String templateKey;//模板key
	private String name;//模板名称
	private String path;//模板路径
	private String subject;//邮件模板标题
	
	private String htmlPath;//模板生成的html存储访问目录
	private String htmlRealPath;//模板生成的html存储绝对目录，由系统自动检测
	
	@Transient
	private Long id;
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}
	
	@Transient
	private List<Long> idList;
	
	public List<Long> getIdList() {
		return idList;
	}

	public void setIdList(List<Long> idList) {
		this.idList = idList;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getTemplateKey() {
		return templateKey;
	}
	public void setTemplateKey(String templateKey) {
		this.templateKey = templateKey;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getHtmlPath() {
		return htmlPath;
	}
	public void setHtmlPath(String htmlPath) {
		this.htmlPath = htmlPath;
	}
	public String getHtmlRealPath() {
		return htmlRealPath;
	}
	public void setHtmlRealPath(String htmlRealPath) {
		this.htmlRealPath = htmlRealPath;
	}

	
}
