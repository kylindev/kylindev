package org.jit8.site.persist.siteinfo.stickynote.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;

public interface StickyNoteConfigDao extends GenericDao<StickyNoteConfig, Long> {

	public List<StickyNoteConfig> getStickyNoteConfigList(boolean disable, boolean deleted) ;
}
