package org.jit8.site.persist.domain.article;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.jit8.framework.jit84j.core.persistent.model.AbstractBasicPersistentEntityWithAutoGeneratedId;

@Entity
public class ArticleImage extends AbstractBasicPersistentEntityWithAutoGeneratedId {

	private static final long serialVersionUID = 1450647327940924137L;
	
	@ManyToOne
	private Article article;
	@ManyToOne
	private Image image;
	private boolean firstImage;
	
	@Transient
	private Long id;
	
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}
	
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Image getImage() {
		return image;
	}
	public void setImage(Image image) {
		this.image = image;
	}
	public boolean isFirstImage() {
		return firstImage;
	}
	public void setFirstImage(boolean firstImage) {
		this.firstImage = firstImage;
	}
}
