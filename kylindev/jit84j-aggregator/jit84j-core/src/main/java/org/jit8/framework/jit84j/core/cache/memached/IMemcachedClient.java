package org.jit8.framework.jit84j.core.cache.memached;

import net.rubyeye.xmemcached.MemcachedClient;

public interface IMemcachedClient {
	
	MemcachedClient getClient();
}
