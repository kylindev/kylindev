package org.jit8.framework.jit84j.core.mail.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailAttachment;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;

public interface MailAttachmentDao extends GenericDao<MailAttachment, Long>{

	List<MailAttachment> getMailAttachmentList(boolean disable,boolean deleted);

}
