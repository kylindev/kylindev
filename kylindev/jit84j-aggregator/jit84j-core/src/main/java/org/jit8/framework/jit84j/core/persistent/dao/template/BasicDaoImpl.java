package org.jit8.framework.jit84j.core.persistent.dao.template;

import java.io.Serializable;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class BasicDaoImpl<T, ID extends Serializable> extends GenericDaoImpl<T, ID> implements BasicDao<T, ID> {

}
