package org.jit8.site.persist.siteinfo.stickynote.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;

public interface SpecialAreaDao extends GenericDao<SpecialArea, Long> {

	public List<SpecialArea> getSpecialAreaList(boolean disable, boolean deleted) ;
	
	public SpecialArea findByCode(String code);
	
	public List<SpecialArea> findByIds(List<Long> idList);
}
