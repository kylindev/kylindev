package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.Developer_;
import org.jit8.user.persist.domain.userinfo.OwnRelation_;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.persist.domain.userinfo.UserId_;
import org.jit8.user.persist.domain.userinfo.User_;
import org.springframework.data.jpa.domain.Specification;



public class DeveloperSpecifications {


	public static Specification<Developer> queryByCondition(final Developer developer) {
		return new Specification<Developer>() {
			public Predicate toPredicate(Root<Developer> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
						User user = developer.getUser();
						if(null != user){
							String username = user.getUsername();
							if(StringUtils.isNotEmpty(username)){
								predicateList.add(builder.like(root.get(Developer_.user).get(User_.username),username + Specifications.PERCENT));
							}
							
							String email = user.getEmail();
							if(StringUtils.isNotEmpty(email)){
								predicateList.add(builder.like(root.get(Developer_.user).get(User_.email),email + Specifications.PERCENT));
							}
							
							String nickname = user.getNickname();
							if(StringUtils.isNotEmpty(nickname)){
								predicateList.add(builder.like(root.get(Developer_.user).get(User_.nickname),nickname + Specifications.PERCENT));
							}
							
							String firstname = user.getFirstname();
							if(StringUtils.isNotEmpty(firstname)){
								predicateList.add(builder.like(root.get(Developer_.user).get(User_.firstname),firstname + Specifications.PERCENT));
							}
							
							String lastname = user.getLastname();
							if(StringUtils.isNotEmpty(lastname)){
								predicateList.add(builder.like(root.get(Developer_.user).get(User_.lastname),lastname + Specifications.PERCENT));
							}
							
							Long ownRelationId = user.getOwnRelationId();
							if(null !=ownRelationId && ownRelationId>0){
								predicateList.add(builder.equal(root.get(Developer_.user).get(User_.ownRelation).get(OwnRelation_.uniqueIdentifier),ownRelationId));
							}
						}
						
						UserId userId = developer.getUserId();
						if(null!= userId){
							Long userIdLong = userId.getUserId();
							if(null != userIdLong && userIdLong>0){
								predicateList.add(builder.equal(root.get(Developer_.userId).get(UserId_.userId),userIdLong));
							}
						}
						
				
				query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				return null;
			}
		};
	}
}
