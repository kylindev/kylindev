package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PhotographRepository extends GenericJpaRepository<Photograph, Long>{

	@Query("from Photograph o where o.disable=:disable and o.deleted=:deleted")
	List<Photograph> getPhotographList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

}
