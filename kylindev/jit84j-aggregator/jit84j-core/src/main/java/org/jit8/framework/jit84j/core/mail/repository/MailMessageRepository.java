package org.jit8.framework.jit84j.core.mail.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MailMessageRepository extends GenericJpaRepository<MailMessage, Long>{

	@Query("from MailMessage o where o.disable=:disable and o.deleted=:deleted")
	List<MailMessage> getMailMessageList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

}
