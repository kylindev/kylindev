package org.jit8.user.persist.validator;

import javax.annotation.Resource;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.jit8.user.persist.dao.userinfo.UserDao;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.util.StringUtils;

public class CheckEmailValidator implements ConstraintValidator<CheckEmail, User> {  
	  
	
	@Resource
	private UserDao userDao;
	
    @Override  
    public void initialize(CheckEmail constraintAnnotation) {  
    }  
  
    @Override  
    public boolean isValid(User user, ConstraintValidatorContext context) {  
        if(user == null) {  
            return true;  
        }  
  
        if(!StringUtils.hasText(user.getEmail())) {  
            context.disableDefaultConstraintViolation();  
            context.buildConstraintViolationWithTemplate("{user.email.empty}")  
                    .addNode("email")  
                    .addConstraintViolation();  
            return false;  
        }
  
        if(StringUtils.hasText(user.getEmail())) {  
        	if(null!=userDao && userDao.exitUserByEmail(user.getEmail())){
	            context.disableDefaultConstraintViolation();  
	            context.buildConstraintViolationWithTemplate("{user.email.unique.error}")  
	                    .addNode("email")  
	                    .addConstraintViolation();  
	            return false;  
        	}
        }  
        
        return true;  
    }  
}  
