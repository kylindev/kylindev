package org.jit8.framework.jit84j.core.persistent.dao.template;

import java.io.Serializable;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;

public interface BasicDao<T, ID extends Serializable> extends GenericDao<T, ID> {

}
