package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery_;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.jit8.site.persist.domain.siteinfo.FileInfo_;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery_;
import org.springframework.data.jpa.domain.Specification;



public class FileInfoSpecifications {

	public static Specification<FileInfo> queryByCondition(final FileInfo fileInfo) {
		return new Specification<FileInfo>() {
			public Predicate toPredicate(Root<FileInfo> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				String contentType = StringUtils.trim(fileInfo.getContentType());
				String fileAccessFullPath = StringUtils.trim(fileInfo.getFileAccessFullPath());
				String fileAccessPath = StringUtils.trim(fileInfo.getFileAccessPath());
				String fileName = StringUtils.trim(fileInfo.getFileName());
				//String fileRealPath = StringUtils.trim(fileInfo.getFileRealPath());
				String fileSuffix = StringUtils.trim(fileInfo.getFileSuffix());
				String originalFileName = StringUtils.trim(fileInfo.getOriginalFileName());
				
				Long owner = fileInfo.getOwner();
				
				predicateList.add(builder.equal(root.get(FileInfo_.deleted ), false));
				
				if(StringUtils.isNotEmpty(contentType)){
					predicateList.add(builder.like(root.get(FileInfo_.contentType ), Specifications.PERCENT + contentType + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(fileName)){
					predicateList.add(builder.like(root.get(FileInfo_.fileName ), Specifications.PERCENT + fileName + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(fileSuffix)){
					predicateList.add(builder.like(root.get(FileInfo_.fileSuffix ), Specifications.PERCENT + fileSuffix + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(originalFileName)){
					predicateList.add(builder.like(root.get(FileInfo_.originalFileName ), Specifications.PERCENT + originalFileName + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(fileAccessFullPath)){
					predicateList.add(builder.like(root.get(FileInfo_.fileAccessFullPath ), Specifications.PERCENT + fileAccessFullPath + Specifications.PERCENT));
				}
				if(StringUtils.isNotEmpty(fileAccessPath)){
					predicateList.add(builder.like(root.get(FileInfo_.fileAccessPath ), Specifications.PERCENT + fileAccessPath + Specifications.PERCENT));
				}
				
				if(null != owner && owner>0){
					predicateList.add(builder.equal(root.get(FileInfo_.owner ), owner ));
				}
				
				FileSystemGallery systemGallery = fileInfo.getSystemGallery();
				if(null != systemGallery){
					Long id = systemGallery.getId();
					if(null != id && id>0){
						predicateList.add(builder.equal(root.get(FileInfo_.systemGallery ).get(FileSystemGallery_.uniqueIdentifier), id ));
					}
					
					String code = StringUtils.trim(systemGallery.getCode());
					if(StringUtils.isNotEmpty(code)){
						predicateList.add(builder.like(root.get(FileInfo_.systemGallery ).get(FileSystemGallery_.code), Specifications.PERCENT + code + Specifications.PERCENT));
					}
					
					String name = StringUtils.trim(systemGallery.getName());
					if(StringUtils.isNotEmpty(name)){
						predicateList.add(builder.like(root.get(FileInfo_.systemGallery ).get(FileSystemGallery_.name), Specifications.PERCENT + name + Specifications.PERCENT));
					}
				}
				
				FileCustomGallery customGallery = fileInfo.getCustomGallery();
				if(null != customGallery){
					Long id = customGallery.getId();
					if(null != id && id>0){
						predicateList.add(builder.equal(root.get(FileInfo_.customGallery ).get(FileCustomGallery_.uniqueIdentifier), id ));
					}
					
					String code = StringUtils.trim(customGallery.getCode());
					if(StringUtils.isNotEmpty(code)){
						predicateList.add(builder.like(root.get(FileInfo_.customGallery ).get(FileCustomGallery_.code), Specifications.PERCENT + code + Specifications.PERCENT));
					}
					
					String name = StringUtils.trim(customGallery.getName());
					if(StringUtils.isNotEmpty(name)){
						predicateList.add(builder.like(root.get(FileInfo_.customGallery ).get(FileCustomGallery_.name), Specifications.PERCENT + name + Specifications.PERCENT));
					}
					
					Long ownerCustomGallery = customGallery.getOwner();
					if(null != ownerCustomGallery && ownerCustomGallery>0){
						predicateList.add(builder.equal(root.get(FileInfo_.owner ), ownerCustomGallery ));
					}
				}
				
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
