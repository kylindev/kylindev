package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileMimeDao;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.persist.repository.siteinfo.FileMimeRepository;
import org.jit8.user.persist.domain.userinfo.spec.FileMimeSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class FileMimeDaoImpl extends GenericDaoImpl<FileMime, Long> implements FileMimeDao {

	@Resource
	private FileMimeRepository fileMimeRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileMimeRepository;
	}

	@Override
	public List<FileMime> getFileMimeList(boolean disable, boolean deleted) {
		return fileMimeRepository.getFileMimeList(disable, deleted);
	}

	@Override
	public FileMime findByMime(String mime) {
		return fileMimeRepository.findByMime(mime);
	}

	@Override
	public List<FileMime> findByIdList(List<Long> idList) {
		return fileMimeRepository.findByIdList(idList);
	}

	@Override
	public List<FileMime> getFileMimeListByAllowable(boolean allowable) {
		return fileMimeRepository.getFileMimeListByAllowable( allowable);
	}

	@Override
	public Page<FileMime> findAll(FileMime fileMime, Pageable pageable) {
		Page<FileMime> page = fileMimeRepository.findAll(FileMimeSpecifications.queryByCondition(fileMime), pageable);
		return page;
	}
	
	
}
