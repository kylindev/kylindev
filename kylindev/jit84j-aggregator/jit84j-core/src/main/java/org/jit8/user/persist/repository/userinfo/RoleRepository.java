package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface RoleRepository extends GenericJpaRepository<Role, Long>{

	@Query("from Role o where o.deleted=false")
	Page<Role> getRoleList(Pageable pageable);
	
	@Query("from Role o where o.deleted=false and o.disable=false and o.roleLevel<=:roleLevel")
	List<Role> getRoleListByRoleLevel(@Param("roleLevel")int roleLevel);
	
	
	@Query("from Role o where o.uniqueIdentifier in :roleIdList")
	public List<Role> getRoleListByRoleIdList(@Param("roleIdList")List<Long> roleIdList); 
	
}
