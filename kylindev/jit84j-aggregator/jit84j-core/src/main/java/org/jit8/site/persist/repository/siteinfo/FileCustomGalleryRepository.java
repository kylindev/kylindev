package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FileCustomGalleryRepository extends GenericJpaRepository<FileCustomGallery, Long>{

	@Query("from FileCustomGallery o where o.disable=:disable and o.deleted=:deleted")
	List<FileCustomGallery> getFileCustomGalleryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from FileCustomGallery o where o.code=:code and o.type=:type")
	public FileCustomGallery findByCodeAndType(@Param("code")String code, @Param("type")int type);
}
