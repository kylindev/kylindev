package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StickyNoteRepository extends GenericJpaRepository<StickyNote, Long>{

	@Query("from StickyNote o where o.disable=:disable and o.deleted=:deleted")
	List<StickyNote> getStickyNoteList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

}
