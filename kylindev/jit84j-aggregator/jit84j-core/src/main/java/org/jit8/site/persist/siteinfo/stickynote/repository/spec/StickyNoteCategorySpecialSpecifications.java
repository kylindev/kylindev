package org.jit8.site.persist.siteinfo.stickynote.repository.spec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial_;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory_;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote_;
import org.springframework.data.jpa.domain.Specification;



public class StickyNoteCategorySpecialSpecifications {

	public static Specification<StickyNoteCategorySpecial> queryByCondition(final StickyNoteCategorySpecial stickyNoteCategorySpecial) {
		return new Specification<StickyNoteCategorySpecial>() {
			public Predicate toPredicate(Root<StickyNoteCategorySpecial> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				StickyNote stickyNote = stickyNoteCategorySpecial.getStickyNote();
				
				
				predicateList.add(builder.equal(root.get(StickyNoteCategorySpecial_.deleted ), false));
				
				Integer minLevel = stickyNoteCategorySpecial.getMinLevel();
				Integer maxLevel = stickyNoteCategorySpecial.getMaxLevel();
				
				Date minPushlishTime = stickyNoteCategorySpecial.getMinPublishTime();
				
				Date maxPushlishTime = stickyNoteCategorySpecial.getMaxPublishTime();
				
				
				
				if(null != stickyNote){
					String title = StringUtils.trim(stickyNote.getTitle());
					String content = StringUtils.trim(stickyNote.getContent());
					String summary = StringUtils.trim(stickyNote.getSummary());
					
					String author = stickyNote.getAuthor();
					String imagePath = stickyNote.getImagePath();
					
					if(StringUtils.isNotEmpty(title)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.title), Specifications.PERCENT + title + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(summary)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.summary), Specifications.PERCENT + summary + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(author)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.author), Specifications.PERCENT + author + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(imagePath)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.imagePath), Specifications.PERCENT + imagePath + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(content)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.content), Specifications.PERCENT + content + Specifications.PERCENT));
					}
					
					if(null != minPushlishTime){
						predicateList.add(builder.greaterThan(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.publishTime), minPushlishTime));
					}
					if(null != maxPushlishTime){
						predicateList.add(builder.lessThan(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.publishTime), maxPushlishTime));
					}
				}
				
				if(minLevel != null){
					predicateList.add(builder.ge(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.level), minLevel));
				}
				
				if(null != maxLevel && maxLevel > 0){
					predicateList.add(builder.le(root.get(StickyNoteCategorySpecial_.stickyNote).get(StickyNote_.level), maxLevel));
				}
				
				CategorySpecial categorySpecial = stickyNoteCategorySpecial.getCategorySpecial();
				if(null != categorySpecial){
					String name = StringUtils.trim(categorySpecial.getName());
					if(StringUtils.isNotEmpty(name)){
						predicateList.add(builder.like(root.get(StickyNoteCategorySpecial_.categorySpecial).get(CategorySpecial_.name), Specifications.PERCENT + name + Specifications.PERCENT));
					}
					
					StickyNoteCategory stickyNoteCategory = categorySpecial.getCategory();
					if(null != stickyNoteCategory){
						if(stickyNoteCategory.getId() != null){
							predicateList.add(builder.equal(root.get(StickyNoteCategorySpecial_.categorySpecial).get(CategorySpecial_.category).get(StickyNoteCategory_.uniqueIdentifier), stickyNoteCategory.getId()));
						}
					}
					SpecialArea specialArea = categorySpecial.getSpecialArea();
					if(null != specialArea){
						if(null != specialArea.getId()){
						predicateList.add(builder.equal(root.get(StickyNoteCategorySpecial_.categorySpecial).get(CategorySpecial_.specialArea).get(SpecialArea_.uniqueIdentifier), specialArea.getId()));
						}
					}
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
