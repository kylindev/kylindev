package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.springframework.data.jpa.domain.Specification;



public class ConstantModelSpecifications {


	public static Specification<ConstantModel> queryByCondition(final ConstantModel constantModel) {
		return new Specification<ConstantModel>() {
			public Predicate toPredicate(Root<ConstantModel> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				if(predicateList.size()>0){
					builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
					//query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
