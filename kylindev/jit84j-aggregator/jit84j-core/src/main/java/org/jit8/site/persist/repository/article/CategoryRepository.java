package org.jit8.site.persist.repository.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.article.Category;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CategoryRepository extends GenericJpaRepository<Category, Long>{

	@Query("from Category o where o.disable=:disable and o.deleted=:deleted")
	List<Category> getCategoryList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from Category o where o.disable=:disable and o.deleted=:deleted and o.uniqueIdentifier in :categoryIdList")
	List<Category> getCategoryListByIds(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("categoryIdList")List<Long> categoryIdList);
	
	@Query("from Category o where o.disable=false and o.deleted=false and o.parent is null")
	public List<Category> getCategoryListByParentNull();
}
