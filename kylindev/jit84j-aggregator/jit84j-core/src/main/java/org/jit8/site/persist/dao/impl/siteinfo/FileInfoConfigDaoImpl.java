package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileInfoConfigDao;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;
import org.jit8.site.persist.repository.siteinfo.FileInfoConfigRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FileInfoConfigDaoImpl extends GenericDaoImpl<FileInfoConfig, Long> implements FileInfoConfigDao {

	@Resource
	private FileInfoConfigRepository fileInfoConfigRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileInfoConfigRepository;
	}

	@Override
	public List<FileInfoConfig> getFileInfoConfigList(boolean disable, boolean deleted) {
		return fileInfoConfigRepository.getFileInfoConfigList(disable, deleted);
	}
}
