package org.jit8.user.persist.dao.userinfo;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.DirectPermission;

public interface DirectPermissionDao extends GenericDao<DirectPermission, Long> {

	public DirectPermission findByType(String type);
}
