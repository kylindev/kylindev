package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface PermissionCategoryRepository extends GenericJpaRepository<PmnCategory, Long>{

	@Query("from PmnCategory p where p.parent is null order by p.sequence asc")
	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable);
	
	@Query("delete from PmnCategory p where p.uniqueIdentifier = :permissionId")
	public void removeAllPermissionFromCategory(@Param("permissionId") Long permissionId);
	
}
