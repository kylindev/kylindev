package org.jit8.site.persist.siteinfo.stickynote.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface CategorySpecialDao extends GenericDao<CategorySpecial, Long> {

	public List<CategorySpecial> getCategorySpecialList(boolean disable, boolean deleted) ;
	
	public CategorySpecial findByName(String code);
	
	public void removeBySpecialIdAndCategoryIdList(Long specialId, List<Long> categoryIdList);
	
	public List<CategorySpecial> findByIdList(List<Long> idList) ;

	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,
			Pageable pageable);
	
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id);
	
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList);
}
