package org.jit8.user.persist.dao.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.UserPermission;

public interface UserPermissionDao extends GenericDao<UserPermission, Long> {

	public List<UserPermission> findByPermissionId(Long permissionId);
}
