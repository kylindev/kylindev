package org.jit8.framework.jit84j.core.mail.service;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.service.GenericService;

public interface MailConfigService extends GenericService<MailConfig, Long>{

	List<MailConfig> getMailConfigList(boolean disable,boolean deleted);
	
	public MailConfig findByType(String type);

}
