package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserRepository extends GenericJpaRepository<User, Long>{

	User findUserByUsername(String username);
	
	
	@Query("from User o where o.email=:email")
	User findUserByEmail(@Param("email")String email);
	
	@Query("from User o where o.email=:email and o.password=:password")
	User findUserByEmailAndPassword(@Param("email")String email, @Param("password")String password);
	
	@Query("from User o where o.deleted=false"
			+ " and (o.username=:username or o.phone=:phone or o.email=:email) and o.password=:password")
	User findUserByPassword(@Param("username")String username,@Param("phone")String phone, @Param("email")String email, @Param("password")String password
			);
	
	@Query("from User o where o.deleted=false and o.disable=false and o.locked=false")
	public Page<User> getLastUserList(Pageable pageable);
	
	
	@Query("from User o where o.deleted=false")
	Page<User> getUserList(Pageable pageable);
	
	@Query("from User o where o.uniqueIdentifier in :userIdList")
	public List<User> getUserListByUserIdList(@Param("userIdList")List<Long> userIdList);
	
	
}
