package org.jit8.site.persist.siteinfo.stickynote.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SpecialAreaRepository extends GenericJpaRepository<SpecialArea, Long>{

	@Query("from SpecialArea o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<SpecialArea> getSpecialAreaList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from SpecialArea o where o.code=:code")
	public SpecialArea findByCode(@Param("code")String code);
	
	@Query("from SpecialArea o where o.uniqueIdentifier in :idList")
	public List<SpecialArea> findByIds(@Param("idList")List<Long> idList);
}
