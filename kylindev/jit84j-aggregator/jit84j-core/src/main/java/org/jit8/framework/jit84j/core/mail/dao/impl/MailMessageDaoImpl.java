package org.jit8.framework.jit84j.core.mail.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailMessageDao;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.mail.repository.MailMessageRepository;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.springframework.stereotype.Repository;

@Repository
public class MailMessageDaoImpl extends GenericDaoImpl<MailMessage, Long> implements MailMessageDao {

	@Resource
	private MailMessageRepository mailMessageRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = mailMessageRepository;
	}

	@Override
	public List<MailMessage> getMailMessageList(boolean disable, boolean deleted) {
		return mailMessageRepository.getMailMessageList(disable, deleted);
	}
}
