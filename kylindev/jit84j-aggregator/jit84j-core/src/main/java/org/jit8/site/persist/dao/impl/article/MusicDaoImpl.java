package org.jit8.site.persist.dao.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.article.MusicDao;
import org.jit8.site.persist.domain.article.Music;
import org.jit8.site.persist.repository.article.MusicRepository;
import org.springframework.stereotype.Repository;

@Repository
public class MusicDaoImpl extends GenericDaoImpl<Music, Long> implements MusicDao {

	@Resource
	private MusicRepository musicRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = musicRepository;
	}

	@Override
	public List<Music> getMusicList(boolean disable, boolean deleted) {
		return musicRepository.getMusicList(disable, deleted);
	}
}
