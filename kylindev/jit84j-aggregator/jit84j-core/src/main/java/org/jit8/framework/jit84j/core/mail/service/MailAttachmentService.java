package org.jit8.framework.jit84j.core.mail.service;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailAttachment;
import org.jit8.framework.jit84j.core.service.GenericService;

public interface MailAttachmentService extends GenericService<MailAttachment, Long>{

	List<MailAttachment> getMailAttachmentList(boolean disable,boolean deleted);

}
