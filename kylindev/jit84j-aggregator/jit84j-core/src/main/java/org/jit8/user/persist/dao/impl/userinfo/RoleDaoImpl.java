package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.RoleDao;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.spec.RoleSpecifications;
import org.jit8.user.persist.repository.userinfo.RoleRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDaoImpl extends GenericDaoImpl<Role, Long> implements RoleDao {

	@Resource
	private RoleRepository roleRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = roleRepository;
	}

	@Override
	public List<Role> getRoleListByRoleLevel(int roleLevel) {
		return roleRepository.getRoleListByRoleLevel(roleLevel);
	}

	@Override
	public List<Role> getRoleListByRoleIdList(List<Long> roleIdList) {
		return roleRepository.getRoleListByRoleIdList(roleIdList);
	}

	@Override
	public Page<Role> getRoleList(Pageable pageable) {
		return roleRepository.getRoleList(pageable);
	}

	@Override
	public List<Role> saveRoleList(List<Role> roleList) {
		return null;
	}

	@Override
	public Page<Role> getRoleList(Role role, Pageable pageable) {
		return roleRepository.findAll(RoleSpecifications.likeRolenameAndRoleCodeAndLessMaxRoleLevel(role.getRolename(), role.getRoleCode(), role.getRoleLevel()), pageable);
	}
}
