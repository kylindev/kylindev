package org.jit8.framework.jit84j.core.mail.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailAttachmentDao;
import org.jit8.framework.jit84j.core.mail.domain.MailAttachment;
import org.jit8.framework.jit84j.core.mail.service.MailAttachmentService;
import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class MailAttachmentServiceImpl extends GenericServiceImpl<MailAttachment, Long> implements MailAttachmentService {

	@Resource
	private MailAttachmentDao mailAttachmentDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = mailAttachmentDao;
	}


	@Override
	public List<MailAttachment> getMailAttachmentList(boolean disable, boolean deleted) {
		return mailAttachmentDao.getMailAttachmentList(disable, deleted);
	}
	
}
