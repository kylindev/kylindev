package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.ConstantModelDao;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.spec.ConstantModelSpecifications;
import org.jit8.user.persist.repository.userinfo.ConstantModelRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ConstantModelDaoImpl extends GenericDaoImpl<ConstantModel, Long> implements ConstantModelDao {

	@Resource
	private ConstantModelRepository constantModelRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = constantModelRepository;
	}

	@Override
	public Page<ConstantModel> getConstantModelList(Pageable pageable) {
		return constantModelRepository.getConstantModelList(pageable);
	}


	@Override
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,
			Pageable pageable) {
		
		return constantModelRepository.findAll(ConstantModelSpecifications.queryByCondition(constantModel), pageable);
	}
}
