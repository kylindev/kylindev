package org.jit8.user.persist.dao.userinfo;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PermissionCategoryDao extends GenericDao<PmnCategory, Long> {

	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable);
	
	public void removeAllPermissionFromCategory(Long permissionId);
}
