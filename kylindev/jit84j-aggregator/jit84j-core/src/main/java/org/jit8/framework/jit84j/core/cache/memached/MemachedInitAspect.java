package org.jit8.framework.jit84j.core.cache.memached;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MemachedInitAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemachedInitAspect.class); 
	
		
	public MemachedInitAspect() {
	}
	
	 @Pointcut("(execution(* org.jit8.**.business.impl.**.*BusinessImpl.find*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.get*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.obtain*(..))"
	 		+ ") && @annotation(org.jit8.framework.jit84j.core.cache.memached.MemachedInit) ")  
    public void pointcut() {  
        // 定义一个pointcut，下面用Annotation标注的通知方法可以公用这个pointcut  
    }  
	 
	
	 
	// 环绕通知  
    @Around("pointcut()")  
    public Object memachedInitAroundAdvice(ProceedingJoinPoint jionpoint)  
            throws Throwable {  
        // 获取被调用的方法名  
        String targetMethodName = jionpoint.getSignature().getName();  
        
        
        LOGGER.info("check memcached begin for " + targetMethodName);  
        MethodSignature methodSignature = (MethodSignature)jionpoint.getSignature();  
        Method targetMethod = methodSignature.getMethod();
        MemachedInit memachedInit = targetMethod.getAnnotation(MemachedInit.class);
        
        Object value = null;
        /*if(null != memachedInit){
        	value = MemachedDefiner.get(memachedInit.value());
        }
        if(null != value){
        	LOGGER.info("check memcached has it , return directly " + targetMethodName);  
        	return value;
        }*/
        //获取缓存初始化注解，如果存在，把当前方法的执行结果放入缓存
        LOGGER.info("环绕通知begin: " + targetMethodName);  
        value = jionpoint.proceed();  
        String logInfoText = "环绕通知end：" + targetMethodName;  
        LOGGER.info(logInfoText);  
        
        if(null != memachedInit){
        	MemachedDefiner.set(memachedInit, value);
        }
        LOGGER.info("check memcached end for " + targetMethodName);  
        
        return value;  
    } 

}
