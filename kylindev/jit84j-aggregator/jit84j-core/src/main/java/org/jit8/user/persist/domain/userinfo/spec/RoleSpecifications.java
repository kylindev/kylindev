package org.jit8.user.persist.domain.userinfo.spec;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.Role_;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.User_;
import org.springframework.data.jpa.domain.Specification;



public class RoleSpecifications {

	public static Specification<User> eqUsername(final String username) {
		return new Specification<User>() {
			public Predicate toPredicate(Root<User> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				return  builder.equal(root.get(User_.username),username);
			}
		};
	}
	
	public static Specification<Role> likeRolenameAndRoleCodeAndLessMaxRoleLevel(final String rolename, final String roleCode, final int roleMaxLevel) {
		return new Specification<Role>() {
			public Predicate toPredicate(Root<Role> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				if(StringUtils.isNotEmpty(rolename)){
					predicateList.add(builder.like(root.get(Role_.rolename),rolename + Specifications.PERCENT));
				}
				
				if(StringUtils.isNotEmpty(roleCode)){
					predicateList.add(builder.like(root.get(Role_.roleCode),roleCode + Specifications.PERCENT));
				}
				
				if(roleMaxLevel < Role.ROLE_MAX_LEVEL){
					predicateList.add(builder.lessThanOrEqualTo(root.get(Role_.roleLevel),roleMaxLevel));
				}else{
					predicateList.add(builder.lessThanOrEqualTo(root.get(Role_.roleLevel),Role.ROLE_DEFAULT_LEVEL));
				}
				
				predicateList.add(builder.equal(root.get(Role_.deleted), false));
				
				query.where(predicateList.toArray(new Predicate[predicateList.size()]));
				
				return null;
			}
		};
	}

}
