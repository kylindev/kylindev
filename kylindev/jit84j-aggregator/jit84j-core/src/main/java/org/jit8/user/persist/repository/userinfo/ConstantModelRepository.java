package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;


public interface ConstantModelRepository extends GenericJpaRepository<ConstantModel, Long>{

	@Query("from ConstantModel o where o.deleted=false")
	Page<ConstantModel> getConstantModelList(Pageable pageable);
	
}
