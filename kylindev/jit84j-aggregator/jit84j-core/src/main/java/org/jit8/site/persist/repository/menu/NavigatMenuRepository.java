package org.jit8.site.persist.repository.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NavigatMenuRepository extends GenericJpaRepository<NavigatMenu, Long>{
	
	@Query("from NavigatMenu o where o.disable=:disable and o.deleted=:deleted")
	List<NavigatMenu> getNavigatMenuList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from NavigatMenu o where o.disable=:disable and o.deleted=:deleted and o.uniqueIdentifier in :navigatMenuIdList")
	List<NavigatMenu> getNavigatMenuListByIds(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("navigatMenuIdList")List<Long> navigatMenuIdList);
	
	@Query("from NavigatMenu o where o.disable=false and o.deleted=false and o.parent is null")
	public List<NavigatMenu> getNavigatMenuListByParentNull();
	
	@Query("from NavigatMenu o where o.disable=:disable and o.deleted=:deleted and o.code=:code")
	public List<NavigatMenu> getNavigatMenuByCode(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("code")String code);
	
	@Query("from NavigatMenu o where o.disable=:disable and o.deleted=:deleted and o.parent.code=:code order by o.sequence asc")
	public List<NavigatMenu> getNavigatMenuByTopCode(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("code")String code);

	@Query("from NavigatMenu o where o.disable=false and o.deleted=false and o.code=:code")
	public NavigatMenu findByCode(@Param("code")String code);
}
