package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.Gallery;

public interface GalleryDao extends GenericDao<Gallery, Long> {

	public List<Gallery> getGalleryList(boolean disable, boolean deleted) ;
}
