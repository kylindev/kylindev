package org.jit8.site.persist.repository.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.article.Music;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MusicRepository extends GenericJpaRepository<Music, Long>{

	@Query("from Music o where o.disable=:disable and o.deleted=:deleted")
	List<Music> getMusicList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

}
