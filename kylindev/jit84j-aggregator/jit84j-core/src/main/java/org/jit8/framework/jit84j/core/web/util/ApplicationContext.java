package org.jit8.framework.jit84j.core.web.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContext implements ApplicationContextAware {

	private static ApplicationContextThreadLocal applicationContextThreadLocal = new ApplicationContextThreadLocal();

	private static org.springframework.context.ApplicationContext applicationContext = null;

	public static ApplicationContext getContext() {

		ApplicationContext context = (ApplicationContext) applicationContextThreadLocal.get();
		if (context == null) {
			context = new ApplicationContext();
			setContext(context);
		}
		return context;

	}

	public static void setContext(ApplicationContext context) {
		applicationContextThreadLocal.set(context);
	}

	private ThreadLocalShareInfo shareInfo;

	public ThreadLocalShareInfo getShareInfo() {
		return shareInfo;
	}

	public void setShareInfo(ThreadLocalShareInfo shareInfo) {
		this.shareInfo = shareInfo;
	}

	@Override
	public void setApplicationContext(
			org.springframework.context.ApplicationContext applicationContext)
			throws BeansException {
		ApplicationContext.applicationContext = applicationContext;
	}

	public static org.springframework.context.ApplicationContext getApplicationContext() {
		return ApplicationContext.applicationContext;
	}

	public static Object getBean(Class clazz) {
		if (null != clazz) {
			Object bean = getApplicationContext().getBean(clazz);
			return bean;
		}
		return null;

	}
}
