package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang.StringUtils;

public class CalendarUtils implements Serializable {

	private static final long serialVersionUID = 2714076999939132622L;
	
	private static final long ONE_DAY = 24*60*60*1000;
	private static final long ONE_HOUR = 60*60*1000;
	private static final long ONE_MINUTE = 60*1000;
	private static final long ONE_SECOND = 1000;

	public CalendarUtils() {
	}

	public static Date getBeforeOrAfterDate(Date date, int year, int month,
			int day) {

		if (null != date) {
			Calendar rightNow = Calendar.getInstance();
			rightNow.setTime(date);
			if (year != 0) {
				rightNow.add(Calendar.YEAR, year);// 日期减1年
			}
			if (month != 0) {
				rightNow.add(Calendar.MONTH, month);// 日期加3个月
			}
			if (day != 0) {
				rightNow.add(Calendar.DAY_OF_YEAR, day);// 日期加10天
			}
			Date newDate = rightNow.getTime();
			return newDate;
		}
		return date;
	}

	public static Date getBeforeOrAfterDateForDay(Date date, int day) {
		return getBeforeOrAfterDate(date, 0, 0, day);
	}
	
	
	public static Date getTodayBegin() {
		Calendar rightNow = Calendar.getInstance();
		int year = rightNow.get(Calendar.YEAR);// 获取年份
		int month = rightNow.get(Calendar.MONTH);// 获取月份
		int day = rightNow.get(Calendar.DATE);// 获取日
		//int minute = rightNow.get(Calendar.MINUTE);// 分
		//int hour = rightNow.get(Calendar.HOUR);// 小时
		//int second = rightNow.get(Calendar.SECOND);// 秒
		rightNow.set(year, month, day, 0, 0, 0);
		return rightNow.getTime();
	}
	
	public static Date getTodayEnd() {
		Calendar rightNow = Calendar.getInstance();
		int year = rightNow.get(Calendar.YEAR);// 获取年份
		int month = rightNow.get(Calendar.MONTH);// 获取月份
		int day = rightNow.get(Calendar.DATE);// 获取日
		//int minute = rightNow.get(Calendar.MINUTE);// 分
		//int hour = rightNow.get(Calendar.HOUR);// 小时
		//int second = rightNow.get(Calendar.SECOND);// 秒
		rightNow.set(year, month, day, 23, 59, 59);
		return rightNow.getTime();
	}

	public static Date getSpecificDate(String specificDate, String format) {

		if (StringUtils.isEmpty(format)) {
			format = "yyyyMMddHHmmss";
		}
		SimpleDateFormat sf = new SimpleDateFormat(format);
		Date date = null;
		try {
			date = sf.parse(specificDate);
			return date;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;

	}
	
	public static Date getSpecificDateBegin(String yearMonthDay, String format) {

		if (StringUtils.isEmpty(format)) {
			format = "yyyyMMddHHmmss";
		}
		String specificDate = yearMonthDay + "000000";
		Date date = getSpecificDate(specificDate,format);
		return date;

	}
	
	public static Date getSpecificDateEnd(String yearMonthDay, String format) {

		if (StringUtils.isEmpty(format)) {
			format = "yyyyMMddHHmmss";
		}
		String specificDate = yearMonthDay + "235959";
		Date date = getSpecificDate(specificDate,format);
		return date;

	}
	
	public   static   void  calcToday(String begin,String end,String now,GregorianCalendar calendar) {
         
         begin = now;
         end = now;
         System.out.println( " begin: " + begin);
         System.out.println( " end: " + end);
         System.out.println( " ---------------------- " );    
     } 
 
	public static void calcYesterday(String begin, String end, String now,
			GregorianCalendar calendar) {

		calendar.add(GregorianCalendar.DATE, -1);
		begin = new java.util.Date(calendar.getTime().getTime()).toString();
		end = begin;
		System.out.println(" begin: " + begin);
		System.out.println(" end: " + end);
		System.out.println(" ---------------------- ");
	}

	public static void calcThisWeek(String begin, String end, String now,
			GregorianCalendar calendar) {
		end = now;
		int minus = calendar.get(GregorianCalendar.DAY_OF_WEEK) - 2;
		if (minus < 0) {
			System.out.println(" 本周还没有开始，请查询上周 ");
			System.out.println(" ---------------------- ");
		} else {

			calendar.add(GregorianCalendar.DATE, -minus);
			begin = new java.util.Date(calendar.getTime().getTime()).toString();
			System.out.println(" begin: " + begin);
			System.out.println(" end: " + end);
			System.out.println(" ---------------------- ");
		}
	}

	public static void calcLastWeek(String begin, String end, String now,
			GregorianCalendar calendar) {
		int minus = calendar.get(GregorianCalendar.DAY_OF_WEEK) + 1;
		calendar.add(GregorianCalendar.DATE, -minus);
		end = new java.sql.Date(calendar.getTime().getTime()).toString();
		calendar.add(GregorianCalendar.DATE, -4);
		begin = new java.sql.Date(calendar.getTime().getTime()).toString();
		System.out.println(" begin: " + begin);
		System.out.println(" end: " + end);
		System.out.println(" ---------------------- ");
	}

	public static void calcThisMonth(String begin, String end, String now,
			GregorianCalendar calendar) {
		end = now;
		int dayOfMonth = calendar.get(GregorianCalendar.DATE);
		calendar.add(GregorianCalendar.DATE, -dayOfMonth + 1);
		begin = new java.util.Date(calendar.getTime().getTime()).toString();
		System.out.println(" begin: " + begin);
		System.out.println(" end: " + end);
		System.out.println(" ---------------------- ");
	}

	public static void calcLastMonth(String begin, String end, String now,
			GregorianCalendar calendar) {

		calendar.set(calendar.get(GregorianCalendar.YEAR),
				calendar.get(GregorianCalendar.MONTH), 1);
		calendar.add(GregorianCalendar.DATE, -1);
		end = new java.util.Date(calendar.getTime().getTime()).toString();

		int month = calendar.get(GregorianCalendar.MONTH) + 1;
		begin = calendar.get(GregorianCalendar.YEAR) + " - " + month + " -01 ";

		System.out.println(" begin: " + begin);
		System.out.println(" end: " + end);
		System.out.println(" ---------------------- ");
	}
	
	
	 public   static  String begin = "" ;
     public   static  String end = "" ;
     public   static  String now = new  java.util.Date( new  Date().getTime()).toString();
     
     public   static   void  main(String[] args)  {
    
        /*
    	 // 今天 
        calcToday(begin,end,now, new  GregorianCalendar());
         // 昨天 
        calcYesterday(begin,end,now, new  GregorianCalendar());
         // 本周 
        calcThisWeek(begin,end,now, new  GregorianCalendar());
         // 上周 
        calcLastWeek(begin,end,now, new  GregorianCalendar());
         // 本月 
        calcThisMonth(begin,end,now, new  GregorianCalendar());
         // 上月 
        calcLastMonth(begin,end,now, new  GregorianCalendar());
        */
    	 
    	 Date date = new Date();
    	 
    	 int lastDay = getLastDayOfMonth(date);
    	 
    	 int firstDay = getFirstDayOfMonth(date);
    	 
    	 System.out.println(lastDay);
    	 
    	 System.out.println(firstDay);
    	 System.out.println(getLastDayOfMonth("20140203",null));
    	 System.out.println(getFirstDayOfMonth("20140203",null));
        
     }
     
     
     //获取天数
     public static long getDays(long time){
    	 if(time >= ONE_DAY){
    		 return time/ONE_DAY;
    	 }
    	 return -1;
     }
     
     //获取小时数
     public static long getHours(long time){
    	 if(time >= ONE_HOUR){
    		 return time/ONE_HOUR;
    	 }
    	 return -1;
     }
     
     //获取分钟数
     public static long getMinutes(long time){
    	 if(time >= ONE_MINUTE){
    		 return time/ONE_MINUTE;
    	 }
    	 return -1;
     }
     
     //获取秒数
     public static long getSeconds(long time){
    	 if(time >= ONE_SECOND){
    		 long temp = time/ONE_SECOND;
    		 return temp;
    	 }
    	 return 0;
     }
     
     
     //得到系统当前毫秒数
     public static long getMillis(){
    	 System.nanoTime();
    	return System.currentTimeMillis();
     }

     //得到系统当前纳秒数
     public static long getNanoTime(){
    	return System.nanoTime();
     }
     
     //得到系统当前纳秒数 除以 1000
     public static long getNano1000Time(){
     	return System.nanoTime()/1000;
     }
     
     
     public static Date getSpecificDate(int year, int month, int day){
    	 Calendar rightNow = Calendar.getInstance();
    	 rightNow.set(year, month, day);
    	 Date date = rightNow.getTime();
    	 return date;
     }
     
     public static String getSpecificYearMonthDayWithFormat(int year, int month, int day,String format){
    	 Date specificDate = getSpecificDate(year,  month,  day);
    	 String date = getSpecificYearMonthDayWithFormat(specificDate,format);
    	 return date;
     }
     
     public static String getSpecificYearMonthDayWithFormat(Date specificDate,String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMMdd";
    	 }
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 String date = sf.format(specificDate);
    	 return date;
     }
     
     public static String getSpecificYearMonthWithFormat(Date specificDate,String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMM";
    	 }
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 String date = sf.format(specificDate);
    	 return date;
     }
     
     public static String getSpecificDayWithFormat(Date specificDate,String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="dd";
    	 }
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 String date = sf.format(specificDate);
    	 return date;
     }
     
     public static Date getCurrentDate(){
    	 return new Date();
     }
     
     public static String getCurrentYearMonthDayWithFormat(String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMMdd";
    	 }
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 String date = sf.format(getCurrentDate());
    	 return date;
     }
     
     /**
      * 
      * @return yyyyMMdd
      */
     public static String getCurrentYearMonthDay(){
    	 return getCurrentYearMonthDayWithFormat(null);
     }
     
     /**
      * 
      * @return yyyyMM
      */
     public static String getCurrentYearMonth(){
    	 return getCurrentYearMonthWithFormat(null);
     }
     
     public static String getCurrentYearMonthWithFormat(String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMM";
    	 }
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 String date = sf.format(getCurrentDate());
    	 return date;
     }
     
     /**
      * 
      * @param day 日期如 20140701
      * @param format default yyyyMMdd
      * @return
      */
     public static int getLastDayOfMonth(String day, String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMMdd";
    	 }
    	 int last = -1;
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 Date date;
		try {
			date = sf.parse(day);
			last = getLastDayOfMonth(date);
		} catch (ParseException e) {
			last = -1;
			return last;
		}
    	 return last;
     }
     
     public static int getFirstDayOfMonth(String day, String format){
    	 if(StringUtils.isEmpty(format)){
    		 format="yyyyMMdd";
    	 }
    	 int first = -1;
    	 SimpleDateFormat sf = new SimpleDateFormat(format);
    	 Date date;
		try {
			date = sf.parse(day);
			first = getFirstDayOfMonth(date);
		} catch (ParseException e) {
			first = -1;
			return first;
		}
    	 return first;
     }
     
     public static int getLastDayOfMonth(Date date){
    	 Calendar rightNow = Calendar.getInstance();
    	 rightNow.setTime(date);
    	 int last = rightNow.getActualMaximum(Calendar.DAY_OF_MONTH);
    	 return last;
     }
     
     public static int getFirstDayOfMonth(Date date){
    	 Calendar rightNow = Calendar.getInstance();
    	 rightNow.setTime(date);
    	 int first = rightNow.getActualMinimum(Calendar.DAY_OF_MONTH);
    	 return first;
     }
     
     
     public static String fillZeroForDay(int day){
    	 String dayStr = String.valueOf(day);
    	 if(day>0 && day<10){
    		 dayStr = "0"+dayStr;
    	 }
    	 return dayStr;
     }
     
}
