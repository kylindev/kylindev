package org.jit8.framework.jit84j.core.mail.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;

public interface MailConfigDao extends GenericDao<MailConfig, Long>{

	List<MailConfig> getMailConfigList(boolean disable,boolean deleted);
	
	public MailConfig findByType(String type);

}
