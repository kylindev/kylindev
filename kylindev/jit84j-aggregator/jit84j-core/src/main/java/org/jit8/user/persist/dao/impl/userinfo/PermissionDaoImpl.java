package org.jit8.user.persist.dao.impl.userinfo;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.PermissionDao;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.Permission_;
import org.jit8.user.persist.domain.userinfo.spec.PermissionSpecifications;
import org.jit8.user.persist.repository.userinfo.PermissionRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Repository;

@Repository
public class PermissionDaoImpl extends GenericDaoImpl<Permission, Long> implements PermissionDao {

	@Resource
	private PermissionRepository permissionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = permissionRepository;
	}

	@Override
	public Permission findPermissionByResourceUrl(String resourceUrl) {
		return permissionRepository.findPermissionByResourceUrl(resourceUrl);
	}

	@Override
	public Permission findPermissionByCode(String code) {
		return permissionRepository.findPermissionByCode(code);
	}

	@Override
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls) {
		return permissionRepository.findPermissionByResourceUrls(Arrays.asList(resourceUrls));
	}

	@Override
	public Page<Permission> getPermissionList(Pageable pageable) {
		return permissionRepository.getPermissionList(pageable);
	}

	@Override
	public Permission fineById(Long id) {
		return null;
	}

	@Override
	public Page<Permission> getPermissionList(Permission permission,
			Pageable pageable) {
		Specification<Permission> spec = PermissionSpecifications.queryByCondition(permission);
		Page<Permission> page = permissionRepository.findAll(spec, pageable);
		return page;
	}

	@Override
	public Permission findByFullUrl(String fullUrl) {
		return permissionRepository.findByFullUrl(fullUrl);
	}

	@Override
	public List<Permission> findPermissionListByCategoryId(Long categoryId) {
		return null;
	}

	@Override
	public List<Permission> findByIdList(List<Long> idList) {
		
		return permissionRepository.findByIdList(idList);
	}
}
