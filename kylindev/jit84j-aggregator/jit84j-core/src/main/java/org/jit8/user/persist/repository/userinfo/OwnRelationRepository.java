package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface OwnRelationRepository extends GenericJpaRepository<OwnRelation, Long>{

	@Query("from OwnRelation o where o.disable=:disable and o.deleted=:deleted")
	List<OwnRelation> getOwnRelationList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);
}
