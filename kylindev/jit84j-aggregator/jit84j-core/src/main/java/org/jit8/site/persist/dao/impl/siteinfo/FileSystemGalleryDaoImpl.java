package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileSystemGalleryDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.persist.repository.siteinfo.FileSystemGalleryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FileSystemGalleryDaoImpl extends GenericDaoImpl<FileSystemGallery, Long> implements FileSystemGalleryDao {

	@Resource
	private FileSystemGalleryRepository fileSystemGalleryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileSystemGalleryRepository;
	}

	@Override
	public List<FileSystemGallery> getFileSystemGalleryList(boolean disable, boolean deleted) {
		return fileSystemGalleryRepository.getFileSystemGalleryList(disable, deleted);
	}

}
