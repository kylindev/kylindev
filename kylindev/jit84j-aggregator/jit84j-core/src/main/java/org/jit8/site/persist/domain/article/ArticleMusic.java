package org.jit8.site.persist.domain.article;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.jit8.framework.jit84j.core.persistent.model.AbstractBasicPersistentEntityWithAutoGeneratedId;

@Entity
public class ArticleMusic extends AbstractBasicPersistentEntityWithAutoGeneratedId {

	
	private static final long serialVersionUID = 4657361108289971046L;
	@ManyToOne
	private Article article;
	@ManyToOne
	private Music music;
	private String mark;
	
	@Transient
	private Long id;
	
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}
	
	public Article getArticle() {
		return article;
	}
	public void setArticle(Article article) {
		this.article = article;
	}
	public Music getMusic() {
		return music;
	}
	public void setMusic(Music music) {
		this.music = music;
	}
	public String getMark() {
		return mark;
	}
	public void setMark(String mark) {
		this.mark = mark;
	}
}
