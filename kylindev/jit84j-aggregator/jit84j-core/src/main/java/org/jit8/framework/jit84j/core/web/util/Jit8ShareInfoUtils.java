package org.jit8.framework.jit84j.core.web.util;

import java.io.Serializable;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserId;

public class Jit8ShareInfoUtils implements Serializable {

	private static final long serialVersionUID = 3634888632120485473L;
	
	public static final String CURRENT_USER = "currentUser";

	protected static Object getSession(Object key){  
        Subject currentUser = SecurityUtils.getSubject();  
        if(null != currentUser){  
            Session session = currentUser.getSession();  
            if(null != session){  
            	Object obj = session.getAttribute(key); 
                return  obj;
            }  
        }  
        return null;
    } 
	
	public static User getCurrentUser(){  
        return (User)getSession(CURRENT_USER);
    }
	
	public static User getUser() {
		return getCurrentUser();
	}
	
	public static Long getUserId() {
		User user = getCurrentUser();
		Long userIdValue = null;
		if(null != user){
			UserId userId = user.getUserId();
			if(null!= userId){
				userIdValue = userId.getUserId();
			}
		}
		return userIdValue;
	}
	
	public static ThreadLocalShareInfo getShareInfo(){
		
		return (ThreadLocalShareInfo)getSession("shareInfo");
	}
	

}
