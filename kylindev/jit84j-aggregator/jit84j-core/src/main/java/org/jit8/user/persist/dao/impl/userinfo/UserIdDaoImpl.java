package org.jit8.user.persist.dao.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.UserIdDao;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.persist.repository.userinfo.UserIdRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserIdDaoImpl extends GenericDaoImpl<UserId, Long> implements UserIdDao {

	@Resource
	private UserIdRepository userIdRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = userIdRepository;
	}
}
