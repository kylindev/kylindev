package org.jit8.user.persist.repository.userinfo;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UrlMappingRepository extends GenericJpaRepository<UrlMapping, Long>{

	@Query("from UrlMapping u where u.urlStaticMapping.idKey=:idKey")
	public UrlMapping findByUrlStaticMappingIdKey(@Param("idKey")String idKey);
	
	@Query("from UrlMapping u where u.urlStaticMapping.urlFullPath=:urlFullPath")
	public UrlMapping findByUrlStaticMappingUrlFullPath(@Param("urlFullPath")String urlFullPath);
	
	
}
