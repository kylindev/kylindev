package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.Photograph;

public interface PhotographDao extends GenericDao<Photograph, Long> {

	public List<Photograph> getPhotographList(boolean disable, boolean deleted) ;
}
