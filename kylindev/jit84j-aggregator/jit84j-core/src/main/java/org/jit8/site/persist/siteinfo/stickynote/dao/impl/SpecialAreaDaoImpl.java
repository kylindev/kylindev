package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.SpecialAreaDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.repository.SpecialAreaRepository;
import org.springframework.stereotype.Repository;

@Repository
public class SpecialAreaDaoImpl extends GenericDaoImpl<SpecialArea, Long> implements SpecialAreaDao {

	@Resource
	private SpecialAreaRepository specialAreaRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = specialAreaRepository;
	}

	@Override
	public List<SpecialArea> getSpecialAreaList(boolean disable, boolean deleted) {
		return specialAreaRepository.getSpecialAreaList(disable, deleted);
	}
	
	@Override
	public SpecialArea findByCode(String code) {
		return specialAreaRepository.findByCode( code);
	}

	@Override
	public List<SpecialArea> findByIds(List<Long> idList) {
		return specialAreaRepository.findByIds(idList);
	}
}
