package org.jit8.site.persist.dao.impl.menu;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.menu.NavigationDao;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.repository.menu.NavigationRepository;
import org.jit8.user.persist.domain.userinfo.spec.NavigationSpecifications;
import org.springframework.stereotype.Repository;

@Repository
public class NavigationDaoImpl extends GenericDaoImpl<Navigation, Long> implements NavigationDao {

	@Resource
	private NavigationRepository navigationRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = navigationRepository;
	}

	@Override
	public List<Navigation> getNavigationList(boolean disable, boolean deleted) {
		return navigationRepository.getNavigationList(disable, deleted);
	}
	
	@Override
	public List<Navigation> getNavigationListByIds(boolean disable,
			boolean deleted, List<Long> navigationIdList) {
		return navigationRepository.getNavigationListByIds(disable, deleted, navigationIdList);
	}

	@Override
	public List<Navigation> getNavigationListByParentNull() {
		return navigationRepository.getNavigationListByParentNull();
	}

	@Override
	public List<Navigation> getNavigationByCode(boolean disable, boolean deleted,
			String code) {
		
		return navigationRepository.getNavigationByCode(disable, deleted, code);
	}

	@Override
	public List<Navigation> getNavigationByTopCode(boolean disable,
			boolean hideable, String code) {
		List<Navigation> navigationList = navigationRepository.getNavigationByTopCode(disable,  code);
		return navigationList;//getDisplayNavigation( navigationList);
	}
	
	public List<Navigation> getNavigationByTopCode(boolean disable,
			 String code) {
		List<Navigation> navigationList = navigationRepository.getNavigationByTopCode(disable,  code);
		return navigationList;
	}
	
	private static List<Navigation> getDisplayNavigation(List<Navigation> navigationList){
		List<Navigation> newList = new ArrayList<Navigation>();
		if(null != navigationList && navigationList.size()>0){
			for(Navigation navigation : navigationList){
				if(!navigation.getHideable()){
					newList.add(navigation);
					List<Navigation> children = navigation.getChildrenNavigation();
					navigation.setChildrenNavigation(getDisplayNavigation(children));
				}
			}
		}
		return newList;
	}

	@Override
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId) {
		return navigationRepository.getNavigationByCodeAndDeveloper( code,  userId);
	}

	@Override
	public List<Navigation> getNavigationByParentCode(
			boolean deleted, String code) {
		List<Navigation> list = navigationRepository.getNavigationByTopCode(deleted,code);//navigationRepository.findAll(NavigationSpecifications.eqParentCode(code));
		return list;
		
		//return navigationRepository.getNavigationByParentCode( deleted,  code);
	}
	
	
	public static void main(String[] args) {
		List<Navigation> newList1 = new ArrayList<Navigation>();
		Navigation n1 = new Navigation();
		n1.setHideable(false);
		n1.setCode("n1");
		Navigation n2 = new Navigation();
		n2.setHideable(false);
		n2.setCode("n2");
		newList1.add(n1);
		newList1.add(n2);
		
		List<Navigation> childList1 = new ArrayList<Navigation>();
		Navigation c1 = new Navigation();
		c1.setHideable(true);
		c1.setCode("c1");
		Navigation c2 = new Navigation();
		c2.setHideable(false);
		c2.setCode("c2");
		childList1.add(c1);
		childList1.add(c2);
		
		n1.setChildrenNavigation(childList1);
		
		
		List<Navigation> childList2 = new ArrayList<Navigation>();
		Navigation c21 = new Navigation();
		c21.setHideable(false);
		c21.setCode("c21");
		Navigation c22 = new Navigation();
		c22.setHideable(false);
		c22.setCode("c22");
		childList2.add(c21);
		childList2.add(c22);
		n2.setChildrenNavigation(childList2);
		
		
		List<Navigation> newList3 = getDisplayNavigation(newList1);
		
		System.out.println(newList3.get(0).getChildrenNavigation().size());
	}

	@Override
	public Navigation findNavigationByIdKey(String idKey) {
		return navigationRepository.findNavigationByIdKey(idKey);
	}
}
