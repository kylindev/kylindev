package org.jit8.site.persist.repository.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.menu.Navigation;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NavigationRepository extends GenericJpaRepository<Navigation, Long>{
	
	@Query("from Navigation o where o.disable=:disable and o.deleted=:deleted")
	List<Navigation> getNavigationList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from Navigation o where o.disable=:disable and o.deleted=:deleted and o.uniqueIdentifier in :navigationIdList")
	List<Navigation> getNavigationListByIds(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("navigationIdList")List<Long> navigationIdList);
	
	@Query("from Navigation o where o.disable=false and o.deleted=false and o.parent is null")
	public List<Navigation> getNavigationListByParentNull();
	
	@Query("from Navigation o where o.disable=:disable and o.deleted=:deleted and o.code=:code")
	public List<Navigation> getNavigationByCode(@Param("disable")boolean disable,@Param("deleted")boolean deleted,@Param("code")String code);
	
	@Query("from Navigation o where o.deleted=:deleted and o.parent.code=:code order by o.sequence")
	public List<Navigation> getNavigationByParentCode(@Param("deleted")boolean deleted,@Param("code")String code);
	
	
	@Query("from Navigation o where o.disable=:disable and o.deleted=false and o.parent.code=:code order by o.sequence")
	public List<Navigation> getNavigationByTopCode(@Param("disable")boolean disable,@Param("code")String code);

	@Query("from Navigation o where o.disable=false and o.deleted=false and o.code=:code and o.developer.userId.userId=:userId")
	public Navigation getNavigationByCodeAndDeveloper(@Param("code")String code, @Param("userId")Long userId) ;
	
	
	@Query("from Navigation o where o.idKey=:idKey")
	public Navigation findNavigationByIdKey(@Param("idKey")String idKey) ;
	
}
