package org.jit8.framework.jit84j.core.mail.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.dao.MailTemplateDao;
import org.jit8.framework.jit84j.core.mail.domain.MailTemplate;
import org.jit8.framework.jit84j.core.mail.service.MailTemplateService;
import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.springframework.stereotype.Service;

@Service
public class MailTemplateServiceImpl extends GenericServiceImpl<MailTemplate, Long> implements MailTemplateService {

	@Resource
	private MailTemplateDao mailTemplateDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = mailTemplateDao;
	}


	@Override
	public List<MailTemplate> getMailTemplateList(boolean disable, boolean deleted) {
		return mailTemplateDao.getMailTemplateList(disable, deleted);
	}
	
}
