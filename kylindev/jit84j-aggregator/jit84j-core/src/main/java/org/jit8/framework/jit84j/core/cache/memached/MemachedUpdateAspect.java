package org.jit8.framework.jit84j.core.cache.memached;

import java.lang.reflect.Method;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.cache.ehcache.management.impl.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class MemachedUpdateAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(MemachedUpdateAspect.class); 
	
	public MemachedUpdateAspect() {
	}
	
	 @Pointcut("(execution(* org.jit8.**.business.impl.**.*BusinessImpl.merge*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.persist*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.update*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.add*(..)) "
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.aisgn*(..))"
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.remove*(..))"
	 		+ " || execution(* org.jit8.**.business.impl.**.*BusinessImpl.delete*(..))"
	 		+ ") && @annotation(org.jit8.framework.jit84j.core.cache.memached.MemachedUpdate) ")  
    public void pointcut() {  
        // 定义一个pointcut，下面用Annotation标注的通知方法可以公用这个pointcut  
    }  
	 
	
	 
	// 环绕通知  
    @Around("pointcut()")  
    public Object memachedUpdateAroundAdvice(ProceedingJoinPoint jionpoint)  
            throws Throwable {  
        // 获取被调用的方法名  
        String targetMethodName = jionpoint.getSignature().getName();  
        
        Object o = jionpoint.proceed();  
        LOGGER.info("finished invok : " + targetMethodName);  
        //Object o = jionpoint.proceed();//注意写到这儿的话，环绕通知和其它通知的顺序  
        
        MethodSignature methodSignature = (MethodSignature)jionpoint.getSignature();  
        Method targetMethod = methodSignature.getMethod();
        MemachedUpdate memachedUpdate = targetMethod.getAnnotation(MemachedUpdate.class);
        
        //只支持第一个参数为domain模型,必须有属性为id
        Object[] args = jionpoint.getArgs();
        Object[] values = null;
        if(null != memachedUpdate.targetMethodParams()){
        	 values = new Object[memachedUpdate.targetMethodParams().length];
        	 String[] targetMethodParamValueProperty = memachedUpdate.targetMethodParamValueProperty();
             if(null != args && args.length>0){
             	values[0] = BeanUtils.getBeanProperty(args[0], targetMethodParamValueProperty[0]);
             }
        }
        
        if(null != memachedUpdate){
        	MemachedDefiner.set(memachedUpdate,values);
        }
        LOGGER.info("check memcached end for " + targetMethodName); 
        
        return o;  
    } 

}
