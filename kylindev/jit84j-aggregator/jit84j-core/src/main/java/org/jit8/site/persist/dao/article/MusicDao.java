package org.jit8.site.persist.dao.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.article.Music;

public interface MusicDao extends GenericDao<Music, Long> {

	public List<Music> getMusicList(boolean disable, boolean deleted) ;
}
