package org.jit8.site.persist.dao.article;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.article.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ArticleDao extends GenericDao<Article, Long> {

	public List<Article> getArticleList(boolean disable, boolean deleted) ;
	public Page<Article> getArticleList(boolean disable, boolean deleted,Pageable pageable);
}
