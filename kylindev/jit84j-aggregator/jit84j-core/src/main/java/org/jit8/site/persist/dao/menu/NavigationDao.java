package org.jit8.site.persist.dao.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.menu.Navigation;
import org.springframework.data.repository.query.Param;

public interface NavigationDao extends GenericDao<Navigation, Long> {

	public List<Navigation> getNavigationList(boolean disable, boolean deleted) ;
	
	public List<Navigation> getNavigationListByIds(boolean disable,boolean deleted,List<Long> navigationIdList);
	
	public List<Navigation> getNavigationListByParentNull();
	
	public List<Navigation> getNavigationByCode(boolean disable,boolean deleted,String code);
	
	public List<Navigation> getNavigationByTopCode(boolean disable,boolean deleted,String code);
	
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId);
	
	public List<Navigation> getNavigationByParentCode(boolean deleted,String code);
	
	public List<Navigation> getNavigationByTopCode(boolean disable,
			 String code);
	
	public Navigation findNavigationByIdKey(String idKey);
	
}
