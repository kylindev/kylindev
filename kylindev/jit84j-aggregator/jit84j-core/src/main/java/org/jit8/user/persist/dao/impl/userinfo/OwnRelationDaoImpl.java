package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.OwnRelationDao;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.jit8.user.persist.repository.userinfo.OwnRelationRepository;
import org.springframework.stereotype.Repository;

@Repository
public class OwnRelationDaoImpl extends GenericDaoImpl<OwnRelation, Long> implements OwnRelationDao {

	@Resource
	private OwnRelationRepository ownRelationRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = ownRelationRepository;
	}

	@Override
	public List<OwnRelation> getOwnRelationList(boolean disable, boolean deleted) {
		return ownRelationRepository.getOwnRelationList(disable, deleted);
	}
}
