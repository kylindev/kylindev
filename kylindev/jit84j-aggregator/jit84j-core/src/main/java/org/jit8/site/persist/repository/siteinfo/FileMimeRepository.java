package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface FileMimeRepository extends GenericJpaRepository<FileMime, Long>{

	@Query("from FileMime o where o.disable=:disable and o.deleted=:deleted")
	List<FileMime> getFileMimeList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	@Query("from FileMime o where o.mime=:mime")
	public FileMime findByMime(@Param("mime")String mime);
	
	@Query("from FileMime o where o.uniqueIdentifier in :idList")
	public List<FileMime> findByIdList(@Param("idList")List<Long> idList);
	
	
	@Query("from FileMime o where o.systemGallery.uniqueIdentifier=:galleryId and o.deleted=false")
	Page<FileMime> findBySystemGalleryId(@Param("galleryId") long galleryId,Pageable pageable);
	
	
	@Query("from FileMime o where o.allowable=:allowable and o.deleted=false")
	public List<FileMime> getFileMimeListByAllowable(@Param("allowable")boolean allowable);
}
