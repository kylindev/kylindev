package org.jit8.framework.jit84j.core.cache.memached;

public interface MemachedDefinition {

	int TIMEOUT_DEFAULT = -1;
	
	/**
	 * 一般过期时间 秒 （7天）
	 */
	int DEFAULT_EXPIRES_TIME = 60*24*7;
	
	/**
	 * 最长过期时间 秒 （30天）
	 */
	int REALTIME_MAXDELTA = 60*60*24*30 ;
	
	/**
	 * 延迟刷新时间 毫秒 默认5秒
	 */
	long DELAY_REFRESH_TIME = 1000*5;
	
	/**延迟执行线程连接池数目*/
	int SCHEDULED_THREAD_POOL = 30;
	
	/**刷新循环周期 毫秒 默认1天*/
	long REFRESH_PERIOD = 1000*60*24;
	
	/**过期时间 毫秒 1天*/
	int EXPIRES_TIME_24H = 1000*60*24;
}
 