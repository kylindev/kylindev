package org.jit8.user.persist.dao.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.user.persist.dao.userinfo.UrlMappingDao;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.jit8.user.persist.repository.userinfo.UrlMappingRepository;
import org.jit8.user.persist.repository.userinfo.UrlStaticMappingRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class UrlMappingDaoImpl extends GenericDaoImpl<UrlMapping, Long> implements UrlMappingDao {

	@Resource
	private UrlMappingRepository urlMappingRepository;
	
	@Resource
	private UrlStaticMappingRepository urlStaticMappingRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = urlMappingRepository;
	}

	@Override
	public UrlMapping findByUrlStaticMappingIdKey(String idKey) {
		UrlMapping urlMapping = urlMappingRepository.findByUrlStaticMappingIdKey(idKey);
		return urlMapping;
	}

	@Override
	public UrlStaticMapping findModuleByIdKey(String idKey) {
		
		return urlStaticMappingRepository.findByUrlStaticMappingIdKey(idKey);
	}

	@Override
	public UrlMapping findByUrlStaticMappingUrlFullPath(String urlFullPath) {
		return urlMappingRepository.findByUrlStaticMappingUrlFullPath( urlFullPath);
	}

	/**
	 * 查找预定义URL分类列表
	 */
	@Override
	public Page<UrlStaticMapping> findModule(Pageable pageable) {
		
		return urlStaticMappingRepository.findModule(pageable);
	}

	@Override
	public Page<UrlStaticMapping> findUrlStaticMapping(String moduleIdKey,
			Pageable pageable) {
		return urlStaticMappingRepository.findUrlStaticMapping(moduleIdKey, pageable);
	}

	@Override
	public List<UrlStaticMapping> findByUrlStaticMappingIdKeyList(
			List<String> idKeyList) {
		return urlStaticMappingRepository.findByUrlStaticMappingIdKeyList(idKeyList);
	}

	@Override
	public Page<UrlStaticMapping> findUrlStaticMappingWithoutModule(
			Pageable pageable) {
		return urlStaticMappingRepository.findUrlStaticMappingWithoutModule(pageable);
	}

}
