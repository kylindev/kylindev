package org.jit8.user.persist.dao.userinfo;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DeveloperDao extends GenericDao<Developer, Long> {

	public Page<Developer> getDeveloperList(Pageable pageable);
	
	public Page<Developer> getDeveloperList(Developer developer,Pageable pageable);
	
	public Developer findByUserId(Long userId);
	
}
