package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteCategoryDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.site.persist.siteinfo.stickynote.repository.StickyNoteCategoryRepository;
import org.springframework.stereotype.Repository;

@Repository
public class StickyNoteCategoryDaoImpl extends GenericDaoImpl<StickyNoteCategory, Long> implements StickyNoteCategoryDao {

	@Resource
	private StickyNoteCategoryRepository stickyNoteCategoryRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = stickyNoteCategoryRepository;
	}

	@Override
	public List<StickyNoteCategory> getStickyNoteCategoryList(boolean disable, boolean deleted) {
		return stickyNoteCategoryRepository.getStickyNoteCategoryList(disable, deleted);
	}
	
	@Override
	public StickyNoteCategory findByCode(String code) {
		return stickyNoteCategoryRepository.findByCode( code);
	}

	@Override
	public List<StickyNoteCategory> findByIds(List<Long> idList) {
		return stickyNoteCategoryRepository.findByIds(idList);
	}
}
