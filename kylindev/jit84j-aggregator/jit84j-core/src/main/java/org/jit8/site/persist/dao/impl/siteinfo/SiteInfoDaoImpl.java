package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.SiteInfoDao;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.persist.repository.siteinfo.SiteInfoRepository;
import org.springframework.stereotype.Repository;

@Repository
public class SiteInfoDaoImpl extends GenericDaoImpl<SiteInfo, Long> implements SiteInfoDao {

	@Resource
	private SiteInfoRepository siteInfoRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = siteInfoRepository;
	}

	@Override
	public List<SiteInfo> getSiteInfoList(boolean disable, boolean deleted) {
		return siteInfoRepository.getSiteInfoList(disable, deleted);
	}

	@Override
	public SiteInfo getSiteInfoByType(String type) {
		return siteInfoRepository.getSiteInfoByType(type) ;
	}
}
