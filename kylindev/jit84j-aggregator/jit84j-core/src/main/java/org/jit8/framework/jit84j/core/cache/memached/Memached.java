package org.jit8.framework.jit84j.core.cache.memached;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Memached {


	String value() default "";//缓存的key存储在这里，如果为空则取类的全名_方法名
	
	String name() default "";//缓存的显示名称存储在这里
	
	int timeout() default MemachedDefinition.TIMEOUT_DEFAULT;
	
	boolean autoRefresh() default false;//是否自动刷新，若为true，可在页面上点击 刷新 按钮进行刷新
	
	boolean repeated() default false; //是否重复
	
	int expiresTime() default MemachedDefinition.DEFAULT_EXPIRES_TIME;//过期时间
	
	long delayRefreshTime() default MemachedDefinition.DELAY_REFRESH_TIME;//延迟刷新时间
	
	long period() default MemachedDefinition.REFRESH_PERIOD;//刷新周期 
	
}
