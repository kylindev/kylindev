package org.jit8.framework.jit84j.core.mail.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;

public interface MailMessageDao extends GenericDao<MailMessage, Long>{

	List<MailMessage> getMailMessageList(boolean disable,boolean deleted);

}
