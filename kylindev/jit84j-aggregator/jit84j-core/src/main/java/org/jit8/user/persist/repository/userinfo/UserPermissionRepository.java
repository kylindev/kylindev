package org.jit8.user.persist.repository.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface UserPermissionRepository extends GenericJpaRepository<UserPermission, Long>{
	
	@Query("from UserPermission u where u.permission.uniqueIdentifier = :permissionId")
	public List<UserPermission> findByPermissionId(@Param("permissionId")Long permissionId);

}
