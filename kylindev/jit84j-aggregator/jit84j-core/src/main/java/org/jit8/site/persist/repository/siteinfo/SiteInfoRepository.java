package org.jit8.site.persist.repository.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SiteInfoRepository extends GenericJpaRepository<SiteInfo, Long>{

	@Query("from SiteInfo o where o.disable=:disable and o.deleted=:deleted")
	List<SiteInfo> getSiteInfoList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);
	
	@Query("from SiteInfo o where o.type=:type")
	public SiteInfo getSiteInfoByType(@Param("type")String type);

}
