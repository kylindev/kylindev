package org.jit8.site.persist.dao.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;

public interface FileCustomGalleryDao extends GenericDao<FileCustomGallery, Long> {

	public List<FileCustomGallery> getFileCustomGalleryList(boolean disable, boolean deleted) ;
	
	public FileCustomGallery findByCodeAndType(String code, int type);
}
