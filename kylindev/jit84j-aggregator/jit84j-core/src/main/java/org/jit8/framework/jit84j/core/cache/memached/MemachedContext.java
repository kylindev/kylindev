package org.jit8.framework.jit84j.core.cache.memached;

import java.io.Serializable;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;

import net.rubyeye.xmemcached.MemcachedClient;

@Component
public class MemachedContext implements Serializable{

	private static final long serialVersionUID = 8391979103463633445L;
	
	private boolean memcachedEnable;
	
	@Resource
	private MemcachedClient memcachedClient;
	

	/**
	 * @return the memcachedClient
	 */
	public MemcachedClient getMemcachedClient() {
		return memcachedClient;
	}

	/**
	 * @param memcachedClient the memcachedClient to set
	 */
	public void setMemcachedClient(MemcachedClient memcachedClient) {
		this.memcachedClient = memcachedClient;
	}

	/**
	 * @return the memcachedEnable
	 */
	public boolean isMemcachedEnable() {
		return memcachedEnable;
	}

	/**
	 * @param memcachedEnable the memcachedEnable to set
	 */
	public void setMemcachedEnable(boolean memcachedEnable) {
		this.memcachedEnable = memcachedEnable;
	}
	
	public MemachedContext(){
		
	}
	
	
}
