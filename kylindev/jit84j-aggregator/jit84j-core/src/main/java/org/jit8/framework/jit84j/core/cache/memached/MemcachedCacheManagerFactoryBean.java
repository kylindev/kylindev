package org.jit8.framework.jit84j.core.cache.memached;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.CacheManager;

import com.googlecode.hibernate.memcached.MemcachedCache;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

public class MemcachedCacheManagerFactoryBean implements FactoryBean, InitializingBean, DisposableBean {

	protected final Logger logger = LogFactory.getLogger(getClass());

	private CacheManager cacheManager;

	public Object getObject() throws Exception {
		return cacheManager;
	}

	public Class getObjectType() {
		return this.cacheManager.getClass();
	}

	public boolean isSingleton() {
		return true;
	}

	public void afterPropertiesSet() throws Exception {
		logger.info("Initializing Memcached CacheManager");
		//cacheManager = CacheUtil.getCacheManager(IMemcachedCache.class, MemcachedCacheManager.class.getName());
		// cacheManager.start();
	}

	public void destroy() throws Exception {
		logger.info("Shutting down Memcached CacheManager");
		// cacheManager.stop();
	}
}
