package org.jit8.user.persist.domain.userinfo;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.jit8.framework.jit84j.core.persistent.model.BasicPersistentEntity;

/**直接权限，是指用户可以直接访问，不用通过permissionCheckInterceptor的校验
 * 按类型分为两种：system是指在系统中有定义的，这部分权限均对应一个或多个Permission域模型
 * custom是指 在系统中没有定义过的，由使用者自己定义
 * */
@Entity
public class DirectPermission extends BasicPersistentEntity {

	private static final long serialVersionUID = -2320511977725503495L;
	
	public static final String TYPE_DEFAULT = "system";
	public static final String TYPE_CUSTOM = "custom";
	
	@OneToMany(mappedBy="directPermission")
	private List<Permission> permissionList;
	
	private String directUrl;
	
	private String directUrlName;
	
	
	private String type;//system,custom
	
	
	@Transient
	private Long permissionId;
	
	@Transient
	private Long id;
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}

	public List<Permission> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<Permission> permissionList) {
		this.permissionList = permissionList;
	}

	public String getDirectUrl() {
		return directUrl;
	}

	public void setDirectUrl(String directUrl) {
		this.directUrl = directUrl;
	}

	public String getDirectUrlName() {
		return directUrlName;
	}

	public void setDirectUrlName(String directUrlName) {
		this.directUrlName = directUrlName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	public Long getPermissionId() {
		return permissionId;
	}
	public void setPermissionId(Long permissionId) {
		this.permissionId = permissionId;
	}

}
