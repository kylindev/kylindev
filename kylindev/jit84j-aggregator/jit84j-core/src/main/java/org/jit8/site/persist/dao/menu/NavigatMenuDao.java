package org.jit8.site.persist.dao.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.persist.domain.menu.NavigatMenu;

public interface NavigatMenuDao extends GenericDao<NavigatMenu, Long> {

	public List<NavigatMenu> getNavigatMenuList(boolean disable, boolean deleted) ;
	
	public List<NavigatMenu> getNavigatMenuListByIds(boolean disable,boolean deleted,List<Long> navigatMenuIdList);
	
	public List<NavigatMenu> getNavigatMenuListByParentNull();
	
	public List<NavigatMenu> getNavigatMenuByCode(boolean disable,boolean deleted,String code);
	
	public List<NavigatMenu> getNavigatMenuByTopCode(boolean disable,boolean deleted,String code);
	
	public NavigatMenu findByCode(String code);
	
}
