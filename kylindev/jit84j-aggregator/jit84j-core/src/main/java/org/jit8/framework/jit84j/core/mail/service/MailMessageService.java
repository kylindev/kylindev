package org.jit8.framework.jit84j.core.mail.service;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.service.GenericService;

public interface MailMessageService extends GenericService<MailMessage, Long>{

	List<MailMessage> getMailMessageList(boolean disable,boolean deleted);

}
