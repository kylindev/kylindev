package org.jit8.user.persist.domain.userinfo;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.jit8.framework.jit84j.core.persistent.model.BasicPersistentEntity;

@Entity
public class PmnCategory extends BasicPersistentEntity {

	private static final long serialVersionUID = 903373209770719560L;
	private String name;
	private String code;
	private String description;
	
	@ManyToOne(optional=true)
	private PmnCategory parent;
	
	@OneToMany(fetch=FetchType.LAZY,mappedBy="parent",cascade={CascadeType.ALL})
	private List<PmnCategory> childrenCategory;
	
	
	@Fetch(FetchMode.SUBSELECT)
	@OneToMany(mappedBy="category")
	private List<Permission> permissionList;
	

	@Transient
	private Long id;
	public Long getId() {
		this.id = this.getUniqueIdentifier();
		return id;
	}
	public void setId(Long id) {
		this.setUniqueIdentifier(id);
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Permission> getPermissionList() {
		return permissionList;
	}

	public void setPermissionList(List<Permission> permissionList) {
		this.permissionList = permissionList;
	}

	public PmnCategory getParent() {
		return parent;
	}

	public void setParent(PmnCategory parent) {
		this.parent = parent;
	}

	public List<PmnCategory> getChildrenCategory() {
		return childrenCategory;
	}

	public void setChildrenCategory(List<PmnCategory> childrenCategory) {
		this.childrenCategory = childrenCategory;
	}
}
