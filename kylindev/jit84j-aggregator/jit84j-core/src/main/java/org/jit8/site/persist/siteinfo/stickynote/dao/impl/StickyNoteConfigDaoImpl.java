package org.jit8.site.persist.siteinfo.stickynote.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteConfigDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.jit8.site.persist.siteinfo.stickynote.repository.StickyNoteConfigRepository;
import org.springframework.stereotype.Repository;

@Repository
public class StickyNoteConfigDaoImpl extends GenericDaoImpl<StickyNoteConfig, Long> implements StickyNoteConfigDao {

	@Resource
	private StickyNoteConfigRepository stickyNoteConfigRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = stickyNoteConfigRepository;
	}

	@Override
	public List<StickyNoteConfig> getStickyNoteConfigList(boolean disable, boolean deleted) {
		return stickyNoteConfigRepository.getStickyNoteConfigList(disable, deleted);
	}
}
