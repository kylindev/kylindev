package org.jit8.site.persist.dao.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.persist.dao.siteinfo.FileSystemGalleryTypeDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.site.persist.repository.siteinfo.FileSystemGalleryTypeRepository;
import org.springframework.stereotype.Repository;

@Repository
public class FileSystemGalleryTypeDaoImpl extends GenericDaoImpl<FileSystemGalleryType, Long> implements FileSystemGalleryTypeDao {

	@Resource
	private FileSystemGalleryTypeRepository fileSystemGalleryTypeRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = fileSystemGalleryTypeRepository;
	}

	@Override
	public List<FileSystemGalleryType> getFileSystemGalleryTypeList(boolean disable, boolean deleted) {
		return fileSystemGalleryTypeRepository.getFileSystemGalleryTypeList(disable, deleted);
	}

}
