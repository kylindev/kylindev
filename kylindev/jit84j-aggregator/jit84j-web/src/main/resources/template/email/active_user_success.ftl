<style  type="text/css">
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.panel-group .panel {
  margin-bottom: 0;
  overflow: hidden;
  border-radius: 4px;
}
.panel-info {
  border-color: #bce8f1;
}

.panel-info > .panel-heading {
  color: #3a87ad;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
</style>

<div class="panel panel-info">
		<div class="panel-heading ">${title} 友情提示</div>
			恭喜${user.username},您已经成功激活帐号。<br/>
			点击下面链接<a href='${emailUrl.url}' target='_blank'><b><font color='red'>进行登录</font></b></a>，如果无法点击，请复制链接粘贴至浏览器地址栏访问：<br/>
			${emailUrl.url}
</div>
