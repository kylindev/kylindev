package org.apache.shiro.web.filter;

import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;

public class Jit8AdminFormAuthenticationFilter extends Jit8FormAuthenticationFilter {
	
	//这里应该是登录页面UI
	public static String jit8LoginUiUrl = KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING;
	
	public static String jit8SuccessUrl = KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING;
	
	
	
	public Jit8AdminFormAuthenticationFilter() {
		setLoginUrl(jit8LoginUiUrl);
		setSuccessUrl(jit8SuccessUrl);
	}
	
	
	
	/**
	 * @return the jit8LoginUrl
	 */
	public static String getJit8LoginUrl() {
		return jit8LoginUrl;
	}
	/**
	 * @param jit8LoginUrl the jit8LoginUrl to set
	 */
	public static void setJit8LoginUrl(String jit8LoginUrl) {
		Jit8AdminFormAuthenticationFilter.jit8LoginUrl = jit8LoginUrl;
	}



	/**
	 * @return the jit8SuccessUrl
	 */
	public static String getJit8SuccessUrl() {
		return jit8SuccessUrl;
	}



	/**
	 * @param jit8SuccessUrl the jit8SuccessUrl to set
	 */
	public static void setJit8SuccessUrl(String jit8SuccessUrl) {
		Jit8AdminFormAuthenticationFilter.jit8SuccessUrl = jit8SuccessUrl;
	}
	
	

}
