package org.apache.shiro.authc;

public class PasswordEmptyException extends AuthenticationException{

	private static final long serialVersionUID = 1L;

	public PasswordEmptyException() {
		super();
	}

	 public PasswordEmptyException(String message) {
        super(message);
    }
}
