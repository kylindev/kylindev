package org.apache.shiro.web.filter;

import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;

public class Jit8FrontFormAuthenticationFilter extends Jit8FormAuthenticationFilter {
	
	public static String jit8LoginUiUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING;
	
	public static String jit8SuccessUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING;
	
	
	public Jit8FrontFormAuthenticationFilter() {
		super();
		setLoginUrl(jit8LoginUiUrl);
		setSuccessUrl(jit8SuccessUrl);
	}
	
	/**
	 * @return the jit8LoginUrl
	 */
	public static String getJit8LoginUrl() {
		return jit8LoginUrl;
	}
	/**
	 * @param jit8LoginUrl the jit8LoginUrl to set
	 */
	public static void setJit8LoginUrl(String jit8LoginUrl) {
		Jit8FrontFormAuthenticationFilter.jit8LoginUrl = jit8LoginUrl;
	}

	/**
	 * @param jit8SuccessUrl the jit8SuccessUrl to set
	 */
	public static void setJit8SuccessUrl(String jit8SuccessUrl) {
		Jit8FrontFormAuthenticationFilter.jit8SuccessUrl = jit8SuccessUrl;
	}


}
