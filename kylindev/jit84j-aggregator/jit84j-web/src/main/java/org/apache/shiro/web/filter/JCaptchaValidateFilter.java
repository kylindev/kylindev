/**
 * Copyright (c) 2005-2012 https://github.com/zhangkaitao
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */
package org.apache.shiro.web.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.IncorrectValidateCodeException;
import org.apache.shiro.web.util.WebUtils;
import org.jit8.framework.jit84j.web.security.captcha.JCaptcha;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;

/**
 * 验证码过滤器
 * <p>User: Zhang Kaitao
 * <p>Date: 13-3-22 下午4:01
 * <p>Version: 1.0
 */
public class JCaptchaValidateFilter extends AccessControlFilter {

    private boolean jcaptchaEbabled = true;

    private String jcaptchaParam = "validateCode";

    private String jcapatchaErrorUrl=KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING;
    
    public static final String JIT8_DEFAULT_ERROR_KEY_ATTRIBUTE_NAME = "jit8LoginFailure"; 
	public String failureKeyAttribute = JIT8_DEFAULT_ERROR_KEY_ATTRIBUTE_NAME;

    /**
     * 是否开启jcaptcha
     *
     * @param jcaptchaEbabled
     */
    public void setJcaptchaEbabled(boolean jcaptchaEbabled) {
        this.jcaptchaEbabled = jcaptchaEbabled;
    }

    /**
     * 前台传入的验证码
     *
     * @param jcaptchaParam
     */
    public void setJcaptchaParam(String jcaptchaParam) {
        this.jcaptchaParam = jcaptchaParam;
    }

    public void setJcapatchaErrorUrl(String jcapatchaErrorUrl) {
        this.jcapatchaErrorUrl = jcapatchaErrorUrl;
    }

    public String getJcapatchaErrorUrl() {
        return jcapatchaErrorUrl;
    }

    @Override
    public boolean onPreHandle(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        request.setAttribute("jcaptchaEbabled", jcaptchaEbabled);
        return super.onPreHandle(request, response, mappedValue);
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        //验证码禁用 或不是表单提交 允许访问
        if (jcaptchaEbabled == false || !"post".equals(httpServletRequest.getMethod().toLowerCase())) {
            return true;
        }
        return JCaptcha.validateResponse(httpServletRequest, httpServletRequest.getParameter(jcaptchaParam));
    }


    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        //redirectToLogin(request, response);
        request.setAttribute(failureKeyAttribute, IncorrectValidateCodeException.class.getName());  
        return true;
    }


    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {
        WebUtils.issueRedirect(request, response, getJcapatchaErrorUrl());
    }

	/**
	 * @return the jcaptchaParam
	 */
	public String getJcaptchaParam() {
		return jcaptchaParam;
	}

	/**
	 * @return the failureKeyAttribute
	 */
	public String getFailureKeyAttribute() {
		return failureKeyAttribute;
	}

	/**
	 * @param failureKeyAttribute the failureKeyAttribute to set
	 */
	public void setFailureKeyAttribute(String failureKeyAttribute) {
		this.failureKeyAttribute = failureKeyAttribute;
	}

}
