package org.apache.shiro.web.filter;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;
import org.jit8.framework.jit84j.core.web.util.EncodeUtil;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;

public class Jit8FormAuthenticationFilter extends FormAuthenticationFilter {
	
	public static String jit8LoginUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING;
	public static String jit8SuccessUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING;
	public static final String JIT8_DEFAULT_ERROR_KEY_ATTRIBUTE_NAME = "jit8LoginFailure"; 
	public static String failureKeyAttribute = JIT8_DEFAULT_ERROR_KEY_ATTRIBUTE_NAME;
	
	public static List<String> jit8LoginUrlContainer = new ArrayList<String>();
	
	public Jit8FormAuthenticationFilter() {
		setLoginUrl(jit8LoginUrl);
		setSuccessUrl(jit8SuccessUrl);
		jit8LoginUrlContainer.add(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING);
		jit8LoginUrlContainer.add(KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_MAPPING);
	}
	
	
	@Override
	 protected boolean isLoginRequest(ServletRequest request, ServletResponse response) {
			String requestURI = getPathWithinApplication(request);
			
	        return jit8LoginUrlContainer.contains(requestURI);
	 }
	
	/**
	 * @return the jit8LoginUrl
	 */
	public static String getJit8LoginUrl() {
		return jit8LoginUrl;
	}
	/**
	 * @param jit8LoginUrl the jit8LoginUrl to set
	 */
	public static void setJit8LoginUrl(String jit8LoginUrl) {
		Jit8FormAuthenticationFilter.jit8LoginUrl = jit8LoginUrl;
	}



	/**
	 * @return the jit8SuccessUrl
	 */
	public static String getJit8SuccessUrl() {
		return jit8SuccessUrl;
	}



	/**
	 * @param jit8SuccessUrl the jit8SuccessUrl to set
	 */
	public static void setJit8SuccessUrl(String jit8SuccessUrl) {
		Jit8FormAuthenticationFilter.jit8SuccessUrl = jit8SuccessUrl;
	}



	/**
	 * @return the failureKeyAttribute
	 */
	public String getFailureKeyAttribute() {
		return failureKeyAttribute;
	}



	/**
	 * @param failureKeyAttribute the failureKeyAttribute to set
	 */
	public void setFailureKeyAttribute(String failureKeyAttribute) {
		Jit8FormAuthenticationFilter.failureKeyAttribute = failureKeyAttribute;
	}
	
	/**
	 * @return the jit8LoginUrlContainer
	 */
	public static List<String> getJit8LoginUrlContainer() {
		return jit8LoginUrlContainer;
	}

	/**
	 * @param jit8LoginUrlContainer the jit8LoginUrlContainer to set
	 */
	public static void setJit8LoginUrlContainer(List<String> jit8LoginUrlContainer) {
		Jit8FrontFormAuthenticationFilter.jit8LoginUrlContainer = jit8LoginUrlContainer;
	}
	
	
	 protected AuthenticationToken createToken(ServletRequest request, ServletResponse response) {
	        String username = getUsername(request);
	        String password = getPassword(request);
	        //String password2 = EncodeUtil.customEncrypt("md5", password, username, 2);
	        return createToken(username, password, request, response);
	 }
	 
	protected boolean onAccessDenied(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
		if (request.getAttribute(getFailureKeyAttribute()) != null) {
			return true;
		}
		return super.onAccessDenied(request, response, mappedValue);
	} 

}
