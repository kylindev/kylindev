package org.apache.shiro.web.filter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.AdviceFilter;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.core.Constants;

public class UserSettingAdviceFilter extends AdviceFilter {
	
	@Override
	protected boolean preHandle(ServletRequest request, ServletResponse response) throws Exception {
		System.out.println("====预处理/前置处理");
		
		
		/*Subject subject = SecurityUtils.getSubject();
        if (subject == null) {
            return false;
        }
        HttpSession session = ((HttpServletRequest)request).getSession();
        User current_user = (User) session.getAttribute(Constants.CURRENT_USER);
		Object recs = session.getAttribute(Constants.USER_MENUS);
        //判断session是否失效，若失效刷新之
        if(current_user == null || recs == null){
        	String username = (String) subject.getPrincipal();
        	User user = userService.findByLoginId(username);
        	session.setAttribute(Constants.CURRENT_USER, user);
        	session.setAttribute(Constants.USER_MENUS, user.getMenus());
        }
		*/
		
		return true;// 返回 false 将中断后续拦截器链的执行
	}

	@Override
	protected void postHandle(ServletRequest request, ServletResponse response) throws Exception {
		System.out.println("====后处理/后置返回处理");
	}

	@Override
	public void afterCompletion(ServletRequest request, ServletResponse response, Exception exception) throws Exception {
		System.out.println("====完成处理/后置最终处理");
	}
}
