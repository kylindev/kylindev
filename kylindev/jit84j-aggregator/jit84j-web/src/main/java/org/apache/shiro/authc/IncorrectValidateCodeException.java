package org.apache.shiro.authc;

public class IncorrectValidateCodeException extends AuthenticationException{

	private static final long serialVersionUID = 1L;

	public IncorrectValidateCodeException() {
		super();
	}

	 public IncorrectValidateCodeException(String message) {
        super(message);
    }
}
