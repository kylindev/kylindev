package org.jit8.site.service.article;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.article.Category;


public interface CategoryService extends GenericService<Category, Long>{

	public List<Category> getCategoryList(boolean disable, boolean deleted);
	
	public List<Category> getCategoryListByIds(boolean disable,boolean deleted,List<Long> categoryIdList);
	
	public List<Category> getCategoryListByParentNull();
}
