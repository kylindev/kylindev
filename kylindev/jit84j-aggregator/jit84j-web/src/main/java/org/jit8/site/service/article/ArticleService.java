package org.jit8.site.service.article;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.article.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ArticleService extends GenericService<Article, Long>{

	public List<Article> getArticleList(boolean disable, boolean deleted);
	
	public Page<Article> getArticleList(boolean disable, boolean deleted,Pageable pageable);
	
	
}
