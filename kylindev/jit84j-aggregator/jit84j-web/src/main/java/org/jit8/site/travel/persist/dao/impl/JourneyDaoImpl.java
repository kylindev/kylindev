package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.JourneyDao;
import org.jit8.site.travel.persist.domain.Journey;
import org.jit8.site.travel.persist.repository.JourneyRepository;
import org.springframework.stereotype.Repository;

@Repository
public class JourneyDaoImpl extends GenericDaoImpl<Journey, Long> implements JourneyDao {

	@Resource
	private JourneyRepository journeyRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = journeyRepository;
	}

	@Override
	public List<Journey> getJourneyList(boolean disable, boolean deleted) {
		return journeyRepository.getJourneyList(disable, deleted);
	}
	
	@Override
	public Journey findByCode(String code) {
		return journeyRepository.findByTitle( code);
	}

	@Override
	public List<Journey> findByIds(List<Long> idList) {
		return journeyRepository.findByIds(idList);
	}
}
