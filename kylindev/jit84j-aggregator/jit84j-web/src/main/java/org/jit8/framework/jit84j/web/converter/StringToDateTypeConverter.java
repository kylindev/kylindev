package org.jit8.framework.jit84j.web.converter;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.ConversionFailedException;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.Converter;

public class StringToDateTypeConverter implements Converter<String, Date> {

    public Date convert(String dateStr) {    
        Date date = null;
        try{
            SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = sd.parse(dateStr);            
        } catch (Exception e) {
            throw new ConversionFailedException(TypeDescriptor.valueOf(String.class),
                    TypeDescriptor.valueOf(Date.class), dateStr, null);
        }        
        return date;
    }
}
