package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteCategorySpecialService;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteCategorySpecialDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StickyNoteCategorySpecialServiceImpl extends GenericServiceImpl<StickyNoteCategorySpecial, Long> implements StickyNoteCategorySpecialService {

	@Resource
	private StickyNoteCategorySpecialDao stickyNoteCategorySpecialDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = stickyNoteCategorySpecialDao;
	}


	@Override
	public List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList(boolean disable, boolean deleted) {
		return stickyNoteCategorySpecialDao.getStickyNoteCategorySpecialList(disable, deleted);
	}


	@Override
	public Page<StickyNoteCategorySpecial> findAll(
			StickyNoteCategorySpecial stickyNoteCategorySpecial,
			Pageable pageable) {
		return stickyNoteCategorySpecialDao.findAll(stickyNoteCategorySpecial, pageable);
	}


	@Override
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList) {
		return stickyNoteCategorySpecialDao.getByCategorySpecialIdList(categorySpecialIdList);
	}


}
