package org.jit8.site.travel.biz.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.site.travel.biz.business.InstantShareImageCounterBusiness;
import org.jit8.site.travel.biz.service.InstantShareImageCounterService;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("instantShareImageCounterBusiness")
public class InstantShareImageCounterBusinessImpl implements InstantShareImageCounterBusiness{

	@Resource
	private InstantShareImageCounterService instantShareImageCounterService;

	@Transactional
	@Override
	public InstantShareImageCounter persist(
			InstantShareImageCounter instantShareImageCounter) {
		
		return instantShareImageCounterService.save(instantShareImageCounter);
	}

	@Transactional
	@Override
	public InstantShareImageCounter merge(InstantShareImageCounter instantShareImageCounter) {
		Long id = instantShareImageCounter.getId();
		if(id != null && id>0){
			instantShareImageCounter = instantShareImageCounterService.save(instantShareImageCounter);
		}
		return instantShareImageCounter;
	}

	
	@Override
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId) {
		if(null != id && null != userId){
			InstantShareImageCounter instantShareImageCounter = instantShareImageCounterService.findByIdAndUserId(id, userId);
			return instantShareImageCounter;
		}
		
		return null;
	}

	@Override
	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId) {
		
		if(StringUtils.isNotEmpty(day) && null != userId){
			
			InstantShareImageCounter instantShareImageCounter = instantShareImageCounterService.findByCountKeyAndUserId(day, userId);
			
			return instantShareImageCounter;
		}
		
		return null;
	}
	
	@Override
	public InstantShareImageCounter findByDailyAndUserId(Date date, Long userId) {
		if(null != date && null != userId){
			String specificDate = CalendarUtils.getSpecificYearMonthDayWithFormat(date, null);
			if(StringUtils.isNotEmpty(specificDate) && null != userId){
				
				InstantShareImageCounter instantShareImageCounter = instantShareImageCounterService.findByCountKeyAndUserId(specificDate, userId);
				
				return instantShareImageCounter;
			}
		}
		
		return null;
	}
	
	
	

	@Transactional
	@Override
	public InstantShareImageCounter addCountByDayAndUserId(int count, String day, Long userId) {
		
		return null;
	}

	@Transactional
	@Override
	public InstantShareImageCounter addCountByDayAndUserId(int count, Date date, Long userId) {
		
		if(date != null && userId != null){
			
			String currentDate = CalendarUtils.getSpecificYearMonthDayWithFormat(date, null);
			
			InstantShareImageCounter instantShareImageCounter = findByCountKeyAndUserId(currentDate,userId);
			
			if(null == instantShareImageCounter){
				
				instantShareImageCounter = new InstantShareImageCounter();
				
				String currentYearMonth = CalendarUtils.getSpecificYearMonthWithFormat(date, null);
				String currentDay = CalendarUtils.getSpecificDayWithFormat(date, null);
				
				instantShareImageCounter.setCountDate(date);
				instantShareImageCounter.setCountKey(currentDate);
				instantShareImageCounter.setYearMonth(currentYearMonth);
				instantShareImageCounter.setDay(currentDay);
				instantShareImageCounter.setUserId(userId);
				User user = Jit8ShareInfoUtils.getUser();
				instantShareImageCounter.setUser(user);
			}
			
			int countNum = instantShareImageCounter.getCount() + count;
			if(countNum<0){
				countNum = 0;
			}
			
			instantShareImageCounter.setCount(countNum);
			
			instantShareImageCounter.setUpdateDate(new Date());
			
			instantShareImageCounter = persist(instantShareImageCounter);
			
			return instantShareImageCounter;
		}
		
		return null;
	}

	@Override
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId) {
		
		if(StringUtils.isNotEmpty(yearMonth) && null != userId){
			List<InstantShareImageCounter> list = instantShareImageCounterService.findByYearMonthAndUserId(yearMonth, userId);
			
			return list;
		}
		
		return null;
	}

	@Override
	public List<InstantShareImageCounter> findByMonthForUser(String yearMonth, Long userId) {
		
		List<InstantShareImageCounter> instantShareImageCounterList = findByYearMonthAndUserId( yearMonth,  userId);
		
		List<InstantShareImageCounter> list = new ArrayList<InstantShareImageCounter>(31);
		int firstDay = CalendarUtils.getFirstDayOfMonth(yearMonth, "yyyyMM");
		int lastDay = CalendarUtils.getLastDayOfMonth(yearMonth, "yyyyMM");
		Map<String,InstantShareImageCounter> map = new HashMap<String,InstantShareImageCounter>();
		if(null != instantShareImageCounterList && instantShareImageCounterList.size()>0){
			for(InstantShareImageCounter instantShareImageCounter : instantShareImageCounterList){
				map.put(instantShareImageCounter.getDay(), instantShareImageCounter);
			}
		}
		for(int i=firstDay;i<=lastDay; i++){
			
			String key = CalendarUtils.fillZeroForDay(i);
			
			InstantShareImageCounter exist = map.get(key);
			if(null != exist){
				list.add(exist);
			}else{
				InstantShareImageCounter empty = new InstantShareImageCounter();
				empty.setDay(key);
				empty.setYearMonth(yearMonth);
				empty.setCountKey(yearMonth+key);
				list.add(empty);
			}
		}
			
		
		
		return list;
	}
	
	public List<InstantShareImageCounter> findByCurrentMonthForUser( Long userId){
		
		String yearMonth = CalendarUtils.getSpecificYearMonthWithFormat(new Date(), "yyyyMM");
		return findByMonthForUser(yearMonth,userId);
	}
	
	
}
