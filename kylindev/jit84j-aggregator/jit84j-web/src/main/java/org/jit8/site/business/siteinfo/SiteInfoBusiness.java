package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.SiteInfo;



public interface SiteInfoBusiness {

	public List<SiteInfo> getSiteInfoList();
	
	public SiteInfo persist(SiteInfo siteInfo);
	
	public SiteInfo merge(SiteInfo siteInfo);
	
	public SiteInfo getSiteInfoById(Long id);
	
	public SiteInfo deleteSiteInfoById(Long id);
	
	//public SiteInfo getFSiteInfo
	public SiteInfo getSiteInfoByType(String type);
	
	public SiteInfo getMainSiteInfo();
}
