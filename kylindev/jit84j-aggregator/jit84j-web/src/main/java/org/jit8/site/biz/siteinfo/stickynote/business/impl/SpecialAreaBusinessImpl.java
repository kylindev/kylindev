package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.SpecialAreaService;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("specialAreaBusiness")
public class SpecialAreaBusinessImpl implements SpecialAreaBusiness{

	@Resource
	private SpecialAreaService specialAreaService;
	

	@Override
	public List<SpecialArea> getSpecialAreaList() {
		return specialAreaService.getSpecialAreaList(false, false);
	}
	
	@Transactional
	@Override
	public SpecialArea persist(SpecialArea specialArea){
		Long id = specialArea.getId();
		SpecialArea exist = null;
		if(id != null && id>0){
			 exist = specialAreaService.findOne(specialArea.getId());
			 exist.setCode(specialArea.getCode());
			 exist.setDescription(specialArea.getDescription());
			 exist.setImagePath(specialArea.getImagePath());
			 exist.setName(specialArea.getName());
			 exist.setOpenable(specialArea.isOpenable());
			 exist.setWritable(specialArea.isWritable());
			 exist.setSequence(specialArea.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=specialArea;
		}
		
		return specialAreaService.save(exist);
	}

	@Override
	public SpecialArea getSpecialAreaById(Long id) {
		return specialAreaService.findOne(id);
	}

	@Override
	public SpecialArea deleteSpecialAreaById(Long id) {
		SpecialArea old = specialAreaService.findOne(id);
		old.setDeleted(true);
		return specialAreaService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			specialAreaService.delete(id);
		}
	}

	@Override
	public Page<SpecialArea> findAll(Pageable pageable) {
		return specialAreaService.findAll(pageable);
	}

	@Override
	public SpecialArea findByCode(String code) {
		return specialAreaService.findByCode( code);
	}

	@Override
	public List<SpecialArea> findByIds(List<Long> idList) {
		return specialAreaService.findByIds(idList);
	}

}
