package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.business.userinfo.PermissionCategoryBusiness;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.jit8.user.service.userinfo.PermissionCategoryService;
import org.jit8.user.service.userinfo.PermissionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("permissionCategoryBusiness")
public class PermissionCategoryBusinessImpl implements PermissionCategoryBusiness {

	@Resource
	private PermissionCategoryService permissionCategoryService;
	
	@Resource
	private PermissionService permissionService;
	
	@Resource
	private DeveloperBusiness developerBusiness;
	
	@Resource
	private PermissionBusiness permissionBusiness;

	@Override
	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable) {
		
		return permissionCategoryService.getPermissionCategoryList(pageable);
	}

	@Override
	public PmnCategory persist(PmnCategory pmnCategory) {
		Long id = pmnCategory.getId();
		PmnCategory permissionCategoryExist = pmnCategory;
		if(id!= null && id>0){
			permissionCategoryExist = permissionCategoryService.findOne(id);
			permissionCategoryExist.setName(pmnCategory.getName());
			permissionCategoryExist.setCode(pmnCategory.getCode());
			permissionCategoryExist.setDescription(pmnCategory.getDescription());
			permissionCategoryExist.setSequence(pmnCategory.getSequence());
			if(pmnCategory.getParent()!=null){
				PmnCategory parent = permissionCategoryService.findOne(pmnCategory.getParent().getId());
				permissionCategoryExist.setParent(parent);
			}
		}
		
		return permissionCategoryService.save(permissionCategoryExist);
	}

	@Override
	public void remove(Long id) throws BusinessException{
		PmnCategory permissionCategoryExist = permissionCategoryService.findOne(id);
		List<PmnCategory> childrenCategory = permissionCategoryExist.getChildrenCategory();
		if(null != childrenCategory && childrenCategory.size()>0){
			throw new BusinessException("org.jit8.100001.userinfo.permissionCategory.delete.error","can't delete, have children items");
		}
		permissionCategoryService.delete(id);
	}

	@Transactional
	@Override
	public void assignPermissionToCategory(PmnCategory pmnCategory) throws BusinessException{
		if(null == pmnCategory){
			throw new BusinessException("org.jit8.100001.userinfo.permissionCategory.assign.permission.list.error", "assign permission list to category error, permission category id can't be null ");
		}
		
		Long id = pmnCategory.getId();
		if(null == id || id < 1){
			throw new BusinessException("org.jit8.100001.userinfo.permissionCategory.assign.permission.list.error", "assign permission list to category error, permission category id can't be null ");
		}

		PmnCategory permissionCategoryExist = permissionCategoryService.findOne(id);
		List<Permission> permissionList = pmnCategory.getPermissionList();
		if(null != permissionList && permissionList.size()>0){
			List<Permission> permissionListExist = new ArrayList<Permission>();
			permissionCategoryExist.setPermissionList(permissionListExist);
			
			for(Permission permission : permissionList){
				if(null != permission.getId()){
					Permission permissionExist = permissionService.findOne(permission.getId());
					permissionListExist.add(permissionExist);
					permissionExist.setCategoryId(id);
					permissionExist.setCategory(permissionCategoryExist);
				}
			}
			
			if(null != permissionListExist && permissionListExist.size()>0){
				permissionService.save(permissionListExist);
			}
			permissionCategoryService.save(permissionCategoryExist);
		}
		
	}
	
	@Transactional
	public void removePermissionFromCategory(PmnCategory pmnCategory) throws BusinessException{
		if(null == pmnCategory){
			throw new BusinessException("org.jit8.100001.userinfo.permissionCategory.assign.permission.list.error", "assign permission list to category error, permission category id can't be null ");
		}
		
		/*Long id = pmnCategory.getId();
		if(null == id || id < 1){
			throw new BusinessException("org.jit8.100001.userinfo.permissionCategory.assign.permission.list.error", "assign permission list to category error, permission category id can't be null ");
		}*/
		//PmnCategory permissionCategoryExist = permissionCategoryService.findOne(id);
		List<Permission> permissionList = pmnCategory.getPermissionList();
		Map<Long, Permission> permissionMap = new HashMap<Long,Permission>();
		
		//List<Permission> permissionExistList = permissionCategoryExist.getPermissionList();
		
		/*if(null != permissionExistList && permissionExistList.size()>0){
			List<Permission> permissionListAfterRemoved = new ArrayList<Permission>();
			for(Permission permissionExist : permissionExistList){
				
				permissionMap.put(permissionExist.getId(), permissionExist);
				
				
			}*/
		
			
			if(null != permissionList && permissionList.size()>0){
				List<Long> permissionIdList = new ArrayList<Long>();
				for(Permission permissionRemove : permissionList){
					Long permissionId = permissionRemove.getId();
					if(null != permissionId ){
						//Permission permissionExist = permissionMap.get(permissionId);
						//permissionExist.setCategoryId(null);
						//permissionExist.setCategory(null);
						permissionIdList.add(permissionId);
						//permissionListAfterRemoved.add(permissionExist);
					}
				}
				
				List<Permission> permisssionExistList = permissionBusiness.fineByIdList(permissionIdList);
				if(null != permisssionExistList){
					for(Permission permissionExist : permisssionExistList){
						permissionExist.setCategoryId(null);
						permissionExist.setCategory(null);
					}
				}
				
				//permissionExistList.removeAll(permissionListAfterRemoved);
			}
			/*permissionCategoryExist.setPermissionList(permissionExistList);
			permissionCategoryService.save(permissionCategoryExist);
		}*/
		
	}

	@Transactional
	@Override
	public void removeAllPermissionFromCategory(
			Long permissionId)  throws BusinessException{
			Permission permission = permissionService.findOne(permissionId);
			permission.setCategory(null);
			permissionService.save(permission);
	}

	@Override
	public Permission enablePermission(Long permissionId) {
		Permission permission = new Permission();
		permission.setId(permissionId);
		permission.setStatus(Permission.STATUS_ENABLE);
		return permissionBusiness.updatePermissionStatus(permission);
	}
	
	
}
