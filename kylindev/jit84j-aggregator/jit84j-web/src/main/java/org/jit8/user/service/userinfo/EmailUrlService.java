package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.EmailUrl;

public interface EmailUrlService extends GenericService<EmailUrl, Long>{

	public EmailUrl findByUrlCode(String urlCode);
}
