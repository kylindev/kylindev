package org.jit8.site.service.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.menu.Navigation;


public interface NavigationService extends GenericService<Navigation, Long>{

	public static final String TOP_NAME="顶级导航";
	public static final String TOP_CODE="top_Navigation";
	
	public List<Navigation> getNavigationList(boolean disable, boolean deleted);
	
	public List<Navigation> getNavigationListByIds(boolean disable,boolean deleted,List<Long> navigationIdList);
	
	public List<Navigation> getNavigationListByParentNull();
	
	public List<Navigation> getNavigationByCode(boolean disable, boolean deleted,
			String code);
	
	public List<Navigation> getNavigationByTopCode(boolean disable,
			boolean hideable, String code);
	public List<Navigation> getNavigationByTopCode(boolean disable,
			 String code);
	
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId);
	
	public List<Navigation> getNavigationByParentCode(
			boolean deleted, String code);
	
	public Navigation findNavigationByIdKey(String idKey);
}
