package org.jit8.site.travel.biz.service;

import java.util.Date;
import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ManifestoService extends GenericService<Manifesto, Long>{

	Page<Manifesto> getList(Date begin, Date end,Pageable pageable);
	
	public List<Manifesto> findManifestoByPublishDateAndUserId(Date begin, Date end, Long userId);
	
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable, Pageable commontPageable);
	
	public Manifesto findOneWithPageable(Long id, Pageable commentPageable);
	
	public Long findPreviousOneByCurrentId(Long id);
	
	public Long findFollowingOneByCurrentId(Long id);
	
	public Manifesto findSimpleOneById(Long id);
	
	public Long findLastId();
}
