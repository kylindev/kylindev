package org.jit8.user.service.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.EmailUrlDao;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.service.userinfo.EmailUrlService;
import org.springframework.stereotype.Service;

@Service
public class EmailUrlServiceImpl extends GenericServiceImpl<EmailUrl, Long> implements EmailUrlService {

	@Resource
	private EmailUrlDao emailUrlDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = emailUrlDao;
	}


	@Override
	public EmailUrl findByUrlCode(String urlCode) {
		return emailUrlDao.findByUrlCode(urlCode);
	}

	
	
}
