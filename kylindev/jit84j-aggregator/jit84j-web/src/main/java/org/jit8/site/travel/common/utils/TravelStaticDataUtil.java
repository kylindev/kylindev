package org.jit8.site.travel.common.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.cache.memached.MemachedDefinition;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.core.web.util.MemcachedUtil;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineManager;
import org.jit8.framework.jit84j.web.security.shiro.Jit8ShiroUtils;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.travel.biz.business.menu.managerarea.UserManageAreaItemBusiness;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class TravelStaticDataUtil{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(TravelStaticDataUtil.class);

	public static final String USERMANAGEAREAITEM_KEY = KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID_AND_SEPERATOR + "_userManageAreaItem_";
	public static final String USERMANAGEAREAITEM_MAP_KEY = KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID_AND_SEPERATOR + "_userManageAreaItem_map";
	
	@Resource
	protected MessageSource messageSource;
	
	@Resource
	protected StaticDataDefineManager staticDataDefineManager;
	
	private static StaticDataDefineManager sdm;
	
	
	
	@PostConstruct
	public void init(){
		sdm = staticDataDefineManager;
	}
	
	
	public static List<UserManageAreaItem> getUserManageAreaItemListByUserId(Long userId){
		
		
		List<UserManageAreaItem> userManageAreaItemList = null;
			String key = USERMANAGEAREAITEM_KEY + userId;
			LOGGER.info("begin---getUserManageAreaItemListByUserId:" + userId);
			Object obj = MemcachedUtil.get(key, sdm.getMemcachedClient());
			
			if(null == obj){
				UserManageAreaItemBusiness userManageAreaItemBusiness = (UserManageAreaItemBusiness) ApplicationContext.getBean(UserManageAreaItemBusiness.class);
				if(null != userManageAreaItemBusiness){
					userManageAreaItemList = userManageAreaItemBusiness.findAllDisplayByUserId(userId);
					LOGGER.info("call---userManageAreaItemBusiness.findAllDisplayByUserId:" + userId + " size:" + userManageAreaItemList.size() + " expires:"+MemachedDefinition.EXPIRES_TIME_24H);
					MemcachedUtil.set(key, MemachedDefinition.EXPIRES_TIME_24H, userManageAreaItemList, sdm.getMemcachedClient());
				}
			}else{
				userManageAreaItemList=(List<UserManageAreaItem>)obj;
			}
			LOGGER.info("end---getUserManageAreaItemListByUserId:" + userId);
		return userManageAreaItemList;
	}
	
	public static Map<String,UserManageAreaItem> getUserManageAreaItemMapByUserId(Long userId){
		
		Map<String,UserManageAreaItem> userManageAreaItemMap = new HashMap<String,UserManageAreaItem>();
			String key = USERMANAGEAREAITEM_MAP_KEY + userId;
			LOGGER.info("begin---getUserManageAreaItemMapByUserId:" + userId);
			Object obj = MemcachedUtil.get(key, sdm.getMemcachedClient());
			
			if(null == obj){
				List<UserManageAreaItem> userManageAreaItemList = getUserManageAreaItemListByUserId(userId);
				if(null == userManageAreaItemList){
					UserManageAreaItemBusiness userManageAreaItemBusiness = (UserManageAreaItemBusiness) ApplicationContext.getBean(UserManageAreaItemBusiness.class);
					if(null != userManageAreaItemBusiness){
						userManageAreaItemList = userManageAreaItemBusiness.findAllDisplayByUserId(userId);
						LOGGER.info("call---userManageAreaItemBusiness.findAllDisplayByUserId:" + userId + " size:" + userManageAreaItemList.size());
					}
				}
				for(UserManageAreaItem uma : userManageAreaItemList){
					String url = uma.getManageAreaItem().getUrl();
					userManageAreaItemMap.put(url, uma);
				}
				LOGGER.info("call---userManageAreaItemBusiness.getUserManageAreaItemMapByUserId:" + userId + " size:" + userManageAreaItemMap.size() + " expires:"+MemachedDefinition.EXPIRES_TIME_24H);
				MemcachedUtil.set(key, MemachedDefinition.EXPIRES_TIME_24H, userManageAreaItemMap, sdm.getMemcachedClient());
			}else{
				userManageAreaItemMap = (Map<String,UserManageAreaItem>)obj;
			}
			LOGGER.info("end---getUserManageAreaItemMapByUserId:" + userId);
		return userManageAreaItemMap;
	}
	
	public static UserManageAreaItem getCurrentUserManageAreaItemByUserIdAndUrl(Long userId,String url){
		
		Map<String,UserManageAreaItem> userManageAreaItemMap = getUserManageAreaItemMapByUserId(userId);
		
		UserManageAreaItem current = null;
		
		if(null != userManageAreaItemMap){
			
			current = userManageAreaItemMap.get(url);
			
		}
		
		return current;
	}
	
	public static void refreshUserManageAreaItemCacheByUserId(Long userId){
		String key = USERMANAGEAREAITEM_KEY + userId;
		String keyMap = USERMANAGEAREAITEM_MAP_KEY + userId;
		MemcachedUtil.delete(key, sdm.getMemcachedClient());
		MemcachedUtil.delete(keyMap, sdm.getMemcachedClient());
		
		getUserManageAreaItemListByUserId(userId);
		
		getUserManageAreaItemMapByUserId(userId);
		
	}
	
	public static void refreshUserManageAreaItemCache(){
		
		Collection<Long> userIds = Jit8ShiroUtils.getActiveUserId();
		LOGGER.info("begin---refreshUserManageAreaItemCache:" );
		if(null != userIds){
			for(Long userId : userIds){
				LOGGER.info("process---refreshUserManageAreaItemCache:" + userId );
				refreshUserManageAreaItemCacheByUserId(userId);
			}
		}
		LOGGER.info("end---refreshUserManageAreaItemCache:" );
	}
	
	
	public static List<Navigation> getNavigationTopList(){
		List<Navigation> navigationTopList = (List<Navigation>)sdm.getData(StaticDataDefineConstant.NAVIGATION_TOP_LIST, false);
		return navigationTopList;
	}
	
	public static List<NavigatMenu> getNavigatMenuTopList(){
		List<NavigatMenu> navigatMenuTopList = (List<NavigatMenu>) sdm.getData(StaticDataDefineConstant.NAVIGATMENU_TOP_LIST);
		return navigatMenuTopList;
	}
	
	public static NavigatMenu getCurrentNavigatMenuByCode(String code){
		Map<String,NavigatMenu > map = (Map<String,NavigatMenu >)sdm.getData(StaticDataDefineConstant.NAVIGATMENU_MAP);
		NavigatMenu navigatMenu = null;
		if(null != map){
			navigatMenu = map.get(code);
		}
		return navigatMenu;
	}
}
