package org.jit8.framework.jit84j.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MemachedModelController extends BaseCommonController{

private static final Logger logger = LoggerFactory.getLogger(MemachedModelController.class);
	
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;
	
	
	
	public MemachedModelController(){
		logger.debug("######### init LoginController #########");
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_MAPPING)
	public String getMemachedModelList(Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		return "memachedModelObjectListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_MAPPING)
	public String getMemachedModelIndexUI(Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		return "memachedModelIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_MAPPING)
	public String getMemachedModelListUI(Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		return "memachedModelListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING)
	@ResponseBody
	public Map<String,String> getMemachedModelSetExp(@ModelAttribute("memachedModel")MemachedModel memachedModel,Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		boolean success = staticDataDefineCoreManager.touch(memachedModel.getKey(), memachedModel.getExpiresTime());
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.siteInfo.memcachedModel.setExp.success",null);
		if(!success){
			msg="failure";
		}
		result.put("msg", msg);
		return result;
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_MAPPING)
	@ResponseBody
	public Map<String,String> refreshMemachedModel(@ModelAttribute("memachedModel")MemachedModel memachedModel,Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		boolean success = staticDataDefineCoreManager.refresh(memachedModel.getKey(), true);

		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.siteInfo.memcachedModel.refresh.success",null);
		if(!success){
			msg="failure";
		}
		result.put("msg", msg);
		
		return result;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_MAPPING)
	@ResponseBody
	public Map<String,String> refreshAllMemachedModel(@ModelAttribute("memachedModel")MemachedModel memachedModel,Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		String keys = StringUtils.trimToEmpty(memachedModel.getKeys());
		String[] keysArray = keys.split(",");
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.siteInfo.memcachedModel.refresh.success",null);
		boolean success = false;
		if(null != keysArray){
			for(String key : keysArray){
				success = staticDataDefineCoreManager.refresh(key, true);
				if(!success){
					msg="failure: key=" + key;
					break;
				}
			}
		}
		Map<String,String> result = new HashMap<String,String>();
		
		result.put("msg", msg);
		
		return result;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_MAPPING)
	@ResponseBody
	public Map<String,String> deleteMemachedModel(@ModelAttribute("memachedModel")MemachedModel memachedModel,Model model){
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE));
		model.addAttribute("memachedList", StaticDataDefineConstant.getMemachedModelByKey().values());
		model.addAttribute("navigationTopList",navigationTopList);
		
		boolean success = staticDataDefineCoreManager.delete(memachedModel.getKey());
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.siteInfo.memcachedModel.delete.success",null);
		
		if(!success){
			msg="failure";
		}
		result.put("msg", msg);
		
		return result;
	}
	
	
}
