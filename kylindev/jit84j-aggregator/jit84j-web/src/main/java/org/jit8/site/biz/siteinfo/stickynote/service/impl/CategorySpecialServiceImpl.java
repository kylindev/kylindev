package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.CategorySpecialService;
import org.jit8.site.persist.siteinfo.stickynote.dao.CategorySpecialDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class CategorySpecialServiceImpl extends GenericServiceImpl<CategorySpecial, Long> implements CategorySpecialService {

	@Resource
	private CategorySpecialDao categorySpecialDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = categorySpecialDao;
	}


	@Override
	public List<CategorySpecial> getCategorySpecialList(boolean disable, boolean deleted) {
		return categorySpecialDao.getCategorySpecialList(disable, deleted);
	}


	@Override
	public void removeBySpecialIdAndCategoryIdList(Long specialId,
			List<Long> categoryIdList) {
		categorySpecialDao.removeBySpecialIdAndCategoryIdList(specialId, categoryIdList);
	}


	@Override
	public List<CategorySpecial> findByIdList(List<Long> idList) {
		return categorySpecialDao.findByIdList( idList) ;
	}


	@Override
	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,
			Pageable pageable) {
		return categorySpecialDao.findAll( categorySpecial,pageable);
	}


	@Override
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id) {
		return categorySpecialDao.getCategorySpecialListBySpecialAreaId( id) ;
	}


	@Override
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList) {
		return categorySpecialDao.getByCategorySpecialIdList(categorySpecialIdList);
	}
	
}
