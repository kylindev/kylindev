package org.jit8.site.travel.biz.business;

import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;





public interface ManifestoCommentBusiness {

	ManifestoComment persist(ManifestoComment manifestoComment,User user);
	
	public Page<ManifestoComment> persistWithReturnPage(ManifestoComment manifestoComment, User user,Pageable pageable);
	
	public Page<ManifestoComment> findByManifestoId(Long manifestoId, Pageable pageable);
	
	public ManifestoComment findOneByIdAndUserId(Long id, Long userId);
}
