package org.jit8.site.travel.web.controller.menu.managerarea;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.menu.managerarea.ItemButtonBusiness;
import org.jit8.site.travel.biz.business.menu.managerarea.ManageAreaItemBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ItemButtonController{

	private static final Logger logger = LoggerFactory.getLogger(ItemButtonController.class);
	
	
	@Resource
	private ItemButtonBusiness itemButtonBusiness;
	
	@Resource
	private ManageAreaItemBusiness manageAreaItemBusiness;
	
	@Resource
	private Mapper mapper;
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING)
	public String userItemButtonEditUI(@ModelAttribute("itemButton") ItemButton itemButton,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING + " #########");
		Long id = itemButton.getId();
		if(id != null && id > 0){
			//userItemButton = userItemButtonBusiness.getUserItemButtonById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("itemButton", itemButton);
		model.addAttribute("batch", CalendarUtils.getNano1000Time());
		return "itemButtonEditUI";
	}
	
	
	
	
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_INDEX_UI_MAPPING)
	public String itemButtonIndexUI(@ModelAttribute("itemButton") ItemButton itemButton,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_INDEX_UI_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("itemButton", itemButton);
		
		
		Page<ItemButton> page= itemButtonBusiness.findAll(pageable);
		model.addAttribute("page", page);
		
		List<ManageAreaItem> manageAreaItemList = manageAreaItemBusiness.findAllDisplay();
		model.addAttribute("manageAreaItemList", manageAreaItemList);
		model.addAttribute("currentManageAreaItem", new ManageAreaItem());
		
		return "itemButtonIndexUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_UI_MAPPING)
	public String itemButtonEditUI(@ModelAttribute("itemButton") ItemButton itemButton,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_UI_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		Long id = itemButton.getId();
		if(null != id && id>0){
			itemButton = itemButtonBusiness.findById(id);
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("itemButton", itemButton);
		Page<ItemButton> page= itemButtonBusiness.findAll(pageable);
		model.addAttribute("page", page);
		
		List<ManageAreaItem> manageAreaItemList = manageAreaItemBusiness.findAllDisplay();
		model.addAttribute("manageAreaItemList", manageAreaItemList);
		
		//return "stickyNoteCategoryEditUI";
		return "itemButtonEditUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_REMOVE_MAPPING)
	public String itemButtonRemove(@ModelAttribute("itemButton") ItemButton itemButton, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = itemButton.getId();
		if(null != id && id>0){
			itemButtonBusiness.removeById(id);
			itemButton.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return itemButtonEditUI( itemButton,model,pageable);
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_MAPPING)
	public String itemButtonEdit(@ModelAttribute("itemButton") ItemButton itemButton,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_MAPPING +" #########");
//		String code= KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT;
//		if(StringUtils.isNotEmpty(code)){
//			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
//		}
//		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		
		itemButton = itemButtonBusiness.persist(itemButton);
		//Page<ItemButton> page= itemButtonBusiness.findAll(pageable);
		
		//model.addAttribute("page", page);
		//model.addAttribute("itemButton", itemButton);
		
		return itemButtonEditUI( itemButton,model,pageable);
	}
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_LIST_EDIT_MAPPING)
	public String itemButtonListEdit(@ModelAttribute("itemButton") ItemButton itemButton,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_ITEMBUTTON_EDIT_MAPPING +" #########");
		
		List<ItemButton> itemButtonList = itemButtonBusiness.persistList(itemButton.getItemButtonList());
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return itemButtonEditUI( itemButton,model,pageable);
	}
}
