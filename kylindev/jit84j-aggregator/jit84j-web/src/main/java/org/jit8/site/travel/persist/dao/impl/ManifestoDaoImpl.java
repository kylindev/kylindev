package org.jit8.site.travel.persist.dao.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.ManifestoDao;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.repository.ManifestoRepository;
import org.jit8.site.travel.persist.repository.spec.ManifestoSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ManifestoDaoImpl extends GenericDaoImpl<Manifesto, Long> implements ManifestoDao {

	@Resource
	private ManifestoRepository manifestoRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = manifestoRepository;
	}

	@Override
	public Page<Manifesto> getList(Date begin, Date end, Pageable pageable) {
		return manifestoRepository.getList( begin,  end,  pageable);
	}
	
	@Override
	public List<Manifesto> findManifestoByPublishDateAndUserId(Date begin, Date end, Long userId) {
		return manifestoRepository.findManifestoByPublishDateAndUserId( begin,  end,  userId);
	}

	@Override
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable) {
		Page<Manifesto> page = manifestoRepository.findAll(ManifestoSpecifications.queryByCondition(manifesto), pageable);
		
		return page;
	}
	
	@Override
	public Long findPreviousOneByCurrentId(Long id) {
		return manifestoRepository.findPreviousOneByCurrentId(id);
	}

	@Override
	public Long findFollowingOneByCurrentId(Long id) {
		return manifestoRepository.findFollowingOneByCurrentId(id);
	}


	@Override
	public Manifesto findSimpleOneById(Long id) {
		return manifestoRepository.findSimpleOneById(id);
	}

	@Override
	public Long findLastId() {
		return manifestoRepository.findLastId();
	}

}
