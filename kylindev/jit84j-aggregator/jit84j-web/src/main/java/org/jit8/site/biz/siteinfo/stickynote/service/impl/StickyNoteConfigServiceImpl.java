package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteConfigService;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteConfigDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.springframework.stereotype.Service;

@Service
public class StickyNoteConfigServiceImpl extends GenericServiceImpl<StickyNoteConfig, Long> implements StickyNoteConfigService {

	@Resource
	private StickyNoteConfigDao stickyNoteConfigDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = stickyNoteConfigDao;
	}


	@Override
	public List<StickyNoteConfig> getStickyNoteConfigList(boolean disable, boolean deleted) {
		return stickyNoteConfigDao.getStickyNoteConfigList(disable, deleted);
	}
	
}
