package org.jit8.site.web.controller.command;

import java.io.Serializable;
import java.util.List;

import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public class UrlStaticMappingFillterCommand implements Serializable{

	private static final long serialVersionUID = 3914947275685287412L;

	private List<UrlStaticMapping> allUrlStaticMappingList;
	
	private List<UrlStaticMapping> existUrlStaticMappingList;
	
	private List<UrlStaticMapping> addedUrlStaticMappingList;
	
	private List<UrlStaticMapping> modifiedUrlStaticMappingList;

	public List<UrlStaticMapping> getExistUrlStaticMappingList() {
		return existUrlStaticMappingList;
	}

	public void setExistUrlStaticMappingList(
			List<UrlStaticMapping> existUrlStaticMappingList) {
		this.existUrlStaticMappingList = existUrlStaticMappingList;
	}

	public List<UrlStaticMapping> getAddedUrlStaticMappingList() {
		return addedUrlStaticMappingList;
	}

	public void setAddedUrlStaticMappingList(
			List<UrlStaticMapping> addedUrlStaticMappingList) {
		this.addedUrlStaticMappingList = addedUrlStaticMappingList;
	}

	public List<UrlStaticMapping> getModifiedUrlStaticMappingList() {
		return modifiedUrlStaticMappingList;
	}

	public void setModifiedUrlStaticMappingList(
			List<UrlStaticMapping> modifiedUrlStaticMappingList) {
		this.modifiedUrlStaticMappingList = modifiedUrlStaticMappingList;
	}

	public List<UrlStaticMapping> getAllUrlStaticMappingList() {
		return allUrlStaticMappingList;
	}

	public void setAllUrlStaticMappingList(
			List<UrlStaticMapping> allUrlStaticMappingList) {
		this.allUrlStaticMappingList = allUrlStaticMappingList;
	}
	
}
