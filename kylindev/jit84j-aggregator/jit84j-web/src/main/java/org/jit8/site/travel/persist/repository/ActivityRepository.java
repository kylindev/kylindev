package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ActivityRepository extends GenericJpaRepository<Activity, Long>{

	
	@Query("from Activity o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<Activity> getActivityList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from Activity o where o.title=:title")
	public Activity findByTitle(@Param("title")String title);
	
	@Query("from Activity o where o.uniqueIdentifier in :idList")
	public List<Activity> findByIds(@Param("idList")List<Long> idList);
	
	@Query("from Activity o where o.publishUserId = :publishUserId and o.deleted=false")
	public Page<Activity> findByUserId(@Param("publishUserId")Long publishUserId, Pageable pageable);
}
