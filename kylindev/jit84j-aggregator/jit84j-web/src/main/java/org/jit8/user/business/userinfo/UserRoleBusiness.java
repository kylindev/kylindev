package org.jit8.user.business.userinfo;

import org.jit8.user.persist.domain.userinfo.UserRole;



public interface UserRoleBusiness {

	public void asignRolesForUser(UserRole userRole);
	
	public void removeRolesForUser(UserRole userRole);
}
