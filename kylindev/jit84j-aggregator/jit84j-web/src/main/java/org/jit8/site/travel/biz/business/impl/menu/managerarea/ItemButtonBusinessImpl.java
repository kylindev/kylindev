package org.jit8.site.travel.biz.business.impl.menu.managerarea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.menu.managerarea.ItemButtonBusiness;
import org.jit8.site.travel.biz.service.menu.managerarea.ItemButtonService;
import org.jit8.site.travel.biz.service.menu.managerarea.ManageAreaItemService;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("itemButtonBusiness")
public class ItemButtonBusinessImpl implements ItemButtonBusiness{

	@Resource
	private ItemButtonService itemButtonService;
	
	@Resource
	private ManageAreaItemService manageAreaItemService;
	
	@Resource
	private DeveloperBusiness developerBusiness;

	@Override
	public ItemButton persist(ItemButton itemButton) {
		ItemButton exist = null;
		
		if(null != itemButton ){
			//1.resolve manageAreaItem
			ManageAreaItem manageAreaItem = itemButton.getManageAreaItem();
			if(null != manageAreaItem){
				Long manageAreaItemId = manageAreaItem.getId();
				if(null != manageAreaItemId && manageAreaItemId > 0 ){
					manageAreaItem = manageAreaItemService.findOne(manageAreaItemId);
					itemButton.setManageAreaItem(manageAreaItem);
				}
			}
			
			
			//2. resolve developer
			Developer developer = null;
			// check developer, if developer is null, set default developer
			Long developerId = null;
			developerId = itemButton.getDeveloperId();
			if(null != developerId ){
				developer = developerBusiness.findById(developerId);
			}
			if(null == developer){
				developer = developerBusiness.findByUserId(KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID_LONG);
				developerId = developer.getId();
			}
			
			//3. update the values
			if (null != developer) {
				Long id = itemButton.getId();
				if(null != id){
					exist = itemButtonService.findOne(id);
				}
				if (null != exist) {
					exist.setCode(itemButton.getCode());
					exist.setName(itemButton.getName());
					exist.setDeveloperId(developerId);
					exist.setIconPath(itemButton.getIconPath());
					exist.setUrl(itemButton.getUrl());
					exist.setOwnerId(developer.getUserId().getUserId());
					exist.setSequence(itemButton.getSequence());
					exist.setDisplay(itemButton.getDisplay());
					exist.setDisable(itemButton.isDisable());
				} else {
					exist = itemButton;
					exist.setDisable(true);// 设置默认显示
					exist.setHideable(false);// 设置默认不隐藏
					exist.setDisable(false);// 设置不禁用
					exist.setDeleted(false);// 设置软删除为false
					// itemButton.setIdKey(idKey); TODO
					exist.setOwnerId(developer.getUserId().getUserId());// 设置所有者，userId
					exist.setDeveloperId(developerId);// 设置开发者
				}
				
				exist.setManageAreaItem(manageAreaItem);
				
				exist = itemButtonService.save(exist);
			}
		}
		return itemButton;
	}

	@Transactional
	@Override
	public List<ItemButton> persistList(List<ItemButton> itemButtonList){
		if(null != itemButtonList && itemButtonList.size()>0){
			Map<Long, ItemButton> map = new HashMap<Long,ItemButton>();
			List<Long> idList = new ArrayList<Long>(itemButtonList.size());
			for(ItemButton itemButton : itemButtonList){
				if(null != itemButton){
					Long id = itemButton.getId();
					if(null != id && id>0){
						map.put(id, itemButton);
						idList.add(id);
					}
				}
			}
			
			if(idList.size()>0){
				List<ItemButton> existList = itemButtonService.findByIdList(idList);
				if(null != existList && existList.size()>0){
					for(ItemButton exist : existList){
						ItemButton source = map.get(exist.getId());
						exist.setSequence(source.getSequence());
						exist.setDisable(source.isDisable());
						exist.setDisplay(source.getDisplay());
						exist.setDeleted(source.isDeleted());
					}
					itemButtonService.save(existList);
				}
			}
		}
		
		return null;
	}

	@Override
	public List<ItemButton> findAll() {
		List<ItemButton> all = itemButtonService.findAll();
		return all;
	}
	
	@Override
	public List<ItemButton> findAllAvailable(){
		List<ItemButton> allAvailable = itemButtonService.findAllAvailable();
		return allAvailable;
	}

	@Override
	public List<ItemButton> findAllDisplay() {
		List<ItemButton> allDisplay = itemButtonService.findAllDisplay();
		return allDisplay;
	}

	@Override
	public Page<ItemButton> findAll(Pageable pageable) {
		return itemButtonService.findAll(pageable);
	}

	@Override
	public ItemButton findById(Long id) {
		ItemButton itemButton = itemButtonService.findOne(id);
		return itemButton;
	}

	@Override
	public void removeById(Long id) {
		itemButtonService.delete(id);
	}
}
