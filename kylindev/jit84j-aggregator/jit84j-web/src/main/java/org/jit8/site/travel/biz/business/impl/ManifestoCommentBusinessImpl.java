package org.jit8.site.travel.biz.business.impl;

import java.util.Date;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.ManifestoCommentBusiness;
import org.jit8.site.travel.biz.service.ManifestoCommentService;
import org.jit8.site.travel.biz.service.ManifestoService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ManifestoCommentBusiness")
public class ManifestoCommentBusinessImpl implements ManifestoCommentBusiness{

	@Resource
	private ManifestoCommentService manifestoCommentService;
	
	@Resource
	private ManifestoService manifestoService;

	@Transactional
	@Override
	public ManifestoComment persist(ManifestoComment manifestoComment, User user) {
		
		if(null != manifestoComment && null != user){
			
			Long id = manifestoComment.getId();
			ManifestoComment exist = null;
			if(id != null && id>0){
				 //如果是已存在的评论
				 exist = findOneByIdAndUserId(manifestoComment.getId(),user.getUserId().getUserId());
				 exist.setComment(manifestoComment.getComment());
				 exist.setSequence(manifestoComment.getSequence());
			}else{
				
				exist = manifestoComment;
				exist.setUser(user);
				Long userId = user.getUserId().getUserId();
				exist.setUserId(userId);
				exist.setNickName(user.getNickname());
				exist.setPraiseCount(1);
				exist.setOwnerId(userId);
				
			}
			
			//针对某一宣言的评论
			Manifesto manifesto = manifestoComment.getManifesto();
			if(null != manifesto){
				Long manifestoId= manifesto.getId();
				manifesto = manifestoService.findOne(manifestoId);
				int commentCount = manifesto.getCommentCount()==null?0:manifesto.getCommentCount();
				manifesto.setCommentCount(commentCount+1);
				exist.setManifesto(manifesto);
			}
			
			
			exist.setUpdateDate(new Date());
			exist.setPublishDate(new Date());
			//是否是回复评论
			ManifestoComment parent = exist.getParent();
			if(null != parent){
				Long parentId = parent.getId();
				ManifestoComment existParent = manifestoCommentService.findOne(parentId);
				if(null != existParent){
					exist.setParent(existParent);
				}
			}
			
			manifestoComment = manifestoCommentService.save(exist);
		}
		return manifestoComment;
	}
	
	@Transactional
	@Override
	public Page<ManifestoComment> persistWithReturnPage(ManifestoComment manifestoComment, User user,Pageable pageable) {
		
		Page<ManifestoComment> page = null;
		if(null != manifestoComment && null != user){
			Manifesto manifesto = manifestoComment.getManifesto();
			if(null != manifesto){
				Long manifestoId= manifesto.getId();
				persist(manifestoComment,user);
				page = manifestoCommentService.findByManifestoId(manifestoId,pageable);
			}
		}
		return page;
	}

	@Override
	public Page<ManifestoComment> findByManifestoId(Long manifestoId, Pageable pageable) {
		Page<ManifestoComment> page = manifestoCommentService.findByManifestoId(manifestoId,pageable);
		return page;
	}

	@Override
	public ManifestoComment findOneByIdAndUserId(Long id, Long userId) {
		return manifestoCommentService.findOneByIdAndOwnerId(id, userId);
	}

}
