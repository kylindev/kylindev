package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.MemberInfo;

public interface MemberInfoDao extends GenericDao<MemberInfo, Long> {

	public List<MemberInfo> getMemberInfoList(boolean disable, boolean deleted) ;
	
	public MemberInfo findByCode(String code);
	
	public List<MemberInfo> findByIds(List<Long> idList);
}
