package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.web.controller.command.ConstantModelCommand;
import org.jit8.user.business.userinfo.ConstantModelBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ConstantModelController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(ConstantModelController.class);
	

	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private ConstantModelBusiness constantModelBusiness;
	
	
	@Resource
	private Mapper mapper;

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_MAPPING )
	public String constantModelViewListUI(@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING +" #########");
		//model.addAttribute("ownRelationList", ownRelationList);
		constantModel.setCurrentUser(getCurrentUser());
		Page<ConstantModel> page = constantModelBusiness.getConstantModelList(constantModel,pageable);
		
		model.addAttribute("page", page);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("constantModel",constantModel);
		
		return "urlImport2dbUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING )
	public String constantModelListUI(@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING +" #########");
		model.addAttribute("ownRelationList", ownRelationList);
		constantModel.setCurrentUser(getCurrentUser());
		Page<ConstantModel> page = constantModelBusiness.getConstantModelList(constantModel,pageable);
		
		model.addAttribute("page", page);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("constantModel",constantModel);
		
		return "constantModelListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING+"2" )
	@ResponseBody
	public Map<String,Object> constantModelListUI2(@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		
		Map<String,Object> map = new HashMap<String,Object>();
		Pageable pageable2 = new PageRequest(0, 2);
		Page<ConstantModel> page = constantModelBusiness.getConstantModelList(pageable2);
		List<ConstantModel> data = page.getContent();
		List<ConstantModelCommand> data2 = new ArrayList<ConstantModelCommand>();
		for(ConstantModel d : data){
			data2.add(mapper.map(d, ConstantModelCommand.class));
		}
		map.put("total", page.getTotalElements());
		map.put("rows", data2);
		return map;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING+"3" )
	@ResponseBody
	public String constantModelListUI3(@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		
		
		String data = "{\"total\":\"3\",\"rows\":[{\"id\":\"81278\",\"firstname\":\"432\",\"lastname\":\"fgfgdfg\",\"phone\":\"324\",\"email\":\"3243@qq.com\"},{\"id\":\"81280\",\"firstname\":\"sdf\",\"lastname\":\"sdf\",\"phone\":\"\",\"email\":\"\"},{\"id\":\"81281\",\"firstname\":\"sdfs\",\"lastname\":\"sdfsdf\",\"phone\":\"\",\"email\":\"\"}]}";
		return data;
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_MAPPING )
	@ResponseBody
	public ConstantModel addConstantModel(@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		
		try {
			constantModel = constantModelBusiness.persist(constantModel);
		} catch (BusinessException e) {
			logger.error("Add ConstantModel failure",e);
		}
		
		return constantModel;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_MAPPING )
	@ResponseBody
	public ConstantModel updateConstantModel(@ModelAttribute("constantModel") ConstantModelCommand constantModelCommand, @PageableDefaults(pageNumber = 0, value = 2) Pageable pageable,Model model) throws IOException {
		
		ConstantModel constantModel = new ConstantModel();
		constantModel.setClazzFullName(constantModelCommand.getClazzFullName());
		constantModel.setId(constantModelCommand.getId());
		constantModel.setTitle(constantModelCommand.getTitle());
		constantModel.setDescription(constantModelCommand.getDescription());
		constantModel = constantModelBusiness.merge(constantModel);
		mapper.map(constantModel, constantModelCommand);
		return constantModel;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_MAPPING )
	@ResponseBody
	public Map<String,Object> removeConstantModel(@ModelAttribute("constantModel") ConstantModelCommand constantModelCommand, @PageableDefaults(pageNumber = 0, value = 2) Pageable pageable,Model model) throws IOException {
		
		 constantModelBusiness.removeById(constantModelCommand.getId());
		 Map<String,Object> map = new HashMap<String,Object>();
		map.put("message", "success");
		return map;
		
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_MAPPING + "/{index}")
	public String showFormConstantModel(@PathVariable int index,@ModelAttribute("constantModel") ConstantModel constantModel, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		
		//constantModel = constantModelBusiness.persist(constantModel);
		
		map.put("message", "success");
		model.addAttribute("index",index);
		
		return "addConstantModelUI";
	}
	
	
	
	
	
}
