package org.jit8.user.business.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface PermissionBusiness {

	public Permission findPermissionByResourceUrl(String resourceUrl);
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls);
	
	public Permission findPermissionByCode(String code);
	
	public Permission persist(Permission permission)throws BusinessException ;
	
	public Page<Permission> getPermissionList(Pageable pageable);
	
	public List<Permission> savePermissionList(Permission role);
	
	public Permission fineById(Long id);
	
	public List<Permission> fineByIdList(List<Long> idList);
	
	public Page<Permission> getPermissionList(Permission permission, Pageable pageable);
	
	public Permission updatePermissionStatus(Permission permission)  ;
	
	public boolean checkPermissionIsUsed(Permission permission);
	
	public boolean checkPermissionIsUsed(Long permissionId);
	
	public List<Permission> findAll(String roleCode);
	
}
