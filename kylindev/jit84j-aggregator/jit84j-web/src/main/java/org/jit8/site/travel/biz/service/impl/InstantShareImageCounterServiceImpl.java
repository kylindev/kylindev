package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.InstantShareImageCounterService;
import org.jit8.site.travel.persist.dao.InstantShareImageCounterDao;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.springframework.stereotype.Service;

@Service
public class InstantShareImageCounterServiceImpl extends GenericServiceImpl<InstantShareImageCounter, Long> implements InstantShareImageCounterService {

	@Resource
	private InstantShareImageCounterDao instantShareImageCounterDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = instantShareImageCounterDao;
	}


	@Override
	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId) {
		return instantShareImageCounterDao.findByCountKeyAndUserId( day, userId);
	}


	@Override
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId) {
		return instantShareImageCounterDao.findByIdAndUserId(id, userId);
	}


	@Override
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId) {
		return instantShareImageCounterDao.findByYearMonthAndUserId(yearMonth, userId);
	}


	
}
