package org.jit8.site.web.controller.command;

import java.io.Serializable;

import javax.persistence.Transient;

public class FileInfoCommand implements Serializable {



	private static final long serialVersionUID = 2681979660557315133L;
	private String fileName;
	private String originalFileName;
	private String fileAccessPath;
	private String fileRealPath;
	private String fileSuffix;
	private String contentType;
	private String fileAccessFullPath;
	private long size;
	private Long owner;//文件所有者，owner为用户uniqueIdentifier	
	private String name;
	private String url;
	private String thumbnailUrl;
	private String success;
	private String error;
	
	private String deleteUrl;
	private String deleteType;
	
	private long batch;//批次
	
	private String title;//文件标题
	private String description;//文件描述
	
	@Transient
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getOriginalFileName() {
		return originalFileName;
	}
	public void setOriginalFileName(String originalFileName) {
		this.originalFileName = originalFileName;
	}
	public String getFileAccessPath() {
		return fileAccessPath;
	}
	public void setFileAccessPath(String fileAccessPath) {
		this.fileAccessPath = fileAccessPath;
	}
	public String getFileRealPath() {
		return fileRealPath;
	}
	public void setFileRealPath(String fileRealPath) {
		this.fileRealPath = fileRealPath;
	}
	public String getFileSuffix() {
		return fileSuffix;
	}
	public void setFileSuffix(String fileSuffix) {
		this.fileSuffix = fileSuffix;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getFileAccessFullPath() {
		return fileAccessFullPath;
	}
	public void setFileAccessFullPath(String fileAccessFullPath) {
		this.fileAccessFullPath = fileAccessFullPath;
	}
	public long getSize() {
		return size;
	}
	public void setSize(long size) {
		this.size = size;
	}
	public Long getOwner() {
		return owner;
	}
	public void setOwner(Long owner) {
		this.owner = owner;
	}
	public String getName() {
		return this.name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getError() {
		return error;
	}
	public void setError(String error) {
		this.error = error;
	}
	public String getUrl() {
		return this.url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public long getBatch() {
		return batch;
	}
	public void setBatch(long batch) {
		this.batch = batch;
	}
	public String getDeleteUrl() {
		return deleteUrl;
	}
	public void setDeleteUrl(String deleteUrl) {
		this.deleteUrl = deleteUrl;
	}
	public String getDeleteType() {
		return deleteType;
	}
	public void setDeleteType(String deleteType) {
		this.deleteType = deleteType;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
}
