package org.jit8.site.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.user.business.userinfo.PermissionCategoryBusiness;
import org.jit8.user.business.userinfo.RoleBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.business.userinfo.UserRoleBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RoleController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(RoleController.class);
	

	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private RoleBusiness roleBusiness;

	@Resource
	private UserRoleBusiness userRoleBusiness;
	
	@Resource
	private PermissionCategoryBusiness permissionCategoryBusiness;
	
	
	
	@RequestMapping({KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_MAPPING + "/{code}"})
	public String roleListUI(@PathVariable("code") String code,@PageableDefaults(pageNumber = 1, value = 15) Pageable pageable, Model model) {
		logger.debug("######### /admin/userinfo/role/roleListUI #########");
		return roleListUIPage(code,pageable.getPageNumber(),pageable.getPageSize(),model);
	}
	
	@RequestMapping({KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_MAPPING + "/{code}/{pageNumber}_{pageSize}"})
	public String roleListUIPage(@PathVariable("code") String code,@PathVariable("pageNumber") int pageNumber, @PathVariable("pageSize") int pageSize, Model model) {
		logger.debug("######### /admin/userinfo/role/roleListUI #########");
		if(pageSize<=0){
			pageSize = 10;
		}
		PageRequest pageable = new PageRequest(pageNumber,pageSize);
		Page<Role> page =roleBusiness.getRoleList(pageable);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("page", page);
		return "roleListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_MAPPING + "/{code}")
	public String roleListModify(@PathVariable("code") String code,@ModelAttribute("role") Role role, @PageableDefaults(pageNumber = 1, value = 15) Pageable pageable,Model model) {
		logger.debug("######### /admin/userinfo/role/roleListUI #########");
		
		
		roleBusiness.saveRoleList(role);
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Page<Role> page =roleBusiness.getRoleList(pageable);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("page", page);
		return "roleListUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_MAPPING + "/{code}")
	public String addRoleUI(@PathVariable("code") String code,@ModelAttribute("user") User user, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/role/addRoleUI/ #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		int roleLevel = 0;
		model.addAttribute("roleList",roleBusiness.getRoleListByRoleLevel(roleLevel));
		model.addAttribute("role",new Role());
		
		return "addRoleUI";
	}
	
	@RequestMapping("/admin/userinfo/role/addRole/{code}")
	public String addRole(@PathVariable("code") String code,@ModelAttribute("role") Role role, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/role/addRole/ #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		
		roleBusiness.persist(role);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		return "redirect:/admin/userinfo/role/roleListUI/" + code;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_MAPPING)
	public String asignPermissionForRole(@ModelAttribute("role") Role role, Model model) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_MAPPING +" #########");
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Pageable pageable2 = new PageRequest(0, 200);
		
		Page<PmnCategory> page =permissionCategoryBusiness.getPermissionCategoryList(pageable2);
		model.addAttribute("page", page);
		int roleLevel = 0;
		//model.addAttribute("roleList",roleBusiness.getRoleListByRoleLevel(roleLevel));
		model.addAttribute("role",new Role());
		
		return "asignPermissionForRoleUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING)
	public String selectedRoleUI(@ModelAttribute("role") Role role, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		//获取用户列表：这里的用户列表应满足，只能获取<=当前登录用户所属最高角色级别的那些用户（TODO），现在暂用获取所有用户代替
		Order order = new Order(Direction.DESC,"roleOrder");
		Pageable pageable2 = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), new Sort(order));
		model.addAttribute("rolePage",roleBusiness.getRoleList(role,pageable2));
		
		return "selectedRoleUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_MAPPING)
	@ResponseBody
	public String addPermissionForRole(@ModelAttribute("role") Role role, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION;
		roleBusiness.asignPermissionForRole(role);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.asignPermissionForUser.success", null);
		return success;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_MAPPING)
	@ResponseBody
	public String removePermissionForUser(@ModelAttribute("role") Role role, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION;
		roleBusiness.removePermissionForRole(role);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.removePermissionForUser.success", null);
		return success;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_MAPPING)
	@ResponseBody
	public String removeAllPermissionForRole(@ModelAttribute("role") Role role, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION;
		roleBusiness.removeAllPermissionForRole(role);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.removeAllPermissionForUser.success", null);
		return success;
	}
	
	
}
