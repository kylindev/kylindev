package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface ActivityBusiness {

	public List<Activity> getActivityList();
	
	public Activity persist(Activity activity);
	
	public Activity getActivityById(Long id);
	
	public Activity deleteActivityById(Long id);
	
	public Page<Activity> findAll(Pageable pageable);
	
	public Activity findByCode(String code);
	
	public void removeById(Long id);
	
	public List<Activity> findByIds(List<Long> idList);
	
	public Page<Activity> findByUserId(Long userId, Pageable pageable);
}
