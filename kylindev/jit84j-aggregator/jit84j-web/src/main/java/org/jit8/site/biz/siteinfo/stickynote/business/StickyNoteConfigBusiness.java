package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;



public interface StickyNoteConfigBusiness {

	public List<StickyNoteConfig> getStickyNoteConfigList();
	public StickyNoteConfig persist(StickyNoteConfig siteInfo);
	
	public StickyNoteConfig getStickyNoteConfigById(Long id);
	
	public StickyNoteConfig deleteStickyNoteConfigById(Long id);
	
	public StickyNoteConfig merge(StickyNoteConfig stickyNoteConfig);
	
}
