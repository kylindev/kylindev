package org.jit8.user.business.impl.userinfo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.user.business.userinfo.DirectPermissionBusiness;
import org.jit8.user.persist.domain.userinfo.DirectPermission;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.service.userinfo.DirectPermissionService;
import org.jit8.user.service.userinfo.PermissionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("directPermissionBusiness")
public class DirectPermissionBusinessImpl implements DirectPermissionBusiness {

	@Resource
	private DirectPermissionService directPermissionService;

	@Resource
	private PermissionService permissionService;
	
	
	@Override
	@Transactional(readOnly=true)
	public List<DirectPermission> findAll() {
		return directPermissionService.findAll();
	}
	
	/**
	 * 获取直接访问的权限集合，由两部分组成，一个直接权限可以包含多个系统定义权限（Permission对象列表）,
	 * 又可以由用户自行定义的权限
	 * 
	 */
	
	@Transactional(readOnly=true)
	public Set<String> getDirectPermissionSet(){
		List<DirectPermission> directPermissionList = directPermissionService.findAll();
		Set<String> directPermissionSet = new HashSet<String>();
		if(null != directPermissionList && directPermissionList.size()>0){
			for(DirectPermission directPermission : directPermissionList){
				List<Permission> permissionList = directPermission.getPermissionList();
				if(null != permissionList && permissionList.size()>0){
					for(Permission permission : permissionList){
						directPermissionSet.add(permission.getFullUrl());
					}
				}
				String directUrl = directPermission.getDirectUrl();
				if(StringUtils.isNotEmpty(directUrl)){
					directPermissionSet.add(directUrl);
				}
			}
		}
		return directPermissionSet;
	}
	
	public DirectPermission persist(DirectPermission directPermission){
		
		Long directPermissionId = directPermission.getId();
		
		DirectPermission current = null;
		if(null == directPermissionId || directPermissionId<=0){
			current = directPermissionService.findByType(DirectPermission.TYPE_DEFAULT);
			if(null == current){
				current = new DirectPermission();
				current.setType(DirectPermission.TYPE_DEFAULT);
			}
		}else{
			current = directPermissionService.findOne(directPermissionId);
		}
		
		
		String directUrl = directPermission.getDirectUrl();
		String directUrlName = directPermission.getDirectUrlName();
		if(StringUtils.isNotEmpty(directUrl)){
			current.setDirectUrl(directUrl);
		}
		if(StringUtils.isNotEmpty(directUrlName)){
			current.setDirectUrlName(directUrlName);
		}
		current.setDisable(false);
		current = directPermissionService.save(current);
		
		
		
		Long permissionId = directPermission.getPermissionId();
		Permission currentPermission = null;
		if(null != permissionId && permissionId>0){
			currentPermission = permissionService.findOne(permissionId);
		}
		
		List<Permission> permissionList = current.getPermissionList();
		if(null != currentPermission ){
			if(null == permissionList || !permissionList.contains(currentPermission)){
				currentPermission.setDirectPermission(current);
			}
			permissionService.save(currentPermission);
			if(null != permissionList && !permissionList.contains(currentPermission)){
				permissionList.add(currentPermission);
			}
		}
		return current;
	}
	
}
