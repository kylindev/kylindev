package org.jit8.site.travel.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.InstantShareImageCounterBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InstantShareImageCounterController{

	private static final Logger logger = LoggerFactory.getLogger(InstantShareImageCounterController.class);
	
	
	@Resource
	private InstantShareImageCounterBusiness instantShareImageCounterBusiness;
	
	@Resource
	private Mapper mapper;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_MAPPING)
	public String nstantShareImageCounterByDailyUI(@ModelAttribute("instantShareImageCounter") InstantShareImageCounter instantShareImageCounter,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		String yearMonth = instantShareImageCounter.getYearMonth();
		List<InstantShareImageCounter> instantShareImageCounterList = null;
		if(StringUtils.isNotEmpty(yearMonth) && null != userId){
			instantShareImageCounterList = instantShareImageCounterBusiness.findByMonthForUser(yearMonth, userId);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShareImageCounterList", instantShareImageCounterList);
		return "instantShareImageCounterListUI";
	}
	
}
