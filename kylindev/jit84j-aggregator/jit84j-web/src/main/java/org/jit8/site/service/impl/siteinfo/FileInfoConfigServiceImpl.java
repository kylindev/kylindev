package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileInfoConfigDao;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;
import org.jit8.site.service.siteinfo.FileInfoConfigService;
import org.springframework.stereotype.Service;

@Service
public class FileInfoConfigServiceImpl extends GenericServiceImpl<FileInfoConfig, Long> implements FileInfoConfigService {

	@Resource
	private FileInfoConfigDao fileInfoConfigDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileInfoConfigDao;
	}


	@Override
	public List<FileInfoConfig> getFileInfoConfigList(boolean disable, boolean deleted) {
		return fileInfoConfigDao.getFileInfoConfigList(disable, deleted);
	}
	
}
