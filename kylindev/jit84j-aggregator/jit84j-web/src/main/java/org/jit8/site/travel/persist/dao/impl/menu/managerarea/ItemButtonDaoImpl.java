package org.jit8.site.travel.persist.dao.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.menu.managerarea.ItemButtonDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.jit8.site.travel.persist.repository.menu.managerarea.ItemButtonRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ItemButtonDaoImpl extends GenericDaoImpl<ItemButton, Long> implements ItemButtonDao {

	@Resource
	private ItemButtonRepository itemButtonRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = itemButtonRepository;
	}

	@Override
	public List<ItemButton> findAllAvailable() {
		return itemButtonRepository.findAllAvailable();
	}

	@Override
	public List<ItemButton> findAllDisplay() {
		return itemButtonRepository.findAllDisplay();
	}

	@Override
	public List<ItemButton> findByIdList(List<Long> idList) {
		List<ItemButton> itemButtonList = itemButtonRepository.findByIdList(idList);
		return itemButtonList;
	}
}
