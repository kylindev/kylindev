package org.jit8.site.travel.biz.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.ManifestoService;
import org.jit8.site.travel.persist.dao.ManifestoCommentDao;
import org.jit8.site.travel.persist.dao.ManifestoDao;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ManifestoServiceImpl extends GenericServiceImpl<Manifesto, Long> implements ManifestoService {

	@Resource
	private ManifestoDao manifestoDao;
	
	@Resource
	private ManifestoCommentDao manifestoCommentDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = manifestoDao;
	}


	@Override
	public Page<Manifesto> getList(Date begin, Date end, Pageable pageable) {
		Page<Manifesto> page = manifestoDao.getList( begin,  end,  pageable);
		return page;
	}


	@Override
	public List<Manifesto> findManifestoByPublishDateAndUserId(Date begin, Date end, Long userId) {
		return manifestoDao.findManifestoByPublishDateAndUserId( begin,  end,  userId);
	}


	@Override
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable, Pageable commontPageable) {
		Page<Manifesto> page = manifestoDao.getTodayList(manifesto, pageable);
		if(null != page){
			List<Manifesto> list = page.getContent();
			long currentTime = System.currentTimeMillis();
			for(Manifesto m : list){
				if(m != null){
					long minus = currentTime - m.getPublishDate().getTime();
					m.setPublishTimeMillis(minus);
					m.setPublishSeconds(minus/1000);
					
					
					long manifestoId = m.getId();
					Page<ManifestoComment> commontPage = manifestoCommentDao.findByManifestoId(manifestoId, pageable);
					if(null != commontPage){
						m.setCommentList(commontPage.getContent());
					}
				}
			}
		}
		return page;
	}
	
	@Override
	public Manifesto findOneWithPageable(Long id, Pageable commentPageable) {
		
		Manifesto manifesto = manifestoDao.findOne(id);
		
		Page<ManifestoComment> commentPage = manifestoCommentDao.findByManifestoId(id, commentPageable);
		if(null != commentPage){
			manifesto.setCommentPage(commentPage);
		}
		
		return manifesto;
	}
	
	@Override
	public Long findPreviousOneByCurrentId(Long id) {
		return manifestoDao.findPreviousOneByCurrentId(id);
	}

	@Override
	public Long findFollowingOneByCurrentId(Long id) {
		return manifestoDao.findFollowingOneByCurrentId(id);
	}


	@Override
	public Manifesto findSimpleOneById(Long id) {
		return manifestoDao.findSimpleOneById(id);
	}


	@Override
	public Long findLastId() {
		return manifestoDao.findLastId();
	}

	
}
