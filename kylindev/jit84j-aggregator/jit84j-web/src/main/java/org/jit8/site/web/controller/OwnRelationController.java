package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.validation.Valid;

import org.jit8.site.web.admin.validator.OwnRelationValidator;
import org.jit8.user.business.userinfo.OwnRelationBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class OwnRelationController {

private static final Logger logger = LoggerFactory.getLogger(OwnRelationController.class);
	
	@Resource
	private OwnRelationBusiness ownRelationBusiness;
	
	@Resource
	private OwnRelationValidator ownRelationValidator;
	
	@InitBinder  
    public void initBinder(DataBinder binder) {  
       binder.setValidator(ownRelationValidator);  
    }
	
	
	
	public OwnRelationController(){
		logger.debug("######### init OwnRelationController #########");
	}
	
	

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_MAPPING)
	public String listOwnRelation( Model model) throws IOException {
		logger.debug("######### /listOwnRelation #########");
		List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("ownRelationList", ownRelationList);
		return "listOwnRelation";
	}
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_DROPDOWN_MAPPING)
	public String listOwnRelationDropdown( Model model) throws IOException {
		logger.debug("######### " +  KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_DROPDOWN_MAPPING + " #########");
		List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("ownRelationList", ownRelationList);
		return "ownRelationDropdown";
	}
	
	@RequestMapping("/modifyOwnRelationUI")
	public String modifyOwnRelationUI(@ModelAttribute("ownRelation") OwnRelation ownRelation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /modifyOwnRelationUI #########");
		return "modifyOwnRelation";
	}
	
	@RequestMapping("/modifyOwnRelation")
	public String modifyOwnRelation(@ModelAttribute("ownRelation") @Valid OwnRelation ownRelation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /modifyOwnRelation #########");
		ownRelation = ownRelationBusiness.persist(ownRelation);
		if(result.hasErrors()){
			return "modifyOwnRelation";
		}
		return "redirect:/listOwnRelation";
	}
	
	@RequestMapping("/modifyOwnRelationList")
	public String modifyOwnRelationList(@ModelAttribute("ownRelationList") @Valid OwnRelation ownRelation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /modifyOwnRelationList #########");
		List<OwnRelation> ownRelationList =  ownRelationBusiness.persist(ownRelation.getOwnRelationList());
		if(result.hasErrors()){
			return "listOwnRelation";
		}
		return "redirect:/listOwnRelation";
	}

}
