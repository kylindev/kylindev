package org.jit8.user.business.userinfo;

import org.jit8.user.persist.domain.userinfo.EmailUrl;



public interface EmailUrlBusiness {

	public EmailUrl findByUrlCode(String urlCode);
	
	public EmailUrl persist(EmailUrl emailUrl);
}
