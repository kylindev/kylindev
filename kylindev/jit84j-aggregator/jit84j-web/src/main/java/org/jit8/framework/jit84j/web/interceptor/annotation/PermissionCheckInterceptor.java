package org.jit8.framework.jit84j.web.interceptor.annotation;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.developer.common.constants.CommonDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class PermissionCheckInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(PermissionCheckInterceptor.class);
	private static final String URL_SEP = CommonDeveloperConstant._COMMON_USER_SEPERATOR_U + CommonDeveloperConstant._COMMON_USER_SEPERATOR + "[0-9]{5,10}" +  CommonDeveloperConstant._COMMON_USER_SEPERATOR + CommonDeveloperConstant._COMMON_USER_SEPERATOR_U ;
	private static final String URL_REX = "([0-9a-zA-Z/-_]*" + URL_SEP + ").*";
	
	
	@Resource
	private PermissionBusiness permissionBusiness;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefine;
	
	private Set<String> directPermissionSet ;
	
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		Subject currentUser = SecurityUtils.getSubject();  
		if(WebConstants.frontPermissionEnable){
			if(handler instanceof HandlerMethod){
				String requestURI = request.getRequestURI();
				Pattern p = Pattern.compile(URL_REX);
				Matcher m = p.matcher(requestURI);
				boolean hasPermission = false;
				if(m.find()){
					try{
						String authURI = m.group(1);
						if(!getDirectPermissionSet().contains(authURI)){
							//currentUser.checkPermission(authURI);
						}
						hasPermission = true;
	            		logger.info("user: [" + currentUser.getPrincipal() +"] has authorize:" + authURI);
	            	}catch(AuthorizationException au){
	            		logger.warn("no authorize: " + requestURI, au);
	            	}
				}else{
					if(directPermissionSet.contains(requestURI)){
						hasPermission = true;
					}
				}
				
				if(!hasPermission){
					logger.error("user: [" + currentUser.getPrincipal() +"] no authorize:" + requestURI);
					response.sendRedirect("/index");
	            	return false;
				}
				
				
			
	        /*if(null != currentUser){  
	            Session session = currentUser.getSession();
	            ThreadLocalShareInfo shareInfo = (ThreadLocalShareInfo)session.getAttribute(WebConstants._SHAREINFO);
	            
	            
	            String authType = request.getAuthType();
	            String characterEncoding = request.getCharacterEncoding();
	            int contentLength = request.getContentLength();
	            String contentType = request.getContentType();
	            String contextPath = request.getContextPath();
	            String localAddr = request.getLocalAddr();
	            String localName = request.getLocalName();
	            Locale locale = request.getLocale();
	            int localPort = request.getLocalPort();
	            String method = request.getMethod();
	            String pathInfo = request.getPathInfo();
	            String pathTranslated = request.getPathTranslated();
	            String protocol = request.getProtocol();
	            String queryString = request.getQueryString();
	            String remoteAddr = request.getRemoteAddr();
	            String remoteHost = request.getRemoteHost();
	            int remotePort = request.getRemotePort();
	            String remoteUser = request.getRemoteUser();
	            String sessionId = request.getRequestedSessionId();
	            String requestURI = request.getRequestURI();
	            String requestURL = request.getRequestURL().toString();
	            String scheme = request.getScheme();
	            String serverName = request.getServerName();
	            int serverPort = request.getServerPort();
	            String servletPath = request.getServletPath();
	            Map parameterMap = request.getParameterMap();
	            request.getAttributeNames();
	            
	            String userAgent = request.getHeader("User-Agent");
	            
	            ApplicationContext.getContext().setShareInfo(shareInfo);
	            
	            
	            //check ip
	            //check uri
	            Permission permission = permissionBusiness.findPermissionByResourceUrl(requestURI);
	            if(permission!=null){
	            	if(permission.isDisable()){
	            		response.sendRedirect("/html/403.html");
	            		return false;
	            	}
	            	StringBuilder sb = new StringBuilder();
	            	sb.append(permission.getCode()).append(":").append(permission.getResourceUrl()).append(":").append(permission.getOpration());
	            	currentUser.checkPermission(sb.toString());
	            	//currentUser.
	            }else{
	            	//response.sendRedirect("/index");
	            	//return false;
	            }*/
	            
	            //check user
	            
	        }
		}
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//response.getWriter().print("in login post");
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		//response.getWriter().print("in login after completaion");
		super.afterCompletion(request, response, handler, ex);
		ApplicationContext.getContext().setShareInfo(null);
	}

	public Set<String> getDirectPermissionSet() {
		if(null == directPermissionSet){
			directPermissionSet = (Set<String>)StaticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.DIRECT_PERMISSION_SET,true);//TODO use Memcache
		}
		return directPermissionSet;
	}

	public void setDirectPermissionSet(Set<String> directPermissionSet) {
		this.directPermissionSet = directPermissionSet;
	}
}
