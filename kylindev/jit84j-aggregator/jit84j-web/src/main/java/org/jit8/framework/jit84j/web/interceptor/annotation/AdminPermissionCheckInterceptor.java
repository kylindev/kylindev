package org.jit8.framework.jit84j.web.interceptor.annotation;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.subject.Subject;
import org.jit8.framework.jit84j.core.trasient.model.UrlContainer;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.developer.common.constants.CommonDeveloperConstant;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.google.gson.Gson;

public class AdminPermissionCheckInterceptor extends HandlerInterceptorAdapter {

	private static final Logger logger = LoggerFactory.getLogger(AdminPermissionCheckInterceptor.class);
	private static final String URL_SEP = CommonDeveloperConstant._COMMON_USER_SEPERATOR_U + CommonDeveloperConstant._COMMON_USER_SEPERATOR + "[0-9]{5,10}" +  CommonDeveloperConstant._COMMON_USER_SEPERATOR + CommonDeveloperConstant._COMMON_USER_SEPERATOR_U ;
	private static final String URL_REX_P_N = "(([0-9a-zA-Z/-_]*" + URL_SEP + ")P([0-9]{1,2})N([0-9]{1,2}).*)";
	private static final String URL_REX = "([0-9a-zA-Z/-_]*" + URL_SEP + ").*";
	
	
	private static final String XML_HTTP_REQUEST = "XMLHttpRequest";
	
	@Resource
	private PermissionBusiness permissionBusiness;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefine;
	
	private Set<String> directPermissionSet ;
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//Object object  = staticDataDefine.getData(StaticDataDefineConstant.MEMACHED_TEST, false);
		
		Subject currentUser = SecurityUtils.getSubject();  
		boolean authenticated = currentUser.isAuthenticated();
		if(!authenticated){
			//response.sendRedirect(KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING);
    		return false;
		}
		//TODO,要区分ajax请求
		if(WebConstants.adminPermissionEnable){
			if(handler instanceof HandlerMethod){
				String requestURI = request.getRequestURI();
				
					UrlContainer urlContainer = UrlContainer.getUrlContainerFromUrl(requestURI);
					boolean hasPermission = false;
					
					//step1 先查看是否带参的url
					//Pattern pStep1 = Pattern.compile(URL_REX_P_N);
					//Matcher mStep1 = pStep1.matcher(requestURI);
					String authURI = urlContainer.getPermisssionUrl();
					//String authURISupper = "";
					//boolean hasPermission = false;
					//若带参数，先获取参数个数，再截取参数路径
					/*if(mStep1.find()){
						//authURISupper = mStep1.group(1);//获取第一级，若第一级有权限，则直接通过
						String g2 = mStep1.group(3);
						StringBuilder sb = new StringBuilder("([0-9a-zA-Z/-_]*");
						sb.append(URL_SEP).append("P([0-9]{1,2})N([0-9]{1,2})(~[0-9a-zA-Z-_]+){").append(g2).append("}).*");
					
						//step2
						Pattern pStep2 = Pattern.compile(sb.toString());
						Matcher mStep2 = pStep2.matcher(requestURI);
						if(mStep2.find()){
							authURI =  mStep2.group(1);
						}
						
					}else{
						//若不是带参数的url，按定义取标准url
						//step3
						Pattern pStep3 = Pattern.compile(URL_REX);
						Matcher mStep3 = pStep3.matcher(requestURI);
						if(mStep3.find()){
							authURI = mStep3.group(1);
							logger.info("user: [" + currentUser.getPrincipal() +"] has authorize:" + authURI);
						}
					}*/
					
			 try{
				 	logger.info("user: [" + currentUser.getPrincipal() +"] has authorize:" + authURI);
				 	directPermissionSet = (Set<String>)staticDataDefine.getData(StaticDataDefineConstant.DIRECT_PERMISSION_SET,true);//TODO use Memcache
				 	directPermissionSet.add("/admin/user/o_remove_all_user_permission_ou_1000001_uP2N2~code~name");
				 	if(!directPermissionSet.contains(authURI)){
						currentUser.checkPermission(authURI);//check permission, if 没有带参数的permission,则查找不带参数的通用路径，如果存在，则放行
					}
					hasPermission = true;
				}catch(AuthorizationException au){
            		logger.error("no authorize: " + requestURI, au);
            	}finally{
					if(!hasPermission){
						String referer = request.getHeader("Referer");
						logger.error("user: [" + currentUser.getPrincipal() +"] no authorize:" + requestURI);
						/*if(StringUtils.isEmpty(referer)){
							
							//response.sendRedirect(KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING);
						}
						String noAuthorize = "?no=l";
						if(!(referer.indexOf(noAuthorize)>-1)){
							if(referer.indexOf("?")>-1){
								referer = referer+"&no=l";
							}else{
								referer = referer+noAuthorize;
							}
						}*/
						//response.sendRedirect(referer /*KylinboyDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING*/);
						String requestAsyn = request.getHeader("X-Requested-With");
						response.setCharacterEncoding("utf-8");
						PrintWriter out = response.getWriter();
						
						if(XML_HTTP_REQUEST.equals(requestAsyn)){
							Map<String,Object> result = new HashMap<String,Object>();
							result.put("msg", "没有权限");
							result.put("error", "没有权限");
							result.put("requestAsyn", requestAsyn);
							Gson gson = new Gson(); 
							String json = gson.toJson(result);
							out.println(json);
						}else{
							//TODO 处理中文乱码问题
							out.println("<script type='text/javascript'>alert('no authorizen')</script>");
						}
						
						out.flush();
						out.close();
		            	return false;
					}
            	}
				/*String requestURI = request.getRequestURI();
				String authURI = requestURI;
				int location = requestURI.indexOf(WebConstants._AUTHORIZE_SEPORATOR);
				
				
				if(location>-1){
					authURI = requestURI.substring(0,location);
				}
				Permission permission = permissionBusiness.findPermissionByResourceUrl(authURI);
				if(permission!=null ){
					boolean hasPermission = false;
						if(!permission.isDisable()){
							StringBuilder sb = new StringBuilder();
			            	sb.append(permission.getCode()).append(":").append(permission.getResourceUrl()).append(":").append(permission.getOpration());
			            	try{
			            		currentUser.checkPermission(sb.toString());
			            		hasPermission = true;
			            		logger.info("user: [" + currentUser.getPrincipal() +"] has authorize:" + sb.toString());
			            	}catch(AuthorizationException au){
			            		logger.warn("no authorize: " + sb.toString(), au);
			            	}
						}
					
					if(!hasPermission){
						logger.error("user: [" + currentUser.getPrincipal() +"] no authorize:" + requestURI);
						response.sendRedirect("/index");
		            	return false;
					}
				}
	            }else{
	            	//response.sendRedirect("/html/403.html");
	        		//return false;
	            }*/
			
	        /*if(null != currentUser){  
	            Session session = currentUser.getSession();
	            ThreadLocalShareInfo shareInfo = (ThreadLocalShareInfo)session.getAttribute(WebConstants._SHAREINFO);
	            
	            
	            String authType = request.getAuthType();
	            String characterEncoding = request.getCharacterEncoding();
	            int contentLength = request.getContentLength();
	            String contentType = request.getContentType();
	            String contextPath = request.getContextPath();
	            String localAddr = request.getLocalAddr();
	            String localName = request.getLocalName();
	            Locale locale = request.getLocale();
	            int localPort = request.getLocalPort();
	            String method = request.getMethod();
	            String pathInfo = request.getPathInfo();
	            String pathTranslated = request.getPathTranslated();
	            String protocol = request.getProtocol();
	            String queryString = request.getQueryString();
	            String remoteAddr = request.getRemoteAddr();
	            String remoteHost = request.getRemoteHost();
	            int remotePort = request.getRemotePort();
	            String remoteUser = request.getRemoteUser();
	            String sessionId = request.getRequestedSessionId();
	            String requestURI = request.getRequestURI();
	            String requestURL = request.getRequestURL().toString();
	            String scheme = request.getScheme();
	            String serverName = request.getServerName();
	            int serverPort = request.getServerPort();
	            String servletPath = request.getServletPath();
	            Map parameterMap = request.getParameterMap();
	            request.getAttributeNames();
	            
	            String userAgent = request.getHeader("User-Agent");
	            
	            ApplicationContext.getContext().setShareInfo(shareInfo);
	            
	            
	            //check ip
	            //check uri
	            Permission permission = permissionBusiness.findPermissionByResourceUrl(requestURI);
	            if(permission!=null){
	            	if(permission.isDisable()){
	            		response.sendRedirect("/html/403.html");
	            		return false;
	            	}
	            	StringBuilder sb = new StringBuilder();
	            	sb.append(permission.getCode()).append(":").append(permission.getResourceUrl()).append(":").append(permission.getOpration());
	            	currentUser.checkPermission(sb.toString());
	            	//currentUser.
	            }else{
	            	//response.sendRedirect("/index");
	            	//return false;
	            }
	            
	            //check user
	             */
	            
	        }
		}
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//response.getWriter().print("in login post");
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		//response.getWriter().print("in login after completaion");
		super.afterCompletion(request, response, handler, ex);
		ApplicationContext.getContext().setShareInfo(null);
	}
}
