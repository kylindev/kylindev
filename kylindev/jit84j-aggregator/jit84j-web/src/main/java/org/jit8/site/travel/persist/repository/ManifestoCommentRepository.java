package org.jit8.site.travel.persist.repository;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ManifestoCommentRepository extends GenericJpaRepository<ManifestoComment, Long>{

	@Query("from ManifestoComment o where o.disable=false and o.deleted=false and o.manifesto.uniqueIdentifier = :manifestoId")
	public Page<ManifestoComment> findByManifestoId(@Param("manifestoId")Long manifestoId, Pageable pageable) ;
}
