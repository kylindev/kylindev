package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.Photograph;


public interface PhotographService extends GenericService<Photograph, Long>{

	public List<Photograph> getPhotographList(boolean disable, boolean deleted);
}
