package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileInfoDao;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.jit8.site.service.siteinfo.FileInfoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FileInfoServiceImpl extends GenericServiceImpl<FileInfo, Long> implements FileInfoService {

	@Resource
	private FileInfoDao fileInfoDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileInfoDao;
	}


	@Override
	public List<FileInfo> getFileInfoList(boolean disable, boolean deleted) {
		return fileInfoDao.getFileInfoList(disable, deleted);
	}


	@Override
	public FileInfo findByIdAndOwner(long id, long owner) {
		return fileInfoDao.findByIdAndOwner(id, owner);
	}


	@Override
	public Page<FileInfo> findAll(FileInfo fileInfo, Pageable pageable) {
		return fileInfoDao.findAll( fileInfo,  pageable);
	}
	
}
