package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ConstantModelService extends GenericService<ConstantModel, Long>{

	public Page<ConstantModel> getConstantModelList(Pageable pageable);
	
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,Pageable pageable);
	
}
