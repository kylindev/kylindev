package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface FileInfoBusiness {

	public List<FileInfo> getFileInfoList();
	
	public Page<FileInfo> findAll(FileInfo fileInfo,Pageable pageable);
	
	public FileInfo persist(FileInfo fileInfo);
	
	public FileInfo getFileInfoById(Long id);
	
	public FileInfo deleteFileInfoById(Long id);
	
	public List<FileInfo> resolveFile(FileInfo fileInfo)  throws BusinessException;
	
	public String getRootStoredPath();

	public String getEmailStoredPath();
	
	public FileInfo deleteFileInfoByIdAndUserId(Long userId,Long id);

}
