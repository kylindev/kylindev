package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.Photograph;



public interface PhotographBusiness {

	public List<Photograph> getPhotographList();
	public Photograph persist(Photograph photograph);
	public Photograph merge(Photograph photograph);
	
	public Photograph getPhotographById(Long id);
	
	public Photograph deletePhotographById(Long id);
	
	//public Photograph getFPhotograph
}
