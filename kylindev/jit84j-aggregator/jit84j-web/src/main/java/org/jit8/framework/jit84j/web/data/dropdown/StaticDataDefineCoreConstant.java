package org.jit8.framework.jit84j.web.data.dropdown;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
/**
 * 定义缓存key
 * @author kylinboy
 *
 */
public abstract class StaticDataDefineCoreConstant extends StaticDataDefineConstant{

	

	private static final long serialVersionUID = 418854607850830360L;

	public StaticDataDefineCoreConstant() {
	}
	
	protected static Map<String,String> keysInMemcached = new HashMap<String,String>();
	
	protected static Map<String,MemachedModel> memachedModelByKey = new ConcurrentHashMap<String,MemachedModel>();
	
	//把每个key添加进set集合
	protected static void addKeyIntoMemcached(String key,String name){
		if(StringUtils.isNotEmpty(key)){
			keysInMemcached.put(key,name);
		}
		
	}
	
	protected static void addMemachedModelByKeyIntoMemcached(String key, MemachedModel memachedModel) {
			memachedModelByKey.put(key, memachedModel);
	}
	public static void setMemachedModelByKey(Map<String,MemachedModel> items){
		memachedModelByKey.putAll(items);
	}
	
	/**
	 * 返回 keys set 集合
	 * @return
	 */
	public static Map<String, String> getKeysInMemcached() {
		return keysInMemcached;
	}

	
	public  abstract void addKeysInMemcached();
	public  abstract void addMemachedModelByKey();
	
	/**
	 * 返回 key MemachedModel 集合
	 * @return
	 */
	public static Map<String, MemachedModel> getMemachedModelByKey() {
		return memachedModelByKey;
	}
	
	public static  MemachedModel getMemachedModelByKey(String key) {
		return memachedModelByKey.get(key);
	}
	
	public synchronized static  MemachedModel setMemachedModelByKey(String key,MemachedModel memachedModel) {
		return memachedModelByKey.put(key, memachedModel);
	}
	
	
	//开发者id
	public static final String DEVELOP_ID = KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID;
	
	/**
	 * 与站长关系列表
	 */
	public static final String OWN_RELATION_LIST = DEVELOP_ID + "Own_Relation_List";
	public static final String OWN_RELATION_LIST_NAME = DEVELOP_ID + "与站长关系列表";
	
	/**
	 * 后台顶级导航列表
	 */
	public static final String NAVIGATION_TOP_LIST = DEVELOP_ID + "navigationTopList";
	public static final String NAVIGATION_TOP_LIST_NAME = DEVELOP_ID + "后台顶级导航列表";
	
	/**
	 * 后台顶级导航列表
	 */
	public static final String NAVIGATION_MAP = DEVELOP_ID + "navigationMap";
	public static final String NAVIGATION_MAP_NAME = DEVELOP_ID + "后台导航Map";
	/**
	 * 前台顶级导航列表
	 */
	public static final String NAVIGATMENU_TOP_LIST = DEVELOP_ID + "navigatMenuTopList";
	public static final String NAVIGATMENU_TOP_LIST_NAME = DEVELOP_ID + "前台顶级导航列表";
	
	/**
	 * 前台顶级导航MAP
	 */
	public static final String NAVIGATMENU_MAP = DEVELOP_ID + "navigatMenuMap";
	public static final String NAVIGATMENU_MAP_NAME = DEVELOP_ID + "前台导航Map";
	
	/**
	 * 导航类型下拉列表
	 */
	public static final String NAVIGATION_TYPE_DROPDOWN = DEVELOP_ID + "navigationTypeDropdown";
	public static final String NAVIGATION_TYPE_DROPDOWN_NAME = DEVELOP_ID + "导航类型下拉列表";
	
	/**
	 * 直接权限集合（白名单）
	 */
	public static final  String DIRECT_PERMISSION_SET = DEVELOP_ID + "directPermissionSet";
	public static final  String DIRECT_PERMISSION_SET_NAME = DEVELOP_ID + "直接权限集合(白名单)";
	
	/**
	 * 图辑列表
	 */
	public static final String GALLERY_LIST = DEVELOP_ID + "galleryList";
	public static final String GALLERY_LIST_NAME = DEVELOP_ID + "图辑列表";
	
	/**
	 * Permission category选择分类下拉列表
	 */
	public static final String COMMON_PERMISSION_CATEGORY_DROPDOWN = DEVELOP_ID + "permission_category_dropdown";
	public static final String COMMON_PERMISSION_CATEGORY_DROPDOWN_NAME = DEVELOP_ID + "选择分类下拉列表";

	public static final String MEMACHED_TEST = DEVELOP_ID + "MEMACHED_TEST";
	public static final String MEMACHED_TEST_NAME = DEVELOP_ID + "缓存测试";

	
	/**
	 * FileSystemGallery系统分类下拉列表
	 */
	public static final String FILE_SYSTEM_GALLERY_DROPDOWN = DEVELOP_ID + "file_system_gallery_dropdown";
	public static final String FILE_SYSTEM_GALLERY_DROPDOWN_NAME = DEVELOP_ID + "文件管理>系统分类下拉列表";
	
	/**
	 * FileSystemGallery系统分类Code下拉列表
	 */
	public static final String FILE_SYSTEM_GALLERY_CODE_DROPDOWN = DEVELOP_ID + "file_system_gallery_code_dropdown";
	public static final String FILE_SYSTEM_GALLERY_CODE_DROPDOWN_NAME = DEVELOP_ID + "文件管理>系统分类代码下拉列表";

	
	/**
	 * FileSystemGallery系统分类allowed file MIME
	 * map的key取自FileSystemGallery预定义系统分类常量
	 */
	public static final String FILE_SYSTEM_GALLERY_MAP_ALLOWED_MIME = DEVELOP_ID + "file_system_gallery_map_allowed_mime";
	public static final String FILE_SYSTEM_GALLERY_MAP_ALLOWED_MIME_NAME = DEVELOP_ID + "文件管理>系统分类可允许上传的类型（Map）";

	
	/**
	 * FileSystemGallery系统分类allowed file MIME后缀
	 * map的key取自FileSystemGallery预定义系统分类常量
	 */
	public static final String FILE_SYSTEM_GALLERY_MAP_ALLOWED_SUFFIX_MIME = DEVELOP_ID + "file_system_gallery_map_allowed_suffix";
	public static final String FILE_SYSTEM_GALLERY_MAP_ALLOWED_SUFFIX_MIME_NAME = DEVELOP_ID + "文件管理>系统分类可允许上传的类型后缀（Map）";
	
	/**
	 * 根据mime+suffix 获取对应的FileSystemGallery 
	 * map的key取自mime+suffix
	 */
	public static final String FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP = DEVELOP_ID + "file_system_gallery_suffix_mime_key_for_map";
	public static final String FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP_NAME = DEVELOP_ID + "文件管理>根据MIME与Suffix获取对应的FileSystemGallery";

	/**
	 * FileSystemGalleryType系统分类类型下拉列表
	 */
	public static final String FILE_SYSTEM_GALLERY_TYPE_DROPDOWN = DEVELOP_ID + "file_system_gallery_type_dropdown";
	public static final String FILE_SYSTEM_GALLERY_TYPE_DROPDOWN_NAME = DEVELOP_ID + "文件管理>系统分类下拉列表";
	
	/**
	 * SiteInfo 主站信息缓存
	 */
	public static final String SITE_INFO_MAIN_SITE = DEVELOP_ID + SiteInfo.MAIN_SITE;
	public static final String SITE_INFO_MAIN_SITE_NAME = DEVELOP_ID + "站点信息【主站】";
	
	/**
	 * 默认邮箱配置
	 */
	public static final String MAIL_CONFIG_DEFAULT = DEVELOP_ID + "mail_config_default";
	public static final String MAIL_CONFIG_DEFAULT_NAME = DEVELOP_ID + "邮箱配置【默认】";

	/**
	 * MailConfigType邮箱配置类型下拉列表
	 */
	public static final String MAIL_CONFIG_TYPE_DROPDOWN = DEVELOP_ID + "mail_config_type_dropdown";
	public static final String MAIL_CONFIG_TYPE_DROPDOWN_NAME = DEVELOP_ID + "邮件配置管理>基本信息>邮箱配置类型下拉列表";
	
	
	/**
	 * SpecialArea 随手贴管理>特殊区下拉列表
	 */
	public static final String SPECIAL_AREA_DROPDOWN = DEVELOP_ID + "special_area_dropdown";
	public static final String SPECIAL_AREA_DROPDOWN_NAME = DEVELOP_ID + "随手贴管理>特殊区下拉列表";

	/**
	 * StickyNoteCategory 随手贴管理>随手贴类别下拉列表STICKYNOTE_CATEGORY_DROPDOWN
	 */
	public static final String STICKYNOTE_CATEGORY_DROPDOWN = DEVELOP_ID + "special_note_gategory_dropdown";
	public static final String STICKYNOTE_CATEGORY_DROPDOWN_NAME = DEVELOP_ID + "随手贴管理>随手贴类别下拉列表";
	

}
