package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.user.business.userinfo.RoleBusiness;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.RolePermission;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.jit8.user.service.userinfo.RolePermissionService;
import org.jit8.user.service.userinfo.RoleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("roleBusiness")
public class RoleBusinessImpl implements RoleBusiness{

	@Resource
	private RoleService roleService;
	
	@Resource
	private RolePermissionService rolePermissionService;

	@Override
	public List<Role> getRoleListByRoleLevel(int roleLevel) {
		return roleService.getRoleListByRoleLevel(roleLevel);
	}
	
	public int getMaxRoleLevel(List<Role> roleLsit){
		int maxRoleLevel = 0;
		for(Role role : roleLsit){
			int roleLevel = role.getRoleLevel();
				if(maxRoleLevel<roleLevel){
					maxRoleLevel = roleLevel;
			}
		}
		return maxRoleLevel;
	}
	
	public int getMaxRoleLevelFromUserRoleList(List<UserRole> roleLsit){
		int maxRoleLevel = 0;
		for(UserRole userRole : roleLsit){
			if(userRole != null){
				Role role = userRole.getRole();
				int roleLevel = role.getRoleLevel();
				if(maxRoleLevel<roleLevel){
					maxRoleLevel = roleLevel;
				}
			}
			
		}
		return maxRoleLevel;
	}

	@Override
	public List<Role> getRoleListByRoleIdList(List<Long> roleIdList) {
		return roleService.getRoleListByRoleIdList(roleIdList);
	}
	
	public Page<Role> getRoleList(Pageable pageable){
		return roleService.getRoleList(pageable);
	}

	@Transactional
	@Override
	public List<Role> saveRoleList(Role role) {
		
		List<Role> persitedRoleList = new ArrayList<Role>();
		if(null != role){
			List<Role> roleList = role.getRoleList();
			if(null != roleList && roleList.size()>0){
				for(Role c : roleList){
					Long id = c.getId();
					if(null != id && id>0){
						Role current = roleService.findOne(id);
						current.setRoleCode(c.getRoleCode());
						current.setRolename(c.getRolename());
						current.setRoleAlias(c.getRoleAlias());
						current.setRoleLevel(c.getRoleLevel());
						current.setRoleOrder(c.getRoleOrder());
						persitedRoleList.add(current);
					}else{
						persitedRoleList.add(c);
					}
					
				}
				persitedRoleList = (List<Role>) roleService.save(persitedRoleList);
			}
		}
		return persitedRoleList;
	}

	@Override
	public Role persist(Role role) {
		return roleService.save(role);
	}

	@Override
	public void asignPermissionForRole(Role role) {
		Long roleId = role.getId();
		if(roleId != null && roleId > 0){
			
			String permissionIds = role.getPermissionIds();
			if(StringUtils.isNotEmpty(permissionIds)){
				Role roleTem = new Role();
				roleTem.setId(roleId);
				//List<Long> permissionIdList = new ArrayList<Long>();
				List<RolePermission> rolePermissionList = new ArrayList<RolePermission>();
				String[] permissionIdArray = permissionIds.split(",");
				for(String permissionId : permissionIdArray){
					if(StringUtils.isNotEmpty(permissionId)){
						Long permissionIdTemp = Long.valueOf(permissionId.trim());
						//permissionIdList.add(permissionIdTemp);
						RolePermission rolePermission = new RolePermission();
						rolePermissionList.add(rolePermission);
						Permission permission = new Permission();
						rolePermission.setRole(roleTem);
						rolePermission.setPermission(permission);
						permission.setId(permissionIdTemp);
						rolePermission.setPermission(permission);
					}
						
				}
				if(rolePermissionList.size()>0){
					rolePermissionService.save(rolePermissionList);
				}
				
			}
		}
	}
	
	@Override
	public Page<Role> getRoleList(Role role, Pageable pageable){
		User user = Jit8ShareInfoUtils.getCurrentUser();
		List<UserRole> roles = user.getRoleList();
		int maxRoleLevel = getMaxRoleLevelFromUserRoleList(roles);
		role.setRoleLevel(maxRoleLevel);
		return roleService.getRoleList(role, pageable);
	}

	@Transactional
	@Override
	public void removePermissionForRole(Role role) {
		Long roleId = role.getId();
		if(null != roleId && roleId > 0){
			Role roleExist = roleService.findOne(roleId);
			if(null != roleExist){
				
				String permissionIds = role.getPermissionIds();
				String[] permissionIdArray = null;
				if(StringUtils.isNotEmpty(permissionIds)){
					permissionIdArray = permissionIds.split(",");
					
				}
				
				List<RolePermission> permissionList = roleExist.getPermissionList();
				if(null != permissionList && permissionList.size()>0){
					Map<String,RolePermission> map = new HashMap<String,RolePermission>();
					
					for(RolePermission rolePermission : permissionList){
						Permission permission = rolePermission.getPermission();
						if(null != permission){
							map.put(permission.getId().toString(),rolePermission);
						}
					}
					
					List<RolePermission> removedList = new ArrayList<RolePermission>();
					if (null != permissionIdArray
							&& permissionIdArray.length > 0) {
						for(String permissionId : permissionIdArray){
							RolePermission rolePermission = map.get(permissionId);
							if(null != rolePermission){
								removedList.add(rolePermission);
							}
						}
						if(removedList.size()>0){
							rolePermissionService.delete(removedList);
						}
					}
				}
			}
		}
		
	}

	@Transactional
	@Override
	public void removeAllPermissionForRole(Role role) {
		Long roleId = role.getId();
		if(null != roleId && roleId > 0){
			Role roleExist = roleService.findOne(roleId);
			if(null != roleExist){
				rolePermissionService.delete(roleExist.getPermissionList());
			}
		}
	}
}
