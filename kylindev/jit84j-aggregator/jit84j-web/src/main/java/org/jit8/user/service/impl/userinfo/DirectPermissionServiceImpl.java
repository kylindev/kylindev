package org.jit8.user.service.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.DirectPermissionDao;
import org.jit8.user.persist.domain.userinfo.DirectPermission;
import org.jit8.user.service.userinfo.DirectPermissionService;
import org.springframework.stereotype.Service;

@Service
public class DirectPermissionServiceImpl extends GenericServiceImpl<DirectPermission, Long> implements DirectPermissionService {

	@Resource
	private DirectPermissionDao directPermissionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = directPermissionDao;
	}


	@Override
	public DirectPermission findByType(String type) {
		return directPermissionDao.findByType(type);
	}
	
}
