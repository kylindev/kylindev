package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteService;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class StickyNoteServiceImpl extends GenericServiceImpl<StickyNote, Long> implements StickyNoteService {

	@Resource
	private StickyNoteDao stickyNoteDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = stickyNoteDao;
	}


	@Override
	public List<StickyNote> getStickyNoteList(boolean disable, boolean deleted) {
		return stickyNoteDao.getStickyNoteList(disable, deleted);
	}


	@Override
	public Page<StickyNote> findAll(StickyNote stickyNote, Pageable pageable) {
		return stickyNoteDao.findAll(stickyNote,pageable);
	}
	
}
