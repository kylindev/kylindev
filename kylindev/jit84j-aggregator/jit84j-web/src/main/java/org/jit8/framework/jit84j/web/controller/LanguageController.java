package org.jit8.framework.jit84j.web.controller;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.springframework.beans.propertyeditors.LocaleEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

@Controller
public class LanguageController extends BaseCommonController {

	private static final Logger LOGGER = LogFactory.getLogger(LanguageController.class);
	
	@RequestMapping(value = KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING + "/{lang}", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,String> changeLanguage(@PathVariable("lang") String lang,
			HttpServletRequest request, HttpServletResponse response,Model model) {
		String msg = "";
		//response.setContentType("application/json");
		//response.setCharacterEncoding("utf-8");
		LocaleEditor localeEditor = new LocaleEditor();
		localeEditor.setAsText(lang);
		try {
			LocaleResolver localeResolver = RequestContextUtils
					.getLocaleResolver(request);
			if (localeResolver == null) {
				throw new IllegalStateException(
						"No LocaleResolver found: not in a DispatcherServlet request?");
			}
			localeResolver.setLocale(request, response,
					(Locale) localeEditor.getValue());
			msg = messageSource.getMessage("org.jit8.language.change.success",null,(Locale)localeEditor.getValue());
		} catch (Exception ex) {
			LOGGER.error(ex);
			msg = messageSource.getMessage("org.jit8.language.change.failure",null,(Locale)localeEditor.getValue());
		}
		model.addAttribute("msg", msg);
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", msg);
		return result;
	}
}
