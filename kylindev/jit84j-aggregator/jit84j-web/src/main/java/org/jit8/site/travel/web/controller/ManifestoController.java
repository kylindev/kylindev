package org.jit8.site.travel.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.InstantShareBusiness;
import org.jit8.site.travel.biz.business.ManifestoBusiness;
import org.jit8.site.travel.common.utils.TravelStaticDataUtil;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ManifestoController{

	private static final Logger logger = LoggerFactory.getLogger(ManifestoController.class);
	
	
	@Resource
	private ManifestoBusiness manifestoBusiness;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_LIST_UI_MAPPING)
	public String manifestoListUI(@ModelAttribute("manifesto") Manifesto manifesto,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_LIST_UI_MAPPING + " #########");
		Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		
		Pageable pageable2 = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),sort);
		
		Pageable commontPageable = new PageRequest(0, 5,sort);
		
		Page<Manifesto> page = manifestoBusiness.getTodayList(manifesto,pageable2,commontPageable);
		
		model.addAttribute("page", page);
		
		return "manifestoListUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_LIST_ITEM_UI_MAPPING)
	public String manifestoListItemUI(@ModelAttribute("manifesto") Manifesto manifesto,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_LIST_UI_MAPPING + " #########");
		Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		
		Pageable pageable2 = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),sort);
		
		Pageable commontPageable = new PageRequest(0, 5,sort);
		
		Page<Manifesto> page = manifestoBusiness.getTodayList(manifesto,pageable2,commontPageable);
		
		model.addAttribute("page", page);
		
		return "manifestoListWithImageUIItem";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_UI_MAPPING)
	public String manifestoEditUI(@ModelAttribute("manifesto") Manifesto manifesto,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_UI_MAPPING + " #########");
		//Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		//Pageable pageable2 = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(),sort);
		Long userId = Jit8ShareInfoUtils.getUserId();
		if(null != userId){
			manifesto = manifestoBusiness.findTodayManifestoByUserId(userId);
		}else{
			model.addAttribute("invalid.user.error","检测到您未登录，无法操作，请先登录系统");
		}
		//model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		//model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		if(manifesto == null){
			manifesto = new Manifesto();
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		
		
		//设置左侧菜单
		model.addAttribute("currentUserManageAreaItem", TravelStaticDataUtil.getCurrentUserManageAreaItemByUserIdAndUrl(Jit8ShareInfoUtils.getUserId(), KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_UI_MAPPING));

		
		return "manifestoEditUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_MAPPING)
	public String manifestoEdit(@ModelAttribute("manifesto") Manifesto manifesto,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		if(user!= null && null != userId){
			manifestoBusiness.persistTodayManifesto(manifesto,user);
			model.addAttribute("msg", "更新成功");
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		if(manifesto == null){
			manifesto = new Manifesto();
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		
		return "manifestoEditResultUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING)
	public String manifestoEditPraise(@ModelAttribute("manifesto") Manifesto manifesto,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		Long id = manifesto.getId();
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING + "/"+id+ " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		if(user!= null && null != userId && id != null && id > 0){
			manifesto = manifestoBusiness.praise(id);
			model.addAttribute("msg", "已赞");
		}else{
			model.addAttribute("error","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		
		return "manifestoPraiseResultUI";
	}
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING)
	public String manifestoEditUI(@ModelAttribute("manifesto") Manifesto manifesto,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING + " #########");
		Long id = manifesto.getId();
		if(id != null && id > 0){
			//manifesto = manifestoBusiness.getManifestoById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		return "manifestoEditUI";
	}
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING)
	public String manifestoEdit2UI(@ModelAttribute("manifesto") Manifesto manifesto,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING + " #########");
		Long id = manifesto.getId();
		if(id != null && id > 0){
			//manifesto = manifestoBusiness.getManifestoById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		return "manifestoEdit2UI";
	}
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING)
	public String manifestoEdit3UI(@ModelAttribute("manifesto") Manifesto manifesto,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING + " #########");
		Long id = manifesto.getId();
		if(id != null && id > 0){
			//manifesto = manifestoBusiness.getManifestoById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifesto", manifesto);
		return "manifestoEdit2UI";
	}

	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING + "/{id}")
	public String listManifestoComment(@PathVariable("id") Long manifestoId,@ModelAttribute("manifestoComment") ManifestoComment manifestoComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable){
		
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING + " #########");
		
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		
		if(null != manifestoId && manifestoId > 0 ){
			
			Map<String,Manifesto> map = manifestoBusiness.getThreeManifestosById(manifestoId);
			
			model.addAttribute("current", map.get(ManifestoBusiness.MANIFESTO_CURRENT));
			model.addAttribute("previous", map.get(ManifestoBusiness.MANIFESTO_PREVIOUS));
			model.addAttribute("following", map.get(ManifestoBusiness.MANIFESTO_FOLLOWING));
			
		}else{
			model.addAttribute("msg","非法参数提交");
		}
		
		return "manifestoDetailUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING)
	public String listManifestoCommentFirst(@ModelAttribute("manifestoComment") ManifestoComment manifestoComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable){
		
		//查找最新的id
		Long manifestoId = manifestoBusiness.findLastId();
		
		return listManifestoComment( manifestoId, manifestoComment, model,pageable);
	}
	
}
