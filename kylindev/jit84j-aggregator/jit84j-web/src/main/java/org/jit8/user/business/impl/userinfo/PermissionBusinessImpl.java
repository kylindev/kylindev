package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.trasient.model.Jit8InitSystemConstant;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.exception.constant.BusinessExceptionConstant;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.RolePermission;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.jit8.user.service.userinfo.PermissionService;
import org.jit8.user.service.userinfo.RolePermissionService;
import org.jit8.user.service.userinfo.UserPermissionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("permissionBusiness")
public class PermissionBusinessImpl implements PermissionBusiness {

	@Resource
	private PermissionService permissionService;
	
	@Resource
	private DeveloperBusiness developerBusiness;
	
	@Resource
	private UserPermissionService userPermissionService;
	
	@Resource
	private RolePermissionService rolePermissionService;
	
	@Override
	public Permission findPermissionByResourceUrl(String resourceUrl) {
		return permissionService.findPermissionByResourceUrl(resourceUrl);
	}
	
	@Override
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls) {
		return permissionService.findPermissionByResourceUrls(resourceUrls);
	}

	@Override
	public Permission findPermissionByCode(String code) {
		return permissionService.findPermissionByCode(code);
	}

	@Override
	public Permission persist(Permission permission) throws BusinessException {
		
		Developer developer = permission.getDeveloper();
		if(null != developer){
			developer = developerBusiness.findById(developer.getId());
			permission.setDeveloper(developer);
		}else{
			throw new BusinessException(BusinessExceptionConstant.PERMISSION_DEVELOPER_REQUIRED,"require a developer");
		}
		
		String code = StringUtils.trim(permission.getCode());
		Permission permissionExist = permissionService.findPermissionByCode(code);
		if(permissionExist != null){
			throw new BusinessException(BusinessExceptionConstant.PERMISSION_CODE_EXISTS,"this code allready exist");
		}
		
		return permissionService.save(permission);
	}

	@Override
	public Page<Permission> getPermissionList(Pageable pageable) {
		return permissionService.getPermissionList(pageable);
	}

	@Override
	public List<Permission> savePermissionList(Permission permission) {
		List<Permission> persitedPermissionList = new ArrayList<Permission>();
		if(null != permission){
			List<Permission> permissionList = permission.getPermissionList();
			if(null != permissionList && permissionList.size()>0){
				for(Permission c : permissionList){
					Long id = c.getId();
					if(null != id && id>0){
						Permission current = permissionService.findOne(id);
						current.setCode(StringUtils.trim(c.getCode()));
						current.setName(StringUtils.trim(c.getName()));
						current.setOprationCount(c.getOprationCount());
						current.setOpration(StringUtils.trim(c.getOpration()));
						//current.setResourceUrl(StringUtils.trim(c.getResourceUrl()));
						current.setFullUrl(StringUtils.trim(c.getFullUrl()));
						persitedPermissionList.add(current);
						
						Developer developer = permission.getDeveloper();
						if(null != developer){
							
						}
						
					}else{
						persitedPermissionList.add(c);
					}
					
				}
				persitedPermissionList = (List<Permission>) permissionService.save(persitedPermissionList);
			}
		}
		return persitedPermissionList;
	}

	@Override
	public Permission fineById(Long id) {
		
		return permissionService.findOne(id);
	}

	@Override
	public Page<Permission> getPermissionList(Permission permission,
			Pageable pageable) {
		return permissionService.getPermissionList(permission, pageable);
	}
	
	@Transactional
	@Override
	public Permission updatePermissionStatus(Permission permission)  {
		Long permissionId = permission.getId();
		if(permissionId != null && permissionId > 0){
			Permission permissionExist = permissionService.findOne(permissionId);
			String status = permission.getStatus();
			if(Permission.getAllStatus().contains(status)){
				permissionExist.setStatus(status);
				return permissionService.save(permissionExist);
			}
		}
		
		return permission;
		
	}

	@Override
	public List<Permission> fineByIdList(List<Long> idList) {
		
		return permissionService.fineByIdList(idList);
	}
	
	public boolean checkPermissionIsUsed(Permission permission){
		if(null != permission){
			if(null != permission.getCategory()){
				return true;
			}
			if(null != permission.getDirectPermission()){
				return true;
			}
			List<UserPermission> userPermissionList = userPermissionService.findByPermissionId(permission.getUniqueIdentifier());
			if(null != userPermissionList && userPermissionList.size()>0){
				return true;
			}
			
			List<RolePermission> rolePermissionList = rolePermissionService.findByPermissionId(permission.getUniqueIdentifier());
			if(null != rolePermissionList && rolePermissionList.size()>0){
				return true;
			}
		}
		return false;
	}
	
	public boolean checkPermissionIsUsed(Long permissionId){
		Permission permission = permissionService.findOne(permissionId);
		if(null != permission){
			return checkPermissionIsUsed(permission);
		}else{
			return false;
		}
	}

	@Override
	public List<Permission> findAll(String roleCode) {
		if(Jit8InitSystemConstant.ROLE_CODE_SUPER.equals(roleCode)){
			return permissionService.findAll();
		}
		return null;
	}

}
