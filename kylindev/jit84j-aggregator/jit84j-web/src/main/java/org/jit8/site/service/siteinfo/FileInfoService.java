package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface FileInfoService extends GenericService<FileInfo, Long>{

	public List<FileInfo> getFileInfoList(boolean disable, boolean deleted);
	
	FileInfo findByIdAndOwner(long id, long owner);
	
	public Page<FileInfo> findAll(FileInfo fileInfo, Pageable pageable);
}
