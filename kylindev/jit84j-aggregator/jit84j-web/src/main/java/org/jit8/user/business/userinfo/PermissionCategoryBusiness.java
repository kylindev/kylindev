package org.jit8.user.business.userinfo;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface PermissionCategoryBusiness {

	Page<PmnCategory> getPermissionCategoryList(Pageable pageable);
	PmnCategory persist(PmnCategory pmnCategory);
	void remove(Long id) throws BusinessException;
	
	void assignPermissionToCategory(PmnCategory pmnCategory) throws BusinessException;
	
	public void removePermissionFromCategory(PmnCategory pmnCategory) throws BusinessException;
	
	public void removeAllPermissionFromCategory (
			Long permissionId) throws BusinessException;
	
	public Permission enablePermission (
			Long permissionId) throws BusinessException;
}
