package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.business.siteinfo.PhotographBusiness;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.jit8.site.service.siteinfo.PhotographService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("photographBusiness")
public class PhotographBusinessImpl implements PhotographBusiness{

	@Resource
	private PhotographService photographService;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@Override
	public List<Photograph> getPhotographList() {
		return photographService.getPhotographList(false, false);
	}
	
	@Transactional
	@Override
	public Photograph persist(Photograph photograph){
		
		return photographService.save(photograph);
	}

	@Override
	public Photograph getPhotographById(Long id) {
		return photographService.findOne(id);
	}

	@Override
	public Photograph deletePhotographById(Long id) {
		Photograph old = photographService.findOne(id);
		old.setDeleted(true);
		return photographService.save(old);
	}

	@Transactional
	@Override
	public Photograph merge(Photograph photograph) {
		if(null != photograph){
			long id = photograph.getId();
			Photograph old = photographService.findOne(id);
			if(id>0){
				BeanUtils.copyProperties(photograph, old);
				Gallery gallery = photograph.getGallery();
				if(null != gallery){
					gallery = galleryBusiness.getGalleryById(gallery.getId());
					old.setGallery(gallery);
				}
				photograph = photographService.save(old);
			}
		}
		return photograph;
	}

}
