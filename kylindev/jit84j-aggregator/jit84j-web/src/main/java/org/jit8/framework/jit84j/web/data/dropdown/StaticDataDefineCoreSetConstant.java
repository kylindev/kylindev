package org.jit8.framework.jit84j.web.data.dropdown;

import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;

public class StaticDataDefineCoreSetConstant extends StaticDataDefineCoreConstant {

	private static final long serialVersionUID = 2406124021010276339L;

	public StaticDataDefineCoreSetConstant() {
	}

	/**
	 * 获取memached中的缓存数据
	 */
	@Override
	public void addKeysInMemcached(){
		addKeyIntoMemcached(StaticDataDefineCoreConstant.OWN_RELATION_LIST,StaticDataDefineCoreConstant.OWN_RELATION_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.NAVIGATION_TOP_LIST,StaticDataDefineCoreConstant.NAVIGATION_TOP_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.NAVIGATMENU_TOP_LIST,StaticDataDefineCoreConstant.NAVIGATMENU_TOP_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.NAVIGATION_TYPE_DROPDOWN,StaticDataDefineCoreConstant.NAVIGATION_TYPE_DROPDOWN_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.DIRECT_PERMISSION_SET,StaticDataDefineCoreConstant.DIRECT_PERMISSION_SET_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.GALLERY_LIST,StaticDataDefineCoreConstant.GALLERY_LIST_NAME);
		addKeyIntoMemcached(StaticDataDefineCoreConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN,StaticDataDefineCoreConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN_NAME);
		
	}
	
	

	@Override
	public void addMemachedModelByKey() {
		addMemachedModelByKeyIntoMemcached(StaticDataDefineCoreConstant.OWN_RELATION_LIST, new MemachedModel());
	}

}
