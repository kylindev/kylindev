package org.jit8.site.business.article;

import java.util.List;

import org.jit8.site.persist.domain.article.Article;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface ArticleBusiness {

	public Page<Article> getArticleList(Pageable page);
	public List<Article> getArticleList();
	public Article persist(Article article);
	
	public Article merge(Article article);
	
	public Article getArticleById(Long id);
	
	public Article deleteArticleById(Long id);
	
	public void asignCategory(Article article);
	
	
}
