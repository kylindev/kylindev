package org.jit8.user.business.userinfo;

import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface DeveloperBusiness {

	public Developer persist(Developer developer);
	
	public Page<Developer> getDeveloperList(Pageable pageable);
	
	public Page<Developer> getDeveloperList(Developer developer,Pageable pageable);
	
	public Developer findByUserId(Long userId);
	
	public Developer findById(Long id);
	
}
