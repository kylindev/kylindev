package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InstantShareCommentDao extends GenericDao<InstantShareComment, Long> {

	public List<InstantShareComment> getInstantShareCommentList(boolean disable, boolean deleted) ;
	
	public InstantShareComment findByCode(String code);
	
	public List<InstantShareComment> findByIds(List<Long> idList);
	
	public Page<InstantShareComment> findByInstantShareId(Long instantShareId, Pageable pageable);
}
