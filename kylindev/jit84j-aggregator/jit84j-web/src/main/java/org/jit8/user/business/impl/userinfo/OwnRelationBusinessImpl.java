package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.user.business.userinfo.OwnRelationBusiness;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.jit8.user.service.userinfo.OwnRelationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("ownRelationBusiness")
public class OwnRelationBusinessImpl implements OwnRelationBusiness{

	@Resource
	private OwnRelationService ownRelationService;

	@Override
	public List<OwnRelation> getOwnRelationList() {
		return ownRelationService.getOwnRelationList(false, false);
	}
	
	@Override
	public OwnRelation persist(OwnRelation ownRelation){
		return ownRelationService.save(ownRelation);
	}

	@Transactional(propagation=Propagation.REQUIRED)
	@Override
	public List<OwnRelation> persist(List<OwnRelation> ownRelationList) {
		List<OwnRelation> ownRelationList2 = new ArrayList<OwnRelation>(); 
		for(OwnRelation ownRelation : ownRelationList){
			Long id = ownRelation.getUniqueIdentifier();
			if(id !=null){
				OwnRelation old = ownRelationService.findOne(id);
				old.setName(ownRelation.getName());
				old.setCode(ownRelation.getCode());
				old.setSequence(ownRelation.getSequence());
				ownRelation = old;
				ownRelationList2.add(old);
			}
		}
		return (List<OwnRelation>)ownRelationService.save(ownRelationList2);
	}
}
