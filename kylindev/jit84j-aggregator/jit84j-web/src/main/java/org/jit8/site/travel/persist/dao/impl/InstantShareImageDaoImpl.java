package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.InstantShareImageDao;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.site.travel.persist.repository.InstantShareImageRepository;
import org.jit8.site.travel.persist.repository.spec.InstantShareImageSpecifications;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class InstantShareImageDaoImpl extends GenericDaoImpl<InstantShareImage, Long> implements InstantShareImageDao {

	@Resource
	private InstantShareImageRepository instantShareImageRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = instantShareImageRepository;
	}

	@Override
	public List<InstantShareImage> getInstantShareImageList(boolean disable, boolean deleted) {
		return instantShareImageRepository.getInstantShareImageList(disable, deleted);
	}
	
	@Override
	public InstantShareImage findByCode(String code) {
		return instantShareImageRepository.findByTitle( code);
	}

	@Override
	public List<InstantShareImage> findByIds(List<Long> idList) {
		return instantShareImageRepository.findByIds(idList);
	}

	@Override
	public void removeByIdAndUserId(Long id, Long userId){
		instantShareImageRepository.removeByIdAndUserId(id,userId);
	}

	@Override
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable) {
		Page<InstantShareImage> page = instantShareImageRepository.findAll(InstantShareImageSpecifications.queryByCondition(instantShareImage),pageable);
		return page;
	}

	@Override
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable) {
		Page<InstantShareImage> page = instantShareImageRepository.findByInstantShareId(instantShareId, pageable);
		return page;
	}
}
