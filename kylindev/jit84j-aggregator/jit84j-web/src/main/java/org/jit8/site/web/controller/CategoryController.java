package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.core.web.util.ThreadLocalShareInfo;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategoryController {

	private static final Logger logger = LoggerFactory.getLogger(CategoryController.class);
	
	@Resource
	private CategoryBusiness categoryBusiness;
	
	@RequestMapping("/admin/article/category/categoryAddUI")
	public String categoryAddUI(@ModelAttribute("category") Category category,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/category/addUI #########");
		
		model.addAttribute("category", new Category());
		List<Category> categoryList = categoryBusiness.getCategoryListByParentNull();
		model.addAttribute("categoryList",categoryList);
		
		return "categoryAddUI";
	}
	
	
	
	@RequestMapping("/admin/article/category/categoryAdd")
	public String categoryAdd(@ModelAttribute("category") Category category,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/category/categoryAdd #########");
		Category parent = category.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				category.setParent(null);
			}
		}
		 
		categoryBusiness.persist(category);
		return "redirect:categoryListUI";
	}
	
	@RequestMapping("/admin/article/category/categoryModify")
	public String categoryModify(@ModelAttribute("category") Category category,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/category/categoryModify #########");
		Category parent = category.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				category.setParent(null);
			}
		}
		categoryBusiness.merge(category);
		return "redirect:categoryListUI";
	}
	
	@RequestMapping("/admin/article/category/categoryModifyUI/{id}")
	public String categoryModifyUI(@PathVariable("id") Long id,Model model) throws IOException {
		logger.debug("######### /admin/article/category/categoryAdd #########");
			if(id!= null && id>0){
				Category category = categoryBusiness.getCategoryById(id);
				model.addAttribute("category",category);
				
				List<Category> categoryList = categoryBusiness.getCategoryListByParentNull();
				for( Category c : categoryList){
					List<Category> categoryList2 = c.getChildrenCategory();
					if(null != categoryList2 && categoryList2.size()>0){
						for(Category ca : categoryList2){
							System.out.println(ca.getName());
						}
					}
				}
				model.addAttribute("categoryList",categoryList);
			}
		return "categoryModifyUI";
	}
	
	
	
	@RequestMapping("/admin/article/category/categoryListUI")
	public String categoryListUI(@ModelAttribute("category") Category category,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/category/categoryListUI #########");
		List<Category> categoryList = categoryBusiness.getCategoryList();
		model.addAttribute("categoryList",categoryList);
		return "categoryListUI";
	}
	
	@RequestMapping("/admin/article/category/categoryList/modify")
	public String categoryListModify(@ModelAttribute("category") Category category,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/category/categoryListUI #########");
		List<Category> categoryList = categoryBusiness.saveCategoryList(category);
		model.addAttribute("categoryList",categoryList);
		return "categoryListUI";
	}
	
	
	
	@RequestMapping("/admin/category/addChilrenCategory")
	public String addChilrenCategory(@PathVariable("currentCategoryId") Long currentCategoryId, @PathVariable("childrenCategoryIds") Long[] childrenCategoryIds, BindingResult result, Model model) throws IOException {
		System.out.println(currentCategoryId);
		
		return "";
	}
	
	
}
