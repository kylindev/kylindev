package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteCategoryService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("stickyNoteCategoryBusiness")
public class StickyNoteCategoryBusinessImpl implements StickyNoteCategoryBusiness{

	@Resource
	private StickyNoteCategoryService stickyNoteCategoryService;
	

	@Override
	public List<StickyNoteCategory> getStickyNoteCategoryList() {
		return stickyNoteCategoryService.getStickyNoteCategoryList(false, false);
	}
	
	@Transactional
	@Override
	public StickyNoteCategory persist(StickyNoteCategory stickyNoteCategory){
		Long id = stickyNoteCategory.getId();
		StickyNoteCategory exist = null;
		if(id != null && id>0){
			 exist = stickyNoteCategoryService.findOne(stickyNoteCategory.getId());
			 exist.setCode(stickyNoteCategory.getCode());
			 exist.setDescription(stickyNoteCategory.getDescription());
			 exist.setImagePath(stickyNoteCategory.getImagePath());
			 exist.setName(stickyNoteCategory.getName());
			 exist.setOpenable(stickyNoteCategory.isOpenable());
			 exist.setSequence(stickyNoteCategory.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=stickyNoteCategory;
		}
		
		return stickyNoteCategoryService.save(exist);
	}

	@Override
	public StickyNoteCategory getStickyNoteCategoryById(Long id) {
		return stickyNoteCategoryService.findOne(id);
	}

	@Override
	public StickyNoteCategory deleteStickyNoteCategoryById(Long id) {
		StickyNoteCategory old = stickyNoteCategoryService.findOne(id);
		old.setDeleted(true);
		return stickyNoteCategoryService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			stickyNoteCategoryService.delete(id);
		}
	}

	@Override
	public Page<StickyNoteCategory> findAll(Pageable pageable) {
		return stickyNoteCategoryService.findAll(pageable);
	}

	@Override
	public StickyNoteCategory findByCode(String code) {
		return stickyNoteCategoryService.findByCode( code);
	}
	
	public List<StickyNoteCategory> findByIds(List<Long> idList){
		return stickyNoteCategoryService.findByIds(idList);
	}

}
