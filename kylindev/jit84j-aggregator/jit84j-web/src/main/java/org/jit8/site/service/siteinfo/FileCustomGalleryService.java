package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;


public interface FileCustomGalleryService extends GenericService<FileCustomGallery, Long>{

	public List<FileCustomGallery> getFileCustomGalleryList(boolean disable, boolean deleted);
	
	FileCustomGallery findByCodeAndType(String code,int type);
}
