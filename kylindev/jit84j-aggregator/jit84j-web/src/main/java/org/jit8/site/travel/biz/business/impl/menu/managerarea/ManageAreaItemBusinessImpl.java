package org.jit8.site.travel.biz.business.impl.menu.managerarea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.menu.managerarea.ManageAreaItemBusiness;
import org.jit8.site.travel.biz.service.menu.managerarea.ManageAreaItemService;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("manageAreaItemBusiness")
public class ManageAreaItemBusinessImpl implements ManageAreaItemBusiness{

	@Resource
	private ManageAreaItemService manageAreaItemService;
	
	@Resource
	private DeveloperBusiness developerBusiness;

	@Transactional
	@Override
	public ManageAreaItem persist(ManageAreaItem manageAreaItem) {
		
		ManageAreaItem exist = null;
		
		if(null != manageAreaItem ){
			
			Developer developer = null;
			// check developer, if developer is null, set default developer
			Long developerId = null;
			developerId = manageAreaItem.getDeveloperId();
			if(null != developerId ){
				developer = developerBusiness.findById(developerId);
			}
			if(null == developer){
				developer = developerBusiness.findByUserId(KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID_LONG);
				developerId = developer.getId();
			}
			
			if (null != developer) {
				Long id = manageAreaItem.getId();
				if(null != id){
					exist = manageAreaItemService.findOne(id);
				}
				if (null != exist) {
					exist.setCode(manageAreaItem.getCode());
					exist.setName(manageAreaItem.getName());
					exist.setDeveloperId(developerId);
					exist.setIconPath(manageAreaItem.getIconPath());
					exist.setUrl(manageAreaItem.getUrl());
					exist.setOwnerId(developer.getUserId().getUserId());
					exist.setSequence(manageAreaItem.getSequence());
					exist.setDisplay(manageAreaItem.getDisplay());
					exist.setDisable(manageAreaItem.isDisable());
				} else {
					exist = manageAreaItem;
					exist.setDisable(true);// 设置默认显示
					exist.setHideable(false);// 设置默认不隐藏
					exist.setDisable(false);// 设置不禁用
					exist.setDeleted(false);// 设置软删除为false
					// manageAreaItem.setIdKey(idKey); TODO
					exist.setOwnerId(developer.getUserId().getUserId());// 设置所有者，userId
					exist.setDeveloperId(developerId);// 设置开发者
				}
				exist = manageAreaItemService.save(exist);
			}
		}
		return manageAreaItem;
	}
	
	@Transactional
	@Override
	public List<ManageAreaItem> persistList(List<ManageAreaItem> manageAreaItemList){
		if(null != manageAreaItemList && manageAreaItemList.size()>0){
			Map<Long, ManageAreaItem> map = new HashMap<Long,ManageAreaItem>();
			List<Long> idList = new ArrayList<Long>(manageAreaItemList.size());
			for(ManageAreaItem manageAreaItem : manageAreaItemList){
				if(null != manageAreaItem){
					Long id = manageAreaItem.getId();
					if(null != id && id>0){
						map.put(id, manageAreaItem);
						idList.add(id);
					}
				}
			}
			
			if(idList.size()>0){
				List<ManageAreaItem> existList = manageAreaItemService.findByIdList(idList);
				if(null != existList && existList.size()>0){
					for(ManageAreaItem exist : existList){
						ManageAreaItem source = map.get(exist.getId());
						exist.setSequence(source.getSequence());
						exist.setDisable(source.isDisable());
						exist.setDisplay(source.getDisplay());
						exist.setDeleted(source.isDeleted());
					}
					manageAreaItemService.save(existList);
				}
			}
		}
		
		return null;
	}

	@Override
	public List<ManageAreaItem> findAll() {
		List<ManageAreaItem> all = manageAreaItemService.findAll();
		return all;
	}
	
	@Override
	public List<ManageAreaItem> findAllAvailable(){
		List<ManageAreaItem> allAvailable = manageAreaItemService.findAllAvailable();
		return allAvailable;
	}

	@Override
	public List<ManageAreaItem> findAllDisplay() {
		List<ManageAreaItem> allDisplay = manageAreaItemService.findAllDisplay();
		return allDisplay;
	}

	@Override
	public Page<ManageAreaItem> findAll(Pageable pageable) {
		return manageAreaItemService.findAll(pageable);
	}

	@Override
	public ManageAreaItem findById(Long id) {
		ManageAreaItem manageAreaItem = manageAreaItemService.findOne(id);
		return manageAreaItem;
	}

	@Override
	public void removeById(Long id) {
		manageAreaItemService.delete(id);
	}
}
