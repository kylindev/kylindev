package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.OwnRelationDao;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.jit8.user.service.userinfo.OwnRelationService;
import org.springframework.stereotype.Service;

@Service
public class OwnRelationServiceImpl extends GenericServiceImpl<OwnRelation, Long> implements OwnRelationService {

	@Resource
	private OwnRelationDao ownRelationDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = ownRelationDao;
	}


	@Override
	public List<OwnRelation> getOwnRelationList(boolean disable, boolean deleted) {
		return ownRelationDao.getOwnRelationList(disable, deleted);
	}
	
	
}
