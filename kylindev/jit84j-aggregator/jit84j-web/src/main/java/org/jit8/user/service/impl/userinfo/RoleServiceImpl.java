package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.RoleDao;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.service.userinfo.RoleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role, Long> implements RoleService {

	@Resource
	private RoleDao roleDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = roleDao;
	}


	@Override
	public List<Role> getRoleListByRoleLevel(int roleLevel) {
		return roleDao.getRoleListByRoleLevel(roleLevel);
	}


	@Override
	public List<Role> getRoleListByRoleIdList(List<Long> roleIdList) {
		return roleDao.getRoleListByRoleIdList(roleIdList);
	}


	@Override
	public Page<Role> getRoleList(Pageable pageable) {
		return roleDao.getRoleList(pageable);
	}


	@Override
	public List<Role> saveRoleList(List<Role> roleList) {
		return null;
	}


	@Override
	public Page<Role> getRoleList(Role role, Pageable pageable) {
		return roleDao.getRoleList( role,  pageable);
	}
	
	
}
