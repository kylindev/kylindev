package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.ActivityBusiness;
import org.jit8.site.travel.biz.service.ActivityService;
import org.jit8.site.travel.persist.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("activityBusiness")
public class ActivityBusinessImpl implements ActivityBusiness{

	@Resource
	private ActivityService activityService;
	

	@Override
	public List<Activity> getActivityList() {
		return activityService.getActivityList(false, false);
	}
	
	@Transactional
	@Override
	public Activity persist(Activity activity){
		Long id = activity.getId();
		Activity exist = null;
		if(id != null && id>0){
			 exist = activityService.findOne(activity.getId());
			 exist.setDescription(activity.getDescription());
			 exist.setImagePath(activity.getImagePath());
			 exist.setSequence(activity.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=activity;
		}
		
		return activityService.save(exist);
	}

	@Override
	public Activity getActivityById(Long id) {
		return activityService.findOne(id);
	}

	@Override
	public Activity deleteActivityById(Long id) {
		Activity old = activityService.findOne(id);
		old.setDeleted(true);
		return activityService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			activityService.delete(id);
		}
	}

	@Override
	public Page<Activity> findAll(Pageable pageable) {
		return activityService.findAll(pageable);
	}

	@Override
	public Activity findByCode(String code) {
		return activityService.findByCode( code);
	}

	@Override
	public List<Activity> findByIds(List<Long> idList) {
		return activityService.findByIds(idList);
	}
	
	@Override
	public Page<Activity> findByUserId(Long userId, Pageable pageable) {
		if(null == userId){
			return null;
		}
		return activityService.findByUserId( userId, pageable);
	}

}
