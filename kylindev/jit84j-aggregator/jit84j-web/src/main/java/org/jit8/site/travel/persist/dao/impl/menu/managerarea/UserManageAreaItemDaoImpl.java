package org.jit8.site.travel.persist.dao.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.menu.managerarea.UserManageAreaItemDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.jit8.site.travel.persist.repository.menu.managerarea.UserManageAreaItemRepository;
import org.springframework.stereotype.Repository;

@Repository
public class UserManageAreaItemDaoImpl extends GenericDaoImpl<UserManageAreaItem, Long> implements UserManageAreaItemDao {

	@Resource
	private UserManageAreaItemRepository userManageAreaItemRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = userManageAreaItemRepository;
	}

	@Override
	public List<UserManageAreaItem> findByUserId(Long userId) {
		return userManageAreaItemRepository.findByUserId( userId);
	}

	@Override
	public List<UserManageAreaItem> findAllDisplayByUserId(Long userId) {
		return userManageAreaItemRepository.findAllDisplayByUserId(userId);
	}

	
}
