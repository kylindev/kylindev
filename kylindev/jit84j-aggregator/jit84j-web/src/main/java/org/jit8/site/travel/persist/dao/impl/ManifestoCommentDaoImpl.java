package org.jit8.site.travel.persist.dao.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.ManifestoCommentDao;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.jit8.site.travel.persist.repository.ManifestoCommentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ManifestoCommentDaoImpl extends GenericDaoImpl<ManifestoComment, Long> implements ManifestoCommentDao {

	@Resource
	private ManifestoCommentRepository manifestoCommentRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = manifestoCommentRepository;
	}

	@Override
	public Page<ManifestoComment> findByManifestoId(Long manifestoId,
			Pageable pageable) {
		return manifestoCommentRepository.findByManifestoId( manifestoId,
				 pageable);
	}


}
