package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.MemberInfo;


public interface MemberInfoService extends GenericService<MemberInfo, Long>{

	public List<MemberInfo> getMemberInfoList(boolean disable, boolean deleted);
	
	public MemberInfo findByCode(String code);
	
	public List<MemberInfo> findByIds(List<Long> idList);
}
