package org.jit8.site.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.xml.ws.handler.MessageContext;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.user.business.userinfo.DirectPermissionBusiness;
import org.jit8.user.developer.common.constants.MessageConstant;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.DirectPermission;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class DirectPermissionController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(DirectPermissionController.class);
	

	@Resource
	private DirectPermissionBusiness directPermissionBusiness;
	
	
	@InitBinder  
    public void initBinder(DataBinder binder) {  
       //binder.setValidator();  
    }
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_MAPPING + "/{code}")
	public String directPermissionListUI(@PathVariable("code") String code,@PageableDefaults(pageNumber = 1, value = 30) Pageable pageable,@ModelAttribute Permission permission,RedirectAttributes  redirectAttributes, Model model) {
		
		logger.debug("######### /admin/userinfo/permission/permissionListUI #########");
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		//model.addAttribute("page", page);
		return "permissionListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING)
	@ResponseBody
	public Map<String,String> directPermissionAdd(@ModelAttribute DirectPermission directPermission,Model model,HttpServletRequest request) {
		//
		logger.debug("######### /admin/userinfo/permission/permissionAdd #########");
		Map<String,String> result = new HashMap<String,String>();
		Long permissionId = directPermission.getPermissionId();
		if(null == permissionId || permissionId<1){
			result.put("message", MessageUtil.getLocaleMessage(request, MessageConstant.CODE_DIRECTPERMISSION_INVALID_PERMISSION_ID, null));
			return result;
		}
		directPermission = directPermissionBusiness.persist(directPermission);
		result.put("directPermissionId", String.valueOf(directPermission.getId()));
		result.put("message", MessageUtil.getLocaleMessage(request, MessageConstant.CODE_DIRECTPERMISSION_ADDED_SUCCESS, null));
		
		return result;
	}
	
}
