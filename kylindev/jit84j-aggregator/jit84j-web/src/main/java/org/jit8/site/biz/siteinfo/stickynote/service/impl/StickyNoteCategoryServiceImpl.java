package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteCategoryService;
import org.jit8.site.persist.siteinfo.stickynote.dao.StickyNoteCategoryDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.springframework.stereotype.Service;

@Service
public class StickyNoteCategoryServiceImpl extends GenericServiceImpl<StickyNoteCategory, Long> implements StickyNoteCategoryService {

	@Resource
	private StickyNoteCategoryDao specialAreaDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = specialAreaDao;
	}


	@Override
	public List<StickyNoteCategory> getStickyNoteCategoryList(boolean disable, boolean deleted) {
		return specialAreaDao.getStickyNoteCategoryList(disable, deleted);
	}


	@Override
	public StickyNoteCategory findByCode(String code) {
		return specialAreaDao.findByCode( code);
	}


	@Override
	public List<StickyNoteCategory> findByIds(List<Long> idList) {
		return specialAreaDao.findByIds( idList);
	}
	
}
