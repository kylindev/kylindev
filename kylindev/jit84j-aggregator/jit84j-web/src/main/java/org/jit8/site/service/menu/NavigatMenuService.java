package org.jit8.site.service.menu;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.menu.NavigatMenu;


public interface NavigatMenuService extends GenericService<NavigatMenu, Long>{

	public static final String TOP_NAME="顶级导航";
	public static final String TOP_CODE="top_NavigatMenu";
	
	public List<NavigatMenu> getNavigatMenuList(boolean disable, boolean deleted);
	
	public List<NavigatMenu> getNavigatMenuListByIds(boolean disable,boolean deleted,List<Long> navigatMenuIdList);
	
	public List<NavigatMenu> getNavigatMenuListByParentNull();
	
	public List<NavigatMenu> getNavigatMenuByCode(boolean disable, boolean deleted,
			String code);
	
	public List<NavigatMenu> getNavigatMenuByTopCode(boolean disable,
			boolean deleted, String code);
	
	public NavigatMenu findByCode(String code);
}
