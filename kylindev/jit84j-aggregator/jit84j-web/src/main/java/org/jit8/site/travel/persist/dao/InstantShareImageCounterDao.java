package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;

public interface InstantShareImageCounterDao extends GenericDao<InstantShareImageCounter, Long> {

	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId);
	
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId);
	
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId);
	
}
