package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.OwnRelation;

public interface OwnRelationService extends GenericService<OwnRelation, Long>{

	public List<OwnRelation> getOwnRelationList(boolean disable, boolean deleted);
}
