package org.jit8.site.travel.web.controller.menu.managerarea;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.menu.managerarea.ManageAreaItemBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ManageAreaItemController{

	private static final Logger logger = LoggerFactory.getLogger(ManageAreaItemController.class);
	
	
	@Resource
	private ManageAreaItemBusiness manageAreaItemBusiness;
	
	@Resource
	private Mapper mapper;
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING)
	public String userManageAreaItemEditUI(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING + " #########");
		Long id = manageAreaItem.getId();
		if(id != null && id > 0){
			//userManageAreaItem = userManageAreaItemBusiness.getUserManageAreaItemById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manageAreaItem", manageAreaItem);
		model.addAttribute("batch", CalendarUtils.getNano1000Time());
		return "manageAreaItemEditUI";
	}
	
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_INDEX_UI_MAPPING)
	public String manageAreaItemIndexUI(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_INDEX_UI_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manageAreaItem", manageAreaItem);
		Page<ManageAreaItem> page= manageAreaItemBusiness.findAll(pageable);
		model.addAttribute("page", page);
		
		return "manageAreaItemIndexUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_UI_MAPPING)
	public String manageAreaItemEditUI(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_UI_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		Long id = manageAreaItem.getId();
		if(null != id && id>0){
			manageAreaItem = manageAreaItemBusiness.findById(id);
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manageAreaItem", manageAreaItem);
		Page<ManageAreaItem> page= manageAreaItemBusiness.findAll(pageable);
		model.addAttribute("page", page);
		//return "stickyNoteCategoryEditUI";
		return "manageAreaItemEditUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_REMOVE_MAPPING)
	public String manageAreaItemRemove(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = manageAreaItem.getId();
		if(null != id && id>0){
			manageAreaItemBusiness.removeById(id);
			manageAreaItem.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return manageAreaItemEditUI( manageAreaItem,model,pageable);
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING)
	public String manageAreaItemEdit(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING +" #########");
//		String code= KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT;
//		if(StringUtils.isNotEmpty(code)){
//			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
//		}
//		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		
		manageAreaItem = manageAreaItemBusiness.persist(manageAreaItem);
		//Page<ManageAreaItem> page= manageAreaItemBusiness.findAll(pageable);
		
		//model.addAttribute("page", page);
		//model.addAttribute("manageAreaItem", manageAreaItem);
		
		return manageAreaItemEditUI( manageAreaItem,model,pageable);
	}
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_LIST_EDIT_MAPPING)
	public String manageAreaItemListEdit(@ModelAttribute("manageAreaItem") ManageAreaItem manageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING +" #########");
		
		List<ManageAreaItem> manageAreaItemList = manageAreaItemBusiness.persistList(manageAreaItem.getManageAreaItemList());
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return manageAreaItemEditUI( manageAreaItem,model,pageable);
	}
}
