package org.jit8.user.business.impl.userinfo;

import javax.annotation.Resource;

import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.service.userinfo.DeveloperService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("developerBusiness")
public class DeveloperBusinessImpl implements DeveloperBusiness {

	@Resource
	private DeveloperService developerService;
	
    @Transactional
	@Override
	public Developer persist(Developer developer) {
		
		return developerService.save(developer);
	}

	@Override
	public Page<Developer> getDeveloperList(Pageable pageable) {
		return developerService.getDeveloperList(pageable);
	}
	
	@Override
	public Developer findByUserId(Long userId){
		return developerService.findByUserId(userId);
	}

	@Override
	public Developer findById(Long id) {
		return developerService.findOne(id);
	}

	@Override
	public Page<Developer> getDeveloperList(Developer developer,
			Pageable pageable) {
		return developerService.getDeveloperList(developer, pageable);
	}
	

}
