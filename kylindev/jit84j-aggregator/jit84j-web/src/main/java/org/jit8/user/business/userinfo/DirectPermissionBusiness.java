package org.jit8.user.business.userinfo;

import java.util.List;
import java.util.Set;

import org.jit8.user.persist.domain.userinfo.DirectPermission;


public interface DirectPermissionBusiness {

	
	public List<DirectPermission> findAll();
	
	public DirectPermission persist(DirectPermission directPermission);
	
	/**
	 * 获取 直接访问 的权限集合
	 * 
	 * @return
	 */
	public Set<String> getDirectPermissionSet();
	
}
