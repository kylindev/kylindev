package org.jit8.site.business.impl.article;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.service.article.CategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("categoryBusiness")
public class CategoryBusinessImpl implements CategoryBusiness{

	private static final Logger logger = LoggerFactory.getLogger(CategoryBusinessImpl.class);
	
	@Resource
	private CategoryService categoryService;
	

	@Override
	public List<Category> getCategoryList() {
		return categoryService.getCategoryList(false, false);
	}
	
	@Transactional
	@Override
	public Category persist(Category category){
		
		return categoryService.save(category);
	}
	
	@Transactional
	@Override
	public Category merge(Category category){
		
		Category old = categoryService.findOne(category.getId());
		
		List<Category> categoryList = category.getChildrenCategory();
		List<Category> existcategoryList = old.getChildrenCategory();
		if(null==existcategoryList){
			existcategoryList = new ArrayList<Category>();
		}
		for(Category existCategory : existcategoryList){
			existCategory.setParent(null);
		}
		Category parent = old.getParent();
		Long parentId = -1l;
		if(parent != null){
			parentId = parent.getId();
		}
		if(null != categoryList && categoryList.size()>0){
			for(Category c : categoryList){
				Long id = c.getId();
				if(id != null && id>0){
					if(id.equals(parentId)){
						continue;//检测子类别不能与父类别相同，删除相同的id
					}
					Category category2 = categoryService.findOne(id);
					
					category2.setParent(old);
					existcategoryList.add(category2);
				}
			}
			category.setChildrenCategory(existcategoryList);
		}
		BeanUtils.copyProperties(category, old);
		return categoryService.save(old);
	}

	@Override
	public Category getCategoryById(Long id) {
		return categoryService.findOne(id);
	}

	@Override
	public Category deleteCategoryById(Long id) {
		Category old = categoryService.findOne(id);
		old.setDeleted(true);
		return categoryService.save(old);
	}

	@Override
	public List<Category> getCategoryListByIds( String categoryIdList) {
		if(StringUtils.isNotEmpty(categoryIdList)){
			String[] ids = categoryIdList.split(",");
			List<Long> categoryIds = new ArrayList<Long>();
			for(String id : ids){
				if(StringUtils.isNotEmpty(id)){
					categoryIds.add(Long.valueOf(id));
				}
			}
			return categoryService.getCategoryListByIds(false, false, categoryIds);
		}
		return null;
	}

	@Override
	public List<Category> getCategoryListByParentNull() {
		return categoryService.getCategoryListByParentNull();
	}
	
	@Transactional
	@Override
	public List<Category> saveCategoryList(Category category){
		List<Category> persitedCategoryList = new ArrayList<Category>();
		if(null != category){
			List<Category> categoryList = category.getCategoryList();
			if(null != categoryList && categoryList.size()>0){
				for(Category c : categoryList){
					Long id = c.getId();
					if(null != id && id>0){
						Category current = categoryService.findOne(id);
						current.setCode(c.getCode());
						current.setName(c.getName());
						current.setSequence(c.getSequence());
						persitedCategoryList.add(current);
					}else{
						persitedCategoryList.add(c);
					}
					
				}
				persitedCategoryList = (List<Category>) categoryService.save(persitedCategoryList);
			}
		}
		return persitedCategoryList;
	}

}
