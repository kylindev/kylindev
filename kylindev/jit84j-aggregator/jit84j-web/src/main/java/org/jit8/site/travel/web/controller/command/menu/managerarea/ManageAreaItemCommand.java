package org.jit8.site.travel.web.controller.command.menu.managerarea;

import java.io.Serializable;

import com.google.gson.annotations.Expose;

public class ManageAreaItemCommand implements Serializable {

	private static final long serialVersionUID = 60411763610492911L;

	@Expose
	private String name;//所显示的名称
	
	@Expose
	private String code;//默认与idKey相同，也可自行定制
	
	@Expose
	private String idKey;//由系统导入的，存储此idKey
	
	@Expose
	private String url;//url
	
	@Expose
	private String iconPath;//前导图标
	
	@Expose
	private boolean hideable;//隐藏
	
	@Expose
	private boolean display;//显示
	
	@Expose
	private boolean disable;//禁用
	
	@Expose
	private Long developerId;//开发者id
	
	@Expose
	private Long id;
	
	@Expose
	private long sequence;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the idKey
	 */
	public String getIdKey() {
		return idKey;
	}

	/**
	 * @param idKey the idKey to set
	 */
	public void setIdKey(String idKey) {
		this.idKey = idKey;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the iconPath
	 */
	public String getIconPath() {
		return iconPath;
	}

	/**
	 * @param iconPath the iconPath to set
	 */
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	/**
	 * @return the hideable
	 */
	public boolean isHideable() {
		return hideable;
	}

	/**
	 * @param hideable the hideable to set
	 */
	public void setHideable(boolean hideable) {
		this.hideable = hideable;
	}

	/**
	 * @return the display
	 */
	public boolean isDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(boolean display) {
		this.display = display;
	}

	/**
	 * @return the developerId
	 */
	public Long getDeveloperId() {
		return developerId;
	}

	/**
	 * @param developerId the developerId to set
	 */
	public void setDeveloperId(Long developerId) {
		this.developerId = developerId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the sequence
	 */
	public long getSequence() {
		return sequence;
	}

	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}

	/**
	 * @return the disable
	 */
	public boolean isDisable() {
		return disable;
	}

	/**
	 * @param disable the disable to set
	 */
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
}
