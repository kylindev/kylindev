package org.jit8.site.service.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.article.ArticleDao;
import org.jit8.site.persist.domain.article.Article;
import org.jit8.site.service.article.ArticleService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ArticleServiceImpl extends GenericServiceImpl<Article, Long> implements ArticleService {

	@Resource
	private ArticleDao articleDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = articleDao;
	}


	@Override
	public List<Article> getArticleList(boolean disable, boolean deleted) {
		return articleDao.getArticleList(disable, deleted);
	}
	
	@Override
	public Page<Article> getArticleList(boolean disable, boolean deleted,Pageable pageable) {
		return articleDao.getArticleList(disable, deleted,pageable);
	}
}
