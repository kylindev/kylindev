package org.jit8.user.business.impl.userinfo;

import javax.annotation.Resource;

import org.jit8.user.business.userinfo.EmailUrlBusiness;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.service.userinfo.EmailUrlService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service("emailUrlBusiness")
public class EmailUrlBusinessImpl implements EmailUrlBusiness {

	private static final Logger logger = LoggerFactory.getLogger(EmailUrlBusinessImpl.class);
	
	@Resource
	private EmailUrlService emailUrlService;

	@Override
	public EmailUrl findByUrlCode(String urlCode) {
		return emailUrlService.findByUrlCode(urlCode);
	}
	
	@Override
	public EmailUrl persist(EmailUrl emailUrl){
		return emailUrlService.save(emailUrl);
	}
	
}
