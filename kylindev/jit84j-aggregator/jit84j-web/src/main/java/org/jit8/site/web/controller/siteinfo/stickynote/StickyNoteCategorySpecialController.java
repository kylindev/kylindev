package org.jit8.site.web.controller.siteinfo.stickynote;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategorySpecialBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StickyNoteCategorySpecialController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(StickyNoteCategorySpecialController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private StickyNoteCategorySpecialBusiness stickyNoteCategorySpecialBusiness;
	
	@Resource
	private StickyNoteCategoryBusiness stickyNoteCategoryBusiness;
	
	@Resource
	private SpecialAreaBusiness specialAreaBusiness;
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_MAPPING)
	public String stickyNoteCategorySpecialIndexUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		Page<StickyNoteCategorySpecial> page= stickyNoteCategorySpecialBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "stickyNoteCategorySpecialIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_MAPPING)
	public String stickyNoteCategorySpecialListUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		StickyNoteCategory stickyNoteCategory = null;
		SpecialArea specialArea = null;
		CategorySpecial categorySpecial = stickyNoteCategorySpecial.getCategorySpecial();
		if(null != categorySpecial){
			stickyNoteCategory = categorySpecial.getCategory();
			specialArea = categorySpecial.getSpecialArea();
		}
		
		if(null == stickyNoteCategory){
			stickyNoteCategory = new StickyNoteCategory();
		}
		if(null == specialArea){
			specialArea = new SpecialArea();
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		model.addAttribute("specialArea", specialArea);
		Pageable stickyNoteCategorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.DESC, "uniqueIdentifier");
		Page<StickyNoteCategorySpecial> page= stickyNoteCategorySpecialBusiness.findAll(stickyNoteCategorySpecial,stickyNoteCategorySpecialPageable);
		model.addAttribute("page", page);
		return "stickyNoteCategorySpecialListUI";
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING)
	public String categoryAsignSpecialUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		
		Pageable categoryPageAll = new PageRequest(0, 200, Direction.ASC, "sequence");
		Page<StickyNoteCategory> categoryPage= stickyNoteCategoryBusiness.findAll(categoryPageAll);
		model.addAttribute("categoryPage", categoryPage);
		
		Page<SpecialArea> specialAreaPage = specialAreaBusiness.findAll(categoryPageAll);
		model.addAttribute("specialAreaPage", specialAreaPage);
		
		Pageable stickyNoteCategorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<StickyNoteCategorySpecial> stickyNoteCategorySpecialPage = stickyNoteCategorySpecialBusiness.findAll(stickyNoteCategorySpecial,stickyNoteCategorySpecialPageable);
		model.addAttribute("stickyNoteCategorySpecialPage", stickyNoteCategorySpecialPage);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		
		return "stickyNoteCategorySpecialBindUI";
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING)
	public String categoryAsignSpecial(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		//stickyNoteCategorySpecialBusiness.asignCategory2SpecialArea(stickyNoteCategorySpecial);
		return categoryAsignSpecialUI(stickyNoteCategorySpecial,model,pageable);
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING)
	public String saveStickyNoteCategorySpecialList(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		//stickyNoteCategorySpecialBusiness.saveStickyNoteCategorySpecialList(stickyNoteCategorySpecial);
		return categoryAsignSpecialUI(stickyNoteCategorySpecial,model,pageable);
	}
	
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING)
	public String stickyNoteCategorySpecialAsignListUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		
		Pageable stickyNoteCategorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<StickyNoteCategorySpecial> stickyNoteCategorySpecialPage = stickyNoteCategorySpecialBusiness.findAll(stickyNoteCategorySpecial,stickyNoteCategorySpecialPageable);
		model.addAttribute("stickyNoteCategorySpecialPage", stickyNoteCategorySpecialPage);
		
		return "stickyNoteCategorySpecialAsignListUI";
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING)
	public String categoryRemoveFromSpecial(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
//		stickyNoteCategorySpecialBusiness.removeCategoryFromSpecialArea(stickyNoteCategorySpecial);
//		stickyNoteCategorySpecial.setCategoryIds(null);
//		stickyNoteCategorySpecial.setSpecialAreaIds(null);
		stickyNoteCategorySpecial.setId(null);
		return categoryAsignSpecialUI(stickyNoteCategorySpecial,model,pageable);
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_MAPPING)
	public String stickyNoteCategorySpecialRemoveInAsignUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = stickyNoteCategorySpecial.getId();
		if(null != id && id>0){
			stickyNoteCategorySpecialBusiness.removeById(id);
			stickyNoteCategorySpecial.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return categoryAsignSpecialUI(stickyNoteCategorySpecial,model,pageable);
	}
	
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING)
	public String stickyNoteCategorySpecialEdit(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,@ModelAttribute("stickyNoteCategorySpecialSearch") StickyNoteCategorySpecial stickyNoteCategorySpecialSearch,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		
		Pageable stickyNoteCategorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<StickyNoteCategorySpecial> page = stickyNoteCategorySpecialBusiness.findAll(stickyNoteCategorySpecialSearch,stickyNoteCategorySpecialPageable);
		//Page<StickyNoteCategorySpecial> page= stickyNoteCategorySpecialBusiness.findAll(pageable);
		if(validateStickyNoteCategorySpecial(stickyNoteCategorySpecial,model)){
			stickyNoteCategorySpecial = stickyNoteCategorySpecialBusiness.persist(stickyNoteCategorySpecial);
		}
		model.addAttribute("page", page);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		
		return stickyNoteCategorySpecialEditUI( stickyNoteCategorySpecial,stickyNoteCategorySpecialSearch,model,pageable);
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING)
	public String stickyNoteCategorySpecialEditUI(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,@ModelAttribute("stickyNoteCategorySpecialSearch") StickyNoteCategorySpecial stickyNoteCategorySpecialSearch,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Long id = stickyNoteCategorySpecial.getId();
		if(null != id && id>0){
			stickyNoteCategorySpecial = stickyNoteCategorySpecialBusiness.getStickyNoteCategorySpecialById(id);
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategorySpecial", stickyNoteCategorySpecial);
		Pageable stickyNoteCategorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<StickyNoteCategorySpecial> page = stickyNoteCategorySpecialBusiness.findAll(stickyNoteCategorySpecialSearch,stickyNoteCategorySpecialPageable);
		model.addAttribute("page", page);
		return "stickyNoteCategorySpecialEditUI";
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_MAPPING)
	public String stickyNoteCategorySpecialRemove(@ModelAttribute("stickyNoteCategorySpecial") StickyNoteCategorySpecial stickyNoteCategorySpecial,@ModelAttribute("stickyNoteCategorySpecialSearch") StickyNoteCategorySpecial stickyNoteCategorySpecialSearch, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = stickyNoteCategorySpecial.getId();
		if(null != id && id>0){
			stickyNoteCategorySpecialBusiness.removeById(id);
			stickyNoteCategorySpecial.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return stickyNoteCategorySpecialEditUI( stickyNoteCategorySpecial,stickyNoteCategorySpecialSearch,model,pageable);
	}
	
	private boolean validateStickyNoteCategorySpecial(StickyNoteCategorySpecial stickyNoteCategorySpecial, Model model){
		boolean pass = true;
//		if(null != stickyNoteCategorySpecial && stickyNoteCategorySpecial.getId()==null){
//			String code = stickyNoteCategorySpecial.getName();
//			StickyNoteCategorySpecial exist = stickyNoteCategorySpecialBusiness.findByName(code);
//			if(null != exist){
//				model.addAttribute("msg", "系统中已经存在此code【"+code+"】,请换一个");
//				pass=false;
//			}
//		}
		
		return pass;
	}
	
}
