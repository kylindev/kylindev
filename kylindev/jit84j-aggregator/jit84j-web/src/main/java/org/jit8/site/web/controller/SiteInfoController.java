package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SiteInfoController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(SiteInfoController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_MAPPING)
	public String siteInfoUI(@ModelAttribute("siteInfo") SiteInfo siteInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_MAPPING + " #########");
		
		Long id = siteInfo.getId();
		if(id != null && id > 0){
			siteInfo = siteInfoBusiness.getSiteInfoById(id);
		}else{
			siteInfo = siteInfoBusiness.getSiteInfoByType(SiteInfo.MAIN_SITE);//取默认主站
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		
		model.addAttribute("siteInfo", siteInfo);
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI));
		
		return "siteInfoUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_MAPPING)
	public String siteInfoBasicEditUI(@ModelAttribute("siteInfo") SiteInfo siteInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_MAPPING + " #########");
		Long id = siteInfo.getId();
		if(id != null && id > 0){
			siteInfo = siteInfoBusiness.getSiteInfoById(id);
		}else{
			siteInfo = siteInfoBusiness.getSiteInfoByType(SiteInfo.MAIN_SITE);//取默认主站
		}
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI));
		model.addAttribute("siteInfo", siteInfo);
		return "siteInfoBasicEditUI";
	}
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING)
	public Map<String,String> siteInfoBasicEdit(@ModelAttribute("siteInfo") SiteInfo siteInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING + " #########");
		Long id = siteInfo.getId();
		if(id != null && id > 0){
			siteInfo = siteInfoBusiness.merge(siteInfo);
		}
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", "success");
		return result;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_MAPPING)
	public String siteInfoListUI(Model model) throws IOException {
		logger.debug("######### /siteInfoUI #########");
		List<SiteInfo> siteInfoList = siteInfoBusiness.getSiteInfoList();
		model.addAttribute("siteInfoList", siteInfoList);
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST));
		model.addAttribute("navigationTopList",navigationTopList);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "siteInfoListUI";
	}
	
	@RequestMapping("/admin/siteInfo/modify")
	public String siteInfoModify(@ModelAttribute("siteInfo") SiteInfo siteInfo,BindingResult result, Model model) throws IOException {
		logger.debug("######### /siteInfoUI #########");
		
		if(result.hasErrors()){
			return "siteInfoUI";
		}
		
		siteInfoBusiness.persist(siteInfo);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:siteInfoListUI";
	}
	
	@RequestMapping("/admin/siteInfo/add")
	public String siteInfoAdd(@ModelAttribute("siteInfo") SiteInfo siteInfo,BindingResult result, Model model) throws IOException {
		logger.debug("######### /siteInfoUI #########");
		
		if(result.hasErrors()){
			return "siteInfoUI";
		}
		
		siteInfoBusiness.persist(siteInfo);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:siteInfoListUI";
	}
	
	
	@RequestMapping("/admin/siteInfo/delete/{id}")
	public String siteInfoDelete(@PathVariable Long id, Model model) throws IOException {
		logger.debug("######### /admin/siteInfo/delete/{id} #########");
		siteInfoBusiness.deleteSiteInfoById(id);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:/admin/siteInfo/siteInfoListUI";
	}
	
	
}
