package org.jit8.site.travel.biz.business;

import java.util.Date;
import java.util.List;

import org.jit8.site.travel.persist.domain.InstantShareImageCounter;




public interface InstantShareImageCounterBusiness {

	public InstantShareImageCounter persist(InstantShareImageCounter instantShareImageCounter);
	
	public InstantShareImageCounter merge(InstantShareImageCounter instantShareImageCounter);
	
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId);
	
	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId);
	
	public InstantShareImageCounter addCountByDayAndUserId(int count, String day, Long userId);
	
	public InstantShareImageCounter addCountByDayAndUserId(int count, Date date, Long userId);
	
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId);
	
	public List<InstantShareImageCounter> findByMonthForUser(String yearMonth, Long userId);
	
	public InstantShareImageCounter findByDailyAndUserId(Date date, Long userId);
	
	public List<InstantShareImageCounter> findByCurrentMonthForUser( Long userId);
	
}
