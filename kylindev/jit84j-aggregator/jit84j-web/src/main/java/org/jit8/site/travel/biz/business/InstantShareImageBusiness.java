package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface InstantShareImageBusiness {

	public List<InstantShareImage> getInstantShareImageList();
	
	public InstantShareImage persist(InstantShareImage journey);
	
	public InstantShareImage getInstantShareImageById(Long id);
	
	public InstantShareImage deleteInstantShareImageById(Long id);
	
	public Page<InstantShareImage> findAll(Pageable pageable);
	
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable);
	
	public Page<InstantShareImage> findByDayAndUserId(InstantShareImage instantShareImage, Pageable pageable);
	
	public InstantShareImage findByCode(String code);
	
	public void removeById(Long id);
	
	public void removeByIdAndUserId(Long id, Long userId);
	
	public List<InstantShareImage> findByIds(List<Long> idList);
	
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable);
}
