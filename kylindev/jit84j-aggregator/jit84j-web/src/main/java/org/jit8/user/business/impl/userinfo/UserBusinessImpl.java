package org.jit8.user.business.impl.userinfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.business.MailMessageBusiness;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.mail.domain.MailTemplate;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.core.web.util.EncodeUtil;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.framework.jit84j.web.utils.MailMessageUtils;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.user.business.userinfo.EmailUrlBusiness;
import org.jit8.user.business.userinfo.RoleBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.jit8.user.service.userinfo.RoleService;
import org.jit8.user.service.userinfo.UserPermissionService;
import org.jit8.user.service.userinfo.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userBusiness")
public class UserBusinessImpl implements UserBusiness{

	private static final Logger LOGGER = LoggerFactory.getLogger(UserBusinessImpl.class);
	@Resource
	private UserService userService;

	@Resource
	private RoleService roleService;
	
	@Resource
	private RoleBusiness roleBusiness;
	
	@Resource
	private UserPermissionService userPermissionService;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;
	
	@Resource
	private MailMessageBusiness mailMessageBusiness;
	
	@Resource
	private EmailUrlBusiness emailUrlBusiness;
	
	@Resource
	private FileInfoBusiness fileInfoBusiness;
	
	public User registerUser(User user){
		user.setUserType(User.USER_TYPE_REGISTER);
		user.setPassword(EncodeUtil.customEncrypt(EncodeUtil.DEFAULT_MD5_ENCRYPT, user.getPassword(), user.getEmail(), 2));
		user.setConfirmPassword(EncodeUtil.customEncrypt(EncodeUtil.DEFAULT_MD5_ENCRYPT, user.getPassword(), user.getEmail(), 2));
		user = userService.save(user);
		
		String realPath = mailMessageBusiness.getDefaultTemplatePath();
		MailMessage mailMessage = MailMessageUtils.getMailMessage(realPath, "active_user.ftl","active_user");
		String activeUrl = KylinboyCoreDeveloperConstant.KYLINBOY_USER_ACTIVE_EMAIL_UI_MAPPING;
		try {
			sendActiveUserEmail(activeUrl, user, mailMessage);
		} catch (BusinessException e) {
			e.printStackTrace();
			LOGGER.error("发送邮箱失败：",e);
		}
		return user;
	}
	
	@Transactional(readOnly=true)
	public User findUserByUsername(String username){

		return userService.findUserByUsername(username);
	}

	@Override
	public boolean exitUserByUsername(String username) {
		return userService.exitUserByUsername(username);
	}

	@Override
	@Transactional(readOnly=true)
	public User findUserByEmail(String email) {
		User user = userService.findUserByEmail(email);
		if(user == null){
			return null;
		}
		List<UserPermission> userPermissionList = user.getPermissionList();
	  	  if(null != userPermissionList && userPermissionList.size()>0){
	  		  for(UserPermission userPermission : userPermissionList){
	  			userPermission.getPermission().getCode();
	  		  }
	  	  }
	  	List<UserRole> roleList = user.getRoleList();
	    if(null!=roleList && roleList.size()>0){  
	    	for(UserRole ur : roleList){
	    		ur.getRole().getRoleCode();
	    	}
	    }
		return user;
	}

	@Override
	public boolean exitUserByEmail(String email) {
		return userService.exitUserByEmail(email);
	}
	
	@Override
	public User findUserByEmailAndPassword(String email,String password) {
		User user = userService.findUserByEmailAndPassword(email,password);
		if(null != user){
			List<UserRole> userRoleList = user.getRoleList();
	        List<Role> roles = new ArrayList<Role>();
	        user.setRoles(roles);
	        for(UserRole userRole : userRoleList){
	      	  roles.add(userRole.getRole());
	        }
		}
		return user;
	}

	@Override
	public User persist(User user) {
		return userService.saveAndFlush(user);
	}
	
	@Transactional
	@Override
	public User activeUser(User user,EmailUrl emailUrl){
		User userExist = userService.findUserByEmail(user.getEmail());
		if(null != userExist){
			userExist.setActived(true);
			userService.save(userExist);
			emailUrl.setActived(true);
			emailUrl.setActiveTime(new Date());
			emailUrl.setUpdateDate(new Date());
			emailUrl.setUpdateUser(userExist.getUniqueIdentifier());
			emailUrlBusiness.persist(emailUrl);
		}
		
		String realPath = mailMessageBusiness.getDefaultTemplatePath();
		MailMessage mailMessage = MailMessageUtils.getMailMessage(realPath, "active_user_success.ftl","active_user_success");
		String loginUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING;
		try {
			sendActiveUserSuccessEmail(loginUrl, user, mailMessage);
		} catch (BusinessException e) {
			e.printStackTrace();
			LOGGER.error("发送邮箱失败：",e);
		}
		
		return userExist;
	}

	@Override
	public Page<User> getUserList(Pageable pageable) {
		return userService.getUserList(pageable);
	}

	@Transactional
	@Override
	public void addUser(User user) {
		
		User currentUser = user.getCurrentUser();
		List<Role> roleList = currentUser.getRoles();//userRoleService.findUserRoleList(userId);
		int maxLevelRoleLevel = roleBusiness.getMaxRoleLevel(roleList);
		List<Long> roleIdList = user.getRoleIdList();
		
		if(null != roleIdList && roleIdList.size()>0){        
			List<Role> roleAsignedList = roleService.getRoleListByRoleIdList(roleIdList);
			List<UserRole> newUserRoleList = new ArrayList<UserRole>();
			user.setRoleList(newUserRoleList);
			for(Role role : roleAsignedList){
				if(role.getRoleLevel()<=maxLevelRoleLevel){
					UserRole newUserRole = new UserRole();
					newUserRole.setRole(role);
					newUserRole.setUser(user);
					newUserRoleList.add(newUserRole);
				}
			}
		}
		
		//加密密码
		user.setPassword( EncodeUtil.customEncrypt(EncodeUtil.DEFAULT_MD5_ENCRYPT, user.getPassword(), user.getUsername(), 2));
		userService.save(user);
	}

	@Override
	public User findUserById(Long id) {
		User user=userService.findOne(id);
		return user;
	}

	@Override
	@Transactional
	public Long saveUpdte(User user) {
		User rUser=new User();
		if(null != user.getId() && user.getId() >0){
			 rUser = userService.findOne(user.getId());
			 rUser.setUsername(user.getUsername());
			 rUser.setEmail(user.getEmail());
			 rUser.setFirstname(user.getFirstname());
			 rUser.setLastname(user.getLastname());
			 rUser.setComeLocation(user.getComeLocation());
			 rUser.setOwnRelation(user.getOwnRelation());
			 userService.save(rUser);
		}else{
			rUser = userService.save(user);
			
		}
		Long id= rUser.getId();
		return id;
	}

	@Override
	@Transactional
	public void deleteUserById(Long id) {
		User rUser=userService.findOne(id);
		rUser.setDeleted(true);
		userService.save(rUser);
	}
	
	@Transactional
	@Override
	public void asignPermissionForUser(User user) {
		Long userId = user.getId();
		if(userId != null && userId > 0){
			
			String permissionIds = user.getPermissionIds();
			if(StringUtils.isNotEmpty(permissionIds)){
				User userTem = new User();
				userTem.setId(userId);
				//List<Long> permissionIdList = new ArrayList<Long>();
				List<UserPermission> userPermissionList = new ArrayList<UserPermission>();
				String[] permissionIdArray = permissionIds.split(",");
				for(String permissionId : permissionIdArray){
					if(StringUtils.isNotEmpty(permissionId)){
						Long permissionIdTemp = Long.valueOf(permissionId.trim());
						//permissionIdList.add(permissionIdTemp);
						UserPermission userPermission = new UserPermission();
						userPermissionList.add(userPermission);
						Permission permission = new Permission();
						userPermission.setUser(userTem);
						userPermission.setPermission(permission);
						permission.setId(permissionIdTemp);
						userPermission.setPermission(permission);
					}
						
				}
				if(userPermissionList.size()>0){
					userPermissionService.save(userPermissionList);
				}
				
			}
		}
	}

	@Override
	public Page<User> getUserList(User user, Pageable pageable) {
		return userService.getUserList(user, pageable);
	}

	@Transactional
	@Override
	public void removePermissionForUser(User user) {
		Long userId = user.getId();
		if(null != userId && userId > 0){
			User userExist = userService.findOne(userId);
			if(null != userExist){
				
				String permissionIds = user.getPermissionIds();
				String[] permissionIdArray = null;
				if(StringUtils.isNotEmpty(permissionIds)){
					permissionIdArray = permissionIds.split(",");
					
				}
				
				List<UserPermission> permissionList = userExist.getPermissionList();
				if(null != permissionList && permissionList.size()>0){
					Map<String,UserPermission> map = new HashMap<String,UserPermission>();
					
					for(UserPermission userPermission : permissionList){
						Permission permission = userPermission.getPermission();
						if(null != permission){
							map.put(permission.getId().toString(),userPermission);
						}
					}
					
					List<UserPermission> removedList = new ArrayList<UserPermission>();
					if (null != permissionIdArray
							&& permissionIdArray.length > 0) {
						for(String permissionId : permissionIdArray){
							UserPermission userPermission = map.get(permissionId);
							if(null != userPermission){
								removedList.add(userPermission);
							}
						}
						if(removedList.size()>0){
							userPermissionService.delete(removedList);
						}
					}
				}
			}
		}
		
	}

	@Transactional
	@Override
	public void removeAllPermissionForUser(User user) {
		Long userId = user.getId();
		if(null != userId && userId > 0){
			User userExist = userService.findOne(userId);
			if(null != userExist){
				userPermissionService.delete(userExist.getPermissionList());
			}
		}
	}
	
	
	
	public boolean findBackPassword(String clickUrl,User user,MailMessage mailMessage) throws BusinessException{
		
		String email = user.getEmail();
		
		 /*获取或创建一个模版*/  
		String subject = MailTemplate.TEMPLATE_NAME_FIND_PASSWORD;
        String path = MailTemplate.TEMPLATE_PATH_FIND_PASSWORD;
       
        
        String urlCode = EncodeUtil.encoderByMd5With16Hex(EncodeUtil.encode(email + System.currentTimeMillis() + MailTemplate.TEMPLATE_NAME_FIND_PASSWORD, "utf-8"));
        
        
        StringBuilder url = new StringBuilder();
        SiteInfo mainSite = (SiteInfo)StaticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.SITE_INFO_MAIN_SITE);
        String siteUrl = mainSite.getSiteUrl();
        if(StringUtils.isEmpty(clickUrl)){
        	clickUrl=KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_UI_MAPPING;//默认使用前台重置页面
        }
        url.append(siteUrl).append(clickUrl);
        url.append("/").append(urlCode);
        
        
        EmailUrl emailUrl = new EmailUrl();
        Date applyTime = new Date();
        emailUrl.setApplyTime(new Date());
        Date expireTime = CalendarUtils.getBeforeOrAfterDateForDay(applyTime, 2);//2天后链接失效
        emailUrl.setExpireTime(expireTime);
        emailUrl.setUrl(url.toString());
        emailUrl.setUrlCode(urlCode);
        emailUrl.setReciever(email);
        emailUrlBusiness.persist(emailUrl);
        
        Map<String,Object> root = new HashMap<String,Object>();  
        root.put("emailUrl", emailUrl);  
        //MailMessage mailMessage = null;
		try {
			
			mailMessage.setTemplatePath(path);//未来可移除,TODO
        	mailMessage.setSubject(subject);//未来可移除,TODO
        	mailMessage.setFileName("find_password");//未来可移除,TODO
        	String templateKey = "";//即：将通过key来查找相应模版，并找到模版的path,subject,fileName，替代上面写死的做法
        	
        	mailMessage.setRoot(root);//设置渲染模板所需数据
        	mailMessage.setReciever(email);//设置收件人
        	mailMessage.setTemplateKey(templateKey);//设置所要调用的模板key
        	
        	mailMessage = mailMessageBusiness.  sendMessage( mailMessage);
			
			
        	//mailMessage = mailMessageBusiness.sendMessage(subject,path, root, reciever);
		} catch (BusinessException e) {
			LOGGER.error(e.getCode(),e);
			throw e;
		}
		return mailMessage.isResult();
	}
	
	public boolean resetPassword(User user,EmailUrl emailUrl, MailMessage mailMessage) throws BusinessException{
		String confirmPassword = user.getConfirmPassword();
		String password = user.getPassword();
		String email = user.getEmail();
		
		if(StringUtils.isNotEmpty(email)
				&& StringUtils.isNotEmpty(password)
				&& StringUtils.isNotEmpty(confirmPassword)
				&& password.equals(confirmPassword)){
			User userExist = userService.findUserByEmail(email);
			if(null != userExist){
				userExist.setPassword(EncodeUtil.encoderByMd5With16Hex(password));
				userService.save(userExist);
				
				if (null != emailUrl) {
					String urlCode = emailUrl.getUrlCode();
					if (StringUtils.isNotEmpty(urlCode)) {
						EmailUrl emailUrlExist = emailUrlBusiness
								.findByUrlCode(urlCode);
						emailUrlExist.setActiveTime(new Date());
						emailUrlExist.setActived(true);
					}
				}
				
				String subject = MailTemplate.TEMPLATE_NAME_RESET_PASSWORD_SUCCESS;
		        String path = MailTemplate.TEMPLATE_PATH_RESET_PASSWORD_SUCCESS;
		        
		        StringBuilder url = new StringBuilder();
		        SiteInfo mainSite = (SiteInfo)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.SITE_INFO_MAIN_SITE);
		        String siteUrl = mainSite.getSiteUrl();
		        url.append(siteUrl).append(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING);
		        
		        Map<String,Object> root = new HashMap<String,Object>();  
		        root.put("user", user); 
		        root.put("loginPath", url.toString());
		        
		        //key, root, reciever, sender, realpath
		        try{
		        	mailMessage.setTemplatePath(path);//未来可移除,TODO
		        	mailMessage.setSubject(subject);//未来可移除,TODO
		        	mailMessage.setFileName("reset_password_success");//未来可移除,TODO
		        	String templateKey = "";
		        	mailMessage.setRoot(root);//设置渲染模板所需数据
		        	mailMessage.setReciever(email);//设置收件人
		        	mailMessage.setTemplateKey(templateKey);//设置所要调用的模板key
		        	
		        	//mailMessage = mailMessageBusiness.sendMessage(subject,path, root, email);
		        	mailMessage = mailMessageBusiness.  sendMessage( mailMessage);
		        }catch(BusinessException e){
		        	throw e;
		        }
				
				return true;
			}else{
				
				throw new BusinessException("invalid.email", "不存在注册邮箱为【"+ email +"】的用户");
			}
		}
		return false;
	}
	
	public boolean resetPassword(User user,MailMessage mailMessage) throws BusinessException{
		String confirmPassword = user.getConfirmPassword();
		String password = user.getPassword();
		String email = user.getEmail();
		
		if(StringUtils.isNotEmpty(email)
				&& StringUtils.isNotEmpty(password)
				&& StringUtils.isNotEmpty(confirmPassword)
				&& password.equals(confirmPassword)){
			User userExist = userService.findUserByEmail(email);
			if(null != userExist){
				userExist.setPassword(EncodeUtil.encoderByMd5With16Hex(password));
				userService.save(userExist);
				
				String subject = MailTemplate.TEMPLATE_NAME_RESET_PASSWORD_BY_SYS;
		        String path = MailTemplate.TEMPLATE_PATH_RESET_PASSWORD_BY_SYS;
		        
		        StringBuilder url = new StringBuilder();
		        SiteInfo mainSite = (SiteInfo)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.SITE_INFO_MAIN_SITE);
		        String siteUrl = mainSite.getSiteUrl();
		        url.append(siteUrl).append(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING);
		        
		        
		        Map<String,Object> root = new HashMap<String,Object>();  
		        root.put("user", user); 
		        root.put("loginPath", url.toString());
		        
		        //key, root, reciever, sender, realpath
		        try{
		        	mailMessage.setTemplatePath(path);//未来可移除,TODO
		        	mailMessage.setSubject(subject);//未来可移除,TODO
		        	mailMessage.setFileName("reset_password");//未来可移除,TODO
		        	String templateKey = "";
		        	mailMessage.setRoot(root);//设置渲染模板所需数据
		        	mailMessage.setReciever(email);//设置收件人
		        	mailMessage.setTemplateKey(templateKey);//设置所要调用的模板key
		        	
		        	//mailMessage = mailMessageBusiness.sendMessage(subject,path, root, email);
		        	mailMessage = mailMessageBusiness.  sendMessage( mailMessage);
		        }catch(BusinessException e){
		        	throw e;
		        }
				
				
				return true;
			}else{
				
				throw new BusinessException("invalid.email", "不存在注册邮箱为【"+ email +"】的用户");
			}
		}
		return false;
	}
	
	private String printContent(InputStream is) throws IOException {  
	      BufferedReader br = new BufferedReader(new InputStreamReader(is));  
	      String line;  
	      StringBuilder result= new StringBuilder();
	      while ((line=br.readLine()) != null) {  
	         System.out.println(line); 
	         result.append(line);
	      }  
	      if (is != null) {  
	         is.close();  
	      }  
	      if (br != null) {  
	         br.close();  
	      }  
	      return result.toString();
	   }  
	
	public static void main(String[] args) {
		String value = "令人头疼的英文单词在我眼前晃悠，忽地一下子黑暗笼罩下来，周围发出一片“啊”的惊呼声，我的心里也咯噔震颤了一下，随着那些蚂蚁一样的拉丁字母忽然消失，心情一下爽朗起来，啊哈！终于可以闭会眼睛休息了。周围的同学已经离开座位，去外面玩耍，我还是安静地坐在座位上，在黑暗中半喜半忧地思考着自己的路途。有一阵欢喜，有一阵沮丧，这种类似于掩耳盗铃一样的欢喜实在只能图个一时之快活。该面对的还是要面对，我的小心脏呯呯地跳着，我听见心底的声音，攻下每一个难题，还有出口成章的幻想，想往着旅行世界各地，与不同种族的人毫无障碍地沟通交流的情景，新的希望与动力逐渐凝聚，心里更增添了一分毅力！";
		System.out.println(value.length());
	}

	@Override
	public Page<User> getUserListByUserIdNotNull(User user, Pageable pageable) {
		Page<User> page = userService.getUserListByUserIdNotNull(user, pageable);
		return page;
	}

	@Override
	public Page<User> getLastUserList(Pageable pageable) {
		Page<User> page = userService.getLastUserList(pageable);
		return page;
	}
	
	
	public boolean sendActiveUserEmail(String clickUrl,User user,MailMessage mailMessage) throws BusinessException{
		
		
		String email = user.getEmail();
		
		 /*获取或创建一个模版*/  
		String subject = MailTemplate.TEMPLATE_NAME_ACTIVE_USER;
        String path = MailTemplate.TEMPLATE_PATH_ACTIVE_USER;
        String emailStoredPath = fileInfoBusiness.getEmailStoredPath();
        mailMessage.setFileAccessPath(emailStoredPath);
        
        String urlCode = EncodeUtil.encoderByMd5With16Hex(EncodeUtil.encode(email + System.currentTimeMillis() + MailTemplate.TEMPLATE_PATH_ACTIVE_USER, "utf-8"));
        
        
        StringBuilder url = new StringBuilder();
        SiteInfo mainSite = (SiteInfo)StaticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.SITE_INFO_MAIN_SITE);
        String siteUrl = mainSite.getSiteUrl();
        if(StringUtils.isEmpty(clickUrl)){
        	return false;
        }
        url.append(siteUrl).append(clickUrl);
        url.append("/").append(urlCode);
        
        
        EmailUrl emailUrl = new EmailUrl();
        Date applyTime = new Date();
        emailUrl.setApplyTime(new Date());
        Date expireTime = CalendarUtils.getBeforeOrAfterDateForDay(applyTime, 1000);//1000天后链接失效
        emailUrl.setExpireTime(expireTime);
        emailUrl.setUrl(url.toString());
        emailUrl.setUrlCode(urlCode);
        emailUrl.setReciever(email);
        emailUrlBusiness.persist(emailUrl);
        
        Map<String,Object> root = new HashMap<String,Object>();  
        root.put("emailUrl", emailUrl);  
        //MailMessage mailMessage = null;
		try {
			
			mailMessage.setTemplatePath(path);//未来可移除,TODO
        	mailMessage.setSubject(subject);//未来可移除,TODO
        	mailMessage.setFileName("active_user");//未来可移除,TODO
        	String templateKey = "";//即：将通过key来查找相应模版，并找到模版的path,subject,fileName，替代上面写死的做法
        	
        	mailMessage.setRoot(root);//设置渲染模板所需数据
        	mailMessage.setReciever(email);//设置收件人
        	mailMessage.setTemplateKey(templateKey);//设置所要调用的模板key
        	
        	mailMessage = mailMessageBusiness.sendMessage(mailMessage);
			
			
        	//mailMessage = mailMessageBusiness.sendMessage(subject,path, root, reciever);
		} catch (BusinessException e) {
			LOGGER.error(e.getCode(),e);
			throw e;
		}
		return mailMessage.isResult();
	}
	
	public boolean sendActiveUserSuccessEmail(String clickUrl,User user,MailMessage mailMessage) throws BusinessException{
		
		//user = activeUser(user);
		
		String email = user.getEmail();
		
		 /*获取或创建一个模版*/  
		String subject = MailTemplate.TEMPLATE_NAME_ACTIVE_USER_SUCCESS;
        String path = MailTemplate.TEMPLATE_PATH_ACTIVE_USER_SUCCESS;
       
        
        String urlCode = EncodeUtil.encoderByMd5With16Hex(EncodeUtil.encode(email + System.currentTimeMillis() + MailTemplate.TEMPLATE_NAME_FIND_PASSWORD, "utf-8"));
        
        
        StringBuilder url = new StringBuilder();
        SiteInfo mainSite = (SiteInfo)StaticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.SITE_INFO_MAIN_SITE);
        String siteUrl = mainSite.getSiteUrl();
        if(StringUtils.isEmpty(clickUrl)){
        	return false;
        }
        url.append(siteUrl).append(clickUrl);
        //url.append("/").append(urlCode);
        
        
        EmailUrl emailUrl = new EmailUrl();
        Date applyTime = new Date();
        emailUrl.setApplyTime(new Date());
        Date expireTime = CalendarUtils.getBeforeOrAfterDateForDay(applyTime, 1000);//1000天后链接失效
        emailUrl.setExpireTime(expireTime);
        emailUrl.setUrl(url.toString());
        emailUrl.setUrlCode(urlCode);
        emailUrl.setReciever(email);
        emailUrl.setCreateDate(new Date());
        if(null != user){
        	emailUrl.setCreateUser(user.getUniqueIdentifier()==null?0l:user.getUniqueIdentifier());
        }
        emailUrlBusiness.persist(emailUrl);
        
        Map<String,Object> root = new HashMap<String,Object>();  
        root.put("emailUrl", emailUrl);  
        root.put("user", user);
        //MailMessage mailMessage = null;
		try {
			
			mailMessage.setTemplatePath(path);//未来可移除,TODO
        	mailMessage.setSubject(subject);//未来可移除,TODO
        	mailMessage.setFileName("active_user_success");//未来可移除,TODO
        	String templateKey = "";//即：将通过key来查找相应模版，并找到模版的path,subject,fileName，替代上面写死的做法
        	
        	mailMessage.setRoot(root);//设置渲染模板所需数据
        	mailMessage.setReciever(email);//设置收件人
        	mailMessage.setTemplateKey(templateKey);//设置所要调用的模板key
        	
        	mailMessage = mailMessageBusiness.sendMessage(mailMessage);
			
		} catch (BusinessException e) {
			LOGGER.error(e.getCode(),e);
			throw e;
		}
		return mailMessage.isResult();
	}
	
	
}
