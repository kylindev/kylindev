package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface StickyNoteCategorySpecialService extends GenericService<StickyNoteCategorySpecial, Long>{

	public List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList(boolean disable, boolean deleted);
	
	public Page<StickyNoteCategorySpecial> findAll(StickyNoteCategorySpecial stickyNoteCategorySpecial,
			Pageable pageable);
	
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList);
}
