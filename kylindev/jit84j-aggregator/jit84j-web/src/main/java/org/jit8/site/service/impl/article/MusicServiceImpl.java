package org.jit8.site.service.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.article.MusicDao;
import org.jit8.site.persist.domain.article.Music;
import org.jit8.site.service.article.MusicService;
import org.springframework.stereotype.Service;

@Service
public class MusicServiceImpl extends GenericServiceImpl<Music, Long> implements MusicService {

	@Resource
	private MusicDao musicDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = musicDao;
	}


	@Override
	public List<Music> getMusicList(boolean disable, boolean deleted) {
		return musicDao.getMusicList(disable, deleted);
	}
	
}
