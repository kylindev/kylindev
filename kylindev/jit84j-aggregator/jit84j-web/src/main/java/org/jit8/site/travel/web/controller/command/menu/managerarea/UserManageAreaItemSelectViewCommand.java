package org.jit8.site.travel.web.controller.command.menu.managerarea;

import java.io.Serializable;

public class UserManageAreaItemSelectViewCommand implements Serializable{

	private static final long serialVersionUID = 1316222392793470370L;

	private ManageAreaItemCommand manageAreaItem;
	
	private String iconPath;//可以个性化图标
	
	private boolean hideable;//隐藏
	
	private boolean display;//显示
	
	private Long userId;//用户id
	
	private String description;//描述
	
	private String remark;//备注，用于当禁用某用户菜单项时，给出原因，可显示在用户的菜单title中
	
	private Long id;
	
	private boolean checked;//选择
	
	private Long manageAreaItemId;

	/**
	 * @return the iconPath
	 */
	public String getIconPath() {
		return iconPath;
	}

	/**
	 * @param iconPath the iconPath to set
	 */
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}

	/**
	 * @return the hideable
	 */
	public boolean isHideable() {
		return hideable;
	}

	/**
	 * @param hideable the hideable to set
	 */
	public void setHideable(boolean hideable) {
		this.hideable = hideable;
	}

	/**
	 * @return the display
	 */
	public boolean isDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(boolean display) {
		this.display = display;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the remark
	 */
	public String getRemark() {
		return remark;
	}

	/**
	 * @param remark the remark to set
	 */
	public void setRemark(String remark) {
		this.remark = remark;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the checked
	 */
	public boolean isChecked() {
		return checked;
	}

	/**
	 * @param checked the checked to set
	 */
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the manageAreaItemId
	 */
	public Long getManageAreaItemId() {
		return manageAreaItemId;
	}

	/**
	 * @param manageAreaItemId the manageAreaItemId to set
	 */
	public void setManageAreaItemId(Long manageAreaItemId) {
		this.manageAreaItemId = manageAreaItemId;
	}

	/**
	 * @return the manageAreaItem
	 */
	public ManageAreaItemCommand getManageAreaItem() {
		return manageAreaItem;
	}

	/**
	 * @param manageAreaItem the manageAreaItem to set
	 */
	public void setManageAreaItem(ManageAreaItemCommand manageAreaItem) {
		this.manageAreaItem = manageAreaItem;
	}
}
