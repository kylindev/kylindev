package org.jit8.site.travel.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.ActivityBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.Activity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ActivityController{

	private static final Logger logger = LoggerFactory.getLogger(ActivityController.class);
	
	
	@Resource
	private ActivityBusiness activityBusiness;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_ACTIVITY_LIST_UI_MAPPING)
	public String activityListUI(@ModelAttribute("activity") Activity activity,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_ACTIVITY_LIST_UI_MAPPING + " #########");
		Long id = activity.getId();
		if(id != null && id > 0){
			activity = activityBusiness.getActivityById(id);
		}
		
		Long userId = Jit8ShareInfoUtils.getUserId();
		
		Page<Activity> page = null;
		
		if(null != userId && userId>0){
			Pageable pageable2 = new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),Direction.DESC,"createDate");
			page = activityBusiness.findByUserId(userId, pageable2);
		}
		model.addAttribute("page", page);
		
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_ACTIVITY_LIST_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("activity", activity);
		return "activityListUI";
	}
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING)
	public String activityEdit2UI(@ModelAttribute("activity") Activity activity,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING + " #########");
		Long id = activity.getId();
		if(id != null && id > 0){
			activity = activityBusiness.getActivityById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("activity", activity);
		return "activityEdit2UI";
	}
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING)
	public String activityEdit3UI(@ModelAttribute("activity") Activity activity,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING + " #########");
		Long id = activity.getId();
		if(id != null && id > 0){
			activity = activityBusiness.getActivityById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("activity", activity);
		return "activityEdit2UI";
	}

	
}
