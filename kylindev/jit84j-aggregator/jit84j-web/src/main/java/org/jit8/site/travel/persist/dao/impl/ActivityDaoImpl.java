package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.ActivityDao;
import org.jit8.site.travel.persist.domain.Activity;
import org.jit8.site.travel.persist.repository.ActivityRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class ActivityDaoImpl extends GenericDaoImpl<Activity, Long> implements ActivityDao {

	@Resource
	private ActivityRepository activityRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = activityRepository;
	}

	@Override
	public List<Activity> getActivityList(boolean disable, boolean deleted) {
		return activityRepository.getActivityList(disable, deleted);
	}
	
	@Override
	public Activity findByCode(String code) {
		return activityRepository.findByTitle(code);
	}

	@Override
	public List<Activity> findByIds(List<Long> idList) {
		return activityRepository.findByIds(idList);
	}

	@Override
	public Page<Activity> findByUserId(Long userId, Pageable pageable) {
		return activityRepository.findByUserId( userId,  pageable);
	}
}
