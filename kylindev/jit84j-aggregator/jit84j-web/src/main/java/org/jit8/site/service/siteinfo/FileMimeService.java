package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface FileMimeService extends GenericService<FileMime, Long>{

	public List<FileMime> getFileMimeList(boolean disable, boolean deleted);
	
	public FileMime findByMime(String mime);
	
	public List<FileMime> findByIdList(List<Long> idList);
	
	public List<FileMime> getFileMimeListByAllowable(boolean allowable);
	
	public Page<FileMime> findAll(FileMime fileMime, Pageable pageable);
}
