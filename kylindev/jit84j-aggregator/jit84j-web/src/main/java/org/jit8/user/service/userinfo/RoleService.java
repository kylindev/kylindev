package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleService extends GenericService<Role, Long>{

	List<Role> getRoleListByRoleLevel(int roleLevel);
	
	List<Role> getRoleListByRoleIdList(List<Long> roleIdList);
	
	public Page<Role> getRoleList(Pageable pageable);
	
	public List<Role> saveRoleList(List<Role> roleList);
	
	public Page<Role> getRoleList(Role role, Pageable pageable);
}
