package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;


public interface StickyNoteConfigService extends GenericService<StickyNoteConfig, Long>{

	public List<StickyNoteConfig> getStickyNoteConfigList(boolean disable, boolean deleted);
}
