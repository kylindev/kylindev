package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InstantShareCommentRepository extends GenericJpaRepository<InstantShareComment, Long>{

	
	@Query("from InstantShareComment o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<InstantShareComment> getInstantShareCommentList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from InstantShareComment o where o.nickName=:nickName")
	public InstantShareComment findByNickName(@Param("nickName")String nickName);
	
	@Query("from InstantShareComment o where o.uniqueIdentifier in :idList")
	public List<InstantShareComment> findByIds(@Param("idList")List<Long> idList);
	
	@Query("from InstantShareComment o where o.instantShare.uniqueIdentifier = :instantShareId and o.deleted=false")
	public Page<InstantShareComment> findByInstantShareId(@Param("instantShareId")Long instantShareId, Pageable pageable);
}
