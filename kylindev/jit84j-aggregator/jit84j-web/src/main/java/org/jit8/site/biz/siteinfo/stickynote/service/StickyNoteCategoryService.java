package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;


public interface StickyNoteCategoryService extends GenericService<StickyNoteCategory, Long>{

	public List<StickyNoteCategory> getStickyNoteCategoryList(boolean disable, boolean deleted);
	
	public StickyNoteCategory findByCode(String code);
	
	public List<StickyNoteCategory> findByIds(List<Long> idList);
}
