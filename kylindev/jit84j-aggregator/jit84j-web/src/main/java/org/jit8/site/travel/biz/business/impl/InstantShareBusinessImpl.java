package org.jit8.site.travel.biz.business.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.core.web.util.ThreadLocalShareInfo;
import org.jit8.site.travel.biz.business.InstantShareBusiness;
import org.jit8.site.travel.biz.business.InstantShareImageCounterBusiness;
import org.jit8.site.travel.biz.service.InstantShareService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("instantShareBusiness")
public class InstantShareBusinessImpl implements InstantShareBusiness{

	@Resource
	private InstantShareService instantShareService;
	
	@Resource
	private InstantShareImageCounterBusiness instantShareImageCounterBusiness;
	

	@Override
	public List<InstantShare> getInstantShareList() {
		return instantShareService.getInstantShareList(false, false);
	}
	
	@Transactional
	@Override
	public InstantShare persist(InstantShare instantShare){
		Long id = instantShare.getId();
		InstantShare exist = null;
		Date publishDate = null;
			
		if(id != null && id>0){
			 exist = instantShareService.findOne(instantShare.getId());
			 exist.setDescription(instantShare.getDescription());
			 exist.setImagePath(instantShare.getImagePath());
			 exist.setSequence(instantShare.getSequence());
			 exist.setTitle(instantShare.getTitle());
			 publishDate = exist.getPublishDate();
			if(null == publishDate){
				publishDate=new Date();
				 exist.setPublishDate(publishDate);
			 }
			 
			 //处理图片
			 List<InstantShareImage> imageList = exist.getImageList();
			 
			 List<InstantShareImage> newImageList = new ArrayList<InstantShareImage>();
			 Map<Long, InstantShareImage> imageMap = new HashMap<Long, InstantShareImage>();
			 for(InstantShareImage image : imageList){
				 imageMap.put(image.getId(),image);
			 }
			 
			 List<InstantShareImage> submitImageList = instantShare.getImageList();
			 if(null != submitImageList){
				 for(InstantShareImage image : submitImageList){
					 InstantShareImage existImage = imageMap.get(image.getId());
					 if(null != existImage){
						 existImage.setImageUrl(image.getImageUrl());
						 existImage.setThumbnailUrl(image.getThumbnailUrl());
						 existImage.setTitle(image.getTitle());
						 existImage.setDescription(image.getDescription());
						 existImage.setSequence(image.getSequence());
					 }else{
						 existImage = image;
//						 User user = Jit8ShareInfoUtils.getUser();
//						 existImage.setUser(user);
//						 existImage.setUserId(Jit8ShareInfoUtils.getUserId());
					 }
					 existImage.setInstantShare(exist);
					 newImageList.add(existImage);
				 }
			 }
			 
			 exist.setImageList(newImageList);
			 
			 
		}else{
			exist=instantShare;
			if(null == publishDate){
				publishDate = new Date();
			}
			exist.setPublishDate(publishDate);
		}
		setTimeForList(exist,publishDate);
		
		return instantShareService.save(exist);
	}
	
	private void setTimeForList(InstantShare instantShare, Date publishDate){
		if(null != instantShare){
			User user = Jit8ShareInfoUtils.getUser();
			List<InstantShareImage> imageList = instantShare.getImageList();
			Long userId = Jit8ShareInfoUtils.getUserId();
			int imageCount = 0;
			if (null != imageList) {
				imageCount = imageList.size();
				for (InstantShareImage image : imageList) {
					Date date = publishDate;
					if (date == null) {
						date = new Date();
					}

					InstantShareImage entity2 = image;
					Date existPublishDate = entity2.getPublishDate();
					if (null == existPublishDate) {
						entity2.setPublishDate(publishDate);
					}
					ThreadLocalShareInfo shareInfo = Jit8ShareInfoUtils.getShareInfo();
					if (entity2.getUniqueIdentifier() == null || entity2.getUniqueIdentifier() <= 0 || entity2.getCreateDate() == null) {
						entity2.setCreateDate(date);
						if (null != shareInfo) {
							entity2.setCreateUser(shareInfo.getCurrentUserOid());
						}
					}
					Date updateDate = new Date();
					entity2.setUpdateDate(updateDate);
					if (null != shareInfo) {
						entity2.setUpdateUser(shareInfo.getCurrentUserOid());
					}

					entity2.setUser(user);
					entity2.setUserId(userId);
					
					image.setInstantShare(instantShare);
				}
			}
			
			instantShare.setUser(user);
			instantShare.setUserId(userId);
			
			Integer existImageCount = instantShare.getImageCount();
			if(null == existImageCount){
				existImageCount = 0;
			}
			int minus = imageCount - existImageCount;
			instantShare.setImageCount(imageCount);
			instantShare.setNickName(user.getNickname());
			
			fillPublishPlace(instantShare);
			
			//更新当天用户照片数 
			updateImageCounter(publishDate, userId, minus);
			
		}
	}

	/**
	 * @param publishDate
	 * @param userId
	 * @param minus
	 */
	private void updateImageCounter(Date publishDate, Long userId, int minus) {
		instantShareImageCounterBusiness.addCountByDayAndUserId(minus,publishDate, userId);
	}

	/**
	 * @param instantShare
	 */
	private void fillPublishPlace(InstantShare instantShare) {
		String place = instantShare.getPlace();
		if(StringUtils.isEmpty(place)){
			String city = instantShare.getCity();
			place = city;
			if(StringUtils.isEmpty(place)){
				String province = instantShare.getProvince();
				place = province;
			}
		}
		instantShare.setPlace(place);
	}

	@Override
	public InstantShare getInstantShareById(Long id) {
		getThreeInstantSharesById( id);
		return instantShareService.findOne(id);
	}
	/**
	 * key: current, previous，following
	 * @param id
	 * @return
	 */
	public Map<String, InstantShare> getThreeInstantSharesById(Long id){
		
		Pageable commentPage = new PageRequest(0, 10, Direction.DESC, "publishDate");
		Pageable imagePage = new PageRequest(0, 10, Direction.ASC, "sequence");
		
		InstantShare current = instantShareService.findOneWithPageable(id,commentPage,imagePage);
		
		//select max(id) from t where t.id< id;
		//select min(id) from t where t.id> id;
		
		Long previousId = instantShareService.findPreviousOneByCurrentId(id);
		InstantShare previous = null;
		if(null != previousId && previousId>0){
			previous = instantShareService.findSimpleOneById(previousId);
		}
		
		Long followingId = instantShareService.findFollowingOneByCurrentId(id);
		InstantShare following = null;
		if(null != followingId && followingId>0){
			following = instantShareService.findSimpleOneById(followingId);
		}
		
		Map<String, InstantShare> map = new HashMap<String, InstantShare>();
		map.put(InstantShareBusiness.INSTANTSHARE_CURRENT, current);
		map.put(InstantShareBusiness.INSTANTSHARE_PREVIOUS, previous);
		map.put(InstantShareBusiness.INSTANTSHARE_FOLLOWING, following);
		return map;
	}

	@Override
	public InstantShare deleteInstantShareById(Long id) {
		InstantShare old = instantShareService.findOne(id);
		old.setDeleted(true);
		int minus = old.getImageCount()==null?0:(0-old.getImageCount());
		updateImageCounter(old.getPublishDate(), old.getUserId(), minus);
		return instantShareService.save(old);
	}
	
	@Transactional
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			InstantShare old = instantShareService.findOne(id);
			int minus = old.getImageCount()==null?0:(0-old.getImageCount());
			updateImageCounter(old.getPublishDate(), old.getUserId(), minus);
			instantShareService.delete(old);
		}
	}

	@Override
	public Page<InstantShare> findAll(Pageable pageable) {
		return instantShareService.findAll(pageable);
	}

	@Override
	public InstantShare findByCode(String code) {
		return instantShareService.findByCode( code);
	}

	@Override
	public List<InstantShare> findByIds(List<Long> idList) {
		return instantShareService.findByIds(idList);
	}

	@Override
	public Page<InstantShare> findByUserId(Long userId, Pageable pageable) {
		if(null == userId){
			return null;
		}
		return instantShareService.findByUserId( userId, pageable);
	}

	@Transactional
	@Override
	public void deleteById(Long id) {
		if(null != id && id>0){
			InstantShare instantShare = instantShareService.findOne(id);
			instantShare.setDeleted(true);
			instantShareService.save(instantShare);
		}
	}

	
	@Transactional
	@Override
	public InstantShare praise(Long id) {
		InstantShare instantShare = instantShareService.findOne(id);
		if(null != instantShare){
			int praiseCount  = instantShare.getPraiseCount();
			instantShare.setPraiseCount(praiseCount+1);
			instantShare.setUpdateDate(new Date());
			instantShareService.save(instantShare);
		}
		return instantShare;
	}

}
