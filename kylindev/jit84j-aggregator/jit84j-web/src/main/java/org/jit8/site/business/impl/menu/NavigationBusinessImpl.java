package org.jit8.site.business.impl.menu;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.exception.constant.BusinessExceptionConstant;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.service.menu.NavigationService;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("navigationBusiness")
public class NavigationBusinessImpl implements NavigationBusiness{

	private static final Logger logger = LoggerFactory.getLogger(NavigationBusinessImpl.class);
	
	@Resource
	private NavigationService navigationService;
	
	@Resource
	private DeveloperBusiness developerBusiness;
	

	@Override
	public List<Navigation> getNavigationList() {
		return navigationService.getNavigationList(false, false);
	}
	
	@Transactional
	@Override
	public Navigation persist(Navigation navigation) throws BusinessException{
		if(null!= navigation){
			Long developerId = navigation.getDeveloperId();
			if(null == developerId){
				Developer developerAsign = navigation.getDeveloper();
				if(null != developerAsign){
					developerId = developerAsign.getId();
				}
			}
			if(null != developerId){
				Developer developer = developerBusiness.findById(developerId);
				if(null != developer){
					navigation.setDeveloper(developer);
				}else{
					throw new BusinessException(BusinessExceptionConstant.PERMISSION_DEVELOPER_REQUIRED,"require a developer");
				}
			}else{
				throw new BusinessException(BusinessExceptionConstant.PERMISSION_DEVELOPER_REQUIRED,"require a developer");
			}
			
			Navigation parent = navigation.getParent();
			List<Navigation> exist = getNavigationByCode(navigation.getCode());
			if(exist != null && exist.size()>0){
				return navigation;
			}
			if(parent==null){
				setParentTopNavigation(navigation);
			}else{
				navigation.setParent(getNavigationById(parent.getId()));
			}
		
			List<Navigation> children = navigation.getChildrenNavigation();
			if(null != children && children.size()>0){
				List<Navigation> persistChildren = new ArrayList<Navigation>();
				navigation.setChildrenNavigation(persistChildren);
				for(Navigation na : children){
					persistChildren.add(getNavigationById(na.getId()));
				}
			}
			
			if(!navigation.getHideable()){
				navigation.setDisplay(true);
			}
			navigationService.save(navigation);
		}
		return navigation;
	}

	public void setParentTopNavigation(Navigation navigation) {
		List<Navigation> topList = getNavigationByCode(NavigationService.TOP_CODE);
		Navigation top = null;
		if(topList==null || topList.size()<1){
			top = new Navigation();
			top.setCode(NavigationService.TOP_CODE);
			top.setName(NavigationService.TOP_NAME);
			top.setParent(null);
			top = navigationService.save(top);
		}else{
			top = topList.get(0);
		}
		navigation.setParent(top);
	}

	@Override
	public Navigation getNavigationById(Long id) {
		return navigationService.findOne(id);
	}

	@Override
	public Navigation deleteNavigationById(Long id) {
		Navigation old = navigationService.findOne(id);
		old.setDeleted(true);
		return navigationService.save(old);
	}
	
	@Transactional
	@Override
	public Navigation merge(Navigation navigation) throws BusinessException{
		
		Navigation old = navigationService.findOne(navigation.getId());
		Developer developer = navigation.getDeveloper();
		if(null != developer){
			Long developerId = developer.getId();
			if(null != developerId && !developerId.equals(old.getDeveloper().getId())){
				developer = developerBusiness.findById(developerId);
				if(null != developer){
					old.setDeveloper(developer);
				}else{
					throw new BusinessException(BusinessExceptionConstant.PERMISSION_DEVELOPER_REQUIRED,"require a developer");
				}
			}
		}else{
			throw new BusinessException(BusinessExceptionConstant.PERMISSION_DEVELOPER_REQUIRED,"require a developer");
		}
		
		
		List<Navigation> navigationList = navigation.getChildrenNavigation();
		List<Navigation> existNavigationList = old.getChildrenNavigation();
		if(null==existNavigationList){
			existNavigationList = new ArrayList<Navigation>();
		}
		for(Navigation existNavigation : existNavigationList){
			existNavigation.setParent(null);
		}
		
		Navigation currentParent = navigation.getParent();
		if(null != currentParent){
			Long currentParentId = currentParent.getId();
			currentParent = navigationService.findOne(currentParentId);
			navigation.setParent(currentParent);
		}
		
		Navigation parent = old.getParent();
		Long parentId = -1l;
		if(parent != null){
			parentId = parent.getId();
			if(currentParent==null){
				navigation.setParent(parent);
			}
		}
		if(null != navigationList && navigationList.size()>0){
			for(Navigation c : navigationList){
				Long id = c.getId();
				if(id != null && id>0){
					if(id.equals(parentId)){
						continue;//检测子类别不能与父类别相同，删除相同的id
					}
					Navigation navigation2 = navigationService.findOne(id);
					
					navigation2.setParent(old);
					existNavigationList.add(navigation2);
				}
			}
			navigation.setChildrenNavigation(existNavigationList);
		}
		//BeanUtils.copyProperties(navigation, old);
		old.setCode(navigation.getCode());
		old.setName(navigation.getName());
		old.setSequence(navigation.getSequence());
		old.setIconPath(navigation.getIconPath());
		old.setHideable(navigation.getHideable());
		old.setDisplay(!navigation.getHideable());
		old.setUrl(navigation.getUrl());
		old.setParent(navigation.getParent());
		old.setType(navigation.getType());
		return navigationService.save(old);
	}

	@Override
	public List<Navigation> getNavigationListByIds( String NavigationIdList) {
		if(StringUtils.isNotEmpty(NavigationIdList)){
			String[] ids = NavigationIdList.split(",");
			List<Long> NavigationIds = new ArrayList<Long>();
			for(String id : ids){
				if(StringUtils.isNotEmpty(id)){
					NavigationIds.add(Long.valueOf(id));
				}
			}
			return navigationService.getNavigationListByIds(false, false, NavigationIds);
		}
		return null;
	}

	@Override
	public List<Navigation> getNavigationListByParentNull() {
		return navigationService.getNavigationListByParentNull();
	}
	
	@Transactional
	@Override
	public List<Navigation> saveNavigationList(Navigation navigation){
		List<Navigation> persitedNavigationList = new ArrayList<Navigation>();
		if(null != navigation){
			List<Navigation> navigationList = navigation.getNavigationList();
			if(null != navigationList && navigationList.size()>0){
				for(Navigation c : navigationList){
					Long id = c.getId();
					
					if(null != id && id>0){
						Navigation current = navigationService.findOne(id);
						current.setCode(c.getCode());
						current.setName(c.getName());
						current.setUrl(c.getUrl());
						current.setSequence(c.getSequence());
						current.setHideable(c.getHideable());
						//current.setType(c.getType());
						
						Developer developer = c.getDeveloper();
						if(null != developer){
							UserId userId = developer.getUserId();
							Long developerId = userId.getUserId();
							Developer developer2 = developerBusiness.findByUserId(developerId);
							current.setDeveloper(developer2);
						}
						persitedNavigationList.add(current);
					}else{
						persitedNavigationList.add(c);
					}
					
				}
				persitedNavigationList = (List<Navigation>) navigationService.save(persitedNavigationList);
			}
		}
		return persitedNavigationList;
	}

	@Override
	public List<Navigation> getNavigationByCode(String code) {
		return navigationService.getNavigationByCode(false, false, code);
	}
	
	@Override
	public List<Navigation> getTopNavigationByCode() {
		return navigationService.getNavigationByCode(false, false, NavigationService.TOP_CODE);
	}

	@Transactional(readOnly=true)
	@Override
	public List<Navigation> getNavigationByTopCode() {
		 List<Navigation> list = navigationService.getNavigationByTopCode(false, false, NavigationService.TOP_CODE);
		//list = getDisplayNavigation(list);
		return list;
	}
	
	
	
	@Override
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId){
		return navigationService.getNavigationByCodeAndDeveloper(code,userId);
	}

	@Override
	public List<Navigation> getNavigationByParentCode(
			boolean deleted, String code) {
		if(StringUtils.isEmpty(code)){
			code = NavigationService.TOP_CODE;
		}
		return navigationService.getNavigationByParentCode( deleted, code);
	}
	
	@Transactional(readOnly=true)
	@Override
	public List<Navigation> getListByTopCode() {
		List<Navigation> list = getNavigationByTopCode( false, NavigationService.TOP_CODE);
		
		return list;
	}

	@Override
	public List<Navigation> getNavigationByTopCode(boolean disable, String code) {
		return navigationService.getNavigationByTopCode(disable, code);
	}
	
	private static List<Navigation> getDisplayNavigation(List<Navigation> navigationList){
		List<Navigation> newList = new ArrayList<Navigation>();
		if(null != navigationList && navigationList.size()>0){
			for(Navigation navigation : navigationList){
				if(!navigation.getHideable()){
					newList.add(navigation);
					List<Navigation> children = navigation.getChildrenNavigation();
					navigation.setChildrenNavigation(getDisplayNavigation(children));
				}
			}
		}
		return newList;
	}

	@Override
	public Navigation findNavigationByIdKey(String idKey) {
		if(StringUtils.isNotEmpty(idKey)){
			return navigationService.findNavigationByIdKey(idKey);
		}
		return null;
	}


}
