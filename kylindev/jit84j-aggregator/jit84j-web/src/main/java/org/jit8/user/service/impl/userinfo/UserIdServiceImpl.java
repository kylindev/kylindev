package org.jit8.user.service.impl.userinfo;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.UserIdDao;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.service.userinfo.UserIdService;
import org.springframework.stereotype.Service;

@Service
public class UserIdServiceImpl extends GenericServiceImpl<UserId, Long> implements UserIdService {

	@Resource
	private UserIdDao userIdDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = userIdDao;
	}
	
	
}
