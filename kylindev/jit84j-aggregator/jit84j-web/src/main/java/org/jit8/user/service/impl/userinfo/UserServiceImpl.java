package org.jit8.user.service.impl.userinfo;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.UserDao;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.service.userinfo.UserIdService;
import org.jit8.user.service.userinfo.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends GenericServiceImpl<User, Long> implements UserService {

	@Resource
	private UserDao userDao;
	
	@Resource
	private UserIdService uerIdService;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = userDao;
	}
	
	public User findUserByUsername(String username){
		return userDao.findUserByUsername(username);
	}

	@Override
	public boolean exitUserByUsername(String username) {
		User user = findUserByUsername(username);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public User findUserByEmail(String email) {
		return userDao.findUserByEmail(email);
	}

	@Override
	public boolean exitUserByEmail(String email) {
		User user = findUserByEmail(email);
		if(user != null){
			return true;
		}
		return false;
	}

	@Override
	public User findUserByEmailAndPassword(String email, String password) {
		return userDao.findUserByEmailAndPassword(email,password);
	}

	@Override
	public Page<User> getUserList(Pageable pageable) {
		return userDao.getUserList(pageable);
	}

	@Override
	public List<User> getUserListByUserIdList(List<Long> userIdList) {
		return userDao.getUserListByUserIdList(userIdList);
	}

	@Override
	public void addUser(User user) {
		userDao.addUser(user);
	}
	
	@Override
	public <S extends User> S save(S entity) {
		if(null == entity.getUserId()){
			UserId userId = new UserId();
			userId = uerIdService.save(userId);
			userId.setUserId(userId.getUniqueIdentifier());
			userId.setGenerateTime(new Date());
			entity.setUserId(userId);
		}
		return super.save(entity);
	}

	@Override
	public Page<User> getUserList(User user, Pageable pageable) {
		return userDao.getUserList( user,  pageable);
	}
	
	@Override
	public Page<User> getUserListByUserIdNotNull(User user, Pageable pageable){
		return userDao.getUserListByUserIdNotNull( user,  pageable);
	}

	@Override
	public Page<User> getLastUserList(Pageable pageable) {
		return userDao.getLastUserList(pageable);
	}
	
}
