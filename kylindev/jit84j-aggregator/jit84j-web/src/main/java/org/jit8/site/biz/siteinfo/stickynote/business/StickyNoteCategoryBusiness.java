package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface StickyNoteCategoryBusiness {

	public List<StickyNoteCategory> getStickyNoteCategoryList();
	public StickyNoteCategory persist(StickyNoteCategory stickyNoteCategory);
	
	public StickyNoteCategory getStickyNoteCategoryById(Long id);
	
	public StickyNoteCategory deleteStickyNoteCategoryById(Long id);
	
	public Page<StickyNoteCategory> findAll(Pageable pageable);
	
	public StickyNoteCategory findByCode(String code);
	
	public void removeById(Long id);
	
	public List<StickyNoteCategory> findByIds(List<Long> ids);
	
}
