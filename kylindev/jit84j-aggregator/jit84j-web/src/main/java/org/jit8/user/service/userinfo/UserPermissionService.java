package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.UserPermission;

public interface UserPermissionService extends GenericService<UserPermission, Long>{

	public List<UserPermission> findByPermissionId(Long permissionId);
}
