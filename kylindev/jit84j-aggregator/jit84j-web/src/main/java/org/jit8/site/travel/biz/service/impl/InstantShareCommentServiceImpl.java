package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.InstantShareCommentService;
import org.jit8.site.travel.persist.dao.InstantShareCommentDao;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstantShareCommentServiceImpl extends GenericServiceImpl<InstantShareComment, Long> implements InstantShareCommentService {

	@Resource
	private InstantShareCommentDao instantShareCommentDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = instantShareCommentDao;
	}


	@Override
	public List<InstantShareComment> getInstantShareCommentList(boolean disable, boolean deleted) {
		return instantShareCommentDao.getInstantShareCommentList(disable, deleted);
	}


	@Override
	public InstantShareComment findByCode(String code) {
		return instantShareCommentDao.findByCode( code);
	}


	@Override
	public List<InstantShareComment> findByIds(List<Long> idList) {
		return instantShareCommentDao.findByIds( idList);
	}


	@Override
	public Page<InstantShareComment> findByInstantShareId(Long instantShareId, Pageable pageable) {
		return instantShareCommentDao.findByInstantShareId( instantShareId,  pageable);
	}
	
}
