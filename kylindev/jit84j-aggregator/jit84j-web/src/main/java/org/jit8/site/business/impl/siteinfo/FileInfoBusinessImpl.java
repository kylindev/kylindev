package org.jit8.site.business.impl.siteinfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteConfigService;
import org.jit8.site.business.siteinfo.FileCustomGalleryBusiness;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.site.business.siteinfo.FileSystemGalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.service.siteinfo.FileInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service("fileInfoBusiness")
public class FileInfoBusinessImpl implements FileInfoBusiness{

	private static final Logger LOGGER = LoggerFactory.getLogger(FileInfoBusinessImpl.class);
	
	@Resource
	private FileInfoService fileInfoService;
	
	@Resource
	private StickyNoteConfigService stickyNoteConfigService;
	
	@Resource
	private FileCustomGalleryBusiness fileCustomGalleryBusiness;
	
	@Resource
	private FileSystemGalleryBusiness fileSystemGalleryBusiness;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;

	@Override
	public List<FileInfo> getFileInfoList() {
		return fileInfoService.getFileInfoList(false, false);
	}
	
	@Transactional
	@Override
	public FileInfo persist(FileInfo fileInfo){
		
		FileInfo old = fileInfoService.findOne(fileInfo.getId());
		return fileInfoService.save(old);
	}

	@Override
	public FileInfo getFileInfoById(Long id) {
		return fileInfoService.findOne(id);
	}

	@Override
	public FileInfo deleteFileInfoById(Long id) {
		FileInfo old = fileInfoService.findOne(id);
		old.setDeleted(true);
		return fileInfoService.save(old);
	}
	
	@Override
	public FileInfo deleteFileInfoByIdAndUserId(Long userId,Long id) {
		FileInfo old = fileInfoService.findOne(id);
		Long oldUserId = old.getCreateUser();
		if(userId.equals(oldUserId)){
			old.setDeleted(true);
		}
		return fileInfoService.save(old);
	}

	@Override
	public List<FileInfo> resolveFile(FileInfo fileInfo)  throws BusinessException{
		
		MultipartFile[] files = fileInfo.getFiles();
		MultipartFile singleFile = fileInfo.getSingleFile();
		
		List<FileInfo> fileInfos = new ArrayList<FileInfo>();
		//解析文件并保存
		if(singleFile != null ){
			if(!singleFile.isEmpty()){
				transform(fileInfo, singleFile, fileInfos);
			}
		}
		
		if(null != files && files.length>0){
			for(MultipartFile file : files){
				if(file.isEmpty()){  
					LOGGER.info("文件未上传");  
	            }else{  
	            	LOGGER.info("文件长度: " + file.getSize());  
	            	LOGGER.info("文件类型: " + file.getContentType());  
	            	LOGGER.info("文件名称: " + file.getName());  
	            	LOGGER.info("文件原名: " + file.getOriginalFilename());  
	            	LOGGER.info("========================================");  
	                //如果用的是Tomcat服务器，则文件会上传到\\%TOMCAT_HOME%\\webapps\\YourWebProject\\WEB-INF\\upload\\文件夹中  
	                //这里不必处理IO流关闭的问题，因为FileUtils.copyInputStreamToFile()方法内部会自动把用到的IO流关掉，我是看它的源码才知道的  
	                transform(fileInfo, file, fileInfos);
	            }  
			}
		}
		
		
		fileInfos = (List<FileInfo>) fileInfoService.save(fileInfos);
		return fileInfos;
	}

	private void transform(FileInfo fileInfo, MultipartFile singleFile,
			List<FileInfo> fileInfos)  throws BusinessException{
		FileInfo single = new FileInfo();
		single.setOwner(fileInfo.getOwner());
		single.setSize(singleFile.getSize());
		single.setBatch(fileInfo.getBatch());
		String fileName = fileInfo.getFileName();
		if(StringUtils.isEmpty(fileName)){
			fileName = singleFile.getOriginalFilename();
		}
		single.setFileName(fileName);
		single.setOriginalFileName(singleFile.getOriginalFilename());
		
		String fileAccessPath = StringUtils.trimToEmpty(fileInfo.getFileAccessPath());
		
		String contentType = singleFile.getContentType();//获取文件类型
		single.setContentType(contentType);
		
		//获取文件后缀名
		String originalFilename = singleFile.getOriginalFilename();
		String suffix = "";
		if(!org.apache.commons.lang.StringUtils.isEmpty(originalFilename)){
			int location = originalFilename.lastIndexOf(".");
			if(location>-1){
				suffix = originalFilename.substring(location+1);
				single.setFileSuffix(suffix);
			}
		}
		
		//根据文件类型与后缀名获取保存路径
		//String storedPath = getStoredPathBySuffixMime(suffix,contentType);
		FileSystemGallery fileSystemGallery = getFileSystemGalleryBySuffixMime(suffix,contentType);
		single.setSystemGallery(fileSystemGallery);
		String storedPath = fileSystemGallery.getStoredPath();
		
		//按照不同类型归类到文件目录
		fileAccessPath=fileAccessPath + storedPath;
		
		fileAccessPath = addDateToDir(fileAccessPath,"yyyy/MM/dd","/");
		single.setFileAccessPath(fileAccessPath);
		//同时要设置磁盘路径
		single.setFileRealPath(addDateToDir(StringUtils.trimToEmpty(fileInfo.getFileRealPath() + storedPath.replace("/", File.separator)),"yyyy"+File.separator+"MM"+File.separator+"dd",File.separator));
		
		try {
			mkdirs(single.getFileRealPath());
			File newFile = new File(single.getFileRealPath(), singleFile.getOriginalFilename());
			if(newFile.exists()){
				originalFilename = addCurrentTimeToFileName(originalFilename);
				single.setOriginalFileName(originalFilename);
				newFile = new File(single.getFileRealPath(), originalFilename);
			}
			String fileAccessFullPath = fileAccessPath + "/" + originalFilename;
			single.setFileAccessFullPath(fileAccessFullPath);
			FileOutputStream fileOutputStream = new FileOutputStream(newFile);
			org.springframework.util.FileCopyUtils.copy(singleFile.getInputStream(),fileOutputStream );
			single.setSuccess("上传成功");
		} catch (FileNotFoundException e) {
			LOGGER.error("FileNotFoundException: ",e);
			single.setError("保存失败");
		} catch (IOException e) {
			LOGGER.error("IOException: ",e);
			single.setError("保存失败");
		}
		fileInfos.add(single);
	}
	
	private void mkdirs(String fullPath){
		File file = new File(fullPath);
		if(!file.exists()){
			file.mkdirs();
		}
	}
	
	@SuppressWarnings({ "unused", "static-access" })
	private void addDateToDir(String dir,TimeZone timeZone){
		//TODO
		Calendar calendar = null;
		if(null != timeZone){
			calendar = calendar.getInstance(timeZone);
		}else{
			calendar = Calendar.getInstance();
		}
		
		
	}
	
	private String addDateToDir(String dir,String format,String separator){
		//check fileName is a valid name
		if(StringUtils.isEmpty(format)){
			format = "yyyy"+File.separator+"MM"+File.separator+"dd";
		}
		if(StringUtils.isEmpty(separator)){
			separator = File.separator;
		}
		SimpleDateFormat sf = new SimpleDateFormat(format);
		String dateDir = sf.format(new Date());
		dir = dir + separator + dateDir;
		return dir;
	}
	
	private String addCurrentTimeToFileName(String fileName){
		//check fileName is a valid name
		SimpleDateFormat sf = new SimpleDateFormat("HHmmss");
		String dateDir = sf.format(new Date());
		int location = fileName.lastIndexOf(".");
		String prefix = "";
		String suffix = "";
		if(location>-1){
			prefix = fileName.substring(0, location);
			suffix = fileName.substring( location);
		}
		fileName = prefix + "_" + dateDir + suffix;
		return fileName;
	}
	
	
	public static void main(String[] args) {
		System.out.println(File.pathSeparator);
		System.out.println(File.separator);
	}
	
	
	public FileInfo asignSystemGallery(Long id, Long fileSystemGalleryId){
		LOGGER.info("FileInfo asignSystemGallery: id-->" + id + ",fileSystemGalleryId-->"+fileSystemGalleryId);
		FileInfo fileInfo = fileInfoService.findOne(id);
		FileSystemGallery systemGallery = fileSystemGalleryBusiness.getFileSystemGalleryById(fileSystemGalleryId);
		if(null != fileInfo && null != systemGallery){
			fileInfo.setSystemGallery(systemGallery);
			fileInfoService.save(fileInfo);
			return fileInfo;
		}
		return null;
	}
	
	public FileInfo removeSystemGallery(Long id){
		LOGGER.info("FileInfo removeSystemGallery: id-->" + id );
		FileInfo fileInfo = fileInfoService.findOne(id);
		if(null != fileInfo){
			fileInfo.setSystemGallery(null);
			fileInfoService.save(fileInfo);
			return fileInfo;
		}
		return null;
	}
	
	
	public FileInfo asignCustomGallery(Long id, Long fileCustomGalleryId){
		LOGGER.info("FileInfo asignSystemGallery: id-->" + id + ",fileSystemGalleryId-->"+fileCustomGalleryId);
		FileInfo fileInfo = fileInfoService.findOne(id);
		FileCustomGallery customGallery = fileCustomGalleryBusiness.getFileCustomGalleryById(fileCustomGalleryId);
		if(null != fileInfo && null != customGallery){
			fileInfo.setCustomGallery(customGallery);
			fileInfoService.save(fileInfo);
			return fileInfo;
		}
		return null;
	}
	
	public FileInfo removeCustomGallery(long id, long owner){
		LOGGER.info("FileInfo removeSystemGallery: id-->" + id );
		//普通用户只能移除自己的分类
		FileInfo fileInfo = fileInfoService.findByIdAndOwner(id,owner);
		if(null != fileInfo){
			FileCustomGallery customGallery = fileCustomGalleryBusiness.getDefaultCustomGallery();//将文件从某类别移除，恢复至默认分类
			fileInfo.setCustomGallery(customGallery);
			fileInfoService.save(fileInfo);
			return fileInfo;
		}
		return null;
	}
	
	public FileInfo removeCustomGallery(Long id){
		LOGGER.info("FileInfo removeSystemGallery: id-->" + id );
		//移除指定id的分类，管理员可操作
		FileInfo fileInfo = fileInfoService.findOne(id);
		if(null != fileInfo){
			FileCustomGallery customGallery = fileCustomGalleryBusiness.getDefaultCustomGallery();//将文件从某类别移除，恢复至默认分类
			fileInfo.setCustomGallery(customGallery);
			fileInfoService.save(fileInfo);
			return fileInfo;
		}
		return null;
	}

	@SuppressWarnings({  "static-access" })
	public String getRootStoredPath(){
		
		SiteInfo siteInfo = (SiteInfo)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SITE_INFO_MAIN_SITE);
		if(null == siteInfo){
			siteInfo = staticDataDefineCoreManager.getMainSiteInfo();
		}
		String rootPath="";
		FileInfoConfig fileInfoConfig = siteInfo.getFileInfoConfig();
		if(null != fileInfoConfig){
			rootPath = StringUtils.trimToEmpty(fileInfoConfig.getRootPath());
		}
		if(StringUtils.isEmpty(rootPath)){
			rootPath = FileInfoConfig.ROOT_PATH_DEFAULT;
		}
		return rootPath;
	}
	
	@SuppressWarnings({  "static-access" })
	public String getEmailStoredPath(){
		
		SiteInfo siteInfo = (SiteInfo)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SITE_INFO_MAIN_SITE);
		if(null == siteInfo){
			siteInfo = staticDataDefineCoreManager.getMainSiteInfo();
		}
		String emailPath="";
		FileInfoConfig fileInfoConfig = siteInfo.getFileInfoConfig();
		if(null != fileInfoConfig){
			emailPath = StringUtils.trimToEmpty(fileInfoConfig.getEmailPath());
		}
		if(StringUtils.isEmpty(emailPath)){
			emailPath = FileInfoConfig.EMAIL_PATH_DEFAULT;
		}
		return emailPath;
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public FileSystemGallery getFileSystemGalleryBySuffixMime(String suffix,String mime) {
		Map<String,FileSystemGallery> map = (Map<String,FileSystemGallery>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP);
		
		if(map == null || map.size()<1){
			map = staticDataDefineCoreManager.getFileSystemGalleryBySuffixMimeForMap();
		}
		FileSystemGallery fileSystemGallery = map.get(suffix+mime);
		return fileSystemGallery;
	}
	
	@SuppressWarnings({ "unchecked", "static-access" })
	public String getStoredPathBySuffixMime(String suffix,String mime) throws BusinessException{
		Map<String,FileSystemGallery> map = (Map<String,FileSystemGallery>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP);
		
		if(map == null || map.size()<1){
			map = staticDataDefineCoreManager.getFileSystemGalleryBySuffixMimeForMap();
		}
		FileSystemGallery fileSystemGallery = map.get(suffix+mime);
		String stroedPath = "";
		if(null != fileSystemGallery){
			stroedPath = fileSystemGallery.getStoredPath();
		}else{
			LOGGER.error("not_allowed_file_type", "not allowed file type : " + suffix + " " + mime);
			throw new BusinessException("not_allowed_file_type", "not allowed file type");
		}
		
		return stroedPath;
	}

	@Override
	public Page<FileInfo> findAll(FileInfo fileInfo, Pageable pageable) {
		return fileInfoService.findAll(fileInfo, pageable);
	}
}
