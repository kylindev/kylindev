package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.site.travel.biz.business.InstantShareBusiness;
import org.jit8.site.travel.biz.business.ManifestoBusiness;
import org.jit8.site.travel.biz.service.ManifestoService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("ManifestoBusiness")
public class ManifestoBusinessImpl implements ManifestoBusiness{

	@Resource
	private ManifestoService manifestoService;

	@Override
	public Page<Manifesto> getList(Date begin, Date end, Pageable pageable) {
		Page<Manifesto> page = manifestoService.getList( begin,  end,  pageable);
		return page;
	}

	@Override
	public Page<Manifesto> getTodayList(Pageable pageable) {
		Date end = new Date();
		Date begin = CalendarUtils.getBeforeOrAfterDateForDay(end, -1);
		
		Page<Manifesto> page = manifestoService.getList( begin,  end,  pageable);
		return page;
	}

	@Override
	public Manifesto findTodayManifestoByUserId(Long userId) {
		Manifesto manifesto = null;
		if(null != userId && userId>0){
			Date begin = CalendarUtils.getTodayBegin();
			Date end = CalendarUtils.getTodayEnd();
			List<Manifesto> list = manifestoService.findManifestoByPublishDateAndUserId(begin, end, userId);
			if(null != list && list.size()>0){
				manifesto = list.get(0);
			}
		}
		return manifesto;
	}

	@Transactional
	@Override
	public Manifesto persistTodayManifesto(Manifesto manifesto,User user) {
		Long userId = user.getUserId().getUserId();
		Manifesto exist = findTodayManifestoByUserId(userId);
		if(null != exist){
			exist.setDecription(manifesto.getDecription());
			exist.setUpdateDate(new Date());
			manifesto = manifestoService.save(exist);
		}else{
			manifesto.setPublishDate(new Date());
			manifesto.setUserId(userId);
			manifesto.setUser(user);
			manifesto.setNickName(user.getNickname());
			manifesto = manifestoService.save(manifesto);
		}
		return manifesto;
	}

	@Transactional
	@Override
	public Manifesto praise(Long id) {
		Manifesto manifesto = manifestoService.findOne(id);
		if(null != manifesto){
			int praiseCount  = manifesto.getPraiseCount()==null?0: manifesto.getPraiseCount();
			manifesto.setPraiseCount(praiseCount+1);
			manifesto.setUpdateDate(new Date());
			manifestoService.save(manifesto);
		}
		return manifesto;
	}

	@Override
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable, Pageable commontPageable) {
		return manifestoService.getTodayList(manifesto, pageable,  commontPageable);
	}


	/**
	 * key: current, previous，following
	 * @param id
	 * @return
	 */
	public Map<String, Manifesto> getThreeManifestosById(Long id){
		
		Pageable commentPage = new PageRequest(0, 20, Direction.DESC, "publishDate");
		
		Manifesto current = manifestoService.findOneWithPageable(id,commentPage);
		
		Long previousId = manifestoService.findPreviousOneByCurrentId(id);
		Manifesto previous = null;
		if(null != previousId && previousId>0){
			previous = manifestoService.findSimpleOneById(previousId);
		}
		
		Long followingId = manifestoService.findFollowingOneByCurrentId(id);
		Manifesto following = null;
		if(null != followingId && followingId>0){
			following = manifestoService.findSimpleOneById(followingId);
		}
		
		Map<String, Manifesto> map = new HashMap<String, Manifesto>();
		map.put(ManifestoBusiness.MANIFESTO_CURRENT, current);
		map.put(ManifestoBusiness.MANIFESTO_PREVIOUS, previous);
		map.put(ManifestoBusiness.MANIFESTO_FOLLOWING, following);
		return map;
	}
	
	@Override
	public Long findLastId(){
		return manifestoService.findLastId();
	}
}
