package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface SpecialAreaBusiness {

	public List<SpecialArea> getSpecialAreaList();
	public SpecialArea persist(SpecialArea specialArea);
	
	public SpecialArea getSpecialAreaById(Long id);
	
	public SpecialArea deleteSpecialAreaById(Long id);
	
	public Page<SpecialArea> findAll(Pageable pageable);
	
	public SpecialArea findByCode(String code);
	
	public void removeById(Long id);
	
	public List<SpecialArea> findByIds(List<Long> idList);
}
