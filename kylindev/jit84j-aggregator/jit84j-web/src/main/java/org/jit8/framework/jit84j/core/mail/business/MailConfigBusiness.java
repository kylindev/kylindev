package org.jit8.framework.jit84j.core.mail.business;

import java.util.List;

import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MailConfigBusiness{

	List<MailConfig> getMailConfigList(boolean disable,boolean deleted);
	
	public MailConfig findByType(String type);
	
	public MailConfig findDefaultMailConfig();
	
	public Page<MailConfig> findAll(Pageable pageable);
	
	public MailConfig findById(long id);
	
	public MailConfig persist(MailConfig mailConfig);
	

}
