package org.jit8.site.travel.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.InstantShareImageBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InstantShareImageController{

	private static final Logger logger = LoggerFactory.getLogger(InstantShareImageController.class);
	
	
	@Resource
	private InstantShareImageBusiness instantShareImageBusiness;
	
	@Resource
	private Mapper mapper;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_MAPPING)
	public String instantShareListUI(@ModelAttribute("instantShareImage") InstantShareImage instantShareImage,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		String yearMonthDay = instantShareImage.getYearMonthDay();
		Page<InstantShareImage> page = null;
		if(StringUtils.isNotEmpty(yearMonthDay) && null != userId){
			instantShareImage.setUserId(userId);
			page = instantShareImageBusiness.findByDayAndUserId(instantShareImage, pageable);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("page", page);
		return "instantShareImageListUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_MAPPING)
	public String instantShareListItemUI(@ModelAttribute("instantShareImage") InstantShareImage instantShareImage,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		String yearMonthDay = instantShareImage.getYearMonthDay();
		Page<InstantShareImage> page = null;
		if(StringUtils.isNotEmpty(yearMonthDay) && null != userId){
			instantShareImage.setUserId(userId);
			page = instantShareImageBusiness.findByDayAndUserId(instantShareImage, pageable);
		}
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",StaticDataUtil.getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI));
		model.addAttribute("page", page);
		return "instantShareImageListItemUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_MAPPING + "/{instantShareId}")
	public String instantShareCommentList(@PathVariable("instantShareId") Long instantShareId,@ModelAttribute("instantShareImage") InstantShareImage instantShareImage,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		//model.addAttribute("depth", instantShareComment.getDepth());
		
		Pageable imagePageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.DESC, "publishDate");
		Page<InstantShareImage> imagePage = null;
		if(null != instantShareId && instantShareId >0 && null != user && null != userId){
			imagePage = instantShareImageBusiness.findByInstantShareId(instantShareId, imagePageable);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShareImage", instantShareImage);
		model.addAttribute("instantShareId", instantShareId);
		model.addAttribute("page",imagePage);
		
		return "instantShareImageList2UI";
	}
	
}
