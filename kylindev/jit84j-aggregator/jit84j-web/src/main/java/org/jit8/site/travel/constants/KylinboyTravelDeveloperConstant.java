package org.jit8.site.travel.constants;

import org.jit8.user.developer.common.constants.CommonDeveloperConstant;


public class KylinboyTravelDeveloperConstant extends CommonDeveloperConstant{

	private static final long serialVersionUID = -2922906669417770496L;
	
	public static final String _KYLINBOY_CODE_SEPERATOR = "_";
	public static final String _KYLINBOY_CODE_SEPERATOR_O = "o";
	public static final long _KYLINBOY_USER_ID_LONG = 100001l;//开发者ID
	public static final String _KYLINBOY_USER_ID = "100001";//开发者ID
	public static final String _KYLINBOY_CODE_PARAM = "{opration}";
	
	/**
	 * 用于url中code两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_CODE_SEPERATOR_PRIFIX = _KYLINBOY_CODE_SEPERATOR_O + _KYLINBOY_CODE_SEPERATOR;
	public static final String _KYLINBOY_CODE_SEPERATOR_SUFFIX = _KYLINBOY_CODE_SEPERATOR + _KYLINBOY_CODE_SEPERATOR_O;
	
	/**
	 * 用于url中userId两侧标识
	 * 例如:
	 * 
	 */
	public static final String _KYLINBOY_USER_SEPERATOR_PRIFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR_U + CommonDeveloperConstant._COMMON_USER_SEPERATOR;
	public static final String _KYLINBOY_USER_SEPERATOR_SUFFIX = CommonDeveloperConstant._COMMON_USER_SEPERATOR + CommonDeveloperConstant._COMMON_USER_SEPERATOR_U;
	
	/**
	 * 用于url中userId+标识组合
	 */
	public static final String _KYLINBOY_USER_ID_AND_SEPERATOR = _KYLINBOY_USER_SEPERATOR_PRIFIX + _KYLINBOY_USER_ID + _KYLINBOY_USER_SEPERATOR_SUFFIX;
	
	/**
	 * 用于url中code+标识组合
	 */
	public static final String _KYLINBOY_CODE_AND_SEPERATOR = _KYLINBOY_CODE_PARAM + _KYLINBOY_CODE_SEPERATOR_SUFFIX;
	
	
	/**
	 * 用于url中code+userId+标识组合
	 */
	public static final String _KYLINBOY_CODE_PARAM_AND_USER_ID = _KYLINBOY_CODE_AND_SEPERATOR + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	
	
	/** JourneyController mapping begin */
	
	/**行程管理模块*/
	
	public static final String KYLINBOY_JOURNEY_MODULE_ID = _KYLINBOY_USER_ID + "journey_manage";
	public static final String KYLINBOY_JOURNEY_MODULE = "journey_manage";
	public static final String KYLINBOY_JOURNEY_MODULE_NAME = "行程管理";
	public static final String KYLINBOY_JOURNEY_MODULE_CODE = _KYLINBOY_USER_ID + KYLINBOY_JOURNEY_MODULE;
	
	public static final String KYLINBOY_JOURNEY_ADD_UI_ID = _KYLINBOY_USER_ID + "journey_add_ui";
	public static final String KYLINBOY_JOURNEY_ADD_UI = "journey_add_ui";
	public static final String KYLINBOY_JOURNEY_ADD_UI_NAME = "行程添加页面";
	public static final String KYLINBOY_JOURNEY_ADD_UI_DESC = "行程添加页面";
	public static final String KYLINBOY_JOURNEY_ADD_UI_URL = "/admin/main/journey/";
	public static final String KYLINBOY_JOURNEY_ADD_UI_MAPPING = KYLINBOY_JOURNEY_ADD_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_JOURNEY_ADD_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_JOURNEY_ADD_2_UI_ID = _KYLINBOY_USER_ID + "journey_add_2_ui";
	public static final String KYLINBOY_JOURNEY_ADD_2_UI = "journey_add_2_ui";
	public static final String KYLINBOY_JOURNEY_ADD_2_UI_NAME = "行程详细日程设计";
	public static final String KYLINBOY_JOURNEY_ADD_2_UI_DESC = "行程详细日程设计页面";
	public static final String KYLINBOY_JOURNEY_ADD_2_UI_URL = "/admin/main/journey/";
	public static final String KYLINBOY_JOURNEY_ADD_2_UI_MAPPING = KYLINBOY_JOURNEY_ADD_2_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_JOURNEY_ADD_2_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_JOURNEY_ADD_3_UI_ID = _KYLINBOY_USER_ID + "journey_add_3_ui";
	public static final String KYLINBOY_JOURNEY_ADD_3_UI = "journey_add_3_ui";
	public static final String KYLINBOY_JOURNEY_ADD_3_UI_NAME = "行程角色设定";
	public static final String KYLINBOY_JOURNEY_ADD_3_UI_DESC = "行程角色设定页面";
	public static final String KYLINBOY_JOURNEY_ADD_3_UI_URL = "/admin/main/journey/";
	public static final String KYLINBOY_JOURNEY_ADD_3_UI_MAPPING = KYLINBOY_JOURNEY_ADD_3_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_JOURNEY_ADD_3_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/** JourneyController mapping end */
	
	/**  ManifestoController mapping begin */
	/**今日宣言模块*/
	public static final String KYLINBOY_MANIFESTO_LIST_UI_ID = _KYLINBOY_USER_ID + "manifesto_list_ui";
	public static final String KYLINBOY_MANIFESTO_LIST_UI = "manifesto_list_ui";
	public static final String KYLINBOY_MANIFESTO_LIST_UI_NAME = "今日宣言列表";
	public static final String KYLINBOY_MANIFESTO_LIST_UI_DESC = "今日宣言列表页面";
	public static final String KYLINBOY_MANIFESTO_LIST_UI_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_LIST_UI_MAPPING = KYLINBOY_MANIFESTO_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI_ID = _KYLINBOY_USER_ID + "manifesto_list_item_ui";
	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI = "manifesto_list_item_ui";
	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI_NAME = "今日宣言Item列表";
	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI_DESC = "今日宣言列表Item页面";
	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_LIST_ITEM_UI_MAPPING = KYLINBOY_MANIFESTO_LIST_ITEM_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_LIST_ITEM_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MANIFESTO_EDIT_UI_ID = _KYLINBOY_USER_ID + "manifesto_edit_ui";
	public static final String KYLINBOY_MANIFESTO_EDIT_UI = "manifesto_edit_ui";
	public static final String KYLINBOY_MANIFESTO_EDIT_UI_NAME = "今日宣言编辑";
	public static final String KYLINBOY_MANIFESTO_EDIT_UI_DESC = "今日宣言编辑页面";
	public static final String KYLINBOY_MANIFESTO_EDIT_UI_URL = "/front/myManage/manifesto/";
	public static final String KYLINBOY_MANIFESTO_EDIT_UI_MAPPING = KYLINBOY_MANIFESTO_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANIFESTO_EDIT_ID = _KYLINBOY_USER_ID + "manifesto_edit";
	public static final String KYLINBOY_MANIFESTO_EDIT = "manifesto_edit";
	public static final String KYLINBOY_MANIFESTO_EDIT_NAME = "今日宣言编辑提交";
	public static final String KYLINBOY_MANIFESTO_EDIT_DESC = "今日宣言编辑提交";
	public static final String KYLINBOY_MANIFESTO_EDIT_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_EDIT_MAPPING = KYLINBOY_MANIFESTO_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE_ID = _KYLINBOY_USER_ID + "manifesto_edit_praise";
	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE = "manifesto_edit_praise";
	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE_NAME = "今日宣言点赞提交";
	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE_DESC = "今日宣言点赞提交";
	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING = KYLINBOY_MANIFESTO_EDIT_PRAISE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_EDIT_PRAISE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL_ID = _KYLINBOY_USER_ID + "manifesto_view_detail_ui";
	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL = "manifesto_view_detail_ui";
	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL_NAME = "今日宣言详细页面";
	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL_DESC = "今日宣言详细页面";
	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL_URL = "/front/main/detail/manifesto/";
	public static final String KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING = KYLINBOY_MANIFESTO_VIEW_DETAIL_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_VIEW_DETAIL + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/**  ManifestoController mapping end */
	
	/**  ManifestoCommentController mapping begin */
	
	/**今日宣言评论模块*/
	
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD_ID = _KYLINBOY_USER_ID + "manifesto_comment_add";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD = "manifesto_comment_add";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD_NAME = "今日宣言评论添加";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD_DESC = "今日宣言评论添加";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING = KYLINBOY_MANIFESTO_COMMENT_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_COMMENT_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2_ID = _KYLINBOY_USER_ID + "manifesto_comment2_add";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2 = "manifesto_comment2_add";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2_NAME = "今日宣言评论添加";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2_DESC = "今日宣言评论添加";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2_URL = "/front/blank/manifesto/";
	public static final String KYLINBOY_MANIFESTO_COMMENT_ADD2_MAPPING = KYLINBOY_MANIFESTO_COMMENT_ADD2_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_COMMENT_ADD2 + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI_ID = _KYLINBOY_USER_ID + "manifesto_comment_list_ui";
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI = "manifesto_comment_list_ui";
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI_NAME = "今日宣言评论列表";
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI_DESC = "今日宣言评论列表";
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI_URL = "/front/blank/manifesto/UI/";
	public static final String KYLINBOY_MANIFESTO_COMMENT_LIST_UI_MAPPING = KYLINBOY_MANIFESTO_COMMENT_LIST_UI_URL + _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANIFESTO_COMMENT_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**  ManifestoCommentController mapping end */
	
	/**即时分享模块*/
	/**  InstantShareController mapping begin */
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI_ID = _KYLINBOY_USER_ID + "instantShare_edit_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI = "instantShare_edit_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI_NAME = "即时分享编辑";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI_DESC = "即时分享编辑页面";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI_URL = "/front/myManage/upload/instantShare/";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING = KYLINBOY_INSTANT_SHARE_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI_ID = _KYLINBOY_USER_ID + "instantShare_edit2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI = "instantShare_edit2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI_NAME = "即时分享编辑";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI_DESC = "即时分享编辑页面";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI_URL = "/front/myManage/upload2/instantShare/";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_UI_MAPPING = KYLINBOY_INSTANT_SHARE_EDIT2_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_EDIT2_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_ID = _KYLINBOY_USER_ID + "instantShare_view_detail_ui";
	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI = "instantShare_view_detail_ui";
	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_NAME = "即时分享查看详细";
	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_DESC = "即时分享查看详细页面";
	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_URL = "/front/main/detail/instantShare/";
	public static final String KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING = KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI_ID = _KYLINBOY_USER_ID + "instantShare_edit3_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI = "instantShare_edit3_ui";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI_NAME = "即时分享编辑";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI_DESC = "即时分享编辑页面";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI_URL = "/front/blank/myManage/upload2/instantShare/";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT3_UI_MAPPING = KYLINBOY_INSTANT_SHARE_EDIT3_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_EDIT3_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_ID = _KYLINBOY_USER_ID + "instantShare_remove_image";
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE = "instantShare_remove_image";
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_NAME = "移除图片从即时分享";
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_DESC = "移除图片从即时分享";
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_URL = "/front/blank/myManage/instantShare/";
	public static final String KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_MAPPING = KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_ID = _KYLINBOY_USER_ID + "instantShare_edit2_do";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2 = "instantShare_edit2_do";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_NAME = "即时分享编辑";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_DESC = "即时分享编辑提交";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_URL = "/front/blank/myManage/upload2/instantShare/DO";
	public static final String KYLINBOY_INSTANT_SHARE_EDIT2_MAPPING = KYLINBOY_INSTANT_SHARE_EDIT2_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_EDIT2 + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI_ID = _KYLINBOY_USER_ID + "instantShare_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI = "instantShare_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI_NAME = "即时分享列表";
	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI_DESC = "即时分享列表";
	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI_URL = "/front/myManage/upload2/instantShare/UI";
	public static final String KYLINBOY_INSTANT_SHARE_LIST_UI_MAPPING = KYLINBOY_INSTANT_SHARE_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI_ID = _KYLINBOY_USER_ID + "instantShare_list2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI = "instantShare_list2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI_NAME = "即时分享列表";
	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI_DESC = "即时分享列表";
	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI_URL = "/front/blank/myManage/instantShare/UI";
	public static final String KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING = KYLINBOY_INSTANT_SHARE_LIST2_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_LIST2_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_INSTANT_SHARE_DELERE_ID = _KYLINBOY_USER_ID + "instantShare_delete";
	public static final String KYLINBOY_INSTANT_SHARE_DELERE = "instantShare_delete";
	public static final String KYLINBOY_INSTANT_SHARE_DELERE_NAME = "即时分享删除";
	public static final String KYLINBOY_INSTANT_SHARE_DELERE_DESC = "即时分享删除提交";
	public static final String KYLINBOY_INSTANT_SHARE_DELERE_URL = "/front/blank/myManage/instantShare/DO";
	public static final String KYLINBOY_INSTANT_SHARE_DELERE_MAPPING = KYLINBOY_INSTANT_SHARE_DELERE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_DELERE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	
	
	
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_ID = _KYLINBOY_USER_ID + "instantShareImageCounter_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI = "instantShareImageCounter_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_NAME = "图片数目统计列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_DESC = "图片数目统计列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_URL = "/front/blank/myManage/upload2/imageCount/UI";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_MAPPING = KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_ID = _KYLINBOY_USER_ID + "instantShareImage_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI = "instantShareImage_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_NAME = "即时分享图片列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_DESC = "即时分享图片列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_URL = "/front/blank/myManage/upload2/image/UI";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_MAPPING = KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_ID = _KYLINBOY_USER_ID + "instantShareImage_list_item_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI = "instantShareImage_list_item_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_NAME = "即时分享图片列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_DESC = "即时分享图片列表";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_URL = "/front/blank/myManage/upload2/image/UI";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_MAPPING = KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_PRAISE_ID = _KYLINBOY_USER_ID + "instant_share_praise";
	public static final String KYLINBOY_INSTANT_SHARE_PRAISE = "instant_share_praise";
	public static final String KYLINBOY_INSTANT_SHARE_PRAISE_NAME = "即时分享点赞提交";
	public static final String KYLINBOY_INSTANT_SHARE_PRAISE_DESC = "即时分享点赞提交";
	public static final String KYLINBOY_INSTANT_SHARE_PRAISE_URL = "/front/blank/instantShare/DO/";
	public static final String KYLINBOY_INSTANT_SHARE_PRAISE_MAPPING = KYLINBOY_INSTANT_SHARE_PRAISE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_PRAISE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD_ID = _KYLINBOY_USER_ID + "instant_share_comment_add";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD = "instant_share_comment_add";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD_NAME = "即时分享评论添加";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD_DESC = "即时分享评论添加";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD_URL = "/front/blank/instantShare/DO/";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_ADD_MAPPING = KYLINBOY_INSTANT_SHARE_COMMENT_ADD_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_COMMENT_ADD + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_ID = _KYLINBOY_USER_ID + "instant_share_comment_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI = "instant_share_comment_list_ui";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_NAME = "即时分享评论列表";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_DESC = "即时分享评论列表";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_URL = "/front/blank/instantShare/UI/";
	public static final String KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_MAPPING = KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_ID = _KYLINBOY_USER_ID + "instant_share_image_list2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI = "instant_share_image_list2_ui";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_NAME = "即时分享图片列表2";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_DESC = "即时分享图片列表2";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_URL = "/front/blank/instantShare/UI/";
	public static final String KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_MAPPING = KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	/**  InstantShareController mapping end */
	
	
	//我的管理区 begin
	/**ManageAreaItemController begin **/
	
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI_ID = _KYLINBOY_USER_ID + "manageAreaItem_index_ui";
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI = "manageAreaItem_index_ui";
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI_NAME = "我的管理区菜单管理";
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI_DESC = "我的管理区菜单管理页面";
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI_URL = "/admin/main/UI/manageAreaItem/";
	public static final String KYLINBOY_MANAGEAREAITEM_INDEX_UI_MAPPING = KYLINBOY_MANAGEAREAITEM_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANAGEAREAITEM_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI_ID = _KYLINBOY_USER_ID + "manageAreaItem_edit_ui";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI = "manageAreaItem_edit_ui";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI_NAME = "我的管理区菜单编辑";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI_DESC = "我的管理区菜单编辑页面";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI_URL = "/admin/blank/UI/manageAreaItem/";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_UI_MAPPING = KYLINBOY_MANAGEAREAITEM_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANAGEAREAITEM_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_ID = _KYLINBOY_USER_ID + "manageAreaItem_edit";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT = "manageAreaItem_edit";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_NAME = "我的管理区菜单编辑提交";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_DESC = "我的管理区菜单编辑提交";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_URL = "/admin/blank/DO/manageAreaItem/";
	public static final String KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING = KYLINBOY_MANAGEAREAITEM_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANAGEAREAITEM_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE_ID = _KYLINBOY_USER_ID + "manageAreaItem_remove";
	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE = "manageAreaItem_remove";
	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE_NAME = "我的管理区菜单删除";
	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE_DESC = "我的管理区菜单删除提交";
	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE_URL = "/admin/blank/DO/manageAreaItem/";
	public static final String KYLINBOY_MANAGEAREAITEM_REMOVE_MAPPING = KYLINBOY_MANAGEAREAITEM_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANAGEAREAITEM_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT_ID = _KYLINBOY_USER_ID + "manageAreaItem_list_edit";
	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT = "manageAreaItem_list_edit";
	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT_NAME = "我的管理区菜单列表编辑提交";
	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT_DESC = "我的管理区菜单列表编辑提交";
	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT_URL = "/admin/blank/DO/manageAreaItem/";
	public static final String KYLINBOY_MANAGEAREAITEM_LIST_EDIT_MAPPING = KYLINBOY_MANAGEAREAITEM_LIST_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_MANAGEAREAITEM_LIST_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**ManageAreaItemController end **/
	
	/**ItemButtonController begin **/
	
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI_ID = _KYLINBOY_USER_ID + "itemButton_index_ui";
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI = "itemButton_index_ui";
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI_NAME = "我的管理区菜单区按钮管理";
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI_DESC = "我的管理区菜单区按钮管理页面";
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI_URL = "/admin/main/UI/itemButton/";
	public static final String KYLINBOY_ITEMBUTTON_INDEX_UI_MAPPING = KYLINBOY_ITEMBUTTON_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ITEMBUTTON_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI_ID = _KYLINBOY_USER_ID + "itemButton_edit_ui";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI = "itemButton_edit_ui";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI_NAME = "我的管理区菜单区按钮编辑";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI_DESC = "我的管理区菜单区按钮编辑页面";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI_URL = "/admin/blank/UI/itemButton/";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_UI_MAPPING = KYLINBOY_ITEMBUTTON_EDIT_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ITEMBUTTON_EDIT_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;
	
	public static final String KYLINBOY_ITEMBUTTON_EDIT_ID = _KYLINBOY_USER_ID + "itemButton_edit";
	public static final String KYLINBOY_ITEMBUTTON_EDIT = "itemButton_edit";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_NAME = "我的管理区菜单区按钮编辑提交";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_DESC = "我的管理区菜单区按钮编辑提交";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_URL = "/admin/blank/DO/itemButton/";
	public static final String KYLINBOY_ITEMBUTTON_EDIT_MAPPING = KYLINBOY_ITEMBUTTON_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ITEMBUTTON_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ITEMBUTTON_REMOVE_ID = _KYLINBOY_USER_ID + "itemButton_remove";
	public static final String KYLINBOY_ITEMBUTTON_REMOVE = "itemButton_remove";
	public static final String KYLINBOY_ITEMBUTTON_REMOVE_NAME = "我的管理区菜单区按钮删除";
	public static final String KYLINBOY_ITEMBUTTON_REMOVE_DESC = "我的管理区菜单区按钮删除提交";
	public static final String KYLINBOY_ITEMBUTTON_REMOVE_URL = "/admin/blank/DO/itemButton/";
	public static final String KYLINBOY_ITEMBUTTON_REMOVE_MAPPING = KYLINBOY_ITEMBUTTON_REMOVE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ITEMBUTTON_REMOVE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT_ID = _KYLINBOY_USER_ID + "itemButton_list_edit";
	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT = "itemButton_list_edit";
	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT_NAME = "我的管理区菜单区按钮列表编辑提交";
	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT_DESC = "我的管理区菜单区按钮列表编辑提交";
	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT_URL = "/admin/blank/DO/itemButton/";
	public static final String KYLINBOY_ITEMBUTTON_LIST_EDIT_MAPPING = KYLINBOY_ITEMBUTTON_LIST_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ITEMBUTTON_LIST_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	/**ItemButtonController end **/
	
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_ID = _KYLINBOY_USER_ID + "userManageAreaItem_index_ui";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI = "userManageAreaItem_index_ui";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_NAME = "用户管理区菜单管理";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_DESC = "用户管理区菜单管理页面";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_URL = "/admin/main/UI/userManageAreaItem/";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_MAPPING = KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA_ID = _KYLINBOY_USER_ID + "userManageAreaItem_data";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA = "userManageAreaItem_data";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA_NAME = "用户管理区菜单数据json格式";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA_DESC = "用户管理区菜单数据json格式";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA_URL = "/admin/blank/data/userManageAreaItem/";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_DATA_MAPPING = KYLINBOY_USER_MANAGEAREAITEM_DATA_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_MANAGEAREAITEM_DATA + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_ID = _KYLINBOY_USER_ID + "userManageAreaItem_list_edit";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT = "userManageAreaItem_list_edit";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_NAME = "我的管理区用户菜单列表编辑提交";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_DESC = "我的管理区用户菜单列表编辑提交";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_URL = "/admin/blank/DO/userManageAreaItem/";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_MAPPING = KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_ID = _KYLINBOY_USER_ID + "userManageAreaItem_refresh_cache";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE = "userManageAreaItem_refresh_cache";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_NAME = "我的管理区用户菜单列表刷新缓存";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_DESC = "我的管理区用户菜单列表刷新缓存";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_URL = "/admin/blank/DO/userManageAreaItem/";
	public static final String KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_MAPPING = KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	//我的管理区 end
	
	public static final String KYLINBOY_ACTIVITY_LIST_UI_ID = _KYLINBOY_USER_ID + "activity_list_ui";
	public static final String KYLINBOY_ACTIVITY_LIST_UI = "activity_list_ui";
	public static final String KYLINBOY_ACTIVITY_LIST_UI_NAME = "活动列表页面";
	public static final String KYLINBOY_ACTIVITY_LIST_UI_DESC = "活动列表页面";
	public static final String KYLINBOY_ACTIVITY_LIST_UI_URL = "/front/myManage/upload2/activity/";
	public static final String KYLINBOY_ACTIVITY_LIST_UI_MAPPING = KYLINBOY_ACTIVITY_LIST_UI_URL+ _KYLINBOY_CODE_SEPERATOR_PRIFIX + KYLINBOY_ACTIVITY_LIST_UI + _KYLINBOY_CODE_SEPERATOR_SUFFIX + _KYLINBOY_USER_ID_AND_SEPERATOR;

	
	@Override
	public Long getDeveloperUserId() {
		return _KYLINBOY_USER_ID_LONG;
	}
}
