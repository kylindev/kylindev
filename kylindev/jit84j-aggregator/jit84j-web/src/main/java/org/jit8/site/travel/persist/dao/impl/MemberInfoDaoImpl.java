package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.MemberInfoDao;
import org.jit8.site.travel.persist.domain.MemberInfo;
import org.jit8.site.travel.persist.repository.MemberInfoRepository;
import org.springframework.stereotype.Repository;

@Repository
public class MemberInfoDaoImpl extends GenericDaoImpl<MemberInfo, Long> implements MemberInfoDao {

	@Resource
	private MemberInfoRepository memberInfoRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = memberInfoRepository;
	}

	@Override
	public List<MemberInfo> getMemberInfoList(boolean disable, boolean deleted) {
		return memberInfoRepository.getMemberInfoList(disable, deleted);
	}
	
	@Override
	public MemberInfo findByCode(String code) {
		return memberInfoRepository.findByUserName( code);
	}

	@Override
	public List<MemberInfo> findByIds(List<Long> idList) {
		return memberInfoRepository.findByIds(idList);
	}
}
