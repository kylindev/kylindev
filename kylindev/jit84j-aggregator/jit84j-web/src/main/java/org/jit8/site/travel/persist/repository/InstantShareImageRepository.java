package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InstantShareImageRepository extends GenericJpaRepository<InstantShareImage, Long>{

	
	@Query("from InstantShareImage o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<InstantShareImage> getInstantShareImageList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from InstantShareImage o where o.title=:title")
	public InstantShareImage findByTitle(@Param("title")String title);
	
	@Query("from InstantShareImage o where o.uniqueIdentifier in :idList")
	public List<InstantShareImage> findByIds(@Param("idList")List<Long> idList);
	
	@Modifying
	@Query("delete from InstantShareImage o where o.uniqueIdentifier=:id and o.userId=:userId")
	public void removeByIdAndUserId(@Param("id")Long id, @Param("userId")Long userId);
	
	@Query("from InstantShareImage o where o.instantShare.uniqueIdentifier = :instantShareId and o.deleted=false")
	public Page<InstantShareImage> findByInstantShareId(@Param("instantShareId")Long instantShareId, Pageable pageable);
}
