package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.InstantShareService;
import org.jit8.site.travel.persist.dao.InstantShareCommentDao;
import org.jit8.site.travel.persist.dao.InstantShareDao;
import org.jit8.site.travel.persist.dao.InstantShareImageDao;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstantShareServiceImpl extends GenericServiceImpl<InstantShare, Long> implements InstantShareService {

	@Resource
	private InstantShareDao instantShareDao;
	
	@Resource
	private InstantShareCommentDao instantShareCommentDao;
	
	@Resource
	private InstantShareImageDao instantShareImageDao;
	
	@PostConstruct
	public void initRepository(){
		this.genericDao = instantShareDao;
	}


	@Override
	public List<InstantShare> getInstantShareList(boolean disable, boolean deleted) {
		return instantShareDao.getInstantShareList(disable, deleted);
	}


	@Override
	public InstantShare findByCode(String code) {
		return instantShareDao.findByCode( code);
	}


	@Override
	public List<InstantShare> findByIds(List<Long> idList) {
		return instantShareDao.findByIds( idList);
	}


	@Override
	public Page<InstantShare> findByUserId(Long userId, Pageable pageable) {
		return instantShareDao.findByUserId( userId,  pageable) ;
	}
	
	@Override
	public Long findPreviousOneByCurrentId(Long id) {
		return instantShareDao.findPreviousOneByCurrentId(id);
	}

	@Override
	public Long findFollowingOneByCurrentId(Long id) {
		return instantShareDao.findFollowingOneByCurrentId(id);
	}


	@Override
	public InstantShare findSimpleOneById(Long id) {
		return instantShareDao.findSimpleOneById(id);
	}


	@Override
	public InstantShare findOneWithPageable(Long id, Pageable commentPageable, Pageable imagePageable) {
		InstantShare instantShare = instantShareDao.findOne(id);
		
		Page<InstantShareComment> instantShareCommentPage = instantShareCommentDao.findByInstantShareId(id, commentPageable);
		instantShare.setCommentPage(instantShareCommentPage);
		
		Page<InstantShareImage> instantShareImagePage = instantShareImageDao.findByInstantShareId(id, imagePageable);
		instantShare.setImagePage(instantShareImagePage);
		
		return instantShare;
	}
	
}
