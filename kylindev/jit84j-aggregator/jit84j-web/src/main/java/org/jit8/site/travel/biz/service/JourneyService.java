package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.Journey;


public interface JourneyService extends GenericService<Journey, Long>{

	public List<Journey> getJourneyList(boolean disable, boolean deleted);
	
	public Journey findByCode(String code);
	
	public List<Journey> findByIds(List<Long> idList);
}
