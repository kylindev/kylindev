package org.jit8.site.business.impl.siteinfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.FileMimeBusiness;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.service.siteinfo.FileMimeService;
import org.jit8.site.service.siteinfo.FileSystemGalleryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service("fileMimeBusiness")
public class FileMimeBusinessImpl implements FileMimeBusiness{

	private static final Logger LOGGER = LoggerFactory.getLogger(FileMimeBusinessImpl.class);
	
	@Resource
	private FileMimeService fileMimeService;
	
	@Resource
	private FileSystemGalleryService fileSystemGalleryService;
	
	public FileMime persist(FileMime fileMime){
		Long id = fileMime.getId();
		FileMime fileMimeExist = null;
		if(null != id && id>0){
			fileMimeExist = fileMimeService.findOne(id);
		}
		if(null != fileMimeExist){
			fileMimeExist.setDeleted(false);
			fileMimeExist.setName(fileMime.getName());
			fileMimeExist.setDescription(fileMime.getDescription());
			fileMimeExist.setSuffix(fileMime.getSuffix());
			fileMimeExist.setSequence(fileMime.getSequence());
			fileMimeExist.setMime(fileMime.getMime());
			fileMimeExist.setAllowable(fileMime.isAllowable());
		}else{
			fileMimeExist = fileMime;
		}
		FileSystemGallery fileSystemGallery = fileMime.getSystemGallery();
		if(null != fileSystemGallery){
			Long fileSystemGalleryId = fileSystemGallery.getId();
			if(null != fileSystemGalleryId && fileSystemGalleryId>0){
				fileSystemGallery = fileSystemGalleryService.findOne(fileSystemGalleryId);
				fileMimeExist.setSystemGallery(fileSystemGallery);
			}
		}
		
		
		return fileMimeService.save(fileMimeExist);
	}
	
	public FileMime findByMime(String mime){
		return fileMimeService.findByMime(mime);
	}
	
	
	public List<FileMime> asignSystemGallery(List<Long> idList, Long fileSystemGalleryId){
		List<FileMime> fileMimeList = findByIdList(idList);
		FileSystemGallery systemGallery = fileSystemGalleryService.findOne(fileSystemGalleryId);
		if(null != fileMimeList && fileMimeList.size()>0 && null != systemGallery){
			for(FileMime fileMime : fileMimeList){
				fileMime.setSystemGallery(systemGallery);
			}
			return (List<FileMime>)fileMimeService.save(fileMimeList);
		}
		return null;
	}
	
	public List<FileMime> findByIdList(List<Long> idList){
		LOGGER.info("List<FileMime> findByIdList:" + StringUtils.collectionToDelimitedString(idList, ","));
		if(null != idList && idList.size()>0){
			return fileMimeService.findByIdList(idList);
		}
		return null;
	}
	
	
	public FileMime asignSystemGallery(Long id, Long fileSystemGalleryId){
		LOGGER.info("FileMime asignSystemGallery: id-->" + id + ",fileSystemGalleryId-->"+fileSystemGalleryId);
		FileMime fileMime = fileMimeService.findOne(id);
		FileSystemGallery systemGallery = fileSystemGalleryService.findOne(fileSystemGalleryId);
		if(null != fileMime && null != systemGallery){
			fileMime.setSystemGallery(systemGallery);
			fileMimeService.save(fileMime);
			return fileMime;
		}
		return null;
	}
	
	public FileMime removeSystemGallery(Long id){
		LOGGER.info("FileMime removeSystemGallery: id-->" + id );
		FileMime fileMime = fileMimeService.findOne(id);
		if(null != fileMime ){
			fileMime.setSystemGallery(null);
			fileMimeService.save(fileMime);
			return fileMime;
		}
		return null;
	}
	
	public List<FileMime> getFileMimeListByAllowable(boolean allowable){
		
		return fileMimeService.getFileMimeListByAllowable(allowable);
	}
	
	public Map<String,List<String>> getAllowableFileMimeMap(){
	 	List<FileMime> list = getFileMimeListByAllowable(true);
		return getFileMimeMap(list);
	}
	
	public Map<String,List<String>> getFileMimeMap(List<FileMime> fileMimeList){
		if(null != fileMimeList && fileMimeList.size()>0){
			Map<String,List<String>> map = new HashMap<String,List<String>>();
			List<String> allowableMime = new ArrayList<String>();
			List<String> allowableSuffix = new ArrayList<String>();
			List<String> unAllowableMime = new ArrayList<String>();
			List<String> unAllowableSuffix = new ArrayList<String>();
			
			for(FileMime fileMime : fileMimeList){
				String mime = fileMime.getMime();
				String suffix = fileMime.getSuffix();
				if(fileMime.isDeleted() || fileMime.isDisable()){
					continue;
				}
				if(fileMime.isAllowable()){
					allowableMime.add(mime);
					allowableSuffix.add(suffix);
				}else{
					unAllowableMime.add(mime);
					unAllowableSuffix.add(suffix);
				}
			}
			map.put(FileMime.ALLOWABLE_MIME, allowableMime);
			map.put(FileMime.ALLOWABLE_SUFFIX, allowableSuffix);
			map.put(FileMime.UN_ALLOWABLE_MIME, unAllowableMime);
			map.put(FileMime.UN_ALLOWABLE_SUFFIX, unAllowableSuffix);
			
			return map;
		}
		return null;
		
	}

	@Override
	public Page<FileMime> findAll(FileMime fileMime, Pageable pageable) {
		return fileMimeService.findAll(fileMime,pageable);
	}

	@Override
	public void removeById(Long id) {
		fileMimeService.delete(id);
	}

	@Override
	public FileMime findById(Long id) {
		FileMime fileMime = null;
		if (null != id && id > 0) {
			fileMime = fileMimeService.findOne(id);
		}
		return fileMime;
	}

}
