package org.jit8.site.travel.web.controller.menu.managerarea;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.core.web.util.GsonUtils;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.menu.managerarea.ManageAreaItemBusiness;
import org.jit8.site.travel.biz.business.menu.managerarea.UserManageAreaItemBusiness;
import org.jit8.site.travel.common.utils.TravelStaticDataUtil;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.jit8.site.travel.web.controller.command.menu.managerarea.UserManageAreaItemCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class UserManageAreaItemController{

	private static final Logger logger = LoggerFactory.getLogger(UserManageAreaItemController.class);
	
	
	@Resource
	private UserManageAreaItemBusiness userManageAreaItemBusiness;
	
	@Resource
	private ManageAreaItemBusiness manageAreaItemBusiness;
	
	@Resource
	private Mapper mapper;
	
	//@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING)
	public String userManageAreaItemEditUI(@ModelAttribute("userManageAreaItem") UserManageAreaItem userManageAreaItem,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING + " #########");
		Long id = userManageAreaItem.getId();
		if(id != null && id > 0){
			//userManageAreaItem = userManageAreaItemBusiness.getUserManageAreaItemById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("userManageAreaItem", userManageAreaItem);
		model.addAttribute("batch", CalendarUtils.getNano1000Time());
		return "userManageAreaItemEditUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_MAPPING)
	public String userManageAreaItemIndexUI(@ModelAttribute("manageAreaItem") UserManageAreaItem userManageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("userManageAreaItem", userManageAreaItem);
		List<UserManageAreaItem> userManageAreaItemList = userManageAreaItemBusiness.findAllWithManageAreaItemByUserId(null);
		model.addAttribute("userManageAreaItemList", userManageAreaItemList);
		//List<UserManageAreaItemCommand> userManageAreaItemCommandList = new ArrayList<UserManageAreaItemCommand>();
		//mapper.map(userManageAreaItemList, userManageAreaItemCommandList);
		return "userManageAreaItemIndexUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_DATA_MAPPING)
	@ResponseBody
	public String userManageAreaItemData(@ModelAttribute("userManageAreaItem") UserManageAreaItem userManageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_DATA_MAPPING +" #########");
		String code= KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_DATA;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("userManageAreaItem", userManageAreaItem);
		Long userId = userManageAreaItem.getUserId();//Jit8ShareInfoUtils.getUserId();
		List<UserManageAreaItem> userManageAreaItemList = null;
		
		if(null != userId){
			userManageAreaItemList = userManageAreaItemBusiness.findAllByUserId(userId);
		}
		
		model.addAttribute("userManageAreaItemList", userManageAreaItemList);
		
		Map<String,Object> map = new HashMap<String,Object>();
		
		//map.put("result", userManageAreaItemList);
		
		List<UserManageAreaItemCommand> userManageAreaItemCommandList = new ArrayList<UserManageAreaItemCommand>(userManageAreaItemList.size());
		if(null != userManageAreaItemList){
			for(UserManageAreaItem uma : userManageAreaItemList){
				
				UserManageAreaItemCommand umac = mapper.map(uma, UserManageAreaItemCommand.class);
				userManageAreaItemCommandList.add(umac);
				
			}
		}
		map.put("result", userManageAreaItemCommandList);
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		map.put("msg", msg);
		
		GsonBuilder gb = new GsonBuilder();
		gb.setDateFormat("yyyy-MM-dd HH:mm:ss");
		Gson gson = gb.excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(map);
		logger.info(json);
		return json;
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_MAPPING)
	@ResponseBody
	public String manageAreaItemListEdit(@ModelAttribute("userManageAreaItem") UserManageAreaItem userManageAreaItem,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING +" #########");
		
		List<UserManageAreaItem> userManageAreaItemList = userManageAreaItemBusiness.persistList(userManageAreaItem.getUserId(),userManageAreaItem.getUserManageAreaItemList());
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		
		model.addAttribute("msg", msg);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("msg", msg);
		
		List<UserManageAreaItemCommand> userManageAreaItemCommandList = new ArrayList<UserManageAreaItemCommand>(userManageAreaItemList.size());
		for(UserManageAreaItem uma : userManageAreaItemList){
			
			UserManageAreaItemCommand umac = mapper.map(uma, UserManageAreaItemCommand.class);
			userManageAreaItemCommandList.add(umac);
			
		}
		map.put("result", userManageAreaItemCommandList);
		
		GsonBuilder gb = new GsonBuilder();
		gb.setDateFormat("yyyy-MM-dd HH:mm:ss");
		Gson gson = gb.excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(map);
		logger.info(json);
		return json;
	}
	

	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_MAPPING)
	@ResponseBody
	public String refreshUserManageAreaItemCache(Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		TravelStaticDataUtil.refreshUserManageAreaItemCache();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("msg", msg);
		String json = GsonUtils.getJsonFromMap(map);
		return json;
	}

	
}
