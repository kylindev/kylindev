package org.jit8.user.business.userinfo;

import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserBusiness {

	public User findUserByUsername(String username);
	
	public boolean exitUserByUsername(String username);
	
	public User findUserByEmail(String email);
	
	public boolean exitUserByEmail(String email);
	
	public User persist(User user);
	
	public User findUserByEmailAndPassword(String email,String password);
	
	public Page<User> getUserList(Pageable pageable);
	
	public Page<User> getLastUserList(Pageable pageable);
	
	public Page<User> getUserList(User user, Pageable pageable);
	
	public void addUser(User user);
	
	public User activeUser(User user,EmailUrl emailUrl);
	
	/**
	 * 根据用户Id查询用户信息
	 * @param id 用户id
	 * @return  用户信息
	 */
	public User findUserById(Long id);
	
	/**
	 * 保存或更新用户信息
	 * @param user 用户信息
	 * @return 用户ID
	 */
	public Long saveUpdte(User user);
	
	/**
	 * 根据id删除用户
	 * @param id 用户ID
	 * @return void
	 */
	public void deleteUserById(Long id);
	
	/**
	 * 为用户分配权限
	 * @param user
	 */
	public void asignPermissionForUser(User user) ;
	
	
	/**
	 * 为用户移除权限
	 * @param user
	 */
	public void removePermissionForUser(User user) ;
	
	/**
	 * 为用户移除所有权限
	 * @param user
	 */
	public void removeAllPermissionForUser(User user) ;
	
	public boolean findBackPassword(String clickUrl, User user,MailMessage mailMessage) throws BusinessException;
	
	public boolean resetPassword(User user,MailMessage mailMessage) throws BusinessException;
	public boolean resetPassword(User user,EmailUrl emailUrl, MailMessage mailMessage) throws BusinessException;
	
	public Page<User> getUserListByUserIdNotNull(User user, Pageable pageable);
	
	public User registerUser(User user);
	
}
