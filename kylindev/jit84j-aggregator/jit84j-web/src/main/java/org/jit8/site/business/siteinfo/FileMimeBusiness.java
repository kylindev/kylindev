package org.jit8.site.business.siteinfo;

import java.util.List;
import java.util.Map;

import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface FileMimeBusiness {

	public List<FileMime> getFileMimeListByAllowable(boolean allowable);
	
	public Map<String,List<String>> getAllowableFileMimeMap();
	
	public Map<String,List<String>> getFileMimeMap(List<FileMime> fileMimeList);
	
	public Page<FileMime> findAll(FileMime fileMime, Pageable pageable);
	
	public FileMime persist(FileMime fileMime);
	
	public void removeById(Long id);
	
	public FileMime findById(Long id);
	
	
}
