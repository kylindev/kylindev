package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.Journey;

public interface JourneyDao extends GenericDao<Journey, Long> {

	public List<Journey> getJourneyList(boolean disable, boolean deleted) ;
	
	public Journey findByCode(String code);
	
	public List<Journey> findByIds(List<Long> idList);
}
