package org.jit8.site.service.impl.menu;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.menu.NavigationDao;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.service.menu.NavigationService;
import org.springframework.stereotype.Service;

@Service
public class NavigationServiceImpl extends GenericServiceImpl<Navigation, Long> implements NavigationService {

	@Resource
	private NavigationDao navigationDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = navigationDao;
	}


	@Override
	public List<Navigation> getNavigationList(boolean disable, boolean deleted) {
		return navigationDao.getNavigationList(disable, deleted);
	}
	
	@Override
	public List<Navigation> getNavigationListByIds(boolean disable,
			boolean deleted, List<Long> navigationIdList) {
		return navigationDao.getNavigationListByIds(disable, deleted, navigationIdList);
	}


	@Override
	public List<Navigation> getNavigationListByParentNull() {
		return navigationDao.getNavigationListByParentNull();
	}


	@Override
	public List<Navigation> getNavigationByCode(boolean disable, boolean deleted,
			String code) {
		return navigationDao.getNavigationByCode(disable, deleted, code);
	}


	@Override
	public List<Navigation> getNavigationByTopCode(boolean disable,
			boolean deleted, String code) {
		return navigationDao.getNavigationByTopCode(disable, deleted, code);
	}
	
	@Override
	public List<Navigation> getNavigationByTopCode(boolean disable,
			 String code) {
		return navigationDao.getNavigationByTopCode(disable, code);
	}
	
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId){
		return navigationDao.getNavigationByCodeAndDeveloper(code,userId);
	}


	@Override
	public List<Navigation> getNavigationByParentCode( 
			boolean deleted, String code) {
		return navigationDao.getNavigationByParentCode( 
				 deleted,  code);
	}


	@Override
	public Navigation findNavigationByIdKey(String idKey) {
		return navigationDao.findNavigationByIdKey( idKey);
	}
	
}
