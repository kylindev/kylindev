package org.jit8.site.web.controller.command;

import java.io.Serializable;
import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * 即时分享图片类
 */
public class InstantShareImageCommand implements Serializable {

	private static final long serialVersionUID = 8244099340679653078L;

	@Expose
	private String title;//标题
	
	@Expose
	private String description;//描述
	
	@Expose
	private String imageUrl;//原图片路径
	
	@Expose
	private String thumbnailUrl;//缩略图片路径
	
	@Expose
	private Long userId;//用户id
	
	
	@Expose(serialize = false, deserialize = false)
	private InstantShareCommand instantShare;//所属即时分享
	
	@Expose
	private Date publishDate;//发布时间
	
	@Expose
	private Integer praiseCount;//点赞数 
	
	@Expose(serialize = false, deserialize = false)
	private InstantShareImageCommand parent;
	
	@Expose
	private Long fileInfoId;

	@Expose
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public InstantShareImageCommand getParent() {
		return parent;
	}
	public void setParent(InstantShareImageCommand parent) {
		this.parent = parent;
	}
	public Integer getPraiseCount() {
		return praiseCount;
	}
	public void setPraiseCount(Integer praiseCount) {
		this.praiseCount = praiseCount;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Long getFileInfoId() {
		return fileInfoId;
	}
	public void setFileInfoId(Long fileInfoId) {
		this.fileInfoId = fileInfoId;
	}
	public String getThumbnailUrl() {
		return thumbnailUrl;
	}
	public void setThumbnailUrl(String thumbnailUrl) {
		this.thumbnailUrl = thumbnailUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	
}
