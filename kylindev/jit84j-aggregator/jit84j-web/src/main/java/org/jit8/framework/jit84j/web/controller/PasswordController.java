package org.jit8.framework.jit84j.web.controller;

import java.io.IOException;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.framework.jit84j.web.utils.MailMessageUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.framework.jit84j.web.validator.LoginValidator;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.user.business.userinfo.EmailUrlBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PasswordController extends BaseCommonController{

private static final Logger logger = LoggerFactory.getLogger(PasswordController.class);
	
	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private LoginValidator loginValidator;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;
	
	@Resource
	private FileInfoBusiness fileInfoBusiness;
	
	@Resource
	private EmailUrlBusiness emailUrlBusiness;
	
	
	@InitBinder  
    public void initBinder(DataBinder binder) {  
    }
	
	public PasswordController(){
		logger.debug("######### init LoginController #########");
	}

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_UI_MAPPING)
	public String forgetPasswordUI(@ModelAttribute("user") User user, Model model) throws IOException {
		logger.debug("######### /loginUI #########");
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		
		//model.addAttribute("ownRelationList", staticDataDefine.getData(StaticDataDefineConstant.OWN_RELATION_LIST, false));
		return "forgetPasswordUI";
	}
	
		//找回密码提交
		@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_MAPPING)
		public String forgetPassword2UI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
			logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_MAPPING +" #########");
			String code= KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD;
			if(StringUtils.isNotEmpty(code)){
				model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
			}
			try {
				boolean pass = validateEmailEmpty(user, model);
				if(pass){
					 MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user, fileInfoBusiness.getEmailStoredPath(), request);
					userBusiness.findBackPassword(KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_UI_MAPPING,user,mailMessage);
					model.addAttribute("msg","重置密码的邮件已发送至您的邮箱【" + user.getEmail() + "】，请您登录邮箱来重置并激活新密码");
				}
				
			} catch (BusinessException e) {
				logger.error(e.getCode(),e);
			}
			model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
			
			return "forgetPassword2UI";
		}
		
		@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_DO_MAPPING)
		public String resetPasswordDo(@ModelAttribute("user") User user, @ModelAttribute("emailUrl") EmailUrl emailUrl, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
			logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_DO_MAPPING +" #########");
			String code= KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_DO;
			if(StringUtils.isNotEmpty(code)){
				model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
			}
			
			
			boolean pass = validateResetPasswordEmail(user,emailUrl, model);
			if(pass){
				try {
					 user.setEmail(emailUrl.getReciever());
					 String email = user.getEmail();
					 MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user,fileInfoBusiness.getEmailStoredPath(), request);
					 
					userBusiness.resetPassword(user,emailUrl,mailMessage);
					model.addAttribute("msg", "注册邮箱为【"+email+"】的用户密码重置成功");
					logger.info("重置成功");
				} catch (BusinessException e) {
					logger.error(e.getCode(),e);
					model.addAttribute("resetPassword@email@invalid", e.getMessage());
				}
			}
			model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
			return "forgetPassword2UI";

		}
		
		@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_UI_MAPPING + "/{urlCode}")
		public String resetPasswordByEmailUI(@PathVariable("urlCode")String urlCode,@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
			logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING +" #########");
			String code= KylinboyCoreDeveloperConstant.KYLINBOY_FORGET_PASSWORD_EMAIL_UI;
			if(StringUtils.isNotEmpty(code)){
				model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
			}
			model.addAttribute("navigatMenuTopList",navigatMenuTopList);
			
			if(StringUtils.isEmpty(urlCode)){
				model.addAttribute("invalid@path","非法路径");
			}
			
			EmailUrl emailUrl = emailUrlBusiness.findByUrlCode(urlCode);
			if(null == emailUrl){
				model.addAttribute("invalid@path","非法路径");
			}else{
				Date expire = emailUrl.getExpireTime();
				Date currentTime = new Date();
				if(currentTime.after(expire)){
					model.addAttribute("expired@path","重置链接已过期，请重新申请");
				}
				
				if(emailUrl.isActived()){
					model.addAttribute("actived@path","重置链接已失效，请重新申请");
				}
			}
			
			model.addAttribute("urlCode",urlCode);
			return "forgetPasswordByEmailUI";
		}
		
		private boolean validateEmailEmpty(User user,Model model){
			String email = user.getEmail();
			if(StringUtils.isNotEmpty(email)){
				user = userBusiness.findUserByEmail(email);
				if(user == null){
					model.addAttribute("msg","不存在此注册邮箱哦，您记错啦，重新想想，点击下面按钮吧！");
					return false;
				}
			}else{
				model.addAttribute("msg","没有邮箱填入哦，点击下面按钮吧！");
				return false;
			}
			return true;
		}
		
		private boolean validateResetPasswordEmail(User user, EmailUrl emailUrl,Model model){
			boolean pass = true;
			String passwrod = user.getPassword();
			String confirmPassword = user.getConfirmPassword();
			String urlCode = emailUrl.getUrlCode();
			
			if(StringUtils.isEmpty(urlCode)){
				model.addAttribute("msg", "非法路径");
				return false;
			}
			EmailUrl emailUrlExist = emailUrlBusiness.findByUrlCode(urlCode);
			if(null == emailUrlExist){
				
				model.addAttribute("msg", "非法路径");
				pass = false;
				return false;
			}else{
				boolean actived = emailUrlExist.isActived();
				if(actived){
					model.addAttribute("msg", "链接已失效");
					return false;
				}
				Date expired = emailUrlExist.getExpireTime();
				Date activeTime = emailUrlExist.getActiveTime();
				Date date = new Date();
				if(date.after(expired) || activeTime!=null){
					model.addAttribute("msg", "链接已失效");
					pass = false;
				}
				emailUrl.setReciever(emailUrlExist.getReciever());
			}
			
			
			if(StringUtils.isEmpty(passwrod)){
				model.addAttribute("resetPassword@passwrod@empty", "New Passwrod不能为空");
				return false;
			}
			
			if(StringUtils.isEmpty(confirmPassword)){
				model.addAttribute("resetPassword@confirmPassword@empty", "Confirm Password 不能为空");
				return false;
			}
			
			if(StringUtils.isNotEmpty(passwrod) && StringUtils.isNotEmpty(confirmPassword) && !passwrod.equals(confirmPassword)){
				model.addAttribute("resetPassword@passwrod@confirmPassword@conflict", "New Passwrod 与 Confirm Password不一致");
				return false;
			}
			
			return pass;
		}
	
}
