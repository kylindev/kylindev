package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.service.siteinfo.GalleryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("galleryBusiness")
public class GalleryBusinessImpl implements GalleryBusiness{

	@Resource
	private GalleryService galleryService;
	
	@Override
	public List<Gallery> getGalleryList() {
		return galleryService.getGalleryList(false, false);
	}
	
	@Transactional
	@Override
	public Gallery persist(Gallery gallery){
		
		return galleryService.save(gallery);
	}
	
	@Transactional
	@Override
	public Gallery merge(Gallery gallery){
		
		Gallery old = galleryService.findOne(gallery.getId());
		
		BeanUtils.copyProperties(gallery, old);
		
		return galleryService.save(old);
	}

	@Override
	public Gallery getGalleryById(Long id) {
		return galleryService.findOne(id);
	}

	@Override
	public Gallery deleteGalleryById(Long id) {
		Gallery old = galleryService.findOne(id);
		old.setDeleted(true);
		return galleryService.save(old);
	}

}
