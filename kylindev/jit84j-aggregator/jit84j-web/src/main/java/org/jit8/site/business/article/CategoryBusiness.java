package org.jit8.site.business.article;

import java.util.List;

import org.jit8.site.persist.domain.article.Category;



public interface CategoryBusiness {

	public List<Category> getCategoryList();
	public Category persist(Category category);
	public Category merge(Category category);
	
	public Category getCategoryById(Long id);
	
	public Category deleteCategoryById(Long id);
	
	public List<Category> getCategoryListByIds(String categoryIdList);
	
	public List<Category> getCategoryListByParentNull();
	
	public List<Category> saveCategoryList(Category category);
	
}
