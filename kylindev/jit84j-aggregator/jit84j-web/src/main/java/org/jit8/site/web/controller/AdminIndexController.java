package org.jit8.site.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.ServletException;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller
public class AdminIndexController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(AdminIndexController.class);
	
	@Resource
	private NavigationBusiness navigationBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws ServletException {
		binder.registerCustomEditor(byte[].class,
				new ByteArrayMultipartFileEditor());

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}
	
	@RequestMapping({KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING})
	public String index(Model model) throws IOException {
		logger.debug("######### /admin/index #########");
		
		model.addAttribute("navigationTopList",getNavigationTopList());
		
		model.addAttribute("galleryList", getGalleryList());
		
		model.addAttribute("navigation",getCurrentNavigation(KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING));
		
		return "adminIndex";
	}
	
	@RequestMapping({KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_SUMMARY_MAPPING})
	public String adminIndex(Model model) throws IOException {
		logger.debug("######### /admin/index #########");
		
		return index(model);
	}
	
	@RequestMapping({KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING_SIMPLE})
	public String indexSimple(Model model) throws IOException {
		logger.debug("######### /admin/index #########");
		
		return index(model);
	}
}
