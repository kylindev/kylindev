package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;


public interface FileInfoConfigService extends GenericService<FileInfoConfig, Long>{

	public List<FileInfoConfig> getFileInfoConfigList(boolean disable, boolean deleted);
}
