package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.PermissionCategoryDao;
import org.jit8.user.persist.dao.userinfo.PermissionDao;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.jit8.user.service.userinfo.PermissionCategoryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PermissionCategoryServiceImpl extends GenericServiceImpl<PmnCategory, Long> implements PermissionCategoryService {

	@Resource
	private PermissionCategoryDao permissionCategoryDao;
	
	@Resource
	private PermissionDao permissionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = permissionCategoryDao;
	}


	@Override
	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable) {
		Page<PmnCategory> page = permissionCategoryDao.getPermissionCategoryList(pageable);
		List<PmnCategory> list = page.getContent();
		if(null != list && list.size()>0){
			setPermissionList(list);
		}
		return page;
	}
	
	private void setPermissionList(List<PmnCategory> permissionCategoryList){
		
		if(null != permissionCategoryList && permissionCategoryList.size()>0){
			for(PmnCategory pmnCategory : permissionCategoryList){
				List<Permission> permissionList = pmnCategory.getPermissionList();
				if(null != permissionList && permissionList.size()>0){
					for(Permission permission : permissionList){
						permission.setCategory(pmnCategory);
						permission.setCategoryId(pmnCategory.getId());
					}
				}
				setPermissionList(pmnCategory.getChildrenCategory());
			}
		}
	}


	@Override
	public void removeAllPermissionFromCategory(
			Long permissionId) {
		 permissionCategoryDao.removeAllPermissionFromCategory(permissionId);
	}

	
}
