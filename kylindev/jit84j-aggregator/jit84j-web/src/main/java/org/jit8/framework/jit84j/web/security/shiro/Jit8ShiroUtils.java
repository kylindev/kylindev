package org.jit8.framework.jit84j.web.security.shiro;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.eis.MemorySessionDAO;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.stereotype.Component;

@Component
public class Jit8ShiroUtils {
	
	private static DefaultWebSecurityManager securityManager;

	@Resource
	private MemorySessionDAO sessionDAO = null;
	
	private static MemorySessionDAO memorySessionDAO = null;
	
	public Jit8ShiroUtils() {
	}
	
	@PostConstruct
	private void init(){
		memorySessionDAO = this.sessionDAO;
	}

	public static Collection<Session> getActiveSessions(){
		
		Collection<Session> sessions = null;
		
		if(null != memorySessionDAO){
			sessions = memorySessionDAO.getActiveSessions();
		}
		
		return sessions;
	}
	
	public static Collection<User> getActiveUser(){
		Collection<User> users = new ArrayList<User>();
		Collection<Session> sessions = getActiveSessions();
		if(null != sessions){
			for(Session session : sessions){
				User user = Jit8ShareInfoUtils.getUser();
				if(null != user){
					users.add(user);
				}
			}
		}
		return users;
	}
	
	public static Collection<Long> getActiveUserId(){
		Collection<Long> userIds = new ArrayList<Long>();
		Collection<Session> sessions = getActiveSessions();
		if(null != sessions){
			for(Session session : sessions){
				User user = Jit8ShareInfoUtils.getUser();
				if(null != user){
					userIds.add(user.getId());
				}
			}
		}
		return userIds;
	}
}
