package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.SiteInfoDao;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.service.siteinfo.SiteInfoService;
import org.springframework.stereotype.Service;

@Service
public class SiteInfoServiceImpl extends GenericServiceImpl<SiteInfo, Long> implements SiteInfoService {

	@Resource
	private SiteInfoDao siteInfoDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = siteInfoDao;
	}


	@Override
	public List<SiteInfo> getSiteInfoList(boolean disable, boolean deleted) {
		return siteInfoDao.getSiteInfoList(disable, deleted);
	}


	@Override
	public SiteInfo getSiteInfoByType(String type) {
		return siteInfoDao.getSiteInfoByType(type);
	}
	
}
