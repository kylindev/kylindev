package org.jit8.site.travel.biz.service.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.menu.managerarea.ManageAreaItemService;
import org.jit8.site.travel.persist.dao.menu.managerarea.ManageAreaItemDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.springframework.stereotype.Service;

@Service
public class ManageAreaItemServiceImpl extends GenericServiceImpl<ManageAreaItem, Long> implements ManageAreaItemService {

	@Resource
	private ManageAreaItemDao manageAreaItemDao;
	
	
	@PostConstruct
	public void initRepository(){
		this.genericDao = manageAreaItemDao;
	}


	@Override
	public List<ManageAreaItem> findAllAvailable() {
		return manageAreaItemDao.findAllAvailable();
	}


	@Override
	public List<ManageAreaItem> findAllDisplay() {
		
		return manageAreaItemDao.findAllDisplay();
	}


	@Override
	public List<ManageAreaItem> findByIdList(List<Long> idList) {
		return manageAreaItemDao.findByIdList(idList);
	}


	
}
