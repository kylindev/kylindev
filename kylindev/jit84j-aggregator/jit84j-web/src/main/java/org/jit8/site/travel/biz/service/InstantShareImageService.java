package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface InstantShareImageService extends GenericService<InstantShareImage, Long>{

	public List<InstantShareImage> getInstantShareImageList(boolean disable, boolean deleted);
	
	public InstantShareImage findByCode(String code);
	
	public List<InstantShareImage> findByIds(List<Long> idList);
	
	public void removeByIdAndUserId(Long id, Long userId);
	
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable);
	
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable);
}
