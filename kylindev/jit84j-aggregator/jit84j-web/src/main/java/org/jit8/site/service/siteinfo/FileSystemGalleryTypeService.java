package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;


public interface FileSystemGalleryTypeService extends GenericService<FileSystemGalleryType, Long>{

	public List<FileSystemGalleryType> getFileSystemGalleryTypeList(boolean disable, boolean deleted);
	
}
