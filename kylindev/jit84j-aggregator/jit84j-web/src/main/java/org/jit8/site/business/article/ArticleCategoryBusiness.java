package org.jit8.site.business.article;

import java.util.List;

import org.jit8.site.persist.domain.article.ArticleCategory;



public interface ArticleCategoryBusiness {

	public List<ArticleCategory> getArticleCategoryList();
	public ArticleCategory persist(ArticleCategory articleCategory);
	
	public ArticleCategory merge(ArticleCategory articleCategory);
	
	public ArticleCategory getArticleCategoryById(Long id);
	
	public ArticleCategory deleteArticleCategoryById(Long id);
	
	public List<ArticleCategory> asignCategory(ArticleCategory articleCategory);
	
	public List<ArticleCategory> persist(List<ArticleCategory> articleCategoryList);
	
	
}
