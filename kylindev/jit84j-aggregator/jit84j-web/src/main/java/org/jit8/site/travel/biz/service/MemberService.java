package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.Member;


public interface MemberService extends GenericService<Member, Long>{

	public List<Member> getMemberList(boolean disable, boolean deleted);
	
	public Member findByCode(String code);
	
	public List<Member> findByIds(List<Long> idList);
}
