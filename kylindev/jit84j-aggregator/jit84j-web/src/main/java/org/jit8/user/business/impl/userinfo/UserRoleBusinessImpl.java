package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.shiro.util.ClassUtils;
import org.jit8.user.business.userinfo.UserRoleBusiness;
import org.jit8.user.persist.domain.userinfo.Role;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.jit8.user.service.userinfo.RoleService;
import org.jit8.user.service.userinfo.UserRoleService;
import org.jit8.user.service.userinfo.UserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userRoleBusiness")
public class UserRoleBusinessImpl implements UserRoleBusiness{

	@Resource
	private UserRoleService userRoleService;

	@Resource
	private RoleService roleService;
	
	@Resource
	private UserService userService;
	
	@Transactional
	@Override
	public void asignRolesForUser(UserRole userRole) {
		
		User currentUser = userRole.getUser();
		List<UserRole> userRoleListOld = currentUser.getRoleList();//userRoleService.findUserRoleList(userId);
		Role maxLevelRole = getMaxLevelRole(userRoleListOld);
		int maxLevelRoleLevel = 0;
		if(null != maxLevelRole){
			maxLevelRoleLevel = maxLevelRole.getRoleLevel();
		}
		List<Long> roleIdList = userRole.getRoleIdList();
		
		List<Long> userIdList = userRole.getUserIdList();

		Long userId = currentUser.getId();
		
		if(null!= userIdList && userIdList.size()>0){
			userIdList.remove(userId);//不能修改自已的角色
			if(userIdList.size()>0){
				userRoleService.removeByIdList(userIdList);//删除所有角色
			}
		}
		
		List<UserRole> newUserRoleList = new ArrayList<UserRole>();
		if(null != roleIdList && roleIdList.size()>0){
		
			List<Role> roleList = roleService.getRoleListByRoleIdList(roleIdList);
			List<User> userList = null;
			if(null != userIdList && userIdList.size()>0){
				userList = userService.getUserListByUserIdList(userIdList);
				for(Role role : roleList){
					if(role.getRoleLevel() <= maxLevelRoleLevel){
						
						if(null != userList && userList.size()>0){
							for(User user : userList){
								UserRole newUserRole = new UserRole();
								newUserRole.setRole(role);
								newUserRole.setUser(user);
								newUserRoleList.add(newUserRole);
							}
						}
					}
				}
				userRoleService.save(newUserRoleList);
			}
		}
	}
	
	
	private Role getMaxLevelRole(List<UserRole> userRoleList){
		Role maxLevelRole = null;
		if(null != userRoleList && userRoleList.size()>0){
			for(UserRole userRole : userRoleList){
				Role role1 = userRole.getRole();
				maxLevelRole = role1;
				int level1 = role1.getRoleLevel();
				for(UserRole userRole2 : userRoleList){
					Role role2 = userRole2.getRole();
					int level2 = role2.getRoleLevel();
					if(level1<level2){
						maxLevelRole = role2;
						level1=level2;
					}
				}
				break;
			}
		}
		return maxLevelRole;
	}
	
	public static void main(String[] args){
		UserRole newUserRole = (UserRole) ClassUtils.newInstance(UserRole.class);
		System.out.println(newUserRole);
	}


	@Transactional
	@Override
	public void removeRolesForUser(UserRole userRole) {
		
		List<Long> userIdList = userRole.getUserIdList();
		if(null!= userIdList && userIdList.size()>0){
			userRoleService.removeByIdList(userIdList);//删除所有角色
		}
	}

}
