package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.PermissionDao;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.service.userinfo.PermissionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl extends GenericServiceImpl<Permission, Long> implements PermissionService {

	@Resource
	private PermissionDao permissionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = permissionDao;
	}


	@Override
	public Permission findPermissionByResourceUrl(String resourceUrl) {
		return permissionDao.findPermissionByResourceUrl(resourceUrl);
	}


	@Override
	public Permission findPermissionByCode(String code) {
		return permissionDao.findPermissionByCode(code);
	}


	@Override
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls) {
		return permissionDao.findPermissionByResourceUrls(resourceUrls);
	}


	@Override
	public Page<Permission> getPermissionList(Pageable pageable) {
		return permissionDao.getPermissionList(pageable);
	}


	@Override
	public Permission fineById(Long id) {
		return permissionDao.fineById(id);
	}


	@Override
	public Page<Permission> getPermissionList(Permission permission,
			Pageable pageable) {
		return permissionDao.getPermissionList(permission,pageable);
	}


	@Override
	public Permission findByFullUrl(String fullUrl) {
		return permissionDao.findByFullUrl(fullUrl);
	}


	@Override
	public List<Permission> fineByIdList(List<Long> idList) {
		return permissionDao.findByIdList(idList);
	}
	
	
}
