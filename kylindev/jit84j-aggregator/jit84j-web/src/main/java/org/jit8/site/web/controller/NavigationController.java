package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NavigationController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(NavigationController.class);
	
	@Resource
	private NavigationBusiness navigationBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	/*@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING)
	public String navigationAddUI(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/navigation/addUI #########");
		
		return navigationAddUIWithId( KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI,navigation, result,  model);
	}*/
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING )
	public String navigationAddUIWithId(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/navigation/addUI #########");
		
		model.addAttribute("navigation", new Navigation());
		model.addAttribute("navigationTopList",navigationTopList);
		
		navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI);
		model.addAttribute("currentNavigation",navigation);
		
		
		return "navigationAddUI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_MAPPING )
	public String navigationAdd(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_MAPPING +" #########");
		Navigation parent = navigation.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				navigation.setParent(null);
			}
		}
		try {
			navigationBusiness.persist(navigation);
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
		}
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_MAPPING)
	public String navigationModify(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_MAPPING+" #########");
		Navigation parent = navigation.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				navigation.setParent(null);
			}
		}
		try {
			navigationBusiness.merge(navigation);
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
		}
		return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_MAPPING;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING + "{code}")
	public String navigationModifyUI(@PathVariable("code") String code,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING + " #########");
		String currentCode = 	KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI;
		if(code!= null && code.length()>0){
				Navigation navigation = getCurrentNavigationByCode(code);
				Navigation currentNavigation = getCurrentNavigationByCode(currentCode);
				model.addAttribute("navigation",navigation);
				model.addAttribute("currentNavigation",currentNavigation);
				model.addAttribute("navigationTopList",navigationTopList);
			}
		return "navigationModifyUI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_MAPPING)
	public String navigationListUI(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigation/navigationListUI #########");
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI));
		
		List<Navigation> navigationList = navigationBusiness.getListByTopCode();
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("navigationList",navigationList);

		model.addAttribute("galleryList", galleryList);
		return "navigationListUI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_MAPPING )
	public String navigationManager(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigation/navigationListUI #########");
		
		return navigationListWithId(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI, navigation, result,  model);
	}

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_MAPPING + "{code}")
	public String navigationListWithId(@PathVariable("code")String code,@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigation/navigationListUI #########");
		List<Navigation> navigationList2 = navigationBusiness.getNavigationList();
		model.addAttribute("navigationList2",navigationList2);
		if(code != null && code.length()>0){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",navigationTopList);

		model.addAttribute("galleryList", galleryList);
		return "navigationListUI";
	}
	
	@RequestMapping("/admin/menu/navigation/navigationAdmin")
	public String navigationAdmin(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigation/navigationListUI #########");
		List<Navigation> navigationList2 = navigationBusiness.getNavigationList();
		model.addAttribute("navigationList2",navigationList2);
		model.addAttribute("currentNavigation",getCurrentNavigation("/admin/menu/navigation/navigationAdmin"));
		
		model.addAttribute("navigationTopList",navigationTopList);

		model.addAttribute("galleryList", galleryList);
		return "navigationListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_MAPPING)
	public String navigationListModify(@ModelAttribute("navigation") Navigation navigation,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigation/navigationListUI #########");
		List<Navigation> navigationList2 = navigationBusiness.saveNavigationList(navigation);
		model.addAttribute("navigationList2",navigationList2);
		
		model.addAttribute("navigationTopList",navigationTopList);

		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI));
		return "navigationListUI";
	}
	
	
	
	@RequestMapping("/admin/navigation/addChilrenNavigation")
	public String addChilrenNavigation(@PathVariable("currentNavigationId") Long currentNavigationId, @PathVariable("childrenNavigationIds") Long[] childrenNavigationIds, BindingResult result, Model model) throws IOException {
		System.out.println(currentNavigationId);
		
		return "";
	}
	
	
}
