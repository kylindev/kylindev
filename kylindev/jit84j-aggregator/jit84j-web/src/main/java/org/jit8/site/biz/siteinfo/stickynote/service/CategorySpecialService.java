package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface CategorySpecialService extends GenericService<CategorySpecial, Long>{

	public List<CategorySpecial> getCategorySpecialList(boolean disable, boolean deleted);
	
	public void removeBySpecialIdAndCategoryIdList(Long specialId,
			List<Long> categoryIdList);
	
	public List<CategorySpecial> findByIdList(List<Long> idList);
	
	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,Pageable pageable) ;
	
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id);
	
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList);
}
