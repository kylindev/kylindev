package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface FileSystemGalleryTypeBusiness {

	public List<FileSystemGalleryType> getList();
	public FileSystemGalleryType persist(FileSystemGalleryType fileSystemGalleryType);
	public FileSystemGalleryType merge(FileSystemGalleryType fileSystemGalleryType);
	
	public FileSystemGalleryType findById(Long id);
	
	public FileSystemGalleryType deleteById(Long id);
	
	public Page<FileSystemGalleryType> findAll( Pageable pageable);
	
	public void removeById(Long id);
	
}
