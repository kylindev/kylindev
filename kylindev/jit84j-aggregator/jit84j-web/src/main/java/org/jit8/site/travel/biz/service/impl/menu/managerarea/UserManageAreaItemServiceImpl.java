package org.jit8.site.travel.biz.service.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.menu.managerarea.UserManageAreaItemService;
import org.jit8.site.travel.persist.dao.menu.managerarea.UserManageAreaItemDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.springframework.stereotype.Service;

@Service
public class UserManageAreaItemServiceImpl extends GenericServiceImpl<UserManageAreaItem, Long> implements UserManageAreaItemService {

	@Resource
	private UserManageAreaItemDao userManageAreaItemDao;
	
	
	@PostConstruct
	public void initRepository(){
		this.genericDao = userManageAreaItemDao;
	}


	@Override
	public List<UserManageAreaItem> findByUserId(Long userId) {
		return userManageAreaItemDao.findByUserId( userId);
	}


	@Override
	public List<UserManageAreaItem> findAllDisplayByUserId(Long userId) {
		return userManageAreaItemDao.findAllDisplayByUserId(userId);
	}


	
}
