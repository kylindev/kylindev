package org.jit8.framework.jit84j.web.security.shiro;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.InactiveAccountException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.PasswordEmptyException;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.jit8.framework.jit84j.core.trasient.model.Jit8InitSystemConstant;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.core.web.util.EncodeUtil;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.core.web.util.ThreadLocalShareInfo;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.RolePermission;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.springframework.transaction.annotation.Transactional;

/** 
 * 自定义的指定Shiro验证用户登录的类 
 * @see 在本例中定义了2个用户:jadyer和玄玉,jadyer具有admin角色和admin:manage权限,玄玉不具有任何角色和权限 
 * @create Sep 29, 2013 3:15:31 PM 
 * @author 玄玉<http://blog.csdn.net/jadyer> 
 */  
public class Jit8DefaultRealm extends AuthorizingRealm {  
	
	public static final String JIT8_DEFAULT_REAL = "jit8DefaultReal";
	
	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private PermissionBusiness permissionBusiness;
	
	/** 
     * 为当前登录的Subject授予角色和权限 
     * @see 经测试:本例中该方法的调用时机为需授权资源被访问时 
     * @see 经测试:并且每次访问需授权资源时都会执行该方法中的逻辑,这表明本例中默认并未启用AuthorizationCache 
     * @see 个人感觉若使用了Spring3.1开始提供的ConcurrentMapCache支持,则可灵活决定是否启用AuthorizationCache 
     * @see 比如说这里从数据库获取权限信息时,先去访问Spring3.1提供的缓存,而不使用Shior提供的AuthorizationCache 
     */  
	@Transactional(readOnly=true)
    @Override  
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals){  
        //获取当前登录的用户名,等价于(String)principals.fromRealm(this.getName()).iterator().next()  
      String currentUsername = (String)super.getAvailablePrincipal(principals);  
      List<String> roleList2 = new ArrayList<String>();  
      List<String> permissionList2 = new ArrayList<String>();  
      //从数据库中获取当前登录用户的详细信息  
      User user = userBusiness.findUserByEmail(currentUsername);//userService.getByUsername(currentUsername);  
      if(null != user){  
    	  List<UserPermission> userPermissionList = user.getPermissionList();
    	  if(null != userPermissionList && userPermissionList.size()>0){
    		  for(UserPermission userPermission : userPermissionList){
    			  if(!userPermission.isDisable()){
	    			  Permission permission = userPermission.getPermission();
	    			  if(!permission.isDisable()){
		    			  //String code = permission.getCode();
		            	  //String opration = permission.getOpration();
		            	  String fullUrl = permission.getFullUrl();
		                  if(!org.apache.commons.lang.StringUtils.isEmpty(fullUrl)){  
		                      permissionList2.add(fullUrl);  
		                  }  
	    			  }
    			  }
    		  }
    	  }
    	  
          //实体类User中包含有用户角色的实体类信息  
    	  List<UserRole> roleList = user.getRoleList();
          if(null!=roleList && roleList.size()>0){  
              //获取当前登录用户的角色  
              for(UserRole role : roleList){  
            	  String roleCode = role.getRole().getRoleCode();
                  roleList2.add(roleCode);  
                  ThreadLocalShareInfo threadLocalShareInfo = Jit8ShareInfoUtils.getShareInfo();
                  if(threadLocalShareInfo.isSysSuperRoot() && Jit8InitSystemConstant.ROLE_CODE_SUPER.equals(roleCode)){
                	  //如果超级管理员,获取所有权限
                	  //校验超级用户是否是已删除状态，校验超级管理员是否是已删除状态
                	 if( user.isDeleted() && role.getRole().isDeleted()){
						List<Permission> permissionList = permissionBusiness.findAll(roleCode);
						for (Permission permission : permissionList) {
							String fullUrl = permission.getFullUrl();
							if (!org.apache.commons.lang.StringUtils
									.isEmpty(fullUrl)) {
								if (!permissionList2.contains(fullUrl)) {
									permissionList2.add(fullUrl);
								}
							}
						} 
                	 }
						break;
                  }else{
                  //实体类Role中包含有角色权限的实体类信息  
	                  List<RolePermission> permissionList = role.getRole().getPemissionList();
	                  if(null!=permissionList && permissionList.size()>0){  
	                      //获取权限  
	                      for(RolePermission pmss : permissionList){
	                    	  if(!pmss.isDisable()){
		                    	  Permission permission = pmss.getPermission();
		                    	  if(!permission.isDisable()){
			                    	  String fullUrl = permission.getFullUrl();
			                          if(!org.apache.commons.lang.StringUtils.isEmpty(fullUrl)){  
			                        	 // sb.append(code).append(":").append(resourceUrl).append(":").append(opration);
			                              if(!permissionList2.contains(fullUrl)){
			                            	  permissionList2.add(fullUrl);  
			                              }
			                          }  
		                    	  }
	                    	  }
	                      }  
	                  }
                  }
              }  
          }  
      }else{  
          throw new AuthorizationException();  
      }  
      //为当前用户设置角色和权限  
      SimpleAuthorizationInfo simpleAuthorInfo = new SimpleAuthorizationInfo();  
      simpleAuthorInfo.addRoles(roleList2);  
      simpleAuthorInfo.addStringPermissions(permissionList2);  
       
       return simpleAuthorInfo;  
        //若该方法什么都不做直接返回null的话,就会导致任何用户访问/admin/listUser.jsp时都会自动跳转到unauthorizedUrl指定的地址  
        //详见applicationContext.xml中的<bean id="shiroFilter">的配置  
    }  
  
      
    /** 
     * 验证当前登录的Subject 
     * @see 经测试:本例中该方法的调用时机为LoginController.login()方法中执行Subject.login()时 
     */  
    @Override  
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {  
        //获取基于用户名和密码的令牌  
        //实际上这个authcToken是从LoginController里面currentUser.login(token)传过来的  
        //两个token的引用都是一样的,本例中是org.apache.shiro.authc.UsernamePasswordToken@33799a1e  
        UsernamePasswordToken token = (UsernamePasswordToken)authcToken;  
        System.out.println("验证当前Subject时获取到token为" + ReflectionToStringBuilder.toString(token, ToStringStyle.MULTI_LINE_STYLE)); 
        char[] passwordChar = token.getPassword();
        if(null == passwordChar){
        	throw new PasswordEmptyException("密码为空");
        }
        
        String password = new String(token.getPassword());
        String username = token.getUsername();
        //String password2 = EncodeUtil.encoderByMd5With16Hex(password);
        
        User user = null;
        
        String username2 = EncodeUtil.encoderByMd5With16Hex(username);
        boolean sysSuperRoot = false;
        //1.检索用户
        //超级管理员用户，加密用户名比较
        if(Jit8InitSystemConstant.getSuperUser().getEmail().equals(username2)){
        	//username = username2;
        	//user = userBusiness.findUserByEmail(username2);
        	user = userBusiness.findUserByEmailAndPassword(username2,password); 
        	sysSuperRoot = true;
        }else{
        	//其他用户
        	user = userBusiness.findUserByEmail(username);
        	 //user = userBusiness.findUserByEmailAndPassword(username,password);  
        }
	    //2.判断是否存在
	      if(null != user){  
	          if(user.isDisable() && !sysSuperRoot){
	        	  throw new DisabledAccountException("该用户已被禁用");
	          }
	          
	          if(user.isLocked() && !sysSuperRoot){
	        	  throw new LockedAccountException("该用户已被锁住");
	          }
	          if(!user.isActived() && !sysSuperRoot){
	        	  throw new InactiveAccountException("该用户尚未激活，请登录您的注册邮箱进行激活");
	          }
	          
	         
	          //全部通过，创建授权对象
	          SimpleAuthenticationInfo authcInfo = new SimpleAuthenticationInfo(username, user.getPassword(),JIT8_DEFAULT_REAL); 
	          authcInfo.setCredentialsSalt(ByteSource.Util.bytes(username));
	          
	          ThreadLocalShareInfo shareInfo = new ThreadLocalShareInfo();
	          shareInfo.setCurrentUserOid(user.getUniqueIdentifier());
	          ApplicationContext.getContext().setShareInfo(shareInfo);
	          
	          this.setSession(Jit8ShareInfoUtils.CURRENT_USER, user);
	          shareInfo.setUser(user);
	          shareInfo.setSysSuperRoot(sysSuperRoot);
	          this.setSession("shareInfo",shareInfo);
	          
	          return authcInfo;  
	      }else{
	    	  throw new UnknownAccountException("用户不存在");
	      }
    }  
      
      
    /** 
     * 将一些数据放到ShiroSession中,以便于其它地方使用 
     * @see 比如Controller,使用时直接用HttpSession.getAttribute(key)就可以取到 
     */  
    private void setSession(Object key, Object value){  
        Subject currentUser = SecurityUtils.getSubject(); 
        //SessionKey sessionKey = currentUser.getSession();
        //SecurityUtils.getSecurityManager().getSession(sessionKey);
        if(null != currentUser){
            Session session = currentUser.getSession();
            System.out.println("Session默认超时时间为[" + session.getTimeout() + "]毫秒");  
            if(null != session){  
                session.setAttribute(key, value);  
            }  
        }  
    }  
}  