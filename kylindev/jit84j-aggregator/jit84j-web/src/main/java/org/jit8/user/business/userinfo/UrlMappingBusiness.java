package org.jit8.user.business.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.site.web.controller.command.UrlStaticMappingFillterCommand;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface UrlMappingBusiness {

	public void persist(List<UrlMapping> urlMappingList);
	
	
	public List<String> getUrlMappingListFromConstant();

	public List<UrlStaticMapping> getUrlStaticMappingModuleListFromConstant();
	
	public UrlStaticMappingFillterCommand fillterUrlStaticMapping(ConstantModel constantModel) throws BusinessException;
	
	public void persistByClazz(ConstantModel constantModel) throws BusinessException;
	
	public Page<UrlStaticMapping> findUrlStaticMapping(String moduleIdKey,Pageable pageable);
	
	public Page<UrlStaticMapping> findModule(Pageable pageable);
	
	/**
	 * 绑定一组url到权限表（以idKey）
	 * @param urlStaticMapping
	 * @throws BusinessException
	 */
	public void bindToPermissionByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException;
	
	public void unbindFromPermissionByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException;
	
	/**
	 * 关联所有url到权限表
	 * @throws BusinessException
	 */
	public void bindAllToPermissionByList() throws BusinessException;
	
	/**
	 * 绑定url到后台导航菜单
	 * @param urlStaticMapping
	 * @throws BusinessException
	 */
	public void bindToNavigationByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException;
	
	/**
	 * 绑定url到后台导航菜单
	 * @param idKeyList
	 * @throws BusinessException
	 */
	public void bindToNavigationByIdKeyList(List<String> idKeyList) throws BusinessException;
	
	/**
	 * 从导航菜单解除关联,byIdKeys
	 */
	public void unbindFromNavigationByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException;
}
