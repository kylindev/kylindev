package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.InstantShareCommentDao;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.repository.InstantShareCommentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class InstantShareCommentDaoImpl extends GenericDaoImpl<InstantShareComment, Long> implements InstantShareCommentDao {

	@Resource
	private InstantShareCommentRepository instantShareCommentRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = instantShareCommentRepository;
	}

	@Override
	public List<InstantShareComment> getInstantShareCommentList(boolean disable, boolean deleted) {
		return instantShareCommentRepository.getInstantShareCommentList(disable, deleted);
	}
	
	@Override
	public InstantShareComment findByCode(String code) {
		return instantShareCommentRepository.findByNickName( code);
	}

	@Override
	public List<InstantShareComment> findByIds(List<Long> idList) {
		return instantShareCommentRepository.findByIds(idList);
	}

	@Override
	public Page<InstantShareComment> findByInstantShareId(Long instantShareId, Pageable pageable) {
		Page<InstantShareComment> page = instantShareCommentRepository.findByInstantShareId(instantShareId, pageable);
		return page;
	}
}
