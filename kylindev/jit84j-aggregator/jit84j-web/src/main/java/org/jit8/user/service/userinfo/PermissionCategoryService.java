package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PermissionCategoryService extends GenericService<PmnCategory, Long>{

	public Page<PmnCategory> getPermissionCategoryList(Pageable pageable);
	
	public void removeAllPermissionFromCategory(
			Long permissionId);
}
