package org.jit8.site.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.solr.servlet.cache.Method;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileInfo;
import org.jit8.site.web.controller.command.FileInfoCommand;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;

@Controller
public class FileInfoController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(FileInfoController.class);
	
	@Resource
	private FileInfoBusiness fileInfoBusiness;
	
	@Resource
	private Mapper mapper;
	
	@RequestMapping("/admin/fileInfoUI/{id}")
	public String fileInfoUIById(@PathVariable Long id,Model model) throws IOException {
		logger.debug("######### /fileInfoUI #########");
		FileInfo fileInfo = new FileInfo();
		if(id>0){
			fileInfo = fileInfoBusiness.getFileInfoById(id);
		}
		model.addAttribute("fileInfo", fileInfo);
		return "fileInfoUI";
	}
	
	
	@RequestMapping("/admin/fileInfo/fileInfoUI")
	public String fileInfoUI(@ModelAttribute("fileInfo") FileInfo fileInfo,BindingResult result, Model model) throws IOException {
		logger.debug("######### /fileInfoUI #########");
		
		return "fileInfoUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_MAPPING)
	public String fileInfoIndexUI(@ModelAttribute("fileInfo") FileInfo fileInfo,Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileInfoListUI #########");
		Sort sort = new Sort(new Order(Direction.DESC,"uniqueIdentifier"));
		Pageable pageable2= new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),  sort);
		Page<FileInfo> page = fileInfoBusiness.findAll(fileInfo, pageable2);
		model.addAttribute("page", page);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI);
		model.addAttribute("currentNavigation",navigation);
		model.addAttribute("editNavigation",navigation);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("fileInfo",fileInfo);
	
		return "fileInfoIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_MAPPING)
	public String fileInfoListUI(@ModelAttribute("fileInfo") FileInfo fileInfo,Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileInfoListUI #########");
		Sort sort = new Sort(new Order(Direction.DESC,"uniqueIdentifier"));
		Pageable pageable2= new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),  sort);
		Page<FileInfo> page = fileInfoBusiness.findAll(fileInfo, pageable2);
		model.addAttribute("page", page);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI);
		model.addAttribute("currentNavigation",navigation);
		model.addAttribute("editNavigation",navigation);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("fileInfo",fileInfo);
	
		return "fileInfoListUI";
	}
	
	@RequestMapping("/admin/fileInfo/modify")
	public String fileInfoModify(@ModelAttribute("fileInfo") FileInfo fileInfo,BindingResult result, Model model) throws IOException {
		logger.debug("######### /fileInfoUI #########");
		
		if(result.hasErrors()){
			return "fileInfoUI";
		}
		
		fileInfoBusiness.persist(fileInfo);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:fileInfoListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING)
	public String fileInfoAddBasicUI(@ModelAttribute("fileInfo") FileInfo fileInfo,Model model) throws IOException {
		logger.debug("######### /admin/fileupload/basicUI #########");
		
		return "basic-plus";
		//return "batch-plus";
		//return "angularjs";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_MULTI_UPLOAD_UI_MAPPING)
	public String fileInfoAddAngularUI(@ModelAttribute("fileInfo") FileInfo fileInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_MULTI_UPLOAD_UI_MAPPING + " #########");
		
		//return "basic-plus";
		//return "batch-plus";
		//return "angularjs";
		//return "angularjs2";
		//return "angularjs9_5_7";
		//return "angularjs";
		return "angularjs_master";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_MAPPING)
	@ResponseBody
	public String fileInfoAddBasic(@ModelAttribute("fileInfo") FileInfo fileInfo,@RequestParam MultipartFile[] files, HttpServletRequest request,Model model) throws IOException {
		logger.debug("######### /fileInfoUI #########");
		if(fileInfo == null){
			fileInfo = new FileInfo();
		}
		//将file存储路径参数化
		String rootStoredPath = fileInfoBusiness.getRootStoredPath();
		//区别用户目录
		Long userId = Jit8ShareInfoUtils.getUser().getId();
		if(userId==null){
			userId=0l;
		}
		rootStoredPath = rootStoredPath + "/" + userId.toString();
		fileInfo.setFileAccessPath(rootStoredPath);
		String realPath = request.getSession().getServletContext().getRealPath(rootStoredPath);
		if(StringUtils.isEmpty(realPath)){
			realPath = request.getSession().getServletContext().getRealPath("/") + rootStoredPath;
			File file = new File(realPath);
			if(!file.exists() && file.isDirectory()){
				file.mkdirs();
			}
		}
		
		//设置上传者id
		fileInfo.setOwner(userId);
		
		fileInfo.setFiles(files);
		fileInfo.setFileRealPath(realPath);
		
		List<FileInfo> fileInfoList = null;
		List<FileInfoCommand> fileInfoCommandList = new ArrayList<FileInfoCommand>();
		try {
			
			fileInfoList = fileInfoBusiness.resolveFile(fileInfo);
			model.addAttribute("fileInfoList", fileInfoList);
			for(FileInfo f : fileInfoList){
				FileInfoCommand fc = new FileInfoCommand();
				mapper.map(f, fc);
				String deleteUrl = KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_DO_MAPPING +"/" + f.getId();
				fc.setDeleteUrl(deleteUrl);
				fc.setDeleteType(Method.GET.name());
				
				fc.setTitle("");
				fc.setDescription("");
				
				fileInfoCommandList.add(fc);
			}
			
		} catch (BusinessException e) {
			logger.error(e.getCode(),e.getMessage());
		}
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("result", fileInfoCommandList);
		map.put("success", "上传成功");
		Gson gson = new Gson(); 
		String json = gson.toJson(map);
		logger.info(json);
		return json;
		
	}
	
	
	
	@RequestMapping("/admin/fileInfo/upload")
	public String fileInfoAdd(@ModelAttribute("fileInfo") FileInfo fileInfo,@RequestParam MultipartFile[] files, HttpServletRequest request,Model model) throws IOException {
		logger.debug("######### /fileInfoUI #########");
		if(fileInfo == null){
			fileInfo = new FileInfo();
		}
		//将file存储路径参数化
		String rootStoredPath = fileInfoBusiness.getRootStoredPath();
		fileInfo.setFileAccessPath(rootStoredPath);
		String realPath = request.getSession().getServletContext().getRealPath(rootStoredPath);

		fileInfo.setFileRealPath(realPath);
		
		List<FileInfo> fileInfoList;
		try {
			fileInfoList = fileInfoBusiness.resolveFile(fileInfo);
			model.addAttribute("fileInfoList", fileInfoList);
		} catch (BusinessException e) {
			logger.error(e.getCode(),e.getMessage());
		}
		
		
		return "redirect:fileInfoListUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING)
	public String fileInfoDelete(@ModelAttribute("fileInfo") FileInfo fileInfo,Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException{
		Long id = fileInfo.getId();
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING + id +" #########");
		if(null != id && id>0){
			fileInfoBusiness.deleteFileInfoById(id);
		}
		model.addAttribute("msg", "removed successfully");
		return fileInfoListUI(fileInfo,model,pageable);
	}
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_DO_MAPPING + "/{id}")
	public String fileInfoDeleteDo(@PathVariable("id")Long id,@ModelAttribute("fileInfo") FileInfo fileInfo,Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException{
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING + id +" #########");
		Map<String,Object> map = new HashMap<String,Object>();
		Gson gson = new Gson(); 
		if(id==null){
			map.put("error", "非法提交");
			String json = gson.toJson(map);
			logger.info(json);
			return json;
		}else if( id>0){
			User user = Jit8ShareInfoUtils.getUser();
			if(null == user){
				map.put("error", "尚未登录或已超时，请重新登录后重试。");
				String json = gson.toJson(map);
				logger.info(json);
				return json;
			}
			Long userId = user.getId();
			
			if(null == userId){
				map.put("error", "尚未登录或已超时，请重新登录后重试");
				String json = gson.toJson(map);
				logger.info(json);
				return json;
			}
			
			fileInfo = fileInfoBusiness.deleteFileInfoByIdAndUserId(userId,id);
			
			if(fileInfo.isDeleted()){
				map.put("success", "删除成功");
				String json = gson.toJson(map);
				logger.info(json);
				return json;
			}else{
				map.put("error", "删除失败");
				String json = gson.toJson(map);
				logger.info(json);
				return json;
			}
		}else{
			map.put("error", "删除失败");
			String json = gson.toJson(map);
			logger.info(json);
			return json;
		}
	}
	
	
}
