package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface FileSystemGalleryBusiness {

	public List<FileSystemGallery> getFileSystemGalleryList();
	public FileSystemGallery persist(FileSystemGallery gallery);
	public FileSystemGallery merge(FileSystemGallery gallery);
	
	public FileSystemGallery getFileSystemGalleryById(Long id);
	
	public FileSystemGallery deleteFileSystemGalleryById(Long id);
	
	public Page<FileSystemGallery> findAll( Pageable pageable);
	
	public void removeById(Long id);
	
}
