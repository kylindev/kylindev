package org.jit8.site.business.impl.menu;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.site.business.menu.NavigatMenuBusiness;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.service.menu.NavigatMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("navigatMenuBusiness")
public class NavigatMenuBusinessImpl implements NavigatMenuBusiness{

	private static final Logger logger = LoggerFactory.getLogger(NavigatMenuBusinessImpl.class);
	
	@Resource
	private NavigatMenuService navigatMenuService;
	

	@Override
	public List<NavigatMenu> getNavigatMenuList() {
		return navigatMenuService.getNavigatMenuList(false, false);
	}
	
	@Transactional
	@Override
	public NavigatMenu persist(NavigatMenu navigatMenu){
		if(null!= navigatMenu){
			NavigatMenu parent = navigatMenu.getParent();
			List<NavigatMenu> exist = getNavigatMenuByCode(navigatMenu.getCode());
			if(exist != null && exist.size()>0){
				return navigatMenu;
			}
			if(parent==null){
				List<NavigatMenu> topList = getNavigatMenuByCode(NavigatMenuService.TOP_CODE);
				NavigatMenu top = null;
				if(topList==null || topList.size()<1){
					top = new NavigatMenu();
					top.setCode(NavigatMenuService.TOP_CODE);
					top.setName(NavigatMenuService.TOP_NAME);
					top.setParent(null);
					top = navigatMenuService.save(top);
				}else{
					top = topList.get(0);
				}
				navigatMenu.setParent(top);
			}
		}
		return navigatMenuService.save(navigatMenu);
	}

	@Override
	public NavigatMenu getNavigatMenuById(Long id) {
		return navigatMenuService.findOne(id);
	}

	@Transactional
	@Override
	public NavigatMenu deleteNavigatMenuById(Long id) {
		NavigatMenu old = navigatMenuService.findOne(id);
		old.setDeleted(true);
		return navigatMenuService.save(old);
	}
	
	@Transactional
	@Override
	public NavigatMenu merge(NavigatMenu navigatMenu){
		
		NavigatMenu old = navigatMenuService.findOne(navigatMenu.getId());
		
		List<NavigatMenu> navigatMenuList = navigatMenu.getChildrenNavigatMenu();
		List<NavigatMenu> existNavigatMenuList = old.getChildrenNavigatMenu();
		if(null==existNavigatMenuList){
			existNavigatMenuList = new ArrayList<NavigatMenu>();
		}
		for(NavigatMenu existNavigatMenu : existNavigatMenuList){
			existNavigatMenu.setParent(null);
		}
		NavigatMenu parent = old.getParent();
		Long parentId = -1l;
		if(parent != null){
			parentId = parent.getId();
		}
		if(null != navigatMenuList && navigatMenuList.size()>0){
			for(NavigatMenu c : navigatMenuList){
				Long id = c.getId();
				if(id != null && id>0){
					if(id.equals(parentId)){
						continue;//检测子类别不能与父类别相同，删除相同的id
					}
					NavigatMenu navigatMenu2 = navigatMenuService.findOne(id);
					
					navigatMenu2.setParent(old);
					existNavigatMenuList.add(navigatMenu2);
				}
			}
			navigatMenu.setChildrenNavigatMenu(existNavigatMenuList);
		}
		BeanUtils.copyProperties(navigatMenu, old);
		return navigatMenuService.save(old);
	}

	@Override
	public List<NavigatMenu> getNavigatMenuListByIds( String NavigatMenuIdList) {
		if(StringUtils.isNotEmpty(NavigatMenuIdList)){
			String[] ids = NavigatMenuIdList.split(",");
			List<Long> NavigatMenuIds = new ArrayList<Long>();
			for(String id : ids){
				if(StringUtils.isNotEmpty(id)){
					NavigatMenuIds.add(Long.valueOf(id));
				}
			}
			return navigatMenuService.getNavigatMenuListByIds(false, false, NavigatMenuIds);
		}
		return null;
	}

	@Override
	public List<NavigatMenu> getNavigatMenuListByParentNull() {
		return navigatMenuService.getNavigatMenuListByParentNull();
	}
	
	@Transactional
	@Override
	public List<NavigatMenu> saveNavigatMenuList(NavigatMenu navigatMenu){
		List<NavigatMenu> persitedNavigatMenuList = new ArrayList<NavigatMenu>();
		if(null != navigatMenu){
			List<NavigatMenu> navigatMenuList = navigatMenu.getNavigatMenuList();
			if(null != navigatMenuList && navigatMenuList.size()>0){
				for(NavigatMenu c : navigatMenuList){
					Long id = c.getId();
					if(null != id && id>0){
						NavigatMenu current = navigatMenuService.findOne(id);
						current.setCode(c.getCode());
						current.setName(c.getName());
						current.setSequence(c.getSequence());
						current.setUrl(c.getUrl());
						current.setIconPath(c.getIconPath());
						persitedNavigatMenuList.add(current);
					}else{
						persitedNavigatMenuList.add(c);
					}
					
				}
				persitedNavigatMenuList = (List<NavigatMenu>) navigatMenuService.save(persitedNavigatMenuList);
			}
		}
		return persitedNavigatMenuList;
	}

	@Override
	public List<NavigatMenu> getNavigatMenuByCode(String code) {
		return navigatMenuService.getNavigatMenuByCode(false, false, code);
	}
	
	@Override
	public List<NavigatMenu> getTopNavigatMenuByCode() {
		return navigatMenuService.getNavigatMenuByCode(false, false, NavigatMenuService.TOP_CODE);
	}

	@Override
	public List<NavigatMenu> getNavigatMenuByTopCode() {
		return navigatMenuService.getNavigatMenuByTopCode(false, false, NavigatMenuService.TOP_CODE);
	}

	@Override
	public NavigatMenu findByCode(String code){
		return navigatMenuService.findByCode(code);
	}

}
