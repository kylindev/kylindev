package org.jit8.site.web.common.utils;

import java.io.File;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.user.persist.domain.userinfo.User;

public class MailMessageUtils {

	public MailMessageUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static MailMessage getEmailHtmlStoredRealPath(User user,FileInfoBusiness fileInfoBusiness , HttpServletRequest request){
			MailMessage mailMessage = new MailMessage();
			//将file存储路径参数化
			String rootStoredPath = fileInfoBusiness.getEmailStoredPath();
			//区别用户目录
			if(user == null){
				user = Jit8ShareInfoUtils.getUser();
			}
			Long userId = 0l;
			if (null != user) {
				userId = user.getId();
				if (userId == null) {
					userId = 0l;
				}
			}
			rootStoredPath = rootStoredPath + "/" + userId.toString() ;
			mailMessage.setFileAccessPath(rootStoredPath);
			String realPath = request.getSession().getServletContext().getRealPath(rootStoredPath);
			if(StringUtils.isEmpty(realPath)){
				realPath = request.getSession().getServletContext().getRealPath("/") + rootStoredPath;
				File file = new File(realPath);
				if(!file.exists()){
					file.mkdirs();
				}
			}
			mailMessage.setFileRealPath(realPath);
			return mailMessage;
}

}
