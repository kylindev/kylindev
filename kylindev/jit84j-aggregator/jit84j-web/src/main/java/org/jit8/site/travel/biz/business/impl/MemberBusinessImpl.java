package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.MemberBusiness;
import org.jit8.site.travel.biz.service.MemberService;
import org.jit8.site.travel.persist.domain.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("memberBusiness")
public class MemberBusinessImpl implements MemberBusiness{

	@Resource
	private MemberService memberService;
	

	@Override
	public List<Member> getMemberList() {
		return memberService.getMemberList(false, false);
	}
	
	@Transactional
	@Override
	public Member persist(Member member){
		Long id = member.getId();
		Member exist = null;
		if(id != null && id>0){
			 exist = memberService.findOne(member.getId());
			 exist.setSequence(member.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=member;
		}
		
		return memberService.save(exist);
	}

	@Override
	public Member getMemberById(Long id) {
		return memberService.findOne(id);
	}

	@Override
	public Member deleteMemberById(Long id) {
		Member old = memberService.findOne(id);
		old.setDeleted(true);
		return memberService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			memberService.delete(id);
		}
	}

	@Override
	public Page<Member> findAll(Pageable pageable) {
		return memberService.findAll(pageable);
	}

	@Override
	public Member findByCode(String code) {
		return memberService.findByCode( code);
	}

	@Override
	public List<Member> findByIds(List<Long> idList) {
		return memberService.findByIds(idList);
	}

}
