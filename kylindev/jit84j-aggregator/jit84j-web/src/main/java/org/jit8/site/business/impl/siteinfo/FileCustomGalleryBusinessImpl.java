package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.FileCustomGalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.jit8.site.service.siteinfo.FileCustomGalleryService;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("fileCustomGalleryBusiness")
public class FileCustomGalleryBusinessImpl implements FileCustomGalleryBusiness{

	@Resource
	private FileCustomGalleryService fileCustomGalleryService;
	
	@Override
	public List<FileCustomGallery> getFileCustomGalleryList() {
		return fileCustomGalleryService.getFileCustomGalleryList(false, false);
	}
	
	@Transactional
	@Override
	public FileCustomGallery persist(FileCustomGallery gallery){
		
		return fileCustomGalleryService.save(gallery);
	}
	
	@Transactional
	@Override
	public FileCustomGallery merge(FileCustomGallery gallery){
		
		FileCustomGallery old = fileCustomGalleryService.findOne(gallery.getId());
		
		BeanUtils.copyProperties(gallery, old);
		
		return fileCustomGalleryService.save(old);
	}

	@Override
	public FileCustomGallery getFileCustomGalleryById(Long id) {
		return fileCustomGalleryService.findOne(id);
	}

	@Override
	public FileCustomGallery deleteFileCustomGalleryById(Long id) {
		FileCustomGallery old = fileCustomGalleryService.findOne(id);
		old.setDeleted(true);
		return fileCustomGalleryService.save(old);
	}
	
	public FileCustomGallery getDefaultCustomGallery(){
		FileCustomGallery fileCustomGallery = fileCustomGalleryService.findByCodeAndType(FileCustomGallery.DEFAULT_CALLERY_CODE, FileCustomGallery.DEFAULT_CALLERY_TYPE);
		if(null == fileCustomGallery){
			fileCustomGallery = new FileCustomGallery();
			fileCustomGallery.setCode(FileCustomGallery.DEFAULT_CALLERY_CODE);
			fileCustomGallery.setName(FileCustomGallery.DEFAULT_CALLERY_NAME);
			fileCustomGallery.setType(FileCustomGallery.DEFAULT_CALLERY_TYPE);
			fileCustomGalleryService.save(fileCustomGallery);
		}
		return fileCustomGallery;
	}

}
