package org.jit8.user.business.impl.userinfo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.Resource;

import net.rubyeye.xmemcached.MemcachedClient;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.trasient.model.UrlContainer;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.service.menu.NavigationService;
import org.jit8.site.web.controller.command.UrlStaticMappingFillterCommand;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.business.userinfo.UrlMappingBusiness;
import org.jit8.user.developer.common.constants.CommonDeveloperInterface;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperMappingConstant;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.service.userinfo.PermissionService;
import org.jit8.user.service.userinfo.UrlMappingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("urlMappingBusiness")
public class UrlMappingBusinessImpl implements UrlMappingBusiness {

	private static final Logger logger = LoggerFactory.getLogger(UrlMappingBusinessImpl.class);
	
	@Resource
	private UrlMappingService urlMappingService;
	
	@Resource
	private PermissionService permissionService;
	
	@Resource
	private DeveloperBusiness developerBusiness;
	
	@Resource
	private PermissionBusiness permissionBusiness;
	
	@Resource
	private NavigationBusiness navigationBusiness;
	
	@Resource
	private MemcachedClient memcachedClient;
	

	/**
	 * 一键导入 url与 权限
	 */
	@Transactional
	public void persist(List<UrlMapping> urlMappingList){
		
		if(null != urlMappingList && urlMappingList.size()>0){
			//userId);
			User user = Jit8ShareInfoUtils.getUser();
			Developer developer = null;
			
			List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
			for(UrlMapping urlMapping : urlMappingList){
				
				UrlStaticMapping urlStaticMapping = urlMapping.getUrlStaticMapping();
				urlStaticMapping.setUrlMapping(urlMapping);
				String idKey = urlStaticMapping.getIdKey();
				Pattern p = Pattern.compile(CommonDeveloperInterface.IDKEY_PATTERN_DEVELOPER);
				Matcher m =p.matcher(idKey);
				if(m.find()){
					String userIdStr = m.group(1);
					if(StringUtils.isNotEmpty(userIdStr)){
						Long userId = Long.valueOf(userIdStr);
						developer = developerBusiness.findByUserId(userId);
					}
				}
				
				UrlMapping urlMappingExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
				String fullPath = urlStaticMapping.getUrlFullPath();
				Permission permission = null;
				if(null != urlMappingExist){
					fullPath = urlMappingExist.getUrlStaticMapping().getUrlFullPath();
					permission= urlMappingExist.getPermission();
				}else{
					permission = permissionService.findByFullUrl(fullPath);
					
					
					urlMappingExist = urlMapping;
					UrlStaticMapping module = urlMappingService.findModuleByIdKey(urlMapping.getUrlStaticMapping().getModule().getIdKey());
					if(null != module){
						urlMappingExist.getUrlStaticMapping().setModule(module);
					}
					
				}
				if(null == permission){
					permission = new Permission();
				}
				
				
				urlMappingSavedList.add(urlMappingExist);
				
				 
				if(null != permission){
					urlMappingExist.setPermission(permission);//建立与权限关联
					
					permission.setCode(urlStaticMapping.getUrlCode());
					permission.setName(urlStaticMapping.getUrlName());
					permission.setDescription(urlStaticMapping.getDecription());
					permission.setResourceUrl(urlStaticMapping.getUrl());
					permission.setFullUrl(urlStaticMapping.getUrlFullPath());
					if(null != developer){
						permission.setDeveloper(developer);
					}
				}
				
				if(null != urlMappingExist){
					UrlStaticMapping urlStaticMappingExist = urlMappingExist.getUrlStaticMapping();
					urlStaticMappingExist.setBindPermission(true);//建立关联,设置标识
					urlStaticMappingExist.setUrlMapping(urlMappingExist);
					urlStaticMappingExist.setUrl(urlStaticMapping.getUrl());
					urlStaticMappingExist.setUrlCode(urlStaticMapping.getUrlCode());
					urlStaticMappingExist.setUrlName(urlStaticMapping.getUrlName());
					urlStaticMappingExist.setUrlFullPath(urlStaticMapping.getUrlFullPath());
					urlStaticMappingExist.setDecription(urlStaticMapping.getDecription());
					urlStaticMappingExist.setModuleCode(urlStaticMapping.getModuleCode());
					urlStaticMappingExist.setModuleName(urlStaticMapping.getModuleName());
					
					UrlStaticMapping moduleExist = urlStaticMappingExist.getModule();
					UrlStaticMapping module = urlStaticMapping.getModule();
					if(null != moduleExist && null != module){
						moduleExist.setModuleCode(module.getModuleCode());
						moduleExist.setModuleName(module.getModuleName());
						moduleExist.setUrlCode(module.getUrlCode());
						moduleExist.setUrlName(module.getUrlName());
					}else{
						urlStaticMappingExist.setModule(module);
					}
				}
			}
			urlMappingService.save(urlMappingSavedList);
		}
	}
	
	public List<String> getUrlMappingListFromConstant(){
		KylinboyCoreDeveloperMappingConstant kylinboyDeveloperMappingConstant = null;
		try {
			kylinboyDeveloperMappingConstant = (KylinboyCoreDeveloperMappingConstant) Class.forName("org.jit8.user.developer.common.constants.KylinboyDeveloperMappingConstant").newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return kylinboyDeveloperMappingConstant.getUrlMappingList();
	}
	
	public List<UrlStaticMapping> getUrlStaticMappingModuleListFromConstant(){
		KylinboyCoreDeveloperMappingConstant kylinboyDeveloperMappingConstant = null;
		try {
			kylinboyDeveloperMappingConstant = (KylinboyCoreDeveloperMappingConstant) Class.forName("org.jit8.user.developer.common.constants.kylilnboy.KylinboyDeveloperMappingConstant").newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return kylinboyDeveloperMappingConstant.getUrlStaticMappingModuleList();
	}
	
	//find by idkey
	public UrlStaticMapping getUrlStaticMappingFromConstant(ConstantModel constantModel, String idKey) throws BusinessException{
		
		if(null != constantModel){
			String clazzFullName = constantModel.getClazzFullName();
			if(StringUtils.isNotEmpty(clazzFullName)){
    			try {
					Object obj = Class.forName(clazzFullName).newInstance();
					if(obj instanceof CommonDeveloperInterface){
						CommonDeveloperInterface  commonDeveloperInterface = (CommonDeveloperInterface)obj;
						Long developerUserId = commonDeveloperInterface.getDeveloperUserId();
						//检查开发者是否为注册用户
						Developer developer = developerBusiness.findByUserId(developerUserId);
						if(null == developer){
							logger.error("invalid developer:"+developerUserId);
							throw new BusinessException("error.developer.invalid","invalid developer:"+developerUserId);
						}
						UrlStaticMapping urlStaticMapping = commonDeveloperInterface.getUrlStaticMapping(idKey);
						return urlStaticMapping;
					}
				} catch (ClassNotFoundException e) {
					logger.error("class not found:"+clazzFullName,e);
				} catch (InstantiationException e) {
					logger.error("InstantiationException:",e);
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					logger.error("IllegalAccessException:",e);
				}
    		}
		}
		
		return null;
	}
	
	
public List<UrlStaticMapping> getUrlStaticMappingModuleListFromConstant(ConstantModel constantModel) throws BusinessException{
		
		if(null != constantModel){
			String clazzFullName = constantModel.getClazzFullName();
			if(StringUtils.isNotEmpty(clazzFullName)){
    			try {
					Object obj = Class.forName(clazzFullName).newInstance();
					if(obj instanceof CommonDeveloperInterface){
						CommonDeveloperInterface  commonDeveloperInterface = (CommonDeveloperInterface)obj;
						
						//检查开发者是否为注册用户
						Long developerUserId = commonDeveloperInterface.getDeveloperUserId();
						Developer developer = developerBusiness.findByUserId(developerUserId);
						if(null == developer){
							logger.error("invalid developer:"+developerUserId);
							throw new BusinessException("error.developer.invalid","invalid developer:"+developerUserId);
						}
						
						List<UrlStaticMapping> urlStaticMappingList = commonDeveloperInterface.getUrlStaticMappingModuleList();
						return urlStaticMappingList;
					}
				} catch (ClassNotFoundException e) {
					logger.error("class not found:"+clazzFullName,e);
				} catch (InstantiationException e) {
					logger.error("InstantiationException:",e);
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					logger.error("IllegalAccessException:",e);
				}
    		}
		}
		
		return null;
	}
	
	/**
	 * 单个保存
	 * @param constantModel
	 * @param idKey
	 * @throws BusinessException 
	 */
	public void persist(ConstantModel constantModel, String idKey) throws BusinessException{
		UrlStaticMapping urlStaticMapping = getUrlStaticMappingFromConstant( constantModel,  idKey);
		if(null != urlStaticMapping){
			UrlMapping urlMappingExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
			if(null != urlMappingExist){
				//如果数据库中已存在，执行更新操作
				UrlStaticMapping urlStaticMappingExist = urlMappingExist.getUrlStaticMapping();
				urlStaticMappingExist.setUrl(urlStaticMapping.getUrl());
				urlStaticMappingExist.setUrlCode(urlStaticMapping.getUrlCode());
				urlStaticMappingExist.setUrlFullPath(urlStaticMapping.getUrlFullPath());
				urlStaticMappingExist.setUrlName(urlStaticMapping.getUrlName());
				
				UrlStaticMapping moduleExist = urlStaticMappingExist.getModule();
				
				UrlStaticMapping module = urlStaticMapping.getModule();
				if(module != null){
					String moduleIdKey = module.getIdKey();
					if(StringUtils.isNotEmpty(moduleIdKey)){
						//如果当前所属module与数据库已存在的module不相同
						if(!moduleIdKey.equals(moduleExist.getIdKey())){
							UrlMapping moduleExistNew = urlMappingService.findByUrlStaticMappingIdKey(moduleIdKey);
							if(null != moduleExistNew){//如果已存在
								urlStaticMappingExist.setModule(moduleExistNew.getUrlStaticMapping());
							}else{//如果是新添加的
								urlStaticMappingExist.setModule(module);
							}
						}
					}
				}
				urlMappingService.save(urlMappingExist);
			}else{
				//如果数据库中不存在，执行添加操作
				UrlMapping urlMappingNew = new UrlMapping();
				urlMappingNew.setUrlStaticMapping(urlStaticMapping);
				//1.处理所属module, 查看module是否已存在
				UrlStaticMapping module = urlStaticMapping.getModule();
				if(module != null){
					String moduleIdKey = module.getIdKey();
					if(StringUtils.isNotEmpty(moduleIdKey)){
						UrlMapping moduleExistNew = urlMappingService.findByUrlStaticMappingIdKey(moduleIdKey);
						if(null != moduleExistNew){//如果已存在,执行module的更新
							UrlStaticMapping moduleExistNew2 = moduleExistNew.getUrlStaticMapping();
							moduleExistNew2.setModuleCode(module.getModuleCode());
							moduleExistNew2.setModuleName(module.getModuleName());
							moduleExistNew2.setUrlCode(module.getUrlCode());
							moduleExistNew2.setUrlName(module.getUrlName());
							urlStaticMapping.setModule(moduleExistNew2);
						}
					}
				}
				
				urlMappingService.save(urlMappingNew);
			}
		}
	}
	
	
	/**
	 * 保存整个模块
	 * @param constantModel
	 * @param moduleSave
	 */
	@Transactional
	public void persistByModule(ConstantModel constantModel, String moduleIdKey) throws BusinessException{
		UrlStaticMapping urlStaticMapping = getUrlStaticMappingFromConstant( constantModel,  moduleIdKey);
		if(null != urlStaticMapping){
			persistByModule(urlStaticMapping);
		}
	}
	/**
	 * 保存整个模块
	 * @param moduleSave
	 */
	@Transactional
	public void persistByModule(UrlStaticMapping moduleSave) throws BusinessException{
		String moduleIdKey = moduleSave.getIdKey();
		if(StringUtils.isNotEmpty(moduleIdKey)){
			UrlStaticMapping module = urlMappingService.findModuleByIdKey(moduleIdKey);
			
			//1.查看module是否已经存在
			if(null == module){
				module = moduleSave;
			}
			
			//2.处理每个url
			List<UrlStaticMapping> children = moduleSave.getChildUrlList();
			if(null != children && children.size()>0){
				List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
					
				for(UrlStaticMapping child : children){
					String idKey = child.getIdKey();
					
					UrlMapping urlMappingExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
					
					if(null != urlMappingExist){
						
						//查看每一个url是否已经存在，若存在则更新其值
						UrlStaticMapping urlStaticMappingExist = urlMappingExist.getUrlStaticMapping();
						urlStaticMappingExist.setUrlMapping(urlMappingExist);
						
						//检查urlFullPath是否已经存在
						String urlFullPath = child.getUrlFullPath();
						if(!urlFullPath.equals(urlStaticMappingExist.getUrlFullPath())){
							UrlMapping urlMappingFullPathExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
							if(null != urlMappingFullPathExist){
								throw new BusinessException("error.fullPathExist", "the url [idKey:" + idKey + ", urlFullPath:"+ urlFullPath +"] has exist in DB, conflict with [ idKey: "+urlMappingFullPathExist.getUrlStaticMapping().getIdKey()+"] please change the full path.");
							}
						}
						
						urlStaticMappingExist.setUrl(StringUtils.trimToNull(child.getUrl()));
						urlStaticMappingExist.setUrlCode(StringUtils.trimToNull(child.getUrlCode()));
						urlStaticMappingExist.setUrlName(StringUtils.trimToNull(child.getUrlName()));
						urlStaticMappingExist.setUrlFullPath(StringUtils.trimToNull(child.getUrlFullPath()));
						urlStaticMappingExist.setDecription(StringUtils.trimToNull(child.getDecription()));
						urlStaticMappingExist.setModuleCode(StringUtils.trimToNull(child.getModuleCode()));
						urlStaticMappingExist.setModuleName(StringUtils.trimToNull(child.getModuleName()));
						urlStaticMappingExist.setModule(module);
						urlMappingSavedList.add(urlMappingExist);
					}else{
						//如果不存在，创建一个新的
						//检查是否是module,如果父节点为空，则为module
						UrlStaticMapping moduleParent = moduleSave.getModule();
						//如果不是module，检查urlFullPath
						if(null == moduleParent){
							//检查urlFullPath是否已经存在
							String urlFullPath = moduleSave.getUrlFullPath();
							UrlMapping urlMappingFullPathExist = urlMappingService.findByUrlStaticMappingUrlFullPath(urlFullPath);
							if(null != urlMappingFullPathExist){
								throw new BusinessException("error.fullPathExist", "the url [idKey:" + idKey + ", urlFullPath:"+ urlFullPath +"] has exist in DB, conflict with [ idKey: "+urlMappingFullPathExist.getUrlStaticMapping().getIdKey()+"] please change the full path.");
							}
						}
						UrlMapping urlMappingNew = new UrlMapping();
						urlMappingNew.setUrlStaticMapping(child);//建立关联
						child.setUrlMapping(urlMappingNew);//建立关联
						child.setModule(module);
						urlMappingSavedList.add(urlMappingNew);
					}
				}
				
				if(null != urlMappingSavedList && urlMappingSavedList.size()>0){
					urlMappingService.save(urlMappingSavedList);
				}
			}
			
		}
	}
	
	
	/**
	 * 保存整个类
	 * @param constantModel
	 */
	@Transactional
	public void persistByClazz(ConstantModel constantModel) throws BusinessException{
		//获取所有的url的module
		List<UrlStaticMapping> urlStaticMappingList = getUrlStaticMappingModuleListFromConstant(constantModel);
		if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
			for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
				persistByModule(urlStaticMapping);//按module挨个保存
			}
		}
		/*if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
			
			List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
			
			for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
				
				//1.检查idKey是否合法，是否存在该开发者
				String idKey = urlStaticMapping.getIdKey();
				Pattern p = Pattern.compile(CommonDeveloperInterface.IDKEY_PATTERN_DEVELOPER);
				Matcher m =p.matcher(idKey);
				if(m.find()){
					String userIdStr = m.group(1);
					if(StringUtils.isNotEmpty(userIdStr)){
						Long developerUserId = Long.valueOf(userIdStr);
						Developer developer = developerBusiness.findByUserId(developerUserId);
						if(null == developer){
							logger.error("invalid developer:"+developerUserId);
							throw new BusinessException("error.developer.invalid","invalid developer:"+developerUserId);
						}
					}
				}
				
				//2.检查idKey是否已经存在于系统中
				UrlMapping urlMappingExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
				if(null != urlMappingExist){
					
					//查看每一个url是否已经存在，若存在则更新其值
					UrlStaticMapping urlStaticMappingExist = urlMappingExist.getUrlStaticMapping();
					
					//检查是否是module,如果moduleCode不为空，则为moduel
					String moduleCode = urlStaticMapping.getModuleCode();
					//如果不是module，检查urlFullPath
					if(StringUtils.isEmpty(moduleCode)){
						//检查urlFullPath是否已经存在
						String urlFullPath = urlStaticMapping.getUrlFullPath();
						if(!urlFullPath.equals(urlStaticMappingExist.getUrlFullPath())){
							UrlMapping urlMappingFullPathExist = urlMappingService.findByUrlStaticMappingIdKey(idKey);
							if(null != urlMappingFullPathExist){
								throw new BusinessException("error.fullPathExist", "the url [idKey:" + idKey + ", urlFullPath:"+ urlFullPath +"] has exist in DB, conflict with [ idKey: "+urlMappingFullPathExist.getUrlStaticMapping().getIdKey()+"] please change the full path.");
							}
						}
					}
					//更新其值
					urlMappingSavedList.add(urlMappingExist);
					urlStaticMappingExist.setUrl(urlStaticMapping.getUrl());
					urlStaticMappingExist.setUrlCode(urlStaticMapping.getUrlCode());
					urlStaticMappingExist.setUrlName(urlStaticMapping.getUrlName());
					urlStaticMappingExist.setUrlFullPath(urlStaticMapping.getUrlFullPath());
					urlStaticMappingExist.setDecription(urlStaticMapping.getDecription());
					urlStaticMappingExist.setModuleCode(urlStaticMapping.getModuleCode());
					urlStaticMappingExist.setModuleName(urlStaticMapping.getModuleName());
					UrlStaticMapping moduleExist = urlStaticMappingExist.getModule();
					UrlStaticMapping module = urlStaticMapping.getModule();
					if(null != moduleExist && null != module){
						moduleExist.setModuleCode(module.getModuleCode());
						moduleExist.setModuleName(module.getModuleName());
					}else{
						urlStaticMappingExist.setModule(module);
					}
				}else{
					//如果不存在，创建一个新的
					//检查是否是module,如果moduleCode不为空，则为moduel
					String moduleCode = urlStaticMapping.getModuleCode();
					//如果不是module，检查urlFullPath
					if(StringUtils.isEmpty(moduleCode)){
						//检查urlFullPath是否已经存在
						String urlFullPath = urlStaticMapping.getUrlFullPath();
						UrlMapping urlMappingFullPathExist = urlMappingService.findByUrlStaticMappingUrlFullPath(urlFullPath);
						if(null != urlMappingFullPathExist){
							throw new BusinessException("error.fullPathExist", "the url [idKey:" + idKey + ", urlFullPath:"+ urlFullPath +"] has exist in DB, conflict with [ idKey: "+urlMappingFullPathExist.getUrlStaticMapping().getIdKey()+"] please change the full path.");
						}
						
					}
					UrlMapping urlMapping = new UrlMapping();
					urlMapping.setUrlStaticMapping(urlStaticMapping);
					urlMappingSavedList.add(urlMapping);
				}
			}
			if(null != urlMappingSavedList && urlMappingSavedList.size()>0){
				urlMappingService.save(urlMappingSavedList);
			}
		}*/
	}
	
	
	
	//将url进行分类过滤，类别：已存在，新添加，要修改
	public UrlStaticMappingFillterCommand fillterUrlStaticMapping(ConstantModel constantModel) throws BusinessException{

		List<UrlStaticMapping> urlStaticMappingList = getUrlStaticMappingModuleListFromConstant(constantModel);
		UrlStaticMappingFillterCommand urlStaticMappingFillterCommand = null;
		if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
			
			List<UrlStaticMapping> existUrlStaticMappingList = new ArrayList<UrlStaticMapping>();
			
			List<UrlStaticMapping> addedUrlStaticMappingList = new ArrayList<UrlStaticMapping>();
			
			List<UrlStaticMapping> modifiedUrlStaticMappingList = new ArrayList<UrlStaticMapping>();
			
			for(UrlStaticMapping module : urlStaticMappingList){
				String idKey = module.getIdKey();
				if(StringUtils.isNotEmpty(idKey)){
					UrlStaticMapping moduleExistFillter = new UrlStaticMapping();
					copyModule(module,moduleExistFillter);
					existUrlStaticMappingList.add(moduleExistFillter);
					List<UrlStaticMapping> existChildren = new ArrayList<UrlStaticMapping>();
					moduleExistFillter.setChildUrlList(existChildren);
					
					
					UrlStaticMapping moduleAddedFillter = new UrlStaticMapping();
					addedUrlStaticMappingList.add(moduleAddedFillter);
					copyModule(module,moduleAddedFillter);
					List<UrlStaticMapping> addedChildren = new ArrayList<UrlStaticMapping>();
					moduleAddedFillter.setChildUrlList(addedChildren);
					
					UrlStaticMapping moduleModifiedFillter = new UrlStaticMapping();
					modifiedUrlStaticMappingList.add(moduleModifiedFillter);
					copyModule(module,moduleModifiedFillter);
					List<UrlStaticMapping> modifiedChildren = new ArrayList<UrlStaticMapping>();
					moduleModifiedFillter.setChildUrlList(modifiedChildren);
					
					//检查module状态
					checkAndSetModuleStatus(idKey, module, moduleExistFillter,
							moduleAddedFillter, moduleModifiedFillter);
					
					//将子节点进行归类
					List<UrlStaticMapping> children = module.getChildUrlList();
					checkAndSetChildStatus(children, existChildren, addedChildren,
							modifiedChildren);
					
				}
				
			}
			urlStaticMappingFillterCommand = new UrlStaticMappingFillterCommand();
			urlStaticMappingFillterCommand.setAddedUrlStaticMappingList(addedUrlStaticMappingList);
			urlStaticMappingFillterCommand.setExistUrlStaticMappingList(existUrlStaticMappingList);
			urlStaticMappingFillterCommand.setModifiedUrlStaticMappingList(modifiedUrlStaticMappingList);
			urlStaticMappingFillterCommand.setAllUrlStaticMappingList(urlStaticMappingList);
		}
		
		return urlStaticMappingFillterCommand;
	}

	/**
	 * @param children
	 * @param existChildren
	 * @param addedChildren
	 * @param modifiedChildren
	 */
	private void checkAndSetChildStatus(List<UrlStaticMapping> children,
			List<UrlStaticMapping> existChildren,
			List<UrlStaticMapping> addedChildren,
			List<UrlStaticMapping> modifiedChildren) {
		if(null != children && children.size()>0){
			for(UrlStaticMapping child : children){
				String childIdKey = child.getIdKey();
				logger.debug(childIdKey + "-----------------------------");
				UrlMapping urlMappingExist = urlMappingService.findByUrlStaticMappingIdKey(childIdKey);
				if(null != urlMappingExist){
					UrlStaticMapping childExist = urlMappingExist.getUrlStaticMapping();
					if(equalsChildUrlStaticMapping(childExist,child)){
						child.setStatus(UrlStaticMapping.ORIGINAL);
						existChildren.add(child);
					}else{
						child.setStatus(UrlStaticMapping.MODIFIED);
						modifiedChildren.add(child);
					}
				}else{
					child.setStatus(UrlStaticMapping.ADDED);
					addedChildren.add(child);
				}
			}
		}
	}

	private void checkAndSetModuleStatus(String moduleIdKey, UrlStaticMapping module,
			UrlStaticMapping moduleExistFillter,
			UrlStaticMapping moduleAddedFillter,
			UrlStaticMapping moduleModifiedFillter) {
		UrlStaticMapping moduleExist = urlMappingService.findModuleByIdKey(moduleIdKey);
		if(null != moduleExist){
			if(equalsModule(moduleExist,module)){
				moduleExistFillter.setStatus(UrlStaticMapping.ORIGINAL);
			}else{
				moduleModifiedFillter.setStatus(UrlStaticMapping.MODIFIED);
			}
		}else{
			moduleAddedFillter.setStatus(UrlStaticMapping.ADDED);
		}
	}
	
	
	
	private void copyModule(UrlStaticMapping old, UrlStaticMapping dest){
		if(old != null && dest != null){
			dest.setModuleCode(old.getModuleCode());
			dest.setModuleName(old.getModuleName());
			dest.setIdKey(old.getIdKey());
			dest.setUrlCode(old.getUrlCode());
			dest.setUrlName(old.getUrlName());
		}
	}
	
	private boolean equalsModule(UrlStaticMapping old, UrlStaticMapping dest){
		if(old == null && dest == null){
			return true;
		}else
		if(old != null && dest == null){
			return false;
		}else
		if(old == null && dest != null){
			return false;
		}else{
			String moduleCodeExist = StringUtils.trimToEmpty(dest.getModuleCode());
			String moduleNameExist = StringUtils.trimToEmpty(dest.getModuleName());
			String moduleIdKeyExist = StringUtils.trimToEmpty(dest.getIdKey());
			
			String moduleCode = StringUtils.trimToEmpty(old.getModuleCode());
			String moduleName = StringUtils.trimToEmpty(old.getModuleName());
			String moduleIdKey = StringUtils.trimToEmpty(old.getIdKey());
			
			if( moduleIdKeyExist.equals(moduleIdKey) && moduleCodeExist.equals(moduleCode) && moduleNameExist.equals(moduleName)){
				return true;
			}
		}
		return false;
	}
	
	private boolean equalsChildUrlStaticMapping(UrlStaticMapping old, UrlStaticMapping dest){
		if(old == null && dest == null){
			return true;
		}else if(old != null && dest != null){
			String idKeyOld =StringUtils.trimToEmpty(old.getIdKey());
			String urlOld = StringUtils.trimToEmpty(old.getUrl());
			String urlCodeOld = StringUtils.trimToEmpty(old.getUrlCode());
			String urlNameOld = StringUtils.trimToEmpty(old.getUrlName());
			String urlFullPathOld = StringUtils.trimToEmpty(old.getUrlFullPath());
			String descriptionOld = StringUtils.trimToEmpty(old.getDecription());
			
			String idKeyDest = StringUtils.trimToEmpty(dest.getIdKey());
			String urlDest = StringUtils.trimToEmpty(dest.getUrl());
			String urlCodeDest = StringUtils.trimToEmpty(dest.getUrlCode());
			String urlNameDest = StringUtils.trimToEmpty(dest.getUrlName());
			String urlFullPathDest = StringUtils.trimToEmpty(dest.getUrlFullPath());
			String descriptionDest = StringUtils.trimToEmpty(dest.getDecription());
			if(idKeyOld.equals(idKeyDest)&& urlOld.equals(urlDest) && urlCodeOld.equals(urlCodeDest)
					&& urlNameOld.equals(urlNameDest) && urlFullPathOld.equals(urlFullPathDest)
					&& descriptionOld.equals(descriptionDest)){
				return true;
			}
		}
		return false;
	}

	@Override
	public Page<UrlStaticMapping> findUrlStaticMapping(String moduleIdKey,
			Pageable pageable) {
		return urlMappingService.findUrlStaticMapping(moduleIdKey, pageable);
	}
	
	@Override
	public Page<UrlStaticMapping> findModule(Pageable pageable){
		return urlMappingService.findModule(pageable);
	}

	//关联所选url到权限
	@Override
	public void bindToPermissionByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException{
		if(null != urlStaticMapping){
			String idKeys = urlStaticMapping.getIdKeys();
			if(StringUtils.isNotEmpty(idKeys)){
				String[] idKeyArray = idKeys.split(",");
				List<String> idKeyList = null;
				if(null != idKeyArray && idKeyArray.length>0){
					idKeyList = Arrays.asList(idKeyArray);
					persistByIdKeyList(idKeyList);
				}
			}
		}
	}
	
	//目前仅支持一组url属于同一个开发者
	public void persistByIdKeyList(List<String> idKeyList) throws BusinessException{
		if(null != idKeyList && idKeyList.size()>0){
			List<UrlStaticMapping> urlStaticMappingList = urlMappingService.findByUrlStaticMappingIdKeyList(idKeyList);
			
			bindToPermissionByList(urlStaticMappingList);
		}
	}

	/**
	 * 将所有url关联到权限
	 */
	public void bindAllToPermissionByList() throws BusinessException{
		int page = 0;
		int size = 30;
		Pageable pageable = new PageRequest(page, size);
		Page<UrlStaticMapping> result = urlMappingService.findUrlStaticMappingWithoutModule(pageable);
		bindToPermissionByList(result.getContent());
		if(null != result){
			int totalPage = result.getTotalPages();
			if(totalPage>1){
				page++;
				for(;page<totalPage;page++){
					Pageable pageable2 = new PageRequest(page, size);
					Page<UrlStaticMapping> resultNext = urlMappingService.findUrlStaticMappingWithoutModule(pageable2);
					bindToPermissionByList(resultNext.getContent());
				}
			}
		}
		
	}
	
	/**
	 * 将urlMapping列表关联到权限
	 * @param urlStaticMappingList
	 * @throws BusinessException
	 */
	private void bindToPermissionByList(
			List<UrlStaticMapping> urlStaticMappingList)
			throws BusinessException {
		if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
			Developer developer = null;
			List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
			for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
				UrlMapping urlMappingExist = urlStaticMapping.getUrlMapping();
				if(urlMappingExist == null){
					urlMappingExist = new UrlMapping();
					urlMappingExist.setUrlStaticMapping(urlStaticMapping);
				}
				urlStaticMapping.setUrlMapping(urlMappingExist);
				
				//检查开发者是否存在
				String idKey = urlStaticMapping.getIdKey();
				Pattern p = Pattern.compile(CommonDeveloperInterface.IDKEY_PATTERN_DEVELOPER);
				Matcher m =p.matcher(idKey);
				if(m.find()){
					String userIdStr = m.group(1);
					if(StringUtils.isNotEmpty(userIdStr)){
						Long userId = Long.valueOf(userIdStr);
						if(null == developer || !userId.equals(developer.getUserId().getUserId())){
							developer = developerBusiness.findByUserId(userId);
						}
					}
				}
				
				//将其加入保存列表
				urlMappingSavedList.add(urlMappingExist);
				
				//根据全路径检查权限是否已存在
				String fullPath = urlStaticMapping.getUrlFullPath();
				Permission permission = urlMappingExist.getPermission();
				if(null == permission){
					permission = permissionService.findByFullUrl(fullPath);
					
					if(null == permission){
						permission = new Permission();
						permission.setStatus(Permission.STATUS_SUBMMIT);
					}
					String status = permission.getStatus();
					if(StringUtils.isEmpty(status)){
						permission.setStatus(Permission.STATUS_SUBMMIT);//设置权限状态为已提交到数据库
					}
				}
					
				 
				if(null != permission){
					urlMappingExist.setPermission(permission);//设置关联
					urlStaticMapping.setBindPermission(true);//同时，设置已绑定权限
					permission.setCode(urlStaticMapping.getUrlCode());
					permission.setName(urlStaticMapping.getUrlName());
					permission.setDescription(urlStaticMapping.getDecription());
					permission.setResourceUrl(urlStaticMapping.getUrl());
					permission.setFullUrl(urlStaticMapping.getUrlFullPath());
					if(null != developer){
						permission.setDeveloper(developer);
					}else{
						throw new BusinessException("org.jit8.100001.userinfo.developer.invalid", "invalid developer");
					}
				}
			}
			
			urlMappingService.save(urlMappingSavedList);
	}
	}
	
	
	/**
	 * 从权限解除关联,byIdKeys
	 */
	public void unbindFromPermissionByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException{
		if(null != urlStaticMapping){
			String idKeys = urlStaticMapping.getIdKeys();
			if(StringUtils.isNotEmpty(idKeys)){
				String[] idKeyArray = idKeys.split(",");
				List<String> idKeyList = null;
				if(null != idKeyArray && idKeyArray.length>0){
					idKeyList = Arrays.asList(idKeyArray);
					unbindFromPermissionByIdKeyList(idKeyList);
				}
			}
		}
	}
	
	/**
	 * 解除与权限关联
	 * @param idKeyList
	 * @throws BusinessException
	 */
	public void unbindFromPermissionByIdKeyList(List<String> idKeyList) throws BusinessException{
		if(null != idKeyList && idKeyList.size()>0){
			List<UrlStaticMapping> urlStaticMappingList = urlMappingService.findByUrlStaticMappingIdKeyList(idKeyList);
			if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
					List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
					for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
						UrlMapping urlMappingExist = urlStaticMapping.getUrlMapping();
						if(urlMappingExist == null){
							urlMappingExist = new UrlMapping();
							urlMappingExist.setUrlStaticMapping(urlStaticMapping);
							urlMappingSavedList.add(urlMappingExist);
						}
						urlStaticMapping.setUrlMapping(urlMappingExist);
						
						Permission permission = urlMappingExist.getPermission();
						if(null != permission){
							//check the permission is used for others, if uesed throw exception
							boolean isUsed = checkPermissionIsUserd( permission);
							if(isUsed){
								throw new BusinessException("org.jit8.100001.userinfo.permission.checkPermissionUsed.used","permission has used",new Object[]{permission.getName()});
							}
						}
						
						urlMappingSavedList.add(urlMappingExist);
						 
						if(null != permission){
							urlMappingExist.setPermission(null);//解除关联，置空
							urlStaticMapping.setBindPermission(false);//同时，把它设置false
						}
					}
					urlMappingService.save(urlMappingSavedList);
			}
		}
	}
	
	/**
	 * 检查权限是否被使用
	 * @param permission
	 * @return
	 */
	public boolean checkPermissionIsUserd(Permission permission){
		return permissionBusiness.checkPermissionIsUsed(permission);
	}
	
	
	
	//关联所选url到导航菜单
	@Override
	public void bindToNavigationByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException{
		if(null != urlStaticMapping){
			String idKeys = urlStaticMapping.getIdKeys();
			if(StringUtils.isNotEmpty(idKeys)){
				String[] idKeyArray = idKeys.split(",");
				List<String> idKeyList = null;
				if(null != idKeyArray && idKeyArray.length>0){
					idKeyList = Arrays.asList(idKeyArray);
					bindToNavigationByIdKeyList(idKeyList);
				}
			}
		}
	}
	/**
	 * 将urlMapping绑定导航菜单，按照idKeyList
	 * @param idKeyList
	 * @throws BusinessException
	 */
	public void bindToNavigationByIdKeyList(List<String> idKeyList) throws BusinessException{
		if(null != idKeyList && idKeyList.size()>0){
			List<UrlStaticMapping> urlStaticMappingList = urlMappingService.findByUrlStaticMappingIdKeyList(idKeyList);
			bindToNavigationByList(urlStaticMappingList);
		}
	}
	
	/**
	 * 将urlMapping列表关联到后台菜单
	 * @param urlStaticMappingList
	 * @throws BusinessException
	 */
	private void bindToNavigationByList(
			List<UrlStaticMapping> urlStaticMappingList)
			throws BusinessException {
		if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
				Developer developer = null;
				List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
				for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
					UrlMapping urlMappingExist = urlStaticMapping.getUrlMapping();
					if(urlMappingExist == null){
						urlMappingExist = new UrlMapping();
						urlMappingExist.setUrlStaticMapping(urlStaticMapping);
					}
					urlStaticMapping.setUrlMapping(urlMappingExist);
					
					//检查开发者是否存在
					String idKey = urlStaticMapping.getIdKey();
					Pattern p = Pattern.compile(CommonDeveloperInterface.IDKEY_PATTERN_DEVELOPER);
					Matcher m =p.matcher(idKey);
					if(m.find()){
						String userIdStr = m.group(1);
						if(StringUtils.isNotEmpty(userIdStr)){
							Long userId = Long.valueOf(userIdStr);
							if(null == developer || !userId.equals(developer.getUserId().getUserId())){
								developer = developerBusiness.findByUserId(userId);
								if(null == developer){
									throw new BusinessException("org.jit8.100001.userinfo.developer.invalid", "invalid developer");
								}
							}
						}
					}
					
					//将其加入保存列表
					urlMappingSavedList.add(urlMappingExist);
					
					//根据idKey检查菜单是否已存在
					Navigation navigation = urlMappingExist.getNavigation();
					if(null == navigation){
						navigation = navigationBusiness.findNavigationByIdKey(idKey);
						if(null == navigation){
							navigation = navigationBusiness.getNavigationByCodeAndDeveloper(urlStaticMapping.getUrlCode(), developer.getUserId().getUserId());
						}
					}
					if(null == navigation){
						navigation = new Navigation();
						navigationBusiness.setParentTopNavigation(navigation);
						navigation.setSequence(-1);
					}
					Navigation parent = navigation.getParent();
					if(parent==null  && !NavigationService.TOP_CODE.equals(navigation.getCode())){
						navigationBusiness.setParentTopNavigation(navigation);
					}
					//绑定后台菜单，路径要满足以/admin开头（绑定前台菜单，路径不能以/admin开头）
					//这里要检查url路径
					String urlFullPath = urlStaticMapping.getUrlFullPath();
					if(urlFullPath.indexOf(WebConstants._ADMIN_PATH_PREFIX)<0){
						throw new BusinessException("org.jit8.100001.userinfo.navigation.adminPath.invalid", "invalid admin path, admin menu must begin with '/admin'");
					}
					
					//检查是否是带参数的路径，若带参数，则要检查参数是否存在，若不存在则仅能保存但设置为禁用状态，带参数的路径必须要给定参数才能正确导航
					//并标识为incorrectUrl
					UrlContainer urlContainer = UrlContainer.getUrlContainerFromUrl(urlFullPath);
					if(StringUtils.isNotEmpty(urlContainer.getErrorCode())){
						throw new BusinessException("param.number.conflict", "navigation param num defined is not equal to acturel param num.");
					}
					
					//检查是否带参数
					Integer paramNum = urlContainer.getNavigationParamNum();
					
					//并比较参数个数是否相同
					/* List<String> paramList = urlContainer.getParamList();
					if(paramNum>0 && paramList!= null && !paramNum.equals(paramList.size())){
						throw new BusinessException(urlContainer.getErrorCode(), urlContainer.getErrorMessage());
					}*/
					
					if(null != navigation){
						urlMappingExist.setNavigation(navigation);;//设置关联
						urlStaticMapping.setBindNavigation(true);//同时，设置已绑定菜单
						
						if(paramNum>0){
							navigation.setDisable(true);//如果带参数，设置该菜单为禁用，因为此时并没有具体参数，无法正确导航
							navigation.setDisplay(true);//设置该菜单显示出来,显示但链接不可以点击,提示用户必须修改
							navigation.setHideable(false);
						}
						
						navigation.setCode(urlStaticMapping.getUrlCode());
						navigation.setName(urlStaticMapping.getUrlName());
						navigation.setIdKey(urlStaticMapping.getIdKey());
						navigation.setUrl(urlFullPath);
						navigation.setType(Navigation.TYPE_SYSTEM);
						navigation.setIdKey(idKey);
						
						if(!navigation.getHideable()){
							navigation.setHideable(false);
							navigation.setDisplay(true);
						}else{
							navigation.setHideable(true);
							navigation.setDisplay(false);
						}
						if(null != developer){
							navigation.setDeveloper(developer);
						}
					}
				}
				
				urlMappingService.save(urlMappingSavedList);
		}
	}
	
	
	
	
	
	/**
	 * 从导航菜单解除关联,byIdKeys
	 */
	public void unbindFromNavigationByIdKeys(UrlStaticMapping urlStaticMapping) throws BusinessException{
		if(null != urlStaticMapping){
			String idKeys = urlStaticMapping.getIdKeys();
			if(StringUtils.isNotEmpty(idKeys)){
				String[] idKeyArray = idKeys.split(",");
				List<String> idKeyList = null;
				if(null != idKeyArray && idKeyArray.length>0){
					idKeyList = Arrays.asList(idKeyArray);
					unbindFromNavigationByIdKeyList(idKeyList);
				}
			}
		}
	}
	
	/**
	 * 解除与导航菜单关联
	 * @param idKeyList
	 * @throws BusinessException
	 */
	public void unbindFromNavigationByIdKeyList(List<String> idKeyList) throws BusinessException{
		if(null != idKeyList && idKeyList.size()>0){
			List<UrlStaticMapping> urlStaticMappingList = urlMappingService.findByUrlStaticMappingIdKeyList(idKeyList);
			if(null != urlStaticMappingList && urlStaticMappingList.size()>0){
					List<UrlMapping> urlMappingSavedList = new ArrayList<UrlMapping>();
					for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
						UrlMapping urlMappingExist = urlStaticMapping.getUrlMapping();
						if(urlMappingExist == null){
							urlMappingExist = new UrlMapping();
							urlMappingExist.setUrlStaticMapping(urlStaticMapping);
							urlMappingSavedList.add(urlMappingExist);
						}
						urlStaticMapping.setUrlMapping(urlMappingExist);
						
						Navigation navigation = urlMappingExist.getNavigation();
						
						urlMappingSavedList.add(urlMappingExist);
						 
						if(null != navigation){
							urlMappingExist.setNavigation(null);//解除关联，置空
							urlStaticMapping.setBindNavigation(false);//同时，把它设置false
						}
					}
					urlMappingService.save(urlMappingSavedList);
			}
		}
	}
	
}
