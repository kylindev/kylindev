package org.jit8.site.travel.biz.service.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;


public interface ManageAreaItemService extends GenericService<ManageAreaItem, Long>{

	List<ManageAreaItem> findAllAvailable();
	
	List<ManageAreaItem> findAllDisplay();
	
	List<ManageAreaItem> findByIdList(List<Long> idList);
}
