package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategorySpecialBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteCategorySpecialService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("stickyNoteCategorySpecialBusiness")
public class StickyNoteCategorySpecialBusinessImpl implements StickyNoteCategorySpecialBusiness{

	@Resource
	private StickyNoteCategorySpecialService stickyNoteCategorySpecialService;
	

	@Override
	public List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList() {
		return stickyNoteCategorySpecialService.getStickyNoteCategorySpecialList(false, false);
	}
	
	@Transactional
	@Override
	public StickyNoteCategorySpecial persist(StickyNoteCategorySpecial stickyNoteCategorySpecial){
		Long id = stickyNoteCategorySpecial.getId();
		StickyNoteCategorySpecial exist = null;
		if(id != null && id>0){
			 exist = stickyNoteCategorySpecialService.findOne(stickyNoteCategorySpecial.getId());
			
			 exist.setSequence(stickyNoteCategorySpecial.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=stickyNoteCategorySpecial;
		}
		
		return stickyNoteCategorySpecialService.save(exist);
	}

	@Override
	public StickyNoteCategorySpecial getStickyNoteCategorySpecialById(Long id) {
		return stickyNoteCategorySpecialService.findOne(id);
	}

	@Override
	public StickyNoteCategorySpecial deleteStickyNoteCategorySpecialById(Long id) {
		StickyNoteCategorySpecial old = stickyNoteCategorySpecialService.findOne(id);
		old.setDeleted(true);
		return stickyNoteCategorySpecialService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			stickyNoteCategorySpecialService.delete(id);
		}
	}

	@Override
	public Page<StickyNoteCategorySpecial> findAll(Pageable pageable) {
		return stickyNoteCategorySpecialService.findAll(pageable);
	}

	@Override
	public Page<StickyNoteCategorySpecial> findAll(
			StickyNoteCategorySpecial stickyNoteCategorySpecial,
			Pageable pageable) {
		return stickyNoteCategorySpecialService.findAll(stickyNoteCategorySpecial, pageable);
	}

	@Override
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList) {
		return stickyNoteCategorySpecialService.getByCategorySpecialIdList(categorySpecialIdList);
	}

	@Transactional
	@Override
	public void delete(Iterable<? extends StickyNoteCategorySpecial> entities) {
		stickyNoteCategorySpecialService.delete(entities);
	}


}
