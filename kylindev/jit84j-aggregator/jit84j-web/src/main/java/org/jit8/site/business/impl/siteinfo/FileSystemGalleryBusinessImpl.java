package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.FileSystemGalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.service.siteinfo.FileSystemGalleryService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("fileSystemGalleryBusiness")
public class FileSystemGalleryBusinessImpl implements FileSystemGalleryBusiness{

	@Resource
	private FileSystemGalleryService fileSystemGalleryService;
	
	@Override
	public List<FileSystemGallery> getFileSystemGalleryList() {
		return fileSystemGalleryService.getFileSystemGalleryList(false, false);
	}
	
	@Transactional
	@Override
	public FileSystemGallery persist(FileSystemGallery gallery){
		return merge(gallery);
	}
	
	@Transactional
	@Override
	public FileSystemGallery merge(FileSystemGallery gallery){
		
		Long id = gallery.getId();
		FileSystemGallery fileSystemGallery = null;
		if(null != id && id > 0){
			fileSystemGallery = fileSystemGalleryService.findOne(id);
			if(null != fileSystemGallery){
				fileSystemGallery.setDeleted(false);
				fileSystemGallery.setDescription(gallery.getDescription());
				fileSystemGallery.setImagePath(gallery.getImagePath());
				fileSystemGallery.setStoredPath(gallery.getStoredPath());
				fileSystemGallery.setName(gallery.getName());
				fileSystemGallery.setCode(gallery.getCode());
				fileSystemGallery.setSequence(gallery.getSequence());
				fileSystemGallery.setType(gallery.getType());
			}
			
		}else{
			fileSystemGallery = gallery;
		}
		return fileSystemGalleryService.save(fileSystemGallery);
	}

	@Override
	public FileSystemGallery getFileSystemGalleryById(Long id) {
		return fileSystemGalleryService.findOne(id);
	}

	@Override
	public FileSystemGallery deleteFileSystemGalleryById(Long id) {
		FileSystemGallery old = fileSystemGalleryService.findOne(id);
		old.setDeleted(true);
		return fileSystemGalleryService.save(old);
	}
	
	public void removeById(Long id) {
		fileSystemGalleryService.delete(id);
	}

	@Override
	public Page<FileSystemGallery> findAll(Pageable pageable) {
		return fileSystemGalleryService.findAll(pageable);
	}

}
