package org.jit8.site.travel.persist.repository;

import java.util.Date;
import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ManifestoRepository extends GenericJpaRepository<Manifesto, Long>{

	@Query("from Manifesto o where o.disable=false and o.deleted=false and o.publishDate between :begin and :end")
	public Page<Manifesto> getList(@Param("begin")Date begin, @Param("end")Date end, Pageable pageable);

	@Query("from Manifesto o where o.disable=false and o.deleted=false and o.publishDate between :begin and :end and o.userId=:userId")
	public  List<Manifesto> findManifestoByPublishDateAndUserId(@Param("begin")Date begin, @Param("end")Date end, @Param("userId")Long userId);
	
	@Query("select max(id) from Manifesto o where o.uniqueIdentifier<:id")
	public Long findPreviousOneByCurrentId(@Param("id")Long id);
	
	@Query("select min(id) from Manifesto o where o.uniqueIdentifier>:id")
	public Long findFollowingOneByCurrentId(@Param("id")Long id);
	
	@Query("select new Manifesto(uniqueIdentifier, iconPath, decription, nickName, userId,  publishDate, praiseCount, careCount, commentCount) from Manifesto o where o.uniqueIdentifier=:id")
	public Manifesto findSimpleOneById(@Param("id")Long id);
	
	
	@Query("select max(id) from Manifesto o")
	public Long findLastId();

}
