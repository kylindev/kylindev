package org.jit8.framework.jit84j.web.controller;

import java.io.IOException;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.web.util.EncodeUtil;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.framework.jit84j.web.utils.MailMessageUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.framework.jit84j.web.validator.RegisterValidator;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.user.business.userinfo.EmailUrlBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class RegisterController /*extends BaseCommonController*/{

private static final Logger logger = LoggerFactory.getLogger(RegisterController.class);
	
	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private RegisterValidator registerValidator;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefine;
	
	@Resource
	private EmailUrlBusiness emailUrlBusiness;
	
	@Resource
	private FileInfoBusiness fileInfoBusiness;
	
	
	@InitBinder  
    public void initBinder(DataBinder binder) {  
      // binder.setValidator(registerValidator);  
    }
	
	
	public RegisterController(){
		logger.debug("######### init LoginController #########");
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_UI_MAPPING)
	public String registerUserUI(@ModelAttribute("user") User user, Model model) throws IOException {
		logger.debug("######### /registerUserUI #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		//model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		Pageable pageable = new PageRequest(0, 20, new Sort(new Order(Direction.DESC, "uniqueIdentifier")));
		
		Page<User> page = userBusiness.getLastUserList(pageable);
		model.addAttribute("page",page);
		
		return "registerUserUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_MAPPING)
	public String registerUser(@Valid @ModelAttribute("user")User user,BindingResult result, Model model) throws IOException {
		logger.debug("######### /registerUser #########");
		if(result.hasErrors()){
			//redirectAttributes.addFlashAttribute("user", user);
			//model.addAttribute("ownRelationList", ownRelationList);
			return registerUserUI(user,model);
		}
		user = userBusiness.registerUser(user);
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_SUCCESS_MAPPING + "/" + EncodeUtil.encodeWithByte(user.getEmail(), "utf-8");
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_SUCCESS_MAPPING+"/{email}")
	public String registerUserSuccess(@PathVariable("email") String email,@ModelAttribute("user") User user, Model model) throws IOException {
		logger.debug("######### /registerUserUI #########");
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		Pageable pageable = new PageRequest(0, 20, new Sort(new Order(Direction.DESC, "uniqueIdentifier")));
		
		Page<User> page = userBusiness.getLastUserList(pageable);
		model.addAttribute("page",page);
		model.addAttribute("email",email);
		
		return "registerUserSuccessUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_ACTIVE_EMAIL_UI_MAPPING + "/{urlCode}")
	public String activeUserByEmailUI(@PathVariable("urlCode")String urlCode,@ModelAttribute("user") User user, Model model,HttpServletRequest request) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ACTIVE_EMAIL_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_ACTIVE_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		
		Pageable pageable = new PageRequest(0, 20, new Sort(new Order(Direction.DESC, "uniqueIdentifier")));
		Page<User> page = userBusiness.getLastUserList(pageable);
		model.addAttribute("page",page);
		
		if(StringUtils.isEmpty(urlCode)){
			model.addAttribute("invalidPath","非法路径");
		}
		
		EmailUrl emailUrl = emailUrlBusiness.findByUrlCode(urlCode);
		if(null == emailUrl){
			model.addAttribute("invalidPath","非法路径");
		}else{
			if(emailUrl.isActived()){
				model.addAttribute("hasActived","您已经激活过啦！直接登录即可");
			}else{
				user.setEmail(emailUrl.getReciever());
				user = userBusiness.activeUser(user,emailUrl);
			}
		}
		
		return "activeUserSuccessUI";
	}
	
}
