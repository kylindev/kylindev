package org.jit8.site.web.admin.validator;

import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class OwnRelationValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return OwnRelation.class.equals(clazz); 
	}

	@Override
	public void validate(Object target, Errors errors) {
		//ValidationUtils.rejectIfEmpty(errors, "username", null, "Username is empty.");  
	}


}
