package org.jit8.user.business.userinfo;

import java.util.List;

import org.jit8.user.persist.domain.userinfo.OwnRelation;



public interface OwnRelationBusiness {

	public List<OwnRelation> getOwnRelationList();
	
	public OwnRelation persist(OwnRelation ownRelation);
	
	public List<OwnRelation> persist(List<OwnRelation> ownRelationList);
}


