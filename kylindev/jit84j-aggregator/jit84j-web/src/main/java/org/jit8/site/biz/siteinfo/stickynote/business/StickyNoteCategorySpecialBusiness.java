package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface StickyNoteCategorySpecialBusiness {

	public List<StickyNoteCategorySpecial> getStickyNoteCategorySpecialList();
	public StickyNoteCategorySpecial persist(StickyNoteCategorySpecial stickyNoteCategorySpecial);
	
	public StickyNoteCategorySpecial getStickyNoteCategorySpecialById(Long id);
	
	public StickyNoteCategorySpecial deleteStickyNoteCategorySpecialById(Long id);
	
	public Page<StickyNoteCategorySpecial> findAll(Pageable pageable);
	
	public void delete(Iterable<? extends StickyNoteCategorySpecial> entities);
	
	
	public void removeById(Long id);
	
	public Page<StickyNoteCategorySpecial> findAll(
			StickyNoteCategorySpecial stickyNoteCategorySpecial,
			Pageable pageable) ;
	
	public List<StickyNoteCategorySpecial> getByCategorySpecialIdList(List<Long> categorySpecialIdList);
	
}
