package org.jit8.framework.jit84j.web.data.dropdown;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import net.rubyeye.xmemcached.MemcachedClient;

import org.jit8.framework.jit84j.core.cache.memached.Memached;
import org.jit8.framework.jit84j.core.cache.memached.MemachedContext;
import org.jit8.framework.jit84j.core.cache.memached.MemachedDefiner;
import org.jit8.framework.jit84j.core.cache.memached.MemachedModel;
import org.jit8.framework.jit84j.core.mail.business.MailConfigBusiness;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.domain.MailConfigType;
import org.jit8.framework.jit84j.core.web.util.MemcachedUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.business.menu.NavigatMenuBusiness;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.business.siteinfo.FileSystemGalleryBusiness;
import org.jit8.site.business.siteinfo.FileSystemGalleryTypeBusiness;
import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.user.business.userinfo.DirectPermissionBusiness;
import org.jit8.user.business.userinfo.OwnRelationBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class StaticDataDefineCoreManager extends StaticDataDefineManager{
	

	private static final long serialVersionUID = 4886491624881603018L;

	private static final Logger LOGGER  = LoggerFactory.getLogger(StaticDataDefineCoreManager.class);

	//memached key定义规则 ： developerId_key
	
	@Resource
	protected MemachedContext memachedContext;
	
	private static MemcachedClient memcachedClient;
	
	@Resource
	private OwnRelationBusiness ownRelationBusiness;
	
	@Resource
	private DirectPermissionBusiness directPermissionBusiness;
	
	@Resource
	private NavigationBusiness navigationBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@Resource
	private  FileSystemGalleryBusiness fileSystemGalleryBusiness;
	
	@Resource
	private  FileSystemGalleryTypeBusiness fileSystemGalleryTypeBusiness;
	
	@Resource
	private  SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private MailConfigBusiness mailConfigBusiness;
	
	@Resource
	private SpecialAreaBusiness specialAreaBusiness;
	
	@Resource
	private StickyNoteCategoryBusiness stickyNoteCategoryBusiness;
	
	@Resource
	private NavigatMenuBusiness navigatMenuBusiness;
	
	private static Set<String> directPermissionSet;
	
	private static Map<String,String> navigationTypeMap = new HashMap<String,String>();
	
	@PostConstruct
	protected void init(){
		//initDirectPermission();
		this.memcachedClient = memachedContext.getMemcachedClient();
		StaticDataDefineConstant.setMemachedModelByKey(StaticDataDefineUtil.scan(StaticDataDefineCoreManager.class));
	}
	
	
	public void initDirectPermission(){
		directPermissionSet = getDirectPermissionSet();
	}
	
	
	@Memached(value=StaticDataDefineConstant.DIRECT_PERMISSION_SET, name=StaticDataDefineConstant.DIRECT_PERMISSION_SET_NAME,autoRefresh=true)
	public Set<String> getDirectPermissionSet() {
		Set<String> directPermissionSet = directPermissionBusiness.getDirectPermissionSet();
		if(null == directPermissionSet){
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING);
			directPermissionSet.add(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_IMPORT_MAPPING);
		} 
		return directPermissionSet;
	}
	
	/**
	 * 文件管理》系统类别》系统分类列表
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN_NAME,autoRefresh=true)
	public List<FileSystemGallery> getFileSystemGalleryDropdown() {
		List<FileSystemGallery> list = fileSystemGalleryBusiness.getFileSystemGalleryList();
		//同时刷新 所允许上传 类型的 MIME, Suffix map
		refresh( StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_MIME, true);
		refresh( StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_SUFFIX_MIME, true);
		refresh( StaticDataDefineConstant.FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP, true);
		return list;
	}
	
	/**
	 * 文件管理》系统类别》系统分类Code列表
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_CODE_DROPDOWN, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_CODE_DROPDOWN_NAME,autoRefresh=true)
	public List<String> getFileSystemGalleryCodeDropdown() {
		List<String> list = FileSystemGallery.codeList;
		return list;
	}
	
	
	/**
	 * FileSystemGallery系统分类allowed file MIME
	 * map的key取自FileSystemGallery预定义系统分类常量
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_MIME, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_MIME_NAME,autoRefresh=true)
	public Map<String,String> getFileSystemGalleryAllowedMimeMap() {
		List<FileSystemGallery> list = fileSystemGalleryBusiness.getFileSystemGalleryList();
		Map<String,String> map = new HashMap<String,String>();
		if(null != list & list.size()>0){
			for(FileSystemGallery fileSystemGallery : list){
				StringBuilder sb = new StringBuilder();
				List<FileMime> fileMimeList = fileSystemGallery.getFileMimeList();
				if(null != fileMimeList){
					for(FileMime fileMime : fileMimeList){
						String mime = fileMime.getMime();
						if(sb.indexOf(mime)<0){
							sb.append(mime).append(",");
						}
					}
				}
				if(sb.lastIndexOf(",")>0){
					sb.deleteCharAt(sb.lastIndexOf(","));
				}
				map.put(fileSystemGallery.getCode(), sb.toString());
			}
		}
		return map;
	}
	
	/**
	 * FileSystemGallery系统分类allowed file MIME后缀
	 * map的key取自FileSystemGallery预定义系统分类常量
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_SUFFIX_MIME, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_MAP_ALLOWED_SUFFIX_MIME_NAME,autoRefresh=true)
	public Map<String,String> getFileSystemGalleryAllowedSuffixMap() {
		List<FileSystemGallery> list = fileSystemGalleryBusiness.getFileSystemGalleryList();
		Map<String,String> map = new HashMap<String,String>();
		if(null != list & list.size()>0){
			for(FileSystemGallery fileSystemGallery : list){
				StringBuilder sb = new StringBuilder();
				List<FileMime> fileMimeList = fileSystemGallery.getFileMimeList();
				if(null != fileMimeList){
					for(FileMime fileMime : fileMimeList){
						String suffix = fileMime.getSuffix();
						if(sb.indexOf(suffix)<0){
							sb.append(suffix).append(",");
						}
					}
				}
				if(sb.lastIndexOf(",")>0){
					sb.deleteCharAt(sb.lastIndexOf(","));
				}
				map.put(fileSystemGallery.getCode(), sb.toString());
			}
		}
		return map;
	}
	
	/**
	 * 根据mime+suffix 获取对应的FileSystemGallery 
	 * map的key取自mime+suffix
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_SUFFIX_MIME_KEY_FOR_MAP_NAME,autoRefresh=true)
	public Map<String,FileSystemGallery> getFileSystemGalleryBySuffixMimeForMap() {
		List<FileSystemGallery> list = fileSystemGalleryBusiness.getFileSystemGalleryList();
		Map<String,FileSystemGallery> map = new HashMap<String,FileSystemGallery>();
		if(null != list & list.size()>0){
			for(FileSystemGallery fileSystemGallery : list){
				StringBuilder sb = new StringBuilder();
				List<FileMime> fileMimeList = fileSystemGallery.getFileMimeList();
				if(null != fileMimeList){
					for(FileMime fileMime : fileMimeList){
						String suffix = fileMime.getSuffix();
						String mime = fileMime.getMime();
						map.put(suffix+mime, fileSystemGallery);
					}
				}
				if(sb.lastIndexOf(",")>0){
					sb.deleteCharAt(sb.lastIndexOf(","));
				}
			}
		}
		return map;
	}

	
	/**
	 * 文件管理》系统类别》系统分类类型列表
	 */
	@Memached(value=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_TYPE_DROPDOWN, name=StaticDataDefineConstant.FILE_SYSTEM_GALLERY_TYPE_DROPDOWN_NAME,autoRefresh=true)
	public List<FileSystemGalleryType> getFileSystemGalleryTypeDropdown() {
		return fileSystemGalleryTypeBusiness.getList();
	}
	
	/**
	 * 主站信息
	 */
	@Memached(value=StaticDataDefineConstant.SITE_INFO_MAIN_SITE, name=StaticDataDefineConstant.SITE_INFO_MAIN_SITE_NAME,autoRefresh=true)
	public SiteInfo getMainSiteInfo() {
		return siteInfoBusiness.getSiteInfoByType(SiteInfo.MAIN_SITE);
	}
	

	@Memached(value=StaticDataDefineConstant.NAVIGATION_TYPE_DROPDOWN, name=StaticDataDefineConstant.NAVIGATION_TYPE_DROPDOWN_NAME,autoRefresh=true)
	public static Map<String, String> getNavigationTypeDropdown() {
		Map<String, String> navigationTypeDropdown = null;
		navigationTypeDropdown = new HashMap<String,String>();
		navigationTypeDropdown.put(Navigation.TYPE_SYSTEM, "系统导入");
		navigationTypeDropdown.put(Navigation.TYPE_CUSTUM, "自由定制");
		return navigationTypeDropdown;
	}

	
	@Memached(value=StaticDataDefineConstant.NAVIGATION_TOP_LIST, name=StaticDataDefineConstant.NAVIGATION_TOP_LIST_NAME,autoRefresh=true)
	public List<Navigation> getNavigationTopList(){
		List<Navigation> navigationList = null;
		navigationList = navigationBusiness.getListByTopCode();
		refresh( StaticDataDefineConstant.NAVIGATION_MAP, true);//同时刷新map容器中的导航
		return navigationList;
	}
	
	@Memached(value=StaticDataDefineConstant.NAVIGATION_MAP, name=StaticDataDefineConstant.NAVIGATION_MAP_NAME,autoRefresh=true)
	public Map<String,Navigation> getNavigationMap(){
		List<Navigation> navigationList = navigationBusiness.getListByTopCode();
		Map<String,Navigation > map = new HashMap<String,Navigation>();
		if(null != navigationList && navigationList.size()>0){
			for(Navigation navigation : navigationList){
				putNavigationWithCodeAsKey(map,navigation);
			}
		}
		return map;
	}
	
	@Memached(value=StaticDataDefineConstant.NAVIGATMENU_TOP_LIST, name=StaticDataDefineConstant.NAVIGATMENU_TOP_LIST_NAME,autoRefresh=true)
	public List<NavigatMenu> getNavigatMenuTopList(){
		 List<NavigatMenu> navigatMenuTopList = navigatMenuBusiness.getNavigatMenuByTopCode();
		 refresh( StaticDataDefineConstant.NAVIGATMENU_MAP, true);//同时刷新map容器中的导航
		return navigatMenuTopList;
	}
	
	@Memached(value=StaticDataDefineConstant.NAVIGATMENU_MAP, name=StaticDataDefineConstant.NAVIGATMENU_MAP_NAME,autoRefresh=true)
	public Map<String,NavigatMenu> getNavigatMenuMap(){
		List<NavigatMenu> navigatMenuTopList = navigatMenuBusiness.getNavigatMenuByTopCode();
		Map<String,NavigatMenu > map = new HashMap<String,NavigatMenu>();
		if(null != navigatMenuTopList && navigatMenuTopList.size()>0){
			for(NavigatMenu navigatMenu : navigatMenuTopList){
				putNavigationWithCodeAsKey(map,navigatMenu);
			}
		}
		return map;
	}
	
	private void putNavigationWithCodeAsKey(Map<String,NavigatMenu> map,NavigatMenu navigatMenu){
		map.put(navigatMenu.getCode(), navigatMenu);
		List<NavigatMenu> navigatMenuList = navigatMenu.getChildrenNavigatMenu();
		if(null != navigatMenuList && navigatMenuList.size()>0){
			for(NavigatMenu n : navigatMenuList){
				//map.put(n.getCode(), navigation);
				putNavigationWithCodeAsKey(map,n);
			}
		}
	}
	
	private void putNavigationWithCodeAsKey(Map<String,Navigation> map,Navigation navigation){
		map.put(navigation.getCode(), navigation);
		List<Navigation> navigationList = navigation.getChildrenNavigation();
		if(null != navigationList && navigationList.size()>0){
			for(Navigation n : navigationList){
				//map.put(n.getCode(), navigation);
				putNavigationWithCodeAsKey(map,n);
			}
		}
	}
	
	@Memached(value = StaticDataDefineConstant.GALLERY_LIST, name = StaticDataDefineConstant.GALLERY_LIST_NAME,autoRefresh=true)
	public List<Gallery> getGalleryList() {
		List<Gallery> galleryList = null;
		galleryList = galleryBusiness.getGalleryList();
		for (Gallery g : galleryList) {
			LOGGER.info("gallery:" + g.getPhotographList());
			List<Photograph> photographList = g.getPhotographList();
			if (photographList != null && photographList.size() > 0) {
				for (Photograph p : photographList) {
					LOGGER.info(p.getUniqueIdentifier() + " : " + p.getTitle());
				}
			}

		}
		return galleryList;
	}
	
	
	@Memached(value=StaticDataDefineConstant.OWN_RELATION_LIST, name=StaticDataDefineConstant.OWN_RELATION_LIST_NAME,autoRefresh=true)
	public List<OwnRelation> getOwnRelationList(){
		List<OwnRelation> ownRelationList = null;
		ownRelationList = ownRelationBusiness.getOwnRelationList();
		return ownRelationList;
	}
	
	
	@Memached(value=StaticDataDefineConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN, name=StaticDataDefineConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN_NAME,autoRefresh=true)
	public Map<Long, String> getPermissionCategoryDropdown() {
		Map<Long, String> permissionCategoryDropdown = new HashMap<Long, String>();
		permissionCategoryDropdown = Permission.getPermissioncategorydropdwon();
		permissionCategoryDropdown.put(
				Permission.PERMISSION_CATEGORY_DROPDWON_ALL, "全部");
		permissionCategoryDropdown.put(
				Permission.PERMISSION_CATEGORY_DROPDWON_HAS_CATEGORIED, "已分类");
		permissionCategoryDropdown.put(
				Permission.PERMISSION_CATEGORY_DROPDWON_UN_CATEGORIED, "未分类");
		return permissionCategoryDropdown;
	}
	
	
	@Memached(value=StaticDataDefineConstant.MAIL_CONFIG_DEFAULT, name=StaticDataDefineConstant.MAIL_CONFIG_DEFAULT_NAME,autoRefresh=true)
	public MailConfig findDefaultMailConfig(){
		MailConfig mailConfig = mailConfigBusiness.findDefaultMailConfig();
		if(null== mailConfig){
			mailConfig = new MailConfig();
			mailConfig.setHost("smtp.163.com");
			mailConfig.setUsername("kylinboy521");
			mailConfig.setPassword("xiaoyanzi");
		}
		return mailConfig;
	}
	
	
	@Memached(value=StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN, name=StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN_NAME,autoRefresh=true)
	public List<SpecialArea> specialAreaDropdown(){
		List<SpecialArea> list = specialAreaBusiness.getSpecialAreaList();
		return list;
	}
	
	@Memached(value=StaticDataDefineConstant.STICKYNOTE_CATEGORY_DROPDOWN, name=StaticDataDefineConstant.STICKYNOTE_CATEGORY_DROPDOWN_NAME,autoRefresh=true)
	public List<StickyNoteCategory> stickyNoteCategoryDropdown(){
		List<StickyNoteCategory> list = stickyNoteCategoryBusiness.getStickyNoteCategoryList();
		return list;
	}
	
	@Memached(value=StaticDataDefineConstant.MAIL_CONFIG_TYPE_DROPDOWN, name=StaticDataDefineConstant.MAIL_CONFIG_TYPE_DROPDOWN_NAME,autoRefresh=true)
	public List<MailConfigType> getMailConfigTypeDropdown(){
		List<MailConfigType> list = new ArrayList<MailConfigType>();
		MailConfigType mailConfigType = new MailConfigType();
		list.add(mailConfigType);
		mailConfigType.setType(MailConfigType.DEFAULT_MAIL_CONFIG);
		mailConfigType.setName("默认配置");
		
		MailConfigType mailConfigType2 = new MailConfigType();
		list.add(mailConfigType2);
		mailConfigType2.setType(MailConfigType.CUSTOM_MAIL_CONFIG);
		mailConfigType2.setName("自定义配置");
		return list;
	}
	
	
	//缓存数据开始
	public void storedDataIntoMemcached(){
		if(null != memachedContext && memachedContext.isMemcachedEnable()){
			//1.初始化数据： 导航类型下拉列表 
			getNavigationTypeDropdown();
			
			//2.初始化数据：直接访问权限列表
		}
	}
	
	
	public static Object getData(String key,boolean refresh){
		LOGGER.debug("getData begin :" + key);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		Object object = MemachedDefiner.get(memachedModel,refresh);
		LOGGER.debug("getData end :" + key);
		return object;
	}
	
	public static Object getData(String key){
		return getData(key,false);
	}
	
	public boolean refresh(String key,boolean refresh){
		LOGGER.debug("refresh begin :" + key);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		Object object = MemachedDefiner.get(memachedModel,refresh);
		boolean success=false;
		if(null != object){
			success=true;
		}
		LOGGER.debug("refresh end :" + key);
		return success;
	}
	
	public boolean touch(String key,int exp){
		Object value = MemcachedUtil.get(key, memcachedClient);
		MemachedModel memachedModel = StaticDataDefineConstant.getMemachedModelByKey(key);
		memachedModel.setExpiresTime(exp);
		StaticDataDefineConstant.setMemachedModelByKey(key, memachedModel);
		if(null == value){
			getData(key,true);
		}
		return MemachedDefiner.touch(key, exp);
	}
	public boolean delete(String key){
		return MemachedDefiner.delete(key);
	}
	
	public static void main(String[] args) {
		Map<String ,Long> map = new HashMap<String,Long>();
		map.put("aaa", 22l);
		map.put("aaa", 99l);
		System.out.println(map.get("aaa"));
	}
	
}
