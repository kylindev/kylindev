package org.jit8.site.business.impl.article;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.site.business.article.ArticleBusiness;
import org.jit8.site.business.article.ArticleCategoryBusiness;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.Article;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.service.article.ArticleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("articleBusiness")
public class ArticleBusinessImpl implements ArticleBusiness{

	private static final Logger logger = LoggerFactory.getLogger(ArticleBusinessImpl.class);
	
	@Resource
	private ArticleService articleService;
	
	@Resource
	private CategoryBusiness categoryBusiness;
	
	@Resource
	private ArticleCategoryBusiness articleCategoryBusiness;
	

	@Override
	public List<Article> getArticleList() {
		return articleService.getArticleList(false, false);
	}
	
	@Override
	public Page<Article> getArticleList(Pageable pageable) {
		return articleService.getArticleList(false, false,pageable);
	}
	
	@Transactional
	@Override
	public Article persist(Article article){
		article = articleService.save(article);
		List<ArticleCategory> articleCategoryList = article.getArticleCategoryList();
		if(articleCategoryList != null && articleCategoryList.size()>0){
			List<ArticleCategory> articleCategoryList2 = new ArrayList<ArticleCategory>();
			for(ArticleCategory ac : articleCategoryList){
				if(null != ac){
					Category category= ac.getCategory();
					if(category != null){
						Long id = category.getId();
						if(id != null && id > 0){
							Category category2 = categoryBusiness.getCategoryById(id);
							ArticleCategory ac2 = new ArticleCategory();
							ac2.setArticle(article);
							ac2.setCategory(category2);
							articleCategoryList2.add(ac2);
						}
					}
				}
			}
			
			if(articleCategoryList2.size()>0){
				articleCategoryList2 = articleCategoryBusiness.persist(articleCategoryList2);
				article.setArticleCategoryList(articleCategoryList2);
			}
		}
		return article;
	}
	
	@Transactional
	@Override
	public Article merge(Article article){
		Article old = getArticleById(article.getId());
		BeanUtils.copyProperties(article, old);
		return articleService.save(old);
	}

	@Override
	public Article getArticleById(Long id) {
		return articleService.findOne(id);
	}

	@Override
	public Article deleteArticleById(Long id) {
		Article old = articleService.findOne(id);
		old.setDeleted(true);
		return articleService.save(old);
	}

	@Override
	public void asignCategory(Article article) {
		Long articleId = article.getId();
		Article currentArticle = null;
		if(null != articleId && articleId>0){
			currentArticle = articleService.findOne(articleId);
		}
		String categoryIdList = article.getCategoryIdList();
		List<Category> categoryList = null;
		if(StringUtils.isNotEmpty(categoryIdList)){
			categoryList = categoryBusiness.getCategoryListByIds(categoryIdList);
		}
		if(null != categoryList && categoryList.size()>0){
			for(Category category : categoryList){
				//TODO
			}
		}
		
		
	}
	
	public Article articleModifySimple(Article article){
		if(null != article){
			Long id = article.getId();
			if(null != id && id>0){
				Article article2 = getArticleById(id);
				article2.setTitle(article.getTitle());
				article2.setAuthorName(article.getAuthorName());
				article2.setSequence(article.getSequence());
				article2.setPenName(article.getPenName());
				return articleService.save(article2);
			}
		}
		return article;
	}

}
