package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.MemberInfoBusiness;
import org.jit8.site.travel.biz.service.MemberInfoService;
import org.jit8.site.travel.persist.domain.MemberInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("memberInfoBusiness")
public class MemberInfoBusinessImpl implements MemberInfoBusiness{

	@Resource
	private MemberInfoService memberInfoService;
	

	@Override
	public List<MemberInfo> getMemberInfoList() {
		return memberInfoService.getMemberInfoList(false, false);
	}
	
	@Transactional
	@Override
	public MemberInfo persist(MemberInfo memberInfo){
		Long id = memberInfo.getId();
		MemberInfo exist = null;
		if(id != null && id>0){
			 exist = memberInfoService.findOne(memberInfo.getId());
			 exist.setSequence(memberInfo.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=memberInfo;
		}
		
		return memberInfoService.save(exist);
	}

	@Override
	public MemberInfo getMemberInfoById(Long id) {
		return memberInfoService.findOne(id);
	}

	@Override
	public MemberInfo deleteMemberInfoById(Long id) {
		MemberInfo old = memberInfoService.findOne(id);
		old.setDeleted(true);
		return memberInfoService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			memberInfoService.delete(id);
		}
	}

	@Override
	public Page<MemberInfo> findAll(Pageable pageable) {
		return memberInfoService.findAll(pageable);
	}

	@Override
	public MemberInfo findByCode(String code) {
		return memberInfoService.findByCode( code);
	}

	@Override
	public List<MemberInfo> findByIds(List<Long> idList) {
		return memberInfoService.findByIds(idList);
	}

}
