package org.jit8.site.service.article;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.article.ArticleCategory;


public interface ArticleCategoryService extends GenericService<ArticleCategory, Long>{

	public List<ArticleCategory> getArticleCategoryList(boolean disable, boolean deleted);
	
	public List<ArticleCategory> getArticleCategoryListByArticleId(boolean disable,boolean deleted,  Long articleId);
}
