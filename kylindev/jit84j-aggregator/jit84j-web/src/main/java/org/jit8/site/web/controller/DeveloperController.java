package org.jit8.site.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DeveloperController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(DeveloperController.class);
	

	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private DeveloperBusiness developerBusiness;

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_MAPPING )
	public String addDeveloperUI(@ModelAttribute("developer") Developer developer, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/developer/addDeveloperUI/ #########");
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("developer",new Developer());
		
		return "addDeveloperUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_MAPPING )
	public String applyDeveloperUI(@ModelAttribute("developer") Developer developer, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/developer/addDeveloperUI/ #########");
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("developer",new Developer());
		
		return "applyDeveloperUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING )
	public String developerListUI(@ModelAttribute("developer") Developer developer, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### /admin/userinfo/developer/addDeveloper/ #########");
		model.addAttribute("ownRelationList", ownRelationList);
		developer.setCurrentUser(getCurrentUser());
		Page<Developer> page = developerBusiness.getDeveloperList(developer,pageable);
		
		model.addAttribute("page", page);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("developer",developer);
		
		return "developerListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_MAPPING + "/{code}")
	public String addDeveloper(@PathVariable("code") String code,@ModelAttribute("developer") Developer developer, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/developer/addDeveloper/ #########");
		developer.setCurrentUser(getCurrentUser());
		developerBusiness.persist(developer);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING + "/" + code;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING)
	public String getDeveloperListUI(@ModelAttribute("developer") Developer developer, Model model) throws IOException {
		PageRequest pageable = new PageRequest(0,20);
		Page<Developer> page = getDeveloperBusiness().getDeveloperList(pageable);
		model.addAttribute("page",page);
		model.addAttribute("currentDeveloper",developer);
		return "developerListForPermissionUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING)
	public String getDeveloperListWithQueryUI(@ModelAttribute("developer") Developer developer, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		Page<Developer> page = getDeveloperBusiness().getDeveloperList(developer,pageable);
		model.addAttribute("page",page);
		model.addAttribute("developer",developer);
		return "selectedDeveloperWithQueryUI";
	}
}
