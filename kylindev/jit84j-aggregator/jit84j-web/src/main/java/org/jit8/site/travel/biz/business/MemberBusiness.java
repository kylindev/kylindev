package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.Member;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface MemberBusiness {

	public List<Member> getMemberList();
	
	public Member persist(Member member);
	
	public Member getMemberById(Long id);
	
	public Member deleteMemberById(Long id);
	
	public Page<Member> findAll(Pageable pageable);
	
	public Member findByCode(String code);
	
	public void removeById(Long id);
	
	public List<Member> findByIds(List<Long> idList);
}
