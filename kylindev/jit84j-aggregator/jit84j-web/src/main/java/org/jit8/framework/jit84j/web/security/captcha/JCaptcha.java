package org.jit8.framework.jit84j.web.security.captcha;

import javax.servlet.http.HttpServletRequest;

import org.apache.shiro.authc.IncorrectValidateCodeException;

import com.octo.captcha.service.CaptchaServiceException;
import com.octo.captcha.service.captchastore.FastHashMapCaptchaStore;

public class JCaptcha {

	public static final Jit8ManageableImageCaptchaService captchaService = new Jit8ManageableImageCaptchaService(new FastHashMapCaptchaStore(),
			new GMailEngine(), 180, 100000, 75000);

	public static boolean validateResponse(HttpServletRequest request, String userCaptchaResponse) {
		if (request.getSession(false) == null)
			return false;
		boolean validated = false;
		try {
			String id = request.getSession().getId();
			validated = captchaService.validateResponseForID(id, userCaptchaResponse).booleanValue();
		} catch (CaptchaServiceException e) {
			e.printStackTrace();
			throw new IncorrectValidateCodeException("该用户已被锁住");
		}
		return validated;
	}

	public static boolean hasCaptcha(HttpServletRequest request, String userCaptchaResponse) {
		if (request.getSession(false) == null)
			return false;
		boolean validated = false;
		try {
			String id = request.getSession().getId();
			validated = captchaService.hasCapcha(id, userCaptchaResponse);
		} catch (CaptchaServiceException e) {
			e.printStackTrace();
		}
		return validated;
	}

}
