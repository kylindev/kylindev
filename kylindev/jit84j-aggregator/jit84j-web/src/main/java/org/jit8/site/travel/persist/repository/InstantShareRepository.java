package org.jit8.site.travel.persist.repository;

import java.util.Date;
import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InstantShareRepository extends GenericJpaRepository<InstantShare, Long>{

	
	@Query("from InstantShare o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<InstantShare> getInstantShareList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from InstantShare o where o.title=:title")
	public InstantShare findByTitle(@Param("title")String title);
	
	@Query("from InstantShare o where o.uniqueIdentifier in :idList")
	public List<InstantShare> findByIds(@Param("idList")List<Long> idList);
	
	
	@Query("from InstantShare o where o.userId = :userId and o.deleted=false")
	public Page<InstantShare> findByUserId(@Param("userId")Long userId, Pageable pageable);
	
	@Query("select max(id) from InstantShare o where o.uniqueIdentifier<:id")
	public Long findPreviousOneByCurrentId(@Param("id")Long id);
	
	@Query("select min(id) from InstantShare o where o.uniqueIdentifier>:id")
	public Long findFollowingOneByCurrentId(@Param("id")Long id);
	
	
	@Query("select new InstantShare( uniqueIdentifier, place,  title,  nickName,  userId,  publishDate,  praiseCount,  careCount, commentCount,  imageCount) from InstantShare o where o.uniqueIdentifier=:id")
	public InstantShare findSimpleOneById(@Param("id")Long id);
}
