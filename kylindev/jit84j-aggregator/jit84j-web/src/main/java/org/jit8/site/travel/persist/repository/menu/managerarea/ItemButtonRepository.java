package org.jit8.site.travel.persist.repository.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ItemButtonRepository extends GenericJpaRepository<ItemButton, Long>{

	@Query("from ItemButton o where o.deleted=false order by o.sequence asc")
	public List<ItemButton> findAllAvailable();
	
	@Query("from ItemButton o where o.deleted=false and o.disable=false and o.hideable=false and o.display=true order by o.sequence asc")
	public List<ItemButton> findAllDisplay();
	
	@Query("from ItemButton o where o.uniqueIdentifier in :idList ")
	public List<ItemButton> findByIdList(@Param("idList")List<Long> idList);
}
