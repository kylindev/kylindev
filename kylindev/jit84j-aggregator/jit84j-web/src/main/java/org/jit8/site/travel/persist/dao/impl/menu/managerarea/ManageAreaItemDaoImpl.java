package org.jit8.site.travel.persist.dao.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.menu.managerarea.ManageAreaItemDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.jit8.site.travel.persist.repository.menu.managerarea.ManageAreaItemRepository;
import org.springframework.stereotype.Repository;

@Repository
public class ManageAreaItemDaoImpl extends GenericDaoImpl<ManageAreaItem, Long> implements ManageAreaItemDao {

	@Resource
	private ManageAreaItemRepository manageAreaItemRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = manageAreaItemRepository;
	}

	@Override
	public List<ManageAreaItem> findAllAvailable() {
		return manageAreaItemRepository.findAllAvailable();
	}

	@Override
	public List<ManageAreaItem> findAllDisplay() {
		return manageAreaItemRepository.findAllDisplay();
	}

	@Override
	public List<ManageAreaItem> findByIdList(List<Long> idList) {
		List<ManageAreaItem> manageAreaItemList = manageAreaItemRepository.findByIdList(idList);
		return manageAreaItemList;
	}

	
}
