package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.Journey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface JourneyBusiness {

	public List<Journey> getJourneyList();
	
	public Journey persist(Journey journey);
	
	public Journey getJourneyById(Long id);
	
	public Journey deleteJourneyById(Long id);
	
	public Page<Journey> findAll(Pageable pageable);
	
	public Journey findByCode(String code);
	
	public void removeById(Long id);
	
	public List<Journey> findByIds(List<Long> idList);
}
