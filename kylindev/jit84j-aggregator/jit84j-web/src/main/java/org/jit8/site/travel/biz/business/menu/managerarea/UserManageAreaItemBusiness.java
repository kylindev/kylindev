package org.jit8.site.travel.biz.business.menu.managerarea;

import java.util.List;

import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserManageAreaItemBusiness {

	UserManageAreaItem persist(UserManageAreaItem userManageAreaItem);

	List<UserManageAreaItem> findAll();

	Page<UserManageAreaItem> findAll(Pageable pageable);

	List<UserManageAreaItem> findAllAvailable();

	List<UserManageAreaItem> findAllDisplayByUserId(Long userId);

	UserManageAreaItem findById(Long id);

	void removeById(Long id);

	List<UserManageAreaItem> persistList(Long userId,List<UserManageAreaItem> userManageAreaItemList);
	
	List<UserManageAreaItem> findAllWithManageAreaItemByUserId(Long userId);
	
	public List<UserManageAreaItem> findAllByUserId(Long userId);
	
	public List<UserManageAreaItem> findAllDisplayByUserIdFromCache(Long userId);

}     
