package org.jit8.framework.jit84j.web.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.site.business.menu.NavigatMenuBusiness;
import org.jit8.site.business.menu.NavigationBusiness;
import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.business.userinfo.OwnRelationBusiness;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.OwnRelation;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

@Controller
public class StaticDataController extends BaseController{
	@Resource
	protected MessageSource messageSource;
	
	@Resource
	private NavigatMenuBusiness navigatMenuBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@Resource
	private OwnRelationBusiness ownRelationBusiness;
	
	@Resource
	private NavigationBusiness navigationBusiness;
	
	@Resource
	private DeveloperBusiness developerBusiness;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;
	
	public DeveloperBusiness getDeveloperBusiness() {
		return developerBusiness;
	}

	public void setDeveloperBusiness(DeveloperBusiness developerBusiness) {
		this.developerBusiness = developerBusiness;
	}

	protected List<Developer> developerList;
	
	public List<Developer> getDeveloperList() {
		
		return developerList;
	}

	public void setDeveloperList(List<Developer> developerList) {
		this.developerList = developerList;
	}

	protected List<Navigation> navigationTopList = null;
	
	@ModelAttribute
	public List<Navigation> getNavigationTopList(){
		//if(navigationList == null){
			//navigationTopList = navigationBusiness.getNavigationByTopCode();
		navigationTopList = (List<Navigation>)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.NAVIGATION_TOP_LIST, false);
		//}
		return navigationTopList;
	}
	
	protected Navigation currentNavigation = null;
	public Navigation getCurrentNavigation(String url){
		for(Navigation n2 : navigationTopList){
			if(url.equals(n2.getUrl())){
				return n2;
			}
		}
		return null;
	}
	
	
	protected NavigatMenu currentNavigatMenu = null;
	public NavigatMenu getCurrentNavigatMenu(String url){
		for(NavigatMenu n2 : navigatMenuTopList){
			if(url.equals(n2.getUrl())){
				return n2;
			}
		}
		return null;
	}
	
	public NavigatMenu getCurrentNavigatMenuById(Long id){
		
		return navigatMenuBusiness.getNavigatMenuById(id);
	}
	
	public Navigation getCurrentNavigationById(Long id){
		
		return navigationBusiness.getNavigationById(id);
	}
	
	public Navigation getCurrentNavigationByCode(String code){
		
		Map<String,Navigation > map = (Map<String,Navigation >)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.NAVIGATION_MAP);
		Navigation navigation = null;
		if(null != map){
			navigation = map.get(code);
		}
		return navigation;
	}
	
	public NavigatMenu getCurrentNavigatMenuByCode(String code){
		
		Map<String,NavigatMenu > map = (Map<String,NavigatMenu >)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.NAVIGATMENU_MAP);
		NavigatMenu navigatMenu = null;
		if(null != map){
			navigatMenu = map.get(code);
		}
		return navigatMenu;
	}
	
	
	public Navigation getCurrentNavigationByCodeAndDeveloper(String code, Long userId){
		
		return navigationBusiness.getNavigationByCodeAndDeveloper(code, userId);
	}
	
	protected static List<OwnRelation> ownRelationList = null;
	
	@ModelAttribute
	public List<OwnRelation> getOwnRelationList(){
		if(null == ownRelationList){
			ownRelationList = (List<OwnRelation>)staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.OWN_RELATION_LIST, false);
		}
		return ownRelationList;
	}
	
	protected List<NavigatMenu> navigatMenuTopList=null;
	@ModelAttribute
	public List<NavigatMenu> getNavigatMenuTopList(){
		if(null == navigatMenuTopList){
			navigatMenuTopList = (List<NavigatMenu>) staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.NAVIGATMENU_TOP_LIST);
		
		}
		return navigatMenuTopList;
	}
	
	protected static List<Gallery> galleryList = null;
	@ModelAttribute
	public List<Gallery>  getGalleryList(){
		
		galleryList = staticDataDefineCoreManager.getGalleryList();
		return galleryList;
	}
	
	protected static List<FileSystemGallery> fileSystemGalleryList=null;
	public List<FileSystemGallery> getFileSystemGalleryDropdown(){
		fileSystemGalleryList= (List<FileSystemGallery>) staticDataDefineCoreManager.getData(StaticDataDefineCoreConstant.FILE_SYSTEM_GALLERY_DROPDOWN);
		
		return fileSystemGalleryList;
	}
}
