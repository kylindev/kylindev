package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.InstantShareCommentBusiness;
import org.jit8.site.travel.biz.service.InstantShareCommentService;
import org.jit8.site.travel.biz.service.InstantShareService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("instantShareCommentBusiness")
public class InstantShareCommentBusinessImpl implements InstantShareCommentBusiness{

	@Resource
	private InstantShareCommentService instantShareCommentService;
	
	@Resource
	private InstantShareService instantShareService;
	

	@Override
	public List<InstantShareComment> getInstantShareCommentList() {
		return instantShareCommentService.getInstantShareCommentList(false, false);
	}
	
	@Transactional
	@Override
	public InstantShareComment persist(InstantShareComment instantShareComment) {
		
		return null;
	}
	
	@Transactional
	@Override
	public InstantShareComment persist(InstantShareComment instantShareComment, User user){
		if(null != instantShareComment && null != user){
			Long id = instantShareComment.getId();
			InstantShareComment exist = null;
			if(id != null && id>0){
				 //如果是已存在的评论
				 exist = findOneByIdAndUserId(instantShareComment.getId(),user.getUserId().getUserId());
				 exist.setComment(instantShareComment.getComment());
				 exist.setSequence(instantShareComment.getSequence());
			}else{
				//如果是新增评论
				exist=instantShareComment;
				instantShareComment.setUser(user);
				Long userId = user.getUserId().getUserId();
				instantShareComment.setUserId(userId);
				instantShareComment.setOwnerId(userId);
				instantShareComment.setNickName(user.getNickname());
			}
			
			exist.setUpdateDate(new Date());
			exist.setPublishDate(new Date());
			
			//针对某一即时分享的评论
			InstantShare instantShare = exist.getInstantShare();
			if(null != instantShare){
				Long instantShareId= instantShare.getId();
				instantShare = instantShareService.findOne(instantShareId);
				int commentCount = instantShare.getCommentCount();
				instantShare.setCommentCount(commentCount+1);
				instantShareComment.setInstantShare(instantShare);
			}
			
			//是否是回复评论
			InstantShareComment parent = exist.getParent();
			if(null != parent){
				Long parentId = parent.getId();
				InstantShareComment existParent = instantShareCommentService.findOne(parentId);
				if(null != existParent){
					exist.setParent(existParent);
				}
			}
		
			return instantShareCommentService.save(exist);
		}
		return instantShareComment;
	}

	@Override
	public InstantShareComment getInstantShareCommentById(Long id) {
		return instantShareCommentService.findOne(id);
	}

	@Override
	public InstantShareComment deleteInstantShareCommentById(Long id) {
		InstantShareComment old = instantShareCommentService.findOne(id);
		old.setDeleted(true);
		return instantShareCommentService.save(old);
	}
	
	@Transactional
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			InstantShareComment instantShareComment = instantShareCommentService.findOne(id);
			updateInstantShareCommentCount(instantShareComment);
			instantShareCommentService.delete(id);
		}
	}
	
	@Transactional
	@Override
	public void removeByIdAndUserId(Long id,Long userId) {
		if(null != id && id>0 && null != userId){
			InstantShareComment instantShareComment = instantShareCommentService.findOneByIdAndOwnerId(id, userId);
			if(null != instantShareComment){
				updateInstantShareCommentCount(instantShareComment);
				instantShareCommentService.delete(id);
			}
		}
	}

	/**
	 * @param instantShareComment
	 */
	private void updateInstantShareCommentCount(InstantShareComment instantShareComment) {
		InstantShare instantShare = instantShareComment.getInstantShare();
		if(null != instantShare){
			int commentCount = instantShare.getCommentCount();
			commentCount--;
			if(commentCount<0){
				commentCount = 0;
			}
			instantShare.setCommentCount(commentCount);
			instantShareService.save(instantShare);
		}
	}

	@Override
	public Page<InstantShareComment> findAll(Pageable pageable) {
		return instantShareCommentService.findAll(pageable);
	}

	@Override
	public InstantShareComment findByCode(String code) {
		return instantShareCommentService.findByCode( code);
	}

	@Override
	public List<InstantShareComment> findByIds(List<Long> idList) {
		return instantShareCommentService.findByIds(idList);
	}

	@Override
	public InstantShareComment findOneByIdAndUserId(Long id, Long userId) {
		return instantShareCommentService.findOneByIdAndOwnerId(id, userId);
	}

	@Override
	public Page<InstantShareComment> findByInstantShareId(Long instantShareId, Pageable pageable) {
		return instantShareCommentService.findByInstantShareId(instantShareId, pageable);
	}

}
