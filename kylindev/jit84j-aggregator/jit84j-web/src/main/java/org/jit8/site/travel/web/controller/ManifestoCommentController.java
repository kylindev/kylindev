package org.jit8.site.travel.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.ManifestoCommentBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ManifestoCommentController{

	private static final Logger logger = LoggerFactory.getLogger(ManifestoCommentController.class);
	
	
	@Resource
	private ManifestoCommentBusiness manifestoCommentBusiness;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING)
	public String manifestoCommentAdd(@ModelAttribute("manifestoComment") ManifestoComment manifestoComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		if(null != user && null != userId){
			Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
			Pageable pageable2 = new PageRequest(0, 5,sort);//查头5条
			Page<ManifestoComment> page = manifestoCommentBusiness.persistWithReturnPage(manifestoComment,user,pageable2);
			model.addAttribute("msg", "添加成功");
			model.addAttribute("page", page);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifestoComment",manifestoComment);
		
		return "manifestoCommentAddResultUI";
	}
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD2_MAPPING)
	public String instantShareCommentAdd(@ModelAttribute("manifestoComment") ManifestoComment manifestoComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		model.addAttribute("depth", manifestoComment.getDepth());
		if(user!= null && null != userId){
			manifestoComment = manifestoCommentBusiness.persist(manifestoComment,user);
			model.addAttribute("msg", "回复成功");
			model.addAttribute("manifestoComment", manifestoComment);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifestoComment",manifestoComment);
		
		return "manifestoCommentAddResult2UI";
	}
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_LIST_UI_MAPPING + "/{manifestoId}")
	public String manifestoCommentList(@PathVariable("manifestoId") Long manifestoId,@ModelAttribute("manifestoComment") ManifestoComment manifestoComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_LIST_UI_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		model.addAttribute("depth", manifestoComment.getDepth());
		
		Pageable commentPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.DESC, "publishDate");
		Page<ManifestoComment> commentPage = null;
		if(null != manifestoId && manifestoId >0 && null != user && null != userId){
			commentPage = manifestoCommentBusiness.findByManifestoId(manifestoId, commentPageable);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("manifestoComment", manifestoComment);
		model.addAttribute("manifestoId", manifestoId);
		model.addAttribute("page",commentPage);
		
		return "manifestoCommentListUI";
	}
}
