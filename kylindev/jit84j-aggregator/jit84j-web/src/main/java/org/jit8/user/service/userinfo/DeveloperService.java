package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface DeveloperService extends GenericService<Developer, Long>{

	public Page<Developer> getDeveloperList(Pageable pageable);
	
	public Page<Developer> getDeveloperList(Developer developer,Pageable pageable);
	
	public Developer findByUserId(Long userId);
	
}
