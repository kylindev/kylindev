package org.jit8.site.travel.biz.service;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ManifestoCommentService extends GenericService<ManifestoComment, Long>{

	Page<ManifestoComment> findByManifestoId(Long manifestoId, Pageable pageable);
	
}
