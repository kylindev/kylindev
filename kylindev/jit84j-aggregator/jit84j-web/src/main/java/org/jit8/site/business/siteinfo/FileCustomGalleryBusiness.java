package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;



public interface FileCustomGalleryBusiness {

	public List<FileCustomGallery> getFileCustomGalleryList();
	public FileCustomGallery persist(FileCustomGallery gallery);
	public FileCustomGallery merge(FileCustomGallery gallery);
	
	public FileCustomGallery getFileCustomGalleryById(Long id);
	
	public FileCustomGallery deleteFileCustomGalleryById(Long id);
	
	public FileCustomGallery getDefaultCustomGallery();
	
}
