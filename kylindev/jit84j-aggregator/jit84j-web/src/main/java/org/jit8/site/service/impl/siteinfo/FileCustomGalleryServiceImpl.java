package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileCustomGalleryDao;
import org.jit8.site.persist.domain.siteinfo.FileCustomGallery;
import org.jit8.site.service.siteinfo.FileCustomGalleryService;
import org.springframework.stereotype.Service;

@Service
public class FileCustomGalleryServiceImpl extends GenericServiceImpl<FileCustomGallery, Long> implements FileCustomGalleryService {

	@Resource
	private FileCustomGalleryDao fileCustomGalleryDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileCustomGalleryDao;
	}


	@Override
	public List<FileCustomGallery> getFileCustomGalleryList(boolean disable, boolean deleted) {
		return fileCustomGalleryDao.getFileCustomGalleryList(disable, deleted);
	}


	@Override
	public FileCustomGallery findByCodeAndType(String code, int type) {
		return fileCustomGalleryDao.findByCodeAndType( code,  type);
	}
	
}
