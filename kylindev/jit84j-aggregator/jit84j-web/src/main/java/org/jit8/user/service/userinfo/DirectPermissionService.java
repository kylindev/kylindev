package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.DirectPermission;

public interface DirectPermissionService extends GenericService<DirectPermission, Long>{

	DirectPermission findByType(String type); 
}
