package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface StickyNoteService extends GenericService<StickyNote, Long>{

	public List<StickyNote> getStickyNoteList(boolean disable, boolean deleted);
	
	public Page<StickyNote> findAll(StickyNote stickyNote,
			Pageable pageable);
}
