package org.jit8.site.travel.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.InstantShareCommentBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InstantShareCommentController{

	private static final Logger logger = LoggerFactory.getLogger(InstantShareCommentController.class);
	
	
	@Resource
	private InstantShareCommentBusiness instantShareCommentBusiness;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_ADD_MAPPING)
	public String instantShareCommentAdd(@ModelAttribute("instantShareComment") InstantShareComment instantShareComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		model.addAttribute("depth", instantShareComment.getDepth());
		if(user!= null && null != userId){
			 instantShareComment = instantShareCommentBusiness.persist(instantShareComment,user);
			model.addAttribute("msg", "添加成功");
			model.addAttribute("instantShareComment", instantShareComment);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShareComment",instantShareComment);
		
		
		return "instantShareCommentAddResultUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_MAPPING + "/{instantShareId}")
	public String instantShareCommentList(@PathVariable("instantShareId") Long instantShareId,@ModelAttribute("instantShareComment") InstantShareComment instantShareComment,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING + " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		model.addAttribute("depth", instantShareComment.getDepth());
		
		Pageable commentPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.DESC, "publishDate");
		Page<InstantShareComment> commentPage = null;
		if(null != instantShareId && instantShareId >0 && null != user && null != userId){
			commentPage = instantShareCommentBusiness.findByInstantShareId(instantShareId, commentPageable);
		}else{
			model.addAttribute("msg","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShareComment", instantShareComment);
		model.addAttribute("instantShareId", instantShareId);
		model.addAttribute("page",commentPage);
		
		return "instantShareCommentListUI";
	}
	
	
}
