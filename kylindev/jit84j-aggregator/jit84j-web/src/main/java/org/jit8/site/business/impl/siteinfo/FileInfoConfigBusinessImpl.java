package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.site.business.siteinfo.FileInfoConfigBusiness;
import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.service.siteinfo.FileInfoConfigService;
import org.jit8.site.service.siteinfo.SiteInfoService;
import org.springframework.stereotype.Service;

@Service("fileInfoConfigBusiness")
public class FileInfoConfigBusinessImpl implements FileInfoConfigBusiness{

	@Resource
	private FileInfoConfigService fileInfoConfigService;
	
	@Resource
	private SiteInfoService siteInfoService;

	@Override
	public List<FileInfoConfig> getFileInfoConfigList() {
		return fileInfoConfigService.getFileInfoConfigList(false, false);
	}
	
	@Override
	public FileInfoConfig persist(FileInfoConfig fileInfoConfig){
		SiteInfo site = fileInfoConfig.getSiteInfo();
		SiteInfo siteInfo = null;
		if(null != site){
			Long siteId = site.getId();
			siteInfo = siteInfoService.findOne(siteId);
		}
		if(null == siteInfo){
			siteInfo = siteInfoService.getSiteInfoByType(SiteInfo.MAIN_SITE);
		}
		fileInfoConfig.setSiteInfo(siteInfo);
		Long id = fileInfoConfig.getId();
		FileInfoConfig old = null;
		if(null != id && id >0){
			old = fileInfoConfigService.findOne(fileInfoConfig.getId());
		}else{
			old = fileInfoConfig;
		}
		
		String rootPath = fileInfoConfig.getRootPath();
		if(StringUtils.isEmpty(rootPath)){
			rootPath=FileInfoConfig.ROOT_PATH_DEFAULT;
		}
		old.setRootPath(rootPath);
		old.setSiteInfo(siteInfo);
		siteInfo.setFileInfoConfig(old);
		return fileInfoConfigService.save(old);
	}
	
	@Override
	public FileInfoConfig merge(FileInfoConfig fileInfoConfig){
		Long id = fileInfoConfig.getId();
		SiteInfo site = fileInfoConfig.getSiteInfo();
		if(null != site){
			Long siteId = site.getId();
			SiteInfo siteInfo = siteInfoService.findOne(siteId);
			if(null == siteInfo){
				siteInfo = siteInfoService.getSiteInfoByType(SiteInfo.MAIN_SITE);
			}
			fileInfoConfig.setSiteInfo(siteInfo);
		}
		FileInfoConfig old = null;
		if(null != id && id >0){
			old = fileInfoConfigService.findOne(fileInfoConfig.getId());
		}else{
			old = fileInfoConfig;
		}
		
		String rootPath = fileInfoConfig.getRootPath();
		if(StringUtils.isEmpty(rootPath)){
			rootPath=FileInfoConfig.ROOT_PATH_DEFAULT;
		}
		old.setRootPath(rootPath);
		fileInfoConfigService.save(old);
		return fileInfoConfig;
	}

	@Override
	public FileInfoConfig getFileInfoConfigById(Long id) {
		return fileInfoConfigService.findOne(id);
	}

	@Override
	public FileInfoConfig deleteFileInfoConfigById(Long id) {
		FileInfoConfig old = fileInfoConfigService.findOne(id);
		old.setDeleted(true);
		return fileInfoConfigService.save(old);
	}

}
