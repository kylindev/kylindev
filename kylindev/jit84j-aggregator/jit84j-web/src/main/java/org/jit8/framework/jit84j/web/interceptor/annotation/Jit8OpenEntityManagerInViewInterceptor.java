package org.jit8.framework.jit84j.web.interceptor.annotation;

import org.springframework.dao.DataAccessException;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewInterceptor;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;

public class Jit8OpenEntityManagerInViewInterceptor extends
		OpenEntityManagerInViewInterceptor {

	@Override
	public void preHandle(WebRequest request) throws DataAccessException {
		// TODO Auto-generated method stub
		super.preHandle(request);
	}
	@Override
	public void postHandle(WebRequest request, ModelMap model) {
		// TODO Auto-generated method stub
		super.postHandle(request, model);
	}
	@Override
	public void afterCompletion(WebRequest request, Exception ex)
			throws DataAccessException {
		// TODO Auto-generated method stub
		super.afterCompletion(request, ex);
	}
}
