package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.ClassConverterUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.CategorySpecialBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.CategorySpecialService;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("categorySpecialBusiness")
public class CategorySpecialBusinessImpl implements CategorySpecialBusiness{

	@Resource
	private CategorySpecialService categorySpecialService;
	
	@Resource
	private StickyNoteCategoryBusiness stickyNoteCategoryBusiness;
	
	@Resource
	private SpecialAreaBusiness specialAreaBusiness;
	

	@Override
	public List<CategorySpecial> getCategorySpecialList() {
		return categorySpecialService.getCategorySpecialList(false, false);
	}
	
	@Transactional
	@Override
	public CategorySpecial persist(CategorySpecial categorySpecial){
		Long id = categorySpecial.getId();
		CategorySpecial exist = null;
		if(id != null && id>0){
			 exist = categorySpecialService.findOne(categorySpecial.getId());
			 exist.setDescription(categorySpecial.getDescription());
			 exist.setImagePath(categorySpecial.getImagePath());
			 exist.setName(categorySpecial.getName());
			 exist.setOpenable(categorySpecial.isOpenable());
			 exist.setWritable(categorySpecial.isWritable());
			 exist.setSequence(categorySpecial.getSequence());
			 exist.setBeginTime(categorySpecial.getBeginTime());
			 exist.setEndTime(categorySpecial.getEndTime());
			 exist.setUpdateDate(new Date());
		}else{
			exist=categorySpecial;
		}
		
		return categorySpecialService.save(exist);
	}

	@Override
	public CategorySpecial getCategorySpecialById(Long id) {
		return categorySpecialService.findOne(id);
	}

	@Transactional
	@Override
	public CategorySpecial deleteCategorySpecialById(Long id) {
		CategorySpecial old = categorySpecialService.findOne(id);
		old.setDeleted(true);
		return categorySpecialService.save(old);
	}
	
	@Transactional
	@Override
	public void removeById(Long id) {
		if(null != id && id>0){
			categorySpecialService.delete(id);
		}
	}

	@Override
	public Page<CategorySpecial> findAll(Pageable pageable) {
		return categorySpecialService.findAll(pageable);
	}
	
	@Override
	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,Pageable pageable) {
		return categorySpecialService.findAll(categorySpecial, pageable);
	}

	@Transactional
	@Override
	public void asignCategory2SpecialArea(CategorySpecial cs){
		String categoryIds = cs.getCategoryIds();
		String specialAreaIds = cs.getSpecialAreaIds();
		if(StringUtils.isNotEmpty(categoryIds) && StringUtils.isNotEmpty(specialAreaIds)){
			String[] categoryIdArray = categoryIds.split(",");
			String[] specialAreaIdArray = specialAreaIds.split(",");
			List<CategorySpecial> categorySpecialList = new ArrayList<CategorySpecial>();
			
			Map<Long,StickyNoteCategory> stickyNoteCategoryMap = new HashMap<Long,StickyNoteCategory>();
			if(null != categoryIdArray && categoryIdArray.length>0){
				List<StickyNoteCategory> stickyNoteCategoryList = stickyNoteCategoryBusiness.findByIds(Arrays.asList(ClassConverterUtil.getLong(categoryIdArray)));
				for(StickyNoteCategory stickyNoteCategory : stickyNoteCategoryList){
					stickyNoteCategoryMap.put(stickyNoteCategory.getId(), stickyNoteCategory);
				}
			}
			
			Map<Long,SpecialArea> specialAreaMap = new HashMap<Long,SpecialArea>();
			if(null != specialAreaIdArray && specialAreaIdArray.length>0){
				List<SpecialArea> specialAreaList = specialAreaBusiness.findByIds(Arrays.asList(ClassConverterUtil.getLong(specialAreaIdArray)));
				for(SpecialArea specialArea : specialAreaList){
					specialAreaMap.put(specialArea.getId(), specialArea);
				}
			}
			
			
			Map<String,CategorySpecial> map = new HashMap<String,CategorySpecial>();
			for(String categoryId : categoryIdArray){
				String categoryId2 = StringUtils.trimToNull(categoryId);
				
				if(null != categoryId2){
					long categoryIdKey = Long.valueOf(categoryId2);
					StickyNoteCategory stickyNoteCategory = stickyNoteCategoryMap.get(categoryIdKey);
					if(null == stickyNoteCategory){//这个必须存在，这里逻辑可以去掉
						stickyNoteCategory = new StickyNoteCategory();
						stickyNoteCategory.setId(categoryIdKey);
					}
					
					for(String specialAreaId : specialAreaIdArray){
						String specialAreaId2 = StringUtils.trimToNull(specialAreaId);
						long specialAreaIdKey = Long.valueOf(specialAreaId2);
						
						SpecialArea specialArea = specialAreaMap.get(specialAreaIdKey);
						
						if(null == specialArea){//这个必须存在，这里逻辑可以去掉
							specialArea = new SpecialArea();
							specialArea.setId(specialAreaIdKey);
						}
						
						CategorySpecial categorySpecial = new CategorySpecial();
						
						categorySpecial.setSpecialArea(specialArea);
						categorySpecial.setCategory(stickyNoteCategory);
						
						String key = categoryId2 + specialAreaId2;
						map.put(key, categorySpecial);
					}
				}
			}
			//从数据库里查询数据并以map分组
			List<CategorySpecial> categorySpecialExistList = categorySpecialService.findAll();
			Map<String,CategorySpecial> existMap = new HashMap<String,CategorySpecial>();
			if(null != categorySpecialExistList && categorySpecialExistList.size()>0){
				for(CategorySpecial categorySpecial : categorySpecialExistList){
					Long categoryId = categorySpecial.getCategory().getId();
					Long specialId = categorySpecial.getSpecialArea().getId();
					String categoryId2 = String.valueOf(categoryId);
					String specialAreaId2 = String.valueOf(specialId);
					String key = categoryId2 + specialAreaId2;
					existMap.put(key, categorySpecial);
				}
			}
			
			//区分是否已经存在
			if(map.size()>0){
				for(Map.Entry<String, CategorySpecial> entry : map.entrySet()){
					String key = entry.getKey();
					CategorySpecial categorySpecial = existMap.get(key);
					if(categorySpecial != null){
						categorySpecialList.add(categorySpecial);
					}else{
						categorySpecial = entry.getValue();
						
						categorySpecialList.add(entry.getValue());
						//设置默认值与类别相同
						StickyNoteCategory category = categorySpecial.getCategory();
						SpecialArea specialArea = categorySpecial.getSpecialArea();
						categorySpecial.setName(specialArea.getName()+"⚛"+category.getName());
						categorySpecial.setImagePath(category.getImagePath());
						categorySpecial.setOpenable(category.isOpenable());
						categorySpecial.setDescription(specialArea.getName()+"⚛"+category.getName()+ ":" + category.getDescription());
					}
				}
			}
			
			if(!categorySpecialList.isEmpty()){
				categorySpecialService.save(categorySpecialList);
			}
		}
	}
	
	

	@Transactional
	@Override
	public void removeCategoryFromSpecialArea(CategorySpecial cs) {
		String categoryIds = cs.getCategoryIds();
		String specialAreaIds = cs.getSpecialAreaIds();
		if(StringUtils.isNotEmpty(categoryIds) && StringUtils.isNotEmpty(specialAreaIds)){
			String[] categoryIdArray = categoryIds.split(",");
			String[] specialAreaIdArray = specialAreaIds.split(",");
			
			for(String specialAreaId : specialAreaIdArray){
				String specialAreaId2 = StringUtils.trimToNull(specialAreaId);
				long specialAreaIdKey = Long.valueOf(specialAreaId2);
			
				if(null != categoryIdArray && categoryIdArray.length>0){
					List<Long> categoryIdList = Arrays.asList(ClassConverterUtil.getLong(categoryIdArray));
					categorySpecialService.removeBySpecialIdAndCategoryIdList(specialAreaIdKey, categoryIdList);
				}
			}
		}
		
	}

	@Override
	@Transactional
	public void saveCategorySpecialList(CategorySpecial categorySpecial) {
		List<CategorySpecial> list = categorySpecial.getCategorySpecialList();
		if(null != list && list.size()>0){
			Map<Long,CategorySpecial> map = new HashMap<Long,CategorySpecial>();
			List<Long> idList = new ArrayList<Long>();
			for(CategorySpecial cs : list){
				Long id = cs.getId();
				map.put(id, cs);
				idList.add(id);
			}
			List<CategorySpecial> categorySpecialList = categorySpecialService.findByIdList(idList);
			for(CategorySpecial cs : categorySpecialList){
				if(null != cs){
					Long id = cs.getId();
					CategorySpecial current = map.get(id);
					cs.setSequence(current.getSequence());
					cs.setOpenable(current.isOpenable());
					cs.setWritable(current.isWritable());
				}
			}
			categorySpecialService.save(categorySpecialList);
		}
	}

	@Override
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id, boolean isAll) {
		if(id==null && isAll){
			return categorySpecialService.getCategorySpecialList(false, false);
		}
		return categorySpecialService.getCategorySpecialListBySpecialAreaId(id);
	}

	@Override
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList) {
		return categorySpecialService.getByCategorySpecialIdList(categorySpecialIdList);
	}

}
