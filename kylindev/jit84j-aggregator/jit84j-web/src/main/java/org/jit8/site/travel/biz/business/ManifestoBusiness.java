package org.jit8.site.travel.biz.business;

import java.util.Date;
import java.util.Map;

import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ManifestoBusiness {
	
	String MANIFESTO_CURRENT = "current";
	String MANIFESTO_PREVIOUS = "previous";
	String MANIFESTO_FOLLOWING = "following";

	Page<Manifesto> getList(Date begin, Date end,Pageable pageable);
	
	Page<Manifesto> getTodayList(Pageable pageable);
	
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable, Pageable commontPageable) ;
	
	Manifesto findTodayManifestoByUserId(Long userId);
	
	Manifesto persistTodayManifesto(Manifesto manifesto,User user);
	
	Manifesto praise(Long id);
	
	public Map<String, Manifesto> getThreeManifestosById(Long id);
	
	public Long findLastId();
}
