package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.InstantShareImageCounterDao;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.jit8.site.travel.persist.repository.InstantShareImageCounterRepository;
import org.springframework.stereotype.Repository;

@Repository
public class InstantShareImageCounterDaoImpl extends GenericDaoImpl<InstantShareImageCounter, Long> implements InstantShareImageCounterDao {

	@Resource
	private InstantShareImageCounterRepository instantShareImageCounterRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = instantShareImageCounterRepository;
	}

	@Override
	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId) {
		return instantShareImageCounterRepository.findByCountKeyAndUserId( day,  userId);
	}

	@Override
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId) {
		return instantShareImageCounterRepository.findByIdAndUserId(id, userId);
	}

	@Override
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId) {
		return instantShareImageCounterRepository.findByYearMonthAndUserId(yearMonth, userId);
	}

}
