package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;


public interface InstantShareImageCounterService extends GenericService<InstantShareImageCounter, Long>{

	public InstantShareImageCounter findByCountKeyAndUserId(String day, Long userId);
	
	public InstantShareImageCounter findByIdAndUserId(Long id, Long userId);
	
	public List<InstantShareImageCounter> findByYearMonthAndUserId(String yearMonth, Long userId);
	
}
