package org.jit8.site.travel.persist.dao;

import java.util.Date;
import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManifestoDao extends GenericDao<Manifesto, Long> {

	public Page<Manifesto> getList(Date begin, Date end, Pageable pageable);
	
	public List<Manifesto> findManifestoByPublishDateAndUserId(Date begin, Date end, Long userId);
	
	public Page<Manifesto> getTodayList(Manifesto manifesto, Pageable pageable);
	
	public Long findPreviousOneByCurrentId(Long id);
	
	public Long findFollowingOneByCurrentId(Long id);
	
	public Manifesto findSimpleOneById(Long id);
	
	public Long findLastId();
}
