package org.jit8.site.biz.siteinfo.stickynote.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.biz.siteinfo.stickynote.service.SpecialAreaService;
import org.jit8.site.persist.siteinfo.stickynote.dao.SpecialAreaDao;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.springframework.stereotype.Service;

@Service
public class SpecialAreaServiceImpl extends GenericServiceImpl<SpecialArea, Long> implements SpecialAreaService {

	@Resource
	private SpecialAreaDao specialAreaDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = specialAreaDao;
	}


	@Override
	public List<SpecialArea> getSpecialAreaList(boolean disable, boolean deleted) {
		return specialAreaDao.getSpecialAreaList(disable, deleted);
	}


	@Override
	public SpecialArea findByCode(String code) {
		return specialAreaDao.findByCode( code);
	}


	@Override
	public List<SpecialArea> findByIds(List<Long> idList) {
		return specialAreaDao.findByIds( idList);
	}
	
}
