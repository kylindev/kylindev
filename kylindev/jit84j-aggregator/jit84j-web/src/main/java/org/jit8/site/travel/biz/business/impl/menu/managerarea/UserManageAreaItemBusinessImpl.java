package org.jit8.site.travel.biz.business.impl.menu.managerarea;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.site.travel.biz.business.menu.managerarea.UserManageAreaItemBusiness;
import org.jit8.site.travel.biz.service.menu.managerarea.ManageAreaItemService;
import org.jit8.site.travel.biz.service.menu.managerarea.UserManageAreaItemService;
import org.jit8.site.travel.common.utils.TravelStaticDataUtil;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;
import org.jit8.user.business.userinfo.DeveloperBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userManageAreaItemBusiness")
public class UserManageAreaItemBusinessImpl implements UserManageAreaItemBusiness{

	@Resource
	private UserManageAreaItemService userManageAreaItemService;
	
	@Resource
	private ManageAreaItemService manageAreaItemService;
	
	@Resource
	private DeveloperBusiness developerBusiness;

	@Transactional
	@Override
	public UserManageAreaItem persist(UserManageAreaItem userManageAreaItem) {
		
		UserManageAreaItem exist = null;
		
		if(null != userManageAreaItem ){
			
			Developer developer = null;
			// check developer, if developer is null, set default developer
			Long developerId = null;
			//developerId = userManageAreaItem.getDeveloperId();
			if(null != developerId ){
				developer = developerBusiness.findById(developerId);
			}
			if(null == developer){
				developer = developerBusiness.findByUserId(KylinboyCoreDeveloperConstant._KYLINBOY_USER_ID_LONG);
				developerId = developer.getId();
			}
			
			if (null != developer) {
				Long id = userManageAreaItem.getId();
				if(null != id){
					//exist = userUserManageAreaItemService.findOne(id);
				}
				if (null != exist) {
					//exist.setCode(userManageAreaItem.getCode());
					//exist.setName(userManageAreaItem.getName());
					//exist.setDeveloperId(developerId);
					exist.setIconPath(userManageAreaItem.getIconPath());
					//exist.setUrl(userManageAreaItem.getUrl());
					exist.setOwnerId(developer.getUserId().getUserId());
					exist.setSequence(userManageAreaItem.getSequence());
					exist.setDisplay(userManageAreaItem.getDisplay());
					exist.setDisable(userManageAreaItem.isDisable());
				} else {
					exist = userManageAreaItem;
					exist.setDisable(true);// 设置默认显示
					exist.setHideable(false);// 设置默认不隐藏
					exist.setDisable(false);// 设置不禁用
					exist.setDeleted(false);// 设置软删除为false
					// userUserManageAreaItem.setIdKey(idKey); TODO
					exist.setOwnerId(developer.getUserId().getUserId());// 设置所有者，userId
					//exist.setDeveloperId(developerId);// 设置开发者
				}
				exist = userManageAreaItemService.save(exist);
			}
		}
		return userManageAreaItem;
	}
	
	@Transactional
	@Override
	public List<UserManageAreaItem> persistList(Long userId,List<UserManageAreaItem> userManageAreaItemList){
		List<UserManageAreaItem> savedList = null;
		if(null != userId){
			List<UserManageAreaItem> userManageAreaItemListExist = userManageAreaItemService.findByUserId(userId);
			Map<Long, UserManageAreaItem> map = new HashMap<Long,UserManageAreaItem>();
			if(null != userManageAreaItemListExist && userManageAreaItemListExist.size()>0){
				
				List<Long> idList = new ArrayList<Long>(userManageAreaItemListExist.size());
				for(UserManageAreaItem userManageAreaItem : userManageAreaItemListExist){
					if(null != userManageAreaItem){
						Long id = userManageAreaItem.getId();
						if(null != id && id>0){
							map.put(id, userManageAreaItem);
							idList.add(id);
						}
					}
				}
			}
			
			if(null != userManageAreaItemList && userManageAreaItemList.size()>0){
				savedList = new ArrayList<UserManageAreaItem>();
				List<UserManageAreaItem> removedList = new ArrayList<UserManageAreaItem>();
				
				for(UserManageAreaItem userManageAreaItem : userManageAreaItemList){
					Long id = userManageAreaItem.getId();
					boolean checked = userManageAreaItem.isChecked();
					if(checked){
						if(null != id){
							UserManageAreaItem exist = map.get(id);
							if(null != exist){
								exist.setUpdateDate(new Date());
								exist.setDisable(userManageAreaItem.isDisable());
								exist.setSequence(userManageAreaItem.getSequence());
								exist.setDisplay(userManageAreaItem.getDisplay());
								savedList.add(exist);
							}
						}else{
							userManageAreaItem.setUserId(userId);
							savedList.add(userManageAreaItem);
						}
					}else{
						if(null != id){
							UserManageAreaItem exist = map.get(id);
							removedList.add(exist);
						}
					}
				}
				
				if(null != removedList && removedList.size()>0){
					userManageAreaItemService.delete(removedList);
				}
				
				savedList = (List<UserManageAreaItem>) userManageAreaItemService.save(savedList);
			}
			
		}
		
		return savedList;
	}

	@Override
	public List<UserManageAreaItem> findAll() {
		List<UserManageAreaItem> all = userManageAreaItemService.findAll();
		return all;
	}
	
	@Override
	public List<UserManageAreaItem> findAllAvailable(){
		List<UserManageAreaItem> allAvailable = null;//userManageAreaItemService.findAllAvailable();
		return allAvailable;
	}

	@Override
	public List<UserManageAreaItem> findAllDisplayByUserIdFromCache(Long userId){
		List<UserManageAreaItem> userManageAreaItemList = TravelStaticDataUtil.getUserManageAreaItemListByUserId( userId);
		return userManageAreaItemList;
	}
	
	@Override
	public List<UserManageAreaItem> findAllDisplayByUserId(Long userId) {
		
		List<UserManageAreaItem> allDisplay = userManageAreaItemService.findAllDisplayByUserId(userId);
		
		return allDisplay;
	}
	
	@Override
	public List<UserManageAreaItem> findAllByUserId(Long userId){
		List<UserManageAreaItem> userManageAreaItemListExist = null;
		if(null != userId){
			userManageAreaItemListExist = userManageAreaItemService.findByUserId(userId);
		}
		return userManageAreaItemListExist;
	}
	
	
	@Override
	public List<UserManageAreaItem> findAllWithManageAreaItemByUserId(Long userId){
		
		List<ManageAreaItem> manageAreaItemList = manageAreaItemService.findAllAvailable();
		
		List<UserManageAreaItem> userManageAreaItemList = new ArrayList<UserManageAreaItem>();
		
		Map<Long, UserManageAreaItem> map = new HashMap<Long, UserManageAreaItem>();
		if(null != userId){
			
			List<UserManageAreaItem> userManageAreaItemListExist = userManageAreaItemService.findByUserId(userId);
			
			if(null != userManageAreaItemListExist && userManageAreaItemListExist.size()>0){
				for(UserManageAreaItem userManageAreaItem : userManageAreaItemListExist){
					ManageAreaItem mai = userManageAreaItem.getManageAreaItem();
					if(null != mai){
						Long maiId = mai.getId();
						map.put(maiId,userManageAreaItem);
					}
				}
			}
		}
		
		if( null != manageAreaItemList && manageAreaItemList.size()>0){
			for(ManageAreaItem manageAreaItem : manageAreaItemList){
				Long manageAreaItemId = manageAreaItem.getId();
				UserManageAreaItem userManageAreaItem = map.get(manageAreaItemId);
				if(null != userManageAreaItem){
					userManageAreaItem.setChecked(true);
					userManageAreaItem.setManageAreaItemId(manageAreaItemId);
					userManageAreaItemList.add(userManageAreaItem);
				}else{
					userManageAreaItem = new UserManageAreaItem();
					userManageAreaItem.setManageAreaItem(manageAreaItem);
					userManageAreaItem.setChecked(false);
					userManageAreaItem.setManageAreaItemId(manageAreaItemId);
					userManageAreaItemList.add(userManageAreaItem);
				}
			}
		}
		
		return userManageAreaItemList;
	}

	@Override
	public Page<UserManageAreaItem> findAll(Pageable pageable) {
		Page<UserManageAreaItem> page = userManageAreaItemService.findAll(pageable);
		List<ManageAreaItem> manageAreaItemList = manageAreaItemService.findAllAvailable();
		
		if(null != manageAreaItemList && manageAreaItemList.size()>0){
			
		}
		
		if(null != page){
			List<UserManageAreaItem> userManageAreaItemList = page.getContent();
			if(null != userManageAreaItemList && userManageAreaItemList.size()>0){
				
			}
		}
		return page;
	}

	@Override
	public UserManageAreaItem findById(Long id) {
		UserManageAreaItem userUserManageAreaItem = null;//userManageAreaItemService.findOne(id);
		return userUserManageAreaItem;
	}

	@Override
	public void removeById(Long id) {
		userManageAreaItemService.delete(id);
	}

}
