package org.jit8.framework.jit84j.web.security.captcha;

import com.octo.captcha.service.image.DefaultManageableImageCaptchaService;

public class Jit8ManageableImageCaptchaService extends DefaultManageableImageCaptchaService {

	public Jit8ManageableImageCaptchaService(  
		      com.octo.captcha.service.captchastore.CaptchaStore captchaStore,        
		      com.octo.captcha.engine.CaptchaEngine captchaEngine,  
		      int minGuarantedStorageDelayInSeconds,   
		      int maxCaptchaStoreSize,   
		      int captchaStoreLoadBeforeGarbageCollection) {  
		        super(captchaStore, captchaEngine, minGuarantedStorageDelayInSeconds,   
		            maxCaptchaStoreSize, captchaStoreLoadBeforeGarbageCollection);  
		    }  
		    public boolean hasCapcha(String id, String userCaptchaResponse) {  
		        return store.getCaptcha(id).validateResponse(userCaptchaResponse);  
		    }  
}
