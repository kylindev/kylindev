package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.UserRole;

public interface UserRoleService extends GenericService<UserRole, Long>{

	public List<UserRole> findUserRoleList(Long userId);
	
	public void removeByIdList(List<Long> userIdList);
}
