package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileSystemGalleryDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.site.service.siteinfo.FileSystemGalleryService;
import org.springframework.stereotype.Service;

@Service
public class FileSystemGalleryServiceImpl extends GenericServiceImpl<FileSystemGallery, Long> implements FileSystemGalleryService {

	@Resource
	private FileSystemGalleryDao fileSystemGalleryDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileSystemGalleryDao;
	}


	@Override
	public List<FileSystemGallery> getFileSystemGalleryList(boolean disable, boolean deleted) {
		return fileSystemGalleryDao.getFileSystemGalleryList(disable, deleted);
	}

	
}
