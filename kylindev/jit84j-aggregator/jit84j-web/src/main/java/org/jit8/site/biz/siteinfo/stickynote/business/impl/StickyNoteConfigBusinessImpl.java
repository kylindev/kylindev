package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteConfigBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteConfigService;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service("stickyNoteConfigBusiness")
public class StickyNoteConfigBusinessImpl implements StickyNoteConfigBusiness{

	@Resource
	private StickyNoteConfigService stickyNoteConfigService;

	@Override
	public List<StickyNoteConfig> getStickyNoteConfigList() {
		return stickyNoteConfigService.getStickyNoteConfigList(false, false);
	}
	
	@Override
	public StickyNoteConfig persist(StickyNoteConfig stickyNoteConfig){
		
		StickyNoteConfig old = stickyNoteConfigService.findOne(stickyNoteConfig.getId());
		BeanUtils.copyProperties(stickyNoteConfig, old);
		return stickyNoteConfigService.save(old);
	}
	
	@Override
	public StickyNoteConfig merge(StickyNoteConfig stickyNoteConfig){
		Long id = stickyNoteConfig.getId();
		if(null != id && id > 0 ){
			StickyNoteConfig old = stickyNoteConfigService.findOne(id);
			old.setStickyNoteImagePath(stickyNoteConfig.getStickyNoteImagePath());
			old.setStickyNoteMusicPath(stickyNoteConfig.getStickyNoteMusicPath());
			old.setStickyNoteVedioPath(stickyNoteConfig.getStickyNoteVedioPath());
			stickyNoteConfigService.save(old);
		}
		return stickyNoteConfig;
	}

	@Override
	public StickyNoteConfig getStickyNoteConfigById(Long id) {
		return stickyNoteConfigService.findOne(id);
	}

	@Override
	public StickyNoteConfig deleteStickyNoteConfigById(Long id) {
		StickyNoteConfig old = stickyNoteConfigService.findOne(id);
		old.setDeleted(true);
		return stickyNoteConfigService.save(old);
	}

}
