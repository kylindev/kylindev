package org.jit8.site.travel.persist.dao.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;

public interface ManageAreaItemDao extends GenericDao<ManageAreaItem, Long> {
	
	public List<ManageAreaItem> findAllAvailable();
	
	public List<ManageAreaItem> findAllDisplay();
	
	public List<ManageAreaItem> findByIdList(List<Long> idList);

}
