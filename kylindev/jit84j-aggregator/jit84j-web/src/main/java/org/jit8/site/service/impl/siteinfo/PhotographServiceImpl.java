package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.PhotographDao;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.jit8.site.service.siteinfo.PhotographService;
import org.springframework.stereotype.Service;

@Service
public class PhotographServiceImpl extends GenericServiceImpl<Photograph, Long> implements PhotographService {

	@Resource
	private PhotographDao photographDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = photographDao;
	}


	@Override
	public List<Photograph> getPhotographList(boolean disable, boolean deleted) {
		return photographDao.getPhotographList(disable, deleted);
	}
	
}
