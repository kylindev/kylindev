package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.business.menu.NavigatMenuBusiness;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.service.menu.NavigatMenuService;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class NavigatMenuController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(NavigatMenuController.class);
	
	@Resource
	private NavigatMenuBusiness navigatMenuBusiness;
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_MAPPING)
	public String navigatMenuAddUI(@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/navigatMenu/addUI #########");
		
		model.addAttribute("navigatMenu", new NavigatMenu());
		List<NavigatMenu> navigatMenuList = navigatMenuBusiness.getNavigatMenuListByParentNull();
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigatMenuList",navigatMenuList);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI));

		return "navigatMenuAddUI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_MAPPING)
	public String navigatMenuAdd(@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigatMenu/navigatMenuAdd #########");
		NavigatMenu parent = navigatMenu.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				navigatMenu.setParent(null);
			}
		}
		navigatMenuBusiness.persist(navigatMenu);
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_REMOVE_MAPPING + "{id}")
	public String navigatMenuRemove(@PathVariable("id") Long id, @ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_REMOVE_MAPPING + " #########");
			if(id!=null && id>0){
				navigatMenuBusiness.deleteNavigatMenuById(navigatMenu.getId());
			}
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_MAPPING)
	public String navigatMenuModify(@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigatMenu/navigatMenuModify #########");
		NavigatMenu parent = navigatMenu.getParent();
		if(parent!=null){
			Long id = parent.getId();
			if(id== null || id<=0){
				navigatMenu.setParent(null);
			}
		}
		Long adminNavigationId = navigatMenu.getAdminNavigationId();
		navigatMenuBusiness.merge(navigatMenu);
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING + navigatMenu.getId();
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING )
	public String navigatMenuModifyUI(Model model) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI + " #########");
				NavigatMenu navigatMenu = navigatMenuBusiness.findByCode(NavigatMenuService.TOP_CODE);
				model.addAttribute("navigatMenu",navigatMenu);
				
				List<NavigatMenu> navigatMenuList = navigatMenuBusiness.getNavigatMenuListByParentNull();
				model.addAttribute("navigatMenuList",navigatMenuList);
				for( NavigatMenu c : navigatMenuList){
					List<NavigatMenu> navigatMenuList2 = c.getChildrenNavigatMenu();
					if(null != navigatMenuList2 && navigatMenuList2.size()>0){
						for(NavigatMenu ca : navigatMenuList2){
							System.out.println(ca.getName());
						}
					}
				}
				//后台导航
				model.addAttribute("navigationTopList",navigationTopList);
				model.addAttribute("navigatMenuTopList",navigatMenuTopList);
				model.addAttribute("currentNavigatMenu",navigatMenu);
				model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI));
				
		return "navigatMenuModifyUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING + "{id}")
	public String navigatMenuModifyUI(@PathVariable("id") Long id,Model model) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI + " #########");
			if(id!= null && id>0){
				NavigatMenu navigatMenu = navigatMenuBusiness.getNavigatMenuById(id);
				model.addAttribute("navigatMenu",navigatMenu);
				
				List<NavigatMenu> navigatMenuList = navigatMenuBusiness.getNavigatMenuListByParentNull();
				model.addAttribute("navigatMenuList",navigatMenuList);
				for( NavigatMenu c : navigatMenuList){
					List<NavigatMenu> navigatMenuList2 = c.getChildrenNavigatMenu();
					if(null != navigatMenuList2 && navigatMenuList2.size()>0){
						for(NavigatMenu ca : navigatMenuList2){
							System.out.println(ca.getName());
						}
					}
				}
				//后台导航
				model.addAttribute("navigationTopList",navigationTopList);
				
				model.addAttribute("navigatMenuTopList",navigatMenuTopList);
				if(id != null && id>0){
					model.addAttribute("currentNavigatMenu",navigatMenu);
				}
				
				model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI));
				
			}
		return "navigatMenuModifyUI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING )
	public String navigatMenuListUI(@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigatMenu/navigatMenuListUI #########");
		List<NavigatMenu> navigatMenuList = navigatMenuBusiness.getNavigatMenuListByParentNull();
		//前台导航菜单
		model.addAttribute("navigatMenuList",navigatMenuList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		//后台导航
		model.addAttribute("navigationTopList",navigationTopList);
		//后台导航当前位置
		
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI));
		
		//前台导航
		model.addAttribute("currentNavigatMenu",navigatMenuList.get(0));
		return "navigatMenuListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_MAPPING )
	public String navigatMenuManagerUI(@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigatMenu/navigatMenuListUI #########");
		List<NavigatMenu> navigatMenuList = navigatMenuBusiness.getNavigatMenuListByParentNull();
		//前台导航菜单
		model.addAttribute("navigatMenuList",navigatMenuList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		//后台导航
		model.addAttribute("navigationTopList",navigationTopList);
		//后台导航当前位置
		
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI));
		
		//前台导航
		model.addAttribute("currentNavigatMenu",navigatMenuList.get(0));
		return "navigatMenuListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_MAPPING+"{id}")
	public String navigatMenuListModify(@PathVariable("id")Long id,@ModelAttribute("navigatMenu") NavigatMenu navigatMenu,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/menu/navigatMenu/navigatMenuListUI #########");
		List<NavigatMenu> navigatMenuList = navigatMenuBusiness.saveNavigatMenuList(navigatMenu);
		model.addAttribute("navigatMenuList",navigatMenuList);
		return "redirect:/admin/menu/navigatMenu/navigatMenuListUI/" + id;
	}
	
	
	
	@RequestMapping("/admin/navigatMenu/addChilrenNavigatMenu")
	public String addChilrenNavigatMenu(@PathVariable("currentNavigatMenuId") Long currentNavigatMenuId, @PathVariable("childrenNavigatMenuIds") Long[] childrenNavigatMenuIds, BindingResult result, Model model) throws IOException {
		System.out.println(currentNavigatMenuId);
		
		return "";
	}
	
	
}
