package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.UserRoleDao;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.jit8.user.service.userinfo.UserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl extends GenericServiceImpl<UserRole, Long> implements UserRoleService {

	@Resource
	private UserRoleDao userRoleDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = userRoleDao;
	}


	@Override
	public List<UserRole> findUserRoleList(Long userId) {
		return userRoleDao.findUserRoleList(userId);
	}


	@Override
	public void removeByIdList(List<Long> userIdList) {
		userRoleDao.removeByIdList(userIdList);
	}
	
	
}
