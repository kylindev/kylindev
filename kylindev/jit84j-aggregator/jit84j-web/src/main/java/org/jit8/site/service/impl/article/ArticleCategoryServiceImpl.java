package org.jit8.site.service.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.article.ArticleCategoryDao;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.jit8.site.service.article.ArticleCategoryService;
import org.springframework.stereotype.Service;

@Service
public class ArticleCategoryServiceImpl extends GenericServiceImpl<ArticleCategory, Long> implements ArticleCategoryService {

	@Resource
	private ArticleCategoryDao articleCategoryDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = articleCategoryDao;
	}


	@Override
	public List<ArticleCategory> getArticleCategoryList(boolean disable, boolean deleted) {
		return articleCategoryDao.getArticleCategoryList(disable, deleted);
	}


	@Override
	public List<ArticleCategory> getArticleCategoryListByArticleId(
			boolean disable, boolean deleted, Long articleId) {
		return articleCategoryDao.getArticleCategoryListByArticleId(disable, deleted, articleId);
	}
	
}
