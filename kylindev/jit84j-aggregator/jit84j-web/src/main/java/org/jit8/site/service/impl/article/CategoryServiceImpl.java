package org.jit8.site.service.impl.article;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.article.CategoryDao;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.service.article.CategoryService;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl extends GenericServiceImpl<Category, Long> implements CategoryService {

	@Resource
	private CategoryDao categoryDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = categoryDao;
	}


	@Override
	public List<Category> getCategoryList(boolean disable, boolean deleted) {
		return categoryDao.getCategoryList(disable, deleted);
	}


	@Override
	public List<Category> getCategoryListByIds(boolean disable,
			boolean deleted, List<Long> categoryIdList) {
		return categoryDao.getCategoryListByIds(disable, deleted, categoryIdList);
	}


	@Override
	public List<Category> getCategoryListByParentNull() {
		return categoryDao.getCategoryListByParentNull();
	}
	
}
