package org.jit8.framework.jit84j.core.mail.business;

import java.util.List;
import java.util.Map;

import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MailMessageBusiness{

	List<MailMessage> getMailMessageList(boolean disable,boolean deleted);
	
	
	public Page<MailMessage> findAll(Pageable pageable);
	
	public MailMessage findById(long id);
	
	public MailMessage persist(MailMessage mailMessage);
	
	public MailMessage sendMessage(String subject,String path, Map<String,Object> root, String mailConfig, String sender, String reciever) throws BusinessException;
	
	public MailMessage sendMessage(String subject,String path, Map<String,Object> root, String mailConfig, String reciever) throws BusinessException;
	
	public MailMessage sendMessage(String subject,String path, Map<String,Object> root, String reciever) throws BusinessException;
	
	public MailMessage sendMessage(String path, Map<String,Object> root, String reciever) throws BusinessException;
	
	public MailMessage sendMessage(String path, Map<String,Object> root) throws BusinessException;
	
	public MailMessage sendMessage(MailMessage mailMessage) throws BusinessException;
	
	public String getDefaultTemplatePath();
	

}
