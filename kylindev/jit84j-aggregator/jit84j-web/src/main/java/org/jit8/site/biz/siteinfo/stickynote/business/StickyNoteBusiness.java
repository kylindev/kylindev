package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface StickyNoteBusiness {

	public List<StickyNote> getStickyNoteList();
	public StickyNote persist(StickyNote stickyNote);
	
	public StickyNote getStickyNoteById(Long id);
	
	public StickyNote deleteStickyNoteById(Long id);
	
	public Page<StickyNote> findAll(StickyNote stickyNote, Pageable pageable);
}
