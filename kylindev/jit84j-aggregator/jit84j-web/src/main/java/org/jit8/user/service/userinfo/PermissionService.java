package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PermissionService extends GenericService<Permission, Long>{

	public Permission findPermissionByResourceUrl(String resourceUrl);
	public List<Permission> findPermissionByResourceUrls(String[] resourceUrls);
	
	public Permission findPermissionByCode(String code);
	
	public Page<Permission> getPermissionList(Pageable pageable);
	
	public Page<Permission> getPermissionList(Permission permission,
			Pageable pageable);
	
	public Permission fineById(Long id);
	
	public List<Permission> fineByIdList(List<Long> idList);
	
	public Permission findByFullUrl(String fullUrl);
}
