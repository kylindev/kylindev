package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.business.siteinfo.PhotographBusiness;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.persist.domain.siteinfo.Photograph;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PhotographController {

	private static final Logger logger = LoggerFactory.getLogger(PhotographController.class);
	
	@Resource
	private PhotographBusiness photographBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@RequestMapping("/admin/photograph/modifyUI/{id}")
	public String photographUI(@PathVariable Long id,Model model) throws IOException {
		logger.debug("######### /photographUI #########");
		Photograph photograph = new Photograph();
		if(id>0){
			photograph = photographBusiness.getPhotographById(id);
		}
		Gallery gallery = photograph.getGallery();
		if(null == gallery){
			photograph.setGallery(new Gallery());
		}
		model.addAttribute("photograph", photograph);
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		return "photographModifyUI";
	}
	
	@RequestMapping("/admin/photograph/photographAddUI")
	public String photographAddUI(Model model) throws IOException {
		logger.debug("######### /admin/photograph/addUI #########");
		Photograph photograph = new Photograph();
		photograph.setGallery(new Gallery());
		model.addAttribute("photograph", photograph);
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		
		return "photographAddUI";
	}
	
	@RequestMapping("/admin/photograph/photographListUI")
	public String photographListUI(Model model) throws IOException {
		logger.debug("######### /photographUI #########");
		List<Photograph> photographList = photographBusiness.getPhotographList();
		model.addAttribute("photographList", photographList);
		
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "photographListUI";
	}
	
	@RequestMapping("/admin/photograph/photographModify")
	public String photographModify(@ModelAttribute("photograph") Photograph photograph,BindingResult result, Model model) throws IOException {
		logger.debug("######### /photographUI #########");
		
		if(result.hasErrors()){
			return "photographModifyUI";
		}
		
		photographBusiness.merge(photograph);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:photographListUI";
	}
	
	@RequestMapping("/admin/photograph/photographAdd")
	public String photographAdd(@ModelAttribute("photograph") Photograph photograph,BindingResult result, Model model) throws IOException {
		logger.debug("######### /photographUI #########");
		
		if(result.hasErrors()){
			return "photographAddUI";
		}
		
		photographBusiness.persist(photograph);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:photographListUI";
	}
	
	
	@RequestMapping("/admin/photograph/delete/{id}")
	public String photographDelete(@PathVariable Long id, Model model) throws IOException {
		logger.debug("######### /admin/photograph/delete/{id} #########");
		photographBusiness.deletePhotographById(id);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:/admin/photograph/photographListUI";
	}
	
	
}
