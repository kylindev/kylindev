package org.jit8.user.business.userinfo;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ConstantModelBusiness {

	public ConstantModel persist(ConstantModel constantModel) throws BusinessException;
	
	public ConstantModel merge(ConstantModel constantModel);
	
	public Page<ConstantModel> getConstantModelList(Pageable pageable);
	
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,Pageable pageable);
	
	public ConstantModel findById(Long id);
	
	public void removeById(Long id);
	
}
