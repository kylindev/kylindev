package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.web.controller.command.UrlStaticMappingFillterCommand;
import org.jit8.user.business.userinfo.UrlMappingBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UrlMappingController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(UrlMappingController.class);
	

	@Resource
	private UrlMappingBusiness urlMappingBusiness;
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING )
	public String listUrlStaticMapping(Model model) throws IOException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING+"#########");
		//List<String> urlMappingList = urlMappingBusiness.getUrlMappingListFromConstant();
		List<UrlStaticMapping> urlStaticMappingList = urlMappingBusiness.getUrlStaticMappingModuleListFromConstant();
		Page<UrlStaticMapping> page = new PageImpl<UrlStaticMapping>(urlStaticMappingList);
		//model.addAttribute("urlMappingList",urlMappingList);
		model.addAttribute("urlStaticMappingList",urlStaticMappingList);
		model.addAttribute("page",page);
		
		return "urlMappingStaticListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_MAPPING )
	public String listUrlStaticMappingAjax(@ModelAttribute ConstantModel constantModel ,Model model) throws IOException, BusinessException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING+"#########");
		//List<String> urlMappingList = urlMappingBusiness.getUrlMappingListFromConstant();
		List<UrlStaticMapping> urlStaticMappingList = urlMappingBusiness.getUrlStaticMappingModuleListFromConstant();
		Page<UrlStaticMapping> page = new PageImpl<UrlStaticMapping>(urlStaticMappingList);
		//model.addAttribute("urlMappingList",urlMappingList);
		model.addAttribute("urlStaticMappingList",urlStaticMappingList);
		model.addAttribute("page",page);
		
		UrlStaticMappingFillterCommand urlStaticMappingFillterCommand = urlMappingBusiness.fillterUrlStaticMapping(constantModel);
		Page<UrlStaticMapping> allPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getAllUrlStaticMappingList());
		Page<UrlStaticMapping> addedPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getAddedUrlStaticMappingList());
		Page<UrlStaticMapping> modifiedPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getModifiedUrlStaticMappingList());
		Page<UrlStaticMapping> existPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getExistUrlStaticMappingList());
		//model.addAttribute("urlMappingList",urlMappingList);
		model.addAttribute("urlStaticMappingFillterCommand",urlStaticMappingFillterCommand);
		model.addAttribute("allPage",allPage);
		model.addAttribute("addedPage",addedPage);
		model.addAttribute("modifiedPage",modifiedPage);
		model.addAttribute("existPage",existPage);
		
		return "urlMappingStaticListAjaxUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_MAPPING )
	public String listUrlStaticMappingFilterAjax(ConstantModel constantModel ,Model model) throws IOException, BusinessException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING+"#########");
		//List<String> urlMappingList = urlMappingBusiness.getUrlMappingListFromConstant();
		constantModel.setClazzFullName("org.jit8.user.developer.common.constants.kylilnboy.KylinboyDeveloperMappingConstant");
		UrlStaticMappingFillterCommand urlStaticMappingFillterCommand = urlMappingBusiness.fillterUrlStaticMapping(constantModel);
		Page<UrlStaticMapping> allPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getAllUrlStaticMappingList());
		Page<UrlStaticMapping> addedPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getAddedUrlStaticMappingList());
		Page<UrlStaticMapping> modifiedPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getModifiedUrlStaticMappingList());
		Page<UrlStaticMapping> existPage = new PageImpl<UrlStaticMapping>(urlStaticMappingFillterCommand.getExistUrlStaticMappingList());
		//model.addAttribute("urlMappingList",urlMappingList);
		model.addAttribute("urlStaticMappingFillterCommand",urlStaticMappingFillterCommand);
		model.addAttribute("allPage",allPage);
		model.addAttribute("addedPage",addedPage);
		model.addAttribute("modifiedPage",modifiedPage);
		model.addAttribute("existPage",existPage);
		
		return "urlMappingStaticListAjaxUI";
	}
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_IMPORT_MAPPING )
	public Map<String,String> importIntoDB(Model model) throws IOException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING+"#########");
		//List<String> urlMappingList = urlMappingBusiness.getUrlMappingListFromConstant();
		List<UrlStaticMapping> urlStaticMappingList = urlMappingBusiness.getUrlStaticMappingModuleListFromConstant();
		
		List<UrlMapping> urlMappingList = new ArrayList<UrlMapping>();
		for(UrlStaticMapping urlStaticMapping : urlStaticMappingList){
			List<UrlStaticMapping> childUrlList = urlStaticMapping.getChildUrlList();
			if(childUrlList!= null && childUrlList.size()>0){
				for(UrlStaticMapping urlChild : childUrlList){
					UrlMapping urlMapping =new UrlMapping();
					urlMappingList.add(urlMapping);
					urlMapping.setUrlStaticMapping(urlChild);
				}
			}
			
		}
		
		urlMappingBusiness.persist(urlMappingList);
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		result.put("msg", msg);
		return result;
	}
	
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING )
	public Map<String,String> persistByClazz(ConstantModel constantModel, Model model) throws IOException, BusinessException {
		urlMappingBusiness.persistByClazz(constantModel);
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		result.put("msg", msg);
		
		return result;
	}
	
	
	/**
	 * 按分类查找URL
	 * @param urlStaticMapping
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_MAPPING )
	public String findUrlStaticMapping(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model,@PageableDefaults(pageNumber = 0, value = 10)Pageable pageable){
		
		Page<UrlStaticMapping> page = urlMappingBusiness.findUrlStaticMapping(urlStaticMapping.getIdKey(), pageable);
		
		model.addAttribute("page",page);
		
		return "urlStaticMappingListUI2";
	}
	
	/**
	 * 查找URL分类
	 * @param urlStaticMapping
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_MAPPING )
	public String findUrlStaticMappingModule(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, @PageableDefaults(pageNumber = 0, value = 10)Pageable pageable,Model model){
		
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		
		
		Page<UrlStaticMapping> page = urlMappingBusiness.findModule(pageable);
		
		model.addAttribute("page",page);
		
		return "urlStaticMappingModuleListUI";
	}
			
	/**
	 * 绑定到权限		
	 * @param urlStaticMapping
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws BusinessException
	 */
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING )
	public Map<String,String> bindToPermissionByIdKeys(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model) throws IOException, BusinessException {
		
		urlMappingBusiness.bindToPermissionByIdKeys(urlStaticMapping);
		
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.urlMapping.bindToPermission.success",null);
		result.put("msg", msg);
		
		return result;
	}
	
	/**
	 * 解除关联权限		
	 * @param urlStaticMapping
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING )
	public Map<String,String> unbindFromPermissionByIdKeys(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model){
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.urlMapping.unbindFromPermission.success",null);
		try {
			urlMappingBusiness.unbindFromPermissionByIdKeys(urlStaticMapping);
		} catch (BusinessException e) {
			logger.error(e.getMessage(),e);e.printStackTrace();
			msg = MessageUtil.getLocaleMessage(e.getCode(),e.getParams());
		}
		
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", msg);
		
		return result;
	}
	
	
	/**
	 * 关联所有url到权限		
	 * @param urlStaticMapping
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_MAPPING )
	public Map<String,String> bindAllToPermissionByList(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model){
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.urlMapping.bindAllToPermission.success",null);
		try {
			urlMappingBusiness.bindAllToPermissionByList();
		} catch (BusinessException e) {
			logger.error(e.getMessage(),e);e.printStackTrace();
			msg = MessageUtil.getLocaleMessage(e.getCode(),e.getParams());
		}
		
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", msg);
		
		return result;
	}
	
	/**
	 * URL初始化页面
	 * @param urlStaticMapping
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_MAPPING )
	public String urlStaticMappingInitUI(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, @PageableDefaults(pageNumber = 0, value = 10)Pageable pageable,Model model){
		
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		
		
		return "urlStaticMappingInitUI";
	}
	
	
	
	
	
	
	/**
	 * 按分类查找URL
	 * @param urlStaticMapping
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING )
	public String findUrlStaticMappingForNavigation(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model,@PageableDefaults(pageNumber = 0, value = 10)Pageable pageable){
		
		Page<UrlStaticMapping> page = urlMappingBusiness.findUrlStaticMapping(urlStaticMapping.getIdKey(), pageable);
		
		model.addAttribute("page",page);
		
		return "urlStaticMappingListUI2ForNavigation";
	}
	
	
	/**
	 * 查找URL分类
	 * @param urlStaticMapping
	 * @param model
	 * @param pageable
	 * @return
	 */
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_MAPPING )
	public String findUrlStaticMappingModuleForNavigation(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, @PageableDefaults(pageNumber = 0, value = 10)Pageable pageable,Model model){
		
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		
		Page<UrlStaticMapping> page = urlMappingBusiness.findModule(pageable);
		
		model.addAttribute("page",page);
		
		return "urlStaticMappingModuleListForNavigationUI";
	}
	
	/**
	 * 绑定到后台导航菜单	
	 * @param urlStaticMapping
	 * @param model
	 * @return
	 * @throws IOException
	 * @throws BusinessException
	 */
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING )
	public Map<String,String> bindToNavigationByIdKeys(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model) throws IOException, BusinessException {
		
		urlMappingBusiness.bindToNavigationByIdKeys(urlStaticMapping);
		
		Map<String,String> result = new HashMap<String,String>();
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.urlMapping.bindToNavigation.success",null);
		result.put("msg", msg);
		
		return result;
	}
	
	
	
	/**
	 * 解除关联导航菜单		
	 * @param urlStaticMapping
	 * @param model
	 * @return
	 */
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING )
	public Map<String,String> unbindFromNavigationByIdKeys(@ModelAttribute("urlStaticMapping")UrlStaticMapping urlStaticMapping, Model model){
		
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.urlMapping.unbindFromNavigation.success",null);
		try {
			urlMappingBusiness.unbindFromNavigationByIdKeys(urlStaticMapping);
		} catch (BusinessException e) {
			logger.error(e.getMessage(),e);e.printStackTrace();
			msg = MessageUtil.getLocaleMessage(e.getCode(),e.getParams());
		}
		
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", msg);
		
		return result;
	}
}
