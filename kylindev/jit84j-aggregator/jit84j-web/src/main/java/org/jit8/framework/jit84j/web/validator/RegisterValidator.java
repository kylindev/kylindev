package org.jit8.framework.jit84j.web.validator;

import javax.annotation.Resource;

import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class RegisterValidator extends LoginValidator {

	@Resource
	private UserBusiness userBusiness;
	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz); 
	}

	@Override
	public void validate(Object target, Errors errors) {
		//ValidationUtils.rejectIfEmpty(errors, "username", null, "Username is empty.");  
	    super.validate(target, errors);   
	    if(target instanceof User){
			User user = (User) target;  
	       String username = user.getUsername();
	       String email = user.getEmail();
	       String confirmPassword = user.getConfirmPassword();
	       String password=user.getPassword();
	       String firstname = user.getFirstname();
	       String lastname = user.getLastname();
	       
	       password = org.apache.commons.lang.StringUtils.trimToEmpty(password);
	       user.setPassword(password);
	       confirmPassword = org.apache.commons.lang.StringUtils.trimToEmpty(confirmPassword);
	       user.setConfirmPassword(confirmPassword);
	       
	       firstname = org.apache.commons.lang.StringUtils.trimToEmpty(firstname);
	       lastname = org.apache.commons.lang.StringUtils.trimToEmpty(lastname);
	       user.setFirstname(firstname);
	       user.setLastname(lastname);
	       
	       if(org.apache.commons.lang.StringUtils.isNotEmpty(username)){
	    	   boolean exist = userBusiness.exitUserByUsername(username);
	    	   if(exist){
	    		   errors.rejectValue("username", null, "该用户名["+username+"]已被注册，请更换一个");  
	    	   }
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isNotEmpty(user.getEmail())){
	    	   
	    	   boolean exist = userBusiness.exitUserByEmail(email);
	    	   if(exist){
	    		   errors.rejectValue("email", null, "该Email["+username+"]已被注册，请更换一个");  
	    	   }
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isEmpty(confirmPassword)){
	    	   errors.rejectValue("confirmPassword", null, "确认密码不能为空");
	       }
	       
	       if(!password.equals(confirmPassword)){
	    	   errors.rejectValue("password", null, "两次密码输入不一致");
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isEmpty(firstname)){
	    	   errors.rejectValue("firstname", null, "敢问您的名字是？");
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isEmpty(lastname)){
	    	   errors.rejectValue("lastname", null, "您贵姓？");
	       }
	    }
	       
	       
	}
	@Override
	protected void validateEmailAndUsername(Errors errors, String username,	String email) {
		if(org.apache.commons.lang.StringUtils.isEmpty(username)){
			
	    	   errors.rejectValue("username", null, "用户名不能为空");  
	    }
		
		if(org.apache.commons.lang.StringUtils.isEmpty(email)){
	    	   errors.rejectValue("email", null, "Email不能为空");  
	    }
	
	}

}
