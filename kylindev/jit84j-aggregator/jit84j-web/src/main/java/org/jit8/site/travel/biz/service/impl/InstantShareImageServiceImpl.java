package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.InstantShareImageService;
import org.jit8.site.travel.persist.dao.InstantShareImageDao;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class InstantShareImageServiceImpl extends GenericServiceImpl<InstantShareImage, Long> implements InstantShareImageService {

	@Resource
	private InstantShareImageDao instantShareImageDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = instantShareImageDao;
	}


	@Override
	public List<InstantShareImage> getInstantShareImageList(boolean disable, boolean deleted) {
		return instantShareImageDao.getInstantShareImageList(disable, deleted);
	}


	@Override
	public InstantShareImage findByCode(String code) {
		return instantShareImageDao.findByCode( code);
	}


	@Override
	public List<InstantShareImage> findByIds(List<Long> idList) {
		return instantShareImageDao.findByIds( idList);
	}


	@Override
	public void removeByIdAndUserId(Long id, Long userId) {
		instantShareImageDao.removeByIdAndUserId( id,  userId);
	}


	@Override
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable) {
		
		Page<InstantShareImage> page = instantShareImageDao.findAll( instantShareImage,  pageable);
		
		return page;
	}


	@Override
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable) {
		return instantShareImageDao.findByInstantShareId(instantShareId, pageable);
	}
	
}
