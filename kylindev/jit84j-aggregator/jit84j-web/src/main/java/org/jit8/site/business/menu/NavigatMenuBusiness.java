package org.jit8.site.business.menu;

import java.util.List;

import org.jit8.site.persist.domain.menu.NavigatMenu;



public interface NavigatMenuBusiness {

	public List<NavigatMenu> getNavigatMenuList();
	public NavigatMenu persist(NavigatMenu navigatMenu);
	
	public NavigatMenu merge(NavigatMenu navigatMenu);
	
	public NavigatMenu getNavigatMenuById(Long id);
	
	public NavigatMenu deleteNavigatMenuById(Long id);
	
	public List<NavigatMenu> getNavigatMenuListByIds(String navigatMenuIdList);
	
	public List<NavigatMenu> getNavigatMenuListByParentNull();
	
	public List<NavigatMenu> saveNavigatMenuList(NavigatMenu navigatMenu);
	
	public List<NavigatMenu> getNavigatMenuByCode(String code);
	
	public List<NavigatMenu> getTopNavigatMenuByCode();
	
	public List<NavigatMenu> getNavigatMenuByTopCode();
	
	public NavigatMenu findByCode(String code);
}
