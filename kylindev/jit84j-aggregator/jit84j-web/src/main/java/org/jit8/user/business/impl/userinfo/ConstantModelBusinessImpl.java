package org.jit8.user.business.impl.userinfo;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.dozer.Mapper;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.user.business.userinfo.ConstantModelBusiness;
import org.jit8.user.developer.common.constants.CommonDeveloperInterface;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.service.userinfo.ConstantModelService;
import org.jit8.user.service.userinfo.DeveloperService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("constantModelBusiness")
public class ConstantModelBusinessImpl implements ConstantModelBusiness {

	private static final Logger logger = LoggerFactory.getLogger(ConstantModelBusinessImpl.class);
	
	@Resource
	private ConstantModelService constantModelService;
	
	@Resource
	private DeveloperService developerService;
	
	@Resource
	private Mapper mapper;
	
    @Transactional
	@Override
	public ConstantModel persist(ConstantModel constantModel) throws BusinessException{
    	
    		String clazzFullName = constantModel.getClazzFullName();
    		if(StringUtils.isNotEmpty(clazzFullName)){
    			try {
					Object obj = Class.forName(clazzFullName);
					if(obj instanceof CommonDeveloperInterface){
						CommonDeveloperInterface  commonDeveloperInterface = (CommonDeveloperInterface)obj;
						Long developerUserId = commonDeveloperInterface.getDeveloperUserId();
						Developer developer = developerService.findByUserId(developerUserId);
						constantModel.setDeveloper(developer);
					}
				} catch (ClassNotFoundException e) {
					logger.error("class not found:"+clazzFullName,e);
				}
    		}else{
    			throw new BusinessException("clazzFullName.empty.error", "clazzFullName is empty");
    		}
    	
		return constantModelService.save(constantModel);
	}
    
    @Transactional
   	@Override
   	public ConstantModel merge(ConstantModel constantModel){
    	Long id = constantModel.getId();
    	if(id != null && id >0){
    		ConstantModel constantModelExist = constantModelService.findOne(id);
    		constantModelExist.setClazzFullName(constantModel.getClazzFullName());
    		constantModelExist.setDescription(constantModel.getDescription());
    		constantModelExist.setTitle(constantModel.getTitle());
    		
    		
    		String clazzFullName = constantModel.getClazzFullName();
    		if(StringUtils.isNotEmpty(clazzFullName)){
    			try {
					Object obj = Class.forName(clazzFullName).newInstance();
					if(obj instanceof CommonDeveloperInterface){
						CommonDeveloperInterface  commonDeveloperInterface = (CommonDeveloperInterface)obj;
						Long developerUserId = commonDeveloperInterface.getDeveloperUserId();
						Developer developer = developerService.findByUserId(developerUserId);
						constantModelExist.setDeveloper(developer);
					}
				} catch (ClassNotFoundException e) {
					logger.error("class not found:"+clazzFullName,e);
				} catch (InstantiationException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
    		}
    		
    		constantModel = constantModelService.save(constantModelExist);
    	}
    	return constantModel;
    }

	@Override
	public Page<ConstantModel> getConstantModelList(Pageable pageable) {
		
		return constantModelService.getConstantModelList(pageable);
	}
	
	@Override
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,Pageable pageable){
		return constantModelService.getConstantModelList( constantModel, pageable);
	}
	
	@Override
	public ConstantModel findById(Long id) {
		return constantModelService.findOne(id);
	}
	

	
	public static void main(String[] args) {
		//Class.forName("org.jit8.user.developer.common.constants.kylilnboy.KylinboyDeveloperMappingConstant");
		//CommonDeveloperInterface
		
		
	}

	@Override
	public void removeById(Long id) {
		
		constantModelService.delete(id);
	}
}
