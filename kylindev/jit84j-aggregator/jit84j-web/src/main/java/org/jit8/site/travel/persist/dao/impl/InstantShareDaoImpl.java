package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.InstantShareDao;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.repository.InstantShareRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

@Repository
public class InstantShareDaoImpl extends GenericDaoImpl<InstantShare, Long> implements InstantShareDao {

	@Resource
	private InstantShareRepository instantShareRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = instantShareRepository;
	}

	@Override
	public List<InstantShare> getInstantShareList(boolean disable, boolean deleted) {
		return instantShareRepository.getInstantShareList(disable, deleted);
	}
	
	@Override
	public InstantShare findByCode(String code) {
		return instantShareRepository.findByTitle( code);
	}

	@Override
	public List<InstantShare> findByIds(List<Long> idList) {
		return instantShareRepository.findByIds(idList);
	}

	@Override
	public Page<InstantShare> findByUserId(Long userId, Pageable pageable) {
		return instantShareRepository.findByUserId( userId,  pageable);
	}

	@Override
	public Long findPreviousOneByCurrentId(Long id) {
		return instantShareRepository.findPreviousOneByCurrentId(id);
	}

	@Override
	public Long findFollowingOneByCurrentId(Long id) {
		return instantShareRepository.findFollowingOneByCurrentId(id);
	}

	@Override
	public InstantShare findSimpleOneById(Long id) {
		return instantShareRepository.findSimpleOneById( id);
	}
}
