package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.DivisionDao;
import org.jit8.site.travel.persist.domain.Division;
import org.jit8.site.travel.persist.repository.DivisionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class DivisionDaoImpl extends GenericDaoImpl<Division, Long> implements DivisionDao {

	@Resource
	private DivisionRepository divisionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = divisionRepository;
	}

	@Override
	public List<Division> getDivisionList(boolean disable, boolean deleted) {
		return divisionRepository.getDivisionList(disable, deleted);
	}
	
	@Override
	public Division findByCode(String code) {
		return divisionRepository.findByCode( code);
	}

	@Override
	public List<Division> findByIds(List<Long> idList) {
		return divisionRepository.findByIds(idList);
	}
}
