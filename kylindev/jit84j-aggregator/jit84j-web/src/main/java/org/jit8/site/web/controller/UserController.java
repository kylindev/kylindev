package org.jit8.site.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MailMessageUtils;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.business.siteinfo.FileInfoBusiness;
import org.jit8.user.business.userinfo.EmailUrlBusiness;
import org.jit8.user.business.userinfo.PermissionCategoryBusiness;
import org.jit8.user.business.userinfo.RoleBusiness;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.business.userinfo.UserRoleBusiness;
import org.jit8.user.developer.common.constants.URLConstant;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.EmailUrl;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserRole;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	

	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private RoleBusiness roleBusiness;

	@Resource
	private UserRoleBusiness userRoleBusiness;
	
	@Resource
	private PermissionCategoryBusiness permissionCategoryBusiness;
	
	@Resource
	private FileInfoBusiness fileInfoBusiness;
	
	@Resource
	private EmailUrlBusiness emailUrlBusiness;
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_LAST_REGISTER_LIST_UI_MAPPING )
	public String lastRegisteUserListUI(@ModelAttribute("user") User user,Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_USER_LAST_REGISTER_LIST_UI_MAPPING +" #########");
		Pageable pageable2 = new PageRequest(0, 50, new Sort(new Order(Direction.DESC, "uniqueIdentifier")));
		Page<User> page =userBusiness.getLastUserList(pageable2);
		model.addAttribute("page", page);
		return "lastRegisteUserListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING )
	public String userListUI(@ModelAttribute("user") User user,Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) {
		logger.debug("######### /admin/userinfo/userListUI #########");
		Page<User> page =userBusiness.getUserList(user,pageable);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("page", page);
		model.addAttribute("user", user);
		return "userListUI";
	}
	//http://localhost:dddd/1_code
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING + "/{id}")
	public String userEditUI(@PathVariable("id") String id,Model model) {
		logger.debug("######### userEditUI #########");
		User user = userBusiness.findUserById(new Long(id));
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(URLConstant._USER_ADD));
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("user",user);
		int roleLevel = 0;
		model.addAttribute("roleList",roleBusiness.getRoleListByRoleLevel(roleLevel));
		return "userEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING )
	public String registerUserUI(@ModelAttribute("user") User user, Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING + " #########");
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI;
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		int roleLevel = 0;
		model.addAttribute("roleList",roleBusiness.getRoleListByRoleLevel(roleLevel));
		
		return "addUserUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING )
	public String userListForAsignRoleUI(@ModelAttribute("user") User user,Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING + " #########");
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		
		Page<User> page =userBusiness.getUserList(user,pageable);
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		int roleLevel = 0;
		model.addAttribute("roleList",roleBusiness.getRoleListByRoleLevel(roleLevel));
		model.addAttribute("page", page);
		model.addAttribute("user",user);
		return "userListForAsignRoleUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING )
	public String asignRolesForUser(@ModelAttribute("userRole")UserRole userRole,Model model) {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING+" #########");
		userRole.setUser((User)this.getSession("currentUser"));
		userRoleBusiness.asignRolesForUser(userRole);
		return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING ;
	}
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_MAPPING )
	public String removeRolesForUser(@ModelAttribute("userRole")UserRole userRole,Model model) {
		logger.debug("######### /admin/userinfo/removeRolesForUser/ #########");
		userRole.setUser((User)this.getSession("currentUser"));
		userRoleBusiness.removeRolesForUser(userRole);
		return "";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_MAPPING )
	public String registerUser(@ModelAttribute("user")User user, Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_MAPPING + " #########");
		user.setCurrentUser(getCurrentUser());
		userBusiness.addUser(user);
		return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING ;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_MAPPING)
	public String saveUser(@ModelAttribute("user")User user, Model model) throws IOException {
		logger.debug("######### /saveUser #########");
		String id=String.valueOf(userBusiness.saveUpdte(user));
		return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING + "/" +id;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_MAPPING + "/{id}")
	public String deleteUser(@PathVariable("id") String id, Model model) throws IOException {
		logger.debug("######### /deleteUser #########");
		if(!StringUtils.isEmpty(id)){
			userBusiness.deleteUserById(new Long(id));
		}
		return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING;
		
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING)
	public String asignPermissionForUserUI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING +" #########");
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Pageable pageable2 = new PageRequest(0, 200);
		
		Page<PmnCategory> page =permissionCategoryBusiness.getPermissionCategoryList(pageable2);
		model.addAttribute("page", page);
		int roleLevel = 0;
		//获取用户列表：这里的用户列表应满足，只能获取<=当前登录用户所属最高角色级别的那些用户（TODO），现在暂用获取所有用户代替
		model.addAttribute("userPage",userBusiness.getUserList(pageable));
		model.addAttribute("user",new User());
		
		return "asignPermissionForUserUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING)
	public String selectedUserUI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		//获取用户列表：这里的用户列表应满足，只能获取<=当前登录用户所属最高角色级别的那些用户（TODO），现在暂用获取所有用户代替
		model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		
		return "selectedUserUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING)
	public String selectedUser3UI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		//获取用户列表：这里的用户列表应满足，只能获取<=当前登录用户所属最高角色级别的那些用户（TODO），现在暂用获取所有用户代替
		Page<User> page = userBusiness.getUserListByUserIdNotNull(user,pageable);
		model.addAttribute("userPage",page);
		
		return "selectedUser3UI";
	}
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING)
	@ResponseBody
	public String addPermissionForUser(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION;
		userBusiness.asignPermissionForUser(user);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.asignPermissionForUser.success", null);
		return success;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_MAPPING)
	@ResponseBody
	public String removePermissionForUser(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION;
		userBusiness.removePermissionForUser(user);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.removePermissionForUser.success", null);
		return success;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING)
	@ResponseBody
	public String removeAllPermissionForUser(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION;
		userBusiness.removeAllPermissionForUser(user);
		//model.addAttribute("userPage",userBusiness.getUserList(user,pageable));
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.removeAllPermissionForUser.success", null);
		return success;
	}
	
	
	@RequestMapping("/admin/user/o_remove_all_user_permission_ou_1000001_uP2~{code}~{name}")
	public String getUser(@PathVariable("code") String code,@PathVariable("name") String name,@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING +" #########");
		String success = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.user.removeAllPermissionForUser.success", null);
		return "userUrl";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING)
	public String findPasswordUI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		return "findPasswordUI";
	}
	
	//找回密码提交
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_MAPPING)
	public String findPassword2UI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		try {
			boolean pass = validateEmailEmpty(user, model);
			if(pass){
				 MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user, fileInfoBusiness.getEmailStoredPath(), request);
				userBusiness.findBackPassword(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_UI_MAPPING,user,mailMessage);
				model.addAttribute("msg","重置密码的邮件已发送至您的邮箱【" + user.getEmail() + "】，请您登录邮箱来重置并激活新密码");
			}
			
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		model.addAttribute("navigationTopList",navigationTopList);
		
		return "findPassword2UI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_UI_MAPPING + "/{urlCode}")
	public String resetPasswordByEmailUI(@PathVariable("urlCode")String urlCode,@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		
		if(StringUtils.isEmpty(urlCode)){
			model.addAttribute("invalid@path","非法路径");
		}
		
		EmailUrl emailUrl = emailUrlBusiness.findByUrlCode(urlCode);
		if(null == emailUrl){
			model.addAttribute("invalid@path","非法路径");
		}else{
			Date expire = emailUrl.getExpireTime();
			Date currentTime = new Date();
			if(currentTime.after(expire)){
				model.addAttribute("expired@path","重置链接已过期，请重新申请");
			}
			
			if(emailUrl.isActived()){
				model.addAttribute("actived@path","重置链接已失效，请重新申请");
			}
		}
		
		model.addAttribute("urlCode",urlCode);
		return "resetPasswordByEmailUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_ADMIN_UI_MAPPING)
	public String resetPasswordByEmailAdminUI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		
		return "resetPasswordByEmailAdminUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_UI_MAPPING)
	public String resetPasswordUI(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_FIND_PASSWORD_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		return "resetPasswordUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_MAPPING)
	public String resetPassword(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		String passwrod = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		String username = user.getUsername();
		String email = user.getEmail();
		
		boolean pass = validateResetPassword(user, model);
		if(pass){
			try {
				MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user,fileInfoBusiness.getEmailStoredPath(), request);
				userBusiness.resetPassword(user, mailMessage);
				model.addAttribute("msg", "注册邮箱为【"+email+"】的用户密码重置成功");
				logger.info("重置成功");
			} catch (BusinessException e) {
				logger.error(e.getCode(),e);
				model.addAttribute("resetPassword@email@invalid", e.getMessage());
			}
		}
		model.addAttribute("navigationTopList",navigationTopList);
		return "resetPassword2UI";

	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_DO_MAPPING)
	public String resetPasswordDo(@ModelAttribute("user") User user, @ModelAttribute("emailUrl") EmailUrl emailUrl, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_EMAIL_DO;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		
		boolean pass = validateResetPasswordEmail(user,emailUrl, model);
		if(pass){
			try {
				 user.setEmail(emailUrl.getReciever());
				 String email = user.getEmail();
				 MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user,fileInfoBusiness.getEmailStoredPath(), request);
				 
				userBusiness.resetPassword(user,emailUrl,mailMessage);
				model.addAttribute("msg", "注册邮箱为【"+email+"】的用户密码重置成功");
				logger.info("重置成功");
			} catch (BusinessException e) {
				logger.error(e.getCode(),e);
				model.addAttribute("resetPassword@email@invalid", e.getMessage());
			}
		}
		model.addAttribute("navigationTopList",navigationTopList);
		return "resetPassword2UI";

	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_AUTO_MAPPING)
	public String resetPasswordAuto(@ModelAttribute("user") User user, Model model,@PageableDefaults(pageNumber = 0, value = 2) Pageable pageable, HttpServletRequest request) throws IOException {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_RESET_PASSWORD;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		String email = user.getEmail();
		
		boolean pass = validateResetPasswordAuto(user, model);
		if(pass){
			try {
				 MailMessage mailMessage = MailMessageUtils.getEmailHtmlStoredRealPath(user,fileInfoBusiness.getEmailStoredPath(), request);
				 String password = RandomStringUtils.random(6,"utf-8");
				 user.setPassword(password);
				 user.setConfirmPassword(password);
				userBusiness.resetPassword(user, mailMessage);
				model.addAttribute("msg", "注册邮箱为【"+email+"】的用户密码重置成功");
				logger.info("重置成功");
			} catch (BusinessException e) {
				logger.error(e.getCode(),e);
				model.addAttribute("resetPassword@email@invalid", e.getMessage());
			}
		}
		model.addAttribute("navigationTopList",navigationTopList);
		return "resetPassword2UI";//resetPasswordUI( user,  model, pageable);

	}
	
	
	
	private boolean validateResetPassword(User user,Model model){
		boolean pass = true;
		String passwrod = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		String email = user.getEmail();
		
		if(StringUtils.isEmpty(email)){
			model.addAttribute("resetPassword@email@empty", "注册邮箱不能为空");
			pass = false;
		}else{
			pass = validateEmailEmpty(user,model);
		}
		
		if(StringUtils.isEmpty(passwrod)){
			model.addAttribute("resetPassword@passwrod@empty", "New Passwrod不能为空");
			pass = false;
		}
		
		if(StringUtils.isEmpty(confirmPassword)){
			model.addAttribute("resetPassword@confirmPassword@empty", "Confirm Password 不能为空");
			pass = false;
		}
		
		if(StringUtils.isNotEmpty(passwrod) && StringUtils.isNotEmpty(confirmPassword) && !passwrod.equals(confirmPassword)){
			model.addAttribute("resetPassword@passwrod@confirmPassword@conflict", "New Passwrod 与 Confirm Password不一致");
			pass = false;
		}
		
		return pass;
	}
	
	private boolean validateResetPasswordEmail(User user, EmailUrl emailUrl,Model model){
		boolean pass = true;
		String passwrod = user.getPassword();
		String confirmPassword = user.getConfirmPassword();
		String urlCode = emailUrl.getUrlCode();
		
		if(StringUtils.isEmpty(urlCode)){
			model.addAttribute("msg", "非法路径");
			return false;
		}
		EmailUrl emailUrlExist = emailUrlBusiness.findByUrlCode(urlCode);
		if(null == emailUrlExist){
			
			model.addAttribute("msg", "非法路径");
			pass = false;
			return false;
		}else{
			boolean actived = emailUrlExist.isActived();
			if(actived){
				model.addAttribute("msg", "链接已失效");
				return false;
			}
			Date expired = emailUrlExist.getExpireTime();
			Date activeTime = emailUrlExist.getActiveTime();
			Date date = new Date();
			if(date.after(expired) || activeTime!=null){
				model.addAttribute("msg", "链接已失效");
				pass = false;
			}
			emailUrl.setReciever(emailUrlExist.getReciever());
		}
		
		
		if(StringUtils.isEmpty(passwrod)){
			model.addAttribute("resetPassword@passwrod@empty", "New Passwrod不能为空");
			return false;
		}
		
		if(StringUtils.isEmpty(confirmPassword)){
			model.addAttribute("resetPassword@confirmPassword@empty", "Confirm Password 不能为空");
			return false;
		}
		
		if(StringUtils.isNotEmpty(passwrod) && StringUtils.isNotEmpty(confirmPassword) && !passwrod.equals(confirmPassword)){
			model.addAttribute("resetPassword@passwrod@confirmPassword@conflict", "New Passwrod 与 Confirm Password不一致");
			return false;
		}
		
		return pass;
	}
	
	private boolean validateResetPasswordAuto(User user,Model model){
		
		return validateEmailEmpty(user,model);
	}
	
	private boolean validateEmailEmpty(User user,Model model){
		String email = user.getEmail();
		if(StringUtils.isNotEmpty(email)){
			user = userBusiness.findUserByEmail(email);
			if(user == null){
				model.addAttribute("msg","不存在此注册邮箱哦，您记错啦，重新想想，点击下面按钮吧！");
				return false;
			}
		}else{
			model.addAttribute("msg","没有邮箱填入哦，点击下面按钮吧！");
			return false;
		}
		return true;
	}
	
	
}
