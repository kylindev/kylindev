package org.jit8.framework.jit84j.web.interceptor.annotation;

import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.jit8.framework.jit84j.core.web.util.ApplicationContext;
import org.jit8.framework.jit84j.core.web.util.ThreadLocalShareInfo;
import org.jit8.framework.jit84j.web.constants.WebConstants;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

public class ShareInfoInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		Subject currentUser = SecurityUtils.getSubject();  
        if(null != currentUser){  
            Session session = currentUser.getSession();
            ThreadLocalShareInfo shareInfo = (ThreadLocalShareInfo)session.getAttribute(WebConstants._SHAREINFO);
            
            
            String authType = request.getAuthType();
            String characterEncoding = request.getCharacterEncoding();
            int contentLength = request.getContentLength();
            String contentType = request.getContentType();
            String contextPath = request.getContextPath();
            String localAddr = request.getLocalAddr();
            String localName = request.getLocalName();
            Locale locale = request.getLocale();
            int localPort = request.getLocalPort();
            String method = request.getMethod();
            String pathInfo = request.getPathInfo();
            String pathTranslated = request.getPathTranslated();
            String protocol = request.getProtocol();
            String queryString = request.getQueryString();
            String remoteAddr = request.getRemoteAddr();
            String remoteHost = request.getRemoteHost();
            int remotePort = request.getRemotePort();
            String remoteUser = request.getRemoteUser();
            String sessionId = request.getRequestedSessionId();
            String requestURI = request.getRequestURI();
            String requestURL = request.getRequestURL().toString();
            String scheme = request.getScheme();
            String serverName = request.getServerName();
            int serverPort = request.getServerPort();
            String servletPath = request.getServletPath();
            Map parameterMap = request.getParameterMap();
            request.getAttributeNames();
            
            String userAgent = request.getHeader("User-Agent");
            
            ApplicationContext.getContext().setShareInfo(shareInfo);
        }
		return super.preHandle(request, response, handler);
	}
	
	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		//response.getWriter().print("in login post");
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		//response.getWriter().print("in login after completaion");
		super.afterCompletion(request, response, handler, ex);
		ApplicationContext.getContext().setShareInfo(null);
	}
}
