package org.jit8.framework.jit84j.core.mail.business.impl;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.business.MailConfigBusiness;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.domain.MailConfigType;
import org.jit8.framework.jit84j.core.mail.service.MailConfigService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service("mailConfigBusiness")
public class MailConfigBusinessImpl implements MailConfigBusiness{

	@Resource
	private MailConfigService mailConfigService;
	
	public List<MailConfig> getMailConfigList(boolean disable,boolean deleted){
		
		return null;
	}
	
	public MailConfig findDefaultMailConfig(){
		MailConfig mailConfig = findByType(MailConfig.DEFAULT_MAIL_CONFIG);
		return mailConfig;
	}
	
	public MailConfig findByType(String type){
		return mailConfigService.findByType(type);
	}

	@Override
	public Page<MailConfig> findAll(Pageable pageable) {
		return mailConfigService.findAll(pageable);
	}

	@Override
	public MailConfig findById(long id) {
		return mailConfigService.findOne(id);
	}

	@Override
	public MailConfig persist(MailConfig mailConfig) {
		MailConfig defaultMailConfig = findDefaultMailConfig();
		if(null != defaultMailConfig && !defaultMailConfig.getId().equals(mailConfig.getId()) && MailConfig.DEFAULT_MAIL_CONFIG.equals(mailConfig.getType())){
			mailConfig.setType(MailConfigType.CUSTOM_MAIL_CONFIG);
		}
		
		Long id = mailConfig.getId();
		if(id != null && id>0){
			MailConfig exist = mailConfigService.findOne(id);
			if(null != exist){
				exist.setHost(mailConfig.getHost());
				exist.setUsername(mailConfig.getUsername());
				exist.setPassword(mailConfig.getPassword());
				exist.setPort(mailConfig.getPort());
				exist.setProtocol(mailConfig.getProtocol());
				exist.setUseClientFrom(mailConfig.isUseClientFrom());
				exist.setType(mailConfig.getType());
				mailConfig = exist;
			}
		}
		
		return mailConfigService.save(mailConfig);
	}

}
