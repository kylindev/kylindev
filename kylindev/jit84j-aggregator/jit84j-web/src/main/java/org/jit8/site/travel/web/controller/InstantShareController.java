package org.jit8.site.travel.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.dozer.Mapper;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.InstantShareBusiness;
import org.jit8.site.travel.biz.business.InstantShareImageBusiness;
import org.jit8.site.travel.biz.business.InstantShareImageCounterBusiness;
import org.jit8.site.travel.common.utils.TravelStaticDataUtil;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.jit8.site.web.controller.command.InstantShareCommand;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Controller
public class InstantShareController{

	private static final Logger logger = LoggerFactory.getLogger(InstantShareController.class);
	
	
	@Resource
	private InstantShareBusiness instantShareBusiness;
	
	@Resource
	private InstantShareImageBusiness instantShareImageBusiness;
	
	@Resource
	private InstantShareImageCounterBusiness instantShareImageCounterBusiness;
	
	@Resource
	private Mapper mapper;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING)
	public String instantShareEditUI(@ModelAttribute("instantShare") InstantShare instantShare,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT_UI_MAPPING + " #########");
		Long id = instantShare.getId();
		if(id != null && id > 0){
			instantShare = instantShareBusiness.getInstantShareById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShare", instantShare);
		model.addAttribute("batch", CalendarUtils.getNano1000Time());
		return "instantShareEditUI";
	}
	
	//test upload decorator
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_UI_MAPPING)
	public String instantShareEditUI2(@ModelAttribute("instantShare") InstantShare instantShare,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_UI_MAPPING + " #########");
		Long id = instantShare.getId();
		if(id != null && id > 0){
			instantShare = instantShareBusiness.getInstantShareById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShare", instantShare);
		long batch = instantShare.getBatch();
		if(batch<=0){
			batch = CalendarUtils.getNano1000Time();
			instantShare.setBatch(batch);
		}
		model.addAttribute("batch", batch);
		return "instantShareEditUI2";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING + "/{id}")
	public String instantShareViewDetailUI(@PathVariable("id")Long id, @ModelAttribute("instantShare") InstantShare instantShare,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING + " #########");
		Map<String, InstantShare> map = null;
		if(id != null && id > 0){
			map = instantShareBusiness.getThreeInstantSharesById(id);
		}
		model.addAttribute("navigatMenuTopList",StaticDataUtil.getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",StaticDataUtil.getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI));
		
		model.addAttribute("instantShare", map.get(InstantShareBusiness.INSTANTSHARE_CURRENT));
		model.addAttribute("previous", map.get(InstantShareBusiness.INSTANTSHARE_PREVIOUS));
		model.addAttribute("following", map.get(InstantShareBusiness.INSTANTSHARE_FOLLOWING));
		
		return "instantShareDetailUI";
	}
	
	//blank调用
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT3_UI_MAPPING)
	public String instantShareEditUI3(@ModelAttribute("instantShare") InstantShare instantShare,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_UI_MAPPING + " #########");
		Long id = instantShare.getId();
		if(id != null && id > 0){
			instantShare = instantShareBusiness.getInstantShareById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		
		if(null == instantShare.getImageList()){
			instantShare.setImageList(new ArrayList<InstantShareImage>());
		}
		
		long batch = instantShare.getBatch();
		if(batch<=0){
			batch = CalendarUtils.getNano1000Time();
			instantShare.setBatch(batch);
		}
		model.addAttribute("batch", batch);
		model.addAttribute("instantShare", instantShare);
		
		return "instantShareEditUI3";
	}
	
	//remove an image from instantShare
	//blank调用
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_MAPPING)
	public String removeImageFromInstantShare(@ModelAttribute("instantShareImage") InstantShareImage instantShareImage,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_MAPPING + " #########");
		Long id = instantShareImage.getId();
		InstantShare instantShare = null;
		if(id != null && id > 0){
			Long userId = Jit8ShareInfoUtils.getUserId();
			instantShareImage = instantShareImageBusiness.getInstantShareImageById(id);
			instantShareImageBusiness.removeByIdAndUserId(id, userId);
			if(null != instantShareImage){
				instantShare = instantShareImage.getInstantShare();
			}
		}
		
		if(instantShare != null){
			model.addAttribute("msg", "移除成功");
			return instantShareEditUI3(instantShare, model);
		}else{
			model.addAttribute("msg", "非法提交，请刷新后重试");
			return "instantShareEditUI3";
		}
		
	}
	
	@ResponseBody
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_MAPPING)
	public String instantShareEdit2(@ModelAttribute("instantShare") InstantShare instantShare,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_MAPPING + " #########");
		//Long id = instantShare.getId();
		instantShare = instantShareBusiness.persist(instantShare);
		
		
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShare", instantShare);
		
		InstantShareCommand instantShareCommand = mapper.map(instantShare, InstantShareCommand.class);
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("result", instantShareCommand);
		map.put("id", instantShareCommand);
		map.put("success", "保存成功");
		GsonBuilder gb = new GsonBuilder();
		gb.setDateFormat("yyyy-MM-dd HH:mm:ss");
		Gson gson = gb.excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(map);
		logger.info(json);
		return json;
		//return "instantShareEdit2UI";
	}
	
	
	
	//我的管理区>即时分享列表
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST_UI_MAPPING)
	public String instantShareList(@ModelAttribute("instantShare") InstantShare instantShare,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST_UI_MAPPING + " #########");
		Long id = instantShare.getId();
		if(id != null && id > 0){
			instantShare = instantShareBusiness.getInstantShareById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShare", instantShare);
		
		//设置左侧菜单
		model.addAttribute("currentUserManageAreaItem", TravelStaticDataUtil.getCurrentUserManageAreaItemByUserIdAndUrl(Jit8ShareInfoUtils.getUserId(), KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST_UI_MAPPING));
		
		Long userId = Jit8ShareInfoUtils.getUserId();
		
		Page<InstantShare> page = null;
		
		if(null != userId && userId>0){
			Pageable pageable2 = new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),Direction.DESC,"createDate");
			page = instantShareBusiness.findByUserId(userId, pageable2);
		}
		model.addAttribute("page", page);
		
		//获取当前月照片分布
		List<InstantShareImageCounter> instantShareImageCounterList = null;
		if(null != userId){
			instantShareImageCounterList = instantShareImageCounterBusiness.findByCurrentMonthForUser(userId);
		}
		model.addAttribute("instantShareImageCounterList", instantShareImageCounterList);
		
		return "instantShareListUI";
	}
	
	//我的管理区>即时分享 列表 blank
		@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING)
		public String instantShareList2(@ModelAttribute("instantShare") InstantShare instantShare,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
				logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING + " #########");
				Long id = instantShare.getId();
				if(id != null && id > 0){
					instantShareBusiness.deleteById(id);
				}
				model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
				model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
				model.addAttribute("instantShare", instantShare);
				
				Long userId = Jit8ShareInfoUtils.getUserId();
				Page<InstantShare> page = null;
				if(null != userId && userId>0){
					Pageable pageable2 = new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),Direction.DESC,"createDate");
					page = instantShareBusiness.findByUserId(userId, pageable2);
				}
				model.addAttribute("page", page);
				
				return "instantShareListUI2";
			}
	
	//我的管理区>即时分享 删除
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_DELERE_MAPPING)
	public String instantShareDelete(@ModelAttribute("instantShare") InstantShare instantShare,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
			logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST_UI_MAPPING + " #########");
			Long id = instantShare.getId();
			if(id != null && id > 0){
				instantShareBusiness.deleteById(id);
			}
			/*model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
			model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
			model.addAttribute("instantShare", instantShare);
			
			Long userId = Jit8ShareInfoUtils.getUserId();
			Page<InstantShare> page = null;
			if(null != userId && userId>0){
				Pageable pageable2 = new PageRequest(pageable.getPageNumber(),pageable.getPageSize(),Direction.DESC,"createDate");
				page = instantShareBusiness.findByUserId(userId, pageable2);
			}
			model.addAttribute("page", page);
			*/
			model.addAttribute("msg", "删除成功！");
			
			return instantShareList2(instantShare,model,pageable);
	}
	
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_PRAISE_MAPPING)
	public String instantSharePraise(@ModelAttribute("instantShare") InstantShare instantShare,Model model,@PageableDefaults(pageNumber = 0, value = 20) Pageable pageable) throws IOException {
		Long id = instantShare.getId();
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_PRAISE_MAPPING + "/"+id+ " #########");
		Long userId = Jit8ShareInfoUtils.getUserId();
		User user = Jit8ShareInfoUtils.getUser();
		if(user!= null && null != userId && id != null && id > 0){
			instantShare = instantShareBusiness.praise(id);
			model.addAttribute("msg", "已赞");
		}else{
			model.addAttribute("error","检测到您未登录，无法操作，请先登录系统");
		}
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("instantShare", instantShare);
		
		return "instantSharePraiseResultUI";
	}
}
