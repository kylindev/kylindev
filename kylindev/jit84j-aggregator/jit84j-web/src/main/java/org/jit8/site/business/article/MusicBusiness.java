package org.jit8.site.business.article;

import java.util.List;

import org.jit8.site.persist.domain.article.Music;



public interface MusicBusiness {

	public List<Music> getMusicList();
	public Music persist(Music music);
	
	public Music getMusicById(Long id);
	
	public Music deleteMusicById(Long id);
	
	
}
