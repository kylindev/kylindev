package org.jit8.site.travel.biz.service.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;


public interface UserManageAreaItemService extends GenericService<UserManageAreaItem, Long>{

	
	List<UserManageAreaItem> findByUserId(Long userId);
	
	public List<UserManageAreaItem> findAllDisplayByUserId(Long userId);
}
