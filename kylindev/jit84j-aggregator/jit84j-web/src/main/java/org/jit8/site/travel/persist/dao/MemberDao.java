package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.Member;

public interface MemberDao extends GenericDao<Member, Long> {

	public List<Member> getMemberList(boolean disable, boolean deleted) ;
	
	public Member findByCode(String code);
	
	public List<Member> findByIds(List<Long> idList);
}
