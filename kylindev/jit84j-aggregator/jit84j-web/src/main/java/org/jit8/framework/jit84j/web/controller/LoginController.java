package org.jit8.framework.jit84j.web.controller;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Locale;

import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.InactiveAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.IncorrectValidateCodeException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.PasswordEmptyException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.Jit8FormAuthenticationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.framework.jit84j.web.utils.VerifyCodeUtil;
import org.jit8.framework.jit84j.web.validator.LoginValidator;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class LoginController extends BaseCommonController{

private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Resource
	private UserBusiness userBusiness;
	
	@Resource
	private LoginValidator loginValidator;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefine;
	
	
	@InitBinder  
    public void initBinder(DataBinder binder) {  
       binder.setValidator(loginValidator);  
    }
	
	public LoginController(){
		logger.debug("######### init LoginController #########");
	}

	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING)
	public String loginUI(@ModelAttribute("user") User user, Model model,HttpServletRequest request) throws IOException {
		logger.debug("######### /loginUI #########");
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		
		model.addAttribute("ownRelationList", staticDataDefine.getData(StaticDataDefineConstant.OWN_RELATION_LIST, false));
		
		
		Subject subject = SecurityUtils.getSubject();
		if(null != subject){
			if(subject.isAuthenticated()){//如果检测到已经授权，直接跳到首页
				return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING;
			}
		}
		
		String errorClassName = (String)request.getAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute);
		if (IncorrectValidateCodeException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "验证码错误");
		} else if (PasswordEmptyException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (UnknownAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (DisabledAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户已被禁用");
		} else if (LockedAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户已被锁住");
		} else if (InactiveAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户尚未激活，请登录您的注册邮箱进行激活！");
		} else if (IncorrectCredentialsException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (errorClassName != null) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "未知错误:" + errorClassName);
		}
		
		return "loginUI";
	}
	
	@RequestMapping("/login")
	public String login(@ModelAttribute("user") @Valid User user,BindingResult result, Model model) throws IOException {
		logger.debug("######### /login #########");
		if(result.hasErrors()){
			return "loginUI";
		}
		return "redirect:/saveUser";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING)
	public String adminLoginUI(@ModelAttribute("user") User user, Model model,HttpServletRequest request) throws IOException {
		logger.debug("######### "+KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING+" #########");
		//model.addAttribute("ownRelationList", staticDataDefine.getData(StaticDataDefineConstant.OWN_RELATION_LIST, false));
		
		String errorClassName = (String)request.getAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute);
		
		if (IncorrectValidateCodeException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "验证码错误");
		} else if (PasswordEmptyException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (UnknownAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (DisabledAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户已被禁用");
		} else if (LockedAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户已被锁住");
		} else if (InactiveAccountException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "该用户尚未激活，请登录您的注册邮箱进行激活！");
		} else if (IncorrectCredentialsException.class.getName().equals(errorClassName)) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "用户名/密码错误");
		} else if (errorClassName != null) {
			model.addAttribute(Jit8FormAuthenticationFilter.failureKeyAttribute, "未知错误:" + errorClassName);
		}
		
		return "adminlogin";
	}
	
	
	
	 /** 
     * 获取验证码图片和文本(验证码文本会保存在HttpSession中) 
     */  
    @RequestMapping("/getVerifyCodeImage")  
    public void getVerifyCodeImage(HttpServletRequest request, HttpServletResponse response) throws IOException {  
        //设置页面不缓存  
        response.setHeader("Pragma", "no-cache");  
        response.setHeader("Cache-Control", "no-cache");  
        response.setDateHeader("Expires", 0);  
        String verifyCode = VerifyCodeUtil.generateTextCode(VerifyCodeUtil.TYPE_NUM_ONLY, 4, null);  
        //将验证码放到HttpSession里面  
        request.getSession().setAttribute("verifyCode", verifyCode);  
       logger.info("本次生成的验证码为[" + verifyCode + "],已存放到HttpSession中");  
        //设置输出的内容的类型为JPEG图像  
        response.setContentType("image/jpeg");  
        BufferedImage bufferedImage = VerifyCodeUtil.generateImageCode(verifyCode, 90, 30, 3, true, Color.CYAN, Color.BLACK, null);  
        //写给浏览器  
        ImageIO.write(bufferedImage, "JPEG", response.getOutputStream());  
    }  
	
	
    @RequestMapping(value = KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING ,method=RequestMethod.POST,produces={"application/json;charset=UTF-8"})
    public String login(User currUser,Model model,HttpSession session, HttpServletRequest request) throws IOException{
        String code = (String) session.getAttribute("validateCode");
        String submitCode = WebUtils.getCleanParam(request, "validateCode");
        if (StringUtils.isEmpty(submitCode) || !StringUtils.equals(code,submitCode.toLowerCase())) {
           // return "loginUI";
            return loginUI(currUser,  model, request);
        }
//        Subject user = SecurityUtils.getSubject();
//        UsernamePasswordToken token = new UsernamePasswordToken(currUser.getEmail(),currUser.getPassword());
//        token.setRememberMe(true);
        try {
        	
        	
        	Subject subject = SecurityUtils.getSubject();
    		if(null != subject){
    			if(subject.isAuthenticated()){//如果检测到已经授权，直接跳到首页
    				return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING;
    			}
    		}
        	
            //user.login(token);
            //return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING;
        }catch (AuthenticationException e) {
            //token.clear();
            return loginUI(currUser,  model, request);
        }
        return loginUI(currUser,  model, request);
    }
    
    
    @RequestMapping(value = KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_MAPPING,method=RequestMethod.POST,produces={"application/json;charset=UTF-8"})
    public String adminLogin(User currUser,HttpSession session, Model model,HttpServletRequest request, Locale locale) throws IOException{
        String code = (String) session.getAttribute("validateCode");
        String submitCode = WebUtils.getCleanParam(request, "validateCode");
        if (StringUtils.isEmpty(submitCode) || !StringUtils.equals(code,submitCode.toLowerCase())) {
            return "adminlogin";
        }
        Subject user = SecurityUtils.getSubject();
        if(user.isAuthenticated()){
        	return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING;
        }else{
        	return adminLoginUI( currUser,  model, request);
        }
        
        /*String username = currUser.getEmail();
        String username2 = EncodeUtil.encoderByMd5With16Hex(username);
        if(Jit8InitSystemConstant.getSuperUser().getEmail().equals(username2)){
        	username = username2;
        }
        String password = currUser.getPassword();
        String password2 = EncodeUtil.encoderByMd5With16Hex(password);
        UsernamePasswordToken token = new UsernamePasswordToken(username, password2);
        token.setRememberMe(true);
        try {
            user.login(token);
            return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING;
        }catch (AuthenticationException e) {
        	logger.error("login error: ",e);
        	e.printStackTrace();
            token.clear();
            //return "adminlogin";
            return adminLoginUI( currUser,  model, request);
        }*/
    }
    
    @RequestMapping(value = KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_MAPPING )
    public String logout(){
        Subject user = SecurityUtils.getSubject();
        user.logout();
        return "redirect:"+KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING;
    }

    /**
     * 生成验证码
     * @param request
     * @param response
     * @throws IOException
     */
    @RequestMapping(value = "/validateCode")
    public void validateCode(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setHeader("Cache-Control", "no-cache");
        String verifyCode = VerifyCodeUtil.generateTextCode(VerifyCodeUtil.TYPE_NUM_ONLY, 4, null);
        request.getSession().setAttribute("validateCode", verifyCode);
        response.setContentType("image/jpeg");
        BufferedImage bim = VerifyCodeUtil.generateImageCode(verifyCode, 90, 30, 3, true, Color.WHITE, Color.BLACK, null);
        ImageIO.write(bim, "JPEG", response.getOutputStream());
    }
	
	
	
	public static void  main(String[] args){
		for(int i=0;i<10;i++){
			try{
				System.out.println("dddee: " + i);
				if(i==5){
					throw new Exception("hh");
				}
				System.out.println("fff: " + i);
			}catch(Exception e){
				logger.debug("ddd",e);
				continue;
			}
			System.out.println(i);
		}
	}
	
}
