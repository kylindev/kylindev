package org.jit8.site.web.controller.command;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * 即时分享
 */
public class InstantShareCommand implements Serializable {


	private static final long serialVersionUID = -8592721867595575741L;

	@Expose
	private String iconPath;//图标
	
	private String imagePath;//封面图片
	
	private String background;//背景图
	
	@Expose
	private String description;//分享描述
	
	@Expose
	private String place;//发布所在地
	
	@Expose
	private String title;//标题
	
	private String nickName;//昵称
	
	private Long userId;//用户id
	
	private Date publishDate;//发布时间
	
	@Expose
	private String updateDateFormat;//更新时间
	
	@Expose
	private long batch;//批次，采用系统纳秒数
	
	private Integer praiseCount;//点赞数 
	private Integer careCount;//关注数 
	private Integer commentCount;//评论数 
	
	@Expose
	private String province;
	
	@Expose
	private String city;
	
	@Expose
	private String district;
	
	@Expose
	private List<InstantShareImageCommand> imageList;
	
	private long sequence;
	
	private boolean deleted;
	
	private boolean disable;
	
	private Long uniqueIdentifier;
	
	@Expose
	private Date updateDate;
	
	@Expose
	private Long id;
	
	private Date minPublishTime;//开始时间
	
	private Date maxPublishTime;//结束时间
	
	private long publishTimeMillis;//发布距今毫秒数
	
	private long publishSeconds;//发布距今秒数
	
	
	public String getIconPath() {
		return iconPath;
	}
	public void setIconPath(String iconPath) {
		this.iconPath = iconPath;
	}
	public String getNickName() {
		return nickName;
	}
	public void setNickName(String nickName) {
		this.nickName = nickName;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Date getPublishDate() {
		return publishDate;
	}
	public void setPublishDate(Date publishDate) {
		this.publishDate = publishDate;
	}
	public Integer getPraiseCount() {
		return praiseCount;
	}
	public void setPraiseCount(Integer praiseCount) {
		this.praiseCount = praiseCount;
	}
	public Integer getCareCount() {
		return careCount;
	}
	public void setCareCount(Integer careCount) {
		this.careCount = careCount;
	}
	public Integer getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
	public Date getMinPublishTime() {
		return minPublishTime;
	}
	public void setMinPublishTime(Date minPublishTime) {
		this.minPublishTime = minPublishTime;
	}
	public Date getMaxPublishTime() {
		return maxPublishTime;
	}
	public void setMaxPublishTime(Date maxPublishTime) {
		this.maxPublishTime = maxPublishTime;
	}
	public long getPublishSeconds() {
		return publishSeconds;
	}
	public void setPublishSeconds(long publishSeconds) {
		this.publishSeconds = publishSeconds;
	}
	public long getPublishTimeMillis() {
		return publishTimeMillis;
	}
	public void setPublishTimeMillis(long publishTimeMillis) {
		this.publishTimeMillis = publishTimeMillis;
	}
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public String getBackground() {
		return background;
	}
	public void setBackground(String background) {
		this.background = background;
	}
	public List<InstantShareImageCommand> getImageList() {
		return imageList;
	}
	public void setImageList(List<InstantShareImageCommand> imageList) {
		this.imageList = imageList;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public long getBatch() {
		return batch;
	}
	public void setBatch(long batch) {
		this.batch = batch;
	}
	public long getSequence() {
		return sequence;
	}
	public void setSequence(long sequence) {
		this.sequence = sequence;
	}
	public boolean isDeleted() {
		return deleted;
	}
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}
	public boolean isDisable() {
		return disable;
	}
	public void setDisable(boolean disable) {
		this.disable = disable;
	}
	public Long getUniqueIdentifier() {
		return uniqueIdentifier;
	}
	public void setUniqueIdentifier(Long uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public String getUpdateDateFormat() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		updateDateFormat = df.format(updateDate);
		return updateDateFormat;
	}
	public void setUpdateDateFormat(String updateDateFormat) {
		this.updateDateFormat = updateDateFormat;
	}
	
	
}
