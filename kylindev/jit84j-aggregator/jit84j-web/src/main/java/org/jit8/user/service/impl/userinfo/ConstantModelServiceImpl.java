package org.jit8.user.service.impl.userinfo;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.ConstantModelDao;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.ConstantModel;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.service.userinfo.ConstantModelService;
import org.jit8.user.service.userinfo.UserIdService;
import org.jit8.user.service.userinfo.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ConstantModelServiceImpl extends GenericServiceImpl<ConstantModel, Long> implements ConstantModelService {

	@Resource
	private ConstantModelDao constantModelDao;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private UserIdService uerIdService;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = constantModelDao;
	}

	@Override
	public Page<ConstantModel> getConstantModelList(Pageable pageable) {
		return constantModelDao.getConstantModelList(pageable);
	}
	


	@Override
	public Page<ConstantModel> getConstantModelList(ConstantModel constantModel,
			Pageable pageable) {
		return constantModelDao.getConstantModelList( constantModel,
				 pageable);
	}
}
