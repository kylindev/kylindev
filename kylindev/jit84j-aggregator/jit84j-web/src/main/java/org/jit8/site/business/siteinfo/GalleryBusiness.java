package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.Gallery;



public interface GalleryBusiness {

	public List<Gallery> getGalleryList();
	public Gallery persist(Gallery gallery);
	public Gallery merge(Gallery gallery);
	
	public Gallery getGalleryById(Long id);
	
	public Gallery deleteGalleryById(Long id);
	
	//public Gallery getFGallery
}
