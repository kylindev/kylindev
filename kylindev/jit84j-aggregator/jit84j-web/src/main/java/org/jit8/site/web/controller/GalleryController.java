package org.jit8.site.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;

import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller
public class GalleryController {

	private static final Logger logger = LoggerFactory.getLogger(GalleryController.class);
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws ServletException {
		binder.registerCustomEditor(byte[].class,
				new ByteArrayMultipartFileEditor());

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}
	
	@RequestMapping("/admin/gallery/modifyUI/{id}")
	public String galleryUI(@PathVariable Long id,Model model) throws IOException {
		logger.debug("######### /galleryUI #########");
		Gallery gallery = new Gallery();
		if(id>0){
			gallery = galleryBusiness.getGalleryById(id);
		}
		model.addAttribute("gallery", gallery);
		return "galleryModifyUI";
	}
	
	@RequestMapping("/admin/gallery/galleryAddUI")
	public String galleryAddUI(Model model) throws IOException {
		logger.debug("######### /admin/gallery/galleryAddUI #########");
		Gallery gallery = new Gallery();
		model.addAttribute("gallery", gallery);
		return "galleryAddUI";
	}
	
	@RequestMapping("/admin/gallery/galleryListUI")
	public String galleryListUI(Model model) throws IOException {
		logger.debug("######### /admin/gallery/galleryListUI #########");
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "galleryListUI";
	}
	
	@RequestMapping("/admin/gallery/galleryModify")
	public String galleryModify(@ModelAttribute("gallery") Gallery gallery,BindingResult result, Model model) throws IOException {
		logger.debug("######### /galleryUI #########");
		
		if(result.hasErrors()){
			return "galleryModifyUI";
		}
		
		galleryBusiness.merge(gallery);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:galleryListUI";
	}
	
	@RequestMapping("/admin/gallery/galleryAdd")
	public String galleryAdd(@ModelAttribute("gallery") Gallery gallery,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/gallery/galleryAdd #########");
		
		if(result.hasErrors()){
			return "galleryAddUI";
		}
		galleryBusiness.persist(gallery);
		return "redirect:galleryListUI";
	}
	
	
	@RequestMapping("/admin/gallery/delete/{id}")
	public String galleryDelete(@PathVariable Long id, Model model) throws IOException {
		logger.debug("######### /admin/gallery/delete/{id} #########");
		galleryBusiness.deleteGalleryById(id);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:/admin/gallery/galleryListUI";
	}
	
	
}
