package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UrlMappingService extends GenericService<UrlMapping, Long>{

	public UrlMapping findByUrlStaticMappingIdKey(String idKey);
	
	public UrlStaticMapping findModuleByIdKey(String idKey);
	
	
	public Page<UrlStaticMapping> findModule(Pageable pageable);
	
	public UrlMapping findByUrlStaticMappingUrlFullPath(String urlFullPath);
	
	public Page<UrlStaticMapping> findUrlStaticMapping(String moduleIdKey,Pageable pageable);
	
	public List<UrlStaticMapping> findByUrlStaticMappingIdKeyList(List<String> idKeyList);
	
	public Page<UrlStaticMapping> findUrlStaticMappingWithoutModule(
			Pageable pageable);
}
