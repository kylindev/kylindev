package org.jit8.site.travel.persist.dao.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.UserManageAreaItem;

public interface UserManageAreaItemDao extends GenericDao<UserManageAreaItem, Long> {

	public List<UserManageAreaItem> findByUserId(Long userId);
	
	public List<UserManageAreaItem> findAllDisplayByUserId(Long userId);
}
