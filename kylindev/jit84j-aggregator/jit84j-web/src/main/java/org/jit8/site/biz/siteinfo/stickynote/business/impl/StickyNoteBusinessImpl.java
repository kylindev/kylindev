package org.jit8.site.biz.siteinfo.stickynote.business.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.jit8.site.biz.siteinfo.stickynote.business.CategorySpecialBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategorySpecialBusiness;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteConfigService;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteService;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNote;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("stickyNoteBusiness")
public class StickyNoteBusinessImpl implements StickyNoteBusiness{

	@Resource
	private StickyNoteService stickyNoteService;
	
	@Resource
	private StickyNoteConfigService stickyNoteConfigService;
	
	@Resource
	private StickyNoteCategorySpecialBusiness stickyNoteCategorySpecialBusiness;
	
	@Resource
	private CategorySpecialBusiness categorySpecialBusiness;
	@Override
	public List<StickyNote> getStickyNoteList() {
		return stickyNoteService.getStickyNoteList(false, false);
	}
	
	@Transactional
	@Override
	public StickyNote persist(StickyNote stickyNote){
		Long id = stickyNote.getId();
		StickyNote exist = null;
		if(null != id){
			exist = stickyNoteService.findOne(id);
		}
		if(null != exist){
			exist.setAuthor(stickyNote.getAuthor());
			exist.setBrowseMaxCount(stickyNote.getBrowseMaxCount());
			exist.setCommentable(stickyNote.isCommentable());
			///exist.setExcellent(stickyNote.isExcellent());
			exist.setFriendable(stickyNote.isFriendable());
			exist.setImagePath(stickyNote.getImagePath());
			exist.setLocked(stickyNote.isLocked());
			exist.setImage2Path(stickyNote.getImage2Path());
			exist.setImage3Path(stickyNote.getImage3Path());
			exist.setMusicPath(stickyNote.getMusicPath());
			exist.setOriginal(stickyNote.isOriginal());
			exist.setContent(stickyNote.getContent());
			exist.setTitle(stickyNote.getTitle());
			exist.setSummary(stickyNote.getSummary());
			exist.setTranspond(stickyNote.isTranspond());
			exist.setTranspondUrl(stickyNote.getTranspondUrl());
			exist.setCategorySpecialIdList(stickyNote.getCategorySpecialIdList());//瞬时对象
		}else{
			exist = stickyNote;
		}
		
		exist = asignCategorySpecial(exist);//把帖子装进分类中
		return stickyNoteService.save(exist);
	}
	
	@Transactional
	public StickyNote asignCategorySpecial(StickyNote stickyNote){
		if(null != stickyNote){
				List<Long> categoryIdList = stickyNote.getCategorySpecialIdList();
				if(null != categoryIdList && categoryIdList.size()>0){
					List<Long> filterIdList = new ArrayList<Long>();
					for(Long id : categoryIdList){
						if(null != id && id>0){
							filterIdList.add(id);
						}
					}
					List<CategorySpecial> categorySpecialList = categorySpecialBusiness.getByCategorySpecialIdList(filterIdList);
					Set<StickyNoteCategorySpecial> set = new HashSet<StickyNoteCategorySpecial>();
					Set<StickyNoteCategorySpecial> existSet =stickyNote.getStickyNoteCategorySpecialSet();
					Set<StickyNoteCategorySpecial> currentExistSet = new HashSet<StickyNoteCategorySpecial>();
					Map<Long,StickyNoteCategorySpecial> map = new HashMap<Long,StickyNoteCategorySpecial>();
					if(existSet!=null){
						for(StickyNoteCategorySpecial exist :existSet){
							map.put(exist.getCategorySpecial().getId(), exist);
						}
					}
					for(CategorySpecial categorySpecial : categorySpecialList){
						StickyNoteCategorySpecial stickyNoteCategorySpecial = null;
						stickyNoteCategorySpecial = map.get(categorySpecial.getId());
						if(null == stickyNoteCategorySpecial){
							stickyNoteCategorySpecial = new StickyNoteCategorySpecial();
							stickyNoteCategorySpecial.setCategorySpecial(categorySpecial);
							stickyNoteCategorySpecial.setStickyNote(stickyNote);
						}else{
							currentExistSet.add(stickyNoteCategorySpecial);
						}
						set.add(stickyNoteCategorySpecial);
					}
					if(currentExistSet.size()>0){
						existSet.removeAll(currentExistSet);
					}
					if(existSet.size()>0){
						stickyNoteCategorySpecialBusiness.delete(existSet);//把未添加的remove掉
					}
					stickyNote.setStickyNoteCategorySpecialSet(set);
				}
		}
		return stickyNote;
	}

	@Override
	public StickyNote getStickyNoteById(Long id) {
		return stickyNoteService.findOne(id);
	}

	@Override
	public StickyNote deleteStickyNoteById(Long id) {
		StickyNote old = stickyNoteService.findOne(id);
		old.setDeleted(true);
		return stickyNoteService.save(old);
	}

	@Override
	public Page<StickyNote> findAll(StickyNote stickyNote, Pageable pageable) {
		return stickyNoteService.findAll(stickyNote, pageable);
	}

}
