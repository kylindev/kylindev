package org.jit8.site.web.controller.siteinfo.stickynote;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SpecialAreaController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(SpecialAreaController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private SpecialAreaBusiness specialAreaBusiness;
	
	
	
//	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING)
	@RequestMapping("/admin/specialArea")
	public String stickyNoteEditUI(@ModelAttribute("specialArea") SpecialArea specialArea,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING + " #########");
		//Long id = siteInfo.getId();
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI));
		model.addAttribute("specialArea", specialArea);
		return "specialAreaEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_INDEX_UI_MAPPING)
	public String specialAreaIndexUI(@ModelAttribute("specialArea") SpecialArea specialArea,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_INDEX_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("specialArea", specialArea);
		Page<SpecialArea> page= specialAreaBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "specialAreaIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_MAPPING)
	public String specialAreaEdit(@ModelAttribute("specialArea") SpecialArea specialArea,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("specialArea", specialArea);
		Page<SpecialArea> page= specialAreaBusiness.findAll(pageable);
		if(validateSpecialArea(specialArea,model)){
			specialArea = specialAreaBusiness.persist(specialArea);
		}
		model.addAttribute("page", page);
		model.addAttribute("specialArea", specialArea);
		
		return specialAreaEditUI( specialArea,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_UI_MAPPING)
	public String specialAreaEditUI(@ModelAttribute("specialArea") SpecialArea specialArea,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Long id = specialArea.getId();
		if(null != id && id>0){
			specialArea = specialAreaBusiness.getSpecialAreaById(id);
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("specialArea", specialArea);
		Page<SpecialArea> page= specialAreaBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "specialAreaEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SPECIAL_AREA_REMOVE_MAPPING)
	public String specialAreaRemove(@ModelAttribute("specialArea") SpecialArea specialArea, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = specialArea.getId();
		if(null != id && id>0){
			specialAreaBusiness.removeById(id);
			specialArea.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return specialAreaEditUI( specialArea,model,pageable);
	}
	
	private boolean validateSpecialArea(SpecialArea specialArea, Model model){
		boolean pass = true;
		if(null != specialArea && specialArea.getId()==null){
			String code = specialArea.getCode();
			SpecialArea exist = specialAreaBusiness.findByCode(code);
			if(null != exist){
				model.addAttribute("msg", "系统中已经存在此code【"+code+"】,请换一个");
				pass=false;
			}
		}
		
		return pass;
	}
	
}
