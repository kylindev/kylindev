package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileSystemGalleryTypeDao;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.site.service.siteinfo.FileSystemGalleryTypeService;
import org.springframework.stereotype.Service;

@Service
public class FileSystemGalleryTypeServiceImpl extends GenericServiceImpl<FileSystemGalleryType, Long> implements FileSystemGalleryTypeService {

	@Resource
	private FileSystemGalleryTypeDao fileSystemGalleryTypeDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileSystemGalleryTypeDao;
	}


	@Override
	public List<FileSystemGalleryType> getFileSystemGalleryTypeList(boolean disable, boolean deleted) {
		return fileSystemGalleryTypeDao.getFileSystemGalleryTypeList(disable, deleted);
	}

	
}
