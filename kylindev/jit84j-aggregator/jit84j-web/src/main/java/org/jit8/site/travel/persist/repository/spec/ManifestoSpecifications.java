package org.jit8.site.travel.persist.repository.spec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.site.travel.persist.domain.Manifesto_;
import org.springframework.data.jpa.domain.Specification;



public class ManifestoSpecifications {

	public static Specification<Manifesto> queryByCondition(final Manifesto manifesto) {
		return new Specification<Manifesto>() {
			public Predicate toPredicate(Root<Manifesto> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				predicateList.add(builder.equal(root.get(Manifesto_.deleted ), false));
				
				if(null != manifesto){
					
					String decription = StringUtils.trim(manifesto.getDecription());
					String nickName = StringUtils.trim(manifesto.getNickName());
					
					
					if(StringUtils.isNotEmpty(decription)){
						predicateList.add(builder.like(root.get(Manifesto_.decription), Specifications.PERCENT + decription + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(nickName)){
						predicateList.add(builder.like(root.get(Manifesto_.nickName), Specifications.PERCENT + nickName + Specifications.PERCENT));
					}
					
					Date minPushlishTime = manifesto.getMinPublishTime();
					
					Date maxPushlishTime = manifesto.getMaxPublishTime();
					
					if(null != minPushlishTime){
						predicateList.add(builder.greaterThan(root.get(Manifesto_.publishDate), minPushlishTime));
					}
					
					if(null != maxPushlishTime){
						predicateList.add(builder.lessThan(root.get(Manifesto_.publishDate), maxPushlishTime));
					}
					
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
