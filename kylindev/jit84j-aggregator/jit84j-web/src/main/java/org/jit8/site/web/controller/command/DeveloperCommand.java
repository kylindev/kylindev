package org.jit8.site.web.controller.command;

import java.io.Serializable;

import javax.persistence.Transient;

public class DeveloperCommand implements Serializable {

	
	private static final long serialVersionUID = -6136606143950218008L;
	
	@Transient
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
