package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.GalleryDao;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.service.siteinfo.GalleryService;
import org.springframework.stereotype.Service;

@Service
public class GalleryServiceImpl extends GenericServiceImpl<Gallery, Long> implements GalleryService {

	@Resource
	private GalleryDao galleryDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = galleryDao;
	}


	@Override
	public List<Gallery> getGalleryList(boolean disable, boolean deleted) {
		return galleryDao.getGalleryList(disable, deleted);
	}
	
}
