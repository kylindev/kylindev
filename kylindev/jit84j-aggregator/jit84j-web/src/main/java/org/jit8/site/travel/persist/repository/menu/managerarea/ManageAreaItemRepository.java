package org.jit8.site.travel.persist.repository.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ManageAreaItemRepository extends GenericJpaRepository<ManageAreaItem, Long>{

	@Query("from ManageAreaItem o where o.deleted=false order by o.sequence asc")
	public List<ManageAreaItem> findAllAvailable();
	
	@Query("from ManageAreaItem o where o.deleted=false and o.disable=false and o.hideable=false and o.display=true order by o.sequence asc")
	public List<ManageAreaItem> findAllDisplay();
	
	@Query("from ManageAreaItem o where o.uniqueIdentifier in :idList ")
	public List<ManageAreaItem> findByIdList(@Param("idList")List<Long> idList);
}
