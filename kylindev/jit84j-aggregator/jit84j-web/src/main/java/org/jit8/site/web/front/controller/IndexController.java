package org.jit8.site.web.front.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreManager;
import org.jit8.site.business.menu.NavigatMenuBusiness;
import org.jit8.site.business.siteinfo.GalleryBusiness;
import org.jit8.site.persist.domain.siteinfo.Gallery;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.travel.biz.business.ManifestoBusiness;
import org.jit8.site.travel.persist.domain.Manifesto;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.support.ByteArrayMultipartFileEditor;

@Controller
public class IndexController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);
	
	@Resource
	private NavigatMenuBusiness navigatMenuBusiness;
	
	@Resource
	private GalleryBusiness galleryBusiness;
	
	@Resource
	private StaticDataDefineCoreManager staticDataDefineCoreManager;
	
	@Resource
	private ManifestoBusiness manifestoBusiness;
	
	@InitBinder
	protected void initBinder(WebDataBinder binder) throws ServletException {
		binder.registerCustomEditor(byte[].class,
				new ByteArrayMultipartFileEditor());

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(
				dateFormat, false));
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING)
	public String index(Model model) throws IOException {
	
		return indexUI(model);
	}
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING)
	public String indexUI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI));
		
		Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		Pageable pageable2 = new PageRequest(0, 20,sort);
		Pageable commentPageable = new PageRequest(0, 5,sort);
		Page<Manifesto> page = manifestoBusiness.getTodayList(new Manifesto(),pageable2,commentPageable);
		model.addAttribute("page", page);
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "index";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_TODAY_LEVEL2_UI_MAPPING)
	public String todayLevel2UI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI));
		
		Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		Pageable pageable2 = new PageRequest(0, 20,sort);
		
		Pageable commentPageable = new PageRequest(0, 5,sort);
		Page<Manifesto> page = manifestoBusiness.getTodayList(new Manifesto(),pageable2,commentPageable);

		model.addAttribute("page", page);
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "todayLevel2";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_TOMORROW_INDEX_UI_MAPPING)
	public String tomorrowIndexUI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_TOMORROW_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "tomorrowIndex";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_TOMORROW_LEVEL2_UI_MAPPING)
	public String tomorrowLevel2UI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_TOMORROW_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "tomorrowLevel2";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_YESTERDAY_INDEX_UI_MAPPING)
	public String yesterdayIndexUI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_YESTERDAY_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "yesterdayIndex";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_DAY_BEFORE_YESTERDAY_INDEX_UI_MAPPING)
	public String dayBeforeYesterdayIndexUI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_DAY_BEFORE_YESTERDAY_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "dayBeforeYesterdayIndex";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_MY_MANAGE_INDEX_UI_MAPPING)
	public String myManageIndexUI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_DAY_BEFORE_YESTERDAY_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "myManageIndex";
	}
	
	//个人首页 元素周期表
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_PERSONALIZE_INDEX_UI_MAPPING)
	public String personalizeIndexUI(Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_PERSONALIZE_INDEX_UI_MAPPING + " #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_DAY_BEFORE_YESTERDAY_INDEX_UI));
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "personalize/css3d_periodictable";
	}
	
	//转到详细页面
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_DETAIL_2_UI_MAPPING)
	public String detail2UI(Model model) throws IOException {
		logger.debug("######### /index #########");
		
		model.addAttribute("navigatMenuTopList",getNavigatMenuTopList());
		model.addAttribute("currentNavigatMenu",getCurrentNavigatMenuByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI));
		
		Sort sort = new Sort(new Order(Direction.DESC,"publishDate"));
		Pageable pageable2 = new PageRequest(0, 20,sort);
		
		Pageable commentPageable = new PageRequest(0, 5,sort);
		Page<Manifesto> page = manifestoBusiness.getTodayList(new Manifesto(),pageable2,commentPageable);

		model.addAttribute("page", page);
		
		List<Gallery> galleryList = galleryBusiness.getGalleryList();
		model.addAttribute("galleryList", galleryList);
		//specialAreaList StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN
		List<SpecialArea> specialAreaList = (List<SpecialArea>)staticDataDefineCoreManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN);
		model.addAttribute("specialAreaList",specialAreaList);
		
		return "instantShareDetailUI2";
	}
}
