package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.article.MusicBusiness;
import org.jit8.site.persist.domain.article.Music;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MusicController {

	private static final Logger logger = LoggerFactory.getLogger(MusicController.class);
	
	@Resource
	private MusicBusiness musicBusiness;
	
	@RequestMapping("/admin/musicUI/{id}")
	public String musicUI(@PathVariable Long id,Model model) throws IOException {
		logger.debug("######### /musicUI #########");
		Music music = new Music();
		if(id>0){
			music = musicBusiness.getMusicById(id);
		}
		model.addAttribute("music", music);
		return "musicUI";
	}
	
	@RequestMapping("/admin/music/musicListUI")
	public String musicListUI(Model model) throws IOException {
		logger.debug("######### /musicUI #########");
		List<Music> musicList = musicBusiness.getMusicList();
		model.addAttribute("musicList", musicList);
		
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "musicListUI";
	}
	
	@RequestMapping("/admin/music/modify")
	public String musicModify(@ModelAttribute("music") Music music,BindingResult result, Model model) throws IOException {
		logger.debug("######### /musicUI #########");
		
		if(result.hasErrors()){
			return "musicUI";
		}
		
		musicBusiness.persist(music);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:musicListUI";
	}
	
	@RequestMapping("/admin/music/add")
	public String musicAdd(@ModelAttribute("music") Music music,BindingResult result, Model model) throws IOException {
		logger.debug("######### /musicUI #########");
		
		if(result.hasErrors()){
			return "musicUI";
		}
		
		musicBusiness.persist(music);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:musicListUI";
	}
	
	
	@RequestMapping("/admin/music/delete/{id}")
	public String musicDelete(@PathVariable Long id, Model model) throws IOException {
		logger.debug("######### /admin/music/delete/{id} #########");
		musicBusiness.deleteMusicById(id);
		//model.addAttribute("ownRelationList", staticDataDefine.getOwnRelationList());
		return "redirect:/admin/music/musicListUI";
	}
	
	
}
