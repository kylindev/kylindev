package org.jit8.site.travel.biz.business;

import java.util.List;
import java.util.Map;

import org.jit8.site.travel.persist.domain.InstantShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface InstantShareBusiness {
	
	String INSTANTSHARE_CURRENT = "current";
	String INSTANTSHARE_PREVIOUS = "previous";
	String INSTANTSHARE_FOLLOWING = "following";

	public List<InstantShare> getInstantShareList();
	
	public InstantShare persist(InstantShare journey);
	
	public InstantShare getInstantShareById(Long id);
	
	public InstantShare deleteInstantShareById(Long id);
	
	public Page<InstantShare> findAll(Pageable pageable);
	
	public Page<InstantShare> findByUserId(Long userId,Pageable pageable);
	
	public InstantShare findByCode(String code);
	
	public void removeById(Long id);
	
	public void deleteById(Long id);
	
	public List<InstantShare> findByIds(List<Long> idList);
	
	public InstantShare praise(Long id);
	
	public Map<String, InstantShare> getThreeInstantSharesById(Long id);
	
}
