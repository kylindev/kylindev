package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface InstantShareService extends GenericService<InstantShare, Long>{

	public List<InstantShare> getInstantShareList(boolean disable, boolean deleted);
	
	public InstantShare findByCode(String code);
	
	public List<InstantShare> findByIds(List<Long> idList);
	
	public Page<InstantShare> findByUserId(Long userId, Pageable pageable);
	
	public Long findPreviousOneByCurrentId(Long id);
	
	public Long findFollowingOneByCurrentId(Long id);
	
	public InstantShare findSimpleOneById(Long id);
	
	public InstantShare findOneWithPageable(Long id, Pageable commentPageable, Pageable imagePageable);
}
