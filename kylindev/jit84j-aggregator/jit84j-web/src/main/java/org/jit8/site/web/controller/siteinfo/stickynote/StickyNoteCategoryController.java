package org.jit8.site.web.controller.siteinfo.stickynote;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StickyNoteCategoryController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(StickyNoteCategoryController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private StickyNoteCategoryBusiness stickyNoteCategoryBusiness;
	
	
	
//	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING)
	//@RequestMapping("/admin/stickyNoteCategory")
	public String stickyNoteEditUI(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING + " #########");
		//Long id = siteInfo.getId();
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI));
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		return "stickyNoteCategoryEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_MAPPING)
	public String stickyNoteCategoryIndexUI(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		Page<StickyNoteCategory> page= stickyNoteCategoryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "stickyNoteCategoryIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_MAPPING)
	public String stickyNoteCategoryListUI(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_LIST_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		Page<StickyNoteCategory> page= stickyNoteCategoryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "stickyNoteCategoryListUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_MAPPING)
	public String stickyNoteCategoryEdit(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		Page<StickyNoteCategory> page= stickyNoteCategoryBusiness.findAll(pageable);
		if(validateStickyNoteCategory(stickyNoteCategory,model)){
			stickyNoteCategory = stickyNoteCategoryBusiness.persist(stickyNoteCategory);
		}
		model.addAttribute("page", page);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		
		return stickyNoteCategoryEditUI( stickyNoteCategory,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_MAPPING)
	public String stickyNoteCategoryEditUI(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Long id = stickyNoteCategory.getId();
		if(null != id && id>0){
			stickyNoteCategory = stickyNoteCategoryBusiness.getStickyNoteCategoryById(id);
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("stickyNoteCategory", stickyNoteCategory);
		Page<StickyNoteCategory> page= stickyNoteCategoryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "stickyNoteCategoryEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_MAPPING)
	public String stickyNoteCategoryRemove(@ModelAttribute("stickyNoteCategory") StickyNoteCategory stickyNoteCategory, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = stickyNoteCategory.getId();
		if(null != id && id>0){
			stickyNoteCategoryBusiness.removeById(id);
			stickyNoteCategory.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return stickyNoteCategoryEditUI( stickyNoteCategory,model,pageable);
	}
	
	private boolean validateStickyNoteCategory(StickyNoteCategory stickyNoteCategory, Model model){
		boolean pass = true;
		if(null != stickyNoteCategory && stickyNoteCategory.getId()==null){
			String code = stickyNoteCategory.getCode();
			StickyNoteCategory exist = stickyNoteCategoryBusiness.findByCode(code);
			if(null != exist){
				model.addAttribute("msg", "系统中已经存在此code【"+code+"】,请换一个");
				pass=false;
			}
		}
		
		return pass;
	}
	
}
