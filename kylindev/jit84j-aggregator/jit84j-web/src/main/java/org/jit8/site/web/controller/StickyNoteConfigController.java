package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteConfigBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class StickyNoteConfigController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(StickyNoteConfigController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private StickyNoteConfigBusiness stickyNoteConfigBusiness;
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING)
	public String siteInfoStickyNoteEditUI(@ModelAttribute("siteInfo") SiteInfo siteInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING + " #########");
		Long id = siteInfo.getId();
		if(id != null && id > 0){
			siteInfo = siteInfoBusiness.getSiteInfoById(id);
		}
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI));
		model.addAttribute("siteInfo", siteInfo);
		return "siteInfoStickyNoteEditUI";
	}
	
	@ResponseBody
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING)
	public Map<String,String>  siteInfoStickyNoteEdit(@ModelAttribute("siteInfo") SiteInfo siteInfo,Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING + " #########");
		Long id = siteInfo.getId();
		if(id != null && id > 0){
			StickyNoteConfig stickyNoteConfig = siteInfo.getStickyNoteConfig();
			stickyNoteConfigBusiness.merge(stickyNoteConfig);
		}
		Map<String,String> result = new HashMap<String,String>();
		result.put("msg", "success");
		return result;
	}
	
	
	
	
}
