package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.Journey;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface JourneyRepository extends GenericJpaRepository<Journey, Long>{

	
	@Query("from Journey o where o.disable=:disable and o.deleted=:deleted order by o.sequence asc")
	List<Journey> getJourneyList(@Param("disable")boolean disable,@Param("deleted")boolean deleted);

	
	@Query("from Journey o where o.title=:title")
	public Journey findByTitle(@Param("title")String title);
	
	@Query("from Journey o where o.uniqueIdentifier in :idList")
	public List<Journey> findByIds(@Param("idList")List<Long> idList);
}
