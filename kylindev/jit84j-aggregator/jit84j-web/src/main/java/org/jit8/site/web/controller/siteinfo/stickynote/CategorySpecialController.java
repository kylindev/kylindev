package org.jit8.site.web.controller.siteinfo.stickynote;

import java.io.IOException;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.biz.siteinfo.stickynote.business.CategorySpecialBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.SpecialAreaBusiness;
import org.jit8.site.biz.siteinfo.stickynote.business.StickyNoteCategoryBusiness;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteCategory;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CategorySpecialController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(CategorySpecialController.class);
	
	@Resource
	private SiteInfoBusiness siteInfoBusiness;
	
	@Resource
	private CategorySpecialBusiness categorySpecialBusiness;
	
	@Resource
	private StickyNoteCategoryBusiness stickyNoteCategoryBusiness;
	
	@Resource
	private SpecialAreaBusiness specialAreaBusiness;
	
	
	
//	@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING)
	@RequestMapping("/admin/categorySpecial")
	public String stickyNoteEditUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING + " #########");
		//Long id = siteInfo.getId();
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI));
		model.addAttribute("categorySpecial", categorySpecial);
		return "categorySpecialEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_MAPPING)
	public String categorySpecialIndexUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		Page<CategorySpecial> page= categorySpecialBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "categorySpecialIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_UI_MAPPING)
	public String categorySpecialListUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_INDEX_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_INDEX_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		Pageable categorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<CategorySpecial> page= categorySpecialBusiness.findAll(categorySpecialPageable);
		model.addAttribute("page", page);
		return "categorySpecialListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING)
	public String categoryAsignSpecialUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		
		Pageable categoryPageAll = new PageRequest(0, 200, Direction.ASC, "sequence");
		Page<StickyNoteCategory> categoryPage= stickyNoteCategoryBusiness.findAll(categoryPageAll);
		model.addAttribute("categoryPage", categoryPage);
		
		Page<SpecialArea> specialAreaPage = specialAreaBusiness.findAll(categoryPageAll);
		model.addAttribute("specialAreaPage", specialAreaPage);
		
		Pageable categorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<CategorySpecial> categorySpecialPage = categorySpecialBusiness.findAll(categorySpecial,categorySpecialPageable);
		model.addAttribute("categorySpecialPage", categorySpecialPage);
		model.addAttribute("categorySpecial", categorySpecial);
		
		return "categorySpecialBindUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING)
	public String categoryAsignSpecial(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		categorySpecialBusiness.asignCategory2SpecialArea(categorySpecial);
		return categoryAsignSpecialUI(categorySpecial,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING)
	public String saveCategorySpecialList(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		categorySpecialBusiness.saveCategorySpecialList(categorySpecial);
		return categoryAsignSpecialUI(categorySpecial,model,pageable);
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING)
	public String categorySpecialAsignListUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		
		Pageable categorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<CategorySpecial> categorySpecialPage = categorySpecialBusiness.findAll(categorySpecial,categorySpecialPageable);
		model.addAttribute("categorySpecialPage", categorySpecialPage);
		
		return "categorySpecialAsignListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING)
	public String categoryRemoveFromSpecial(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		categorySpecialBusiness.removeCategoryFromSpecialArea(categorySpecial);
		categorySpecial.setCategoryIds(null);
		categorySpecial.setSpecialAreaIds(null);
		categorySpecial.setId(null);
		return categoryAsignSpecialUI(categorySpecial,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_MAPPING)
	public String categorySpecialRemoveInAsignUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = categorySpecial.getId();
		if(null != id && id>0){
			categorySpecialBusiness.removeById(id);
			categorySpecial.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return categoryAsignSpecialUI(categorySpecial,model,pageable);
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING)
	public String categorySpecialEdit(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,@ModelAttribute("categorySpecialSearch") CategorySpecial categorySpecialSearch,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		
		Pageable categorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<CategorySpecial> page = categorySpecialBusiness.findAll(categorySpecialSearch,categorySpecialPageable);
		//Page<CategorySpecial> page= categorySpecialBusiness.findAll(pageable);
		if(validateCategorySpecial(categorySpecial,model)){
			categorySpecial = categorySpecialBusiness.persist(categorySpecial);
		}
		model.addAttribute("page", page);
		model.addAttribute("categorySpecial", categorySpecial);
		
		return categorySpecialEditUI( categorySpecial,categorySpecialSearch,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING)
	public String categorySpecialEditUI(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,@ModelAttribute("categorySpecialSearch") CategorySpecial categorySpecialSearch,Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable){
		
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING +" #########");
		String code= KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		Long id = categorySpecial.getId();
		if(null != id && id>0){
			categorySpecial = categorySpecialBusiness.getCategorySpecialById(id);
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("categorySpecial", categorySpecial);
		Pageable categorySpecialPageable = new PageRequest(pageable.getPageNumber(), pageable.getPageSize(), Direction.ASC, "specialArea.name");
		Page<CategorySpecial> page = categorySpecialBusiness.findAll(categorySpecialSearch,categorySpecialPageable);
		model.addAttribute("page", page);
		return "categorySpecialEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_MAPPING)
	public String categorySpecialRemove(@ModelAttribute("categorySpecial") CategorySpecial categorySpecial,@ModelAttribute("categorySpecialSearch") CategorySpecial categorySpecialSearch, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = categorySpecial.getId();
		if(null != id && id>0){
			categorySpecialBusiness.removeById(id);
			categorySpecial.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return categorySpecialEditUI( categorySpecial,categorySpecialSearch,model,pageable);
	}
	
	private boolean validateCategorySpecial(CategorySpecial categorySpecial, Model model){
		boolean pass = true;
//		if(null != categorySpecial && categorySpecial.getId()==null){
//			String code = categorySpecial.getName();
//			CategorySpecial exist = categorySpecialBusiness.findByName(code);
//			if(null != exist){
//				model.addAttribute("msg", "系统中已经存在此code【"+code+"】,请换一个");
//				pass=false;
//			}
//		}
		
		return pass;
	}
	
}
