package org.jit8.site.travel.biz.service.impl.menu.managerarea;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.menu.managerarea.ItemButtonService;
import org.jit8.site.travel.persist.dao.menu.managerarea.ItemButtonDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.springframework.stereotype.Service;

@Service
public class ItemButtonServiceImpl extends GenericServiceImpl<ItemButton, Long> implements ItemButtonService {

	@Resource
	private ItemButtonDao itemButtonDao;
	
	
	@PostConstruct
	public void initRepository(){
		this.genericDao = itemButtonDao;
	}

	@Override
	public List<ItemButton> findAllAvailable() {
		return itemButtonDao.findAllAvailable();
	}


	@Override
	public List<ItemButton> findAllDisplay() {
		
		return itemButtonDao.findAllDisplay();
	}


	@Override
	public List<ItemButton> findByIdList(List<Long> idList) {
		return itemButtonDao.findByIdList(idList);
	}

	
}
