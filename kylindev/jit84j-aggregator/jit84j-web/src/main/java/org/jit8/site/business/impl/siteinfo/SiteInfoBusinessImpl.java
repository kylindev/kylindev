package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.cache.memached.MemachedInit;
import org.jit8.framework.jit84j.core.cache.memached.MemachedUpdate;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineCoreConstant;
import org.jit8.site.biz.siteinfo.stickynote.service.StickyNoteConfigService;
import org.jit8.site.business.siteinfo.SiteInfoBusiness;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;
import org.jit8.site.persist.siteinfo.stickynote.domain.StickyNoteConfig;
import org.jit8.site.service.siteinfo.SiteInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("siteInfoBusiness")
public class SiteInfoBusinessImpl implements SiteInfoBusiness{

	@Resource
	private SiteInfoService siteInfoService;
	
	@Resource
	private StickyNoteConfigService stickyNoteConfigService;

	@Override
	public List<SiteInfo> getSiteInfoList() {
		return siteInfoService.getSiteInfoList(false, false);
	}
	
	@Transactional
	@Override
	public SiteInfo persist(SiteInfo siteInfo){
		
		SiteInfo old = siteInfoService.findOne(siteInfo.getId());
		StickyNoteConfig stickyOld = old.getStickyNoteConfig();
		StickyNoteConfig sticky = siteInfo.getStickyNoteConfig();
		
		BeanUtils.copyProperties(siteInfo, old);
		
		stickyOld.setStickyNoteMusicPath(sticky.getStickyNoteMusicPath());
		//BeanUtils.copyProperties(sticky, stickyOld);
		//stickyNoteConfigService.save(stickyOld);
		
		old.setStickyNoteConfig(stickyOld);
		return siteInfoService.save(old);
	}

	@Override
	@MemachedInit(value=StaticDataDefineCoreConstant.MEMACHED_TEST,
	  name=StaticDataDefineCoreConstant.MEMACHED_TEST_NAME,
	  autoRefresh=true
	)
	public SiteInfo getSiteInfoById(Long id) {
		return siteInfoService.findOne(id);
	}

	@Override
	public SiteInfo deleteSiteInfoById(Long id) {
		SiteInfo old = siteInfoService.findOne(id);
		old.setDeleted(true);
		return siteInfoService.save(old);
	}

	@Override
	public SiteInfo getSiteInfoByType(String type) {
		return siteInfoService.getSiteInfoByType(type);
	}

	@Override
	@Transactional
	@MemachedUpdate(value=StaticDataDefineCoreConstant.MEMACHED_TEST,
			  name=StaticDataDefineCoreConstant.MEMACHED_TEST_NAME,
	          autoRefresh=true,
	          targetClazz=SiteInfoBusiness.class,
	          targetMethod="getSiteInfoById",
	          targetMethodParams={Long.class},
	          targetMethodParamValueProperty={"id"}
	)
	public SiteInfo merge(SiteInfo siteInfo) {
		if(null != siteInfo){
			Long id = siteInfo.getId();
			if(null != id && id > 0 ) {
				SiteInfo old = siteInfoService.findOne(id);
				old.setDescription(siteInfo.getDescription());
				old.setDisable(siteInfo.isDisable());
				old.setFooter(siteInfo.getFooter());
				old.setIconPath(siteInfo.getIconPath());
				old.setIpcInfo(siteInfo.getIpcInfo());
				old.setLogoPath(siteInfo.getLogoPath());
				old.setPhone(siteInfo.getPhone());
				old.setQq(siteInfo.getQq());
				old.setSiteAddress(siteInfo.getSiteAddress());
				old.setSiteAdminName(siteInfo.getSiteAdminName());
				old.setSiteEmail(siteInfo.getSiteEmail());
				old.setSiteInstallUrl(siteInfo.getSiteInstallUrl());
				old.setSiteKey(siteInfo.getSiteKey());
				old.setSiteName(siteInfo.getSiteName());
				old.setSiteSecondTitle(siteInfo.getSiteSecondTitle());
				old.setSiteTitle(siteInfo.getSiteTitle());
				old.setSiteUrl(siteInfo.getSiteUrl());
				siteInfoService.save(old);
			}
		}
		return siteInfo;
	}
	
	public SiteInfo getMainSiteInfo() {
		return getSiteInfoByType(SiteInfo.MAIN_SITE);
	}

}
