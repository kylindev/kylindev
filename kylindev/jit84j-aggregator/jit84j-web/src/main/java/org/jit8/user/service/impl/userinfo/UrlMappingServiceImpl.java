package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.UrlMappingDao;
import org.jit8.user.persist.domain.userinfo.UrlMapping;
import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;
import org.jit8.user.service.userinfo.UrlMappingService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UrlMappingServiceImpl extends GenericServiceImpl<UrlMapping, Long> implements UrlMappingService {

	@Resource
	private UrlMappingDao urlMappingDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = urlMappingDao;
	}


	@Override
	public UrlMapping findByUrlStaticMappingIdKey(String idKey) {
		return urlMappingDao.findByUrlStaticMappingIdKey(idKey);
	}


	@Override
	public UrlStaticMapping findModuleByIdKey(String idKey) {
		return  urlMappingDao.findModuleByIdKey( idKey);
	}


	@Override
	public UrlMapping findByUrlStaticMappingUrlFullPath(String urlFullPath) {
		return urlMappingDao.findByUrlStaticMappingUrlFullPath( urlFullPath);
	}


	@Override
	public Page<UrlStaticMapping> findModule(Pageable pageable) {
		
		return urlMappingDao.findModule(pageable);
	}


	@Override
	public Page<UrlStaticMapping> findUrlStaticMapping(String moduleIdKey,
			Pageable pageable) {
		return urlMappingDao.findUrlStaticMapping(moduleIdKey, pageable);
	}
	
	public Page<UrlStaticMapping> findUrlStaticMappingWithoutModule(
			Pageable pageable){
		return urlMappingDao.findUrlStaticMappingWithoutModule(pageable);
	}


	@Override
	public List<UrlStaticMapping> findByUrlStaticMappingIdKeyList(
			List<String> idKeyList) {
		
		return urlMappingDao.findByUrlStaticMappingIdKeyList(idKeyList);
	}
	
	
}
