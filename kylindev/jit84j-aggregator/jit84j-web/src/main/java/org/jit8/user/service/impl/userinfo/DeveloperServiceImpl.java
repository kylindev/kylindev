package org.jit8.user.service.impl.userinfo;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.DeveloperDao;
import org.jit8.user.persist.domain.userinfo.Developer;
import org.jit8.user.persist.domain.userinfo.User;
import org.jit8.user.persist.domain.userinfo.UserId;
import org.jit8.user.service.userinfo.DeveloperService;
import org.jit8.user.service.userinfo.UserIdService;
import org.jit8.user.service.userinfo.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class DeveloperServiceImpl extends GenericServiceImpl<Developer, Long> implements DeveloperService {

	@Resource
	private DeveloperDao developerDao;
	
	
	@Resource
	private UserService userService;
	
	@Resource
	private UserIdService uerIdService;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = developerDao;
	}

	@Override
	public Page<Developer> getDeveloperList(Pageable pageable) {
		return developerDao.getDeveloperList(pageable);
	}
	
	@Override
	public <S extends Developer> S save(S entity) {
		User user = userService.findOne(entity.getCurrentUser().getId());
		UserId userId = user.getUserId();
		if(null == userId){
			userId = new UserId();
			userId = uerIdService.save(userId);
			userId.setUserId(userId.getUniqueIdentifier() + UserId.ID_STARTING);
			userId.setGenerateTime(new Date());
			user.setUserId(userId);
		}
		entity.setUser(user);
		entity.setUserId(userId);
		entity.setSeperator("_");//developer seperator
		return super.save(entity);
	}

	@Override
	public Developer findByUserId(Long userId) {
		
		return developerDao.findByUserId(userId);
	}

	@Override
	public Page<Developer> getDeveloperList(Developer developer,
			Pageable pageable) {
		return developerDao.getDeveloperList( developer,
				 pageable);
	}
}
