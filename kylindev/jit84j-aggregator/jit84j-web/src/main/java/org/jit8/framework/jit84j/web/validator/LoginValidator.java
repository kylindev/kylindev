package org.jit8.framework.jit84j.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class LoginValidator implements Validator {
	protected static  String EMAIL_RULE="^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w{2,3}){1,3})$";
	protected static  String USERNAME_RULE="[0-9a-zA-Z]{6,20}";

	@Override
	public boolean supports(Class<?> clazz) {
		return User.class.equals(clazz); 
	}

	@Override
	public void validate(Object target, Errors errors) {
		//ValidationUtils.rejectIfEmpty(errors, "username", null, "Username is empty.");  
	       User user = (User) target;  
	       String username = user.getUsername();
	       String email = user.getEmail();
    	   username = org.apache.commons.lang.StringUtils.trimToEmpty(username);
    	   user.setUsername(username);
    	   email = org.apache.commons.lang.StringUtils.trimToEmpty(email);
	       user.setEmail(email);
	       validateEmailAndUsername(errors, username, email);
	       if (org.apache.commons.lang.StringUtils.isEmpty(user.getPassword())){
	    	   errors.rejectValue("password", null, "密码不能为空");  
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isNotEmpty(username)){
	    	   Pattern p = Pattern.compile(USERNAME_RULE);
	    	   Matcher m = p.matcher(username);
	    	   if(!m.find()){
	    		   errors.rejectValue("username", null, "用户名必须为数字0-9、字母a-z或A-Z的组合,必须在6-20位之间");  
	    	   }
	       }
	       
	       if(org.apache.commons.lang.StringUtils.isNotEmpty(email)){
	    	   Pattern p = Pattern.compile(EMAIL_RULE);
	    	   Matcher m = p.matcher(email);
	    	   if(!m.find()){
	    		   errors.rejectValue("email", null, "不合法的Email格式，请按例输入：kylinboy521@163.com");  
	    	   }
	       }
	       
	}

	protected void validateEmailAndUsername(Errors errors, String username,	String email) {
		if(org.apache.commons.lang.StringUtils.isEmpty(username) && org.apache.commons.lang.StringUtils.isEmpty(email)){
	    	   errors.rejectValue("username", null, "用户名与email必须填一个");  
	    }
	}

}
