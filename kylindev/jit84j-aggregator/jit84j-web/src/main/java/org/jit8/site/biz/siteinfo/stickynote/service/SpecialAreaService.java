package org.jit8.site.biz.siteinfo.stickynote.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.siteinfo.stickynote.domain.SpecialArea;


public interface SpecialAreaService extends GenericService<SpecialArea, Long>{

	public List<SpecialArea> getSpecialAreaList(boolean disable, boolean deleted);
	
	public SpecialArea findByCode(String code);
	
	public List<SpecialArea> findByIds(List<Long> idList);
}
