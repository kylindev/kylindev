package org.jit8.site.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.web.util.Jit8ShareInfoUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.business.siteinfo.FileSystemGalleryBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileSystemGallery;
import org.jit8.user.business.userinfo.UserBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FileSystemGalleryController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(FileSystemGalleryController.class);
	
	@Resource
	private FileSystemGalleryBusiness fileSystemGalleryBusiness;
	
	@Resource
	private UserBusiness userBusiness;
	
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING)
	public String fileSystemGalleryIndexUI(@ModelAttribute("fileSystemGallery") FileSystemGallery fileSystemGallery, Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING + " #########");
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI);
		model.addAttribute("currentNavigation",navigation);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI);
		model.addAttribute("editNavigation",editNavigation);
		Page<FileSystemGallery> page = fileSystemGalleryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		
		return "fileSystemGalleryIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING)
	public String fileSystemGalleryUI(@ModelAttribute("fileSystemGallery") FileSystemGallery fileSystemGallery, Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryUI #########");
		//fileSystemGalleryBusiness.persist(fileSystemGallery);
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI);
		model.addAttribute("currentNavigation",navigation);
		
		Long id = fileSystemGallery.getId();
		if(null!=id && id>0){
			fileSystemGallery = fileSystemGalleryBusiness.getFileSystemGalleryById(id);
		}
		
		Page<FileSystemGallery> page = fileSystemGalleryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		model.addAttribute("fileSystemGallery", fileSystemGallery);
		return "fileSystemGalleryAddUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING)
	public String fileSystemGalleryListUI(@ModelAttribute("fileSystemGallery")FileSystemGallery fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI);
		model.addAttribute("editNavigation",editNavigation);
		model.addAttribute("currentNavigation",navigation);
		Page<FileSystemGallery> page = fileSystemGalleryBusiness.findAll(pageable);
		model.addAttribute("page", page);
		model.addAttribute("navigationTopList",navigationTopList);
		return "fileSystemGalleryListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_MAPPING)
	public String fileSystemGalleryAdd(@ModelAttribute("fileSystemGallery")FileSystemGallery fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		fileSystemGallery = fileSystemGalleryBusiness.persist(fileSystemGallery);
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return fileSystemGalleryUI(fileSystemGallery,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING)
	public String fileSystemGalleryRemove(@ModelAttribute("fileSystemGallery")FileSystemGallery fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Long id = fileSystemGallery.getId();
		if(null != id && id>0){
		 fileSystemGalleryBusiness.removeById(id);
		 fileSystemGallery.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return fileSystemGalleryListUI(fileSystemGallery,model,pageable);
	}
	
	
	
}
