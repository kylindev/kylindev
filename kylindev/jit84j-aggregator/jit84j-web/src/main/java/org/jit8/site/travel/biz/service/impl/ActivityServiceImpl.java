package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.ActivityService;
import org.jit8.site.travel.persist.dao.ActivityDao;
import org.jit8.site.travel.persist.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ActivityServiceImpl extends GenericServiceImpl<Activity, Long> implements ActivityService {

	@Resource
	private ActivityDao activityDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = activityDao;
	}


	@Override
	public List<Activity> getActivityList(boolean disable, boolean deleted) {
		return activityDao.getActivityList(disable, deleted);
	}


	@Override
	public Activity findByCode(String code) {
		return activityDao.findByCode( code);
	}


	@Override
	public List<Activity> findByIds(List<Long> idList) {
		return activityDao.findByIds( idList);
	}
	
	@Override
	public Page<Activity> findByUserId(Long userId, Pageable pageable) {
		if(null == userId){
			return null;
		}
		return activityDao.findByUserId( userId, pageable);
	}
	
}
