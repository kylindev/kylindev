package org.jit8.site.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.business.siteinfo.FileMimeBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FileMimeController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(FileMimeController.class);
	
	@Resource
	private FileMimeBusiness fileMimeBusiness;
	
	
	
	@RequestMapping("/admin/fileMime/fileMimeUI")
	public String fileMimeUI(@ModelAttribute("fileMime") FileMime fileMime,BindingResult result, Model model) throws IOException {
		logger.debug("######### /fileMimeUI #########");
		
		return "fileMimeUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_MAPPING)
	public String fileMimeListUI(@ModelAttribute("fileMime")FileMime fileMime,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeListUI #########");
		Page<FileMime> page = fileMimeBusiness.findAll(fileMime, pageable);
		
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI);
		model.addAttribute("editNavigation",navigation);
		model.addAttribute("currentNavigation",navigation);
		
		model.addAttribute("page", page);
		model.addAttribute("navigationTopList",navigationTopList);
		return "fileMimeListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_MAPPING)
	public String fileMimeIndexUI(@ModelAttribute("fileMime")FileMime fileMime,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeListUI #########");
		Page<FileMime> page = fileMimeBusiness.findAll(fileMime, pageable);
		
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI);
		model.addAttribute("editNavigation",navigation);
		model.addAttribute("currentNavigation",navigation);
		
		model.addAttribute("page", page);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("fileMimeSearch",fileMime);
		return "fileMimeIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_MAPPING)
	public String fileMimeAddUI(@ModelAttribute("fileMime")FileMime fileMime,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeAddUI #########");
		
		Page<FileMime> page = fileMimeBusiness.findAll(fileMime, pageable);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI);
		
		Long id = fileMime.getId();
		if(id!= null && id>0){
			fileMime = fileMimeBusiness.findById(id);
		}
		
		model.addAttribute("editNavigation",navigation);
		model.addAttribute("currentNavigation",navigation);
		
		model.addAttribute("page", page);
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("fileMime",fileMime);
		model.addAttribute("fileMimeSearch",new FileMime());
		return "fileMimeAddUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_MAPPING)
	public String fileMimeAdd(@ModelAttribute("fileMime")FileMime fileMime,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeAdd #########");
		fileMime = fileMimeBusiness.persist(fileMime);
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		model.addAttribute("fileMime",fileMime);
		return fileMimeAddUI(new FileMime(),model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_MAPPING)
	public String fileMimeRemove(@ModelAttribute("fileMime")FileMime fileMime,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeRemove #########");
		Long id = fileMime.getId();
		if(id!= null && id>0){
			fileMimeBusiness.removeById(fileMime.getId());
			fileMime.setId(null);
		}
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return fileMimeAddUI(new FileMime(),model,pageable);
	}
	
}
