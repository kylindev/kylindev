package org.jit8.site.travel.biz.service;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.travel.persist.domain.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ActivityService extends GenericService<Activity, Long>{

	public List<Activity> getActivityList(boolean disable, boolean deleted);
	
	public Activity findByCode(String code);
	
	public List<Activity> findByIds(List<Long> idList);
	
	public Page<Activity> findByUserId(Long userId, Pageable pageable);
	
}
