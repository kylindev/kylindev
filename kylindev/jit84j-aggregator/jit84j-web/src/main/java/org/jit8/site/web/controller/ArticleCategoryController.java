package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.article.ArticleCategoryBusiness;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.persist.domain.article.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ArticleCategoryController {

	private static final Logger logger = LoggerFactory.getLogger(ArticleCategoryController.class);
	
	@Resource
	private ArticleCategoryBusiness articleCategoryBusiness; 
	
	@Resource
	private CategoryBusiness categoryBusiness; 
	
	@RequestMapping(value="/admin/article/asignCategory",method = RequestMethod.POST)
	public String articleAsignCategory(@ModelAttribute("articleCategory") ArticleCategory articleCategory, Model model) throws IOException {
		logger.debug("######### /admin/article/asignCategory #########");
		
		List<ArticleCategory> asignCategoryForArticleUI = articleCategoryBusiness.asignCategory(articleCategory);
		model.addAttribute("article",asignCategoryForArticleUI.get(0).getArticle());
		
		List<Category> categoryList = categoryBusiness.getCategoryListByParentNull();
		setCurrentArticleAsignedForCategoryList(asignCategoryForArticleUI,categoryList);
		
		model.addAttribute("categoryList",categoryList);
		//prepareList(model);
		return "asignCategoryForArticleUI";
	}




	public static void setCurrentArticleAsignedForCategoryList(
			List<ArticleCategory> asignCategoryForArticleUI,
			List<Category> categoryList) {
		for(Category c : categoryList){
			setCurrentArticleAsignedForCategoryList(asignCategoryForArticleUI,c.getChildrenCategory());
			for(ArticleCategory ac : asignCategoryForArticleUI){
				if(c.getUniqueIdentifier().equals(ac.getCategory().getUniqueIdentifier())){
					c.setCurrentArticleAsigned(true);
					break;
				}
			}
		}
	}
	
	
	
	
	private void prepareList(Model model){
		List<Category> categoryList = new ArrayList<Category>();
		Category category1 = new Category();
		category1.setId(1l);
		category1.setName("计算机编程");
		List<Category> children1 = new ArrayList<Category>();
		category1.setChildrenCategory(children1);
		
		Category category2 = new Category();
		category2.setId(2l);
		category2.setName("java");
		category2.setParent(category1);
		children1.add(category2);
		
		Category category3 = new Category();
		category3.setId(3l);
		category3.setName("C++");
		category3.setParent(category1);
		children1.add(category3);
		
		
		categoryList.add(category1);
		
		model.addAttribute("categoryList",categoryList);
		model.addAttribute("category",category3);
		
		
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tagList.add(tag1);
		tag1.setId(1l);
		tag1.setName("江湖");
		
		Tag tag2 = new Tag();
		tagList.add(tag2);
		tag2.setId(2l);
		tag2.setName("英雄");
		model.addAttribute("tagList",tagList);
	}
	
	
}
