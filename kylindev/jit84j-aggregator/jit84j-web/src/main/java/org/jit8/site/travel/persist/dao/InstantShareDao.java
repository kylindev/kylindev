package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;

public interface InstantShareDao extends GenericDao<InstantShare, Long> {

	public List<InstantShare> getInstantShareList(boolean disable, boolean deleted) ;
	
	public InstantShare findByCode(String code);
	
	public List<InstantShare> findByIds(List<Long> idList);
	
	public Page<InstantShare> findByUserId(Long userId, Pageable pageable);
	
	public Long findPreviousOneByCurrentId(Long id);
	
	public Long findFollowingOneByCurrentId(Long id);
	
	public InstantShare findSimpleOneById(Long id);
}
