package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.business.siteinfo.FileSystemGalleryTypeBusiness;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class FileSystemGalleryTypeController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(FileSystemGalleryTypeController.class);
	
	@Resource
	private FileSystemGalleryTypeBusiness fileSystemGalleryTypeBusiness;
	
	
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING)
	public String fileSystemGalleryIndexUI(@ModelAttribute("fileSystemGallery") FileSystemGalleryType fileSystemGalleryType, Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING + " #########");
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI);
		model.addAttribute("currentNavigation",navigation);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI);
		model.addAttribute("editNavigation",editNavigation);
		Page<FileSystemGalleryType> page = fileSystemGalleryTypeBusiness.findAll(pageable);
		model.addAttribute("page", page);
		return "fileSystemGalleryIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_MAPPING)
	public String fileSystemGalleryUI(@ModelAttribute("fileSystemGallery") FileSystemGalleryType fileSystemGalleryType, Model model,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryUI #########");
		//fileSystemGalleryBusiness.persist(fileSystemGallery);
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI);
		model.addAttribute("currentNavigation",navigation);
		
		model.addAttribute("editNavigation",navigation);

		
		Long id = fileSystemGalleryType.getId();
		if(null!=id && id>0){
			fileSystemGalleryType = fileSystemGalleryTypeBusiness.findById(id);
		}
		
		Page<FileSystemGalleryType> page = fileSystemGalleryTypeBusiness.findAll(pageable);
		model.addAttribute("page", page);
		model.addAttribute("fileSystemGalleryType", fileSystemGalleryType);
		return "fileSystemGalleryTypeAddUI";
	}
	
	//@RequestMapping(KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING)
	public String fileSystemGalleryListUI(@ModelAttribute("fileSystemGallery")FileSystemGalleryType fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI);
		model.addAttribute("editNavigation",editNavigation);
		model.addAttribute("currentNavigation",navigation);
		Page<FileSystemGalleryType> page = fileSystemGalleryTypeBusiness.findAll(pageable);
		model.addAttribute("page", page);
		model.addAttribute("navigationTopList",navigationTopList);
		return "fileSystemGalleryListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_MAPPING)
	public String fileSystemGalleryAdd(@ModelAttribute("fileSystemGallery")FileSystemGalleryType fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		fileSystemGallery = fileSystemGalleryTypeBusiness.persist(fileSystemGallery);
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return fileSystemGalleryUI(fileSystemGallery,model,pageable);
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_MAPPING)
	public String fileSystemGalleryRemove(@ModelAttribute("fileSystemGalleryType")FileSystemGalleryType fileSystemGallery,Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileSystemGalleryListUI #########");
		fileSystemGalleryTypeBusiness.removeById(fileSystemGallery.getId());
		fileSystemGallery.setId(null);
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		return fileSystemGalleryUI(fileSystemGallery,model,pageable);
	}
	
	
	
}
