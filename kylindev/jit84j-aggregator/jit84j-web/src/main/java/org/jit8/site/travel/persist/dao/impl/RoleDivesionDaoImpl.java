package org.jit8.site.travel.persist.dao.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDaoImpl;
import org.jit8.site.travel.persist.dao.RoleDivesionDao;
import org.jit8.site.travel.persist.domain.RoleDivesion;
import org.jit8.site.travel.persist.repository.RoleDivesionRepository;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDivesionDaoImpl extends GenericDaoImpl<RoleDivesion, Long> implements RoleDivesionDao {

	@Resource
	private RoleDivesionRepository roleDivesionRepository;
	
	@PostConstruct
	public void initRepository(){
		this.genericJpaRepository = roleDivesionRepository;
	}

	@Override
	public List<RoleDivesion> getRoleDivesionList(boolean disable, boolean deleted) {
		return roleDivesionRepository.getRoleDivesionList(disable, deleted);
	}
	
	@Override
	public RoleDivesion findByCode(String code) {
		return roleDivesionRepository.findByCode( code);
	}

	@Override
	public List<RoleDivesion> findByIds(List<Long> idList) {
		return roleDivesionRepository.findByIds(idList);
	}
}
