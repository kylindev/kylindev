package org.jit8.site.service.article;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.article.Music;


public interface MusicService extends GenericService<Music, Long>{

	public List<Music> getMusicList(boolean disable, boolean deleted);
}
