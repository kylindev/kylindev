package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.page.PageCommand;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class PermissionController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);
	

	@Resource
	private PermissionBusiness permissionBusiness;
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING + "/{navigationCode}")
	public String permissionListUI(@PathVariable("navigationCode") String code,@PageableDefaults(pageNumber = 1, value = 15) Pageable pageable,@ModelAttribute Permission permission,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### /admin/userinfo/permission/permissionListUI #########");
		//RequestContextUtils.
		Map<String,?> flash = redirectAttributes.getFlashAttributes();
		
		Page<Permission> page =permissionBusiness.getPermissionList(permission,pageable);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("page", page);
		return "permissionListUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING + "/{code}")
	public String permissionListModify(@PathVariable("code") String code,@ModelAttribute("permission") Permission permission, @PageableDefaults(pageNumber = 1, value = 15) Pageable pageable,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### /admin/userinfo/permission/permissionListUI #########");
		permissionBusiness.savePermissionList(permission);
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		model.addAttribute("navigationTopList",navigationTopList);
		Page<Permission> page =permissionBusiness.getPermissionList(pageable);
		model.addAttribute("page", page);
		Permission permission2 = new Permission();
		permission2.setCode("user_%");
		permission2.setName("分配角色");
		redirectAttributes.addFlashAttribute("per",permission2);
		redirectAttributes.addAttribute("param coming");
		return "permissionListUI";
		//return "redirect:" + KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING + "/" + code;
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING+"/{code}")
	public String addPermissionUI(@PathVariable("code") String code,@ModelAttribute("permission") Permission permission, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/permission/addPermissionUI/ #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("ownRelationList", ownRelationList);
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		model.addAttribute("permission",new Permission());
		
		return "addPermissionUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_MAPPING+"/{code}")
	public String addPermission(@PathVariable("code") String code,@ModelAttribute("permission") Permission permission, Model model) throws IOException {
		logger.debug("######### /admin/userinfo/permission/addPermission/ #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		
		try {
			permissionBusiness.persist(permission);
		} catch (BusinessException e) {
			e.printStackTrace();
		}
		
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING + "/" + code;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_MAPPING + "/{id}")
	public String modifyPermissionUI(@PathVariable("id") Long id,@ModelAttribute("permission") Permission permission, Model model) throws IOException {
		logger.debug("######### " + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_MAPPING + " #########");
		//List<OwnRelation> ownRelationList = ownRelationBusiness.getOwnRelationList();
		model.addAttribute("navigatMenuTopList",navigatMenuTopList);
		model.addAttribute("navigationTopList",navigationTopList);
		
		String code = KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI;
		if(StringUtils.isNotEmpty(code)){
			model.addAttribute("currentNavigation",getCurrentNavigationByCode(code));
		}
		permission = permissionBusiness.fineById(id);
		model.addAttribute("permission",permission);
		
		return "modifyPermissionUI";
	}
	
	
}
