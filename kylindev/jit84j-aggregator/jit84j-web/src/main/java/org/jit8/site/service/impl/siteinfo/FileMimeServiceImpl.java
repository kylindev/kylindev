package org.jit8.site.service.impl.siteinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.siteinfo.FileMimeDao;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.site.service.siteinfo.FileMimeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class FileMimeServiceImpl extends GenericServiceImpl<FileMime, Long> implements FileMimeService {

	@Resource
	private FileMimeDao fileMimeDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = fileMimeDao;
	}


	@Override
	public List<FileMime> getFileMimeList(boolean disable, boolean deleted) {
		return fileMimeDao.getFileMimeList(disable, deleted);
	}


	@Override
	public FileMime findByMime(String mime) {
		return fileMimeDao.findByMime(mime);
	}


	@Override
	public List<FileMime> findByIdList(List<Long> idList) {
		return fileMimeDao.findByIdList(idList);
	}


	@Override
	public List<FileMime> getFileMimeListByAllowable(boolean allowable) {
		return fileMimeDao.getFileMimeListByAllowable(allowable);
	}


	@Override
	public Page<FileMime> findAll(FileMime fileMime, Pageable pageable) {
		return fileMimeDao.findAll( fileMime,  pageable);
	}
	
}
