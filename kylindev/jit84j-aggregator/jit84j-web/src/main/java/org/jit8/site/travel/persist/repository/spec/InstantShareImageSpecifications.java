package org.jit8.site.travel.persist.repository.spec;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.persistent.reponsitory.spec.Specifications;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.jit8.site.travel.persist.domain.InstantShareImage_;
import org.springframework.data.jpa.domain.Specification;



public class InstantShareImageSpecifications {

	public static Specification<InstantShareImage> queryByCondition(final InstantShareImage instantShareImage) {
		return new Specification<InstantShareImage>() {
			public Predicate toPredicate(Root<InstantShareImage> root,
					CriteriaQuery<?> query, CriteriaBuilder builder) {
				
				List<Predicate> predicateList = new ArrayList<Predicate>();
				
				predicateList.add(builder.equal(root.get(InstantShareImage_.deleted ), false));
				
				if(null != instantShareImage){
					
					String description = StringUtils.trim(instantShareImage.getDescription());
					String title = StringUtils.trim(instantShareImage.getTitle());
					
					
					if(StringUtils.isNotEmpty(description)){
						predicateList.add(builder.like(root.get(InstantShareImage_.description), Specifications.PERCENT + description + Specifications.PERCENT));
					}
					if(StringUtils.isNotEmpty(title)){
						predicateList.add(builder.like(root.get(InstantShareImage_.title), Specifications.PERCENT + title + Specifications.PERCENT));
					}
					
					Date minPublishDate = instantShareImage.getMinPublishDate();
					
					Date maxPublishDate = instantShareImage.getMaxPublishDate();
					
					if(null != minPublishDate){
						predicateList.add(builder.greaterThan(root.get(InstantShareImage_.publishDate), minPublishDate));
					}
					
					if(null != maxPublishDate){
						predicateList.add(builder.lessThan(root.get(InstantShareImage_.publishDate), maxPublishDate));
					}
					
					Long userId = instantShareImage.getUserId();
					if(null != userId){
						predicateList.add(builder.equal(root.get(InstantShareImage_.userId), userId));
					}
					
				}
				
				return builder.and(predicateList.toArray(new Predicate[predicateList.size()]));
			}
		};
	}
}
