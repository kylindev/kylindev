package org.jit8.user.business.userinfo;

import java.util.List;

import org.jit8.user.persist.domain.userinfo.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface RoleBusiness {

	List<Role> getRoleListByRoleLevel(int roleLevel);
	
	public int getMaxRoleLevel(List<Role> roleLsit);
	
	List<Role> getRoleListByRoleIdList(List<Long> roleIdList);
	
	Page<Role> getRoleList(Role role,Pageable pageable);
	
	public Page<Role> getRoleList(Pageable pageable);
	
	public List<Role> saveRoleList(Role role);
	
	public Role persist(Role role);
	
	/**
	 * 为角色分配权限
	 * @param role
	 */
	public void asignPermissionForRole(Role role) ;
	
	
	/**
	 * 为角色移除权限
	 * @param role
	 */
	public void removePermissionForRole(Role role) ;
	
	/**
	 * 为角色移除所有权限
	 * @param role
	 */
	public void removeAllPermissionForRole(Role role) ;
}
