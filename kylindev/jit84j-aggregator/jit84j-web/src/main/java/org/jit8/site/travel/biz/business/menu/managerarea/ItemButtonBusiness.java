package org.jit8.site.travel.biz.business.menu.managerarea;

import java.util.List;

import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;




public interface ItemButtonBusiness {
	
	ItemButton persist(ItemButton itemButton);
	
	
	List<ItemButton> findAll();
	
	Page<ItemButton> findAll(Pageable pageable);
	
	List<ItemButton> findAllAvailable();
	
	List<ItemButton> findAllDisplay();
	
	ItemButton findById(Long id);
	
	void removeById(Long id);
	
	List<ItemButton> persistList(List<ItemButton> itemButtonList);
}
