package org.jit8.site.travel.persist.dao;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ManifestoCommentDao extends GenericDao<ManifestoComment, Long> {

	public Page<ManifestoComment> findByManifestoId(Long manifestoId,
			Pageable pageable);
}
