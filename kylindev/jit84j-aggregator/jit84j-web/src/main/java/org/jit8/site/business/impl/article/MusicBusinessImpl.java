package org.jit8.site.business.impl.article;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.article.MusicBusiness;
import org.jit8.site.persist.domain.article.Music;
import org.jit8.site.service.article.MusicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("musicBusiness")
public class MusicBusinessImpl implements MusicBusiness{

	private static final Logger logger = LoggerFactory.getLogger(MusicBusinessImpl.class);
	
	@Resource
	private MusicService musicService;
	

	@Override
	public List<Music> getMusicList() {
		return musicService.getMusicList(false, false);
	}
	
	@Transactional
	@Override
	public Music persist(Music music){
		
		Music old = musicService.findOne(music.getId());
		return musicService.save(old);
	}

	@Override
	public Music getMusicById(Long id) {
		return musicService.findOne(id);
	}

	@Override
	public Music deleteMusicById(Long id) {
		Music old = musicService.findOne(id);
		old.setDeleted(true);
		return musicService.save(old);
	}

}
