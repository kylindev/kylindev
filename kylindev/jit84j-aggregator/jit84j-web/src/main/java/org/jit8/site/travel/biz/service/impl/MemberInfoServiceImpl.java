package org.jit8.site.travel.biz.service.impl;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.MemberInfoService;
import org.jit8.site.travel.persist.dao.MemberInfoDao;
import org.jit8.site.travel.persist.domain.MemberInfo;
import org.springframework.stereotype.Service;

@Service
public class MemberInfoServiceImpl extends GenericServiceImpl<MemberInfo, Long> implements MemberInfoService {

	@Resource
	private MemberInfoDao memberInfoDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = memberInfoDao;
	}


	@Override
	public List<MemberInfo> getMemberInfoList(boolean disable, boolean deleted) {
		return memberInfoDao.getMemberInfoList(disable, deleted);
	}


	@Override
	public MemberInfo findByCode(String code) {
		return memberInfoDao.findByCode( code);
	}


	@Override
	public List<MemberInfo> findByIds(List<Long> idList) {
		return memberInfoDao.findByIds( idList);
	}
	
}
