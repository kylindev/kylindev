package org.jit8.site.travel.biz.service.impl;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.travel.biz.service.ManifestoCommentService;
import org.jit8.site.travel.persist.dao.ManifestoCommentDao;
import org.jit8.site.travel.persist.domain.ManifestoComment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class ManifestoCommentServiceImpl extends GenericServiceImpl<ManifestoComment, Long> implements ManifestoCommentService {

	@Resource
	private ManifestoCommentDao manifestoCommentDao;

	@PostConstruct
	public void initRepository() {
		this.genericDao = manifestoCommentDao;
	}

	@Override
	public Page<ManifestoComment> findByManifestoId(Long manifestoId, Pageable pageable) {
		return manifestoCommentDao.findByManifestoId(manifestoId, pageable);
	}

}
