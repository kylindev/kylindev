package org.jit8.site.web.controller;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.user.business.userinfo.PermissionBusiness;
import org.jit8.user.business.userinfo.PermissionCategoryBusiness;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.jit8.user.persist.domain.userinfo.Permission;
import org.jit8.user.persist.domain.userinfo.PmnCategory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class PermissionCategoryController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(PermissionCategoryController.class);
	

	@Resource
	private PermissionCategoryBusiness permissionCategoryBusiness;
	
	@Resource
	private PermissionBusiness permissionBusiness;
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING)
	public String permissionListUI(@ModelAttribute("permission") Permission permission,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### /admin/userinfo/permission/permissionListUI #########");
		//RequestContextUtils.
		Pageable pageable2 = new PageRequest(0, 200);
		
		Page<PmnCategory> page =permissionCategoryBusiness.getPermissionCategoryList(pageable2);
		
		Page<Permission> permissionPage =permissionBusiness.getPermissionList(permission,pageable);
		
		
		model.addAttribute("currentNavigation",getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX));
		
		model.addAttribute("navigationTopList",navigationTopList);
		model.addAttribute("page", page);
		model.addAttribute("permissionPage", permissionPage);
		model.addAttribute("currentCategory",pmnCategory);
		return "permissionCategoryUI";
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_MAPPING)
	public String permissionCategoryAddUI(@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### /admin/userinfo/permission/category/addo_permission_category_add_ou_100001_u #########");
		//RequestContextUtils.
		
		
		pmnCategory =permissionCategoryBusiness.persist(pmnCategory);
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING + "{id}")
	public String permissionCategoryRemove(@PathVariable Long id,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### /admin/userinfo/permission/category/addo_permission_category_add_ou_100001_u #########");
		//RequestContextUtils.
		
		if(id != null && id>0){
			try {
				permissionCategoryBusiness.remove(id);
			} catch (BusinessException e) {
				logger.error(e.getCode(),e);
				String error = MessageUtil.getLocaleMessage(e.getCode(), null);
				redirectAttributes.addAttribute("error", error);
				return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING  ;
			}
		}
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING ;
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING)
	public String assignPermissionToCategory(@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING +" #########");
		
		try {
			permissionCategoryBusiness.assignPermissionToCategory(pmnCategory);
			
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
			String error = MessageUtil.getLocaleMessage(e.getCode(), null);
			redirectAttributes.addAttribute("error", error);
			return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING  ;
		}
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING ;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING)
	public String removePermissionFromCategory(@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING +" #########");
		
		try {
			permissionCategoryBusiness.removePermissionFromCategory(pmnCategory);
			
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
			String error = MessageUtil.getLocaleMessage(e.getCode(), null);
			redirectAttributes.addAttribute("error", error);
			return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING  ;
		}
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING ;
	}
	
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_MAPPING +"/{permissionId}")
	public String removePermissionAllFromCategory(@PathVariable("permissionId")Long permissionId, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING +" #########");
		
		try {
			permissionCategoryBusiness.removeAllPermissionFromCategory(permissionId);
			
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
			String error = MessageUtil.getLocaleMessage(e.getCode(), null);
			redirectAttributes.addAttribute("error", error);
			return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING  ;
		}
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING ;
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING +"/{permissionId}")
	public String enablePermission(@PathVariable("permissionId")Long permissionId, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable,@ModelAttribute PmnCategory pmnCategory,RedirectAttributes  redirectAttributes, Model model) {
		logger.debug("######### "+ KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING +" #########");
		
		try {
			permissionCategoryBusiness.enablePermission(permissionId);
			
		} catch (BusinessException e) {
			logger.error(e.getCode(),e);
			String error = MessageUtil.getLocaleMessage(e.getCode(), null);
			redirectAttributes.addAttribute("error", error);
			return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING  ;
		}
		
		
		return "redirect:" + KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING ;
	}
	
}
