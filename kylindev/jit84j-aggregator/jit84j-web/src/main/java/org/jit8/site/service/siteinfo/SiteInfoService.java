package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.SiteInfo;


public interface SiteInfoService extends GenericService<SiteInfo, Long>{

	public List<SiteInfo> getSiteInfoList(boolean disable, boolean deleted);
	
	public SiteInfo getSiteInfoByType(String type);
}
