package org.jit8.framework.jit84j.core.mail.business.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.mail.MessagingException;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.mail.business.MailMessageBusiness;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.core.mail.domain.MailMessage;
import org.jit8.framework.jit84j.core.mail.service.MailMessageService;
import org.jit8.framework.jit84j.core.mail.util.MailUtil;
import org.jit8.framework.jit84j.core.web.util.FilePathUtils;
import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineConstant;
import org.jit8.framework.jit84j.web.data.dropdown.StaticDataDefineManager;
import org.jit8.framework.jit84j.web.freemarker.FreemarkerUtils;
import org.jit8.framework.jit84j.web.utils.MailMessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("mailMessageBusiness")
public class MailMessageBusinessImpl implements MailMessageBusiness {

	private static final Logger LOGGER = LoggerFactory.getLogger(MailMessageBusinessImpl.class);

	@Resource
	private MailMessageService mailMessageService;

	@Resource
	private StaticDataDefineManager staticDataDefineManager;
	
	private static String appPath = FilePathUtils.getAppPath(MailMessageBusinessImpl.class);
	
	private static String templatePath = "/template/email";
	
	private static String realPath = getAppPath() + getTemplatePath();

	public List<MailMessage> getMailMessageList(boolean disable, boolean deleted) {

		return null;
	}

	@Override
	public Page<MailMessage> findAll(Pageable pageable) {
		return mailMessageService.findAll(pageable);
	}

	@Override
	public MailMessage findById(long id) {
		return mailMessageService.findOne(id);
	}

	@Override
	public MailMessage persist(MailMessage mailMessage) {

		return mailMessageService.save(mailMessage);
	}

	@Override
	@Transactional
	public MailMessage sendMessage(String subject, String path, Map<String, Object> root, String mailConfig, String sender, String reciever)
			throws BusinessException {

		if (StringUtils.isEmpty(mailConfig)) {
			mailConfig = StaticDataDefineConstant.MAIL_CONFIG_DEFAULT;
		}

		@SuppressWarnings("static-access")
		MailConfig mailConfig2 = (MailConfig) staticDataDefineManager.getData(mailConfig);
		if (StringUtils.isNotEmpty(sender)) {
			mailConfig2.setSender(sender);
		}

		MailMessage mailMessage = new MailMessage();

		String recieverRoot = (String) root.get("reciever");
		if (StringUtils.isNotEmpty(reciever)) {
			mailMessage.setReciever(reciever);
		} else if (StringUtils.isNotEmpty(recieverRoot)) {
			mailMessage.setReciever(recieverRoot);
		} else {
			mailMessage.setReciever(MailConfig.DEFAULT_EMAIL_RECIEVER);
		}

		String subjectRoot = (String) root.get("subject");
		if (StringUtils.isNotEmpty(subject)) {
			mailMessage.setSubject(subject);
		} else if (StringUtils.isNotEmpty(subjectRoot)) {
			mailMessage.setSubject(subjectRoot);
		} else {
			mailMessage.setSubject(MailConfig.DEFAULT_EMAIL_SUBJECT);
		}

		StringBuffer sb = FreemarkerUtils.process(path, root);
		if (null != sb) {
			mailMessage.setContent(sb.toString());
			String filePath = FilePathUtils.getAppPath(MailMessageBusinessImpl.class);
			System.out.println(FilePathUtils.getAppPath(MailMessageBusinessImpl.class));

			try {
				MailMessage mailMessage2 = MailUtil.sendMail(mailConfig2, mailMessage);
				persist(mailMessage2);
				mailMessage2.setResult(true);
				return mailMessage2;
			} catch (MessagingException e) {
				LOGGER.error("email.send.failure", e);
				throw new BusinessException("email.send.failure", "发送失败", e);
			}
		}
		return null;
	}

	@Override
	public MailMessage sendMessage(String subject, String path, Map<String, Object> root, String mailConfig, String reciever) throws BusinessException {
		return sendMessage(subject, path, root, mailConfig, null, reciever);
	}

	@Override
	public MailMessage sendMessage(String subject, String path, Map<String, Object> root, String reciever) throws BusinessException {
		return sendMessage(subject, path, root, null, reciever);
	}

	@Override
	public MailMessage sendMessage(String path, Map<String, Object> root, String reciever) throws BusinessException {
		return sendMessage(null, path, root, null, reciever);
	}

	@Override
	public MailMessage sendMessage(String path, Map<String, Object> root) throws BusinessException {
		return sendMessage(null, path, root, null);
	}

	public static void main(String[] args) {
		String path = FilePathUtils.getAppPath(MailMessageBusinessImpl.class);
		String template = "/template/email";
		System.out.println();
		MailMessage mm = MailMessageUtils.getMailMessage(path+template,"active_user_success.ftl","active_user_success");
		System.out.println(mm.getFileRealPath());
		
	}

	@Override
	public MailMessage sendMessage(MailMessage mailMessage) throws BusinessException {
		String mailConfig = mailMessage.getMailConfig();
		if (StringUtils.isEmpty(mailConfig)) {
			mailConfig = StaticDataDefineConstant.MAIL_CONFIG_DEFAULT;
		}

		String sender = mailMessage.getSender();
		@SuppressWarnings("static-access")
		MailConfig mailConfig2 = (MailConfig) staticDataDefineManager.getData(mailConfig);
		if (StringUtils.isNotEmpty(sender)) {
			mailConfig2.setSender(sender);
		}

		String reciever = mailMessage.getReciever();
		Map<String, Object> root = mailMessage.getRoot();

		if (StringUtils.isNotEmpty(reciever)) {
			mailMessage.setReciever(reciever);
		} else if (null != root) {
			String recieverRoot = (String) root.get("reciever");
			if (StringUtils.isNotEmpty(recieverRoot)) {
				mailMessage.setReciever(recieverRoot);
			} else {
				mailMessage.setReciever(MailConfig.DEFAULT_EMAIL_RECIEVER);
			}
		} else {
			mailMessage.setReciever(MailConfig.DEFAULT_EMAIL_RECIEVER);
		}

		String subject = mailMessage.getSubject();
		if (StringUtils.isNotEmpty(subject)) {
			mailMessage.setSubject(subject);
		} else if (null != root) {
			String subjectRoot = (String) root.get("subject");
			if (StringUtils.isNotEmpty(subjectRoot)) {
				mailMessage.setSubject(subjectRoot);
			} else {
				mailMessage.setSubject(MailConfig.DEFAULT_EMAIL_SUBJECT);
			}
		} else {
			mailMessage.setSubject(MailConfig.DEFAULT_EMAIL_SUBJECT);
		}

		String path = mailMessage.getTemplatePath();
		StringBuffer sb = FreemarkerUtils.process(path, root);
		if (null != sb) {
			mailMessage.setContent(sb.toString());

			String realPath = mailMessage.getFileRealPath();

			try {
				String fileName = FilePathUtils.generateCurrentTimeToFileName(StringUtils.trimToEmpty(mailMessage.getFileName()), "html");
				String fileFullPath = realPath + "/" + fileName;
				String fileAccessPath = mailMessage.getFileAccessPath() + "/" + fileName;
				String fileAccessFullPath = mailMessage.getFileAccessPath() + "/" + fileName;
				Writer fileOutputStream = new FileWriter(fileFullPath);

				org.springframework.util.FileCopyUtils.copy(sb.toString(), fileOutputStream);
				mailMessage.setFileAccessFullPath(fileAccessFullPath);
				mailMessage.setFileAccessPath(fileAccessPath);
				mailMessage.setFileRealPath(fileFullPath);

				MailMessage mailMessage2 = MailUtil.sendMail(mailConfig2, mailMessage);
				persist(mailMessage2);
				mailMessage2.setResult(true);
				return mailMessage2;
			} catch (MessagingException e) {
				LOGGER.error("email.send.failure", e);
				throw new BusinessException("email.send.failure", "发送失败", e);
			} catch (IOException e) {
				LOGGER.error("email.stored.failure", e);
				throw new BusinessException("email.stored.failure", "存储失败", e);
			}
		}
		return null;
	}

	/**
	 * @return the appPath
	 */
	public static String getAppPath() {
		return appPath;
	}

	/**
	 * @param appPath the appPath to set
	 */
	public static void setAppPath(String appPath) {
		MailMessageBusinessImpl.appPath = appPath;
	}

	/**
	 * @return the templatePath
	 */
	public static String getTemplatePath() {
		return templatePath;
	}

	/**
	 * @param templatePath the templatePath to set
	 */
	public static void setTemplatePath(String templatePath) {
		MailMessageBusinessImpl.templatePath = templatePath;
	}

	/**
	 * @return the realPath
	 */
	public static String getRealPath() {
		return realPath;
	}

	/**
	 * @param realPath the realPath to set
	 */
	public static void setRealPath(String realPath) {
		MailMessageBusinessImpl.realPath = realPath;
	}
	
	public String getDefaultTemplatePath(){
		return MailMessageBusinessImpl.realPath;
	}

}
