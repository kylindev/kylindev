package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.RolePermissionDao;
import org.jit8.user.persist.domain.userinfo.RolePermission;
import org.jit8.user.service.userinfo.RolePermissionService;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionServiceImpl extends GenericServiceImpl<RolePermission, Long> implements RolePermissionService {

	@Resource
	private RolePermissionDao rolePermissionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = rolePermissionDao;
	}


	@Override
	public List<RolePermission> findByPermissionId(Long permissionId) {
		
		return rolePermissionDao.findByPermissionId( permissionId);
	}


}
