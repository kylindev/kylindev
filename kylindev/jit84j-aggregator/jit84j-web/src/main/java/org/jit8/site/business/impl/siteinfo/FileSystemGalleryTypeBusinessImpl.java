package org.jit8.site.business.impl.siteinfo;

import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.siteinfo.FileSystemGalleryTypeBusiness;
import org.jit8.site.persist.domain.siteinfo.FileSystemGalleryType;
import org.jit8.site.service.siteinfo.FileSystemGalleryTypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("fileSystemGalleryTypeBusiness")
public class FileSystemGalleryTypeBusinessImpl implements FileSystemGalleryTypeBusiness{

	@Resource
	private FileSystemGalleryTypeService fileSystemGalleryTypeService;
	
	@Override
	public List<FileSystemGalleryType> getList() {
		return fileSystemGalleryTypeService.getFileSystemGalleryTypeList(false, false);
	}
	
	@Transactional
	@Override
	public FileSystemGalleryType persist(FileSystemGalleryType gallery){
		return merge(gallery);
	}
	
	@Transactional
	@Override
	public FileSystemGalleryType merge(FileSystemGalleryType fileSystemGalleryType){
		
		Long id = fileSystemGalleryType.getId();
		FileSystemGalleryType fileSystemGalleryTypeSaved = null;
		if(null != id && id > 0){
			fileSystemGalleryTypeSaved = fileSystemGalleryTypeService.findOne(id);
			if(null != fileSystemGalleryTypeSaved){
				fileSystemGalleryTypeSaved.setDeleted(false);
				fileSystemGalleryTypeSaved.setDescription(fileSystemGalleryType.getDescription());
				fileSystemGalleryTypeSaved.setName(fileSystemGalleryType.getName());
				fileSystemGalleryTypeSaved.setType(fileSystemGalleryType.getType());
				fileSystemGalleryTypeSaved.setSequence(fileSystemGalleryType.getSequence());
			}
			
		}else{
			fileSystemGalleryTypeSaved = fileSystemGalleryType;
		}
		return fileSystemGalleryTypeService.save(fileSystemGalleryTypeSaved);
	}

	@Override
	public FileSystemGalleryType findById(Long id) {
		return fileSystemGalleryTypeService.findOne(id);
	}

	@Override
	public FileSystemGalleryType deleteById(Long id) {
		FileSystemGalleryType old = fileSystemGalleryTypeService.findOne(id);
		old.setDeleted(true);
		return fileSystemGalleryTypeService.save(old);
	}
	
	public void removeById(Long id) {
		fileSystemGalleryTypeService.delete(id);
	}

	@Override
	public Page<FileSystemGalleryType> findAll(Pageable pageable) {
		return fileSystemGalleryTypeService.findAll(pageable);
	}

}
