package org.jit8.site.web.controller.command;

import java.io.Serializable;

import javax.persistence.Transient;

public class ConstantModelCommand implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5355363518632894464L;


	private String clazzFullName;
	
	
	/**
	 * 该类功能的详细描述
	 */
	private String description;
	
	/**
	 * 描述该类的标题
	 */
	private String title;
	
	
	private String developerUserId;
	private String developerName;
	
	
	@Transient
	private Long id;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getClazzFullName() {
		return clazzFullName;
	}
	public void setClazzFullName(String clazzFullName) {
		this.clazzFullName = clazzFullName;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDeveloperUserId() {
		return developerUserId;
	}
	public void setDeveloperUserId(String developerUserId) {
		this.developerUserId = developerUserId;
	}
	public String getDeveloperName() {
		return developerName;
	}
	public void setDeveloperName(String developerName) {
		this.developerName = developerName;
	}
}
