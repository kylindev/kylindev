package org.jit8.site.service.impl.menu;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.site.persist.dao.menu.NavigatMenuDao;
import org.jit8.site.persist.domain.menu.NavigatMenu;
import org.jit8.site.service.menu.NavigatMenuService;
import org.springframework.stereotype.Service;

@Service
public class NavigatMenuServiceImpl extends GenericServiceImpl<NavigatMenu, Long> implements NavigatMenuService {

	@Resource
	private NavigatMenuDao navigatMenuDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = navigatMenuDao;
	}


	@Override
	public List<NavigatMenu> getNavigatMenuList(boolean disable, boolean deleted) {
		return navigatMenuDao.getNavigatMenuList(disable, deleted);
	}
	
	@Override
	public List<NavigatMenu> getNavigatMenuListByIds(boolean disable,
			boolean deleted, List<Long> navigatMenuIdList) {
		return navigatMenuDao.getNavigatMenuListByIds(disable, deleted, navigatMenuIdList);
	}


	@Override
	public List<NavigatMenu> getNavigatMenuListByParentNull() {
		return navigatMenuDao.getNavigatMenuListByParentNull();
	}


	@Override
	public List<NavigatMenu> getNavigatMenuByCode(boolean disable, boolean deleted,
			String code) {
		return navigatMenuDao.getNavigatMenuByCode(disable, deleted, code);
	}


	@Override
	public List<NavigatMenu> getNavigatMenuByTopCode(boolean disable,
			boolean deleted, String code) {
		return navigatMenuDao.getNavigatMenuByTopCode(disable, deleted, code);
	}


	@Override
	public NavigatMenu findByCode(String code) {
		return navigatMenuDao.findByCode(code);
	}
	
}
