package org.jit8.site.business.siteinfo;

import java.util.List;

import org.jit8.site.persist.domain.siteinfo.FileInfoConfig;



public interface FileInfoConfigBusiness {

	public List<FileInfoConfig> getFileInfoConfigList();
	public FileInfoConfig persist(FileInfoConfig fileInfoConfig);
	
	public FileInfoConfig getFileInfoConfigById(Long id);
	
	public FileInfoConfig deleteFileInfoConfigById(Long id);
	
	public FileInfoConfig merge(FileInfoConfig stickyNoteConfig);
	
}
