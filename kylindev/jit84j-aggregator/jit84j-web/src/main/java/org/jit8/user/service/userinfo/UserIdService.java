package org.jit8.user.service.userinfo;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.UserId;

public interface UserIdService extends GenericService<UserId, Long>{

}
