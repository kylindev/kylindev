package org.jit8.site.service.siteinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.site.persist.domain.siteinfo.Gallery;


public interface GalleryService extends GenericService<Gallery, Long>{

	public List<Gallery> getGalleryList(boolean disable, boolean deleted);
}
