package org.jit8.site.business.impl.article;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.site.business.article.ArticleCategoryBusiness;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.Article;
import org.jit8.site.persist.domain.article.ArticleCategory;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.service.article.ArticleCategoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("articleCategoryBusiness")
public class ArticleCategoryBusinessImpl implements ArticleCategoryBusiness{

	private static final Logger logger = LoggerFactory.getLogger(ArticleCategoryBusinessImpl.class);
	
	@Resource
	private ArticleCategoryService articleCategoryService;
	
	@Resource
	private CategoryBusiness categoryBusiness;
	

	@Override
	public List<ArticleCategory> getArticleCategoryList() {
		return articleCategoryService.getArticleCategoryList(false, false);
	}
	
	@Transactional
	@Override
	public ArticleCategory persist(ArticleCategory articleCategory){
		
		return articleCategoryService.save(articleCategory);
	}
	
	@Transactional
	@Override
	public List<ArticleCategory> persist(List<ArticleCategory> articleCategoryList){
		
		return (List<ArticleCategory>) articleCategoryService.save(articleCategoryList);
	}
	
	@Transactional
	@Override
	public ArticleCategory merge(ArticleCategory articleCategory){
		ArticleCategory old = getArticleCategoryById(articleCategory.getId());
		BeanUtils.copyProperties(articleCategory, old);
		return articleCategoryService.save(old);
	}

	@Override
	public ArticleCategory getArticleCategoryById(Long id) {
		return articleCategoryService.findOne(id);
	}

	@Override
	public ArticleCategory deleteArticleCategoryById(Long id) {
		ArticleCategory old = articleCategoryService.findOne(id);
		old.setDeleted(true);
		return articleCategoryService.save(old);
	}

	@Override
	public List<ArticleCategory> asignCategory(ArticleCategory articleCategory) {
		
		Long articleId = articleCategory.getArticleId();
		
		
		List<ArticleCategory> currentArticleCategoryList = null;
		List<ArticleCategory> newArticleCategoryList = new ArrayList<ArticleCategory>();
		if(null != articleId && articleId>0){
			currentArticleCategoryList = articleCategoryService.getArticleCategoryListByArticleId(false, false, articleId);
		}
		
		
		String categoryIdList = articleCategory.getCategoryIdList();
		List<Category> categoryList = null;
		if(StringUtils.isNotEmpty(categoryIdList)){
			categoryList = categoryBusiness.getCategoryListByIds(categoryIdList);
		}
		
		if(null != categoryList && categoryList.size()>0){
			for(Category category : categoryList){
				boolean exist = false;
				if(currentArticleCategoryList!=null && currentArticleCategoryList.size()>0){
					for(ArticleCategory currentArticleCategory : currentArticleCategoryList){
						if (category.getUniqueIdentifier().equals(
								currentArticleCategory.getCategory()
										.getUniqueIdentifier())) {
							currentArticleCategory.setDeleted(false);
							currentArticleCategory.setDisable(false);
							newArticleCategoryList.add(currentArticleCategory);
							exist=true;
							currentArticleCategoryList.remove(currentArticleCategory);
							break;
						}else{
							currentArticleCategory.setDeleted(true);
							currentArticleCategory.setDisable(true);
						}
					}
				}
				if(!exist){
					ArticleCategory articleCategoryAdd = new ArticleCategory();
					Article article = new Article();
					article.setId(articleId);
					articleCategoryAdd.setArticle(article);
					articleCategoryAdd.setCategory(category);
					newArticleCategoryList.add(articleCategoryAdd);
				}
			}
			newArticleCategoryList = (List<ArticleCategory>) articleCategoryService.save(newArticleCategoryList);
		}
		return newArticleCategoryList;
	}

}
