package org.jit8.site.biz.siteinfo.stickynote.business;

import java.util.List;

import org.jit8.site.persist.siteinfo.stickynote.domain.CategorySpecial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface CategorySpecialBusiness {

	public List<CategorySpecial> getCategorySpecialList();
	public CategorySpecial persist(CategorySpecial categorySpecial);
	
	public CategorySpecial getCategorySpecialById(Long id);
	
	public CategorySpecial deleteCategorySpecialById(Long id);
	
	public Page<CategorySpecial> findAll(Pageable pageable);
	
	
	public void removeById(Long id);
	
	public void asignCategory2SpecialArea(CategorySpecial categorySpecial);
	
	public void removeCategoryFromSpecialArea(CategorySpecial categorySpecial);
	
	public void saveCategorySpecialList(CategorySpecial categorySpecial);
	
	public Page<CategorySpecial> findAll(CategorySpecial categorySpecial,Pageable pageable) ;
	
	
	public List<CategorySpecial> getCategorySpecialListBySpecialAreaId(Long id, boolean isAll);
	
	public List<CategorySpecial> getByCategorySpecialIdList(
			List<Long> categorySpecialIdList);
	
	
	
}
