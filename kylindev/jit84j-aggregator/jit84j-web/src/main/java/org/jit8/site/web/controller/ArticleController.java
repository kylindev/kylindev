package org.jit8.site.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.jit8.site.business.article.ArticleBusiness;
import org.jit8.site.business.article.CategoryBusiness;
import org.jit8.site.persist.domain.article.Article;
import org.jit8.site.persist.domain.article.Category;
import org.jit8.site.persist.domain.article.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ArticleController {

	private static final Logger logger = LoggerFactory.getLogger(ArticleController.class);
	
	@Resource
	private ArticleBusiness articleBusiness; 
	
	@Resource
	private CategoryBusiness categoryBusiness; 
	
	@RequestMapping("/admin/article/articleAddUI")
	public String articleAddUI(@ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/addUI #########");
		
		//musicBusiness.persist(music);
		model.addAttribute("article", new Article());
		
		prepareList(model);
		
		return "articleAddUI";
	}
	@RequestMapping("/admin/article/articleModifyUI/{id}")
	public String articleModify(@PathVariable("id") Long id, Model model) throws IOException {
		logger.debug("######### /admin/article/articleModify #########");
	
		Article article = articleBusiness.getArticleById(id);
		model.addAttribute("article",article);
		List<Category> categoryList = categoryBusiness.getCategoryListByParentNull();
		ArticleCategoryController.setCurrentArticleAsignedForCategoryList(article.getArticleCategoryList(),categoryList);
		model.addAttribute("categoryList",categoryList);
		model.addAttribute("category",categoryList.get(0));
		
		
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tagList.add(tag1);
		tag1.setId(1l);
		tag1.setName("江湖");
		
		Tag tag2 = new Tag();
		tagList.add(tag2);
		tag2.setId(2l);
		tag2.setName("英雄");
		model.addAttribute("tagList",tagList);
		
		//prepareList(model);
		return "articleModifyUI";
	}
	
	@RequestMapping("/admin/article/articleModify")
	public String articleModify(@ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/addUI #########");
		
		articleBusiness.merge(article);
		prepareList(model);
		return "redirect:articleListUI";
	}
	
	@RequestMapping("/admin/article/articleModify/simple")
	@ResponseBody
	public String articleModifySimple(@RequestBody @ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/articleModify/simple #########");
		
		//articleBusiness.merge(article);
		//prepareList(model);
		//return new HashMap<String, String>().put("success", "true");
		return "success";
	}
	
	@RequestMapping("/admin/article/articleAdd")
	public String articleAdd(@ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
		logger.debug("######### /admin/article/addUI #########");
		
		articleBusiness.persist(article);
		prepareList(model);
		return "redirect:articleAddUI";
	}
	
	@RequestMapping("/admin/article/articleListUI")
	public String articleList(@ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
	
		PageRequest page = new PageRequest(0, 5);
		Page<Article> pageArticleList = articleBusiness.getArticleList(page);
		List<Article> articleList = pageArticleList.getContent();
		model.addAttribute("articleList",articleList);
		
		return "articleListUI";
	}
	
	@RequestMapping("/admin/article/articleListUI/google")
	public String articleListPage(@ModelAttribute("article") Article article,BindingResult result, Model model) throws IOException {
	
		PageRequest page = new PageRequest(0, 5);
		Page<Article> pageArticleList = articleBusiness.getArticleList(page);
		List<Article> articleList = pageArticleList.getContent();
		model.addAttribute("articleList",articleList);
		
		return "google";
	}
	
	
	private void prepareList(Model model){
		
		List<Category> categoryList = categoryBusiness.getCategoryListByParentNull();
		
		model.addAttribute("categoryList",categoryList);
		model.addAttribute("category",categoryList.get(0));
		
		
		List<Tag> tagList = new ArrayList<Tag>();
		Tag tag1 = new Tag();
		tagList.add(tag1);
		tag1.setId(1l);
		tag1.setName("江湖");
		
		Tag tag2 = new Tag();
		tagList.add(tag2);
		tag2.setId(2l);
		tag2.setName("英雄");
		model.addAttribute("tagList",tagList);
	}
	
	
}
