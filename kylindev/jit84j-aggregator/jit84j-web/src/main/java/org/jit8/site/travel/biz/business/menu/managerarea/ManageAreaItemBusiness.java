package org.jit8.site.travel.biz.business.menu.managerarea;

import java.util.List;

import org.jit8.site.travel.persist.domain.menu.managerarea.ManageAreaItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface ManageAreaItemBusiness {
	
	ManageAreaItem persist(ManageAreaItem manageAreaItem);
	
	List<ManageAreaItem> findAll();
	
	Page<ManageAreaItem> findAll(Pageable pageable);
	
	List<ManageAreaItem> findAllAvailable();
	
	List<ManageAreaItem> findAllDisplay();
	
	ManageAreaItem findById(Long id);
	
	void removeById(Long id);
	
	List<ManageAreaItem> persistList(List<ManageAreaItem> manageAreaItemList);
	
	
}
