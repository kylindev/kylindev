package org.jit8.site.travel.persist.repository;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.reponsitory.GenericJpaRepository;
import org.jit8.site.travel.persist.domain.InstantShareImageCounter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface InstantShareImageCounterRepository extends GenericJpaRepository<InstantShareImageCounter, Long>{

	@Query("from InstantShareImageCounter o where o.countKey=:countKey and o.userId=:userId and o.deleted=false")
	public InstantShareImageCounter findByCountKeyAndUserId(@Param("countKey")String day, @Param("userId")Long userId);
	
	@Query("from InstantShareImageCounter o where o.id=:id and o.userId=:userId and o.deleted=false")
	public InstantShareImageCounter findByIdAndUserId(@Param("id")Long id, @Param("userId")Long userId);
	
	@Query("from InstantShareImageCounter o where o.yearMonth=:yearMonth and o.userId=:userId and o.deleted=false")
	public List<InstantShareImageCounter> findByYearMonthAndUserId(@Param("yearMonth")String yearMonth, @Param("userId")Long userId);
	
}
