package org.jit8.site.travel.persist.dao;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InstantShareImageDao extends GenericDao<InstantShareImage, Long> {

	public List<InstantShareImage> getInstantShareImageList(boolean disable, boolean deleted) ;
	
	public InstantShareImage findByCode(String code);
	
	public List<InstantShareImage> findByIds(List<Long> idList);
	
	public void removeByIdAndUserId(Long id, Long userId);
	
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable);
	
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable);
}
