package org.jit8.site.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.mail.business.MailConfigBusiness;
import org.jit8.framework.jit84j.core.mail.domain.MailConfig;
import org.jit8.framework.jit84j.web.controller.BaseCommonController;
import org.jit8.framework.jit84j.web.utils.MessageUtil;
import org.jit8.site.persist.domain.menu.Navigation;
import org.jit8.site.persist.domain.siteinfo.FileMime;
import org.jit8.user.developer.common.constants.kylilnboy.KylinboyCoreDeveloperConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefaults;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MailConfigController extends BaseCommonController{

	private static final Logger logger = LoggerFactory.getLogger(MailConfigController.class);
	
	@Resource
	private MailConfigBusiness mailConfigBusiness;
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_INDEX_UI_MAPPING)
	public String mailConfigIndexUI(@ModelAttribute("mailConfig") MailConfig mailConfig, Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /mailConfigUI #########");
		
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI);
		model.addAttribute("currentNavigation",navigation);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI);
		model.addAttribute("editNavigation",editNavigation);
		
		Page<MailConfig> page = mailConfigBusiness.findAll(pageable);
		model.addAttribute("page",page);
		
		return "mailConfigIndexUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI_MAPPING)
	public String mailConfigEditUI(@ModelAttribute("mailConfig") MailConfig mailConfig, Model model, @PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /mailConfigUI #########");
		
		Long id = mailConfig.getId();
		if(id!= null && id>0){
			mailConfig = mailConfigBusiness.findById(id);
		}
		
		model.addAttribute("navigationTopList",navigationTopList);
		Navigation navigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI);
		model.addAttribute("currentNavigation",navigation);
		Navigation editNavigation = getCurrentNavigationByCode(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI);
		model.addAttribute("editNavigation",editNavigation);
		
		Page<MailConfig> page = mailConfigBusiness.findAll(pageable);
		model.addAttribute("page",page);
		model.addAttribute("mailConfig",mailConfig);
		
		return "mailConfigEditUI";
	}
	
	@RequestMapping(KylinboyCoreDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_MAPPING)
	public String mailConfigAdd(@ModelAttribute("mailConfig") MailConfig mailConfig, Model model ,@PageableDefaults(pageNumber = 0, value = 200) Pageable pageable) throws IOException {
		logger.debug("######### /fileMimeAdd #########");
		mailConfig = mailConfigBusiness.persist(mailConfig);
		String msg = MessageUtil.getLocaleMessage("org.jit8.100001.userinfo.UrlMapping.import.success",null);
		model.addAttribute("msg", msg);
		model.addAttribute("mailConfig",mailConfig);
		return mailConfigEditUI(new MailConfig(),model,pageable);
	}
	
}
