package org.jit8.site.travel.persist.dao.menu.managerarea;

import java.util.List;

import org.jit8.framework.jit84j.core.persistent.dao.GenericDao;
import org.jit8.site.travel.persist.domain.menu.managerarea.ItemButton;

public interface ItemButtonDao extends GenericDao<ItemButton, Long> {

	public List<ItemButton> findAllAvailable();

	public List<ItemButton> findAllDisplay();

	public List<ItemButton> findByIdList(List<Long> idList);
}
