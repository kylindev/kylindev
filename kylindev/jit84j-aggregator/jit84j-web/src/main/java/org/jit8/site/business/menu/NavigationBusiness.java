package org.jit8.site.business.menu;

import java.util.List;

import org.jit8.framework.jit84j.exception.BusinessException;
import org.jit8.site.persist.domain.menu.Navigation;



public interface NavigationBusiness {

	public List<Navigation> getNavigationList();
	public Navigation persist(Navigation navigation) throws BusinessException;
	
	public Navigation merge(Navigation navigation) throws BusinessException;
	
	public Navigation getNavigationById(Long id);
	
	/**
	 * 通过idKey查找菜单，每个菜单的idKey若不为空必然唯一，并且idKey仅用于系统预定义的路径，
	 * 如果是自定义路径，该字段为空，code字段将被填充
	 * UrlMapping绑定菜单时将idKey一齐填充进来
	 * @param idKey
	 * @return
	 */
	public Navigation findNavigationByIdKey(String idKey);//UrlMapping绑定菜单时将idKey一齐填充进来
	
	public Navigation deleteNavigationById(Long id);
	
	public List<Navigation> getNavigationListByIds(String navigationIdList);
	
	public List<Navigation> getNavigationListByParentNull();
	
	public List<Navigation> saveNavigationList(Navigation navigation);
	
	public List<Navigation> getNavigationByCode(String code);
	
	public List<Navigation> getTopNavigationByCode();
	
	public List<Navigation> getNavigationByTopCode(boolean disable,
			 String code);
	
	public List<Navigation> getNavigationByTopCode();
	
	public Navigation getNavigationByCodeAndDeveloper(String code, Long userId);
	
	public List<Navigation> getNavigationByParentCode(
			boolean deleted, String code);
	public List<Navigation> getListByTopCode();
	
	public void setParentTopNavigation(Navigation navigation);
}
