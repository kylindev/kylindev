package org.jit8.user.service.impl.userinfo;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.jit8.framework.jit84j.core.service.impl.GenericServiceImpl;
import org.jit8.user.persist.dao.userinfo.UserPermissionDao;
import org.jit8.user.persist.domain.userinfo.UserPermission;
import org.jit8.user.service.userinfo.UserPermissionService;
import org.springframework.stereotype.Service;

@Service
public class UserPermissionServiceImpl extends GenericServiceImpl<UserPermission, Long> implements UserPermissionService {

	@Resource
	private UserPermissionDao userPermissionDao;
	

	@PostConstruct
	public void initRepository(){
		this.genericDao = userPermissionDao;
	}


	@Override
	public List<UserPermission> findByPermissionId(Long permissionId) {
		
		return userPermissionDao.findByPermissionId(permissionId);
	}


}
