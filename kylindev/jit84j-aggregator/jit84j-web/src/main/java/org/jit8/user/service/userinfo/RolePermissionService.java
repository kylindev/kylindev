package org.jit8.user.service.userinfo;

import java.util.List;

import org.jit8.framework.jit84j.core.service.GenericService;
import org.jit8.user.persist.domain.userinfo.RolePermission;

public interface RolePermissionService extends GenericService<RolePermission, Long>{

	public List<RolePermission> findByPermissionId(Long permissionId);
}
