package org.jit8.site.travel.biz.business;

import java.util.List;

import org.jit8.site.travel.persist.domain.InstantShareComment;
import org.jit8.user.persist.domain.userinfo.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;



public interface InstantShareCommentBusiness {

	public List<InstantShareComment> getInstantShareCommentList();
	
	public InstantShareComment persist(InstantShareComment instantShareComment);
	
	public InstantShareComment persist(InstantShareComment instantShareComment, User user);
	
	public InstantShareComment getInstantShareCommentById(Long id);
	
	public InstantShareComment deleteInstantShareCommentById(Long id);
	
	public Page<InstantShareComment> findAll(Pageable pageable);
	
	public InstantShareComment findByCode(String code);
	
	public void removeById(Long id);
	
	public List<InstantShareComment> findByIds(List<Long> idList);
	
	public InstantShareComment findOneByIdAndUserId(Long id, Long userId);
	
	public void removeByIdAndUserId(Long id,Long userId);
	
	public Page<InstantShareComment> findByInstantShareId(Long instantShareId, Pageable pageable);
}
