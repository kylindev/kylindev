package org.jit8.user.developer.common.constants.kylilnboy;

import java.util.List;

import org.jit8.user.persist.domain.userinfo.UrlStaticMapping;

public class KylinboyCoreDeveloperMappingConstant extends KylinboyCoreDeveloperConstant{

	private static final long serialVersionUID = 8267163625961520713L;
	
	/**
	 * 获取系统预定义的URL列表
	 * @return
	 */
	@Override
	public List<UrlStaticMapping> getUrlStaticMappingModuleList(){
		
		UrlStaticMapping userModule = addModule(KYLINBOY_USER_MODULE_ID,KYLINBOY_USER_MODULE_NAME,KYLINBOY_USER_MODULE_CODE);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING
				);
		
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING
				);
		
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING
				);	
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_MAPPING
				);
		addUrlStaticMapping(
				userModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_USER_DELETE_MAPPING
				);
		//为用户分配权限页面
				addUrlStaticMapping(
						userModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING
						);
				//为用户分配权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING
						);	
				
				//为用户移除权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_MAPPING
						);	
				//为用户移除所有权限执行
				addUrlStaticMapping(
						userModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING
						);	


				
				//选择用户页面
				addUrlStaticMapping(
						userModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING
						);
				
				
				
		UrlStaticMapping adminCommonModule = addModule(KYLINBOY_ADMIN_COMMON_MODULE_ID,KYLINBOY_ADMIN_COMMON_MODULE_NAME,KYLINBOY_ADMIN_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_INDEX_MAPPING
				);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_UI_MAPPING
				);
		addUrlStaticMapping(
				adminCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_MAPPING
				);

		
		UrlStaticMapping commonModule = addModule(KYLINBOY_COMMON_MODULE_ID,KYLINBOY_COMMON_MODULE_NAME,KYLINBOY_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				commonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_VALIDATE_CODE_MAPPING
				);
		addUrlStaticMapping(
				commonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING
				);
		
		
		
		UrlStaticMapping frontCommonModule = addModule(KYLINBOY_FRONT_COMMON_MODULE_ID,KYLINBOY_FRONT_COMMON_MODULE_NAME,KYLINBOY_FRONT_COMMON_MODULE_CODE);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING
				);
		
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_LOGOUT_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING
				);
		addUrlStaticMapping(
				frontCommonModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_INDEX_MAPPING
				);
		
		
		UrlStaticMapping developerModule = addModule(KYLINBOY_DEVELOPER_MODULE_ID,KYLINBOY_DEVELOPER_MODULE_NAME,KYLINBOY_DEVELOPER_MODULE_CODE);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING
				);
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING
				);
		/**列表Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING
				);
		
		/**列表Constant model 仅有查看功能*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_VIEW_LIST_UI_MAPPING
				);
		
		/**列表URL定义 */
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_MAPPING
				);

		/**添加Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_MAPPING
				);
		
		/**更新Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_MAPPING
				);

		/**删除Constant model*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_MAPPING
				);


		/**添加Constant model UI*/
		addUrlStaticMapping(
				developerModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_CONSTANT_MODEL_SHOW_FORM_UI_MAPPING
				);
		
		
		

		
		UrlStaticMapping permissionModule = addModule(KYLINBOY_PERMISSION_MODULE_ID,KYLINBOY_PERMISSION_MODULE_NAME,KYLINBOY_PERMISSION_MODULE_CODE);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ADD_MAPPING
				);
		
		//权限修改页面
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_MAPPING
				);
		
		
		
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_MAPPING
				);
		//权限类别删除
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING
				);
		//为权限类别分配权限
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING
				);
		
		//从权限类别移除权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING
						);
				//从所有权限类别移除该权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_MAPPING
						);
				
				//启用该权限
				addUrlStaticMapping(
						permissionModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING
						);
				
				
		
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING
				);
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_MAPPING
				);
		//URL分类
		addUrlStaticMapping(
				permissionModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_URL_MAPPING_FILTER_AJAX_MAPPING
				);
		//按类保存URL
		addUrlStaticMapping(
						permissionModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING
						);
		//URL映射列表
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_MAPPING
								);
				
				//URL映射分类
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_MAPPING
								);
				//URL映射分类[For Navigation]
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_MODULE_LIST_FOR_NAVIGATION_MAPPING
								);
				//URL映射列表[For Navigation]
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING
								);

				
				//URL关联权限
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING
								);
				//所有URL关联权限
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_ALL_URL_BIND_TO_PERMISSION_MAPPING
								);
				//URL初始化
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_INIT_MAPPING
								);

			
				
				//URL解除权限关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING
								);
				
				
				//URL导航菜单关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING
								);
				//URL解除导航菜单关联
				addUrlStaticMapping(
								permissionModule,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_ID,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_NAME,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_DESC,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_URL,
								KylinboyCoreDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING
								);
				


				
		UrlStaticMapping roleModule = addModule(KYLINBOY_ROLE_MODULE_ID,KYLINBOY_ROLE_MODULE_NAME,KYLINBOY_ROLE_MODULE_CODE);
		addUrlStaticMapping(
				roleModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				roleModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_MAPPING
				);
		addUrlStaticMapping(
				roleModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ADD_UI_MAPPING
				);
		//为角色添分配权限页面
		addUrlStaticMapping(
				roleModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_UI_MAPPING
				);
		//为角色添分配权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_ASIGN_PERMISSION_MAPPING
						);
				//角色移除权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_PERMISSION_MAPPING
						);
				//角色移除所有权限执行
				addUrlStaticMapping(
						roleModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_ROLE_REMOVE_ALL_PERMISSION_MAPPING
						);
				//选择角色ajax请求
				addUrlStaticMapping(
						roleModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING
						);

		
		UrlStaticMapping errorModule = addModule(KYLINBOY_ERROR_MODULE_ID,KYLINBOY_ERROR_MODULE_NAME,KYLINBOY_ERROR_MODULE_CODE);
		addUrlStaticMapping(
				errorModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_403_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_404_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_405_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_406_UI_MAPPING
				);
		addUrlStaticMapping(
				errorModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_EXCEPTION_500_UI_MAPPING
				);
		
		UrlStaticMapping navigationModule = addModule(KYLINBOY_NAVIGATION_MODULE_ID,KYLINBOY_NAVIGATION_MODULE_NAME,KYLINBOY_NAVIGATION_MODULE_CODE);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_UI_MAPPING
				);
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MANAGER_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_ADD_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_MAPPING
				);
		
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_MAPPING
				);
		//前台导航菜单管理
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MANAGER_UI_MAPPING
				);
		//前台导航菜单列表
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_LIST_UI_MAPPING
				);
		//前台菜单修改页面
		addUrlStaticMapping(
				navigationModule,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_ID,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_NAME,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_DESC,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_URL,
				KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING
				);
		//前台菜单修改提交
				addUrlStaticMapping(
						navigationModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_MAPPING
						);
				//前台菜单添加页面
				addUrlStaticMapping(
						navigationModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_UI_MAPPING
						);
				
				//前台菜单添加提交
				addUrlStaticMapping(
						navigationModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_MAPPING
						);
				
				/** 站长关系管理模块 begin*/
				UrlStaticMapping ownRelationModule = addModule(KYLINBOY_OWNRELATION_MODULE_ID,KYLINBOY_OWNRELATION_MODULE_NAME,KYLINBOY_OWNRELATION_MODULE_CODE);
				//站长关系管理页面
				addUrlStaticMapping(
						ownRelationModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_MAPPING
						);
				/** 站长关系管理模块 end*/
				
				
				/** 站点设置模块 begin*/
				UrlStaticMapping siteInfoSettingModule = addModule(KYLINBOY_SITEINFO_SETTING_MODULE_ID,KYLINBOY_SITEINFO_SETTING_MODULE_NAME,KYLINBOY_SITEINFO_SETTING_MODULE_CODE);
				//站点列表页面
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_LIST_MAPPING
						);
				
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_MAPPING
						);
				/**基本信息设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_UI_MAPPING
						);
				/**随手贴设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_UI_MAPPING
						);
				
				/**基本信息设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING
						);
				/**随手贴设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING
						);

				/**文件上传设置页面 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFOFILEINFO_EDIT_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_UI_MAPPING
						);
				/**文件上传设置提交 */
				addUrlStaticMapping(
						siteInfoSettingModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_MAPPING
						);


				/** 站点设置模块 end*/
				
				
				/** 缓存模块 MemachedModelController begin*/
				UrlStaticMapping memcacheModelModule = addModule(KYLINBOY_MEMACHED_MODEL_MODULE_ID,KYLINBOY_MEMACHED_MODEL_MODULE_NAME,KYLINBOY_MEMACHED_MODEL_MODULE_CODE);
				//缓存管理页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_INDEX_MAPPING
						);
				//缓存列表页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_LIST_MAPPING
						);

				//对象缓存【内存】页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_OBJECT_MAPPING
						);
				//页面缓存【html】页面
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_HTML_MAPPING
						);

				//设置过期时间
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING
						);

				//刷新缓存
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_MAPPING
						);
				
				//刷新整个缓存
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_MAPPING
						);

				//删除缓存ByKey
				addUrlStaticMapping(
						memcacheModelModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_MAPPING
						);


				/** 缓存模块 MemachedModelController end*/
				
				
				/** 文件管理模块 begin*/
				
				UrlStaticMapping fileModule = addModule(KYLINBOY_FILE_MODEL_MODULE_ID,KYLINBOY_FILE_MODEL_MODULE_NAME,KYLINBOY_FILE_MODEL_MODULE_CODE);
				/** 文件类型 FileMimeController begin*/
				//文件类型列表
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_LIST_UI_MAPPING
						);
				//文件类型首页
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_MAPPING
						);
				//文件类型添加页面
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_MAPPING
						);
				//文件类型添加提交
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_ADD_MAPPING
						);
				//文件类型删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_MAPPING
						);



				/** 文件类型 FileMimeController end*/
				
				/** 文件系统分类 FileSystemGalleryController begin*/
				//文件系统分类首页
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_INDEX_UI_MAPPING
						);

				//文件类型列表
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING
						);

				//文件类型添加
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING
						);
				
				//文件类型添加提交
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_MAPPING
						);
				//文件类型删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING
						);


				/** 文件系统分类 FileSystemGalleryController end*/
				/** 文件系统分类类型定义 FileSystemGalleryTypeController begin*/
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_MAPPING
						);
				//系统分类类型添加
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_MAPPING
						);
				//系统分类类型删除
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_MAPPING
						);

				/** 文件系统分类类型定义 FileSystemGalleryTypeController end*/
				/** 文件信息处理 FileInfoController begin*/
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FFILEINFO_UPLOAD,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_MAPPING
						);
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FFILEINFO_UPLOAD_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING
						);
				//文件信息列表
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_MAPPING
						);
				//文件信息管理首页
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_INDEX_UI_MAPPING
						);
				//文件删除提交
				addUrlStaticMapping(
						fileModule,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_ID,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_NAME,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_DESC,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_URL,
						KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING
						);
				
				/** 文件信息处理 FileInfoController end*/
				
				/** 文件管理模块  end*/


		return urlStaticMappingModuleList;
		
	}
	
	
}