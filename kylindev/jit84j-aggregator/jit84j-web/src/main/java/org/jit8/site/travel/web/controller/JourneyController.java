package org.jit8.site.travel.web.controller;

import java.io.IOException;

import javax.annotation.Resource;

import org.jit8.framework.jit84j.web.utils.instance.StaticDataUtil;
import org.jit8.site.travel.biz.business.JourneyBusiness;
import org.jit8.site.travel.constants.KylinboyTravelDeveloperConstant;
import org.jit8.site.travel.persist.domain.Journey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class JourneyController{

	private static final Logger logger = LoggerFactory.getLogger(JourneyController.class);
	
	
	@Resource
	private JourneyBusiness journeyBusiness;
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING)
	public String journeyEditUI(@ModelAttribute("journey") Journey journey,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING + " #########");
		Long id = journey.getId();
		if(id != null && id > 0){
			journey = journeyBusiness.getJourneyById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("journey", journey);
		return "journeyEditUI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING)
	public String journeyEdit2UI(@ModelAttribute("journey") Journey journey,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI_MAPPING + " #########");
		Long id = journey.getId();
		if(id != null && id > 0){
			journey = journeyBusiness.getJourneyById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_2_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("journey", journey);
		return "journeyEdit2UI";
	}
	
	@RequestMapping(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING)
	public String journeyEdit3UI(@ModelAttribute("journey") Journey journey,Model model) throws IOException {
		logger.debug("######### " + KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI_MAPPING + " #########");
		Long id = journey.getId();
		if(id != null && id > 0){
			journey = journeyBusiness.getJourneyById(id);
		}
		model.addAttribute("currentNavigation",StaticDataUtil.getCurrentNavigationByCode(KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_3_UI));
		model.addAttribute("navigationTopList",StaticDataUtil.getNavigationTopList());
		model.addAttribute("journey", journey);
		return "journeyEdit2UI";
	}

	
}
