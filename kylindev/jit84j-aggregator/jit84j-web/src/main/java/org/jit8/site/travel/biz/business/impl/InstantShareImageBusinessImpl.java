package org.jit8.site.travel.biz.business.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.jit8.framework.jit84j.core.web.util.CalendarUtils;
import org.jit8.site.travel.biz.business.InstantShareImageBusiness;
import org.jit8.site.travel.biz.business.InstantShareImageCounterBusiness;
import org.jit8.site.travel.biz.service.InstantShareImageService;
import org.jit8.site.travel.persist.domain.InstantShare;
import org.jit8.site.travel.persist.domain.InstantShareImage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("instantShareImageBusiness")
public class InstantShareImageBusinessImpl implements InstantShareImageBusiness{

	@Resource
	private InstantShareImageService instantShareImageService;
	
	@Resource
	private InstantShareImageCounterBusiness instantShareImageCounterBusiness;
	

	@Override
	public List<InstantShareImage> getInstantShareImageList() {
		return instantShareImageService.getInstantShareImageList(false, false);
	}
	
	@Transactional
	@Override
	public InstantShareImage persist(InstantShareImage instantShareImage){
		Long id = instantShareImage.getId();
		InstantShareImage exist = null;
		if(id != null && id>0){
			 exist = instantShareImageService.findOne(instantShareImage.getId());
			 exist.setDescription(instantShareImage.getDescription());
			 exist.setImageUrl(instantShareImage.getImageUrl());
			 exist.setThumbnailUrl(instantShareImage.getThumbnailUrl());
			 exist.setSequence(instantShareImage.getSequence());
			 exist.setUpdateDate(new Date());
		}else{
			exist=instantShareImage;
		}
		
		return instantShareImageService.save(exist);
	}

	@Override
	public InstantShareImage getInstantShareImageById(Long id) {
		return instantShareImageService.findOne(id);
	}

	@Override
	public InstantShareImage deleteInstantShareImageById(Long id) {
		InstantShareImage old = instantShareImageService.findOne(id);
		old.setDeleted(true);
		return instantShareImageService.save(old);
	}
	
	@Override
	public void removeById(Long id) {
		
		if(null != id && id>0){
			instantShareImageService.delete(id);
		}
	}

	@Override
	public Page<InstantShareImage> findAll(Pageable pageable) {
		return instantShareImageService.findAll(pageable);
	}

	@Override
	public InstantShareImage findByCode(String code) {
		return instantShareImageService.findByCode( code);
	}

	@Override
	public List<InstantShareImage> findByIds(List<Long> idList) {
		return instantShareImageService.findByIds(idList);
	}

	@Transactional
	@Override
	public void removeByIdAndUserId(Long id, Long userId) {
		if(null != id && id>0 && null != userId && userId>0 ){
			InstantShareImage image = instantShareImageService.findOne(id);
			Long existUserId = image.getUserId();
			if(userId.equals(existUserId)){
				InstantShare instantShare = image.getInstantShare();
				Integer imageCount = instantShare.getImageCount();
				if(null != imageCount && imageCount>0){
					imageCount--;
					instantShare.setImageCount(imageCount);
					updateImageCounter(instantShare.getPublishDate(), userId, -1);
				}
				instantShareImageService.delete(id);
			}
			/*instantShareImageService.removeByIdAndUserId(id, userId);*/
		}
	}
	
	/**
	 * @param publishDate
	 * @param userId
	 * @param minus
	 */
	private void updateImageCounter(Date publishDate, Long userId, int minus) {
		instantShareImageCounterBusiness.addCountByDayAndUserId(minus,publishDate, userId);
	}

	@Override
	public Page<InstantShareImage> findAll(InstantShareImage instantShareImage, Pageable pageable) {
		Page<InstantShareImage> page = null;
		Long userId = instantShareImage.getUserId();
		if(null != userId){
			page = instantShareImageService.findAll(instantShareImage, pageable);
		}
		return page;
	}
	
	public Page<InstantShareImage> findByDayAndUserId(InstantShareImage instantShareImage, Pageable pageable) {
		Page<InstantShareImage> page = null;
		Long userId = instantShareImage.getUserId();
		if(null != userId){
			String yearMonthDay = instantShareImage.getYearMonthDay();
			if (StringUtils.isNotEmpty(yearMonthDay)) {
				Date minDate = CalendarUtils.getSpecificDateBegin(yearMonthDay, null);
				Date maxDate = CalendarUtils.getSpecificDateEnd(yearMonthDay, null);
				instantShareImage.setMinPublishDate(minDate);
				instantShareImage.setMaxPublishDate(maxDate);
				page = findAll(instantShareImage, pageable);
			}
		}
		return page;
	}

	@Override
	public Page<InstantShareImage> findByInstantShareId(Long instantShareId, Pageable pageable) {
		return instantShareImageService.findByInstantShareId(instantShareId, pageable);
	}

}
