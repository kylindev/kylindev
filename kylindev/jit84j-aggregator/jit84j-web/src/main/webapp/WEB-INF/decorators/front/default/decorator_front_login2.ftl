[#ftl]
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
 <!-- Bootstrap -->
    <link href="/assets/common/css/bootstrap/bootstrap_min.css" rel="stylesheet" media="screen">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
<script src="/assets/common/js/jquery/jquery.js"></script>
<script src="/assets/common/js/bootstrap/bootstrap.min.js"></script>
[#include "definefunction.ftl"]
</head>
<body>
<div class="container">
	<sitemesh:write property="div.top_login"/>	
</div>
<div class="container">
	  <sitemesh:write property="div.top_banner"/>
</div>
<div class="container">
	  <sitemesh:write property="div.top_menu"/>
</div>
<div class="container">
	<sitemesh:write property="div.bottom"/>	
</div>
[#include "footer.ftl"]
</body>
</html>