[#ftl]
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
[#include "definefunction.ftl"]
</head>
<body>
<div class="container">
<div class="row">
  <sitemesh:write property="body"/>
</div>
</div>


</body>
</html>