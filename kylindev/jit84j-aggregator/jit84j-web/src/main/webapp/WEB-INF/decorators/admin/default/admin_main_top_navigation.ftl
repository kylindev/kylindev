[#ftl]
<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<div class="container admin_main_top_navigation" id="admin_main_top_navigation">
	<div class="row">
	<nav class="navbar navbar-default" role="navigation">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	    <ul class="nav navbar-nav">
	   <@spring.bind "navigationList" />  
	   <#if (navigationList??)>
	   	<#list navigationList as navigation> 
	   	  <#if !(navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0)>
	      	 <li ><a href="${navigation.url}">${navigation.name}</a></li>
	      </#if>
	      <#if navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0>
	      	<li class="dropdown">
	      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">${navigation.name}
	      	<b class="caret"></b></a>
	      	<ul class="dropdown-menu">
	      	<#list navigation.childrenNavigation as navigation2> 
	      		<li><a href="${navigation2.url}">${navigation2.name}</a></li>
	      	</#list>
	      	</ul>
	      	</li>
	      </#if>
	     </#list>
	    </#if>
	    </ul>
	    <#--
	    <form class="navbar-form navbar-right" role="search">
	      <div class="form-group">
	        <input type="text" class="form-control" placeholder="Search">
	      </div>
	      <button type="submit" class="btn btn-default">Submit</button>
	    </form>
	    -->
	  </div>
	</nav>
	</div>
</div>
