<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_specialArea.ftl">
<#include "travel/definefunction_manifesto.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1771902817/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent" autoPlay="true"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary">进入我的管理区</a> <@loginAndLogout/></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>
</div>

<div class="container" id="body_content">
	<br/>
	<div id="body_content_main_menu"><!--主菜单按钮区-->
		<sitemesh:write property="div.body_content_main_menu"/>
	</div>
	<hr/>
	
	<div class="container" id="body_content_main_search"> <!--主菜单条件搜索查询区-->
		<sitemesh:write property="div.body_content_main_search"/>
	</div>

	<div class="container text-center" id="body_content_main_screen"><!--主屏展示区-->
		<sitemesh:write property="div.body_content_main_screen"/>
	</div>
	
</div>

<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>