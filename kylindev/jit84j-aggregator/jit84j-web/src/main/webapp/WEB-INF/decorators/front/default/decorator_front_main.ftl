[#ftl]
<!doctype html>
<html lang="zh-CN" ng-app>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
 <!-- Bootstrap -->
<title><sitemesh:write property="title"/></title>
<link rel="stylesheet" href="/assets/common/css/bootstrap/3.1.1/css/bootstrap.min.css">
<sitemesh:write property="head"/>
<!--
<link href="/assets/common/css/bootstrap/bootstrap_min.css" rel="stylesheet" media="screen">
<script src="/assets/common/js/jquery/jquery.js"></script>

<script src="/assets/common/js/bootstrap/bootstrap.min.js"></script>
-->
<script src="/assets/common/js/jquery/1.11.0/jquery-1.11.0.min.js"></script>
<script src="/assets/common/css/bootstrap/3.1.1/js/bootstrap.min.js"></script>

<script src="/assets/default/js/site/jit84j_core.js"></script>
<!--
<script src="/assets/common/js/angular/1.2.16/angular.min.js"></script>
-->
[#include "definefunction.ftl"]
</head>
<body>
<div class="container" id="top_login">
	  <sitemesh:write property="div.top_login"/>
</div>

<div class="container">
	  <sitemesh:write property="div.top_banner"/>
</div>
<div class="container">
	  <sitemesh:write property="div.top_menu"/>
</div>
<div class="container">
	<sitemesh:write property="div.body_content"/>	
</div>

<div class="container">
	<sitemesh:write property="div.body_bottom"/>	
</div>

<div class="container">
	<sitemesh:write property="div.body_footer"/>	
</div>
</body>
</html>