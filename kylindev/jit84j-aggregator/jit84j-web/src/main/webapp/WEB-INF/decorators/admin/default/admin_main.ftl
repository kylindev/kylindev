[#ftl]
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
 <!-- Bootstrap -->
<link href="/assets/common/css/bootstrap/bootstrap_min.css" rel="stylesheet" media="screen">
<title>JIT8.org 就他吧管理后台--独家记忆</title>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/assets/common/js/jquery/jquery.js"></script>
<script src="/assets/default/js/site/jit84j_core.js?t=192"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/common/js/bootstrap/bootstrap.min.js"></script>
<script src="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>
<script src="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"></script>
<link href="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">

<script src="/assets/common/tools/ckeditor/ckeditor.js"></script>
<script src="/assets/common/tools/ckeditor/adapters/jquery.js"></script>

</head>
<body>
<div class="container">
<sitemesh:write property="body"/>
</div>
</body>
</html>