<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1772903315/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary" href="#">新留言【<span class="text-danger">19</span>】条</a> <a class="text-primary"  href="#">网站首页</a> <@loginAndLogout /></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<#--<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>-->
</div>

<div class="container" id="body_content">
	<br/>
	
	<div class="row">	
		<div class="col-md-2">
			<button class="btn btn-info">我的管理区</button>
		</div>
		<div class="col-md-10" style="padding: 6px">
			<sitemesh:write property="div.body_content_opration"/>
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-2">
			<sitemesh:write property="div.body_content_navigation"/>
		</div>
		<div class="col-md-10" style="padding: 6px">
			<sitemesh:write property="div.body_content_detail"/>
		</div>
	</div>
	
</div>



<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			
		</div>
	</div>
</div>

<div id="uploadModalLabel2ContentBody">
	<sitemesh:write property="div.uploadModalLabel2ContentBody"/>
</div>
<div class="container" id="body_footer">

</div>
</body>
</html>