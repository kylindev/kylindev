<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_specialArea.ftl">
<#include "travel/definefunction_manifesto.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div id="body_content_main_screen_first">
	<!--主屏首元素展示一区-->
	<!--
	<div class="row" >
		<div class="col-xs-2">
			<span class="text-info">五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span> 
			<br/>
			关注人次： <span class="text-danger">1234</span> 
			<br/>
			最后关注 <span class="text-danger">by</span> <span class="text-primary">在天边上</span> 
		</div>
		<div class="col-xs-8">
				<div style="width:665px;height:150px;background-color:#33CCFF">
					<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='665' height='150' alt="..."/>
				</div>
		</div>
		<div class="col-xs-2">
			<span class="text-info">晚上在松山湖散步很舒适，风清月圆</span> <span class="text-danger">reply by</span> <span class="text-primary">rose丿悲歌</span> 
			<br/>
			<span class="text-info">好久没去了，看了之后更加想念 </span> <span class="text-danger">reply by</span> <span class="text-primary"> 天秤奇</span> 
		</div>
		
	</div>
	-->
		<sitemesh:write property="div.body_content_main_screen_first"/>
</div>

<div id="body_content_main_screen_second"><!--主屏元素展示二区-->
	<sitemesh:write property="div.body_content_main_screen_second"/>
	<!-- 详细页面 广告位 begin-->
	<br/>
	<div class="row" >
		<div class="col-xs-12">
			<img src="/assets/data/upload/files/1/image/photo/2014/07/16/Snip20140602_35.png" width='1100px' height='150px'/>
		</div>
	</div>
	<!-- 详细页面 广告位 end-->
</div>

</body>
</html>

