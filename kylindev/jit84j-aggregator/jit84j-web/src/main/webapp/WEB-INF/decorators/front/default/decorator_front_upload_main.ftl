<!DOCTYPE HTML>
<!--
/*
 * jQuery File Upload Plugin AngularJS Demo 2.1.2
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */
-->
<html lang="zh-CN">
<head>
<!-- Force latest IE rendering engine or ChromeFrame if installed -->
<!--[if IE]>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<![endif]-->
<meta charset="utf-8">
<title><sitemesh:write property="title"/></title>
<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bars, validation and preview images, audio and video for AngularJS. Supports cross-domain, chunked and resumable file uploads and client-side image resizing. Works with any server-side platform (PHP, Python, Ruby on Rails, Java, Node.js, Go etc.) that supports standard HTML form file uploads.">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Bootstrap styles -->
<!--<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">-->
<link rel="stylesheet" href="/assets/common/css/bootstrap/3.2.0/css/bootstrap.min.css">
<!-- Generic page styles -->
<!--<link rel="stylesheet" href="/assets/fileupload/css/style.css">-->
<!-- blueimp Gallery styles -->
<!--<link rel="stylesheet" href="http://blueimp.github.io/Gallery/css/blueimp-gallery.min.css">-->
<link rel="stylesheet" href="/assets/common/tools/Gallery/css/blueimp-gallery.min.css">
<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="/assets/fileupload/css/jquery.fileupload.css">
<link rel="stylesheet" href="/assets/fileupload/css/jquery.fileupload-ui.css">

<link href="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.css" rel="stylesheet" media="screen">

<!-- CSS adjustments for browsers with JavaScript disabled -->
<noscript><link rel="stylesheet" href="/assets/fileupload/css/jquery.fileupload-noscript.css"></noscript>
<noscript><link rel="stylesheet" href="/assets/fileupload/css/jquery.fileupload-ui-noscript.css"></noscript>
<script src="/assets/common/js/jquery/1.11.0/jquery-1.11.0.min.js"></script>
<script src="/assets/common/js/jquery/citySelect/jquery.cityselect.js"></script>

<script src="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.js"></script>
<script src="/assets/common/js/bootstrap/bootstrap-datetimepicker-master/js/locales/bootstrap-datetimepicker.zh-CN.js" charset="UTF-8"></script>


<script type="text/javascript">
var remote_ip_info = {};
</script>
<script type="text/javascript" src="http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js"></script>

<style>
/* Hide Angular JS elements before initializing */
.ng-cloak {
    display: none;
}
</style>
<sitemesh:write property="head"/>
</head>
<body>
<div >



<div class="container" id="top_login">
	  <sitemesh:write property="div.top_login"/>
</div>

<div class="container">
	  <sitemesh:write property="div.top_banner"/>
</div>
<div class="container">
	  <sitemesh:write property="div.top_menu"/>
</div>
<div class="container">
	<sitemesh:write property="div.body_content"/>
	
	<#--
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#uploadModal">
			  上传图片
			</button>
		</div>
	</div>	
	-->
</div>

<div class="container">
	<sitemesh:write property="div.body_bottom"/>	
</div>

<div class="container">
		<div class="row">
			<div class="col-md-12">
				<!-- Modal -->
				<div class="modal fade" id="uploadModal" tabindex="-1" role="dialog" aria-labelledby="uploadModalLabel" aria-hidden="true">
				  <div class="modal-dialog" style="width:800px;height=500px" draggable="true">
				    <div class="modal-content" >
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="uploadModalLabel">图片上传</h4>
				      </div>
				      <div class="modal-body">
						    <!-- The file upload form used as target for the file upload widget -->
						    <form id="fileupload" action="${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_MAPPING}" method="POST" enctype="multipart/form-data" data-ng-app="demo" data-ng-controller="DemoFileUploadController" data-file-upload="options" data-ng-class="{'fileupload-processing': processing() || loadingFiles}">
						        <!-- Redirect browsers with JavaScript disabled to the origin page -->
						        <noscript><input type="hidden" name="redirect" value="http://blueimp.github.io/jQuery-File-Upload/"></noscript>
						        <input type="hidden" value="${batch}" name="batch" id="fileInfo_batch_id"/>
						        
						        <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
						        <div class="row fileupload-buttonbar">
						            <div class="col-lg-7">
						                <!-- The fileinput-button span is used to style the file input field as button -->
						                <span class="btn btn-success fileinput-button" ng-class="{disabled: disabled}">
						                    <i class="glyphicon glyphicon-plus"></i>
						                    <span>添加...</span>
						                    <input type="file" name="files" multiple ng-disabled="disabled">
						                </span>
						                <button type="button" class="btn btn-primary start" data-ng-click="submit()">
						                    <i class="glyphicon glyphicon-upload"></i>
						                    <span>上传</span>
						                </button>
						                <button type="button" class="btn btn-warning cancel" data-ng-click="cancel()">
						                    <i class="glyphicon glyphicon-ban-circle"></i>
						                    <span>取消</span>
						                </button>
						                
						                <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				        				
						                <!-- The global file processing state -->
						                <span class="fileupload-process"></span>
						            </div>
						            <!-- The global progress state -->
						            <div class="col-lg-5 fade" data-ng-class="{in: active()}">
						                <!-- The global progress bar -->
						                <div class="progress progress-striped active" data-file-upload-progress="progress()"><div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}"></div></div>
						                <!-- The extended global progress state -->
						                <div class="progress-extended">&nbsp;</div>
						            </div>
						        </div>
						        <!-- The table listing the files available for upload/download -->
						        <table class="table table-striped files ng-cloak" id="table_content">
						            <tr data-ng-repeat="file in queue" data-ng-class="{'processing': file.$processing()}">
						                <td data-ng-switch data-on="!!file.thumbnailUrl">
						                    <div class="preview" data-ng-switch-when="true">
						                        <a data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery><img data-ng-src="{{file.thumbnailUrl}}" alt="" width="80px"></a>
						                    </div>
						                    <div class="preview" data-ng-switch-default data-file-upload-preview="file"></div>
						                </td>
						                <td>
						                    <p class="name" data-ng-switch data-on="!!file.url">
						                        <span data-ng-switch-when="true" data-ng-switch data-on="!!file.thumbnailUrl">
						                            <a data-ng-switch-when="true" data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}" data-gallery>{{file.name}}</a>
						                            <a data-ng-switch-default data-ng-href="{{file.url}}" title="{{file.name}}" download="{{file.name}}">{{file.name}}</a>
						                        </span>
						                        <span data-ng-switch-default>{{file.name}}</span>
						                    </p>
						                   <strong data-ng-show="file.success" class="success text-success">{{file.success}}</strong>
						                    <strong data-ng-show="file.error" class="error text-danger">{{file.error}}</strong>
						                </td>
						                <td>
						                    <p class="size">{{file.size | formatFileSize}}</p>
						                    <div class="progress progress-striped active fade" data-ng-class="{pending: 'in'}[file.$state()]" data-file-upload-progress="file.$progress()"><div class="progress-bar progress-bar-success" data-ng-style="{width: num + '%'}"></div></div>
						                </td>
						                <td>
						                    <button type="button" class="btn btn-primary start" data-ng-click="file.$submit()" data-ng-hide="!file.$submit || options.autoUpload" data-ng-disabled="file.$state() == 'pending' || file.$state() == 'rejected'">
						                        <i class="glyphicon glyphicon-upload"></i>
						                        <span>上传</span>
						                    </button>
						                    <button type="button" class="btn btn-warning cancel" data-ng-click="file.$cancel()" data-ng-hide="!file.$cancel">
						                        <i class="glyphicon glyphicon-ban-circle"></i>
						                        <span>取消</span>
						                    </button>
						                    <button data-ng-controller="FileDestroyController" type="button" class="btn btn-danger destroy" data-ng-click="file.$destroy()" data-ng-hide="!file.$destroy">
						                        <i class="glyphicon glyphicon-trash"></i>
						                        <span>删除</span>
						                    </button>
						                    <button  type="button" class="btn btn-primary start" data-ng-hide="!file.$destroy" onclick="copyFileFromBack(this)">
						                        <i class="glyphicon glyphicon-plus"></i><span class="hide">{{file.id}}</span>
						                        <span>加入</span>
						                    </button>
						                </td>
						            </tr>
						        </table>
						    </form>
						    <br>
						    <div class="panel panel-default">
						        <div class="panel-heading">
						            <h3 class="panel-title">提醒：</h3>
						        </div>
						        <div class="panel-body">
						            <ul>
						                <li>最大允许上传<strong>5 MB</strong></li>
						                <li>允许图片格式 (<strong>JPG, GIF, PNG</strong>) </li>
						            </ul>
						        </div>
						    </div>
						</div>
						<!-- The blueimp Gallery widget -->
						<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
						    <div class="slides"></div>
						    <h3 class="title"></h3>
						    <a class="prev">‹</a>
						    <a class="next">›</a>
						    <a class="close">×</a>
						    <a class="play-pause"></a>
						    <ol class="indicator"></ol>
						</div>
				      </div>
				      <#--
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				        <button type="button" class="btn btn-primary" onclick="saveImage()">保存</button>
				       -->
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!-- /.column -->
		</div><!-- /.row -->
		
		
		<div class="row">
			<div class="col-md-12">
				<!-- Modal -->
				<div class="modal fade" id="uploadModal2" tabindex="-2" role="dialog" aria-labelledby="uploadModalLabel2" aria-hidden="true">
				  <div class="modal-dialog" style="width:970px;height=900px" draggable="true">
				    <div class="modal-content" >
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <ul class="list-inline">
					        <li><h4 class="modal-title" id="uploadModalLabel2">图片选择</h4></li>
					        <li><button type="button" class="btn btn-default" data-dismiss="modal">关闭</button></li>
					       <#-- <li><button type="button" class="btn btn-primary" onclick="saveImage()">保存</button></li> -->
				        </ul>
				      </div>
				      <div class="modal-body" id="uploadModalLabel2ContentBody">
						    <sitemesh:write property="div.uploadModalLabel2ContentBody"/>	
						</div>
						<!-- The blueimp Gallery widget -->
						<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
						    <div class="slides"></div>
						    <h3 class="title"></h3>
						    <a class="prev">‹</a>
						    <a class="next">›</a>
						    <a class="close">×</a>
						    <a class="play-pause"></a>
						    <ol class="indicator"></ol>
						</div>
				      </div>
				     
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				        <#--<button type="button" class="btn btn-primary" onclick="saveImage()">保存</button>-->
				       
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
			</div><!-- /.column -->
		</div><!-- /.row -->
		
		
		<script src="/assets/common/js/angular/angular/angular.min.js"></script>
		
		<!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
		<script src="/assets/fileupload/js/vendor/jquery.ui.widget.js"></script>
		<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
		<!--<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>-->
		<script src="/assets/common/tools/JavaScript-Load-Image/js/load-image.min.js"></script>
		<!-- The Canvas to Blob plugin is included for image resizing functionality -->
		<!--<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>-->
		<script src="/assets/common/tools/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
		<!-- Bootstrap JS is not required, but included for the responsive demo navigation -->
		<!--<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>-->
		<script src="/assets/common/css/bootstrap/3.2.0/js/bootstrap.min.js"></script>
		<!-- blueimp Gallery script -->
		<!-- <script src="http://blueimp.github.io/Gallery/js/jquery.blueimp-gallery.min.js"></script>-->
		<script src="/assets/common/tools/Gallery/js/jquery.blueimp-gallery.min.js"></script>
		<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
		<script src="/assets/fileupload/js/jquery.iframe-transport.js"></script>
		<!-- The basic File Upload plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload.js"></script>
		<!-- The File Upload processing plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload-process.js"></script>
		<!-- The File Upload image preview & resize plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload-image.js"></script>
		<!-- The File Upload audio preview plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload-audio.js"></script>
		<!-- The File Upload video preview plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload-video.js"></script>
		<!-- The File Upload validation plugin -->
		<script src="/assets/fileupload/js/jquery.fileupload-validate.js"></script>
		<!-- The File Upload Angular JS module -->
		<script src="/assets/fileupload/js/jquery.fileupload-angular.js"></script>
		<!-- The main application script -->
		<script src="/assets/fileupload/js/app.js"></script>
		
		<script src="/assets/default/js/site/jit84j_core.js"></script>
		
		
</div>
<div class="container">
<sitemesh:write property="div.body_footer"/>
</div>
<script type="text/javascript">
function saveImage(){
	$("#image_content").html($("#table_content").html());
}

</script>
</body> 
</html>
