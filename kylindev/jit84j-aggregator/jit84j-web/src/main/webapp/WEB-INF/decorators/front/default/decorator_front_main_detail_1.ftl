<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_specialArea.ftl">
<#include "travel/definefunction_manifesto.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

	<div id="body_content_main_menu"><!--主菜单按钮区-->
		<div class="row">	
			<div class="col-md-1">
				<button class="btn btn-info">今天</button>
			</div>
			<div class="col-md-1 text-center" style="padding: 6px">
				<span class="glyphicon glyphicon-chevron-right text-info">
				<span class="glyphicon glyphicon-chevron-right text-info">
			</div>
			<div class="col-md-10" style="padding: 6px">
				<ul class="list-inline">
			   		<li><button type="button" class="btn btn-xs btn-primary"> 行程活动精推版 进行中...</button></li>
			   		<li><button type="button" class="btn btn-xs btn-primary">【散兵】自由行 进行中...</button></li>
			   		<li><button type="button" class="btn btn-xs btn-primary">【队伍】协作 进行中...</button></li>
			   		<li><button type="button" class="btn btn-xs btn-primary"> 今日宣言</button></li>
			   		<li><button type="button" class="btn btn-xs btn-success"> 即时分享</button></li>
			   		<li><button type="button" class="btn btn-xs btn-primary">今日关注</button></li>
			   		<li><button type="button" class="btn btn-xs btn-primary">注册宣言</button></li>
			   	</ul>
			</div>
		</div>
		
		<div class="row">	
			<div class="col-md-1">
					<h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 活动</h4>
			</div>
			<div class="col-md-1 text-center"  style="padding: 10px">
					<span class="glyphicon glyphicon-minus text-info">
					<span class="glyphicon glyphicon-minus text-info">
				</div>
			<div class="col-md-10" style="padding: 10px">
				  <button type="button" class="btn btn-xs btn-primary">正在进行</button>
				  <button type="button" class="btn btn-xs btn-primary">室内活动</button>
				  <button type="button" class="btn btn-xs btn-primary">户外活动</button>
				  <button type="button" class="btn btn-xs btn-primary">活动关注排行榜</button>
			</div>
		</div>
		
		<div class="row">	
			<div class="col-md-1 ">
					<h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程</h4>
			</div>
			<div class="col-md-1 text-center" style="padding: 10px">
					<span class="glyphicon glyphicon-minus text-info">
					<span class="glyphicon glyphicon-minus text-info">
				</div>
			<div class="col-md-10" style="padding: 10px">
				<button type="button" class="btn btn-xs btn-primary">说走就走</button>
				<button type="button" class="btn btn-xs btn-primary">边行边记</button>
				<button type="button" class="btn btn-xs btn-primary">旅行达人行程同步</button>
				<button type="button" class="btn btn-xs btn-primary">行程宣言</button>
			</div>
		</div>
	</div>
	<hr/>
<#--搜索查询 begin TODO-->	
<!--主菜单条件搜索查询区-->
<#--
<div class="container" id="body_content_main_search">
	<form class="form" id="form_id_manifesto" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_LIST_UI_MAPPING}" method="post">
			<div class="row">	
				<div class="col-xs-2">
					<h4 class="text-danger text-center">过滤条件:</h4>
				</div>
				<div class="col-xs-2">
				    <input type="text" name="decription" value="<#if manifesto??>${manifesto.decription}</#if>"  class="form-control"  placeholder="昵称"/>
				</div>
				<div class="col-xs-2">
					<input type="text" name="nickName" value="<#if manifesto??>${manifesto.nickName}</#if>"  class="form-control" placeholder="标题关键字"/>
				</div>
				<div class="col-xs-2">
					<input type="text" name="minPublishTime" value="<#if manifesto??>${manifesto.minPublishTime}</#if>"  class="form-control" placeholder="开始日期"/>
				</div>
				<div class="col-xs-2">
					<input type="text" name="maxPublishTime" value="<#if manifesto??>${manifesto.maxPublishTime}</#if>"  class="form-control" placeholder="结束日期"/>
				</div>
				<div class="col-xs-2">
					<button class="btn btn-primary" onClick="formSubmit(1)">搜索</button>
				</div>
			</div>
	</form>	
</div>
-->
<#--搜索查询 end TODO-->	
<div class="container text-center" id="body_content_main_screen"><!--主屏展示区-->	
		<sitemesh:write property="div.body_content_main_screen_first"/>
		<!--主屏列表元素展示区-->
		<sitemesh:write property="div.body_content_main_screen_second"/>
</div>	

<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>

