
<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_user.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
</head>
<body>

<div class="container" id="top_login">
	<@loginAndLogout />
</div>

<div class="container" id="top_banner">
	<@commonBanner/>
</div>

<div class="container" id="top_menu">
	<#--
	<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>
	-->
</div>

<div class="container" id="body_content">
	<sitemesh:write property="div.body_content"/>	
</div>

<div id="body_bottom">
	<sitemesh:write property="div.body_bottom"/>
</div>
<div class="container" id="body_footer">
	<@commonFooter/>
</div>
</body>
</html>