<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<div class="container admin_main_top_header" id="admin_main_top_header">
	<div class="nav-justified">
	<sitemesh:write property="div.admin_main_top_login_logout"/>
	</div>
</div>
<title><sitemesh:write property="title"/></title>

<div class="container">

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<div class="container">
	<sitemesh:write property="div.body_content"/>	
</div>

<div class="container">
	<sitemesh:write property="div.body_bottom"/>	
</div>


<div class="container" id="body_footer">
	<@commonFooter/>
</div>

<sitemesh:write property="div.admin_main_top_navigation"/>
<sitemesh:write property="div.body_id"/>
<sitemesh:write property="div.footer"/>
</div>
