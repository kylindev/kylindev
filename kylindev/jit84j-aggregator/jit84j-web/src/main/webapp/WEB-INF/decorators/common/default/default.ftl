[#ftl]
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
 <!-- Bootstrap -->
    <link href="/assets/common/css/bootstrap/bootstrap_min.css" rel="stylesheet" media="screen">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/assets/common/js/jquery/jquery.js"></script>


<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/common/js/bootstrap/bootstrap.min.js"></script>
[#include "definefunction.ftl"]
</head>
<body>

<div class="container">
	<h3> Welcome ${name} to jit8.org, jit8 <small>means</small> 就他吧！</h3>
	<p class="text-primary">Simple is perfect!</p>
	<p class="text-primary"><!--我喜欢旅行，也喜欢分享，一路行程，独家记忆，我怀着真诚地心而来，若我的脚步无法为你停留，只因你未曾在乎过我... 希望我的旅程与感受带给你们快乐，走到那里会有一个素昧蒙面的朋友在等待......--></p>
	<h4>致于于方便灵活的架构构建，快捷高效，降低开发难度，高速访问，自由式管理，兴趣为先，享受分享的快乐，将资源价值化，将人脉资产化，发挥优势，让更多的人受益，<span class="label label-success">诚信牌照</span> 发散思维</h4>
	<!--<h4>每个人都有一个属于自己的品牌，这便是贯穿你生命历程的整个人生，将自己记录，寄托希望与心愿，便是积累力量，不仅仅是分享快乐与资源，而且将你的资源价值化，将你的人脉资产化，慢慢传播影响力，发挥你的优势，让更多的人受益，这是你一生的品牌，用你的<span class="label label-success">诚信牌照</span>经营好自己的网络人生.....</h4>-->
	
</div>
<div class="container">
<div class="row">
  <sitemesh:write property="body"/>
</div>
</div>


[#include "footer.ftl"]
</body>
</html>