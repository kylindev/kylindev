
<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_user.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><sitemesh:write property="title"/></title>
<sitemesh:write property="head"/>
</head>
<body>

<div class="container" id="top_login">
	<sitemesh:write property="div.top_login"/>
</div>

<div class="container" id="top_banner">
	<sitemesh:write property="div.top_banner"/>
</div>

<div class="container" id="top_menu">
	 <#--<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>-->
</div>

<div class="container" id="body_content">
	<sitemesh:write property="div.body_content"/>	
</div>

<div id="body_bottom">
	<sitemesh:write property="div.body_bottom"/>
</div>

<div id="uploadModalLabel2ContentBody">
	
	<div class="input-append date form_datetime" data-date='${CalendarUtils.getCurrentYearMonthWithFormat("yyyy-MM")}'>
	    <input size="16" type="text" value="${CalendarUtils.getCurrentYearMonthWithFormat("yyyy-MM")}" placeholder="选择月份" readonly/>
	    <span class="add-on"><i class="icon-th"></i></span>
	    <button class="btn btn-primary btn-xs" onclick="getCalendarDay('mirror_yearMonth','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_COUNTER_LIST_UI_MAPPING}','calandar_day_container')"> 检索 </button>
	    <span>请按日历选择有照片(数目>0)的日子</span>
	</div>
	<input type="hidden" data="data" id="mirror_yearMonth" value="${CalendarUtils.getCurrentYearMonth()}" readonly name="yearMonth"/>
	
	<div id="calandar_day_container">
		<sitemesh:write property="div.uploadModalLabel2ContentBody"/>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			$(".form_datetime").datetimepicker({
		        format: "yyyy-MM",
		        linkField: "mirror_yearMonth",
	        	linkFormat: "yyyyMM",
		        autoclose: true,
		        startDate: "2013-10-14 11:11",
		        minuteStep: 60,
		        startView:3,
		        minView:3,
		        rtl: true
		    });
		});
		
	    function getCalendarDay(dataId,url,containerId){
	    	var _dataId = "#" + dataId;
	    	var _dataValue = $(_dataId).val();
	    	var request={"yearMonth" : _dataValue};
	    	getContent(url, request,containerId);
	    }
	</script>
</div>



<div class="container" id="body_footer">
	<sitemesh:write property="div.body_footer"/>
	<@commonFooter/>
</div>
</body>
</html>