<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_specialArea.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1771902817/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent" autoPlay="true"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary">进入我的管理区</a> <@loginAndLogout /></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/><#---->
</div>

<div class="container" id="body_content">
	<br/>
	
	<div class="row">	
		<div class="col-md-1">
			<button class="btn btn-info">明天之后</button>
		</div>
		<div class="col-md-1 text-center" style="padding: 6px">
			<span class="glyphicon glyphicon-chevron-right text-info">
			<span class="glyphicon glyphicon-chevron-right text-info">
		</div>
		<div class="col-md-10" style="padding: 6px">
			<ul class="list-inline">
		   		<li> <button type="button" class="btn btn-xs btn-success">活动行程计划精推版</button></li>
		   		<li><button type="button" class="btn btn-xs btn-primary">【散兵】自由行计划</button></li>
		   		<li><button type="button" class="btn btn-xs btn-primary">【队伍】协作计划</button></li>
		   	</ul>
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-1">
				<h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 活动</h4>
			
		</div>
		<div class="col-md-1 text-center"  style="padding: 10px">
				<span class="glyphicon glyphicon-minus text-info">
				<span class="glyphicon glyphicon-minus text-info">
			</div>
		<div class="col-md-10" style="padding: 10px">
			  <button type="button" class="btn btn-xs btn-primary">招募中的活动</button>
			  <button type="button" class="btn btn-xs btn-primary">室内活动</button>
			  <button type="button" class="btn btn-xs btn-primary">户外活动</button>
			  <button type="button" class="btn btn-xs btn-primary">活动关注排行榜</button>
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-1 ">
				<h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程</h4>
		</div>
		<div class="col-md-1 text-center" style="padding: 10px">
				<span class="glyphicon glyphicon-minus text-info">
				<span class="glyphicon glyphicon-minus text-info">
			</div>
		<div class="col-md-10" style="padding: 10px">
			<button type="button" class="btn btn-xs btn-primary">结伴出行计划</button>
			<button type="button" class="btn btn-xs btn-primary">旅行达人行程</button>
			<button type="button" class="btn btn-xs btn-primary">行程关注排行榜</button>
			<button type="button" class="btn btn-xs btn-primary">【明天】就出发</button>
			<button type="button" class="btn btn-xs btn-primary">即将开始的旅行</button>
			<button type="button" class="btn btn-xs btn-primary">行程宣言</button>
		</div>
	</div>
	<hr/>
	<form class="form">
		<div class="row">	
			<div class="col-xs-2">
				<h4 class="text-danger text-center">过滤条件:</h4>
			</div>
			
			<div class="col-xs-2">
			    <input type="text" name="" value=""  class="form-control"  placeholder="昵称"/>
			</div>
			<div class="col-xs-2">
				<input type="text" name="" value=""  class="form-control" placeholder="标题关键字"/>
			</div>
			<div class="col-xs-2">
				<input type="text" name="" value=""  class="form-control" placeholder="开始日期"/>
			</div>
			<div class="col-xs-2">
				<input type="text" name="" value=""  class="form-control" placeholder="结束日期"/>
			</div>
			<div class="col-xs-2">
				<button class="btn btn-primary">搜索</button>
			</div>
		</div>
	</form>
	
	<div class="container text-center">
		<div class="row">
			<div class="col-xs-3">
				<span class="text-info">五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span> 
				<br/>
				关注人次： <span class="text-danger">1234</span> 
				<br/>
				最后关注 <span class="text-danger">by</span> <span class="text-primary">在天边上</span> 
			</div>
			<div class="col-xs-7">
					<div style="width:600px;height:150px;background-color:#33CCFF">
						<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='600' height='150' alt="..."/>
					</div>
			</div>
			<div class="col-xs-2">
				<span class="text-info">晚上在松山湖散步很舒适，风清月圆</span> <span class="text-danger">reply by</span> <span class="text-primary">rose丿悲歌</span> 
				<br/>
				<span class="text-info">好久没去了，看了之后更加想念 </span> <span class="text-danger">reply by</span> <span class="text-primary"> 天秤奇</span> 
			</div>
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-2">
			<div>
				<img src="http://i2.sinaimg.cn/dy/main/mnews/2014/04/0430/90x90.png" width="180">
				<br/>
				<img src="http://image.songtaste.com/images/usericon/s/92/1988892.jpg" class="icon">
				<img src="http://image.songtaste.com/images/usericon/s/17/3800417.JPG" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/11/3989011.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/56/446456.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/l/20/4523320.jpg" border="0" class="icon">
			</div>
		</div>
		<div class="col-md-8">
			
			<div class="text-center">
							<ul class="media-list">
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">1.五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span> 已被<span class="text-danger">1234</span>人次关注 最后关注 <span class="text-danger">by</span>
									      <span class="text-primary">在天边上</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">2.五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">1224</span>人次关注 最后关注 <span class="text-danger">by</span>
									      <span class="text-primary">神马都是浮云</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">3.五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									         已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小葡萄</span> 
									      </h5>
									       <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">4.07/07 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">@梦@</span> 
									      </h5>
									       <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">5. 06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">6. 10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">江湖浪子</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">7. 10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">江湖浪子</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">8. 10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">江湖浪子</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">9. 10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">江湖浪子</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
									  <li class="media">
									    <a class="pull-left" href="#">
									      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
									    </a>
									    <div class="media-body">
									      <h5 class="media-heading"><span class="text-info">10. 10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">江湖浪子</span> 
									      </h5>
									      <h5><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
									       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
									      	 <span class="text-primary">小榕树</span> </h5>
									    </div>
									  </li>
							</ul>
							<div ><h4><a href="" class="text-warning" title="加载20条"><span class="glyphicon glyphicon-circle-arrow-down"></span></a></h4></div>
				</div>
		</div>
		<div class="col-md-2">
			<!--广告区-->
			<img src="http://image.songtaste.com/images/usericon/s/17/3800417.JPG" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/11/3989011.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/56/446456.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/l/20/4523320.jpg" border="0" class="icon">
		</div>
	</div>
	  

</div>







<#--
<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			<span class="label label-default">Default</span>
			<span class="label label-primary">Primary</span>
			<span class="label label-success">Success</span>
			<span class="label label-info">Info</span>
			<span class="label label-warning">Warning</span>
			<span class="label label-danger">Danger</span>
		</div>
	</div>
</div>
-->
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>