<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_siteInfo.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>网站信息</title>
</head>
<div class="container" id="top_login">
	 <@loginAndLogout />
</div>
<div class="container" id="top_menu">
	<div class="row">
	<@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
	</div>
</div>

<body>
<div class="container body_id" id="body_content">
<div class="row">
<div class="col-md-3">
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		
	  	<#list navigationList as navigation> 
		  	<#if navigation?? >
		  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('${siteInfo.id}','${navigation.id}','${navigation.url}')">
		 		${navigation.name}
		 	</a>
		  	</#if>
	  	</#list>
	 </#if>
</div>

<script type="text/javascript">
function switchSetting(siteInfoId,navigationId,url){
	var listItem = $(".list-group-item");
	var id="left_navigation_"+navigationId;
	if(listItem.length>0){
		$(listItem).each(function(){
			if(id==$(this).attr("id")){
				$(this).addClass("active");
			}else{
				$(this).removeClass("active");
			}
		});
	}
	var request={'id':siteInfoId};
	//alert(request);
	getContent(url,request);
}

function getContent(url,request){
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
					$("#siteInfo_setting_content_div").html(data);
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}			
</script>


</div>
  <div class="col-md-9" id="siteInfo_setting_content_div">
  <@siteInfoEdit siteInfo=siteInfo/>
  </div>
</div>
</div>
</body>
<div class="container" id="body_footer">
	<@commonFooter/> 
</div>
</html>