<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>小狐网络工作室登录</title>
</head>
<div id="banner">
	<h3> <img src="/assets/data/upload/2013/11/13/logodujia.gif"/> Welcome ${name} to jit8.org, jit8 <small>means</small> 就 <span class="text-danger"> <strong>他/她/它</strong></span> 吧！</h3>
	<p class="text-primary">我喜欢旅行，也喜欢分享，一路行程，独家记忆，我怀着真诚地心而来，若我的脚步无法为你停留，只因你未曾在乎过我... 希望我的旅程与感受带给你们快乐，走到那里会有一个素昧蒙面的朋友在等待......</p>
	<h4>每个人都有一个属于自己的品牌，这便是贯穿你生命历程的整个人生，将自己记录，寄托希望与心愿，便是积累力量，不仅仅是分享快乐与资源，而且将你的资源价值化，将你的人脉资产化，慢慢传播影响力，发挥你的优势，让更多的人受益，这是你一生的品牌，用你的<span class="label label-success">诚信牌照</span>经营好自己的网络人生.....</h4>
</div>

<body>
<div class="container">
<@languageBar/>

<span><a href="javascript:void(0)" onclick="changeLanguage('en')">EN</a></span>
|

<span><a href="javascript:void(0)" onclick="changeLanguage('zh')">中文</a></span> 
		<@spring.message "label.menu"/> 
		<@form.form commandName="user" role="form" action="${KylinboyCoreDeveloperConstant.KYLINBOY_ADMIN_LOGIN_MAPPING}">
		<div class="row" >
			<div class="col-md-8">
				   <div class="panel panel-info" >
					<div class="panel-heading ">最新信息</div>
					  <div class="panel-body">
					   <img width="708" height="270" src="http://images2.baihe.com/images/baihe_new/images/skin_index/20131118index_focus_02.jpg" alt="">
					  </div>
					</div>
			  </div>
		
			<div class="col-md-4">
			<div class="panel panel-warning">
				 <div class="panel-heading ">从这里登录</div>
				   <@spring.bind "user.email" /> 
				  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
				    <label class="control-label" for="email_1">Email address:
				    <#if (spring.status.errorMessages?size>0)>
					
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
					
					</#if>
				    </label>
				    <@form.input path="email" class="form-control" id="email_1" placeholder="Enter email"/>
				  </div>	
			  
				  <@spring.bind "user.password" /> 
				  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
				    <label class="control-label" for="password_1">Password:
				    <#if (spring.status.errorMessages?size>0)>
					
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
					
					</#if>
				    </label>
				    <@form.password path="password" class="form-control" id="password_1" placeholder="输入密码"/>
				   </div> 
			    
				    <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
					    <label class="control-label" for="validateCode_1">验证码：
					    <#if (spring.status.errorMessages?size>0)>
						
						  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
						
						</#if>
					    </label>
					    <input type="text" name="validateCode" class="form-control" id="validateCode_1"/>&nbsp;&nbsp;<img id="validateCodeImg" src="/validateCode" />&nbsp;&nbsp;<a href="#" onclick="javascript:reloadValidateCode();">看不清？</a>
				   </div>
				 
				    

					<ul class="list-inline">
					  <li><input type="submit" value="登  录" class="btn btn-warning"/></li>
					  <li><a class="text-danger" href='<@spring.url relativeUrl="/index"/>'>返回首页</a></li>
					</ul>

			      </div>  
			  </div>
			  
			  
		  </div>
	    
	  </@form.form>
	  </div>
  </div>
  
</div>
<script type="text/javascript">
<!--
function reloadValidateCode(){
    $("#validateCodeImg").attr("src","/validateCode?data=" + new Date() + Math.floor(Math.random()*24));
}
//-->
</script>
</body>

<div id="bottom">
${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_MAPPING}
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg">
</div>
</html>