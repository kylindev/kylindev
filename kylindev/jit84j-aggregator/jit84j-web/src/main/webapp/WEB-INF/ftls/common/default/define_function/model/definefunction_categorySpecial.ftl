<#macro categorySpecialIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#categorySpecial_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</#macro>


<#macro categorySpecialCheckboxUI  categorySpecialList categorySpecialCheckedSet name="name">
	<#if categorySpecialList??>
		<ul class="list-inline">
			<#list categorySpecialList as category>  
				<li>
					<label class="checkbox-inline">
					<input type="checkbox" data="data-check-value" id="category_checkbox_${category.id}" value="${category.id}"
					 name="${name}[${category_index}]" 
					 <#if categorySpecialCheckedSet??> 
					 <#list categorySpecialCheckedSet as categorySpecialChecked> 
					 	<#if category.id==categorySpecialChecked.id>
					 		checked
					 	</#if>
					 </#list>
					 </#if>
					> ${category.name}
					</label>
				</li>
			</#list>
		</ul>
	</#if>
</#macro>

<#--categorySpecialEditUI 添加、修改页面 begin-->
<#macro categorySpecialEditUI categorySpecial page>
<div class="row">
		  <div class="col-md-3">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 类别特殊区关系</p>
		  	  
		  	  <form id="categorySpecial_form" action="${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_MAPPING}" method="post">
		  	
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if categorySpecial??>${categorySpecial.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称: 
				</label>
			    <input type="text" data="data" name="name" value="<#if categorySpecial??>${categorySpecial.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="imagePath">图标: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图标路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if categorySpecial??>${categorySpecial.imagePath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if categorySpecial??>${categorySpecial.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if categorySpecial??>${categorySpecial.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="beginTime">开放起始时间: 
				</label>
			    <input type="text" data="data" name="beginTime" value="<#if categorySpecial??>${categorySpecial.beginTime}</#if>" class="form-control" id="beginTime" placeholder="开放起始时间"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="endTime">开放终止时间: 
				</label>
			    <input type="text" data="data" name="endTime" value="<#if categorySpecial??>${categorySpecial.endTime}</#if>" class="form-control" id="endTime" placeholder="开放终止时间"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="openable">是否开放: 
				</label>
			    <input type="checkbox" data="data" name="openable" <#if categorySpecial?? && categorySpecial.openable> checked </#if> value="${categorySpecial.openable}" class="form-control" id="openable"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="openable">允许发表: 
				</label>
			    <input type="checkbox" data="data" name="writable" <#if categorySpecial?? && categorySpecial.writable> checked </#if> value="${categorySpecial.writable}" class="form-control" id="writable"/>
			  </div>
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#categorySpecial_form','#stickyNoteCategory_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#categorySpecial_form')" value='清空'/>
				</li>
		      </ul>
		</div>
		<div class="col-md-9">
		
		<p class="text-primary">类别特殊区关系列表</p>
		<@listCategorySpecialView page=page/>
		</div> 
		<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</div>
</#macro>
<#--categorySpecialEditUI 添加、修改页面 end-->

<#macro listCategorySpecialView page>
<#if page?? && page.content??>
		<#assign categorySpecialList = page.content />
		<form id="categorySpecial_list_view_form" action="${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_UI_MAPPING}" method="post">
	
		 <table class="table">
		 <tr>
		 	<td>
			</td>
		 	<td>
			 <input type="text" value="" name="name" placeholder="输入名称">
			</td>
			 <td>
			 
			 </td>
			 <td>
			 <input type="checkbox" data="data" name="writable" <#if categorySpecial?? && categorySpecial.writable>checked</#if> class="form-control" id="writable${categorySpecial.uniqueIdentifier}" />
			 
			 </td>
			 <td>
			 <input type="checkbox" data="data" name="openable" <#if categorySpecial?? && categorySpecial.openable>checked</#if> class="form-control" id="openable${categorySpecial.uniqueIdentifier}" />
			 </td>
			 <td>
			 
			 </td>
			 <td>
			 
			 </td>
			 <td>
			<button class="btn btn-primary" >搜索</button>
			 </td>
		</tr>
		 <tr>
		 	<th>
			 Id
			</th>
			<th>
			 特殊区
			</th>
		 	<th>
			 名称
			</th>
			 <th>
			 图标
			 </th>
			  <th>
			 允许发表
			 </th>
			 <th>
			 开放
			 </th>
			 <th>
			 排序
			 </th>
			 <th>
			 描述
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		<#list categorySpecialList as categorySpecial>  
			 <tr>
			 <td>
				 ${categorySpecial.id}
				</td>
				<td>
				 ${categorySpecial.specialArea.name}
				</td>
				<td>
				 ${categorySpecial.name}
				</td>
				 <td>
				 ${categorySpecial.imagePath}
				 </td>
				 <td>
			 	 <input type="checkbox" data="data" name="writable" <#if categorySpecial?? && categorySpecial.writable>checked</#if> class="form-control" id="writable${categorySpecial.uniqueIdentifier}" />
				 </td>
				 <td>
			 	 <input type="checkbox" data="data" name="openable" <#if categorySpecial?? && categorySpecial.openable>checked</#if> class="form-control" id="openable${categorySpecial.uniqueIdentifier}" />
				 </td>
				 <td>
				  <input type="text" data="data" name="sequence" value="<#if categorySpecial??>${categorySpecial.sequence}</#if>" class="form-control" id="sequence${categorySpecial.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
				 </td>
				  <td>
				  ${categorySpecial.description}
				 </td>
				 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${categorySpecial.id}" onclick="switchSetting('${categorySpecial.id}','${categorySpecial.id}','${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING}','#stickyNoteCategory_content_div')">编辑</a>
				  	</li>
				    <li>
				    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${categorySpecial.id}','${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_MAPPING}','#stickyNoteCategory_content_div')">删除</a>
				
				    </li>
				  </ul>
			 </td>
			 </tr>
		</#list>
		<tr>
		<td colspan="7">
		<@pageBarAjax page=page url="${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_UI_MAPPING}" formId='categorySpecial_list_view_form' backDataContainerId="stickyNoteCategory_content_div"/>
		</td>
		</tr>
		</table>
		</form>
</#if>
</#macro>

<#-- 绑定 类别 与 特殊区 begin-->
<#macro categorySpecialBindUI categoryPage specialAreaPage categorySpecialPage categorySpecial>
<div class="row">
	  <div class="col-md-12">
	 	  <p class="text-danger">${msg}</p>
	 	  
	 	  <div class="panel panel-warning" id="specialArea_list_div">
			<div class="panel-heading ">特殊区列表 【<#if specialAreaPage??>${specialAreaPage.totalElements}</#if>】 <label class="checkbox-inline"><input type="checkbox" id="specialArea_checkbox_all" value="" onclick="selectAll('specialArea_checkbox_all','specialArea_checkbox_')"> 全选</label></div>
			<div class="panel-body"> 
		  	  <#if specialAreaPage?? && specialAreaPage.content??>
				<#assign specialAreaList = specialAreaPage.content />
				<#list specialAreaList as specialArea>  
					<label class="checkbox-inline"><input type="checkbox" id="specialArea_checkbox_${specialArea.id}" value="${specialArea.id}"> ${specialArea.name}【${specialArea.code}】</label>
				</#list>
			   </#if>
			 </div>
		  </div>
	  </div>
	  <div class="col-md-12">
		<div class="panel panel-warning" id="category_list_div">
			<div class="panel-heading ">类别列表 【<#if categoryPage??>${categoryPage.totalElements}</#if>】 <label class="checkbox-inline"><input type="checkbox" id="category_checkbox_all" value="" onclick="selectAll('category_checkbox_all','category_checkbox_')"> 全选</label></div>
			<div class="panel-body"> 
		  	  <#if categoryPage?? && categoryPage.content??>
				<#assign categoryList = categoryPage.content />
				<#list categoryList as category>  
					<label class="checkbox-inline"><input type="checkbox" id="category_checkbox_${category.id}" value="${category.id}"> ${category.name}【${category.code}】</label>
				</#list>
				
			   </#if>
			 </div>
		  </div>
	  </div>
	   <div class="col-md-12">
	  	<button class="btn btn-primary" onClick="bindSpecialArea('${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_MAPPING}','#stickyNoteCategory_content_div')">绑定分类</button>
	  	<button class="btn btn-primary" onClick="unbindSpecialArea('${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_REMOVE_FROM_SPECIAL_MAPPING}','#stickyNoteCategory_content_div')">移除分类</button>
	  	<button class="btn btn-primary" onClick="filterSpecialArea('${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_ASIGN_LIST_UI_MAPPING}','#list_content_div')">筛选</button>
	  </div>
	  
	  <div class="col-md-12">
	  	<p class="text-primary">类别列表</p>
	  	<div id="list_content_div">
		<@listCategorySpecialView2 page=categorySpecialPage categorySpecial=categorySpecial/>
		</div> 	
	  </div>
	 </div> 
<script src="/assets/default/js/site/jit84j_core.js?t=13"></script>	
<script type="text/javascript">
function bindSpecialArea(url,backDataContainerId){
	var specialArea_checkbox_Value = joinCheckboxValue("specialArea_checkbox_");
	var category_checkbox_Value = joinCheckboxValue("category_checkbox_");
	
	if(""==specialArea_checkbox_Value){
		alert("没有选择【特殊区】");
		return;
	}
	if(""==category_checkbox_Value){
		alert("没有选择【类别】");
		return;
	}
	var request = {"specialAreaIds":specialArea_checkbox_Value,"categoryIds":category_checkbox_Value};
	submitOpration(url,request,backDataContainerId);
	//alert(specialArea_checkbox_Value + " : " + category_checkbox_Value);
}

function unbindSpecialArea(url,backDataContainerId){
	var specialArea_checkbox_Value = joinCheckboxValue("specialArea_checkbox_");
	var category_checkbox_Value = joinCheckboxValue("category_checkbox_");
	
	if(""==specialArea_checkbox_Value){
		alert("没有选择【特殊区】");
		return;
	}
	if(""==category_checkbox_Value){
		alert("没有选择【类别】");
		return;
	}
	var request = {"specialAreaIds":specialArea_checkbox_Value,"categoryIds":category_checkbox_Value};
	submitOpration(url,request,backDataContainerId);
}

function filterSpecialArea(url,backDataContainerId){
	var specialArea_checkbox_Value = joinCheckboxValue("specialArea_checkbox_");
	var category_checkbox_Value = joinCheckboxValue("category_checkbox_");
	
	
	var request = {"specialAreaIds":specialArea_checkbox_Value,"categoryIds":category_checkbox_Value};
	submitOpration(url,request,backDataContainerId);
}

</script>
</#macro>

<#macro listCategorySpecialView2 page categorySpecial>
<#if page?? && page.content??>
		<#assign categorySpecialList = page.content />
		<form id="categorySpecial_list_view_form" action="${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING}" method="post">
	
		 <table class="table">
		<thead>
		
		<tr>
		 	<th>
			 
			</th>
		 	<th>
			 <input type="text"data="data" value="<#if categorySpecial?? && categorySpecial.specialArea??>${categorySpecial.specialArea.name}</#if>" name="specialArea.name" id="search_specialArea_name" placeholder="输入特殊区名称">
			</th>
			<th>
			 <input type="text" data="data" value="<#if categorySpecial??>${categorySpecial.name}</#if>" name="name" id="search_name" placeholder="输入类别名称">
			</th>
			 <th>
			 
			 </th>
			  <th>
			 	<input type="checkbox" data="data" name="writable" <#if categorySpecial?? && categorySpecial.writable> checked </#if> id="search_writable"  class="form-control" id="writable" />
			 </th>
			 <th>
			 	<input type="checkbox" data="data" name="openable" <#if categorySpecial?? && categorySpecial.openable> checked </#if> id="search_openable"  class="form-control" id="openable" />
			 </th>
			 <th>
			 
			 </th>
			 <th>
			 
			 </th>
			 <th>
			 
			 </th>
			 <th>
			<#--TODO some issue for the search button <button class="btn btn-primary" onClick="searchSpecialArea('#list_item_tr','search_')">搜索</button>-->
			 </th>
		</tr>
	
		 <tr>
		 	<th>
			 Id
			</th>
			<th>
			 特殊区
			</th>
		 	<th>
			 名称
			</th>
			 <th>
			 图标
			 </th>
			  <th>
			 允许发表<input type="checkbox" data="data" name="writable"  class="form-control" id="writable_checkbox_all" onclick="selectAll('writable_checkbox_all','writable_checkbox_')"/>
			 
			 </th>
			 <th>
			 开放<input type="checkbox" data="data" name="openable"  class="form-control" id="openable_checkbox_all" onclick="selectAll('openable_checkbox_all','openable_checkbox_')"/>
			 </th>
			 <th>
			 开放时间
			 </th>
			 <th>
			 排序
			 </th>
			 <th>
			 描述
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		
		</thead>
		<tbody id="list_item_tr">
		<@listCategorySpecialView2Item page=page/>
		</tbody>
		
		</table>
		</form>
		<button class="btn btn-primary" onClick="saveList('#categorySpecial_list_view_form','${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_LIST_SAVE_MAPPING}','${page.number}')">保存列表</button>
</#if>
<script type="text/javascript">
function saveList(formId, action, currentNum){
	if(""!=formId){
		$(formId).attr("action",action);
		formSubmitAjax(currentNum);
	}
}
function searchSpecialArea(url,backDataContainerId){
	//var specialArea_checkbox_Value = joinCheckboxValue("specialArea_checkbox_");
	//var category_checkbox_Value = joinCheckboxValue("category_checkbox_");
	
	
	//var request = {"specialAreaIds":specialArea_checkbox_Value,"categoryIds":category_checkbox_Value};
	alert(url);
	formSubmitAjax('0');
}
</script>
</#macro>
<#macro listCategorySpecialView2Item page>
		<#if page?? && page.content??>
			<#assign categorySpecialList = page.content />
			<#list categorySpecialList as categorySpecial>  
				 <tr>
				 	<td>
					 ${categorySpecial.id}
					  <input type="hidden" data="data" name="categorySpecialList[${categorySpecial_index}].id" value="${categorySpecial.uniqueIdentifier}" class="form-control" id="id${categorySpecial.uniqueIdentifier}" />
					</td>
					<td>
					 ${categorySpecial.specialArea.name}
					</td>
					<td>
					 ${categorySpecial.name}
					</td>
					 <td>
					 ${categorySpecial.imagePath}
					 </td>
					  <td>
				 	 <input type="checkbox" data="data" name="categorySpecialList[${categorySpecial_index}].writable" <#if categorySpecial?? && categorySpecial.writable>checked</#if> class="form-control" id="writable_checkbox_${categorySpecial.uniqueIdentifier}" />
					 </td>
					 <td>
				 	 <input type="checkbox" data="data" name="categorySpecialList[${categorySpecial_index}].openable" <#if categorySpecial?? && categorySpecial.openable>checked</#if> class="form-control" id="openable_checkbox_${categorySpecial.uniqueIdentifier}" />
					 </td>
					 <td>
					  ${categorySpecial.beginTime} - ${categorySpecial.endTime}
					 </td>
					 <td>
					  <input type="text" data="data" name="categorySpecialList[${categorySpecial_index}].sequence" value="<#if categorySpecial??>${categorySpecial.sequence}</#if>" class="form-control" id="sequence${categorySpecial.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
					 </td>
					  <td>
					  ${categorySpecial.description}
					 </td>
					 <td>
						 <ul class="list-inline"> 
							<li>
						 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${categorySpecial.id}" onclick="switchSetting('${categorySpecial.id}','${categorySpecial.id}','${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_EDIT_UI_MAPPING}','#stickyNoteCategory_content_div')">编辑</a>
						  	</li>
						    <li>
						    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${categorySpecial.id}','${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_SPECIAL_REMOVE_FROM_ASIGN_UI_MAPPING}','#stickyNoteCategory_content_div')">删除</a>
						
						    </li>
						  </ul>
				 	  </td>
				 </tr>
			</#list>
			<tr>
				<td colspan="9">
				<@pageBarAjax page=page url="${KylinboyDeveloperConstant.KYLINBOY_CATEGORY_ASIGN_SPECIAL_UI_MAPPING}" formId='categorySpecial_list_view_form' backDataContainerId="stickyNoteCategory_content_div" urlFirst="false"/>
				</td>
			</tr>
		</#if>
</#macro>
<#-- 绑定 类别 与 特殊区 end-->

