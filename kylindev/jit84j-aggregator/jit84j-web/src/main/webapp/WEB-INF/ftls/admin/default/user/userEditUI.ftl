<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加用户</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>



<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="user" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_SAVE_MAPPING}">
   <input type="hidden" name="id"  id="id" value="${user.id}"/>
	<div class="col-md-8">
	<div class="panel panel-warning" >
		<div class="panel-heading ">请填写以下必填信息</div>
		  
	      <@spring.bind "user.username" />  
	      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="username_1">User Name: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div class="col-sm-9">
		    <@form.input path="username" class="form-control" id="username" placeholder="输入用户名，必须唯一" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "user.email" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="email">Email address:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="email" class="form-control" id="email" placeholder="Enter email" required="true"/>
		  	</div>
		  </div>
		 
		   <@spring.bind "user.firstname" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="firstname">大名:
		    	<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="firstname" class="form-control" id="firstname" placeholder="输入名字" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "user.lastname" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="lastname">尊姓:
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="lastname" class="form-control" id="lastname" placeholder="输入姓氏" required="true"/>
		  	</div>
		   </div>
		  
		  <@spring.bind "user.comeLocation" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="comeLocation">来自哪里?
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="comeLocation" class="form-control" id="comeLocation" placeholder="输入你来自何方，不要输 [火星] 哦" required="true"/>
		  	</div>
		  </div>
		  
		   <@spring.bind "user.ownRelation" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		  	 <label class="col-sm-3 control-label" for="ownRelation.code">
		  	 与站长关系:
		  	 <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
		    </label>
		  	<div class="col-sm-9" class="radio-inline">
			  <#list ownRelationList as or>
			  <label >
			  <input type="radio" name="ownRelation.id" <#if user.ownRelation?? && user.ownRelation.uniqueIdentifier==or.id>checked</#if> id="ownRelation.code[${or.id}]" value="${or.id}" >
			   ${or.name}
			  </label>
			  </#list>
			  </div>
	  	   </div>
		  
		  <ul class="list-inline">
				  <li>
			       		<input type="submit" value="保存用户" class="btn btn-primary"/>
			       		<a href='${KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING}'><input type="button" id="btn_back" value="返回列表" class="btn btn-primary"/></a>
			       </li>
	       </ul>
	  
	  </div>
	
	</div>
	<div class="col-md-4">
		<@selectRole roleList=roleList user=user/>
  </div>
</@form.form>
</div>
</div>
</body>
<script type="text/javascript">

 
</script>

<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>