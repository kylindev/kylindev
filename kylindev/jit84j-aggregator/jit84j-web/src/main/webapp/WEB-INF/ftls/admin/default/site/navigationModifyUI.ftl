<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>修改导航</title>
</head>
<div class="container" id="admin_main_top_navigation">
	<div class="row">
		<@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
	</div>
</div>
<body>
<div class="container body_id" id="body_id">
<@form.form commandName="navigation" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_MAPPING}">
<div class="row">
		  <div class="col-md-12" id="listDeveloperForPermission">
		  		<input type="hidden" name="developerId" id="developerId" value="${navigation.developer.id}"/>
		  		<input type="hidden" name="listDeveloperForPermissionAction" id="listDeveloperForPermissionAction" value="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING}"/>
		  		<script src="/assets/default/js/developer/developer.js"></script>
		  </div>
</div>

<div class="row">

  <div class="col-md-6">
  <p class="text-primary">修改导航</p>
  
  <@spring.bind "navigation.uniqueIdentifier" /> 
  <@form.hidden path="id"/>
      <@spring.bind "navigation.name" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="name">导航名称: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="name" class="form-control" id="name" placeholder="输入导航名称" required="true"/>
	  </div>
	  <@spring.bind "navigation.code" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">导航代码: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="code" class="form-control" id="code" placeholder="输入导航代码，使用字母" required="true"/>
	  </div>
	  <@spring.bind "navigation.iconPath" />
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">导航图标: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="iconPath" class="form-control" id="iconPath" placeholder="输入导航图标" />
	 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
	    	<div id="iconPath_img">
	    	<img src='${navigation.iconPath}'/>
	    	</div>
	    </div>
	  <@spring.bind "navigation.url" />
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">导航路径: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="url" class="form-control" id="url" placeholder="输入导航路径" />
	  </div>
	  <@spring.bind "navigation.sequence" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">顺序: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="sequence" class="form-control" id="sequence" placeholder="输入导航顺序，使用数字1,2,10,12" required="true"/>
	  </div>
	  <@spring.bind "navigation.hideable" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="hideable">是否隐藏: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <input type="checkbox" name="hideable" <#if (navigation.hideable)> value="true" checked</#if>  class="form-control" id="hideable" />
	  </div>
	   <@spring.bind "navigation.type" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="type">选择类型: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <select class="form-control" name="type" id="type" required="true">
	    <#assign typeMaps = StaticDataDefineCoreManager.getNavigationTypeDropdown()/>
		    <#list typeMaps.keySet() as key>
		    	 <option value="${key}">${typeMaps.get(key)}</option> 
		    </#list>
		</select>
	  </div>
	  
	  <ul class="list-inline">
       	<li><input type="submit" value="保存设置" class="btn btn-primary"/></li>
       </ul>
	  </div>
	  <div class="col-md-3">
	  <div class="panel panel-info">
		<div class="panel-heading ">选择父类</div>
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="control-label" for="parent.id"> 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div>
			<#assign depth = 1 />  
		  	<@selectParentNavigationRadio children=navigationList currentNavigation=navigation />
		  	</div>
		  </div>
		 </div>
	  </div>
	  <div class="col-md-3">
	  <div class="panel panel-info">
		<div class="panel-heading ">分配子类</div>
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if> ">
		    <label class="control-label" for="parent.id"> 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div>
			<#assign depth = 1 />  
			<@asignChildrenNavigationCheckbox children=navigationList currentNavigation=navigation/>
			</div> 
		  </div>
		 </div>
	   </div>
	   
	</@form.form>
  
</div>
<script src="/assets/ckeditor/adapters/jquery.js"></script>
<div id="filepathTextFieldId_hidden"></div>
<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
<script src="/assets/common/js/jquery/jquery.json-2.4.min.js"></script>
</div>

</body>
</html>