<#macro displayExistRole roleList>
<div class="panel panel-info">
		<div class="panel-heading ">已存在的角色</div>
			<#if roleList??>
				
				<#list roleList as role>
					<ul class="list-inline">
					<li>${role.rolename}</li>
					<li>
						[ ${role.roleCode} ] ${role.roleAlias} [ ${role.roleLevel} ]
					</li>
					</ul>
				</#list>
				
			</#if>
	</div>

</#macro>

<#macro selectRole roleList user="user">
	<div class="panel panel-info">
		<div class="panel-heading ">分配角色</div>
			<@selectRoleInLine roleList=roleList user=user/>
	</div>
</#macro>

<#macro selectRoleInLine roleList checkboxName="roleIdList" user="user">
			<#if roleList??>
				<ul class="list-inline">
				<#list roleList as role>
					<li>
					<input type="checkbox" name="${checkboxName}" value="${role.uniqueIdentifier}"
					<#if user?? && user !="user" && user.roleList??>
						<#list user.roleList as userRole>
							<#if userRole?? && userRole.role.uniqueIdentifier==role.uniqueIdentifier>
							checked
							</#if>
						</#list>
					</#if>
					>
					${role.rolename}
					<li>
				</#list>
				</ul>
			</#if>
</#macro>


<#-- 选择角色begin 单选 -->
<#macro selectRadioRole roleList>
<div class="panel panel-warning">
		<div class="panel-heading ">选择角色</div>
			<#if roleList??>
				
				<#list roleList as role>
					<ul class="list-inline">
					<li><input type="radio" value="${role.id}" name="role.id"/></li>
					<li>${role.rolename}</li>
					<li>
						[ ${role.roleCode} ] ${role.roleAlias} [ ${role.roleLevel} ]
					</li>
					</ul>
				</#list>
				
			</#if>
	</div>

</#macro>
<#-- 选择角色end 单选 -->

<#-- 角色列表 -->
<#macro listRoleTree page>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>颜色标识：</td>
 <#assign trClass = '' />

  <#assign roleList = page.content />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级菜单</td>
</#list>
</tr>
</table>
  <table class="table">
  <thead>
    <tr>
      <th width="3%">Id</th>
      <th width="15%">角色名称</th>
      <th width="10%">角色代码</th>
      <th width="15%">角色别名</th>
      <th width="8%">角色级别</th>
      <th width="5%">排序</th>
      <th width="25%">操作</th>
    </tr>
  </thead>
  <@form.form commandName="role" id="form_role" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_MODIFY_MAPPING}/role_list">
  <tbody>
  <#if (roleList??)>
  	 <#assign depth = 0 />  
  	 <#assign countNum = 0 /> 
	<@listRoleTreeItem page=page/>
	<tr>
		<td colspan="7"><@pageBar page=page url='${KylinboyDeveloperConstant.KYLINBOY_ROLE_LIST_UI_MAPPING}/role_list' formId='form_role'/></td>
	</tr>
  </#if>
	<tr>
		<td>
		<#if (roleList??)>
			<input type="submit" value="保存列表" class="btn btn-primary"/>
		</#if>
		</td>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="/admin/userinfo/role/addRoleUI/role_add"/>'>
				<input type="button" value="添加角色" class="btn btn-primary"/>
			</a>
		</td>
		<td>
			<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
   </tbody>
   </@form.form>
  </table>
</div>
</#macro>

<#macro listRoleTreeItem page>
 <#assign roleList = page.content />
	<#if roleList??>
		<#list roleList as role>  
			 <#assign depth = depth + 1 />
			 <#assign trClass = '' />
			 <@getClassColor depth/>
			 
			 <tr ${trClass}>
			 <td>${role.uniqueIdentifier} <#list 1..depth as i> > </#list>
			 <input type="hidden" name="roleList[${countNum}].id" value="${role.uniqueIdentifier}"/>
			 </td>
			 <td>
			 <input type="text" name="roleList[${countNum}].rolename" value="${role.rolename}" class="form-control" id="rolename[${countNum}]" placeholder="输入名称"/>
			 </td>
			 <td>
			 <input type="text" name="roleList[${countNum}].roleCode" value="${role.roleCode}" class="form-control" id="roleCode[${countNum}]" placeholder="输入代码"/>
			 </td>
			 <td>
			 <input type="text" name="roleList[${countNum}].roleAlias" value="${role.roleAlias}" class="form-control" id="roleAlias[${countNum}]" placeholder="输入别名"/>
			 </td>
			  <td>
			 <input type="text" name="roleList[${countNum}].roleLevel" value="${role.roleLevel}" class="form-control" id="roleLevel[${countNum}]" placeholder="输入级别"/>
			 </td>
			 <td>
			 <input type="text" name="roleList[${countNum}].roleOrder" value="${role.roleOrder}" class="form-control" id="roleOrder[${countNum}]" placeholder="排序"/>
			 </td>
			 <td>
			 <ul class="list-inline"> 
				 <#if role.code?? && role.code!='top_Navigation'>
					<li>
				 	<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/navigationModifyUI/${role.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
				  	</li>
				  </#if>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/delete/${role.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
				    </li>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${role.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign countNum = countNum + 1 />
			 <#assign depth = depth-1 />
		</#list>
		
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>

<#macro selectRadioRole2 page backDataContainerId=''>

		 	<input type="text" data="data" value="${role.rolename}" id="condition_rolename" name="rolename" placeholder="输入角色名称"/>
		 	<input type="text" data="data" value="${role.roleCode}" id="condition_roleCode" name="roleCode" placeholder="输入角色代码"/>
		 	<input type="button" value="查询" id="condition_query"  onclick="selectedRole()"/>
		 </div>
			<#if page??>
				<#assign roleList = page.content />
				<#if roleList??>
					<#list roleList as role>
						<ul class="list-unstyled">
							<li class="checkbox input-sm" onclick="showCurrentPermission('#div_role_current_${role.id}')">
							    <label >
							     	<input type="radio" value="${role.id}" id="role_id_${role.id}" name="id"/> ${role.rolename} [${role.roleCode} ] ${role.roleLevel} 
							    </label>
							</li>
						</ul>
						<div class="hidden" id="div_role_current_${role.id}">
							<#if role??>
								<#if role.permissionList??>
									<#list role.permissionList as rolePermission>
									<input type="hidden" value="permission_id_${rolePermission.permission.id}">
									</#list>
								</#if>
							</#if>
						</div>
					</#list>
					<@pageBarAjax page=page formId='role_permission_form_id' url=KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING backDataContainerId=backDataContainerId/> 
				</#if>
			</#if>
<script type="text/javascript">
function selectedRole(){
		var rolename = $('#condition_rolename').val();
		var roleCode = $('#condition_roleCode').val();
        if(rolename ==null){
        	rolename='';
        }
        if(roleCode ==null){
        	roleCode='';
        }
        var backDataContainerId = '#${backDataContainerId}';
        $.ajax({
			url: '${KylinboyDeveloperConstant.KYLINBOY_SELECTED_ROLE_UI_MAPPING}',
			data: {"rolename":rolename, "roleCode":roleCode},
			type: "POST",
			dataType : "text",
			contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
			success: function(data) {
						if(backDataContainerId != "#"){
							$(backDataContainerId).html(data);
						}
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		}); 
}
function showCurrentPermission(divId){
	$("div[id ^='div_role_current_']").removeClass("container");
	$("div[id ^='div_role_current_']").addClass("hidden");
	$(divId).removeClass("hidden");
	$(divId).addClass("container");
	var selectedNodes = $("#tree_permission").dynatree("getSelectedNodes");
	if(null != selectedNodes && selectedNodes.length>0){
		for(var i=0;i<selectedNodes.length;i++){
			$(selectedNodes[i].span).removeClass("dynatree-selected");
			selectedNodes[i].bSelected=false;
		}
	}
	$(divId).find(":hidden").each(function(){
		var _key = $(this).val();
		var _node = $("#tree_permission").dynatree("getTree").getNodeByKey(_key);
		if($(_node).length>0){
			_node.bSelected=true;
			$(_node.span).addClass("dynatree-selected");
		}
	});
}
</script>
</div>

</#macro>


