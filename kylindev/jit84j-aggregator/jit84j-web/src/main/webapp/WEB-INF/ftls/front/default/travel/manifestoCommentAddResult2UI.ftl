<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<div class="row" id="row_span_comment_id_${manifestoComment.id}">
	<div class="col-md-12" id="span_comment_id_${manifestoComment.id}">
	<#assign manifesto=manifestoComment.manifesto/>
	<#if manifestoComment.manifesto??>
		<input type="hidden" name="commentTotalNumber_${manifestoComment.manifesto.id}" id="commentTotalNumber_${manifestoComment.manifesto.id}" value="${manifestoComment.manifesto.commentCount}">
	</#if>
		<p <#if !manifestoComment.parent??> class="lead" </#if>>
		<#assign depth=depth+1>
		<#if depth gt 0>
			<#list 1..depth as i>
			<span class="glyphicon glyphicon-eye-open"></span> 
			</#list>
		</#if>
		<span class="text-danger">${manifestoComment.nickName}</span> 
		at <small>${manifestoComment.publishDate?datetime}</small> <#if manifestoComment.parent??>【回复】 ${manifestoComment.parent.nickName} : <#else> 【说】：</#if> 
		${manifestoComment.comment}
		<span class="text-success">${msg}</span>
		</p>
	</div>
</div>
