

<#--站点基本信息编辑 begin-->
<#macro siteInfoEdit siteInfo>
<p class="text-success">设置站点基本信息</p>

  <@form.form id="siteInfo_basic_edit_ui_form_id" commandName="siteInfo" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_SITEINFO_BASIC_EDIT_MAPPING}">
  <@spring.bind "siteInfo.uniqueIdentifier" /> 
  <@form.hidden data="data" path="id"/>

  <@spring.bind "siteInfo.stickyNoteConfig.uniqueIdentifier" />  
  <@form.hidden data="data" path="stickyNoteConfig.id"/>

    <@spring.bind "siteInfo.siteName" />  
    <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteName">站点名称: 
	<#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
	</#if>
	</label>

    <@form.input data="data" path="siteName" class="form-control" id="siteName" placeholder="输入站点名称" required="true"/>

  </div>

  <@spring.bind "siteInfo.siteInstallUrl" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteInstallUrl">安装路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteInstallUrl" class="form-control" id="siteInstallUrl" placeholder="输入安装路径" required="true"/>

  </div>

 

  <@spring.bind "siteInfo.siteUrl" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteUrl">站点域名:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteUrl" class="form-control" id="siteUrl" placeholder="输入站点域名" required="true"/>

  </div>

 

  <@spring.bind "siteInfo.siteAdminName" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label for="siteAdminName">站长名字：
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteAdminName" class="form-control" id="siteAdminName" placeholder="输入站长名字" required="true"/>

  </div>

  <@spring.bind "siteInfo.siteEmail" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteEmail">站点Email:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteEmail" class="form-control" id="siteEmail" placeholder="站点Email，用于接收注册信息" required="true"/>

  </div>

  <@spring.bind "siteInfo.siteTitle" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteTitle">站点标题:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteTitle" class="form-control" id="siteTitle" placeholder="输入站点标题" required="true"/>

  </div>

  <@spring.bind "siteInfo.siteSecondTitle" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteSecondTitle">站点副标题:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="siteSecondTitle" class="form-control" id="siteSecondTitle" placeholder="输入副标题" />

  </div>

  <@spring.bind "siteInfo.siteKey" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="siteKey">站点关键词:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.textarea data="data" path="siteKey" class="form-control" id="siteKey" placeholder="站点关键词" />

  </div>

  <@spring.bind "siteInfo.description" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="description">站点描述:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.textarea data="data" path="description" class="form-control" id="description" placeholder="站点描述" />

  </div>

  <@spring.bind "siteInfo.iconPath" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="iconPath">站标路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="iconPath" class="form-control" id="iconPath" placeholder="站标路径,默认/image/site_info/icon.png" />

    <img src="${siteInfo.iconPath}" title="站标" alt="独家记忆" class="img-rounded">

  </div>

 

  <@spring.bind "siteInfo.logoPath" /> 

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="iconPath">站点logo路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input path="logoPath" class="form-control" id="logoPath" placeholder="站点logo路径,默认/image/site_info/logo.gif" />

    <img src="${siteInfo.logoPath}" title="站点logo" alt="独家记忆" class="img-rounded">

  </div>

</@form.form>
  <div class="col-md-12">
       <a href="javascript:void(0)" onClick="submitForm('siteInfo_basic_edit_ui_form_id')" class="btn btn-primary">保存设置</a>
  </div>
  
<script type="text/javascript">
  function submitForm(formId){
  	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	var _formId="#"+formId;
	var action = $(_formId).attr("action");
	$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "json",
			success: function(data) {
				alert(data.msg);
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		});
	}								
  </script>
</#macro> 
<#--站点基本信息编辑 end-->


<#--随手贴编辑 begin-->
<#macro siteInfoStickyNoteEdit siteInfo>

    <p class="text-success">随手贴设置:</p>
    
  <@form.form id="siteInfo_sticky_note_edit_ui_form_id" commandName="siteInfo" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_SITEINFO_STICKYNOTE_EDIT_MAPPING}">
  <@spring.bind "siteInfo.uniqueIdentifier" /> 
  <@form.hidden data="data" path="id"/>

  <@spring.bind "siteInfo.stickyNoteConfig.uniqueIdentifier" />  
  <@form.hidden data="data" path="stickyNoteConfig.id"/>
    
  <@spring.bind "siteInfo.stickyNoteConfig.stickyNoteImagePath" />

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="stickyNoteConfig.stickyNoteImagePath">随手贴图片保存路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="stickyNoteConfig.stickyNoteImagePath" class="form-control" id="stickyNoteConfig.stickyNoteImagePath" placeholder="随手贴图片保存路径,默认/image/sticky_note" />

  </div>

  <@spring.bind "siteInfo.stickyNoteConfig.stickyNoteMusicPath" />

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">

    <label class="control-label" for="stickyNoteConfig.stickyNoteImagePath">随手贴音乐保存路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>

    <@form.input data="data" path="stickyNoteConfig.stickyNoteMusicPath" class="form-control" id="stickyNoteConfig.stickyNoteMusicPath" placeholder="随手贴音乐保存路径,默认/music/sticky_note" />

  </div>

  <@spring.bind "siteInfo.stickyNoteConfig.stickyNoteVedioPath" />

  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
    <label class="control-label" for="stickyNoteConfig.stickyNoteVedioPath">随手贴视频保存路径:
    <#if (spring.status.errorMessages?size>0)>
	  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
	</#if>
    </label>
    <@form.input data="data" path="stickyNoteConfig.stickyNoteVedioPath" class="form-control" id="stickyNoteConfig.stickyNoteVedioPath" placeholder="随手贴视频保存路径,默认/vedio/sticky_note" />
  </div>
  
  <div class="col-md-12">
        <a href="javascript:void(0)" onClick="submitForm('siteInfo_sticky_note_edit_ui_form_id')" class="btn btn-primary">保存设置</a>
  </div>
  </@form.form>
<script type="text/javascript"> 
  function submitForm(formId){
  	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	var _formId="#"+formId;
	var action = $(_formId).attr("action");
	$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "json",
			success: function(data) {
				alert(data.msg);
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		});
	}								
  </script>
</#macro> 
<#--随手贴编辑 end-->


<#--文件信息设置 begin-->
<#macro siteInfoFileInfoConfigEdit siteInfo>

    <p class="text-success">文件信息设置:</p>
    
  <@form.form id="siteInfo_file_info_edit_ui_form_id" commandName="siteInfo" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_SITEINFO_FILEINFO_EDIT_MAPPING}">
  <@form.hidden data="data" path="id"/>

  <@form.hidden data="data" path="fileInfoConfig.id"/>
    

  <div class="form-group">

    <label class="control-label" for="fileInfoConfig.rootPath">文件上传保存根路径:
    </label>

    <@form.input data="data" path="fileInfoConfig.rootPath" class="form-control" id="fileInfoConfig.rootPath" placeholder="上传文件保存路径,默认 ${FileInfoConfig.ROOT_PATH_DEFAULT}" />

  </div>

  <div class="col-md-12">
        <a href="javascript:void(0)" onClick="submitForm('siteInfo_file_info_edit_ui_form_id')" class="btn btn-primary">保存设置</a>
  </div>
  </@form.form>
<script type="text/javascript"> 
  function submitForm(formId){
  	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	var _formId="#"+formId;
	var action = $(_formId).attr("action");
	$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "json",
			success: function(data) {
				alert(data.msg);
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		});
	}								
  </script>
</#macro> 
<#--文件信息设置 end-->


