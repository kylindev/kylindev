<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_permission.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加权限</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="permission" permission="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_MAPPING}">
	<div class="col-md-8">
	<div class="panel panel-warning" >
		<div class="panel-heading ">请填写以下必填信息</div>
		  
	      <@spring.bind "permission.name" />  
	      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="name">权限名称: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div class="col-sm-9">
		    <@form.input path="name" class="form-control" id="name" placeholder="输入权限名称，必须唯一" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "permission.code" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="code">权限代码:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="code" class="form-control" id="code" placeholder="输入权限代码" required="true"/>
		  	</div>
		  </div>
		  
		  <@spring.bind "permission.resourceUrl" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="resourceUrl">资源路径:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="resourceUrl" class="form-control" id="resourceUrl" placeholder="输入资源路径" required="true"/>
		  	</div>
		  </div>
		  
		  
		   <@spring.bind "permission.opration" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="opration">操作代码:
		    	<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="opration" class="form-control" id="opration" placeholder="输入操作代码" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "permission.oprationCount" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="oprationCount">操作次数:
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="oprationCount" class="form-control" id="oprationCount" placeholder="输入操作次数" required="true"/>
		  	</div>
		   </div>
		  
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="description">权限描述
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.textarea path="description" class="form-control" id="description" placeholder="输入权限描述" />
		  	</div>
		  </div>
		  
		  <ul class="list-inline">
				  <li>
			       		<input type="submit" value="确认添加" class="btn btn-primary"/>
			       </li>
	       </ul>
	  
	  </div>
	
	</div>
  <div class="col-md-4" id="listDeveloperForPermission">
		<input type="hidden" name="developerId" id="developerId" value=""/>
		<input type="hidden" name="listDeveloperForPermissionAction" id="listDeveloperForPermissionAction" value="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_AJAX_MAPPING}"/>
		<script src="/assets/default/js/developer/developer.js"></script>
  </div>
</@form.form>
</div>
</div>

</body>

<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg"/>
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>