<#macro listUser page>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>用户身份：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级用户</td>
</#list>
</tr>
</table>
  <table class="table">
  <@form.form commandName="userRole" id="user_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING}">
  <thead>
  <tr>
      <td width="3%"></td>
      <td width="10%"><input type="text" placeholder="用户名" size="5" name="username" value="<#if user??>${user.username}</#if>"></td>
      <td width="10%"><input type="text" placeholder="Email" size="5" name="email" value="<#if user??>${user.email}</#if>"></td>
      <td width="15%"><input type="text" placeholder="姓氏" size="2" name="lastname" value="<#if user??>${user.lastname}</#if>">
      	<input type="text" placeholder="名字" size="3" name="firstname" value="<#if user??>${user.firstname}</#if>">
	 </td>
      <td width="15%">
      	<select placeholder="与站长关系" name="ownRelationId" value="<#if user??>${user.ownRelationId}</#if>">
      			<option value="">ALL</option>
      			<#list ownRelationList as or>
			  	<option value="${or.id}" <#if user.ownRelationId == or.id>selected</#if>>${or.name}</option>
				</#list>
      	</select>
      	</td>
      <td width="15%"><input type="text" placeholder="昵称" size="5" name="nickname" value="<#if user??>${user.nickname}</#if>"></td>
      <td width="25%"><input type="button" value="搜索" class="btn btn-primary" onclick="formSubmit(1)"/></td>
    </tr>
    <tr>
      <th width="3%">Id</th>
      <th width="10%">用户名</th>
      <th width="10%">Email</th>
      <th width="15%">姓名</th>
      <th width="15%">与站长关系</th>
      <th width="15%">昵称</th>
      <th width="25%">操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page.content??)>
  	 <#assign depth = 1 />  
	<@listUserItem page=page/>
  </#if>
	<tr>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
				<input type="button" value="添加用户" class="btn btn-primary"/>
			</a>
		</td>
		<td>
		<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING}"/>'>
				<input type="button" value="分配角色" class="btn btn-primary"/>
		</a>
		</td>
		
	 </tr>
	
   </tbody>
   </@form.form>
  </table>
</div>
</#macro>

<#macro listUserItem page>
	<#if page?? && page.content??>
	<#assign userList = page.content />
		<#list userList as user>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth-1/>
			 
			 <tr ${trClass}>
			 <td>
			 ${user.uniqueIdentifier}
			 </td>
			 <td>
			 ${user.username}
			 </td>
			 <td>
			 ${user.email}
			 </td>
			 <td>
			 ${user.lastname}  ${user.firstname}
			 </td>
			  <td>
			  <#if user.ownRelation?exists>
			  ${user.ownRelation.name}
			  </#if>
			 </td>
			 <td>
			 ${user.nickname}
			 </td>
			 <td>
			 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING}/${user.id}"/>'>编 辑</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_USER_DELETE_MAPPING}/${user.id}"/>'>删 除</a>
				    </li>
			
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<tr>
		<td colspan="7">
		<@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_USER_LIST_UI_MAPPING}" formId='user_list_form'/>
		</td>
		</tr>
		
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>





<#-- 分配角色-->
<#macro listUserForAsignRole page roleList user>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>用户身份：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级用户</td>
</#list>
</tr>
</table>
  
  <table class="table">
   <@form.form commandName="userRole" id="user_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_MAPPING}">
  <thead>
  <tr>
      <td width="3%">
      </td>
      
       <td width="10%"><input type="text" placeholder="用户名" size="5" name="username" value="<#if user??>${user.username}</#if>"></th>
      <td width="10%"><input type="text" placeholder="Email" size="5" name="email" value="<#if user??>${user.email}</#if>"></th>
      <td width="15%"><input type="text" placeholder="姓氏" size="2" name="lastname" value="<#if user??>${user.lastname}</#if>">
      	<input type="text" placeholder="名字" size="3" name="firstname" value="<#if user??>${user.firstname}</#if>">
	 </td>
      
      
      <td width="15%"><input type="text" placeholder="昵称" size="5" name="nickname" value="<#if user??>${user.nickname}</#if>"></td>
      <td width="15%"><input type="text" placeholder="用户角色" name=""></td><#--TODO-->
      <td width="25%"><input type="button" value="搜索" class="btn btn-primary" onclick="formSubmit(1)"/></td>
    </tr>
    <tr>
      <th width="3%">全选
      <input type="checkbox" name="select_all" id="select_all" value="all">
      </th>
      <th width="10%">用户名</th>
      <th width="10%">Email</th>
      <th width="15%">姓名</th>
      <th width="15%">昵称</th>
      <th width="15%">用户角色</th>
      <th width="25%">操作角色</th>
    </tr>
  </thead>
  <tbody>
 
  <#if (page??)>
  	 <#assign depth = 1 />  
	<@listUserlistUserForAsignRoleItem page=page/>
  </#if>
  <tr>
  	<td>选择角色:</td>
    	<td  colspan="6">
      		<@selectRoleInLine roleList=roleList checkboxName="roleIdList"/>
        </td>
   </tr>
   <tr>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
				<input type="button" value="添加用户" class="btn btn-primary"/>
			</a>
		</td>
		<td colspan="6">
				<input type="submit" value="分配角色" class="btn btn-primary"/>
		</td>
	 </tr>
   </tbody>
   </@form.form>
  </table>
</div>
</#macro>

<#macro listUserlistUserForAsignRoleItem page>
<#if page??>
<#assign userList = page.content />
	<#if userList??>
		<#list userList as user>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth-1/>
			 
			 <tr ${trClass}>
			 <td>
			 ${user.uniqueIdentifier}
			 <input type="checkbox" name="userIdList" value="${user.uniqueIdentifier}">
			 </td>
			 <td>
			 ${user.username}
			 </td>
			 <td>
			 ${user.email}
			 </td>
			 <td>
			 ${user.firstname}  ${user.lastname}
			 </td>
			 <td>
			 ${user.nickname}
			 </td>
			 <td id="user_role_list_${user.uniqueIdentifier}">
			 <@listRoleName user=user/>
			 </td>
			 <td>
			 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:addRoles("${user.uniqueIdentifier}")'>添加绑定</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href="javascript:void(0)" onclick="removeRoles('${user.uniqueIdentifier}','${KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ROLES_MAPPING}')">全部解除</a>
				    </li>
				    <#--
				    <li>
				    	<a class="text-danger" href="javascript:void(0)" onclick="${KylinboyDeveloperConstant.KYLINBOY_USER_EDIT_UI_MAPPING}/${user.uniqueIdentifier}">查看</a>
				    </li>
				    -->
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<tr>
		<td colspan="7">
		<@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_ROLES_UI_MAPPING}" formId='user_list_form'/>
		</td>
		</tr>
		<script type="text/javascript">
			function addRoles(userId){
				
			}
			function removeRoles(userId,url){
			    alert(userId + code);
			    var action = url;
				var request={
					"code":code,
					"userIdList":userId
				};
				$.ajax({
					url: action,
					data: request,
					type: "POST",
					dataType : "html",
					success: function( val ) {
						$( "#user_role_list_"+userId ).html( val );
					},
					error: function( xhr, status ) {
						alert( "Sorry, there was a problem!" );
					},
					complete: function( xhr, status ) {
					}
				});
			}
		</script>
		
	</#if>
</#if>		
</#macro>

<#macro listRoleName user>
<#if user??>
	 <#assign userRoleList = user.roleList />
	 <#if userRoleList??>
	 	<ul class="list-inline">
				<#list userRoleList as userRole>
					<li>
					${userRole.role.rolename}
					<li>
				</#list>
		</ul>
	 </#if>
</#if>
</#macro>


<#-- 选择用户begin 单选 -->
<#macro selectRadioUser page>
<div class="panel panel-warning">
		<div class="panel-heading ">选择用户</div>
			<#if page??>
				<#assign userList = page.content />
				<#if userList??>
					<#list userList as user>
						<ul class="list-inline">
							<li><input type="radio" value="${user.id}" id="user_id_${user.id}" name="user.id"/></li>
							<li>${user.username}</li>
							<li>
								[${user.lastname} ${user.firstname} ] ${user.email} 
							</li>
						</ul>
					</#list>
					<@pageBar page=page url=KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING/> 
				</#if>
			</#if>
</div>

</#macro>
<#-- 选择用户end 单选 -->
<#macro selectRadioUser2 page backDataContainerId=''>
		 <div id="div_user_selected">
		 	<input type="text" data="data" value="${user.username}" id="condition_username" name="username" placeholder="输入用户名"/>
		 	<input type="text" data="data" value="${user.email}" id="condition_email" name="email" placeholder="输入email"/>
		 	<input type="button" value="查询" id="condition_query"  onclick="selectedUser()"/>
		 </div>
			<#if page??>
				<#assign userList = page.content />
				<#if userList??>
					<#list userList as user>
						<ul class="list-unstyled">
							<li class="checkbox input-sm" onclick="showCurrentPermission('#div_user_current_${user.id}')">
							    <label >
							     	<input type="radio" value="${user.id}" id="user_id_${user.id}" name="id"/> ${user.username} [${user.lastname} ${user.firstname} ] ${user.email} 
							    </label>
							</li>
						</ul>
						<div class="hidden" id="div_user_current_${user.id}">
							<#if user??>
								<#if user.permissionList??>
									<#list user.permissionList as userPermission>
									<input type="hidden" value="permission_id_${userPermission.permission.id}">
									</#list>
								</#if>
							</#if>
						</div>
					</#list>
					<@pageBarAjax page=page formId='user_permission_form_id' url=KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING backDataContainerId=backDataContainerId/> 
				</#if>
			</#if>
			<script type="text/javascript">
function selectedUser(){
		var username = $('#condition_username').val();
		var email = $('#condition_email').val();
        if(username ==null){
        	username='';
        }
        if(email ==null){
        	email='';
        }
        var backDataContainerId = '#${backDataContainerId}';
        $.ajax({
			url: '${KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING}',
			data: {"username":username, "email":email},
			type: "POST",
			dataType : "text",
			contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
			success: function(data) {
						if(backDataContainerId != "#"){
							$(backDataContainerId).html(data);
						}
					
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		}); 
}
function showCurrentPermission(divId){
	$("div[id ^='div_user_current_']").removeClass("container");
	$("div[id ^='div_user_current_']").addClass("hidden");
	$(divId).removeClass("hidden");
	$(divId).addClass("container");
	var selectedNodes = $("#tree_permission").dynatree("getSelectedNodes");
	if(null != selectedNodes && selectedNodes.length>0){
		for(var i=0;i<selectedNodes.length;i++){
			$(selectedNodes[i].span).removeClass("dynatree-selected");
			selectedNodes[i].bSelected=false;
		}
	}
	$(divId).find(":hidden").each(function(){
		var _key = $(this).val();
		var _node = $("#tree_permission").dynatree("getTree").getNodeByKey(_key);
		if($(_node).length>0){
			_node.bSelected=true;
			$(_node.span).addClass("dynatree-selected");
		}
	});
}
</script>
</div>

</#macro>

<#macro selectRadioUser3 page backDataContainerId=''>
		<div id="div_user_selected">
		 	<input type="text" data="data" value="${user.username}" id="condition_username" name="username" placeholder="输入用户名"/>
		 	<input type="text" data="data" value="${user.email}" id="condition_email" name="email" placeholder="输入email"/>
		 	<input type="button" value="查询" id="condition_query"  onclick="selectedUser()"/>
		 </div>
			<#if page??>
				<#assign userList = page.content />
				<#if userList??>
					<#list userList as user>
						<ul class="list-unstyled">
							<li class="checkbox input-sm" >
							    <label >
							     	<input type="radio" value="${user.id}" id="user_id_${user.id}" name="id" onclick="queryUserManageAreaItem('${KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_DATA_MAPPING}','${user.userId.userId}')"/> ${user.username} [${user.lastname} ${user.firstname} ] ${user.email} 
							    </label>
							</li>
						</ul>
					</#list>
					<@pageBarAjax page=page formId='user_manageAreaItem_form_id' url=KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING backDataContainerId=backDataContainerId/> 
				</#if>
			</#if>
<script type="text/javascript">

function selectedUser(){
		var username = $('#condition_username').val();
		var email = $('#condition_email').val();
        if(username == null){
        	username='';
        }
        if(email == null){
        	email='';
        }
        var backDataContainerId = '#${backDataContainerId}';
        $.ajax({
			url: '${KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING}',
			data: {"username":username, "email":email},
			type: "POST",
			dataType : "text",
			contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
			success: function(data) {
				if(backDataContainerId != "#"){
					$(backDataContainerId).html(data);
				}
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
		}); 
}
				
</script>
</div>

</#macro>



<#-- 最新注册用户列表 完整-->
<#macro lastRegisterUserListFull page>
<#if page??>
	<#assign lastList=page.content/>
	<#list lastList as user>
		<dd>
		<span class="text-danger">${user.username}</span> 
		<strong>来自</strong> 
		<span class="text-info">${user.comeLocation?default('*火星*')}</span> 
		<strong>的</strong> 
		<span class="text-danger">${user.civilit?default('*网友*')}</span>
		<strong>at</strong> 
		<span class="text-info">${user.createDate?string("MM-dd HH:mm")}</span>
		<strong>说:</strong> 
		${user.manifesto?default('这家伙啥也没说')}
		</dd>
	</#list>
</#if>
</#macro>
<#-- 最新注册用户列表 简单-->
<#macro lastRegisterUserListSimple page>
<#if page??>
	<#assign lastList=page.content/>
	<#list lastList as user>
		<dd>
		<span class="text-danger">${user.username}</span> 
		<strong>来自</strong> 
		<span class="text-info">${user.comeLocation?default('*火星*')}</span> 
		<strong>的</strong> 
		<span class="text-danger">${user.civilit?default('*网友*')}</span>
		<strong>at</strong> 
		<span class="text-info">${user.createDate?string("MM-dd HH:mm")}</span>
		</dd>
	</#list>
</#if>
</#macro>