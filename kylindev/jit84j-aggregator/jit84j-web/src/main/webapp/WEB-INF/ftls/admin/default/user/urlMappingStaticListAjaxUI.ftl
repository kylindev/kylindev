<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_urlMapping.ftl">
<script src="/assets/common/js/jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="/assets/common/js/jquery/jquery.cookie.js" type="text/javascript"></script>

<link href="/assets/common/js/jquery/dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" id="skinSheet">
<script src="/assets/common/js/jquery/dynatree/jquery.dynatree.js" type="text/javascript"></script>

<div class="row">
<div class="col-md-12">
<p class="text-success">URL Mapping列表:</p>
<#--
<@listUrlMappingTree page=page/>
-->
<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
		
			<div class="tabbable" id="tabs-995050">
				<ul class="nav nav-tabs">
					<li  >
						<a href="#panel-first" data-toggle="tab" >全部URL</a>
					</li>
					<li>
						<a href="#panel-second" data-toggle="tab" >新增URL</a>
					</li>
					<li >
						<a href="#panel-third" data-toggle="tab" >有变动URL</a>
					</li>
					<li >
						<a href="#panel-fourth" data-toggle="tab" >未变动URL</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="panel-first">
						<!--SPAN8-->
						<div class="panel panel-warning" >
							<div class="panel-heading ">URL模块划分</div>
							<div class="panel-body"> 
							<@urlStaticMappingCategoryListTree page=allPage  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING treeId="1"/>
							</div>	
						</div>				
									
						<!--SAPN8 END-->
						
						<!--SAPN4 BEGIN-->
						<div id="add_permission_category_div">
						</div>
						<!--SAPN4 END-->
								
					</div>
					
					<div class="tab-pane " id="panel-second">
						<p>
							<#--TODO-->
							<button onclick="persistByClazz('${KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING}','${constantModel.clazzFullName}')">导入新增URL</button>
						</p>
						
						<div class="panel panel-warning" >
							<div class="panel-heading ">URL模块划分</div>
							<div class="panel-body"> 
								<@urlStaticMappingCategoryListTree page=addedPage  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING treeId="2"/>
								<#---->
								
							</div>	
						</div>
					</div>
					<div class="tab-pane " id="panel-third">
						<p>
							<#--TODO-->
							<button onclick="persistByClazz('${KylinboyDeveloperConstant.KYLINBOY_URL_PERSISTER_BY_CLAZZ_MAPPING}','${constantModel.clazzFullName}')">同步到数据库</button>
						</p>
						<div class="panel panel-warning" >
							<div class="panel-heading ">URL模块划分</div>
							<div class="panel-body"> 
								<@urlStaticMappingCategoryListTree page=modifiedPage  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING treeId="3"/>
							<#---->
							</div>	
						</div>
					</div>
					
					<div class="tab-pane " id="panel-fourth">
						<p>
							<button>移除所选URL</button>
						</p>
						<div class="panel panel-warning" >
							<div class="panel-heading ">URL模块划分</div>
							<div class="panel-body"> 
								<@urlStaticMappingCategoryListTree page=existPage  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING treeId="4"/>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
					$("#tree_urlStaticMapping1").dynatree({
			        	 title: "urlStaticMapping", 
			        	 minExpandLevel: 2,
			        	 checkbox: false, // Show checkboxes.
			        	 classNames: {checkbox: "dynatree-checkbox"},
			  			 selectMode: 2, // 1:single, 2:multi, 3:multi-hier
			  			 autoFocus: true,
			             onActivate: function(node) {
				             
			            },
			            onClick: function(flag,node){
			            	
			            },
			            onSelect: function(flag, node) {
				            
				        }
			        });
			       $("#tree_urlStaticMapping2").dynatree({
			        	 title: "urlStaticMapping", 
			        	 minExpandLevel: 2,
			        	 checkbox: false, // Show checkboxes.
			        	 classNames: {checkbox: "dynatree-checkbox"},
			  			 selectMode: 2, // 1:single, 2:multi, 3:multi-hier
			  			 autoFocus: true,
			             onActivate: function(node) {
				             
			            },
			            onClick: function(flag,node){
			            	
			            },
			            onSelect: function(flag, node) {
				            
				        }
			        });
			         $("#tree_urlStaticMapping3").dynatree({
			        	 title: "urlStaticMapping", 
			        	 minExpandLevel: 2,
			        	 checkbox: false, // Show checkboxes.
			        	 classNames: {checkbox: "dynatree-checkbox"},
			  			 selectMode: 2, // 1:single, 2:multi, 3:multi-hier
			  			 autoFocus: true,
			             onActivate: function(node) {
				             
			            },
			            onClick: function(flag,node){
			            	
			            },
			            onSelect: function(flag, node) {
				            
				        }
			        });
			        $("#tree_urlStaticMapping4").dynatree({
			        	 title: "urlStaticMapping", 
			        	 minExpandLevel: 2,
			        	 checkbox: false, // Show checkboxes.
			        	 classNames: {checkbox: "dynatree-checkbox"},
			  			 selectMode: 2, // 1:single, 2:multi, 3:multi-hier
			  			 autoFocus: true,
			             onActivate: function(node) {
				             
			            },
			            onClick: function(flag,node){
			            	
			            },
			            onSelect: function(flag, node) {
				            
				        }
			        });
			        
			        function persistByClazz(action,clazzFullName){
			        	$.ajax({
						url: action,
						data: {'clazzFullName':clazzFullName},
						type: "POST",
						dataType : "json",
						contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
						success: function(data) {
								alert(data.msg);
								$('#win_static_url_list').window('close');
								window.location.reload();
						},
						error: function() {
							alert( "Sorry, there was a problem!" );
						},
						complete: function() {
							//alert('complete');
						}
					});
			        }
			     
</script>
</div>
</div>
