<#macro displayExistPermission permissionList>
<div class="panel panel-info">
		<div class="panel-heading ">已存在的角色</div>
			<#if permissionList??>
				<dl class="dl-horizontal">
				<#list permissionList as permission>
					<dt>${permission.name}</dt>
					<dd>
						[ ${permission.code} ] ${permission.permissionAlias} [ ${permission.permissionLevel} ]
					</dd>
				</#list>
				</dl>
			</#if>
	</div>

</#macro>

<#macro selectPermission permissionList>
	<div class="panel panel-info">
		<div class="panel-heading ">分配角色</div>
			<@selectPermissionInLine permissionList=permissionList/>
	</div>
</#macro>

<#macro selectPermissionInLine permissionList checkboxName="permissionIdList">
			<#if permissionList??>
				<ul class="list-inline">
				<#list permissionList as permission>
					<li>
					<input type="checkbox" name="${checkboxName}" value="${permission.uniqueIdentifier}">
					${permission.permissionname}
					<li>
				</#list>
				</ul>
			</#if>
</#macro>


<#-- 权限列表 -->
<#macro listPermissionTree page>
<#assign permissionList = page.content />
<div class="table-responsive">

  <table class="table table-striped">
  <@form.form commandName="permission" id="permission_form_id" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING}/authority_list">
  <thead>
<tr>
      <th width="4%"></th>
      <th width="12%"><input type="text" placeholder="权限名称" size="6" name="name" value="<#if permission??>${permission.name}</#if>"/></th>
      <th width="14%"><input type="text" placeholder="权限代码" size="6" name="code" value="<#if permission??>${permission.code}</#if>"/></th>
      <th width="12%"><input type="text" placeholder="资源路径" size="10" name="resourceUrl" value="<#if permission??>${permission.resourceUrl}</#if>"/></th>
      <th width="14%"><input type="text" placeholder="操作代码" size="10" name="opration" value="<#if permission??>${permission.opration}</#if>"/></th>
      <th width="12%"><input type="text" placeholder="全路径" size="10" name="fullUrl" value="<#if permission??>${permission.fullUrl}</#if>"/></th>
      <th width="5%">
      </th>
      <th width="8%"><input type="text" placeholder="开发者ID" size="5" name="developerId" value="<#if permission??>${permission.developerId}</#if>"/></th>
      <th width="10%">
      	<input type="button" value="搜索" class="btn btn-primary" onclick="formSubmit(1)"/>
      </th>
    </tr>
    <tr>
      <th width="4%">Id</th>
      <th width="12%">权限名称</th>
      <th width="14%">权限代码</th>
      <th width="10%">资源路径</th>
      <th width="14%">操作代码</th>
      <th width="12%">全路径</th>
      <th width="5%">次数</th>
      <th width="8%">开发者ID</th>
      <th width="10%">操作</th>
    </tr>
  </thead>
  
  <tbody>
  	
  <#if (permissionList??)>
  	 <#assign depth = 0 />  
  	 <#assign countNum = 0 /> 
		<@listPermissionTreeItem page=page/>
	
  </#if>
    <tr>
		<td colspan="9">
		<#if (permissionList??)>
			<button onClick="saveSubmit('${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING}/authority_list')" class="btn btn-primary">保存列表 </button>
		</#if>
		
			<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING}/authority_add"/>'>
				<input type="button" value="添加权限" class="btn btn-primary"/>
			</a>
		
			<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
	 <tr><td colspan="9"><@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_UI_MAPPING}/authority_list" formId="permission_form_id"/></td></tr>
	</@form.form>
   </tbody>
  </table>
</div>
<script type="text/javascript">
function saveSubmit(action){
	$("#permission_form_id").attr("action",action);
	$("#permission_form_id").submit();
}
</script>
</#macro>

<#macro listPermissionTreeItem page>
<#assign permissionList = page.content />
	<#if permissionList??>
		<#list permissionList as permission>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth/>
			
			 <tr ${trClass}>
			 <td>${permission.uniqueIdentifier} <#list 1..depth as i> > </#list>
			 <input type="hidden" name="permissionList[${countNum}].id" value="${permission.uniqueIdentifier}"/>
			 </td>
			 <td>
			 <input type="text" size="5" name="permissionList[${countNum}].name" value="${permission.name}" class="form-control" id="name[${countNum}]" placeholder="输入名称"/>
			 </td>
			 <td>
			 <input type="text" size="3" name="permissionList[${countNum}].code" value="${permission.code}" class="form-control" id="code[${countNum}]" placeholder="输入代码"/>
			 </td>
			 <td>
			 <div style="width:100px;word-wrap:break-word;" >${permission.resourceUrl}</div>
			 <#--<input type="text" size="7" name="permissionList[${countNum}].resourceUrl" value="${permission.resourceUrl}" class="form-control" id="resourceUrl[${countNum}]" placeholder="输入别名"/>
			 -->
			 </td>
			  <td>
			  <#--操作代码若为空，取权限代码-->
			 <input type="text" size="3" name="permissionList[${countNum}].opration" value="${permission.opration?default(permission.code)}" class="form-control" id="opration[${countNum}]" placeholder="输入操作代码"/>
			 </td>
			 <td width="150px">
			 	<#assign _fullPath="${permission.fullUrl}"/>
			 	<div style="width:120px;word-wrap:break-word;" > <@permissionFullPath permission=permission/></div>
			    <input type="hidden" name="permissionList[${countNum}].fullUrl" value="<#if permission.fullUrl??>${permission.fullUrl}<#else>${_fullPath}</#if> " class="form-control" id="fullUrl[${countNum}]" placeholder="输入全路径"/>
			 </td>
			 <td>
			 ${permission.oprationCount}
			 <#--<input type="text" name="permissionList[${countNum}].oprationCount" value="${permission.oprationCount}" class="form-control" id="oprationCount[${countNum}]" placeholder="操作次数"/>
			  -->
			 </td>
			 <td>
			 ${permission.developer.userId.userId}
			 <#--<input type="text" name="permissionList[${countNum}].developer.userId.userId" value="${permission.developer.userId.userId}" class="form-control" id="userId[${countNum}]" />
			 -->
			 </td>
			 <td>
			 <ul class="list-inline"> 
				 <#if permission.code?? && permission.code!='top_Navigation'>
					<li>
				 		<a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_MODIFY_UI_MAPPING}/${permission.id}'>编辑</a>
				  	</li>
				  </#if>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/delete/${permission.uniqueIdentifier}"/>'>删除</a>
				    </li>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${permission.uniqueIdentifier}"/>'>保存</a>
				    </li>
				     <li>
				    <a class="text-danger" id="addDirectPermission_id_${permission.uniqueIdentifier}" onclick="addDirectPermission('${KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING}',${permission.uniqueIdentifier},'<#if permission.directPermission??>${permission.directPermission.uniqueIdentifier}<#else>0</#if>')" href='javascript:void(0)'>加入白名单</a>
				    </li>
				    <li>
				    <a class="text-danger" id="addDirectPermission_id_${permission.uniqueIdentifier}" onclick="addToNavigation('${KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_MAPPING}','${permission.uniqueIdentifier}')" href='javascript:void(0)'>加入菜单</a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign countNum = countNum + 1 />
			 <#assign depth = depth-1 />
		</#list>
		<script type="text/javascript">
			function addDirectPermission(url,permissionId,directPermissionId){
				if(directPermissionId==''){
					directPermissionId=0;
				}
				
				var requestData={
					"permissionId":permissionId,
					"directPermissionId":directPermissionId
				}
				$.ajax({
			       type: "POST",
			       url: url,
			       data: requestData,
			       dataType:"json",
			       async: true,
			       error: function(data, error) {
			       		alert(error);
			       },
			       success: function(data)
			       {
			       	alert(data.message);
			       }
				}); 
			}
			
			function addToNavigation(url,permissionId){
			
				var requestData={
					"permissionId":permissionId
				}
				$.ajax({
			       type: "POST",
			       url: url,
			       data: requestData,
			       dataType:"json",
			       async: true,
			       error: function(data, error) {
			       		alert(error);
			       },
			       success: function(data)
			       {
			       	alert(data.message);
			       }
				}); 
			}
		</script>
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>

<#macro permissionFullPath permission>
<#if permission??>
<#assign _fullPath="${permission.resourceUrl}${permission.developer.oprationSeperator}${permission.developer.seperator}${permission.code}${permission.developer.seperator}${permission.developer.oprationSeperator}${permission.developer.userIdSeperator}${permission.developer.userId.seperatorShare}${permission.developer.userId.userId}${permission.developer.userId.seperatorShare}${permission.developer.userIdSeperator}"/>
${_fullPath}
</#if>
</#macro>
<#macro queryPermission permission>
<@form.form commandName="permission" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING}/authority_list">

</@form.form>
</#macro>

<#macro permissionCategoryIndex page>



</#macro>

<#--权限分类目录 选择父类 begin-->

<#macro selectParentCategoryRadioListPanel page currentCategory radioName="parent.id">
<div class="panel panel-warning" >
			<div class="panel-heading ">选择父类</div>
			<div class="panel-body"> 
			<@selectParentCategoryRadioList children=page.content currentCategory=currentCategory/>
			</div>
</div>
</#macro>

<#macro selectParentCategoryRadioList children currentCategory radioName="parent.id">  
<#assign depth = 1 />
<@selectParentCategoryRadio children=children currentCategory=currentCategory radioName=radioName/>
</#macro>

<#macro selectParentCategoryRadio children currentCategory radioName="parent.id">  
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
        	<#list 1..depth as i>- - </#list>
            <#if currentCategory?? && currentCategory.id!=child.id>
            	<input type="Radio" value="${child.uniqueIdentifier}" name="${radioName}" id="select_parent_category_${child.uniqueIdentifier}" <#if currentCategory?? && currentCategory.parent?? && currentCategory.parent.id == child.id> checked</#if>/>
            </#if>
	             <#if currentCategory?? && currentCategory.id==child.id>
	             	<font color="red">
	             </#if>
             ${(child.name)?if_exists}
	             <#if currentCategory?? && currentCategory.id==child.id>
	             	</font>
	             </#if>
             
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            	<@selectParentCategoryRadio children = child.childrenCategory currentCategory=currentCategory radioName=radioName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro>
<#--权限分类目录 选择父类 end-->

<#--添加权限类别 begin-->
<#macro addPermissionCategoryForm page currentCategory radioName="parent.id">
<div class="row">
	<@form.form commandName="pmnCategory" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ADD_MAPPING}">
	<div class="col-md-8">
		<div class="panel panel-warning" >
							<div class="panel-heading ">添加/修改 权限分类</div>
							<div class="panel-body"> 
								<div class="col-sm-9">
								
							    <input type="hidden" name="id" value="${id}" class="form-control" id="permissionCategory_id" />
							    <input type="text" name="name" value="${name}" class="form-control" id="permissionCategory_name" placeholder="输入类别名称" required="true"/>
							  	</div>

							    <div class="col-sm-9">
							    	<input type="text" name="code" value="${code}" class="form-control" id="permissionCategory_code" placeholder="输入类别代码" >
							  	</div>
							  
							 
							    <div class="col-sm-9">
							    <@form.textarea path="description" class="form-control" id="permissionCategory_description" placeholder="输入类别描述" />
							  	</div>
							  
							  <ul class="list">
									  <li>
								       		<input type="submit" value="保存" class="btn btn-primary"/>
								       </li>
								       <li>
								       		<input type="button" onClick="cleanValue()" value="清空" class="btn btn-primary"/>
								       </li>
						       </ul>
						       </div>	
		</div>		
		
	</div>
	<div class="col-md-4">
					<@selectParentCategoryRadioListPanel page=page currentCategory=currentCategory/>
	</div>
	<script type="text/javascript">
		function cleanValue(){
			$("#permissionCategory_id").val("");
			$("#permissionCategory_name").val("");
            $("#permissionCategory_code").val("");
            $("#permissionCategory_description").val("");
			$(":radio:checked").val("");
            $(":radio:checked").attr("checked",false);
		}
	</script>
	</@form.form>
</div>
</#macro>
<#--添加权限类别 end-->








<#-- 树形权限分类目录含权限列表 begin-->
<#macro permissionCategoryListIncludePermissionTree page expandedCategory='first' radioName="parent.id" removeUrl="">  
<#if page.content?? && page.content?size gt 0>  
${removeUrl}
点击<button>移除</button>将会 移除 在该类别下的权限
<div id="tree_permission">
	<ul id="treeData_permission" style="display: block;">
	 	<#assign depth = 1 />  
		<@permissionCategoryIncludePermissionItem children=page.content expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/>
	</ul>
</div>
</#if>
</#macro>

<#macro permissionCategoryIncludePermissionItem children expandedCategory='first' radioName="parent.id" removeUrl="">  
    <script type="text/javascript">
    	function removeCategory(removeUrl,id){
    		if(id !="" && removeUrl != ""){
    			var _removeUrl = removeUrl + id;
    			window.location.href= _removeUrl;
    		}
    	}
    </script>
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
        	
            <#if expandedCategory?? >
	            
	            <li id="id_${child.id}" 
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
		           		class="folder"
		         </#if>
	            >
            	
		            	<input type="hidden" value="${child.uniqueIdentifier}" name="${radioName}" id="category_${child.uniqueIdentifier}" <#if expandedCategory??&& expandedCategory!='first' && expandedCategory.parent?? && expandedCategory.parent.id == child.id> checked</#if>/>
		           		<label id="description_${child.uniqueIdentifier}" title='${(child.description)?if_exists}'>
		           		<span id="name_${child.uniqueIdentifier}">${(child.name)?if_exists}</span>
		           		【<span id="code_${child.uniqueIdentifier}">${(child.code)?if_exists}</span>】
		             	</label>
             
	            <#assign depth = depth + 1 />  
	            <ul> 
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
	            	<@permissionCategoryIncludePermissionItem children = child.childrenCategory expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/> 
	            </#if>
	            <#if child.permissionList?? && child.permissionList?size gt 0> 
	            	<@permissionCategoryIncludePermissionDetail children = child.permissionList radioName=radioName removeUrl=removeUrl/> 
	            	</#if>
	            </ul> 
	            <#assign depth = depth - 1 /> 
             	</li>
             </#if>
        </#list>  
      
    </#if>  	
</#macro>

<#macro permissionCategoryIncludePermissionDetail children radioName="parent.id" removeUrl="">  
    <script type="text/javascript">
    	function removePermissionFromCategory(formId,categoryId,permissionId){
    		if(categoryId !="" && formId != "" && permissionId!=""){
    			//alert(formId + " " + categoryId + " " + permissionId);
    			$("#"+formId).submit();
    		}
    	}
    	
    </script>
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
	          <script type="text/javascript">
	            
    </script>
	            <li class="text-danger" id="permission_id_${child.id}" title='${(child.description)?if_exists}'>
            	
		             	<@form.form commandName="permission" id="permisson_category_form_id_${child.uniqueIdentifier}" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING}#panel-second">
		           		<span>
		           		----
		           		<span class="text-danger" id="name_${child.uniqueIdentifier}">${(child.name)?if_exists}</span>
		           		【<span class="text-danger" id="code_${child.uniqueIdentifier}">${(child.code)?if_exists}</span>】
		           		【<span class="text-danger" id="fullUrl_${child.uniqueIdentifier}">${(child.fullUrl)?if_exists}</span>】
						
		             	<input type="hidden" id="permission_id_${child.uniqueIdentifier}" name="permissionList[0].id" value="${child.uniqueIdentifier}"  >
		             	<input type="hidden" id="permission_category_id_${child.uniqueIdentifier}" name="id" value="${child.category.id}"  />
		             	
		             	<button onclick="removePermissionFromCategory('permisson_category_form_id_${child.uniqueIdentifier}','permission_category_id_${child.uniqueIdentifier}','permission_id_${child.uniqueIdentifier}')">移除</button>
		             	
		             	</span>
		             	</@form.form>
             	</li>
        </#list>  
      
    </#if>  	
</#macro>

<#-- 树形权限分类目录含权限列表 end-->

<#-- 权限列表 view begin-->
<#-- 权限列表 -->
<#macro listPermissionViewTree page>
<#assign permissionList = page.content />
点击<button>移除</button>将会 移除 在 类别 下的该权限, <span class="text-success"> 绿色 标题 </span>表示 已生效
<div class="table-responsive">
<@form.form commandName="permission" id="permisson_category_form_id" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING}#panel-second">
  <table class="table table-condensed table-striped">
  <thead>
	<tr>
      <th width="1%"></th>
      <th width="8%"><input type="text" placeholder="权限名称" size="6" name="name" value="<#if permission??>${permission.name}</#if>"/></th>
      <th width="10%"><input type="text" placeholder="权限代码" size="6" name="code" value="<#if permission??>${permission.code}</#if>"/></th>
      <th width="15%"><input type="text" placeholder="全路径" size="15" name="fullUrl" value="<#if permission??>${permission.fullUrl}</#if>"/></th>
      <th width="8%"><input type="text" placeholder="开发者ID" size="5" name="developerId" value="<#if permission??>${permission.developerId}</#if>"/></th>
      <th width="5%">
      <input type="button" value="搜索" class="btn btn-primary" onclick="formSubmit(1)"/>
      </th>
      <th width="15%">
      	<select class="form-control" name="categoryDropdownValue" id="categoryDropdownValue" value="<#if permission??>${permission.categoryDropdownValue}</#if>" required="true">
	    <#assign typeMaps=StaticDataDefineManager.getData(StaticDataDefineConstant.COMMON_PERMISSION_CATEGORY_DROPDOWN)/>
		    <#list typeMaps.keySet() as key>
		    	 <option value="${key}"  <#if permission?? && permission.categoryDropdownValue==key>selected</#if>>${typeMaps.get(key)}</option> 
		    </#list>
		</select>
      </th>
    </tr>
    <tr>
      <th width="1%">Id</th>
      <th width="8%">权限名称</th>
      <th width="10%">权限代码</th>
      <th width="15%">全路径</th>
      <th width="8%">开发者ID</th>
      <th width="5%">选择
      <input type="checkbox" id="checkbox_id_select_all" name="checkbox_id_select_all" value="true"/>
      </th>
      <th width="15%">操作</th>
    </tr>
  </thead>
  
  <tbody>
  <#if (permissionList??)>
  	 <#assign depth = 0 />  
  	 <#assign countNum = 0 /> 
		<@listPermissionViewTreeItem page=page/>
  </#if>
    <tr>
		<td colspan="7" class="text-align">
		
		<div class="panel panel-warning" id="permission_list_div">
			<div class="panel-heading ">对权限进行操作 第三步:【应用操作】</div>
			<div class="panel-body"> 
				<#if (permissionList??)>
					<input type="hidden" id="permission_category_id" name="id" value=""  >
					
					<input type="button" onClick="assignPermissionToCategory('permisson_category_form_id','${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING}#panel-second')" value="归类所选" class="btn btn-primary"/>
				</#if>
						
					<input type="button" value="移除所选权限" class="btn btn-primary" onClick="removeSelectedPermissionFromCategory('permisson_category_form_id','${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING}#panel-second')"/>
						权限审核流程：
						<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING}/authority_add"/>'>
							<input type="button" value="接受所选权限" class="btn btn-primary"/>
						</a>
					
						<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING}/authority_add"/>'>
							<input type="button" value="权限审核通过" class="btn btn-primary"/>
						</a>
					
						<input type="button" value="权限审核不通过" class="btn btn-primary"/>
					
						<input type="button" value="启用权限" class="btn btn-primary"/>
						
						<input type="button" value="禁用权限" class="btn btn-primary"/>
			</div>	
		</div>
		</td>
	 </tr>
	 <tr><td colspan="7"><@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_INDEX_MAPPING}#panel-second" formId="permisson_category_form_id"/></td></tr>
   </tbody>
  </table>
</@form.form>
  <script type="text/javascript">
  	function assignPermissionToCategory(formId, action,permissionId){
  		if(formId != "" && formId != null && action != null && action !=""){
  			var _permission_category_id_value = $("#permission_category_id").val();
  			if(_permission_category_id_value != "" && _permission_category_id_value != null){
  			
	 			//alert(_permission_category_id_value);
	 			
  				$("#permisson_category_form_id").attr("action",action);
  				
  				$("#permisson_category_form_id").submit();
  			}else{
  				alert("请选择归类目录");
  			}
  		}
  	}
  	
  	function removeSelectedPermissionFromCategory(formId, action){
  		if(formId != "" && formId != null && action != null && action !=""){
  			
  				$("#permisson_category_form_id").attr("action",action);
  				
  				$("#permisson_category_form_id").submit();
  		}
  	}
  </script>
  
</div>
</#macro>

<#macro listPermissionViewTreeItem page>
<#assign permissionList = page.content />
	<#if permissionList??>
		<#list permissionList as permission>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth/>
			
			 <tr class="">
			 <td>${permission.uniqueIdentifier}
			 </td>
			 <td>
			<span  <#if permission?? && permission.category?? && permission.category.id gt 0> class="text-success" </#if>  >
			${permission.name}
			<span>
			 </td>
			 <td>
			 ${permission.code}
			 </td>
			 <td >
			 	<#assign _fullPath="${permission.fullUrl}"/>
			    <@permissionFullPath permission=permission/>
			 </td>
			 <td>
			 ${permission.developer.userId.userId}
			 </td>
			 <td>
			 <input type="checkbox" id="checkbox_id_${permission.uniqueIdentifier}" name="permissionList[${countNum}].id" value="${permission.uniqueIdentifier}"/>
			 </td>
			 <td>
			 <ul class="list-inline"> 
				 <#if permission.code?? && permission.code!='top_Navigation'>
					<li>
				 		<a   <#if permission?? && permission.category?? && permission.category.id gt 0> class="text-success" <#else> class="text-danger"</#if>   href="javascript:void(0)" onclick="assignPermissionToCategory('permisson_category_form_id','${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_ASSIGN_MAPPING}#panel-second')">归类</a>
				  	</li>
				  </#if>
				    <li>
				    	<a <#if permission?? && permission.category?? && permission.category.id gt 0> class="text-danger" <#else> class="text-success"</#if>  href="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_ALL_FROM_MAPPING}/${permission.uniqueIdentifier}#panel-second">移除</a>
				    </li>
				    <li>
				    	<a  href='${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ENABLE_MAPPING}/${permission.uniqueIdentifier}#panel-second'><span <#if permission?? && permission.status=Permission.STATUS_ENABLE> class="text-success" <#else> class="text-danger" </#if>>启用</span></a>
				    </li>
				     <li>
				    	<a <#if permission?? && permission.directPermission??> class="text-success" <#else> class="text-danger" </#if> id="addDirectPermission_id_${permission.uniqueIdentifier}" onclick="addDirectPermission('${KylinboyDeveloperConstant.KYLINBOY_DIRECT_PERMISSION_ADD_MAPPING}',${permission.uniqueIdentifier},'<#if permission.directPermission??>${permission.directPermission.uniqueIdentifier}<#else>0</#if>')" href='javascript:void(0)'>加入白名单</a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign countNum = countNum + 1 />
			 <#assign depth = depth-1 />
		</#list>
		<script type="text/javascript">
			function addDirectPermission(url,permissionId,directPermissionId){
				if(directPermissionId==''){
					directPermissionId=0;
				}
				
				var requestData={
					"permissionId":permissionId,
					"directPermissionId":directPermissionId
				}
				$.ajax({
			       type: "POST",
			       url: url,
			       data: requestData,
			       dataType:"json",
			       async: true,
			       error: function(data, error) {
			       		alert(error);
			       },
			       success: function(data)
			       {
			       		$("#addDirectPermission_id_" + permissionId).removeClass("text-danger");
			       		$("#addDirectPermission_id_" + permissionId).addClass("text-success");
			       		alert(data.message);
			       }
				}); 
			}
		</script>
	</#if>
		
</#macro>
<#-- 权限列表 view end-->

<#-- 树形权限分类目录列表 begin -->
<#macro permissionCategoryListTree page expandedCategory='first' radioName="parent.id" removeUrl="">  
<#if page.content?? && page.content?size gt 0>  
${removeUrl}
<div id="tree">
	<ul id="treeData" style="display: block;">
	 	<#assign depth = 1 />  
		<@permissionCategoryItem children=page.content expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/>
	</ul>
</div>
</#if>
</#macro>

<#macro permissionCategoryItem children expandedCategory='first' radioName="parent.id" removeUrl="">  
    <script type="text/javascript">
    	function removeCategory(removeUrl,id){
    		if(id !="" && removeUrl != ""){
    			var _removeUrl = removeUrl + id;
    			window.location.href= _removeUrl;
    		}
    	}
    </script>
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
        	
            <#if expandedCategory?? >
	            
	            <li id="id_${child.id}" 
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
		           		class="folder expanded"
		         </#if>
	            
	            >
            	
		            	<input type="hidden" value="${child.uniqueIdentifier}" name="${radioName}" id="category_${child.uniqueIdentifier}" <#if expandedCategory??&& expandedCategory!='first' && expandedCategory.parent?? && expandedCategory.parent.id == child.id> checked</#if>/>
		           		<label id="description_${child.uniqueIdentifier}" title='${(child.description)?if_exists}'>
		           		<span id="name_${child.uniqueIdentifier}">${(child.name)?if_exists}</span>
		           		【<span id="code_${child.uniqueIdentifier}">${(child.code)?if_exists}</span>】
		             	</label>
	             	
	             	
	             	 	<button onClick="removeCategory('${removeUrl}','${child.uniqueIdentifier}')">删除</button> 
	             	 
             
	            <#assign depth = depth + 1 />  
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
	            	<ul> 
	            	<@permissionCategoryItem children = child.childrenCategory expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/> 
	            	</ul> 
	            </#if>
	            <#assign depth = depth - 1 /> 
             	</li>
             </#if>
        </#list>  
      
    </#if>  	
</#macro>
<#-- 树形权限分类目录列表 end -->







<#-- 树形权限分类目录列表 for 角色 begin -->
<#macro permissionCategoryListIncludePermissionTreeForRole page expandedCategory='first' radioName="parent.id" removeUrl="">  
<#if page.content?? && page.content?size gt 0>  
<div id="tree_permission">
	<ul id="treeData_permission" style="display: block;">
	 	<#assign depth = 1 />  
		<@permissionCategoryIncludePermissionItemForRole children=page.content expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/>
	</ul>
</div>
</#if>
</#macro>

<#macro permissionCategoryIncludePermissionItemForRole children expandedCategory='first' radioName="parent.id" removeUrl="">  
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
        	
            <#if expandedCategory?? >
	            
	            <li id="id_${child.id}" 
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
		           		class="folder"
		         </#if>
	            >
            	
		            	<input type="hidden" value="${child.uniqueIdentifier}" name="${radioName}" id="category_${child.uniqueIdentifier}" <#if expandedCategory??&& expandedCategory!='first' && expandedCategory.parent?? && expandedCategory.parent.id == child.id> checked</#if>/>
		           		<label id="description_${child.uniqueIdentifier}" title='${(child.description)?if_exists}'>
		           		<span id="name_${child.uniqueIdentifier}">${(child.name)?if_exists}</span>
		           		【<span id="code_${child.uniqueIdentifier}">${(child.code)?if_exists}</span>】
		             	</label>
             
	            <#assign depth = depth + 1 />  
	            <ul> 
	            <#if child.childrenCategory?? && child.childrenCategory?size gt 0> 
	            	<@permissionCategoryIncludePermissionItemForRole children = child.childrenCategory expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/> 
	            </#if>
	            <#if child.permissionList?? && child.permissionList?size gt 0> 
	            	<@permissionCategoryIncludePermissionDetailForRole children = child.permissionList radioName=radioName removeUrl=removeUrl/> 
	            	</#if>
	            </ul> 
	            <#assign depth = depth - 1 /> 
             	</li>
             </#if>
        </#list>  
      
    </#if>  	
</#macro>

<#macro permissionCategoryIncludePermissionDetailForRole children radioName="parent.id" removeUrl="">  
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
	          <script type="text/javascript">
	            
    </script>
	            <li class="text-danger" id="permission_id_${child.id}" title='${(child.description)?if_exists}'>
            	
		             	<@form.form commandName="permission" id="permisson_category_form_id_${child.uniqueIdentifier}" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING}#panel-second">
		           		<span>
		           		----
		           		<span class="text-danger" id="name_${child.uniqueIdentifier}">${(child.name)?if_exists}</span>
		           		【<span class="text-danger" id="code_${child.uniqueIdentifier}">${(child.code)?if_exists}</span>】
		           		【<span class="text-danger" id="fullUrl_${child.uniqueIdentifier}">${(child.fullUrl)?if_exists}</span>】
						
		             	<input type="hidden" id="permission_id_${child.uniqueIdentifier}" name="permissionList[0].id" value="${child.uniqueIdentifier}"  >
		             	<input type="hidden" id="permission_category_id_${child.uniqueIdentifier}" name="id" value="${child.category.id}"  />
		             	
		             	</span>
		             	</@form.form>
             	</li>
        </#list>  
      
    </#if>  	
</#macro>
<#-- 树形权限分类目录列表 for 角色 end -->
