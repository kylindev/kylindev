<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_gallery.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>修改相片</title>
</head>
<body>
<div class="container">
<div class="row">

  <div class="col-md-8">
  <p class="text-success">修改相片</p>
  <@form.form commandName="photograph" role="form" action="/admin/photograph/photographModify">
  <@spring.bind "photograph.uniqueIdentifier" /> 
  <@form.hidden path="id"/>
      <@spring.bind "photograph.title" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="name">相片名称: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="title" class="form-control" id="title" placeholder="输入相片名称" required="true"/>
	  </div>
	  <@spring.bind "photograph.description" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">相片描述: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="description" class="form-control" id="description" placeholder="输入相片描述" />
	  </div>
	  
	  <@spring.bind "photograph.author" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">摄影人: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="author" class="form-control" id="author" placeholder="输入摄影人" />
	  </div>
	  
	  <@spring.bind "photograph.snatTime" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">摄影时间: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="snatTime" class="form-control" id="snatTime" placeholder="输入摄影时间" />
	  </div>
	  
	    <@spring.bind "photograph.snatPlace" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">摄影地点: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="snatPlace" class="form-control" id="snatPlace" placeholder="输入摄影地点" />
	  </div>
	  <@spring.bind "photograph.imagePath" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">相片路径: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="imagePath" class="form-control" id="imagePath" placeholder="输入相片路径" />
	 	<input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','imagePath')"/><br/>
	    <div id="imagePath_img">
	    	<img src="${photograph.imagePath}" title="相片" alt="独家记忆" class="img-rounded">
	    </div>
	  </div>
	  <@spring.bind "photograph.imagePathAd" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">相片缩略图路径: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="imagePathAd" class="form-control" id="imagePathAd" placeholder="输入相片路径" />
	    
	  </div>
	  
	  
	  <@spring.bind "photograph.sequence" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">顺序: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="sequence" class="form-control" id="sequence" placeholder="输入相片排序，使用数字1,2,3..." required="true"/>
	  </div>
	  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="parent.id"><input type="button" value="选择影集" id="parent_id_button" class="btn btn-primary"/>: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
		<div>
		<#assign depth = 1 />  
		<@spring.bind "photograph.gallery" />  
		<#assign currentGallery = photograph.gallery /> 
	  	<@selectGalleryRadio children=galleryList currentGallery=currentGallery />
	  	</div>
	  </div>
	 <#--  -->
		  
	  
	   <div class="col-md-12">
       	<input type="submit" value="保存设置" class="btn btn-primary"/>
       </div>
       
 
	</@form.form>
  
  </div>
</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/ckeditor/adapters/jquery.js"></script>
<div id="filepathTextFieldId_hidden"></div>
<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
<script src="/assets/common/js/jquery/jquery.json-2.4.min.js"></script>
</body>
</html>