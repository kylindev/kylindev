<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<#include "definefunction_urlMapping.ftl">
<#include "definefunction_page.ftl">
<#--<@urlStaticMappingModuleForNavigation page=page/>-->
	<div id="urlStaticMapping_div"> 
		 <@urlStaticMappingListForNavigation page=page/>
		 <form id="urlMapping_form_id" action="${KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING}" method="post">
		 	<input type="hidden" data="data" name="idKey" value="${urlStaticMapping.idKey}"/>
		 	<@pageBarAjax page=page url='${KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING}' formId='urlMapping_form_id' urlFirst='true' showNum=7 backDataContainerId='url_list_content_div'/>
		 </form>
	</div>


