<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>文章分类列表</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<p class="text-success">文章分类列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>类别名称</th>
      <th>代码</th>
      <th>排序</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "categoryList" />  
  <@form.form commandName="categoryList" role="form" action="/admin/article/category/categoryList/modify">
  <tbody>
  <#if (categoryList??)>

	<#list categoryList as category>  


	 <tr>
	 <td>${category.uniqueIdentifier}
	 <input type="hidden" name="categoryList[${category_index}].id" value="${category.uniqueIdentifier}">
	 </td>
	 <td>
	 <input type="text" name="categoryList[${category_index}].name" value="${category.name}" class="form-control" id="code[${category_index}]" placeholder="输入代码"/>
	 </td>
	 <td>
	 <input type="text" name="categoryList[${category_index}].code" value="${category.code}" class="form-control" id="name[${category_index}]" placeholder="输入名称"/>
	 </td>
	 <td>
	 <input type="text" name="categoryList[${category_index}].sequence" value="${category.sequence}" class="form-control" id="sequence[${category_index}]" placeholder="站点路径"/>
	 </td>
	 <td>
	 <a class="text-danger" href='<@spring.url relativeUrl="/admin/article/category/categoryModifyUI/${category.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/article/category/delete/${category.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/article/category/modify/${category.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
	 </td>
	 </tr>
	</#list>
	<tr>
		<td>
		<input type="submit" value="保存列表" class="btn btn-primary"/>
		</td>
		<td>
		<a class="text-danger" href='<@spring.url relativeUrl="/admin/article/category/categoryAddUI"/>'>
		<input type="button" value="添加分类" class="btn btn-primary"/>
		</a>
		</td>
		<td>
		<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
  </#if>
   </tbody>
  </table>
</div>
 </div>
 </@form.form>
</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>