<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>影集列表</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-9">
<p class="text-success">影集列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>主题</th>
      <th>描述</th>
      <th>发布时间</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "galleryList" />  
  <@form.form commandName="galleryList" role="form" action="/galleryUI">
  <tbody>
  <#if (galleryList??)>

	<#list galleryList as gallery>  


	 <tr>
	 <td>${gallery.uniqueIdentifier}
	 <input type="hidden" name="galleryList[${gallery_index}].id" value="${gallery.uniqueIdentifier}">
	 </td>
	 <td>
	 <input type="text" name="galleryList[${gallery_index}].theme" value="${gallery.theme}" class="form-control" id="theme[${gallery_index}]" placeholder="输入标题"/>
	 </td>
	 <td>
	 <input type="text" name="galleryList[${gallery_index}].description" value="${gallery.description}" class="form-control" id="description[${gallery_index}]" placeholder="输入描述"/>
	 </td>
	 <td>
	 <input type="text" name="galleryList[${gallery_index}].publishTime" value="${gallery.publishTime}" class="form-control" id="publishTime[${gallery_index}]" placeholder="拍摄时间"/>
	 </td>
	 <td>
	 <a class="text-danger" href='<@spring.url relativeUrl="/admin/gallery/modifyUI/${gallery.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/gallery/delete/${gallery.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/gallery/modify/${gallery.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
	 </td>
	 </tr>
	</#list>
	<tr>
		<td>
		<input type="submit" value="保存列表" class="btn btn-primary"/>
		</td>
		<td>
		<a class="text-danger" href='<@spring.url relativeUrl="/admin/gallery/galleryAddUI"/>'>
		<input type="button" value="添加影集" class="btn btn-primary"/>
		</a>
		</td>
		<td>
		<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
  </#if>
   </tbody>
  </table>
</div>
 </div>
 </@form.form>
  <div class="col-md-6">
  
  </div>
</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>