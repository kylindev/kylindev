<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_user.ftl">
<#include "definefunction_travel.ftl">
<#include "definefunction_userManageAreaItem.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_page.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>我的管理区-用户管理区项管理</title>
</head>

<body>
<div class="container body_id" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@manageAreaItemIndexNavigationUI currentNavigation=currentNavigation/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="container_content_div">
			<@userManageAreaItemEditUI userManageAreaItem=userManageAreaItem userManageAreaItemList=userManageAreaItemList/>
		</div>
	</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>