<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_user.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户注册</title>
</head>

<div id="top_banner">

	<h3> <img src="/upload/2013/11/13/logodujia.gif"/> Welcome ${name} to jit8.org, jit8 <small>means</small> 就 <span class="text-danger"> <strong>他/她/它</strong></span> 吧！</h3>
	<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>
	<h2 class="text-danger text-center">说走就走，潇洒自由</h2>
</div>

<body>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-4">
			<!--
			<img src="/assets/images/user/register.jpg" title="梧桐山顶，望向远方" alt="梧桐山顶，望向远方" class="img-rounded">
			
			<p class="lead">寻梦，在远方...</p>
			-->
			<div class="panel panel-info">
				<div class="panel-heading ">最新注册</div>
				<div class="panel-body">
					   <@lastRegisterUserListSimple page=page/>
						
						<button class="btn btn-primary" data-toggle="modal" data-target=".bs-lastUser-modal-lg" onclick="getLastRegisterList('${KylinboyCoreDeveloperConstant.KYLINBOY_USER_LAST_REGISTER_LIST_UI_MAPPING}','#lastRegisterContainer')">查看最新注册用户【 50 】名</button>
	
						<div class="modal fade bs-lastUser-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						   	 <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="text-danger">&times;</span><span class="sr-only text-danger">Close</span></button>
						        <h4 class="modal-title text-info">最新注册用户【 50 】名</h4>
						      </div>
						      <div class="modal-body">
						        <div id="lastRegisterContainer">
								</div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-danger" data-dismiss="modal">关闭</button>
						      </div>
						    
						    </div>
						  </div>
						</div>
				  </div>
				
			</div>
		</div>
	  <div class="col-md-8">
		  <div class="panel panel-warning">
			<div class="panel-heading ">注册成功，请进入邮箱激活</div>
			 <h3 class="text-danger"> 注册成功，帐号激活链接已发送至您的注册邮箱
			 <br/>[ <span class="text-info">${EncodeUtil.decodeWithByte(user.email,"utf-8")}</span> ]
			 <br/>激活之后方可登录系统.</h3>
		  </div>
	  </div>
	  <script type="text/javascript">
	  function getLastRegisterList(url,containerId){
	  		var request={};
	  		var dataExist = $(containerId).find("dd");
	  		if(dataExist.length<=0){
	  			getContent(url,request,containerId);
	  		}
	  }
	  </script>
	</div>
</div>

</body>

<div id="body_bottom">
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>