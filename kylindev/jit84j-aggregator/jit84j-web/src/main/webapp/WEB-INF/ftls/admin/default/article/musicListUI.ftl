<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>音乐列表</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-9">
<p class="text-success">音乐列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>音乐名称</th>
      <th>音乐标题</th>
      <th>歌手</th>
      <th>路径</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "musicList" />  
  <@form.form commandName="musicList" role="form" action="/musicUI">
  <tbody>
  <#if (musicList??)>

	<#list musicList as music>  


	 <tr>
	 <td>${music.uniqueIdentifier}
	 </td>
	 <td>
	 <input type="text" name="musicList[${music_index}].name" value="${music.name}" class="form-control" id="code[${music_index}]" placeholder="音乐名称"/>
	 </td>
	 <td>
	 <input type="text" name="musicList[${music_index}].title" value="${music.title}" class="form-control" id="code[${music_index}]" placeholder="音乐标题"/>
	 </td>
	 <td>
	 <input type="text" name="musicList[${music_index}].singer" value="${music.singer}" class="form-control" id="name[${music_index}]" placeholder="歌手"/>
	 </td>
	 <td>
	 <input type="text" name="musicList[${music_index}].musicPath" value="${music.musicPath}" class="form-control" id="sequence[${music_index}]" placeholder="音乐路径"/>
	 </td>
	 <td>
	 <a class="text-danger" href='<@spring.url relativeUrl="/admin/musicUI/${music.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/music/delete/${music.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/music/modify/${music.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
	 </td>
	 </tr>
	</#list>
	
  </#if>
   </tbody>
  </table>
</div>
 </div>
 </@form.form>
  <div class="col-md-6">
  
  </div>
</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>