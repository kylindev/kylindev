<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加角色</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="role" role="form" action="/admin/userinfo/role/addRole/role_list">
	<div class="col-md-8">
	<div class="panel panel-warning" >
		<div class="panel-heading ">请填写以下必填信息</div>
		  
	      <@spring.bind "role.rolename" />  
	      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="rolename_1">角色名称: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div class="col-sm-9">
		    <@form.input path="rolename" class="form-control" id="rolename" placeholder="输入角色名称，必须唯一" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "role.roleCode" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="email">角色代码:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="roleCode" class="form-control" id="roleCode" placeholder="输入角色代码" required="true"/>
		  	</div>
		  </div>
		  
		  <@spring.bind "role.roleAlias" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="password">角色别名:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="roleAlias" class="form-control" id="roleAlias" placeholder="输入角色别名" required="true"/>
		  	</div>
		  </div>
		  
		  
		   <@spring.bind "role.roleLevel" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="roleLevel">角色级别:
		    	<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="roleLevel" class="form-control" id="roleLevel" placeholder="输入角色级别，不能超过999" required="true"/>
		  	</div>
		  </div>
		  <@spring.bind "role.roleOrder" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="lastname">角色排序:
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="roleOrder" class="form-control" id="roleOrder" placeholder="输入角色排序" required="true"/>
		  	</div>
		   </div>
		  
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="comeLocation">角色描述
		    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.textarea path="roleDescription" class="form-control" id="roleDescription" placeholder="输入角色描述" />
		  	</div>
		  </div>
		  
		  <ul class="list-inline">
				  <li>
			       		<input type="submit" value="确认添加" class="btn btn-primary"/>
			       </li>
	       </ul>
	  
	  </div>
	
	</div>
  <div class="col-md-4">
		<@displayExistRole roleList=roleList/>
  </div>
</@form.form>
</div>
</div>

</body>

<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg"/>
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>