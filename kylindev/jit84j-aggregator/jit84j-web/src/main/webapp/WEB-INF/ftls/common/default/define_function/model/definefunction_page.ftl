<#macro pageBar page url='default_url' formId='default_form' urlFirst='true' showNum=7 >  
  <#if page??>
  
		<ul class="pagination">
			<#assign totalPage=page.totalPages/>
			<#assign currentPageNum=page.number+1/>
			<#assign isForm= (formId = 'default_form')/>
			<#if showNum gt 5>
				<#assign showNum= 5/>
			<#elseif showNum gt 15>
				<#assign showNum= 15/>
			</#if>
				<#if currentPageNum gt showNum+1>
					<li ><a href="javascript:void(0)" onClick="formSubmit(${currentPageNum-6})"><span>&laquo;</span></a></li>
					<li ><a href="javascript:void(0)" onClick="formSubmit(${currentPageNum-5})"><span>..</span></a></li>
					<#list currentPageNum-showNum..currentPageNum as i>
					
						<#if currentPageNum = i>
			  				<li class="active"><span>${i}<span class="sr-only">(current)</span></span></li>
						<#else>
							<li ><a href="javascript:void(0)" onClick="formSubmit(${i})"><span>${i}</span></a></li>
						</#if>
						
					</#list>
					
				<#else>
					<#list 1..currentPageNum as i>
						<#if currentPageNum = i>
			  			<li class="active"><span>${i}<span class="sr-only">(current)</span></span></li>
						<#else>
							<li ><a href="javascript:void(0)" onClick="formSubmit(${i})"><span>${i}</span></a></li>
						</#if>
					</#list>
				</#if>	
				
				<#if currentPageNum lt (totalPage-showNum)>
					<#list currentPageNum+1..currentPageNum+showNum as i>
						<li ><a href="javascript:void(0)" onClick="formSubmit(${i})"><span>${i}</span></a></li>
					</#list>
					<li ><a href="javascript:void(0)" onClick="formSubmit(${currentPageNum+showNum-2})"><span>..</span></a></li>
					<li ><a href="javascript:void(0)" onClick="formSubmit(${currentPageNum+showNum-1})"><span>&raquo;</span></a></li>
				</#if>
				<#if currentPageNum gte (totalPage-showNum) && currentPageNum lt totalPage>
					<#list currentPageNum+1..totalPage as i>
						
							<li ><a href="javascript:void(0)" onClick="formSubmit(${i})"><span>${i}</span></a></li>
						
					</#list>
				</#if>
		  <li>
		  <#if isForm>
		  <form action="${url}" id="${formId}" method="post">
		  </#if>
			  <span>
				  <div class="input-group input-group-sm">
				  <#assign urlIdLastIndex=url?last_index_of('#')/>
				  <#if urlIdLastIndex gt -1 >
				  	<#assign urlId=url?substring(url?last_index_of('/')+1,urlIdLastIndex)/>
				  <#else>
				   	<#assign urlId=url?substring(url?last_index_of('/')+1)/>
				   </#if>
				  
				  	<#assign formUrlId="pageForm_"+ urlId />
				  	
				  	
					  	<span class="input-group-addon"><a href="javascript:void(0)" onClick="formSubmit(0)">Go</a></span>
					  	<input type="text" id="pageNumberShow${urlId}" name="pageNumberShow" value="${currentPageNum}" size="1" class="form-control" placeholder="currentNumber">
					  	<input type="hidden" id="pageNumber${urlId}" name="page" value="${currentPageNum-1}" size="1" class="form-control" placeholder="currentNumber">
					  	<input type="hidden" id="size" name="size" value="${page.size}" size="1" class="form-control" placeholder="size">
					  	
					  	<span class="input-group-addon">/${page.totalPages}(每页显示${page.size}条记录)</span>
					  	
					  	<script type="text/javascript">
					  		function formSubmit(_pageNumber){
					  			var pageNumber = "#pageNumber${urlId}";
						  		if(_pageNumber>0){
						  			_pageNumberValue =_pageNumber;
						  		}else{
					  				var pageNumberShow = "#pageNumberShow${urlId}";
						  			_pageNumberValue = $(pageNumberShow).val();
						  		}
					  			
					  			if(_pageNumberValue>0){
					  				_pageNumberValue=_pageNumberValue-1;
					  				$(pageNumber).val(_pageNumberValue);
					  			}
					  			<#--alert($(pageNumber).val());
					  			alert($("#size").val());-->
					  			
					  			var formId="#${formId}";
					  			//alert($(formId).attr("action"));
					  			<#if urlFirst = 'true'>
				  					$(formId).attr("action","${url}");
				  				</#if>
					  			
					  			$(formId).submit();
					  		}
					  	</script>
				  </div>
			  </span>
			 <#if isForm>
			  </form>
			 </#if>
		  </li>
		</ul>
  </#if>
</#macro> 


<#--ajax分页，点击页码异步提交，backDataContainerId为返回数据所填充的容器，提交数据使用 <input type='hidden'data='data'/>-->
<#macro pageBarAjax page url='default_url' formId='default_form' urlFirst='true' showNum=7 backDataContainerId=''>  
  <#if page??>
  
		<ul class="pagination">
			<#assign totalPage=page.totalPages/>
			<#assign currentPageNum=page.number+1/>
			<#assign isForm= (formId = 'default_form')/>
			<#if showNum gt 5>
				<#assign showNum= 5/>
			<#elseif showNum gt 15>
				<#assign showNum= 15/>
			</#if>
				<#if currentPageNum gt 8>
					<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${currentPageNum-6})"><span>&laquo;</span></a></li>
					<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${currentPageNum-5})"><span>..</span></a></li>
					<#list currentPageNum-showNum..currentPageNum as i>
					
						<#if currentPageNum = i>
			  				<li class="active"><span>${i}<span class="sr-only">(current)</span></span></li>
						<#else>
							<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${i})"><span>${i}</span></a></li>
						</#if>
						
					</#list>
					
				<#else>
					<#list 1..currentPageNum as i>
						<#if currentPageNum = i>
			  			<li class="active"><span>${i}<span class="sr-only">(current)</span></span></li>
						<#else>
							<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${i})"><span>${i}</span></a></li>
						</#if>
					</#list>
				</#if>	
				
				<#if currentPageNum lt (totalPage-showNum)>
					<#list currentPageNum+1..currentPageNum+showNum as i>
						<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${i})"><span>${i}</span></a></li>
					</#list>
					<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${currentPageNum+showNum-2})"><span>..</span></a></li>
					<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${currentPageNum+showNum-1})"><span>&raquo;</span></a></li>
				</#if>
				<#if currentPageNum gte (totalPage-showNum) && currentPageNum lt totalPage>
					<#list currentPageNum+1..totalPage as i>
						
							<li ><a href="javascript:void(0)" onClick="formSubmitAjax(${i})"><span>${i}</span></a></li>
						
					</#list>
				</#if>
		  <li>
		  <#if isForm>
		  <form action="${url}" id="${formId}" method="post">
		  </#if>
			  <span>
				  <div class="input-group input-group-sm">
				  <#assign urlIdLastIndex=url?last_index_of('#')/>
				  <#if urlIdLastIndex gt -1 >
				  	<#assign urlId=url?substring(url?last_index_of('/')+1,urlIdLastIndex)/>
				  <#else>
				   	<#assign urlId=url?substring(url?last_index_of('/')+1)/>
				   </#if>
				  
				  	<#assign formUrlId="pageForm_"+ urlId />
				  	
				  	
					  	<span class="input-group-addon"><a href="javascript:void(0)" onClick="formSubmitAjax(0)">Go</a></span>
					  	<input type="text" id="pageNumberShow${urlId}" name="pageNumberShow" value="${currentPageNum}" size="1" class="form-control" placeholder="currentNumber">
					  	<input type="hidden" id="pageNumber${urlId}" data="data" name="page" value="${currentPageNum-1}" size="1" class="form-control" placeholder="currentNumber">
					  	<input type="hidden" id="size${urlId}" data="data" name="size" value="${page.size}" size="1" class="form-control" placeholder="size">
					  	
					  	<span class="input-group-addon">/${page.totalPages}(每页显示${page.size}条记录)</span>
					  	
					  	<script type="text/javascript">
					  		function formSubmitAjax(_pageNumber){
					  			var pageNumber = "#pageNumber${urlId}";
					  			var pageSize = "#size${urlId}";
						  		if(_pageNumber>0){
						  			_pageNumberValue =_pageNumber;
						  		}else{
					  				var pageNumberShow = "#pageNumberShow${urlId}";
						  			_pageNumberValue = $(pageNumberShow).val();
						  		}
					  			
					  			if(_pageNumberValue>0){
					  				_pageNumberValue=_pageNumberValue-1;
					  				$(pageNumber).val(_pageNumberValue);
					  			}
					  			
					  			var formId="#${formId}";
					  			
									
					  			<#if urlFirst == 'true'>
				  					$(formId).attr("action","${url}");
				  				</#if>
				  				//处理input
				  				var request = new Object();
				  				
				  				var _fom = $(formId);
								if(_fom.length>0){
										var _input= _fom.find("input[data$='data']");
										if(_input.length>0){
								  			$(_input).each(function(){
								  				if($(this).attr("type")=="checkbox"){
								  					if($(this).attr('checked')==undefined){
								  						request[$(this).attr("name")]='false';
								  					}else{
								  						request[$(this).attr("name")]='true';
								  					}
								  					//alert($(this).attr("checked"));
								  				}else{
								  					request[$(this).attr("name")]=$(this).attr("value");
								  				}
									  		});
							  			}
							  			
							  			
							  			//处理select
							  			var _select = formId+ " select[data$='data']";
							  			if($(_select).length>0){
								  			$(_select).each(function(){
								  				request[$(this).attr("name")]=$(this).attr("value");
								  			});
							  			}
								}
					  			
								var action = $(formId).attr("action");
								
								var backDataContainerId = '#${backDataContainerId}';
								$.ajax({
										url: action,
										data: request,
										type: "POST",
										dataType : "text",
										contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
										success: function(data) {
										
											if($(backDataContainerId).length>0){
													$(backDataContainerId).html(data);
											}
												
										},
										error: function() {
											alert( "Sorry, there was a problem!" );
										},
										complete: function() {
											//alert('complete');
										}
									});
					  			
					  		}
					  		
					  	</script>
				  </div>
			  </span>
			 <#if isForm>
			  </form>
			 </#if>
		  </li>
		</ul>
  </#if>
</#macro> 

<#macro nextPage page containerId="" url="#" pageSize=20> 
	<#if page??>
		<#assign totalPage=page.totalPages/>
		<#assign currentPageNum=page.number+1/>
		<#if totalPage gte 1 && currentPageNum lte totalPage>
			<#assign currentPageNum=page.number+1/>
			<div id="nextPage_${containerId}"><h4><a href="javascript:void(0)" onClick="nextPage('${url}','${currentPageNum}','${pageSize}','${containerId}','nextPage_${containerId}','${totalPage}')" class="text-warning" title="加载${pageSize}条"><span class="glyphicon glyphicon-circle-arrow-down"></span></a></h4></div>
		</#if>
	</#if>
</#macro> 

<#macro nextPageButtonBar page containerId="" url="#" pageSize=20> 
	<#if page??>
		<#assign totalPage=page.totalPages/>
		<#assign currentPageNum=page.number+1/>
		<#if totalPage gte 1 && currentPageNum lt totalPage>
			<#assign currentPageNum=page.number+1/>
			<div class="btn-group btn-group-justified" id="nextPage_${containerId}">
				<a href="javascript:void(0)" onClick="nextPage('${url}','${currentPageNum}','${pageSize}','${containerId}','nextPage_${containerId}','${totalPage}')" class="btn btn-warning" title="加载${pageSize}条"><h4><span class="glyphicon glyphicon-circle-arrow-down"></span> 加载${pageSize}条 </h4></a>
			</div>
		</#if>
	</#if>
</#macro> 