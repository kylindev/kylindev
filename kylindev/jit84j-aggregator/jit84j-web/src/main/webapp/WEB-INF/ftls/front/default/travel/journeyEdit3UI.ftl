<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<div class="container body_content" id="body_content">

	<div class="row">
			<div class="col-md-12">
			 	<div class="panel panel-warning">
				<div class="panel-heading ">发布行程 【第三步：行程角色设定】</div>
					  
				  	  <form id="journey_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_JOURNEY_ADD_UI_MAPPING}" method="post">
				  	  <div class="row">
							<div class="col-md-12">
							  	  <ul class="list-inline">
							       	<li><input type="button" onClick="submit('#journey_form','#journey_content_div')" value="保存行程" class="btn btn-primary"/></li>
							       	<li><input type="button" onClick="submit('#journey_form','#journey_content_div')" value="发布行程" class="btn btn-primary"/></li>
							      	<li>
									   <input type="button" class="btn btn-primary" onClick="cleanForm('#journey_form')" value='清空'/>
									</li>
							      </ul>
						     </div>
				      </div>
				  	  <input type="hidden" data="data" id="id" name="id" value="<#if journey??>${journey.id}</#if>">
				      <div class="row">
						<div class="col-md-4">
						      <div class="form-group ">
							    <label class="control-label" for="title">行程标题: 
								</label>
							    <input type="text" data="data" name="title" value="<#if journey??>${journey.title}</#if>" class="form-control" id="title" placeholder="输入行程标题" required="true"/>
							  </div>
							  <div class="form-group ">
							    <label class="control-label" for="manifesto">本程宣言: 
								</label>
							    <input type="text" data="data" name="manifesto" value="<#if journey??>${journey.manifesto}</#if>" class="form-control" id="manifesto" placeholder="输入本次行程宣言" required="true"/>
							  </div>
							  <div class="form-group ">
							    <label class="control-label" for="destinations">途经目的地: 
								</label>
							    <input type="text" data="data" name="destinations" value="<#if journey??>${journey.destinations}</#if>" class="form-control" id="destinations" placeholder="输入本次行程重要所经之地" required="true"/>
							  </div>
							  
							  
						 </div>
						 <div class="col-md-4">
						 
						 	<div class="form-group ">
							   <label class="control-label" for="begin_end_date"> 行程起止时间:</label>
							   <div id="begin_end_date">
								<input type="text" data="data" name="beginDate" size="18" value="<#if journey??>${journey.beginDate}</#if>"  id="beginDate" placeholder="开始" />
							  	--
							    <input type="text" data="data" name="endDate" size="18" value="<#if journey??>${journey.endDate}</#if>" id="endDate" placeholder="结束" />
							    </div>
							 </div>
							 <div class="form-group ">
							    	<label class="control-label" for="description">参与类型: 
									</label>
								  	<select name= 'participationType' data="data" id="participationType">
										  	<option value="${Journey.PARTICIPATION_TYPE_SELF}" >自由行</option>
										  	<option value="${Journey.PARTICIPATION_TYPE_PARTY_TEMP}" >临时组队</option>
										  	<option value="${Journey.PARTICIPATION_TYPE_PARTY_ORIG}" >原创队伍</option>
								  	</select>
							  </div>
							  <script type="text/javascript">
							  	$(document).ready(function(){ 
							  		$('#beginDate').datetimepicker({
									    format: 'yyyy-mm-dd hh:ii',
									    language:'zh-CN',
									    autoclose:true,
									});
									$('#endDate').datetimepicker({
									    format: 'yyyy-mm-dd hh:ii',
									    language:'zh-CN',
									    autoclose:true,
									});
									
									$('#detailDate').datetimepicker({
									    format: 'yyyy-mm-dd',
									    language:'zh-CN',
									    autoclose:true,
									    minView:2
									});
									
									//初始化，类型
									function initparticipationType(){
											$("#type_self").show();
							  				$("#type_party_1").hide();
							  				$("#type_party_2").hide();
									}
									initparticipationType();
									
							  		$('#participationType').change(function(){
							  			var value=$(this).children('option:selected').val();
							  			if(0==value){
							  				$("#type_self").show();
							  				$("#type_party_1").hide();
							  				$("#type_party_2").hide();
							  			}else if(1==value){
							  				$("#type_self").hide();
							  				$("#type_party_1").show();
							  				$("#type_party_2").show();
							  				$("#partyId").val("");
							  				$("#user_current_party").html("");
							  			}else if(2==value){
							  				$("#type_self").hide();
							  				$("#type_party_1").show();
							  				$("#type_party_2").show();
							  			}
							  		});
							  		
							  		
									$( 'textarea#content' ).ckeditor();
							  	}); 
							  </script>
							  <div class="form-group " id="type_self">
							    <label class="control-label">是否结伴: 
								</label>
							    <input type="checkbox" data="data" name="togetherRequired" <#if journey?? && journey.togetherRequired> checked </#if> value="${journey.togetherRequired}"  id="togetherRequired"/>
							  </div>
							  
							  <div class="form-group" id="type_party_1">
							    <label class="control-label" for="partyName">队伍名称: 
								</label>
								<div id="user_current_party"></div>
								<input type="hidden" data="data" name="partyId" id="partyId" value=""/> 
							    <input type="text" data="data" name="partyName" value="<#if journey??>${journey.partyName}</#if>" class="form-control" id="partyName" placeholder="输入队伍名称" required="true"/>
							  </div>
							  
							  <div class="form-group" id="type_party_2">
							    人数:
								
							    <input type="text" data="data" name="minMember" size="3" value="<#if journey??>${journey.minMember}</#if>"  id="minMember" placeholder="最小"/>
							  	--
							    <input type="text" data="data" name="maxMember" size="2" value="<#if journey??>${journey.maxMember}</#if>" id="maxMember" placeholder="最大"/>
							    
							  </div>
							  
							  
							  <div class="form-group ">
							    <label class="control-label" for="imagePath">行程封面图片: 
								</label>
							    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图片路径" />
							 	
							 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING})"/><br/>
							    <div id="iconPath_img">
							    	<img src='<#if journey??>${journey.imagePath}</#if>'/>
							    </div>
							  </div>
							  
						 </div> 
						 <div class="col-md-4">
						  	  <div class="form-group ">
							    <label class="control-label" for="description">行程备忘: 
								</label>
								<textarea data="data" id="description" name="description" value="<#if journey??>${journey.description}</#if>" class="form-control" id="description" placeholder="内容只会自己看到" ><#if journey??>${journey.description}</#if></textarea>
							  </div>
						 	
							 <p class="text-primary">详细日程：</p>
							 <div class="row" id="detail_journey_title">
						 		<div class="col-md-3">
						 		日期时间
						 		</div>
						 		<div class="col-md-3">
						 		停留天数
						 		</div>
						 		<div class="col-md-3">
						 		抵达地点
						 		</div>
						 		<div class="col-md-3">
						 		<button onclick="addDetailItem()">添加</button>
						 		</div>
						 	</div>
						 	<#--
							 <ul class="list-unstyled">
							 	<li>
								    <label class="control-label" >日期时间: 
									</label>
								    <input type="text" data="data" size="15" name="detailDate" value="<#if journey??>${journey.partyName}</#if>"  id="detailDate" placeholder="选择日期" required="true"/>
								</li>
								<li>	    
								    <label class="control-label" >停留天数: 
									</label>
								    <input type="text" data="data" size="15" name="days" value="<#if journey??>${journey.partyName}</#if>"  id="partyName" placeholder="停留天数" required="true"/>
								 </li>
								<li>
								   <label class="control-label" >抵达地点: 
									</label>
								    <input type="text" data="data" size="15" name="place" value="<#if journey??>${journey.partyName}</#if>"  id="place" placeholder="抵达地点" required="true"/>
								</li>
								<li>
								    <label class="control-label">简短描述: 
									</label>
								    <input type="text" data="data" size="28" name="description" value="<#if journey??>${journey.partyName}</#if>" id="description" placeholder="简短描述该段行程" required="true"/>
							  	</li>
							  </ul>
							  -->
						 </div>
						 
					</div>
					<div class="row">
							  <div class="col-md-12">
						 		  <div class="form-group ">
								    <label class="control-label" for="content">行程内容: 
									</label>
									<textarea data="data" id="content" rows="10" name="content" value="<#if journey??>${journey.content}</#if>" class="form-control" id="content" placeholder="输入行程内容" required="true"><#if journey??>${journey.content}</#if></textarea>
								  </div>
							  </div>
					</div>
				  </form>
				</div>
			</div>
			
	</div>
	<div class="row">
		<div class="col-md-12" id="journey_content_div">
		
			发布行程：
			标题
			内容
			行程宣言
			起始时间
			结束时间
			发布时间 系统指定
			发布人 系统显示登录者昵称，
			参与类型[个人自由行，临时组队，原创队伍]
			1.是否求结伴，当选择个人自由行时，显示此checkbox
			2.队伍名称，最小人数，最大人数（当类型为队伍时，显示此三个字段）
				设定：*角色分工
			
			*行程详细设计
			开始时间
			天数
			地点
			简短描述
		</div>
	</div>
</div>
