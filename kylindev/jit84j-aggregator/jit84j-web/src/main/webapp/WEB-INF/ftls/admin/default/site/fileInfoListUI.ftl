<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_user.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_fileSystemGallery.ftl">
<#include "definefunction_fileInfo.ftl">

<@listFileInfo page=page editNavigation=editNavigation/>