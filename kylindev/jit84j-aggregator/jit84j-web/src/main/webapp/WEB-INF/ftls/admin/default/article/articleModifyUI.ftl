<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>修改文章</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-8">
  <p class="text-success">设置文章基本信息</p>
  <@form.form commandName="article" role="form" action="/admin/article/articleModify">
  <@spring.bind "article.uniqueIdentifier" /> 
  <input type="hidden" name="id" value="${article.uniqueIdentifier}">
      <@spring.bind "article.title" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="title">标题: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="title" class="form-control" id="title" placeholder="输入文章标题" required="true"/>
	  </div>
	  <@spring.bind "article.titleSecond" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="titleSecond">第二标题:
		    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
	    </label>
	    <@form.input path="titleSecond" class="form-control" id="titleSecond" placeholder="输入第二标题" required="true"/>
	  </div>
	  
	  <@spring.bind "article.keyWord" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="keyWord">文章关键字:
		    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="keyWord" class="form-control" id="keyWord" placeholder="输入文章关键字,以,分隔" required="true"/>
	  </div>
	  
	  <@spring.bind "article.description" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label for="description">文章描述：
	    	<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="description" class="form-control" id="description" placeholder="输入文章描述" required="true"/>
	  </div>
	  <@spring.bind "article.primaryDesc" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="primaryDesc">文章概要:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="primaryDesc" class="form-control" id="primaryDesc" placeholder="文章概要" />
	  </div>
	   <@spring.bind "article.authorName" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteTitle">文章作者:
	    	<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="authorName" class="form-control" id="authorName" placeholder="输入作者" required="true"/>
	  </div>
	  <@spring.bind "article.penName" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteSecondTitle">笔名:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="penName" class="form-control" id="penName" placeholder="输入笔名" />
	  </div>
	   <@spring.bind "article.firstImagePath" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteKey">封面图片路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    
	    <@form.input path="firstImagePath" class="form-control" id="firstImagePath" placeholder="封面图片路径" />
	    <input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','firstImagePath')"/><br/>
	    <div id="firstImagePath_img">
	    <img src='${article.firstImagePath}'/>
	    </div>
	  </div>
	  <@spring.bind "article.firstImageAdPath" /> 
	   <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="description">封面图片缩略图:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="firstImageAdPath" class="form-control" id="firstImageAdPath" placeholder="封面图片缩略图" />
	    <input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','firstImageAdPath')"/><br/>
	    <div id="firstImageAdPath_img">
	    <img src='${article.firstImageAdPath}'/>
	    </div>
	  </div>
	  <@spring.bind "article.iconPath" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="iconPath">文章图标路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="iconPath" class="form-control" id="iconPath" placeholder="文章图标路径,默认/image/site_info/icon.png" />
	    <input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','iconPath')"/><br/>
	    <div id="iconPath_img">
	    	<img src="${article.iconPath}" title="文章图标" alt="独家记忆" class="img-rounded">
	    </div>
	  </div>
	  <@spring.bind "article.linkedUrl" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="linkedUrl">文章转载路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="linkedUrl" class="form-control" id="linkedUrl" placeholder="文章转载路径" />
	    <img src="${article.linkedUrl}" title="文章转载路径" alt="文章转载路径" class="img-rounded">
	  </div>
	   <@spring.bind "article.articleContent.content" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="articleContent.content">文章内容:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="articleContent.content" class="form-control" id="articleContent_content" placeholder="文章内容" />
	  </div>
	  
	  
	  
	   <div class="col-md-12">
       	<input type="submit" value="保存修改" class="btn btn-primary"/>
       	<a class="text-danger" href='<@spring.url relativeUrl="/admin/article/articleListUI"/>'>
       		<input type="button" value="返回列表" class="btn btn-primary"/>
       	</a>
       </div>
  	</@form.form>
	</div>
	<div class="col-md-2">
	<input type="button" value="选择标签" class="btn btn-primary"/><br/>
	    <@listTag tags=tagList />
	
	<hr/>
	
		<div id="asignCategoryForArticle_div">
		<input type="button" value="选择分类" class="btn btn-primary"/><br/>
		<@spring.bind "article.uniqueIdentifier" /> 
		<#assign depth = 1 />  
		<#assign currentArticleId = article.uniqueIdentifier /> 
		<@asignCategoryForArticle children=categoryList currentArticleId=currentArticleId/> 
		</div>
	
	<hr/>
	</div>

</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/ckeditor/adapters/jquery.js"></script>
<div id="filepathTextFieldId_hidden"></div>
<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
<script src="/assets/common/js/jquery/jquery.json-2.4.min.js"></script>


<script type="text/javascript">
$( document ).ready( function() {
	$( 'textarea#articleContent_content' ).ckeditor();
} );

$( document ).ready(function() {
	console.log( "ready!" );
});
</script>

</body>
</html>