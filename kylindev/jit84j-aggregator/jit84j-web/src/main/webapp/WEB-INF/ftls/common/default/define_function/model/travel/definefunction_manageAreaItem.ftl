
<#macro manageAreaItemIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#container_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
</#macro>

<#--manageAreaItemEditUI 添加、修改页面 begin-->
<#macro manageAreaItemEditUI manageAreaItem page>
<#if msg??><div class="alert alert-success" role="alert">${msg}</div></#if>
<div class="row">
		  <div class="col-md-3">
		  	  <p class="text-primary">添加/修改 我的管理区菜单项</p>
		  	  
		  	  <form id="manageAreaItem_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_MAPPING}" method="post">
		  	
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if manageAreaItem??>${manageAreaItem.id}</#if>">
			  
			  	<div class="input-group form-group">
				  <span class="input-group-addon">名称:</span>
				  <input type="text" data="data" name="name" value="<#if manageAreaItem??>${manageAreaItem.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
				</div>
				
				<div class="input-group form-group">
				  <span class="input-group-addon">代码:</span>
				 <input type="text" data="data" name="code" value="<#if manageAreaItem??>${manageAreaItem.code}</#if>" class="form-control" id="code" placeholder="使用字母，必须唯一" required="true"/>
				</div>
				
				  <div class="input-group form-group">
					<span class="input-group-addon">路径:</span>
				    <input type="text" data="data" name="url" value="<#if manageAreaItem??>${manageAreaItem.url}</#if>" class="form-control" id="code" placeholder="输入url" required="true"/>
				  </div>
				  
				   <div class="input-group form-group">
					<span class="input-group-addon">开发者id:</span>
				    <input type="text" data="data" name="developerId" value="<#if manageAreaItem??>${manageAreaItem.developerId}</#if>" class="form-control" id="code" placeholder="输入开发者Id" required="true"/>
				  </div>
				  
			   <div class="input-group form-group">
					<span class="input-group-addon">图标:</span>
				    <input type="text" data="data" name="iconPath" class="form-control" id="iconPath" placeholder="输入图标路径" />
				</div>
				
				<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if manageAreaItem??>${manageAreaItem.iconPath}</#if>'/>
			    </div>
				  
			  <#--
			  <div class="form-group ">
			    <label class="control-label" for="iconPath">图标: 
				</label>
			    <input type="text" data="data" name="iconPath" class="form-control" id="iconPath" placeholder="输入图标路径" />
			  </div>
			  -->
			  <br/>
			  
			   <div class="input-group form-group">
					<span class="input-group-addon">顺序:</span>
				    <input type="text" data="data" name="sequence" value="<#if manageAreaItem??>${manageAreaItem.sequence}</#if>" class="form-control" id="sequence" placeholder="使用数字1,2...10,12" required="true"/>
				</div>
			  
			   <div class="input-group form-group">
					<span class="input-group-addon">显示:</span>
				    <input type="checkbox" data="data" name="display" <#if manageAreaItem?? && manageAreaItem.display> checked </#if> value="${manageAreaItem.display}" class="form-control" id="display"/>
				</div>
			  
			   <div class="input-group form-group">
					<span class="input-group-addon">禁用:</span>
				    <input type="checkbox" data="data" name="disable" <#if manageAreaItem?? && manageAreaItem.disable> checked </#if> value="${manageAreaItem.disable}" class="form-control" id="disable"/>
				</div>
				
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#manageAreaItem_form','#container_content_div')" value="保存" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#manageAreaItem_form')" value='清空'/>
				</li>
		      </ul>
		</div>
		<div class="col-md-9">
		
		<p class="text-primary">我的管理区菜单项列表</p>
		<@listManageAreaItemView page=page/>
		</div> 
		<script src="/assets/default/js/site/jit84j_core.js"></script>
</div>
</#macro>
<#--manageAreaItemEditUI 添加、修改页面 end-->

<#macro listManageAreaItemView page>
<#if page?? && page.content??>
		<#assign manageAreaItemList = page.content />
		 <form id="manageAreaItem_list_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_LIST_EDIT_MAPPING}" name="manageAreaItem_list_form" method="post">
				 
				 <table class="table table-striped table-bordered table-condensed" >
					 <thead>
					 
						 <tr>
						 	<th width="2%">
							 Id
							</th>
						 	<th width="8%">
							 名称
							</th>
							 <th width="8%">
							 代码
							 </th>
							  <th width="10%">
							 路径
							 </th>
							 <th width="5%">
							 图标
							 </th>
							 <th width="3%">
							 显示
							 </th>
							 <th width="3%">
							 禁用
							 </th>
							  <th width="3%">
							 删除
							 </th>
							 <th width="3%">
							 排序
							 </th>
							 <th width="4%">
							 开发者ID
							 </th>
							 <th width="10%">
							操作
							 </th>
						</tr>
					 </thead>
					 <tbody>
					<#list manageAreaItemList as manageAreaItem>  
						 <tr>
						 <td>
							 ${manageAreaItem.id}
							 <input type="hidden" data="data" name="manageAreaItemList[${manageAreaItem_index}].id" value="${manageAreaItem.id}" id="id${manageAreaItem.uniqueIdentifier}" />
							</td>
							<td>
							 ${manageAreaItem.name}
							</td>
							 <td>
							 ${manageAreaItem.code}
							 </td>
							  <td>
							 <a href="${manageAreaItem.url}" title="查看菜单【${manageAreaItem.name}】" target="_blank">${manageAreaItem.url}</a>
							 </td>
							 <td>
							 ${manageAreaItem.iconPath}
							 </td>
							 <td>
						 	 <input type="checkbox" data="data" name="manageAreaItemList[${manageAreaItem_index}].display" <#if manageAreaItem?? && manageAreaItem.display>checked</#if> class="form-control" id="display${manageAreaItem.uniqueIdentifier}" />
							 </td>
							  <td>
						 	 <input type="checkbox" data="data" name="manageAreaItemList[${manageAreaItem_index}].disable" <#if manageAreaItem?? && manageAreaItem.disable>checked</#if> class="form-control" id="disable${manageAreaItem.uniqueIdentifier}" />
							 </td>
							  <td>
						 	 <input type="checkbox" data="data" name="manageAreaItemList[${manageAreaItem_index}].deleted" <#if manageAreaItem?? && manageAreaItem.deleted>checked</#if> class="form-control" id="deleted${manageAreaItem.uniqueIdentifier}" />
							 </td>
							 <td>
							  <input type="text" size="2" data="data" name="manageAreaItemList[${manageAreaItem_index}].sequence" value="<#if manageAreaItem??>${manageAreaItem.sequence}</#if>" class="form-control" id="sequence${manageAreaItem.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
							 </td>
							  <td>
							  ${manageAreaItem.developerId}
							 </td>
							 <td>
								 <ul class="list-inline"> 
									<li>
								 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${manageAreaItem.id}" onclick="switchSetting('${manageAreaItem.id}','${manageAreaItem.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_UI_MAPPING}','#container_content_div')">编辑</a>
								  	</li>
								    <li>
								    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${manageAreaItem.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_REMOVE_MAPPING}','#container_content_div')">删除</a>
								    </li>
								  </ul>
						 	  </td>
						 </tr>
					</#list>
					</tbody>
				</table>
		</form>
		<ul class="list-inline">
		       	<li><input type="button" onClick="submit('#manageAreaItem_list_form','#container_content_div')" value="更新列表" class="btn btn-primary"/></li>
		</ul>
		
</#if>
</#macro>

<#macro getManageAreaItemDropdown  manageAreaItemList currentManageAreaItem='' name="manageAreaItem.id">
<select name=${name} data="data">
		<#list manageAreaItemList as manageAreaItem>
	  		<option value="${manageAreaItem.id}" <#if manageAreaItem?? && currentManageAreaItem?? && currentManageAreaItem!='' && manageAreaItem.id == currentManageAreaItem.id> selected </#if> >${manageAreaItem.name}</option>
		</#list>
</select>
</#macro>