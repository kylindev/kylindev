<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_specialArea.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">
<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1771902817/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent" autoPlay="true"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary">进入我的管理区</a> <@loginAndLogout /></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/><#---->
</div>

<div class="container" id="body_content">
	<div class="row">	
		<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-heading text-center"><span class="text-danger"><span class="glyphicon glyphicon glyphicon-fire"></span>长线活动召集公告</span></div>
		  <div class="panel-body">
		   		<ul class="list-inline">
		   		<li>
					<a href="#" class="text-primary">漫游西藏40天活动召集，出发时间七月七日，报名截止时间六月三十日<span class="badge">23</span></a>
					<br/>
					<a href="#" class="text-primary">厦门-土楼-武夷山-20天骑行活动召集[4-8人]，出发时间6月13日，报名截止时间6月13日<span class="badge">07</span></a>
				</li>
				<li>
					<a href="#" class="text-primary">开往欧洲秋季的旅行列车，到达时间十月十四日，欧洲小镇浪漫之旅<span class="badge">59</span></a>
					<br/>
					<a href="#" class="text-primary">雪山、峡谷、飞溪、古树、巨石...徒步雨崩-梅里雪山-西当村，出发时间9月4日<span class="badge">36</span></a>
				</li>
				</ul>
		  </div>
		</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<button class="btn btn-warning">我要发布</button><h4 class="text-warning text-right"><strong>最热标签:</strong></h4>
		</div>
		<div class="col-md-10">
		<p class="text-left text-left" style="padding: 10px">
			<button class="btn"><label class="label label-default">好玩的</label></button>
			<button class="btn"><label class="label label-default">有趣的</label></button>
			<button ><label class="label label-default">奇葩的</label></button>
			<button class="btn"><label class="label label-success">风光的</label></button>
			<button><label class="label label-danger">深度的</label></button>
			<button><label class="label label-default">秀的</label></button>
			<button><label class="label label-default">美不胜收的</label></button>
			<button><label class="label label-warning">瞬时的</label> </button>
			<button><label class="label label-default">问路的</label> </button>
			<button><label class="label label-default">你懂的</label> </button>
			<button class="btn btn-primary"><label class="label label-primary">在哪的</label></button>
			<button><label class="label label-info">有什么的</label></button>
			<button><label class="label label-default">其实没什么的</label></button>
			<button><label class="label label-default">无聊的</label></button>
			
			<button><label class="label label-default">好玩的</label></button>
			<button><label class="label label-default">有趣的</label></button>
			<button><label class="label label-default">奇葩的</label></button>
			<button><label class="label label-success">风光的</label></button>
			<button><label class="label label-danger">深度的</label></button>
			<button><label class="label label-default">秀的</label></button>
			<button><label class="label label-default">美不胜收的</label></button>
			<button><label class="label label-warning">瞬时的</label> </button>
			<button><label class="label label-default">问路的</label> </button>
			<button><label class="label label-default">你懂的</label> </button>
			<button><label class="label label-default">在哪的</label></button>
			<button><label class="label label-info">有什么的</label></button>
			<button><label class="label label-default">其实没什么的</label></button>
			<button><label class="label label-default">无聊的</label></button>
		<#--
		<ul class="list-inline">
			<li><label class="label label-default">好玩的</label></li>
			<li><label class="label label-default">有趣的</label></li>
			<li><label class="label label-default">奇葩的</label></li> 
			<li><label class="label label-success">风光的</label></li> 
			<li><label class="label label-default">深度的</label></li> 
			<li><label class="label label-default">秀的</label></li> 
			<li><label class="label label-default">美不胜收的</label></li> 
			<li><label class="label label-default">瞬时的</label></li> 
			<li><label class="label label-default">问路的</label></li> 
			<li><label class="label label-default">你懂的</label></li> 
			<li><label class="label label-default">在哪的</label></li> 
			<li><label class="label label-default">有什么的</label></li> 
			<li><label class="label label-default">其实没什么的</label></li> 
			<li><label class="label label-default">无聊的</label></li> 
		</ul>
		-->
		</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-1">
			<button type="button" class="btn btn-primary btn-lg btn-block">徒步</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">骑行</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">自驾</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">混合</button>
		</div>
		<div class="col-md-7">
			<div class="container">
				<div id="myCarousel" class="carousel slide" >
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <div class="active item">
					   		
					   		<div style="position:relative;width:750px;height:300px;border:1px solid red;float:left;">
								<img src="/assets/data/upload/files/1/image/photo/2014/05/19/33675570_1276082644PEm7.jpg" style="width:620px;">
								<div style="position:absolute;top:10px;left:10px; padding:10" class="text-info">
									<strong>
							   		已经记不清楚自己是从什么时候开始，喜欢上了登山。
									也许是出自天性中的不安分因子，
									也许是现实中的人们终须有一个自己认可的人生出口。
									也许根本不用也许
									很多人问过我同样的问题：“登山这么累这么苦，这么危险。为什么还要坚持？”
									怎么去回答呢？
									我想，对于这样的问题，答案一定是千千万万的。
									我只是觉得
									喜欢一个人，或者一件事情，是不需要理由的。如果不喜欢，一定会有千万个理由。
									</strong>
								</div>
							</div>
					   		
					   		
				    </div>
				    <div class="item">
				    	<div style="background:url(http://img01.taobaocdn.com/imgextra/i1/15833137/T2I57sXj4aXXXXXXXX_!!15833137.jpg); height:300px">
				    	</div>
				    </div>
				    <div class="item" >
				    	
				    	<div style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/1.jpeg);">
				    		<p class="text-danger">就在这时，他们在走：</p>
				    		<ul>
							    <li class="text-primary"><strong>大鹏之旅开始第一天 by 深圳的孩子</strong></li>
							    <li class="text-primary"><strong>小狐厦门之行第三天 by 小狐</strong></li>
							    <li class="text-primary"><strong>欧洲浪漫七天之第五天 by 枫叶的思念</strong></li>
							    <li class="text-primary"><strong>走向西川第8天 by 孤寂西川</strong></li>
							    <li class="text-primary"><strong>新西兰随处是风景第13天 by 小菲</strong></li>
							    <li class="text-primary"><strong>坐上火车去拉萨第2天 by 小白</strong></li>
							    <li class="text-primary"><strong>滇藏骑行第5天 by 落客</strong></li>
							    <li class="text-primary"><strong>走近撒哈拉第15天 by 骆驼的脚步</strong></li>
							    <li class="text-primary"><strong>去往悄江南第4天 by 女儿红</strong></li>
							    <li class="text-primary"><strong>云南篇之暴走雨崩 第3天 by 驴子</strong></li>
							    <li class="text-primary"><strong>环骑台湾岛 第2天 by 四小分队</strong></li>
							    <li class="text-primary"><strong>内蒙大草原 品鲜马奶酒 第4天 by 小娅</strong></li>
							    <li class="text-primary"><strong>开路七娘山 第2天 by 天涯浪子</strong></li>
							</ul>
				    	</div>
					    
				    </div>
				    <div class="item">
				    	<div style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg); height:350px">
				    	</div>
				    
					</div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
				  <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
				</div>
			</div>
		</div>
		<div class="col-md-3">
		<label>今日宣言</label>
			<ul class="list-unstyled">
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				<li class="text-primary">遇见你是我最大的幸福 by kylinboy</li>
				
			</ul>
		</div>
		<div class="col-md-1">
			<button type="button" class="btn btn-primary btn-lg btn-block">队伍</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">散兵</button>
		</div>
	</div>
<hr/>
</div>


<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			<div class="btn-group btn-group-justified">
			  <a herf="#" class="btn btn-default"><label class="text-primary">说走就走</label></a>
			  <a herf="#" class="btn btn-default"><label class="text-primary">边行边记</label></a>
			  <a herf="#" class="btn btn-default"><label class="text-primary">摄人心魂</label></a>
			  <a herf="#" class="btn btn-default"><label class="text-primary">走遍世界</label></a>
			</div>
			<#--<@specialAreaListIndex specialAreaList=specialAreaList/>-->
			
		</div>
	</div>
	
	<div class="row">
		<div class="col-md-3">
		
			  <div class="panel-heading">西北大漠</div>
			  <div class="panel-body" style="height:300px">
			  西北大漠
				<#if (galleryList??)>
					<#list galleryList as gallery>
					<div>${gallery.theme}--${gallery.description}--${gallery.publishTime}<br/>
					<#if gallery.photographList?? && gallery.photographList?size gt 0>
						<#list gallery.photographList as photograph>
						##${photograph.title}##${photograph.imagePath}<br/>
						</#list>
					</#if>	
					</div>  
					</#list>
				</#if>
				</div>
		</div>
  		<div class="col-md-3">
				  <div class="panel-heading">江南风情</div>
				  <div class="panel-body" style="height:300px">
	  			
				  </div>
  		</div>
  		<div class="col-md-3">
				  <div class="panel-heading">环走东亚</div>
				  <div class="panel-body" style="height:300px">
				  </div>
  		</div>
  		<div class="col-md-3">
				  <div class="panel-heading">猎趣欧洲</div>
				  <div class="panel-body" style="height:300px">
				  </div>
  		</div>
  		
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="jumbotron">
			  <h1>Hello, world!</h1>
			  <p>...</p>
			  <p><a class="btn btn-primary btn-lg" role="button">Learn more</a></p>
			</div>
		</div>
	</div>
</div>

<#--
<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			<span class="label label-default">Default</span>
			<span class="label label-primary">Primary</span>
			<span class="label label-success">Success</span>
			<span class="label label-info">Info</span>
			<span class="label label-warning">Warning</span>
			<span class="label label-danger">Danger</span>
		</div>
	</div>
</div>
-->
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>