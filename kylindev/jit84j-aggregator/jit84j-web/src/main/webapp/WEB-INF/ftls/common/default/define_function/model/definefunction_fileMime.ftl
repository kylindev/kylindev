
<#-- 文件类型列表 -->
<#macro listFileMime page editNavigation>
<p class="text-success">MIME列表:</p>
<table class="table">
<#assign systemGalleryList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN)/>
  <@form.form commandName="fileMime" id="fileMime_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_MAPPING}">
  <thead>
 <#--
  <tr>
      <td width="3%"></td>
      <td width="10%"><input type="text" placeholder="名称" size="5" name="name" value="<#if fileMimeSearch??>${fileMimeSearch.name}</#if>"></td>
      <td width="10%"><input type="text" placeholder="扩展名" size="5" name="suffix" value="<#if fileMimeSearch??>${fileMimeSearch.suffix}</#if>"></td>
      <td width="15%"><input type="text" placeholder="MIME" size="2" name="mime" value="<#if fileMimeSearch??>${fileMimeSearch.mime}</#if>"> </td>
      <td width="15%">
      	<@getOwnerSystemGalleryWithAll fileMime=fileMimeSearch systemGalleryList=systemGalleryList name="systemGallery.id"/>
      </td>
      <td width="15%"><input type="checkbox" placeholder="允许上传" name="allowable" <#if fileMimeSearch?? && fileMimeSearch.allowable>checked</#if> value="${fileMimeSearch.allowable}"></td>
      <th width="15%">排序</th>
      <td width="15%"><input type="button" value="搜索" class="btn btn-primary" onclick="formSubmit(1)"/></td>
    </tr>
    -->
    <tr>
      <th width="3%">Id</th>
      <th width="10%">名称</th>
      <th width="10%">扩展名</th>
      <th width="15%">MIME</th>
      <th width="15%">所属分类</th>
      <th width="15%">允许上传</th>
      <th width="15%">排序</th>
      <th width="15%">操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page.content??)>
  	 <#assign depth = 1 />  
	<@listFileMimeItem page=page systemGalleryList=systemGalleryList/>
  </#if>
	<tr>
		<td colspan="8">
			<a class="btn btn-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
			保存列表
			</a>
		</td>
		
	 </tr>
	
   </tbody>
   </@form.form>
 </table>
</#macro>

<#macro listFileMimeItem page systemGalleryList>
	<#if page?? && page.content??>
	<#assign fileMimeList = page.content />
		<#list fileMimeList as fileMime>  
			 <#assign depth = depth + 1 />
			 
			 <tr>
			 <td>
			 ${fileMime.uniqueIdentifier}
			 </td>
			 <td>
			 ${fileMime.name}
			 </td>
			 <td>
			 ${fileMime.suffix}
			 </td>
			 <td>
			 ${fileMime.mime}
			 </td>
			  <td>
			  <#if systemGalleryList??>
			  <@getOwnerSystemGallery fileMime=fileMime systemGalleryList=systemGalleryList name="systemGallery.id"/>
			  </#if>
			 </td>
			 <td>
			 <input type="checkbox" name="allowable" <#if fileMime?? && fileMime.allowable>checked</#if> class="form-control" id="allowable_${fileMime.uniqueIdentifier}" />
			 </td>
			 <td>
			 <input type="text" name="sequence" value="<#if fileMime??>${fileMime.sequence}</#if>" class="form-control" id="sequence_${fileMime.uniqueIdentifier}" />
			 </td>
			 <td>
			 	<ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${fileMime.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_MAPPING}')">编辑</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${fileMime.id}','${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_REMOVE_MAPPING}','#fileSystemGallery_content_div')">删除</a>
				    </li>
				  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<tr>
		<td colspan="8">
		<@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_INDEX_UI_MAPPING}" formId='fileMime_list_form'/>
		</td>
		</tr>
	</#if>
		
</#macro>


<#--fileMimeUI 添加、修改页面 begin-->
<#macro fileMimeIndexUI fileMime>
<div class="row">
<#assign systemGalleryList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN)/>
		  <div class="col-md-4">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 文件类型</p>
		  	  
		  	  <form id="fileMime_form" action="${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_MAPPING}" method="post">
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if fileMime??>${fileMime.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称【不能重复】: 
				</label>
			    <input type="text" data="data" name="name" value="<#if fileMime??>${fileMime.name}</#if>" class="form-control" id="name" placeholder="输入名称，必须唯一" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="suffix">扩展名: 
				</label>
			  <input type="text" data="data" name="suffix" value="<#if fileMime??>${fileMime.suffix}</#if>" class="form-control" id="suffix" placeholder="输入扩展名"/>
			  </div>
			   <div class="form-group ">
			    <label class="control-label" for="mime">MIME: 
				</label>
			  <input type="text" data="data" name="mime" value="<#if fileMime??>${fileMime.mime}</#if>" class="form-control" id="mime" placeholder="输入MIME类型"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if fileMime??>${fileMime.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">所属分类: 
				</label>
				<@getOwnerSystemGallery fileMime=fileMime systemGalleryList=systemGalleryList name="systemGallery.id"/>
			  </div>
			  <div class="form-group">
			  <label class="control-label" for="allowable">是否允许上传:
				</label>
			    <input type="checkbox" data="data" name="allowable" <#if fileMime?? && fileMime.allowable>checked</#if> class="form-control" id="allowable" />
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if fileMime??>${fileMime.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#fileMime_form','#fileSystemGallery_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#fileMime_form')" value='清空'/>
				</li>
		      </ul>
		</div>	 
		<div class="col-md-8">
		  	  <@listFileMime page=page editNavigation=editNavigation/>
		 </div>	
		 <script type="text/javascript">
		 			function cleanForm(formId){
		 				if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				if($(this).attr("type")=="checkbox"){
				  					$(this).attr("checked",false);
				  				}else{
				  					$(this).attr("value","");
				  				}
				  			});
			  			}
		 			}
		 			function submit(formId,backDataContainerId){
		 				var request = new Object();
			  			if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				
				  				if($(this).attr("type")=="checkbox"){
				  					if($(this).attr('checked')==undefined){
				  						request[$(this).attr("name")]='false';
				  					}else{
				  						request[$(this).attr("name")]='true';
				  					}
				  					alert($(this).attr("checked"));
				  				}else{
				  					request[$(this).attr("name")]=$(this).attr("value");
				  				}
				  			});
			  			}
			  			var _select = formId+" select[data$='data']";
			  			if($(_select).length>0){
				  			$(_select).each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  				//alert($(this).attr("value"));
				  			});
			  			}
			  			
						var action = $(formId).attr("action");
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
					function removeItem(id,action,backDataContainerId){
						if(id==''){
							alert("没有id");
							return;
						}
						var request = {'id':id};
						
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
		 </script> 
</div>
</#macro>
