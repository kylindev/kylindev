<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction.ftl">

<@spring.bind "article.uniqueIdentifier" /> 
<#assign currentArticleId = article.uniqueIdentifier /> 
<#assign depth = 1 />  
<@asignCategoryForArticle children=categoryList currentArticleId=currentArticleId/> 
</div>
