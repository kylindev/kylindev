<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_user.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户注册</title>
</head>

<div id="top_banner">

	<h3> <img src="/upload/2013/11/13/logodujia.gif"/> Welcome ${name} to jit8.org, jit8 <small>means</small> 就 <span class="text-danger"> <strong>他/她/它</strong></span> 吧！</h3>
	<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>
	<h2 class="text-danger text-center">说走就走，潇洒自由</h2>
	<#--
	<p class="text-primary">我喜欢旅行，也喜欢分享，一路行程，独家记忆，我怀着真诚地心而来，若我的脚步无法为你停留，只因你未曾在乎过我... 希望我的旅程与感受带给你们快乐，走到那里会有一个素昧蒙面的朋友在等待......</p>
	<h5>每个人都有一个属于自己的品牌，这便是贯穿你生命历程的整个人生，将自己记录，寄托希望与心愿，便是积累力量，不仅仅是分享快乐与资源，而且将你的资源价值化，将你的人脉资产化，慢慢传播影响力，发挥你的优势，让更多的人受益，这是你一生的品牌，用你的<span class="label label-success">诚信牌照</span>经营好自己的网络人生.....</h5>
	-->
</div>

<body>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-4">
			<!--
			<img src="/assets/images/user/register.jpg" title="梧桐山顶，望向远方" alt="梧桐山顶，望向远方" class="img-rounded">
			
			<p class="lead">寻梦，在远方...</p>
			-->
			<div class="panel panel-info">
				<div class="panel-heading ">最新注册</div>
				<div class="panel-body">
					   <@lastRegisterUserListSimple page=page/>
						
						<button class="btn btn-primary" data-toggle="modal" data-target=".bs-lastUser-modal-lg" onclick="getLastRegisterList('${KylinboyCoreDeveloperConstant.KYLINBOY_USER_LAST_REGISTER_LIST_UI_MAPPING}','#lastRegisterContainer')">查看最新注册用户【 50 】名</button>
	
						<div class="modal fade bs-lastUser-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						  <div class="modal-dialog modal-lg">
						    <div class="modal-content">
						   	 <div class="modal-header">
						        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true" class="text-danger">&times;</span><span class="sr-only text-danger">Close</span></button>
						        <h4 class="modal-title text-info">最新注册用户【 50 】名</h4>
						      </div>
						      <div class="modal-body">
						        <div id="lastRegisterContainer">
								</div>
						      </div>
						      <div class="modal-footer">
						        <button type="button" class="btn btn-danger" data-dismiss="modal">关闭</button>
						      </div>
						    
						    </div>
						  </div>
						</div>
				  </div>
				
			</div>
		</div>
	  <div class="col-md-8">
		  <div class="panel panel-warning">
			<div class="panel-heading ">从这里注册: 请填写以下必填信息</div>
			  <@form.form commandName="user"  method="post" action="${KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_MAPPING}" >
		      <@spring.bind "user.username" />  
		      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="username">用户名（允许输入 数字0-9，字母a-zA-Z，下划线_，中文）: 
					<#if (spring.status.errorMessages?size>0)>
					  <br/>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
				</label>
			    <input type="text" name="username" class="form-control" id="username" placeholder="输入用户名，必须唯一" value="${user.username}" />
			  </div>
			  
			  <@spring.bind "user.email" /> 
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="email">Email邮箱:
					<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="email" class="form-control" id="email" placeholder="Enter email" value="${user.email}" />
			  </div>
			  
			  <@spring.bind "user.password" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="password">密码:
					<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="password" name="password" class="form-control" id="password" placeholder="输入密码" value="${user.password}" />
			  </div>
			  
			  <@spring.bind "user.confirmPassword" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="confirmPassword">确认密码（必须与密码相同）
					<#if (spring.status.errorMessages?size>0)>
						<br/>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="password" name="confirmPassword" class="form-control" id="confirmPassword" value="" placeholder="再次输入密码" />
			  </div>
			   
			   <@spring.bind "user.firstname" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="firstname">大名（准确填写真实名字，以后不能修改）:
			    	
					<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="firstname" class="form-control" id="firstname" placeholder="输入名字" value="${user.firstname}" />
			  </div>
			  
			  <@spring.bind "user.lastname" /> 
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="lastname">尊姓（准确填写真实姓氏，以后不能修改）:
			    	 
					<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="lastname" class="form-control" id="lastname" placeholder="输入姓氏" value="${user.lastname}" />
			  </div>
			  
			  <@spring.bind "user.comeLocation" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="comeLocation">来自哪里?
					<#if (spring.status.errorMessages?size>0)>
					  <br/>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="comeLocation" class="form-control" id="comeLocation" placeholder="输入你来自何方，不要输 [火星] 哦"  value="${user.comeLocation}" />
			  </div>
			  
			  <@spring.bind "user.civilit" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="civilit">职业或称谓：如:网友，工程师，探索者，老师（将会在‘最新注册’显示的哦，例如：'小明'是来自'火星'的'探索者'）
			    	
					<#if (spring.status.errorMessages?size>0)>
					  <br/>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="civilit" class="form-control" id="civilit" placeholder="输入职业或称谓 选个最相近的吧【网友，朋友，工程师，设计者，旅行者，老师，哥们儿，摄影爱好者，天文爱好者...】"  value="${user.civilit}" />
			  </div>
			  
			   <@spring.bind "user.manifesto" />  
			  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="manifesto">说一句你最想说的话 (请使用文明用语，恶意发言者帐号将永久被锁)
			    	<#if (spring.status.errorMessages?size>0)>
					  <br/>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
					</#if>
			    </label>
			    <input type="text" name="manifesto" class="form-control" id="manifesto" placeholder="向站上的朋友们说一句你最想说的话"  value="${user.manifesto}" />
			  </div>
			  
			  <#--
			  <div class="form-group">
			  	身份:
				  <#list ownRelationList as or>
				  <label class="radio-inline">${or.name}
				  <input type="radio" name="ownRelation.id" id="ownRelation.code[${or.id}]" value="${or.id}" >
				  </label>
				  </#list>
		  	   </div>
			  -->
			  <ul class="list-inline">
					  <li>
				       		<input type="submit" value="注册新用户" class="btn btn-primary"/>
				       </li>
				       <li>
				       		<p class="text-info">没有帐号，点击注册新用户 | <a class="text-danger" href='${KylinboyCoreDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING}'>已有帐号，点此进入登录页</a></p>
				       </li>
		       </ul>
		  </@form.form>
		  </div>
	  </div>
	  <script type="text/javascript">
	  function getLastRegisterList(url,containerId){
	  		var request={};
	  		var dataExist = $(containerId).find("dd");
	  		if(dataExist.length<=0){
	  			getContent(url,request,containerId);
	  		}
	  }
	  </script>
	</div>
</div>

</body>

<div id="body_bottom">
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>