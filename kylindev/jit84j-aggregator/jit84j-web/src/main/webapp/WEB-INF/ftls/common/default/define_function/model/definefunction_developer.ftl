
<#macro listDeveloper page developer>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>用户身份：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级用户</td>
</#list>
</tr>
</table>
  <table class="table">
  <@form.form commandName="developer" id="developer_list_form" method="post" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING}">
  <thead>
  <tr>
      <td width="2%"><input type="text" placeholder="Id" size="1"></th>
      <td width="5%"><input type="text" placeholder="用户名" size="5" name="user.username" value="<#if developer?? && developer.user??>${developer.user.username}</#if>"></th>
      <td width="8%"><input type="text" placeholder="Email" size="5" name="user.email" value="<#if developer?? && developer.user??>${developer.user.email}</#if>"></th>
      <td width="8%">
      	<input type="text" placeholder="姓氏" size="2" name="user.lastname" value="<#if developer?? && developer.user??>${developer.user.lastname}</#if>">
      	<input type="text" placeholder="名字" size="3" name="user.firstname" value="<#if developer?? && developer.user??>${developer.user.firstname}</#if>">
      </th>
      <td width="15%">
      	<select placeholder="与站长关系" name="user.ownRelationId" value="<#if developer?? && developer.user??>${developer.user.ownRelationId}</#if>">
      			<option value="">ALL</option>
      			<#list ownRelationList as or>
			  	<option value="${or.id}" <#if developer?? && developer.user?? && developer.user.ownRelationId == or.id>selected</#if>>${or.name}</option>
				</#list>
      	</select>
      </th>
      <td width="5%"><input type="text" placeholder="昵称" size="5" name="user.nickname" value="<#if developer?? && developer.user??>${developer.user.nickname}</#if>"></th>
      <td width="10%"><input type="text" placeholder="前后分隔符" size="6"></th>
      <td width="10%"><input type="text" placeholder="用户分隔符" size="6"></th>
      <td width="10%"><input type="text" placeholder="code分隔符" size="6"></th>
      <td width="10%"><input type="text" placeholder="开发ID" size="4" name="userId.userId" value="<#if developer?? && developer.userId??>${developer.userId.userId}</#if>"></th>
      <td width="25%"><input type="button" onclick="formSubmit(1)" value="搜索" class="btn btn-primary"/></th>
    </tr>
  
    <tr>
      <th width="2%">Id</th>
      <th width="5%">用户名</th>
      <th width="8%">Email</th>
      <th width="8%">姓名</th>
      <th width="15%">与站长关系</th>
      <th width="5%">昵称</th>
      <th width="10%">前后分隔符</th>
      <th width="10%">用户分隔符</th>
      <th width="10%">code分隔符</th>
      <th width="10%">开发ID</th>
      <th width="25%">操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page??)>
  	 <#assign depth = 1 />  
	<@listDeveloperItem page=page/>
  </#if>
	<tr>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_APPLY_DEVELOPER_UI_MAPPING}/apply_developer"/>'>
				<input type="button" value="申请开发者" class="btn btn-primary"/>
			</a>
		</td>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_UI_MAPPING}"/>'>
				<input type="button" value="添加开发者" class="btn btn-primary"/>
			</a>
		</td>
	 </tr>
	</@form.form>
   </tbody>
  </table>
</div>
</#macro>

<#macro listDeveloperItem page>

 <#assign developerList = page.content />
	<#if developerList??>
		<#list developerList as developer>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth-1/>
			 
			 <tr ${trClass}>
			 <td>
			 ${developer.uniqueIdentifier}
			<#-- <input type="checkbox" name="developer.idList" value="${developer.uniqueIdentifier}">-->
			 </td>
			 <td>
			 ${developer.user.username}
			 </td>
			 <td>
			 ${developer.user.email}
			 </td>
			 <td>
			 ${developer.user.firstname}${developer.user.lastname}
			 </td>
			  <td>
			  <#if developer.user.ownRelation?exists>
			  ${developer.user.ownRelation.name}
			  </#if>
			 </td>
			 <td>
			 	${developer.user.nickname}
			 </td>
			  <td>
			 	${developer.seperator}
			 </td>
			 <td>
			 	${developer.userIdSeperator}
			 </td>
			  <td>
			 	${developer.oprationSeperator}
			 </td>
			 <td>
			 	${developer.userId.userId}
			 </td>
			 <td>
			 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/navigationModifyUI/${developer.uniqueIdentifier}"/>'>编 辑</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/delete/${developer.uniqueIdentifier}"/>'>删 除</a>
				    </li>
				    <li>
				    	<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${developer.uniqueIdentifier}"/>'>查看</a>
				    </li>
				    <li>
				    	<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${developer.uniqueIdentifier}"/>'>申请开发者</a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		
		<tr>
		<td colspan="11">
		<@pageBar page = page url="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_LIST_UI_MAPPING}" formId='developer_list_form'/>
		</td>
		</tr>
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>





<#-- 分配角色-->
<#macro listUserForAsignRole developerList roleList>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>用户身份：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级用户</td>
</#list>
</tr>
</table>
  <table class="table">
  <thead>
  <tr>
      <td width="3%">
      <input type="text" placeholder="id" size="3"> 
      </td>
      <td width="10%"><input type="text" placeholder="用户名" size="5"></td>
      <td width="10%"><input type="text" placeholder="Email"></td>
      <td width="15%"><input type="text" placeholder="姓氏" size="5"><input type="text" placeholder="名字" size="5"></td>
      <td width="15%"><input type="text" placeholder="昵称"></td>
      <td width="15%"><input type="text" placeholder="用户角色"></td>
      <td width="25%"><input type="button" value="搜索" class="btn btn-primary"/></td>
    </tr>
    <tr>
      <th width="3%">全选
      <input type="checkbox" name="select_all" id="select_all" value="all">
      </th>
      <th width="10%">用户名</th>
      <th width="10%">Email</th>
      <th width="15%">姓名</th>
      <th width="15%">昵称</th>
      <th width="15%">用户角色</th>
      <th width="25%">操作角色</th>
    </tr>
  </thead>
  <tbody>
  <@form.form commandName="userRole" role="form" action="/admin/userinfo/asignRolesForUser/user_asign_roles">
  <#if (developerList??)>
  	 <#assign depth = 1 />  
	<@listUserlistUserForAsignRoleItem developerList=developerList/>
  </#if>
  <tr>
  	<td>选择角色:</td>
    <td  colspan="6">
      <@selectRoleInLine roleList=roleList checkboxName="roleIdList"/>
     </td>
   </tr>
   <tr>
		<td>
			<a class="text-danger" href='<@spring.url relativeUrl="/admin/userinfo/addUserUI/user_add"/>'>
				<input type="button" value="添加用户" class="btn btn-primary"/>
			</a>
		</td>
		<td colspan="6">
				<input type="submit" value="分配角色" class="btn btn-primary"/>
		</td>
	 </tr>
	 
	</@form.form>
   </tbody>
  </table>
</div>
</#macro>

<#macro listDeveloperForAsignRoleItem developerList>
	<#if developerList??>
		<#list developerList as user>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth-1/>
			 
			 <tr ${trClass}>
			 <td>
			 ${user.uniqueIdentifier}
			 <input type="checkbox" name="userIdList" value="${user.uniqueIdentifier}">
			 </td>
			 <td>
			 ${user.username}
			 </td>
			 <td>
			 ${user.email}
			 </td>
			 <td>
			 ${user.firstname}${user.lastname}
			 </td>
			 <td>
			 ${user.nickname}
			 </td>
			 <td id="user_role_list_${user.uniqueIdentifier}">
			 <@listRoleName user=user/>
			 </td>
			 <td>
			 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:addRoles("${user.uniqueIdentifier}")'>添加绑定</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href="javascript:void(0)" onclick="removeRoles('${user.uniqueIdentifier}','user_asign_roles')">全部解除</a>
				    </li>
				    <li>
				    	<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${user.uniqueIdentifier}"/>'>查看</a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		
		<script type="text/javascript">
			function addRoles(userId){
				
			}
			function removeRoles(userId,code){
			    alert(userId + code);
			    var action = "/admin/userinfo/removeRolesForUser/" + code;
				var request={
					"code":code,
					"userIdList":userId
				};
				$.ajax({
					url: action,
					data: request,
					type: "POST",
					dataType : "html",
					success: function( val ) {
						$( "#user_role_list_"+userId ).html( val );
					},
					error: function( xhr, status ) {
						alert( "Sorry, there was a problem!" );
					},
					complete: function( xhr, status ) {
					}
				});
			}
		</script>
		
	</#if>
		
</#macro>

<#macro listRoleName user>
<#if user??>
	 <#assign userRoleList = user.roleList />
	 <#if userRoleList??>
	 	<ul class="list-inline">
				<#list userRoleList as userRole>
					<li>
					${userRole.role.rolename}
					<li>
				</#list>
		</ul>
	 </#if>
</#if>
</#macro>



<#macro selectDeveloperForPermission page currentDeveloper='default'>
	<div class="panel panel-info">
		<div class="panel-heading ">选择开发者</div>
			<@listDeveloperForPermission developerList=page.content currentDeveloper=currentDeveloper/>
			
	</div>
</#macro>

<#macro listDeveloperForPermission developerList currentDeveloper='default'>
	<#if developerList??>
		<ul class="list-inline">
		<#list developerList as developer>
			<li>  <input type="radio" name="developer.id" value="${developer.uniqueIdentifier}" 
			<#if currentDeveloper?? && currentDeveloper !='default' && currentDeveloper.id == developer.id> checked</#if>
			/> ${developer.user.username}
				[ ${developer.userId.userId} ]
				${developer.user.email}
			</li>
		</#list>
		</ul>
	</#if>
</#macro>


<#macro selectDeveloperWithQuery page currentDeveloper='default'>
	<div class="panel panel-info">
		<div class="panel-heading ">选择开发者</div>
			<@listDeveloperForPermission developerList=page.content currentDeveloper=currentDeveloper/>
			<@pageBarAjax url='' page=page/>
	</div>
</#macro>

