<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>文章列表</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-12">
<p class="text-success">文章列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>文章名称</th>
      <th>作者</th>
      <th>笔名</th>
      <th>排序</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "articleList" />  
  <@form.form commandName="articleList" role="form" action="/articleModifyUI">
  <tbody>
  <#if (articleList??)>

	<#list articleList as article>  


	 <tr>
	 <td>${article.uniqueIdentifier}
	 <input type="hidden" id="articleList${article_index}_id" name="articleList[${article_index}].id" value="${article.id}">
	 </td>
	 <td>
	 <input type="text" name="articleList[${article_index}].title" value="${article.title}" class="form-control" id="articleList${article_index}_title" placeholder="输入代码"/>
	 </td>
	 <td>
	 <input type="text" name="articleList[${article_index}].authorName" value="${article.authorName}" class="form-control" id="articleList${article_index}_authorName" placeholder="输入名称"/>
	 </td>
	 <td>
	 <input type="text" name="articleList[${article_index}].penName" value="${article.penName}" class="form-control" id="articleList${article_index}_penName" placeholder="站点路径"/>
	 </td>
	 <td>
	 <input type="text" name="articleList[${article_index}].sequence" value="${article.sequence}" class="form-control" id="articleList${article_index}_sequence" placeholder="排序"/>
	 </td>
	 <td>
	 <a class="text-danger" href='<@spring.url relativeUrl="/admin/article/articleModifyUI/${article.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/article/articleDelete/${article.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
	  | <input type="button" value="保 存" class="btn btn-primary" id="articleList${article_index}_persist_button" onclick="saveSingleArticle('articleList${article_index}')"/>
	 </td>
	 </tr>
	</#list>
	
  </#if>
   </tbody>
  </table>
</div>
 </div>
 </@form.form>
  
</div>
</div>
<script type="text/javascript">
function saveSingleArticle(id){
	var _id = "#" + id + "_id";
	var _title = "#" + id + "_title";
	var _authorName = "#" + id + "_authorName";
	var _penName = "#" + id + "_penName";
	var _sequence = "#" + id + "_sequence";
	var _persit_button = "#" + id + "_persist_button";
	
	var articleId=$(_id).attr("value");
	var title = $(_title).attr("value");
	//alert(title);
	var authorName = $(_authorName).attr("value");
	var penName = $(_penName).attr("value");
	var sequence = $(_sequence).attr("value");
	
	var request={
	"id":articleId,
	"title":title,
	"authorName":authorName,
	"penName":penName,
	"sequence":sequence
	}
	var action = "/admin/article/articleModify/simple";
	//alert(title + "  " + authorName + "  " + penName + "  " + sequence);
		$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "text",
			contentType:'application/json;charset=UTF-8', 
			success: function(data) {
				if("success"==data){
					alert("修改成功");
				}
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
			}
		});
}
</script>
</body>
</html>