<#-- URL Mapping列表 -->
<#macro listUrlMappingTree page>
<#assign urlMappingList = page.content />
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>颜色标识：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级菜单</td>
</#list>
</tr>
</table>

	
	
	<div class="panel panel-default">
			  <!-- Default panel contents -->
	<div class="panel-heading">系统预定义路径(<span id="url_path_total"></span>)</div>
		  <!-- Table -->
	  <table class="table">
	
	    
	  
	  	<@form.form commandName="permission" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_LIST_MODIFY_MAPPING}/authority_list">
	  <#if (urlMappingList??)>
	  	 <#assign depth = 0 />  
	  	 <#assign countNum = 0 />
	  	  <#assign total = 0 /> 
			<@listUrlMappingTreeItem page=page/>
		<span class="hidden" id="url_path_total_count">${total}</span>
	  </#if>
	    <tr>
			<td>
			<#if (urlMappingList??)>
				<a class="btn btn-primary" onClick="importUrlMapping('<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_IMPORT_MAPPING}"/>')" href='javascript:void(0)'>
				导入系统预定义URL Mapping到数据库 
				</a>
			</#if>
			</td>
			<td>
				<a class="btn btn-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_ADD_UI_MAPPING}/authority_add"/>'>
					添加自定义URL路径
				</a>
			</td>
		 </tr>
		</@form.form>
	  </table>
	  
  </div>
</div>
</#macro>

<#macro listUrlMappingTreeItem page>
<#assign urlStaticMappingList = page.content />
	<#if urlStaticMappingList??>
		<#list urlStaticMappingList as urlStaticMapping>  
			 <#assign depth = depth + 1 />
			 
			 <#assign trClass = '' />
			 <@getClassColor depth/>
			
			 <tr ${trClass}>
				 <td>${countNum} <#list 1..depth as i> > </#list>
				 
				 <ul>
					<li>			
						序    号: ${countNum}
					</li>
					<li>模块唯一键:	${urlStaticMapping.idKey}</li>
				 	<li>模块名称:	${urlStaticMapping.moduleName}</li>
				 	<li>模块代码:	${urlStaticMapping.moduleCode}</li>
				 </ul>
				 </td>
				 <td>
				 <@urlStaticMappingItem urlStaticMappingList=urlStaticMapping.childUrlList/>
				 </td>
			 </tr>
			 <#assign countNum = countNum + 1 />
			 <#assign depth = depth-1 />
		</#list>
		<script type="text/javascript">
			function importUrlMapping(url){
				alert("import url begin");
				$.ajax({
			       type: "POST",
			       url: url,
			       data: "",
			       dataType:"json",
			       async: true,
			       error: function(data, error) {
			       		alert(error);
			       },
			       success: function(data)
			       {
			       	alert(data.msg);
			       }
				}); 
			}
		</script>
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>

<#macro urlStaticMappingItem urlStaticMappingList>
	<#if urlStaticMappingList??>
	
		<div class="panel panel-default">
		  <!-- Default panel contents -->
		  <div class="panel-heading">资源路径(<span name="url_path_part_num">${urlStaticMappingList?size}</span>)</div>
		
		  <!-- Table -->
		  <table class="table">
		  <tr><th>映射唯一键</th><th>映射代码</th><th width="50%">full Path</th><th>映射名称</th></tr>
		    <#list urlStaticMappingList as urlStaticMapping> 
				<#assign total = total + 1 />
					<tr>
					<td ><small>${urlStaticMapping.idKey!}<small></td>
					<td ><small>${urlStaticMapping.urlCode!}<small></td>
					<td><a href="javascript:void(0)" title="${urlStaticMapping.description}"><small>${urlStaticMapping.urlFullPath}</small></a></td>
					<td>${urlStaticMapping.urlName}</td>
					</tr>
				</ul>
			</#list>
		  </table>
			
		</div>
	</#if>
</#macro>



<#-- 树形URL分类目录列表 for 角色 begin -->
<#macro urlStaticMappingCategoryListTree page expandedCategory='first' radioName="parent.id" removeUrl="" treeId="">  
<div id="tree_urlStaticMapping${treeId}">
<#if page.content?? && page.content?size gt 0>  
	<ul id="treeData_urlStaticMapping" style="display: block;">
	 	<#assign depth = 1 />  
		<@urlStaticMappingCategoryItem children=page.content expandedCategory=expandedCategory radioName=radioName removeUrl=removeUrl/>
	</ul>
</#if>
</div>
</#macro>

<#macro urlStaticMappingCategoryItem children expandedCategory='first' radioName="parent.id" removeUrl="">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
            <#if expandedCategory?? >
	            <li id="id_${child.idKey}" class="folder"
	            >
	            <label id="description_${child.idKey}" title='${(child.decription)?if_exists}'>
		           		<span id="name_${child.idKey}">${(child.urlName)?if_exists}</span>
		           		【<span id="code_${child.idKey}">${(child.urlCode)?if_exists}</span>】
		         </label>
	            
	            <#assign depth = depth + 1 />  
	            <ul> 
	            <#if child.childUrlList?? && child.childUrlList?size gt 0> 
	            	<@urlStaticMappingCategoryDetail children = child.childUrlList radioName=radioName removeUrl=removeUrl/> 
	            	</#if>
	            </ul> 
	            <#assign depth = depth - 1 /> 
             	</li>
             </#if>
        </#list>  
      
    </#if>  	
</#macro>

<#macro urlStaticMappingCategoryDetail children radioName="parent.id" removeUrl="">  
    <#if children?? && children?size gt 0>  
    
        <#list children as child>  
	          <script type="text/javascript">
	            
    			</script>
	            <li class="text-danger" id="permission_id_${child.idKey}" title='${(child.decription)?if_exists}'>
            	
		             	<@form.form commandName="urlStaticMapping" id="permisson_category_form_id_${child.idKey}" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_FROM_MAPPING}#panel-second">
		           		<span>
		           		----
		           		<span class="text-danger" id="name_${child.idKey}">${(child.urlName)?if_exists}</span>
		           		【<span class="text-danger" id="code_${child.idKey}">${(child.urlCode)?if_exists}</span>】
		           		【<span class="text-danger" id="fullUrl_${child.idKey}">${(child.urlFullPath)?if_exists}</span>】
						
		             	<input type="hidden" id="permission_id_${child.uniqueIdentifier}" name="childUrlList[0].idKey" value="${child.idKey}"  >
		             	<input type="hidden" id="permission_category_id_${child.idKey}" name="id" value="${child.module.idKey}"  />
		             	
		             	</span>
		             	</@form.form>
             	</li>
        </#list>  
      
    </#if>  	
</#macro>
<#-- 树形URL分类目录列表 for 角色 end -->

<#macro urlStaticMappingModule page>  
<div id="tree_urlStaticMapping">

<#if page?? && page.content?? && page.content?size gt 0>  
	<#assign modlueList = page.content />
	<#if modlueList?? && modlueList?size gt 0>  
	
		<div id="treeData_urlStaticMapping" class="list-group">
		 	<#assign depth = 1 />  
		 	<#list modlueList as module> 
		 	<a href="javascript:void(0)" class="list-group-item" id="module_id_li_${module.idKey}" onclick="findUrlMappingListByModuleIdKey('${module.idKey}')">
		 		${module.urlName}
		 		${module.idKey}
		 	</a>
		 	</#list>  
		</div>
		<script type="text/javascript">
			function findUrlMappingListByModuleIdKey(moduleIdKey){
				
				var listItem = $(".list-group-item");
				var id="module_id_li_"+moduleIdKey;
				if(listItem.length>0){
					$(listItem).each(function(){
						if(id==$(this).attr("id")){
							$(this).addClass("active");
						}else{
							$(this).removeClass("active");
						}
					});
					
				}
				
				$.ajax({
					url: '${KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_MAPPING}',
					data: {'idKey':moduleIdKey},
					type: "POST",
					dataType : "text",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								//alert(data);
								$("#url_list_content_div").html(data);
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
			
			
			function bindAllToPermissionSingle(url,idKey){
				if(''!= idKey){
					var request={"idKeys":idKey};
					$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
								//formSubmitAjax(1);
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
				}
			}
		</script>
	</#if>
</#if>
</div>
</#macro>

<#--模块列表 for 绑定菜单 begin-->
<#macro urlStaticMappingModuleForNavigation page>  
<div id="tree_urlStaticMapping">

<#if page?? && page.content?? && page.content?size gt 0>  
	<#assign modlueList = page.content />
	<#if modlueList?? && modlueList?size gt 0>  
	
		<div id="treeData_urlStaticMapping" class="list-group">
		 	<#assign depth = 1 />  
		 	<#list modlueList as module> 
		 	<a href="javascript:void(0)" class="list-group-item" id="module_id_li_${module.idKey}" onclick="findUrlMappingListByModuleIdKey('${module.idKey}')">
		 		${module.urlName}
		 		${module.idKey}
		 	</a>
		 	</#list>  
		</div>
		<script type="text/javascript">
			function findUrlMappingListByModuleIdKey(moduleIdKey){
				
				var listItem = $(".list-group-item");
				var id="module_id_li_"+moduleIdKey;
				if(listItem.length>0){
					$(listItem).each(function(){
						if(id==$(this).attr("id")){
							$(this).addClass("active");
						}else{
							$(this).removeClass("active");
						}
					});
					
				}
				
				$.ajax({
					url: '${KylinboyDeveloperConstant.KYLINBOY_URL_STATIC_MAPPING_LIST_FOR_NAVIGATION_MAPPING}',
					data: {'idKey':moduleIdKey},
					type: "POST",
					dataType : "text",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								//data="{'msg':'<div>ddd</div>'}";
								//var obj = JSON.parse(data); 
								//if(obj.error!=''){
								//	alert(obj.msg);
								//}
								$("#url_list_content_div").html(data);
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
			
			
			/*function bindAllToPermissionSingle(url,idKey){
				if(''!= idKey){
					var request={"idKeys":idKey};
					$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
								//formSubmitAjax(1);
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
				}
			}*/
		</script>
	</#if>
</#if>
</div>
</#macro>
<#--模块列表 for 绑定菜单 end-->

<#--绑定权限 begin-->
<#macro urlStaticMappingList page>  
<div id="tree_urlStaticMapping">

<#if page?? && page.content?? && page.content?size gt 0>  
	<#assign urlStaticMappingList = page.content />
	<#if urlStaticMappingList?? && urlStaticMappingList?size gt 0>  
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${urlStaticMapping.idKey}">
			 		<label>全选 <input type="checkbox" name="urlMapping_checkbox_selectAll" id="urlMapping_checkbox_selectAllCheckbox1" /></label >
			 		(注:解除关联仅限于该权限尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING}')">关联所选权限</button> <button class="btn btn-warning" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING}')">解除所选关联</button>
			 </li> 
		 </ul>
		<ol id="treeData_urlStaticMapping" class="list-unstyled">
		 	<#assign depth = 1 />  
		 	<#list urlStaticMappingList as urlStaticMapping> 
		 	<li class="text-info" style="text-inline:" id="urlStaticMapping_id${urlStaticMapping.idKey}">
		 		<dl>
				  <dt><input type="checkbox" value="${urlStaticMapping.idKey}" id="urlMapping_checkbox_${urlStaticMapping_index}" name="urlMappingIdKey"/>${urlStaticMapping.urlName} - ${urlStaticMapping.idKey} </dt>
				  <dd>${urlStaticMapping.urlFullPath}</dd>
				  <dd><#if urlStaticMapping.urlMapping?? && urlStaticMapping.urlMapping.permission??> <button class="btn btn-warning btn-xs" onClick="bindToPermissionSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING}','${urlStaticMapping.idKey}')">解除关联</button> <#else> <button class="btn btn-primary btn-xs" onClick="bindToPermissionSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING}','${urlStaticMapping.idKey}')">关联权限</button></#if> </dd>
				</dl>
		 	</li>
		 	</#list> 
		 	
		</ol>
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${urlStaticMapping.idKey}">
			 		<label >全选 <input type="checkbox" name="urlMapping_checkbox_selectAll2" id="urlMapping_checkbox_selectAllCheckbox2" /></label >
			 		(注:解除关联仅限于该权限尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_PERMISSION_MAPPING}')">关联所选权限</button> <button class="btn btn-warning" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_PERMISSION_MAPPING}')">解除所选关联</button>
			 </li>
		 </ul>
		 
		<script type="text/javascript">
			function selectAll(){
				if(this.checked){ 
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=true;}); 
				}else{
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=false;}); 
				}
			}
			$("#urlMapping_checkbox_selectAllCheckbox1").click(selectAll);
			$("#urlMapping_checkbox_selectAllCheckbox2").click(selectAll);
			
			
			function bindToPermissionSingle(url,idKey){
				if(''!= idKey){
					var request={"idKeys":idKey};
					submitBindToPermission(url, request);
				}
			}
			
			
			function bindToPermission(url){
				prepareBindToPermission(url);
			}
			
			function prepareBindToPermission(url){
				var requestData="";
				if($("input[id ^='urlMapping_checkbox_']:checked").length>0){
					$("input[id ^='urlMapping_checkbox_']:checked").each(function(){
					if(($(this).attr("id")).indexOf("selectAllCheckbox")<0){
						requestData += $(this).val()+",";
					}
					
				}); 
				
				if(requestData.lastIndexOf(",")>0){
						requestData = requestData.trim();
						requestData = requestData.substring(0,requestData.lastIndexOf(","));
					}
				}
				if(""==requestData){
					alert("没有选择任何权限");
					return;
				}
				var request={"idKeys":requestData};
				submitBindToPermission(url, request);
			}
			
			function submitBindToPermission(url,request){
				$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
								formSubmitAjax('${page.number +1}');
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
		</script>
	<#else>
	   木有数据。
	</#if>
</#if>
</div>
</#macro>
<#--绑定权限 end-->

<#--加入菜单 begin-->
<#macro urlStaticMappingListForNavigation page>  
<div id="tree_urlStaticMapping">

<#if page?? && page.content?? && page.content?size gt 0>  
	<#assign urlStaticMappingList = page.content />
	<#if urlStaticMappingList?? && urlStaticMappingList?size gt 0>  
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${urlStaticMapping.idKey}">
			 		<label>全选 <input type="checkbox" name="urlMapping_checkbox_selectAll" id="urlMapping_checkbox_selectAllCheckbox1" /></label >
			 		(注:解除关联仅限于该菜单尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}')">加入所选至菜单</button> <button class="btn btn-warning" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}')">从菜单移除所选</button>
			 </li> 
		 </ul>
		<ol id="treeData_urlStaticMapping" class="list-unstyled">
		 	<#assign depth = 1 />  
		 	<#list urlStaticMappingList as urlStaticMapping> 
		 	<li class="text-info" style="text-inline:" id="urlStaticMapping_id${urlStaticMapping.idKey}">
		 		<dl>
				  <dt><input type="checkbox" value="${urlStaticMapping.idKey}" id="urlMapping_checkbox_${urlStaticMapping_index}" name="urlMappingIdKey"/>${urlStaticMapping.urlName} - ${urlStaticMapping.idKey} </dt>
				  <dd>${urlStaticMapping.urlFullPath}</dd>
				  <dd><#if urlStaticMapping.urlMapping?? && urlStaticMapping.urlMapping.navigation??> <button class="btn btn-warning btn-xs" onClick="bindToNavigationSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}','${urlStaticMapping.idKey}')">从菜单移除</button> <#else> <button class="btn btn-primary btn-xs" onClick="bindToNavigationSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}','${urlStaticMapping.idKey}')">加入菜单</button></#if> </dd>
				</dl>
		 	</li>
		 	</#list> 
		 	
		</ol>
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${urlStaticMapping.idKey}">
			 		<label >全选 <input type="checkbox" name="urlMapping_checkbox_selectAll2" id="urlMapping_checkbox_selectAllCheckbox2" /></label >
			 		(注:解除关联仅限于该菜单尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToNavigation('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}')">加入所选至菜单</button> <button class="btn btn-warning" onClick="bindToNavigation('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}')">从菜单移除所选</button>
			 </li>
		 </ul>
		 
		<script type="text/javascript">
			function selectAll(){
				if(this.checked){ 
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=true;}); 
				}else{
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=false;}); 
				}
			}
			$("#urlMapping_checkbox_selectAllCheckbox1").click(selectAll);
			$("#urlMapping_checkbox_selectAllCheckbox2").click(selectAll);
			
			
			function bindToNavigationSingle(url,idKey){
				if(''!= idKey){
					var request={"idKeys":idKey};
					submitBindToNavigation(url, request);
				}
			}
			
			
			function bindToNavigation(url){
				prepareBindToNavigation(url);
			}
			
			function prepareBindToNavigation(url){
				var requestData="";
				if($("input[id ^='urlMapping_checkbox_']:checked").length>0){
					$("input[id ^='urlMapping_checkbox_']:checked").each(function(){
					if(($(this).attr("id")).indexOf("selectAllCheckbox")<0){
						requestData += $(this).val()+",";
					}
					
				}); 
				
				if(requestData.lastIndexOf(",")>0){
						requestData = requestData.trim();
						requestData = requestData.substring(0,requestData.lastIndexOf(","));
					}
				}
				if(""==requestData){
					alert("没有选择任何权限");
					return;
				}
				var request={"idKeys":requestData};
				submitBindToNavigation(url, request);
			}
			
			function submitBindToNavigation(url,request){
				$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
								
								formSubmitAjax('${page.number +1}');
								
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
		</script>
	<#else>
	   木有数据。
	</#if>
</#if>
</div>
</#macro>
<#--加入菜单 end-->
