<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_user.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>找回密码</title>
</head>
<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="user" id="reset_password_form_id" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FIND_PASSWORD_MAPPING}">
	<div class="col-md-12">
	<div class="panel panel-warning" >
		<div class="panel-heading ">找回密码</div>
		  
	      <div class="form-group ">
		    <label class="col-sm-3 control-label" for="rolename_1">注册时的邮箱: 
			</label>
				<ul class="list-inline">
				<li>
		    	<@form.input path="email"size="50" class="form-control" id="email" placeholder="输入注册时的Email" required="true"/>
			   </li>
			   <li>
			     <button  class="btn btn-primary" onclick="findPassword('#email','#reset_password_form_id','${KylinboyDeveloperConstant.KYLINBOY_FIND_PASSWORD_MAPPING}')">
			     点击找回密码
			     </button>
			   </li>
			   </ul>
	       </div>
	  </div>
	</div>
</@form.form>
</div>
</div>
<script type="text/javascript">
function findPassword(email,formId,action){
	var value = $(email).val();
	if(''!=value){
		var form = $(formId).attr("action",action)
		form.submit();
	}else{
		alert('注册邮箱不能为空！');
	}
}
</script>
</body>
</html>