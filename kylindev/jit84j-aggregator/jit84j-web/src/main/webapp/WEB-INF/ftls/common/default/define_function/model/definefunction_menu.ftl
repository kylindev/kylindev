
<#macro asignChildrenNavigatMenuCheckbox children currentNavigatMenu checkboxName="childrenNavigatMenu">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <#if currentNavigatMenu?? && currentNavigatMenu.id!=child.id>
	            <input type="checkbox" value="${child.uniqueIdentifier}" name="${checkboxName}[${child_index}].id" id="navigatMenu_${child.uniqueIdentifier}"
	            	<#if currentNavigatMenu?? && child.parent?? && currentNavigatMenu.id==child.parent.id> checked </#if>
	            	<#if child?? && !(child.parent?exists)> disabled </#if>
	            />
            </#if>
            
            <#if currentNavigatMenu?? && currentNavigatMenu.id==child.id>
	             	<font color="red">
	         </#if>
             ${(child.name)?if_exists}
             <#if currentNavigatMenu?? && currentNavigatMenu.id==child.id>
	             	</font>
	         </#if>
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenNavigatMenu?? && child.childrenNavigatMenu?size gt 0>  
            <@asignChildrenNavigatMenuCheckbox children = child.childrenNavigatMenu currentNavigatMenu=currentNavigatMenu checkboxName=checkboxName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
        
    </#if>  	
</#macro> 


<#--保存类别-->
<#macro persistNavigatMenu children current>

<form action="/admin/navigatMenu/addChilrenNavigatMenu" id="addChilrenNavigatMenu" method="post">
<input type="hidden" name="currentNavigatMenuId" value="current.id">为[${current.name}]分配子类:<br/>
<@buildNodeCheckbox children = children checkboxName="childrenNavigatMenuIds"/>  
<input type="submit" value="保存">
<input type="reset" value="重置">
</form>
</#macro> 



<#--选择父类-->
<#macro selectParentNavigatMenuRadio children currentNavigatMenu radioName="parent.id">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <#if currentNavigatMenu?? && currentNavigatMenu.id!=child.id>
            	<input type="Radio" value="${child.uniqueIdentifier}" name="${radioName}" id="select_parent_navigatMenu_${child.uniqueIdentifier}" <#if currentNavigatMenu?? && currentNavigatMenu.parent?? && currentNavigatMenu.parent.id == child.id> checked</#if>/>
            </#if>
	             <#if currentNavigatMenu?? && currentNavigatMenu.id==child.id>
	             	<font color="red">
	             </#if>
             ${(child.name)?if_exists}
	             <#if currentNavigatMenu?? && currentNavigatMenu.id==child.id>
	             	</font>
	             </#if>
             
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenNavigatMenu?? && child.childrenNavigatMenu?size gt 0>  
            	<@selectParentNavigatMenuRadio children = child.childrenNavigatMenu currentNavigatMenu=currentNavigatMenu radioName=radioName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro>


<#macro navigatMenuTopbar navigatMenuList>
<ul class="nav navbar-nav">
	   <#if (navigatMenuList??)>
	   	<#list navigatMenuList as navigatMenu> 
	   	  <#if !(navigatMenu.childrenNavigatMenu?? && navigatMenu.childrenNavigatMenu?size gt 0)>
	      	 <li ><a href="${navigatMenu.url}">${navigatMenu.name}</a></li>
	      </#if>
	      <#if navigatMenu.childrenNavigatMenu?? && navigatMenu.childrenNavigatMenu?size gt 0>
	      	<li class="dropdown">
	      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">${navigatMenu.name}
	      	<b class="caret"></b></a>
	      	<ul class="dropdown-menu">
	      	<#list navigatMenu.childrenNavigatMenu as navigatMenu2> 
	      		<li><a href="${navigatMenu2.url}">${navigatMenu2.name}</a></li>
	      	</#list>
	      	</ul>
	      	</li>
	      </#if>
	     </#list>
	    </#if>
</ul>
</#macro>

<#macro navigatMenuTopbarDropdown navigatMenuList >
	<ul <#if depth = 0>class="dropdown-menu" </#if>>
	
	  	<#list navigatMenuList as navigation> 
	  	<#assign depth = depth + 1 />
	  		<li><a href="${navigation.url}">${navigation.name}</a></li>
	  		 <#if navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0>
	  		 	 <@navigatMenuTopbarDropdown navigatMenuList=navigation.childrenNavigation/>
	  		 </#if>
	  	<#assign depth = depth - 1 />
	  		 <li class="divider"></li>
	  	</#list>
	  	
  	</ul>
</#macro>

<#macro navigatMenuTopbar navigatMenuList currentNavigatMenu='default'>
	<#if navigatMenuList?? && navigatMenuList?size gt 0>
		 <ul class="nav nav-tabs nav-justified">
		  	 <#list navigatMenuList as navigation> 
		   	  <#if !(navigation.childrenNavigatMenu?? && navigation.childrenNavigatMenu?size gt 0)>
		      	 <li <#if currentNavigatMenu?? && currentNavigatMenu!='default' && currentNavigatMenu.code == navigation.code> class="active" </#if>><a href="${navigation.url}"><h4 class="text-primary">${navigation.name}</h4></a></li>
		      </#if>
		      <#if navigation.childrenNavigatMenu?? && navigation.childrenNavigatMenu?size gt 0>
		      	<li class="dropdown">
		      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">${navigation.name}
		      	<b class="caret"></b></a>
		      	<#assign depth = 0/>
		      	<@navigatMenuTopbarDropdown navigatMenuList=navigation.childrenNavigatMenu/>
		      	</li>
		      </#if>
		     </#list>
		  </ul>
	</#if>
</#macro>

<#macro navigatMenuTopbar2 navigatMenuList currentNavigatMenu='default'>
<nav class="navbar navbar-default" role="navigation" style="z-index:100">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
	    <ul class="nav navbar-nav">
	   <#if (navigatMenuList??)>
	   	<#list navigatMenuList as navigation> 
	   	  <#if !(navigation.childrenNavigatMenu?? && navigation.childrenNavigatMenu?size gt 0)>
	      	 <li ><a href="${navigation.url}">${navigation.name}</a></li>
	      </#if>
	      <#if navigation.childrenNavigatMenu?? && navigation.childrenNavigatMenu?size gt 0>
	      	<li class="dropdown">
	      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">${navigation.name}
	      	<b class="caret"></b></a>
	      	<#assign depth = 0/>
	      	<@navigatMenuTopbarDropdown navigatMenuList=navigation.childrenNavigatMenu/>
	      	</li>
	      </#if>
	     </#list>
	    </#if>
	    </ul>
	    
	    <#--
	    <form class="navbar-form navbar-right" role="search">
	      <div class="form-group">
	        <input type="text" class="form-control" placeholder="Search">
	      </div>
	      <button type="submit" class="btn btn-default">Submit</button>
	    </form>
	    -->
	  </div>
	</nav>
	
	<#if (currentNavigatMenu?? && currentNavigatMenu !='default')>
		<@breadcrumbNavigatMenu currentNavigatMenu=currentNavigatMenu/>
	</#if>
</#macro>


<#macro breadcrumbNavigatMenu currentNavigatMenu>
<#if currentNavigatMenu??>
	<ol class="breadcrumb-small">
	<#if currentNavigatMenu.parent??>
	  <@breadcrumbNavigatMenuItem currentNavigatMenu.parent/>
	</#if>
	  <li class="active">${currentNavigatMenu.name}</li>
	</ol>
</#if>
</#macro>

<#macro breadcrumbNavigatMenuItem parentNavigatMenu>
<#if parentNavigatMenu??>
	<#if parentNavigatMenu.parent??>
		<@breadcrumbNavigatMenuItem parentNavigatMenu.parent/>
	</#if>
	<li><a href="${parentNavigatMenu.url}">${parentNavigatMenu.name}</a></li>
</#if>
</#macro>
















<#macro listNavigatMenuTreeItem currentNavigatMenu>
	<#if currentNavigatMenu??>
		<#assign children = currentNavigatMenu.childrenNavigatMenu/> 
	    <#if children?? && children?size gt 0>  
	        <#list children as navigatMenu>  
	        <#assign depth = depth + 1 />  
	        <div class="panel panel-info pull-left" style="width:250px">
			  	<div class="panel-heading"><#list 1..depth as i>></#list>${navigatMenu.name} </div>
			  	<div class="panel-body">
				  	<div class="form-group">
					    <label class="col-sm-3 control-label" for="navigatMenuList[${countNum}].id">Id</label>
					 	${navigatMenu.uniqueIdentifier}
					 	<input type="hidden" class="form-control" id="navigatMenuList[${countNum}].id" name="navigatMenuList[${countNum}].id" value="${navigatMenu.uniqueIdentifier}">
					 	
				 	</div>
			 
			 		<div class="form-group">
						    <label class="col-sm-3 control-label" for="sequence[${countNum}]">排序</label>
					 		<div class="col-sm-9">
					 			<input type="text" name="navigatMenuList[${countNum}].sequence" value="${navigatMenu.sequence}" class="form-control input-sm" id="sequence[${countNum}]" placeholder="排序"/>
					 		</div>
					 </div>
					 <div class="form-group">
					    <label class="col-sm-3 control-label" for="name[${countNum}]">名称</label>
					 	<div class="col-sm-9">	
					 		<input type="text"  name="navigatMenuList[${countNum}].name" value="${navigatMenu.name}" class="form-control input-sm" id="name[${countNum}]" placeholder="输入代码"/>
					 	</div>
					 </div>
				 
					 <div class="form-group">
						    <label class="col-sm-3 control-label" for="code[${countNum}]">代码</label>
					 		 <div class="col-sm-9">
					 			<input type="text"  name="navigatMenuList[${countNum}].code" value="${navigatMenu.code}" class="form-control input-sm" id="code[${countNum}]" placeholder="输入名称"/>
					 		</div>
					 </div>
					 <div class="form-group">
						    <label class="col-sm-3 control-label" for="iconPath[${countNum}]">图标</label>
					 		<div class="col-sm-9">
					 			<input type="text" name="navigatMenuList[${countNum}].iconPath" value="${navigatMenu.iconPath}" class="form-control input-sm" id="iconPath[${countNum}]"  placeholder="输入图标路径"/>
					 		</div>
					 </div>
					  <div class="form-group">
						    <label class="col-sm-3 control-label" for="url[${countNum}]">链接</label>
					 		<div class="col-sm-9">
					 			<input type="text" name="navigatMenuList[${countNum}].url" value="${navigatMenu.url}" class="form-control input-sm" id="url[${countNum}]" placeholder="输入导航路径"/>
					 		</div>
					 </div>
					
					 <ul class="list-inline"> 
						<li>
						 <#if navigatMenu.code?? && navigatMenu.parent??>
						 <a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_MODIFY_UI_MAPPING}${navigatMenu.uniqueIdentifier}'><input type="button" value="编 辑" class="btn btn-primary"/></a>
						  </#if>
						</li>
						<li>  
						  <a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_REMOVE_MAPPING}${navigatMenu.uniqueIdentifier}'><input type="button" value="删 除" class="btn btn-primary"/></a>
						</li>
						<li>
						  <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigatMenu/modify/${navigatMenu.uniqueIdentifier}_${currentNavigation.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
					 	</li>
					 </ul>
	 
	 			</div>
			</div>
	         <#assign countNum = countNum + 1 />
	        <@listNavigatMenuTreeItem currentNavigatMenu= navigatMenu/>
	       	<#if navigatMenu.parent.code=currentNavigatMenu.code && !currentNavigatMenu.parent?exists>
	       		<div class="clearfix"></div>
	       		<hr/>
	       	</#if>
	       	<#assign depth = depth - 1 />
    	</#list>  
    	
	        
	    </#if>  
	  </#if> 	
</#macro> 



<#macro listNavigatMenuTree currentNavigatMenu>
<@form.form class="form-horizontal" commandName="navigatMenuList" role="form" action="/admin/menu/navigatMenu/navigatMenuList/${currentNavigation.uniqueIdentifier}/modify">
	<ul class="list-inline"> 
	<li>
		<input type="submit" value="保存列表" class="btn btn-primary"/>
	</li>
	<li>
		<a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigatMenu/navigatMenuAddUI"/>'>
		<input type="button" value="添加导航" class="btn btn-primary"/>
		</a>
	</li>
	<li>
		<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
	<li>
	</ul>
  
  	<ul class="list-inline"><li>以下是菜单块，以主菜单为一组作水平分隔</li></ul>
  	<#assign countNum = 0 /> 
  	<@listNavigatMenuTreeItem currentNavigatMenu=currentNavigatMenu/>
  </@form.form>
</#macro> 


