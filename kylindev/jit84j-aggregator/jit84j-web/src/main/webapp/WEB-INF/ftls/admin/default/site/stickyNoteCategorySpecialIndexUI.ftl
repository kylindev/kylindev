<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_stickyNoteCategorySpecial.ftl">
<#include "definefunction_navigation.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>随手贴帖子Index-随手帖管理</title>
</head>

<body>
<div class="container body_id" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@horizontalNavigationUI currentNavigation=currentNavigation isAjax='false'/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="stickyNoteCategorySpecial_content_div">
			这里做一些统计工作
			如：各分类下的帖子数目，排行，最新发布帖子数，昨日帖子数
			<#--<@specialAreaEditUI specialArea=specialArea page=page/>-->
		</div>
	</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>