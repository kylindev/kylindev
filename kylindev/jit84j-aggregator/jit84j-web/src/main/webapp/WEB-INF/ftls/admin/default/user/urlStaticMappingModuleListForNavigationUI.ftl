<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<#include "definefunction_urlMapping.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>URL模块</title>

</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<script src="/assets/common/js/jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="/assets/common/js/jquery/jquery.cookie.js" type="text/javascript"></script>
<div class="row">
	<div class="col-md-4">
		<div class="panel panel-warning">
			 <div class="panel-heading ">URL模块</div>
			 <div id="module_div"> 
			 <@urlStaticMappingModuleForNavigation page=page/>
			 </div>
		</div>
	</div>
	<div class="col-md-8">
			<div class="panel panel-warning" >
				<div class="panel-heading ">URL列表</div>
				<div id="url_list_content_div"> 
			      请选择左侧的模块
			 	</div>
			 </div>
	</div>
</div>

<div class="row">
	<div class="col-md-12">
	</div>
</div>


</div>

</body>

<div id="body_bottom">
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>