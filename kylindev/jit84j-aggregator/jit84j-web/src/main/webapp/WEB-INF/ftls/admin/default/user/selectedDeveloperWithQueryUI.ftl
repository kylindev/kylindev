<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_developer.ftl">
<#include "definefunction_page.ftl">


<div class="panel panel-info">
	<div class="panel-heading ">选择开发者</div>
	<form id="condition_form_query" action="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING}">
			<input type="text" data="data" value="<#if developer?? && developer.user??>${developer.user.username}</#if>" id="condition_username" name="user.username" placeholder="输入用户名"/>
		 	<input type="text" data="data" value="" id="condition_email" name="user.email" placeholder="输入email"/>
		 	<input type="text" data="data" value="" id="condition_userId" name="userId.userId" placeholder="输入ID"/>
		 	<input type="button" value="查询" id="condition_query"  onclick="formSubmitAjax(1)"/>
		<#if page??>
			<#assign developerList=page.content>
				<#if developerList??>
					<ul class="list-inline">
					<#list developerList as developer>
						<li>
						<label>
							<input type="radio" name="id" value="${developer.uniqueIdentifier}" 
							<#if currentDeveloper?? && currentDeveloper.id == developer.id> checked</#if>
							/> 
							    ${developer.user.username}
								[ ${developer.userId.userId} ]
								${developer.user.email}
						</label>
						</li>
					</#list>
					</ul>
					<@pageBarAjax url='${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_QUERY_AJAX_MAPPING}' page=page formId='condition_form_query' backDataContainerId='content'/>
				</#if>
		</#if>
	</form>
</div>

	
