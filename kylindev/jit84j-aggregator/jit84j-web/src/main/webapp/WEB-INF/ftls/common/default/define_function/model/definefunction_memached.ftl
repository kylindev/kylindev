<#macro listMemachedModel memachedList>
<p class="text-success">缓存对象列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>Key</th>
      <th>对象名称</th>
      <th>默认过期时间(s)</th>
      <th>过期时间(s)</th>
      <th>选择<input type="checkbox" id="memachedModel_checkbox_selectAllCheckbox1" value="all" /></th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "memachedList" />  
  
  <tbody>
  <#if (memachedList??)>

	<#list memachedList as memachedModel>  
	 <tr>
	 <td>
	 ${memachedModel_index}
	 </td>
	 <td>${memachedModel.key}
	 <input type="hidden" name="key" value="${memachedModel.key}" />
	 </td>
	 <td>
	 ${memachedModel.name}
	 </td>
	 <td>
	 <input type="text" name="expiresTime" readonly id="memachedModel_expiresTime_${memachedModel.key}" value="${memachedModel.targetMemached.expiresTime()}" class="form-control"  placeholder="初始过期时间"/>
	 </td>
	 <td>
	 <input type="text" name="expiresTime" id="memachedModel_expiresTime_new_${memachedModel.key}" value="${memachedModel.expiresTime}" class="form-control input-sm"  placeholder="过期时间"/>
	 </td>
	 <td>
	 <input type="checkbox" name="memachedModel_checkbox" id="memachedModel_checkbox_${memachedModel.key}" value="${memachedModel.key}" class="form-control input-sm"/>
	 </td>
	 <td >
	 <ul class="list-inline">
	 <button onClick="setExpSingle('${KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING}','${memachedModel.key}','memachedModel_expiresTime_new_${memachedModel.key}')"  class="btn btn-primary btn-xs">设置</button>
	 <button onClick="setExpSingle('${KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_SET_EXP_MAPPING}','${memachedModel.key}','memachedModel_expiresTime_${memachedModel.key}')"  class="btn btn-primary btn-xs">恢复默认</button>
	 <button onClick="oprationMemcachedSingle('${KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_MAPPING}','${memachedModel.key}')"  class="btn btn-primary btn-xs">刷新缓存</button>
	 <button onClick="oprationMemcachedSingle('${KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_DELETE_MAPPING}','${memachedModel.key}')"  class="btn btn-primary btn-xs">移除</button>
	 </ul>
	 </td>
	 </tr>
	</#list>
	<tr>
		<td colspan="7">
		<button class="btn btn-primary" onClick="oprationMemcached('${KylinboyDeveloperConstant.KYLINBOY_MEMACHED_MODEL_REFRESH_ALL_MAPPING}')">批量刷新</button>
		</td>
	</tr>
  </#if>
   </tbody>
  </table>
 <script type="text/javascript">
			function selectAll(){
				if(this.checked){ 
					$("input[id ^='memachedModel_checkbox_']").each(function(){this.checked=true;}); 
				}else{
					$("input[id ^='memachedModel_checkbox_']").each(function(){this.checked=false;}); 
				}
			}
			
			$("#memachedModel_checkbox_selectAllCheckbox1").click(selectAll);
			
			function setExpSingle(url,key,expId){
				if(''!= key){
					var _expId = "#"+expId;
				    var expiresTime = $(_expId).val();
					var request={"key":key,"expiresTime":expiresTime};
					submitOprationToMemcached(url, request);
				}
			}
			
			function oprationMemcachedSingle(url,key){
				if(''!= key){
					var request={"key":key};
					submitOprationToMemcached(url, request);
				}
			}
			
			function oprationMemcached(url){
				prepareOprationMemcached(url);
			}
			
			function prepareOprationMemcached(url){
				var requestData="";
				if($("input[id ^='memachedModel_checkbox_']:checked").length>0){
					$("input[id ^='memachedModel_checkbox_']:checked").each(function(){
					if(($(this).attr("id")).indexOf("selectAllCheckbox")<0){
						requestData += $(this).val()+",";
					}
					
				}); 
				
				if(requestData.lastIndexOf(",")>0){
						requestData = requestData.trim();
						requestData = requestData.substring(0,requestData.lastIndexOf(","));
					}
				}
				if(""==requestData){
					alert("没有选择任何缓存对象");
					return;
				}
				var request={"keys":requestData};
				submitOprationToMemcached(url, request);
			}
			
			function submitOprationToMemcached(url,request){
				$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
</script>
 </div>
</#macro>




<#--缓存对象 begin-->
<#macro listMemachedModelForModify page>  
<div id="tree_memachedModel">

	<#if memachedList?? && memachedList?size gt 0>  
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${memachedModel.key}">
			 		<label>全选 <input type="checkbox" name="urlMapping_checkbox_selectAll" id="urlMapping_checkbox_selectAllCheckbox1" /></label >
			 		(注:解除关联仅限于该菜单尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}')">加入所选至菜单</button> <button class="btn btn-warning" onClick="bindToPermission('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}')">从菜单移除所选</button>
			 </li> 
		 </ul>
		<ol id="treeData_memachedModel" class="list-unstyled">
		 	<#assign depth = 1 />  
		 	<#list memachedList as memachedModel> 
		 	<li class="text-info" style="text-inline:" id="memachedModel_id${memachedModel.key}">
		 		<dl>
				  <dt><input type="checkbox" value="${memachedModel.key}" id="urlMapping_checkbox_${memachedModel_index}" name="urlMappingIdKey"/>${memachedModel.name} - ${memachedModel.key} </dt>
				  <dd>${memachedModel.urlFullPath}</dd>
				  <dd><#if memachedModel.urlMapping?? && memachedModel.urlMapping.navigation??> <button class="btn btn-warning btn-xs" onClick="bindToNavigationSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}','${memachedModel.key}')">从菜单移除</button> <#else> <button class="btn btn-primary btn-xs" onClick="bindToNavigationSingle('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}','${memachedModel.key}')">加入菜单</button></#if> </dd>
				</dl>
		 	</li>
		 	</#list> 
		 	
		</ol>
		<ul class="list-unstyled">
			<li class="text-info" id="urlStaticMapping_id${urlStaticMapping.idKey}">
			 		<label >全选 <input type="checkbox" name="urlMapping_checkbox_selectAll2" id="urlMapping_checkbox_selectAllCheckbox2" /></label >
			 		(注:解除关联仅限于该菜单尚未被使用)
			 		<button class="btn btn-primary" onClick="bindToNavigation('${KylinboyDeveloperConstant.KYLINBOY_URL_BIND_TO_NAVIGATION_MAPPING}')">加入所选至菜单</button> <button class="btn btn-warning" onClick="bindToNavigation('${KylinboyDeveloperConstant.KYLINBOY_URL_UNBIND_FROM_NAVIGATION_MAPPING}')">从菜单移除所选</button>
			 </li>
		 </ul>
		 
		<script type="text/javascript">
			function selectAll(){
				if(this.checked){ 
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=true;}); 
				}else{
					$("input[id ^='urlMapping_checkbox_']").each(function(){this.checked=false;}); 
				}
			}
			$("#urlMapping_checkbox_selectAllCheckbox1").click(selectAll);
			$("#urlMapping_checkbox_selectAllCheckbox2").click(selectAll);
			
			
			function bindToNavigationSingle(url,idKey){
				if(''!= idKey){
					var request={"idKeys":idKey};
					submitBindToNavigation(url, request);
				}
			}
			
			
			function bindToNavigation(url){
				prepareBindToNavigation(url);
			}
			
			function prepareBindToNavigation(url){
				var requestData="";
				if($("input[id ^='urlMapping_checkbox_']:checked").length>0){
					$("input[id ^='urlMapping_checkbox_']:checked").each(function(){
					if(($(this).attr("id")).indexOf("selectAllCheckbox")<0){
						requestData += $(this).val()+",";
					}
					
				}); 
				
				if(requestData.lastIndexOf(",")>0){
						requestData = requestData.trim();
						requestData = requestData.substring(0,requestData.lastIndexOf(","));
					}
				}
				if(""==requestData){
					alert("没有选择任何权限");
					return;
				}
				var request={"idKeys":requestData};
				submitBindToNavigation(url, request);
			}
			
			function submitBindToNavigation(url,request){
				$.ajax({
					url: url,
					data: request,
					type: "POST",
					dataType : "json",
					contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
					success: function(data) {
								alert(data.msg);
								
								formSubmitAjax('${page.number +1}');
								
					},
					error: function() {
						alert( "Sorry, there was a problem!" );
					},
					complete: function() {
						//alert('complete');
					}
				}); 
			}
		</script>
	<#else>
	   木有数据。
	</#if>

</div>
</#macro>
<#--缓存对象 end-->
