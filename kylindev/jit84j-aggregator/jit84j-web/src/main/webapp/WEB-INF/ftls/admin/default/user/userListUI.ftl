<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_user.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户列表</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>
<div class="container" id="top_menu">
	<div class="row">
	<@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
	</div>
</div>

<body>
<div class="container body_id" id="body_content">

	<div class="row">
		<div class="col-md-12">
		<p class="text-success">用户列表:</p>
		<@listUser page=page />
		</div>
	
	</div>
</div>
<script type="text/javascript">
</script>
</body>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>