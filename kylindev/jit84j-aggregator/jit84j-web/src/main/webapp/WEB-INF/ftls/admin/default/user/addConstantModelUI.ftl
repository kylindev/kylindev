<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<form method="post">
	<table class="dv-table" style="width:100%;border:1px solid #ccc;padding:5px;margin-top:5px;">
		<tr>
			<td>标题</td>
			<td><input name="title" class="easyui-validatebox" required="true"></input></td>
			<td>Last Name</td>
			<td><input name="clazzName" class="easyui-validatebox" required="true"></input></td>
		</tr>
		<tr>
			<td colspan="2">描述</td>
			<td colspan="2"><input type="textarea" name="description"></input></td>
		</tr>
		<tr>
			<td>开发者姓名</td>
			<td><input name="developer.user.username" class="easyui-validatebox" required="true"></input></td>
			<td>开发者Id</td>
			<td><input name="developer.userId.userId" class="easyui-validatebox" required="true"></input></td>
		</tr>
	</table>
	<div style="padding:5px 0;text-align:right;padding-right:30px">
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="saveItem(${index})">Save</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-cancel" plain="true" onclick="cancelItem(${index})">Cancel</a>
	</div>
</form>
