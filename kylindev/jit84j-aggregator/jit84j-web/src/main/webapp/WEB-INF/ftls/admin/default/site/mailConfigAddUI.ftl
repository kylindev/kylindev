<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_mailConfig.ftl">
<#include "definefunction_page.ftl">
<div class="row">
	<div class="col-md-4">
		${msg}
		<@mailConfigEditUI mailConfig=mailConfig/>
	</div>
	<div class="col-md-8">
		<@listEmailConfigUI  page=page editNavigation=editNavigation/>
	</div>
</div>
