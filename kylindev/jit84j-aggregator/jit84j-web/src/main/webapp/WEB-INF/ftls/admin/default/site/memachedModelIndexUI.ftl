<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_memached.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>缓存管理</title>
</head>
<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <#--<@commonBanner/>-->
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>


<body>
<div class="container body_content" id="body_content">
		<div class="row">
			<div class="col-md-12">
				<p class="text-success">缓存对象操作: <span class="text-danger"><strong>【注意： 操作缓存要由专业人员来操作，必须谨慎，影响系统性能及稳定性！】<、strong></span></p> 
				
				<div class="table-responsive">
				<@currentNavigationButton currentNavigation=currentNavigation/>
				</div>
				<#-- -->
			</div>
		</div>
	

		
</div>
</body>
<div class="container" id="body_footer">
	<@commonFooter/> 
</div>
</html>