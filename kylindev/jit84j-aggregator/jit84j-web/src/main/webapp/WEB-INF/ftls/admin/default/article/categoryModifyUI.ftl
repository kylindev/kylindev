<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>修改分类</title>
</head>
<body>
<div class="container">
<div class="row">

  <div class="col-md-8">
  <p class="text-success">修改分类</p>
  <@form.form commandName="category" role="form" action="/admin/article/category/categoryModify">
  <@spring.bind "category.uniqueIdentifier" /> 
  <@form.hidden path="id"/>
      <@spring.bind "category.name" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="name">类别名称: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="name" class="form-control" id="name" placeholder="输入类别名称" required="true"/>
	  </div>
	  <@spring.bind "category.code" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">类别代码: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="code" class="form-control" id="code" placeholder="输入类别代码，使用字母" required="true"/>
	  </div>
	  
	  <@spring.bind "category.sequence" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">顺序: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="sequence" class="form-control" id="sequence" placeholder="输入类别顺序，使用数字1,2,10,12" required="true"/>
	  </div>
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="control-label" for="parent.id"><input type="button" value="选择父类" id="parent_id_button" class="btn btn-primary"/>: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div>
			<#assign depth = 1 />  
		  	<@selectParentCategoryRadio children=categoryList currentCategory=category />
		  	</div>
		  </div>
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="control-label" for="parent.id"><input type="button" value="分配子类" id="parent_id_button" class="btn btn-primary"/>: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div>
			<#assign depth = 1 />  
			<@asignChildrenCategoryCheckbox children=categoryList currentCategory=category/>
			</div> 
		  </div>
	  
	   <div class="col-md-12">
       	<input type="submit" value="保存设置" class="btn btn-primary"/>
       	<input type="reset" value="重置" class="btn btn-primary"/>
       </div>
       
 
	</@form.form>
  
  </div>
</div>
</div>
<script type="text/javascript">
$("#parent_id_button").click(function(){
	
	alert("选择父类");
});
</script>
</body>
</html>