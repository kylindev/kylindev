<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_gallery.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加影集</title>
</head>
<body>
<div class="container">
<div class="row">

  <div class="col-md-8">
  <p class="text-success">添加影集</p>
  <@form.form commandName="gallery" role="form" action="/admin/gallery/galleryAdd">
  <@spring.bind "gallery.uniqueIdentifier" /> 
  <@form.hidden path="id"/>
      <@spring.bind "gallery.theme" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="name">影集名称: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="theme" class="form-control" id="theme" placeholder="输入影集名称" required="true"/>
	  </div>
	  <@spring.bind "gallery.description" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">影集描述: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="description" class="form-control" id="description" placeholder="输入影集描述" />
	  </div>
	  
	  <@spring.bind "gallery.author" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">摄影人: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="author" class="form-control" id="author" placeholder="输入摄影人" />
	  </div>
	  
	  <@spring.bind "gallery.publishTime" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">影集时间: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="publishTime" class="form-control" id="publishTime" placeholder="输入摄影时间" />
	  </div>
	  
	  <@spring.bind "gallery.openBeginTime" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">开放时间: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="openBeginTime" class="form-control" id="openBeginTime" placeholder="输入开放时间" />
	  </div>
	  
	  <@spring.bind "gallery.musicPath" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">背景音乐路径: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="musicPath" class="form-control" id="musicPath" placeholder="背景音乐路径" />
	  </div>
	  
	  <@spring.bind "gallery.imagePath" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">封面路径: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="imagePath" class="form-control" id="imagePath" placeholder="输入相片路径" />
	 	<input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','imagePath')"/><br/>
	    <div id="imagePath_img">
	    	<img src="${gallery.imagePath}" title="相片" alt="独家记忆" class="img-rounded">
	    </div>
	  </div>
	  
	  
	  <@spring.bind "gallery.sequence" />  
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="code">顺序: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="sequence" class="form-control" id="sequence" placeholder="输入相片排序，使用数字1,2,3..." required="true"/>
	  </div>
	  
		  
	  
	   <div class="col-md-12">
       	<input type="submit" value="保存设置" class="btn btn-primary"/>
       </div>
       
 
	</@form.form>
  
  </div>
</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/ckeditor/adapters/jquery.js"></script>
<div id="filepathTextFieldId_hidden"></div>
<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
<script src="/assets/common/js/jquery/jquery.json-2.4.min.js"></script>
</body>
</html>