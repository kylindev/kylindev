<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">
<#include "definefunction_travel.ftl">

<div class="container" id="body_content_opration">
	<ul class="list-inline">
		   		<li><a href="#"><span class="label label-success">发布即时分享</span></a></li>
		   		<li class="active"><a href="#"><span class="label label-primary">发布行程计划</span> </a></li>
		   		<li><a href="#"> <span class="label label-primary">发布活动计划</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布活动回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程攻略分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布装备分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布摄影攻略分享帖</span></a></li>
		   	</ul>
</div>
<div class="container" id="body_content_navigation">
		<ul class="list-unstyled">
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 今日宣言</h4></a></li>
			<li><a href="#"><h4 class="text-center text-success"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我要发布</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 即时分享</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的关注</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 个人资料</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的行程</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的活动</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的队伍</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的装备</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的回忆</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的驴友</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 摄影攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 首页定制</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 消息留言</h4></a></li>
		</ul>
</div>
<div class="container" id="body_content_detail">
<hr/>
	<@editInstantShare instantShare=instantShare/>
	 <p>您所在的城市是：<script>document.write(myprovince+' '+mycity);</script></p>
</div>

