<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>申请成为开发者</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="developer" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_DEVELOPER_ADD_MAPPING}/developer_list">
	<div class="col-md-8">
	<div class="panel panel-warning" >
		<div class="panel-heading ">请填写以下必填信息</div>
		 
	      <@spring.bind "developer.seperator" />  
	      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="seperator">Seperator: 
				<#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
			</label>
			<div class="col-sm-9">
		    <@form.input path="seperator" class="form-control" id="seperator" placeholder="输入分隔符" required="true"/>
		  	</div>
		  </div>
		  
		  <@spring.bind "developer.userIdSeperator" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="email">userId seperator:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    	<input type="text" name="userIdSeperator" readonly="true" value="${developer.userIdSeperator}" class="form-control" id="userIdSeperator" placeholder="Enter userId seperator" >
		  	</div>
		  </div>
		 
		  <@spring.bind "developer.oprationSeperator" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="col-sm-3 control-label" for="password">oprationSeperator:
			    <#if (spring.status.errorMessages?size>0)>
					  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				</#if>
		    </label>
		    <div class="col-sm-9">
		    <@form.input path="oprationSeperator" class="form-control" id="oprationSeperator" placeholder="输入操作分隔符" required="true"/>
		  	</div>
		  </div>
		  
		  
		  <ul class="list-inline">
				  <li>
			       		<input type="submit" value="成为开发者" class="btn btn-primary"/>
			       </li>
	       </ul>
	  
	  </div>
	
	</div>
  <div class="col-md-4">
		
  </div>
</@form.form>
</div>
</div>

</body>

<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg"/>
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>