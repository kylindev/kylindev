<#macro getMailConfigTypeDropdown mailConfig mailConfigTypeList name="type">
<select name="${name}" data="data">
		<#list mailConfigTypeList as mailConfigType>
	  	<option value="${mailConfigType.type}" <#if mailConfig?? && mailConfig.type == mailConfigType.type>selected</#if>>${mailConfigType.name}</option>
		</#list>
</select>
</#macro>

<#--Email配置编辑 begin-->
<#macro mailConfigEditUI mailConfig>
<p class="text-success">Email配置基本信息</p>

  <@form.form id="mailConfig_basic_edit_ui_form_id" commandName="mailConfig" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_MAPPING}">
  <@spring.bind "mailConfig.uniqueIdentifier" /> 
  <@form.hidden data="data" path="id"/>

  <div class="form-group ">
    <label class="control-label" for="host">服务器路径: 
	</label>
    <@form.input data="data" path="host" class="form-control" id="host" placeholder="输入服务器路径" required="true"/>
  </div>

  <div class="form-group">
    <label class="control-label" for="username">用户名:
    </label>
    <@form.input data="data" path="username" class="form-control" id="username" placeholder="输入用户名" required="true"/>
  </div>

  <div class="form-group">
    <label class="control-label" for="password">密码:
    </label>
    <@form.input data="data" path="password" class="form-control" id="password" placeholder="输入密码" required="true"/>

  </div>

  <div class="form-group">

    <label for="port">端口：
    </label>

    <@form.input data="data" path="port" class="form-control" id="port" placeholder="输入端口" required="true"/>

  </div>


  <div class="form-group">

    <label class="control-label" for="protocol">协议:
    </label>

    <@form.input data="data" path="protocol" class="form-control" id="protocol" placeholder="协议" required="true"/>

  </div>

  <div class="form-group">

    <label class="control-label" for="sender">默认发送者:
    </label>

    <@form.input data="data" path="sender" class="form-control" id="sender" placeholder="输入默认发送者" required="true"/>

  </div>

  <div class="form-group">
    <label class="control-label" for="useClientFrom">使用客户端发送者:
    </label>
	<input data="data" type="checkbox" name="useClientFrom" class="form-control" id="useClientFrom" value="${mailConfig.useClientFrom}"/>
  </div>

  <div class="form-group">
    <label class="control-label" for="type">类型:
    </label>
    <#assign mailConfigTypeList=StaticDataDefineManager.getData(StaticDataDefineConstant.MAIL_CONFIG_TYPE_DROPDOWN)/>
    <@getMailConfigTypeDropdown mailConfig=mailConfig mailConfigTypeList=mailConfigTypeList name="type"/>
  </div>

</@form.form>
  <div class="col-md-12">
  <ul class="list-inline">
   	<li><input type="button" onClick="submit('#mailConfig_basic_edit_ui_form_id','#mailConfig_content_div')" value="保存分类" class="btn btn-primary"/></li>
  	<li>
	   <input type="button" class="btn btn-primary" onClick="cleanForm('#mailConfig_basic_edit_ui_form_id')" value='清空'/>
	</li>
  </ul>
  </div>
  
 <script type="text/javascript">
function cleanForm(formId){
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			if($(this).attr("type")=="checkbox"){
				$(this).attr("checked",false);
			}else{
				$(this).attr("value","");
			}
		});
	}
}
function submit(formId,backDataContainerId){
	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			
			if($(this).attr("type")=="checkbox"){
				if($(this).attr('checked')==undefined){
					request[$(this).attr("name")]='false';
				}else{
					request[$(this).attr("name")]='true';
				}
			}else{
				request[$(this).attr("name")]=$(this).attr("value");
			}
		});
	}
	var _select = formId+" select[data$='data']";
	if($(_select).length>0){
		$(_select).each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	
	var action = $(formId).attr("action");
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}

function removeItem(id,action,backDataContainerId){
	if(id==''){
		alert("没有id");
		return;
	}
	var request = {'id':id};
	
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
			
 </script> 
</#macro> 
<#--Email配置编辑 end-->

<#-- 文件系统分类列表 -->
<#macro listEmailConfigUI page editNavigation>

<p class="text-success">邮件配置列表:</p>
  <@form.form commandName="emailConfig" id="emailConfig_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING}">
<table class="table">
  <thead>
    <tr>
      <th width="3%">Id</th>
      <th width="12%">host</th>
      <th width="12%">用户名</th>
      <th width="10%">密码</th>
      <th width="5%">端口</th>
      <th width="5%">协议</th>
      <th width="8%">默认发送者</th>
      <th width="8%">客户端发送者</th>
      <th width="8%">类型</th>
      <th width="8%">操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page.content??)>
  	 <#assign depth = 1 />  
	<@listEmailConfigItem page=page editNavigation=editNavigation/>
  </#if>
	<#--
	<tr>
		<td colspan="10">
			<a class="btn btn-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
				保存列表
			</a>
		</td>
		
	 </tr>
	-->
   </tbody>
 </table>
  </@form.form>
 <script type="text/javascript">
 function removeItem(id,action,backDataContainerId){
	if(id==''){
		alert("没有id");
		return;
	}
	var request = {'id':id};
	
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
 </script>
</#macro>

<#macro listEmailConfigItem page editNavigation>
	<#if page?? && page.content??>
	<#assign emailConfigList = page.content />
	<#assign mailConfigTypeList=StaticDataDefineManager.getData(StaticDataDefineConstant.MAIL_CONFIG_TYPE_DROPDOWN)/>
		<#list emailConfigList as emailConfig>  
			 <#assign depth = depth + 1 />
			 <tr>
			 <td>
			 ${emailConfig.uniqueIdentifier}
			 </td>
			 <td>
			 ${emailConfig.host}
			 </td>
			 <td>
			 ${emailConfig.username}
			 </td>
			 <td>
			 ${emailConfig.password}
			 </td>
			  <td>
			  ${emailConfig.port}
			 </td>
			 <td>
			 ${emailConfig.protocol}
			 </td>
			 <td>
			 ${emailConfig.sender}
			 </td>
			 <td>
			 ${emailConfig.useClientFrom}
			 <input data="data" type="checkbox" name="useClientFrom" class="form-control" id="useClientFrom" <#if mailConfig.useClientFrom> checked </#if> value="${mailConfig.useClientFrom}"/>
			 </td>
			 <td>
			     <@getMailConfigTypeDropdown mailConfig=mailConfig mailConfigTypeList=mailConfigTypeList name="type"/>
			 
			 </td>
			 <td>
			 <ul class="list-inline"> 
				<li>
			 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${emailConfig.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_MAILCONFIG_EDIT_UI_MAPPING}')">编辑</a>
			  	</li>
			    <li>
			    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${emailConfig.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING}','#emailConfig_content_div')">删除</a>
			
			    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<#--
		<tr>
		<td colspan="10">
		<@pageBarAjax page=page url="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING}" formId='fileSystemGallery_list_form' backDataContainerId="emailConfig_content_div"/>
		</td>
		</tr>
		-->
	</#if>
</#macro>



