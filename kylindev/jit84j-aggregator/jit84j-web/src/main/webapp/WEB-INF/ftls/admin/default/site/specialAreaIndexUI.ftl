<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_specialArea.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>特殊区管理-随手帖管理</title>
</head>

<body>
<div class="container body_id" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@specialAreaIndexNavigationUI currentNavigation=currentNavigation/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="specialArea_content_div">
			<@specialAreaEditUI specialArea=specialArea page=page/>
		</div>
	</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>