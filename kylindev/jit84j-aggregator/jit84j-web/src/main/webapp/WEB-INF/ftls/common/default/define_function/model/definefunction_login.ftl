<#macro loginAndLogout>  
  <span id="_welcome">
    <ul class="list-inline pull-right">
        <li>
        	Welcome <@shiro.principal/>  
        </li>
        <li>
        	<a class="text-danger" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_LOGOUT_MAPPING}"/>'>logout</a>
        </li>
	</ul>
  </span>
  <span id="_login">
        <ul class="list-inline">
			  <li>
			  	<a class="text-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_UI_MAPPING}"/>'>点击登录</a>
			  </li>
			  <li>
			    <a class="text-danger" href='${KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_UI_MAPPING}'>没有帐号，<strong>点此</strong>新注册一个</a>
			  </li>
		</ul>
  </span>
<script type='text/javascript'>
var _principal='<@shiro.principal/>';
$(document).ready(function(){
	getLogin();
});
function getLogin(){
	if(_principal!=''){
		$("#_welcome").show();
		$("#_login").hide();
	}else{
		$("#_welcome").hide();
		$("#_login").show();
	}
}

</script>
     	
</#macro> 

