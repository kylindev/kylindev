<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_user.ftl">
<#include "definefunction_permission.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>权限分类管理</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>
<div class="container" id="top_menu">
	<div class="row">
	<@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
	</div>
</div>
<#-- -->
<body>
<div class="container body_id" id="body_content">
<script src="/assets/common/js/jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="/assets/common/js/jquery/jquery.cookie.js" type="text/javascript"></script>

<link href="/assets/common/js/jquery/dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" id="skinSheet">
<script src="/assets/common/js/jquery/dynatree/jquery.dynatree.js" type="text/javascript"></script>
<script src="/assets/common/js/jquery/jquery.hashchange.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(function(){
        $("#tree").dynatree({
        	 title: "permission_category_index", 
        	minExpandLevel: 1,
        	checkbox: false, // Show checkboxes.
        	classNames: {checkbox: "dynatree-radio"},
  			selectMode: 1, // 1:single, 2:multi, 3:multi-hier
            onActivate: function(node) {
                var _current_id = $(node.data.title).filter(":input").val();
                var _decription = $(node.data.title).filter("label").attr("title").trim();
                var _name = $(node.data.title).find("span:first").html().trim();
                var _code = $(node.data.title).find("span:last").html().trim();
                 var _parent_id = $(node.parent.data.title).filter(":input").val();
                 //permissionCategory_name
                 $("#permissionCategory_name").val(_name);
                 $("#permissionCategory_code").val(_code);
                 $("#permissionCategory_description").val(_decription);
                 $("#permissionCategory_id").val(_current_id);
                 if(_parent_id != null && _parent_id!=""){
                 
                 	$("#select_parent_category_"+_parent_id).val(_parent_id);
                 	$("#select_parent_category_"+_parent_id).attr("checked",true);
                 }else{
                 	$(":radio:checked").val("");
                 	$(":radio:checked").attr("checked",false);
                 }
            }
        });
        
        $("#tree_permission").dynatree({
        	 title: "permission_category_asign", 
        	 minExpandLevel: 1,
        	 checkbox: false, // Show checkboxes.
        	 classNames: {checkbox: "dynatree-radio"},
  			 selectMode: 1, // 1:single, 2:multi, 3:multi-hier
  			 autoFocus: true,
             onActivate: function(node) {
	             if($("#permission_category_id").length>0){
	              	var _current_id = $(node.data.title).filter(":input").val();
	             	$("#permission_category_id").val(_current_id);
	             }
            }
        });
        
        <#--触发选择的tab begin-->
        function triggerSelectedTab(){
			var _current_url = window.location.href;
			if(_current_url !=null && _current_url!="" ){
				var _select_tab = _current_url.split("#");
				if(_select_tab!= null && _select_tab.length >1){
					var _selected_tab = _select_tab[1];
					$(".tabbable a[href='#"+_selected_tab+"']").trigger("click");
				}else{
					history.pushState(null,null,window.location.href + "#panel-first");
				}
				
			}
			
		}
		triggerSelectedTab();
        <#--触发选择的tab end-->
        
    });
    function changeMorkToUrl(mork){
     		var _current_url = window.location.href;
			if(_current_url !=null && _current_url!="" ){
				var _select_tab = _current_url.split("#");
				if(_select_tab!= null && _select_tab.length >1){
					var _selected_tab = _select_tab[1];
					history.pushState(null,null,_select_tab[0] + mork);
				}
			}
    }

</script>

<p class="text-success">权限分类管理</p>

<div class="container-fluid">
	<div class="row-fluid">
		<div class="span12">
		
			<div class="tabbable" id="tabs-995050">
				<ul class="nav nav-tabs">
					<li onclick="changeMorkToUrl('#panel-first')" >
						<a href="#panel-first" data-toggle="tab" >权限类别列表</a>
					</li>
					<li onclick="changeMorkToUrl('#panel-second')">
						<a href="#panel-second" data-toggle="tab" >将权限归类</a>
					</li>
					<li onclick="changeMorkToUrl('#panel-third')">
						<a href="#panel-third" data-toggle="tab" >权限移动</a>
					</li>
				</ul>
				<div class="tab-content">
					<div class="tab-pane active" id="panel-first">
						<!--SPAN8-->
						<div class="panel panel-warning" >
							<div class="panel-heading ">权限分类管理树</div>
							<div class="panel-body"> 
								<@permissionCategoryListTree page=page  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING/>
							</div>	
						</div>				
									
						<!--SAPN8 END-->
						
						<!--SAPN4 BEGIN-->
						<div id="add_permission_category_div">
  						<#----> <@addPermissionCategoryForm page=page currentCategory=currentCategory/> 
						</div>
						<!--SAPN4 END-->
								
					</div>
					
					<div class="tab-pane " id="panel-second">
						<p>
							第二部分内容.
						</p>
						
						<div class="panel panel-warning" >
							<div class="panel-heading ">权限分类管理树【第一步：选择分类】</div>
							<div class="panel-body"> 
								
								<@permissionCategoryListIncludePermissionTree page=page  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING/>
							</div>	
						</div>
						<div class="panel panel-warning" id="permission_list_div">
							<div class="panel-heading ">所有权限列表【第二步：选择权限】</div>
							<div class="panel-body"> 
  								<@listPermissionViewTree page=permissionPage />
  							</div>	
						</div>
					</div>
					<div class="tab-pane " id="panel-third">
						<p>
							第三部分内容.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



</div>
<script type="text/javascript">
</script>
</body>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>