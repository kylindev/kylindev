<#macro manifestoListUI page>
	<#if page??>
		<#assign manifestoList=page.content/>
			<#if manifestoList??>
				<ul class="list-unstyled">
				<#list manifestoList as manifesto>  
					<li ><small><span class="badge"><a href="javascript:void(0)" id="praise_a_${manifesto.id}" onClick="praise('${manifesto.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING}','#praise_${manifesto.id}','#praise_a_${manifesto.id}')" class="text-danger" title="赞一个">赞</a><span id="praise_${manifesto.id}">${manifesto.praiseCount}</span></span></small> <a class="text-info" href="#" title="查看【${manifesto.nickName}】的宣言">${manifesto.decription}</a> <span class="text-danger">by</span> <a class="text-primary" href="" title="了解【${manifesto.nickName}】"><span class="text-primary">${manifesto.nickName}</span></a>  </li>
				</#list>
				</ul>
			</#if>	
	</#if>
</#macro>

<#macro manifestoListWithImageUI page url>
	<#if page??>
		<#assign manifestoList=page.content/>
			<#if manifestoList??>
				<ul class="media-list" id="manifesto_list_ul">
					<@manifestoListWithImageUIItem page=page/>
				</ul>
				
				<@nextPage page=page containerId='#manifesto_list_ul' url=url pageSize=20/>
				
				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				        <h4 class="modal-title" id="myModalLabel">评论(200字以内)</h4>
				      </div>
				      <div class="modal-body">
					      <form id="form_id_comment" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD_MAPPING}" method="post">
					      	<input type="hidden" value="" name="manifesto.id" id="myModalManifestoId" data="data"/>
					      	<textarea name="comment" class="form-control" rows="5" placeholder="输入评论" data="data"> </textarea>
					      </form>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
				        <button type="button" class="btn btn-primary" id="button_id_save_comment" onClick="saveComment('#form_id_comment','#span_comment_id')">保存评论</button>
				      </div>
				    </div><!-- /.modal-content -->
				  </div><!-- /.modal-dialog -->
				</div><!-- /.modal -->
				
			</#if>	
	</#if>
<script type="text/javascript">


function addComment(manifestoId){
	if(null==manifestoId || ''==manifestoId){
		return;
	}
	$('#myModalManifestoId').val(manifestoId);
	//span_comment_id_6
	var backDataContainerId='#span_comment_id_'+ manifestoId;
	var clickFunction = "saveComment('#form_id_comment','"+backDataContainerId+"','"+manifestoId+"')";
	$('#button_id_save_comment').attr("onClick",clickFunction);
	$('#myModal').modal('toggle');
}
function saveComment(formId,backDataContainerId,id){
	//alert(formId + ":"+backDataContainerId);
	submit(formId,backDataContainerId);
	var totalCommentSpanId="#span_comment_total_"+id;
	var totalValueItemId = "#commentTotalNumber_"+id;
	//alert(totalValueItemId);
	if($(totalValueItemId).length>0){
		var total = $(totalValueItemId).val();
		$(totalCommentSpanId).html(total);
	}
	$('#myModal').modal('hide');
}
</script>
</#macro>


<#macro manifestoListWithImageUIItem page>
	<#if page??>
			<#assign manifestoList=page.content/>
			<#if manifestoList??>
				<#list manifestoList as manifesto>  
				  <li class="media">
				    <a class="pull-left" href="#">
				      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
				    </a>
				    <div class="media-body">
				      <h5 class="media-heading">
					     <small>
					     	<span class="badge">
					     	<a href="javascript:void(0)" id="praise_a_${manifesto.id}" onClick="praise('${manifesto.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING}','#praise_${manifesto.id}','#praise_a_${manifesto.id}')" class="text-danger" title="赞一个">赞</a>
					     	<span id="praise_${manifesto.id}">${manifesto.praiseCount}</span>
					     	</span>
					     </small> 
					     <a target="_blank" class="text-info" href="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING}\${manifesto.id}" title="查看【${manifesto.nickName}】的宣言"><abbr title="${manifesto.decription}"><#if manifesto.decription?? && manifesto.decription?length gt 35>${manifesto.decription?substring(0,35)}...<#else>${manifesto.decription}</#if></abbr></a> 
					     	<span class="text-danger">by</span> <a class="text-primary" href="" title="了解【${manifesto.nickName}】">
					     	<span class="text-primary">${manifesto.nickName}</span>
					     </a> 
					     
					     <@timeFormat currentTime=manifesto.publishTimeMillis/>
				      </h5>
				      
				       <h5 class="media-heading">
				       			<dl class="dl-horizontal">
					       			<dt>
								        <small>
									     	<span class="badge">
										     	<a href="javascript:void(0)" id="praise_a_${manifesto.id}" onClick="addComment('${manifesto.id}')" class="text-danger" title="评一下">评</a>
										     	<span id="span_comment_total_${manifesto.id}">${manifesto.commentCount}</span>
									     	</span>
									     </small>
								     </dt>
								     <dd>
								     <span id="span_comment_id_${manifesto.id}">
								     	<ul id="scroll_ul_${manifesto.id}" class="list-unstyled text-left" style="height:20px;overflow:hidden;"> 
										     <#assign commentList=manifesto.commentList/>
										     <#if commentList?? && commentList?size gt 0 >
											     	<#list commentList as comment> 
											     	<li>
													     <a class="text-info" href="#" title="查看【${comment.nickName}】的宣言">${comment.comment}</a> 
													     	<span class="text-danger">commented by</span> <a class="text-primary" href="" title="了解【${comment.nickName}】">
													     	<span class="text-primary">${comment.nickName}</span>
													     </a> 
												    </li>
												    </#list> 
												<script type="text/javascript">
												    setInterval("scroll_news('#span_comment_id_${manifesto.id} li')",5000);
												</script>
											  <#else>
											  	<a href="javascript:void(0)" onClick="addComment('${manifesto.id}')" class="text-danger">我来评一下先</a>
										     </#if>
									    </ul>
								     </span>
								     </dd>
							     </dl>
				       </h5>
				    </div>
				  </li>
			</#list>
		</#if>
	</#if>
</#macro>



<#macro listCommentUIByPage page url containerId>
<#if page??>
	<#assign manifestoCommentList=page.content/>
		
	<#if manifestoCommentList?? && manifestoCommentList?size gt 0 >
		<@listCommentItemUI manifestoCommentList=manifestoCommentList/>
	</#if>
	<@nextPageButtonBar page=page containerId=containerId url=url pageSize=page.size/> 
</#if>
</#macro>

<#macro listCommentUIByManifesto current containerId>
	<#if current??>
			<#assign commentPage=current.commentPage/>
			<#if commentPage??>
				<#assign manifestoCommentList=commentPage.content/>
				
				<#if manifestoCommentList?? && manifestoCommentList?size gt 0 >
					<@listCommentItemUI manifestoCommentList=manifestoCommentList/>
				</#if>
				<#assign url=KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_LIST_UI_MAPPING + "/" + current.id/>
				<@nextPageButtonBar page=commentPage containerId=containerId url=url pageSize=commentPage.size/> 
			</#if>
	</#if>
</#macro>

<#macro listCommentItemUI manifestoCommentList isReply=false>
	<#if !isReply>
	<#assign depth=0/>
	</#if>
	<#if manifestoCommentList?? && manifestoCommentList?size gt 0>
				<#list manifestoCommentList as manifestoComment>
					<div class="row" id="row_span_comment_id_${manifestoComment.id}">
							<div class="col-md-12" id="span_comment_id_${manifestoComment.id}">
								<input type="hidden" value="${depth}" name="depth" id="depth_comment_id_${manifestoComment.id}"/>
								<p <#if !isReply> class="lead" </#if> >
								<#if depth gt 0>
									<#list 1..depth as i>
									<span class="glyphicon glyphicon-eye-open"></span> 
									</#list>
								</#if>
								<span class="text-danger">${manifestoComment.nickName}</span> 
								at <small>${manifestoComment.publishDate}</small> <#if isReply> 【回复】 ${manifestoComment.parent.nickName} : <#else>【说】:</#if> 
								${manifestoComment.comment}
								<a href="javascript:void(0)" id="a_${manifestoComment.id}" onclick="addComment('${manifestoComment.id}','2')">回复</a>
								</p> 
								<#assign replyCommentList=manifestoComment.replyCommentList/>
								<#if replyCommentList??>
									<#assign depth=depth+1/>
									<@listCommentItemUI manifestoCommentList=replyCommentList isReply=true/>
									<#assign depth=depth-1/>
								</#if>
							</div>
					</div>
				</#list>
	</#if>
</#macro>