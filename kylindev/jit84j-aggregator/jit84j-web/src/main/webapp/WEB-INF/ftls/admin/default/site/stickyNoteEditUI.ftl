<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_stickyNote.ftl">
<#include "definefunction_specialArea.ftl">
<#include "definefunction_categorySpecial.ftl">
<#include "definefunction_navigation.ftl">

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>随手贴帖子编辑-随手帖管理</title>
</head>

<body>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@horizontalNavigationUI currentNavigation=currentNavigation isAjax='false'/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<@stickyNoteEditUI stickyNote=stickyNote/>
		</div>
	</div>
</div>
</body>
</html>