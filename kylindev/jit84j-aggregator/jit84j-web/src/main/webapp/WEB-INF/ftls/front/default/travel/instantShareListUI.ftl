<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">
<#include "definefunction_travel.ftl">
<#include "definefunction_userManageAreaItem.ftl">

<div class="container" id="body_content_opration">
	<#-- 我的即时分享列表 -->
	
	<ul class="list-inline" id="second_navigation_container">
		   		<li class="active" id="left_navigation_button_list"><a href="javascript:void(0)" onclick="switchSetting('','button_list','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING}','#instantShare_content_div','second_navigation_container')"><span class="label label-success">列表</span> </a></li>
		   		<li id="left_navigation_button_add"><a href="javascript:void(0)" onclick="switchSetting('','button_add','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT3_UI_MAPPING}','#instantShare_content_div','second_navigation_container')"><span class="label label-primary">新建</span></a></li>
		   		<li id="left_navigation_button_edit"><a href="javascript:void(0)"><span class="label label-primary">编辑</span></a></li>
	</ul>
</div>
<div class="container" id="body_content_navigation">

		<@listUserManageAreaItem currentUserManageAreaItem=currentUserManageAreaItem/>
		<!--
		<ul class="list-unstyled">
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 今日宣言</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我要发布</h4></a></li>
			<li><a href="#"><h4 class="text-center text-success"><span class="glyphicon glyphicon-circle-arrow-right"></span> 即时分享</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的关注</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 个人资料</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的行程</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的活动</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的队伍</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的装备</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的回忆</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的驴友</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 摄影攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 首页定制</h4></a></li>
			<li><a href="#"><h4 class="text-center"><span class="glyphicon glyphicon-circle-arrow-right"></span> 消息留言</h4></a></li>
		</ul>
		-->
</div>
<div class="container" id="body_content_detail">
	<div class="row">
			<div class="col-md-12">
				<div id="instantShare_content_div">
						<@listInstantShare page=page />
				</div>
			</div>
	 </div>
	 <br/>
</div>

<div id="uploadModalLabel2ContentBody">
		<@listInstantShareImageCounter instantShareImageCounterList=instantShareImageCounterList/>
</div>


