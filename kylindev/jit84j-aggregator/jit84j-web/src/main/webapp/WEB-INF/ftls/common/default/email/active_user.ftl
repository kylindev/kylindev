<style  type="text/css">
.panel-heading {
  padding: 10px 15px;
  border-bottom: 1px solid transparent;
  border-top-right-radius: 3px;
  border-top-left-radius: 3px;
}
.panel-group .panel {
  margin-bottom: 0;
  overflow: hidden;
  border-radius: 4px;
}
.panel-info {
  border-color: #bce8f1;
}

.panel-info > .panel-heading {
  color: #3a87ad;
  background-color: #d9edf7;
  border-color: #bce8f1;
}
</style>

<div class="panel panel-info">
		<div class="panel-heading ">友情提示</div>
			感谢您注册${title},这是系统给您发送的激活链接，请尽快激活。
			点击下面链接<a href='${emailUrl.url}' target='_blank'><b><font color='red'>激活用户</font></b></a>，如果无法点击，请复制链接粘贴至浏览器地址栏访问：<br/>
			${emailUrl.url}
</div>
