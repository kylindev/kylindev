<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_user.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>重置密码【系统】谨慎操作</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="user" id="reset_password_form_id" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_RESET_PASSWORD_MAPPING}">
	<div class="col-md-12">
	<div class="panel panel-warning" >
		<div class="panel-heading ">重置密码【系统】谨慎操作</div>
		  ${resetPassword@success!}
		  ${resetPassword@email@empty!}
		  ${resetPassword@passwrod@empty!}
		  ${resetPassword@confirmPassword@empty!}
		  ${resetPassword@passwrod@confirmPassword@conflict!}
		  ${resetPassword@email@invalid!}
		  
		  <div class="form-group ">
		    <label class="col-sm-3 control-label" for="password">New Password:
		    </label>
		   <ul class="list-inline">
				<li>
		    	<@form.password path="password" size="50" class="form-control" id="password" placeholder="输入新密码" required="true"/>
		  		</li>
			</ul>
		  </div>
		  
		  <div class="form-group ">
		    <label class="col-sm-3 control-label" for="confirmPassword">Confirm Password
		    </label>
		    <ul class="list-inline">
				<li>
		    	<@form.password path="confirmPassword" size="50" class="form-control" id="confirmPassword" placeholder="再次输入密码" required="true"/>
		  		</li>
			</ul>
		  </div>
		  
		  
	      <div class="form-group ">
		    <label class="col-sm-3 control-label" for="rolename_1">注册邮箱: 
			</label>
				<ul class="list-inline">
				<li>
		    	<@form.input path="email"size="50" class="form-control" id="email" placeholder="输入注册时的Email" required="true"/>
			   </li>
			   <li>
			      <input type="submit" value="重置密码" class="btn btn-primary"/>
			   </li>
			   <li>
			      <input type="button" value="系统生成随机6位字符重置密码" class="btn btn-primary" onclick="generate('#email','#reset_password_form_id','${KylinboyDeveloperConstant.KYLINBOY_RESET_PASSWORD_AUTO_MAPPING}')"/>
			   </li>
			   </ul>
	       </div>
	  </div>
	</div>
<script type="text/javascript">
function generate(email,formId,action){
	var value = $(email).val();
	if(''!=value){
		var form = $(formId).attr("action",action)
		form.submit();
	}else{
		alert('注册邮箱不能为空！');
	}
}
</script>
</@form.form>
</div>
</div>

</body>

<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg"/>
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>