<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_specialArea.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1771902817/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent" autoPlay="true"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary" href="#">新留言【<span class="text-danger">19</span>】条</a> <a class="text-primary"  href="#">网站首页</a> <@loginAndLogout /></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<#--<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>-->
</div>

<div class="container" id="body_content">
	<br/>
	
	<div class="row">	
		<div class="col-md-2">
			<button class="btn btn-info">我的管理区</button>
		</div>
		<div class="col-md-10" style="padding: 6px">
			<ul class="list-inline">
		   		<li><a href="#"><span class="label label-primary">发布即时分享</span></a></li>
		   		<li class="active"><a href="#"><span class="label label-success">发布行程计划</span> </a></li>
		   		<li><a href="#"> <span class="label label-primary">发布活动计划</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布活动回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程攻略分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布装备分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布摄影攻略分享帖</span></a></li>
		   	</ul>
		</div>
	</div>
	
	<div class="row">	
		<div class="col-md-2">
			<ul class="list-unstyled">
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 今日宣言</h4></a></li>
				<li><a href="#"><h4 class="text-center text-warning"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我要发布</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 即时分享</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的关注</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 个人资料</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的行程</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的活动</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的队伍</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程攻略</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的装备</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的回忆</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的驴友</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 摄影攻略</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 首页定制</h4></a></li>
				<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 消息留言</h4></a></li>
			</ul>
		</div>
		<div class="col-md-10" style="padding: 6px">
			 <form id="journey_form" action="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_MAPPING}" method="post">
			  	 <div class="row">
						<div class="col-md-12">
						  	  <ul class="list-inline">
						       	<li><input type="button" onClick="submit('#journey_form','#journey_content_div')" value="保存行程" class="btn btn-warning"/></li>
						       	<li><input type="button" onClick="submit('#journey_form','#journey_content_div')" value="发布行程" class="btn btn-warning"/></li>
						      	<li>
								   <input type="button" class="btn btn-warning" onClick="cleanForm('#journey_form')" value='清空'/>
								</li>
						      </ul>
					     </div>
			      </div>
			  	<div class="row">
				  	<div class="col-md-6">
				  	  <input type="hidden" data="data" id="id" name="id" value="">
				      <div class="form-group ">
					    <label class="control-label" for="title">行程标题: 
						</label>
					    <input type="text" data="data" name="title" value="" class="form-control" id="title" placeholder="行程标题" required="true"/>
					  </div>
					  <div class="form-group ">
					    <label class="control-label" for="manifesto">本程宣言: 
						</label>
					    <input type="text" data="data" name="manifesto" value="" class="form-control" id="manifesto" placeholder="行程宣言" required="true"/>
					  </div>
					  <div class="form-group ">
					    <label class="control-label" for="destinations">途经目的地: 
						</label>
					    <input type="text" data="data" name="destinations" value="<#if journey??>${journey.destinations}</#if>" class="form-control" id="destinations" placeholder="输入本次行程所经重要之地" required="true"/>
					  </div>
					  <div class="form-group ">
						   <label class="control-label" for="begin_end_date"> 行程起止时间:</label>
						   <div id="begin_end_date">
							<input type="text" data="data" name="beginDate" size="18" value="<#if journey??>${journey.beginDate}</#if>"  id="beginDate" placeholder="开始" />
						  	--
						    <input type="text" data="data" name="endDate" size="18" value="<#if journey??>${journey.endDate}</#if>" id="endDate" placeholder="结束" />
						    </div>
					   </div>
					 
				  </div>
				  <div class="col-md-6">
				   		 
					    <div class="form-group ">
					    	<label class="control-label" for="description">参与类型: 
							</label>
						  	<select name= 'participationType' data="data" id="participationType">
								  	<option value="${Journey.PARTICIPATION_TYPE_SELF}" >自由行</option>
								  	<option value="${Journey.PARTICIPATION_TYPE_PARTY_TEMP}" >临时组队</option>
								  	<option value="${Journey.PARTICIPATION_TYPE_PARTY_ORIG}" >原创队伍</option>
						  	</select>
					  </div>
					  <script type="text/javascript">
					  	$(document).ready(function(){ 
					  		$('#beginDate').datetimepicker({
							    format: 'yyyy-mm-dd hh:ii',
							    language:'zh-CN',
							    autoclose:true,
							});
							$('#endDate').datetimepicker({
							    format: 'yyyy-mm-dd hh:ii',
							    language:'zh-CN',
							    autoclose:true,
							});
							
							$('#detailDate').datetimepicker({
							    format: 'yyyy-mm-dd',
							    language:'zh-CN',
							    autoclose:true,
							    minView:2
							});
							
							//初始化，类型
							function initparticipationType(){
									$("#type_self").show();
					  				$("#type_party_1").hide();
					  				$("#type_party_2").hide();
							}
							initparticipationType();
							
					  		$('#participationType').change(function(){
					  			var value=$(this).children('option:selected').val();
					  			if(0==value){
					  				$("#type_self").show();
					  				$("#type_party_1").hide();
					  				$("#type_party_2").hide();
					  			}else if(1==value){
					  				$("#type_self").hide();
					  				$("#type_party_1").show();
					  				$("#type_party_2").show();
					  				$("#partyId").val("");
					  				$("#user_current_party").html("");
					  			}else if(2==value){
					  				$("#type_self").hide();
					  				$("#type_party_1").show();
					  				$("#type_party_2").show();
					  			}
					  		});
					  		
					  		
							$( 'textarea#content' ).ckeditor();
					  	}); 
					  </script>
					  <div class="form-group " id="type_self">
					    <label class="control-label">求伴同行: 
						</label>
					    <input type="checkbox" data="data" name="togetherRequired" <#if journey?? && journey.togetherRequired> checked </#if> value="<#if journey??>${journey.togetherRequired}</#if>>"  id="togetherRequired"/>
					  </div>
					  <div class="form-group" id="type_party_1">
					    <label class="control-label" for="partyName">队伍名称: 
						</label>
						<div id="user_current_party"></div>
						<input type="hidden" data="data" name="partyId" id="partyId" value=""/> 
					    <input type="text" data="data" name="partyName" value="<#if journey??>${journey.partyName}</#if>" class="form-control" id="partyName" placeholder="输入队伍名称" required="true"/>
					  </div>
					  
					  <div class="form-group" id="type_party_2">
					    人数:
						
					    <input type="text" data="data" name="minMember" size="3" value="<#if journey??>${journey.minMember}</#if>"  id="minMember" placeholder="最小"/>
					  	--
					    <input type="text" data="data" name="maxMember" size="2" value="<#if journey??>${journey.maxMember}</#if>" id="maxMember" placeholder="最大"/>
					    
					  </div>
					  
					  <div class="form-group ">
					    <label class="control-label" for="imagePath">行程封面图片: 
						</label>
					    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图片路径" />
					 	
					 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING})"/><br/>
					    <div id="iconPath_img">
					    	<img src='<#if journey??>${journey.imagePath}</#if>'/>
					    </div>
					  </div>
					  
				  </div>
			  </div>
			  <div class="row">
			  		<div class="col-md-12">
					   	<div class="form-group ">
						    <label class="control-label" for="content">行程内容: 
							</label>
							<textarea data="data" id="content" rows="10" name="content" value="<#if journey??>${journey.content}</#if>" class="form-control" id="content" placeholder="输入行程内容" required="true"><#if journey??>${journey.content}</#if></textarea>
						</div>
				  </div>
			  </div>
			 </form>
		</div>
	</div>
	
</div>







<#--
<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			<span class="label label-default">Default</span>
			<span class="label label-primary">Primary</span>
			<span class="label label-success">Success</span>
			<span class="label label-info">Info</span>
			<span class="label label-warning">Warning</span>
			<span class="label label-danger">Danger</span>
		</div>
	</div>
</div>
-->
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>