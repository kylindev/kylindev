
<#macro manageAreaItemIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#container_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
</#macro>

<#-- userManageAreaItemEditUI 添加、修改页面 begin-->
<#macro userManageAreaItemEditUI userManageAreaItem userManageAreaItemList>
<div class="row">
		  <div class="col-md-12">
		  		<p class="">菜单做了更改之后，必须刷新缓存才能即时生效，刷新缓存将刷新所有在线用户的菜单</p>
		 		<ul class="list-inline">
				      <li><input type="button" value="刷新缓存" class="btn btn-primary" onclick="refreshCache('${KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_REFRESH_CACHE_MAPPING}')"/></li>
				</ul>
		  </div>
</div>

<#if msg??><div class="alert alert-success" role="alert">${msg}</div></#if>

		  
<div class="row">
		  <div class="col-md-5">
			<form id="user_manageAreaItem_form_id" action="${KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING}">
			  	 <p class="text-primary">用户列表</p>
				 <div id="selectedUserDiv"> 
				 </div>
			 </form>
		</div>
		
		<div class="col-md-7">
		
		<p class="text-primary">用户管理区菜单项列表</p>
		<@listUserManageAreaItemView userManageAreaItemList=userManageAreaItemList/>
		</div> 
		<script type="text/javascript">
					function initSelectedUser(){
					       $.ajax({
								url: '${KylinboyCoreDeveloperConstant.KYLINBOY_SELECTED_USER3_UI_MAPPING}',
								data: '',
								type: "POST",
								dataType : "text",
								contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
								success: function(data) {
											$("#selectedUserDiv").html(data);
								},
								error: function() {
									alert( "Sorry, there was a problem!" );
								},
								complete: function() {
									//alert('complete');
								}
							}); 
					}
					initSelectedUser();
					
					function refreshCache(url){
						var request={};
						var containerId='';
						getContentWithCallBackForJson(url,request,containerId,function(data){
							alert(data.result.msg);
						});
					}
		</script>
</div>

</#macro>
<#--userManageAreaItemEditUI 添加、修改页面 end-->

<#macro listUserManageAreaItemView userManageAreaItemList>
<#if userManageAreaItemList?? && userManageAreaItemList?size gt 0>
<div id="msg"> </div>
		 <form id="manageAreaItem_list_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_USER_MANAGEAREAITEM_LIST_EDIT_MAPPING}" name="manageAreaItem_list_form" method="post">
				 <input type="hidden" data="data" name="userId" id="userId" class="form-control input-sm" value=""/>
				 <table class="table table-striped table-bordered table-condensed" >
					 <thead>
						 <tr>
						 	<th width="2%">
							 选择<input type="checkbox" id="manageAreaItem_checkbox_selectAllCheckbox1" value="all" onclick="selectAll('manageAreaItem_checkbox_selectAllCheckbox1','manageAreaItem_checkbox_')">
							</th>
						 	<th width="2%">
							 Id
							</th>
						 	<th width="8%">
							 名称
							</th>
							 <th width="8%">
							 代码
							 </th>
							 
							 <th width="5%">
							 图标
							 </th>
							 <th width="3%">
							 显示
							 </th>
							 <th width="3%">
							 禁用
							 </th>
							 <th width="3%">
							 排序
							 </th>
							 <th width="4%">
							 开发者ID
							 </th>
							 <th width="10%">
							  操作
							 </th>
						</tr>
					 </thead>
					 <tbody>
					<#list userManageAreaItemList as userManageAreaItem>  
						 <tr>
						 	<td>
						 		<input type="checkbox" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].checked" id="manageAreaItem_checkbox_${userManageAreaItem.manageAreaItem.uniqueIdentifier}" <#if userManageAreaItem.checked> checked </#if> class="form-control input-sm"/>
						 	</td>
						 	<td>
							 	${userManageAreaItem.manageAreaItem.id}
							 	<input type="hidden" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].manageAreaItemId" value="${userManageAreaItem.manageAreaItem.id}" id="manageAreaItemId${userManageAreaItem.manageAreaItem.uniqueIdentifier}" />
							 	<input type="hidden" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].manageAreaItem.id" value="${userManageAreaItem.manageAreaItem.id}" id="manageAreaItem_id${userManageAreaItem.manageAreaItem.uniqueIdentifier}" />
							 	<input type="hidden" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].id" value="${userManageAreaItem.id}" id="id${userManageAreaItem.manageAreaItem.uniqueIdentifier}" />
							</td>
							<td>
							 	<a href="${userManageAreaItem.manageAreaItem.url}" title="查看菜单【${userManageAreaItem.manageAreaItem.name}】" target="_blank">${userManageAreaItem.manageAreaItem.name}</a>
							</td>
							 <td>
							 	${userManageAreaItem.manageAreaItem.code}
							 </td>
							 <td>
							 	${userManageAreaItem.manageAreaItem.iconPath}
							 </td>
							 <td>
						 	 	<input type="checkbox" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].display" <#if userManageAreaItem?? && userManageAreaItem.display>checked</#if> class="form-control" id="display${userManageAreaItem.manageAreaItem.uniqueIdentifier}" />
							 </td>
							  <td>
						 	 	<input type="checkbox" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].disable" <#if userManageAreaItem?? && userManageAreaItem.disable>checked</#if> class="form-control" id="disable${userManageAreaItem.manageAreaItem.uniqueIdentifier}" />
							 </td>
							 <td>
							  	<input type="text" size="2" data="data" name="userManageAreaItemList[${userManageAreaItem_index}].sequence" value="<#if userManageAreaItem??>${userManageAreaItem.sequence}</#if>" class="form-control" id="sequence${userManageAreaItem.manageAreaItem.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
							 </td>
							  <td>
							  	${userManageAreaItem.manageAreaItem.developerId}
							 </td>
							 <td>
								 <ul class="list-inline"> 
									<li>
								 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${userManageAreaItem.id}" onclick="switchSetting('${userManageAreaItem.id}','${userManageAreaItem.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_EDIT_UI_MAPPING}','#container_content_div')">保存</a>
								  	</li>
								    <li>
								    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${userManageAreaItem.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANAGEAREAITEM_REMOVE_MAPPING}','#container_content_div')">移除</a>
								    </li>
								  </ul>
						 	  </td>
						 </tr>
					</#list>
					</tbody>
				</table>
		</form>
				<ul class="list-inline text-right">
				      <li><input type="button" value="保存列表" class="btn btn-primary" onclick="saveUserManageAreaItemList()"/></li>
				</ul>
</#if>
<script type="text/javascript">

function fillData(data){
		//先清除当前状态
		var _idPrefix="manageAreaItem_checkbox_";
		var _checkbox = $("#manageAreaItem_list_form").find("input[id^='"+_idPrefix+"']");
		if(_checkbox.length>0){
			_checkbox.each(function(){
				$(this).attr("checked",false);
				var _idItemValue = $(this).attr("id");
				
				if(_idItemValue != "manageAreaItem_checkbox_selectAllCheckbox1"){
					var _value = _idItemValue.substring(_idPrefix.length);
					
					var display = "#display" + _value;
					$(display).attr("checked",true);
					
					var disable = "#disable" + _value;
					$(disable).attr("checked",false);
					
					var sequence = "#sequence" + _value;
					$(sequence).val(0);
					
					var id = "#id" + _value;
					$(id).val("");
				}
				
			});
		}
		
		
		
		//再标识当前状态
		var result = data.result;
		//alert(result);
		if(result != undefined && result != null && result.length>0){
			//alert(result[0].checked);
			var length = result.length;
			for(var i=0;i<length;i++){
				var manageAreaItemId = result[i].manageAreaItem.id;
				var manageAreaItemChecked= "#manageAreaItem_checkbox_" + manageAreaItemId;
				$(manageAreaItemChecked).attr("checked",true);
				
				var userManageAreaItemId="#id"+manageAreaItemId;
				$(userManageAreaItemId).val(result[i].id);
				
				var display = "#display" + manageAreaItemId;
				$(display).attr("checked",result[i].display);
				
				var disable = "#disable" + manageAreaItemId;
				$(disable).attr("checked",result[i].disable);
				
				var sequence = "#sequence" + manageAreaItemId;
				$(sequence).val(result[i].sequence);
			}
		}
		
		var msg = data.msg;
		$("#msg").html(msg);
		$("#msg").addClass("alert alert-success");
	}
function queryUserManageAreaItem(url,userId){
	event.stopPropagation();
	
	//清除信息
	$("#msg").html("");
	$("#msg").removeClass("alert alert-success");
	
	
	var request={"userId":userId};
	$("#userId").val(userId);
	var containerId="";
	var callback=fillData;
	getContentWithCallBackForJson(url,request,containerId,callback);
}

function saveUserManageAreaItemList(){
	submitForm2("#manageAreaItem_list_form","", fillData);
}
</script>
</#macro>

<#macro getManageAreaItemDropdown  manageAreaItemList currentManageAreaItem='' name="manageAreaItem.id">
<select name=${name} data="data">
		<#list manageAreaItemList as manageAreaItem>
	  		<option value="${manageAreaItem.id}" <#if manageAreaItem?? && currentManageAreaItem?? && currentManageAreaItem!='' && manageAreaItem.id == currentManageAreaItem.id> selected </#if> >${manageAreaItem.name}</option>
		</#list>
</select>
</#macro>

<#macro listUserManageAreaItem currentUserManageAreaItem=''>
<ul class="list-unstyled">
	
	<#assign userManageAreaItemList=TravelStaticDataUtil.getUserManageAreaItemListByUserId(Jit8ShareInfoUtils.getUserId())>
	<#assign currentUrl=''/>
	
	<#if currentUserManageAreaItem!='' && currentUserManageAreaItem?? && currentUserManageAreaItem.manageAreaItem>
		<#assign currentUrl=currentUserManageAreaItem.manageAreaItem.url/>
	</#if>
	
	<#if userManageAreaItemList?? && userManageAreaItemList?size gt 0>
		<#list userManageAreaItemList as userManageAreaItem>
			<#assign url=userManageAreaItem.manageAreaItem.url/>
			<li><a href="${url}"><h4 class="text-center <#if url==currentUrl> text-success </#if>"><span class="glyphicon glyphicon-circle-arrow-right"></span> ${userManageAreaItem.manageAreaItem.name}</h4></a></li>
		</#list>
	</#if>
	
</ul>
</#macro>
<#macro listItemButton currentUserManageAreaItem=''>
<ul class="list-unstyled">
currentUserManageAreaItem
</ul>
</#macro>