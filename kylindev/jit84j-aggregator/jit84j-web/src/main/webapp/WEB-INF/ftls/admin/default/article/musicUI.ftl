<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>上传文件</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-4">
<img src="/images/user/register.jpg" title="梧桐山顶，望向远方" alt="梧桐山顶，望向远方" class="img-rounded">
<p class="lead">寻梦，在远方...</p>
<p class="text-warning">
<dl>
  <dt>最新注册:</dt>
  <dd>kylinboy1 来自山东网友</dd>
  <dd>kylinboy2 来自河南朋友</dd>
  <dd>kylinboy3 来自深圳朋友</dd>
  <dd>kylinboy4</dd>
  <dd>kylinboy5</dd>
  <dd>kylinboy6</dd>
  <dd>kylinboy7</dd>
</dl></p>
</div>
  <div class="col-md-8">
  <p class="text-success">上传文件</p>
  
  <@form.form method="POST" commandName="music"
		enctype="multipart/form-data" action="/admin/music/upload">
 
		<@form.errors path="*" cssClass="errorblock" element="div" />
 
		Please select a file to upload : <input type="file" name="files" />
		<input type="submit" value="upload" />
		<span><@form.errors path="music" cssClass="error" />
		</span>
 
	</@form.form>
  
  </div>
</div>
</div>

</body>
</html>