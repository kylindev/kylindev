<#macro getFileSystemGalleryTypeDropdownWithAll fileSystemGallery systemGalleryTypeList name="type">
<select name="${name}" data="data">
		<option value="">ALL</option>
		<#list systemGalleryTypeList as galleryType>
	  	<option value="${galleryType.type}" <#if fileSystemGallery.type == galleryType.type>selected</#if>>${galleryType.name}</option>
		</#list>
</select>
</#macro>

<#macro getFileSystemGalleryTypeDropdown fileSystemGallery systemGalleryTypeList name="type">
<select name="${name}" data="data">
		<#list systemGalleryTypeList as galleryType>
	  	<option value="${galleryType.type}" <#if fileSystemGallery?? && fileSystemGallery.type == galleryType.type>selected</#if>>${galleryType.name}</option>
		</#list>
</select>
</#macro>


<#--fileSystemGalleryUI 添加、修改页面 begin-->
<#macro fileSystemGalleryTypeAddUI fileSystemGallery>
<div class="row">
<#assign systemGalleryTypeList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_TYPE_DROPDOWN)/>
		  <div class="col-md-5">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 系统分类类型</p>
		  	  
		  	  <form id="fileSystemGalleryType_form" action="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_MAPPING}" method="post">
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if fileSystemGallery??>${fileSystemGallery.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称【不能重复】: 
				</label>
			    <input type="text" data="data" name="name" value="<#if fileSystemGallery??>${fileSystemGallery.name}</#if>" class="form-control" id="name" placeholder="输入名称，必须唯一" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="type">类型【使用 0-9 数字，不能重复】: 
				</label>
			  <input type="text" data="data" name="type" value="<#if fileSystemGallery??>${fileSystemGallery.type}</#if>" class="form-control" id="type" placeholder="输入类型，必须为数字，且唯一"/>
			 <#--  -->
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if fileSystemGallery??>${fileSystemGallery.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if fileSystemGallery??>${fileSystemGallery.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#fileSystemGalleryType_form','#fileSystemGallery_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#fileSystemGalleryType_form')" value='清空'/>
				</li>
		      </ul>
		</div>	 
		<div class="col-md-7">
		  	  <p class="text-primary">系统分类类型列表</p> 
		  	  <@listFileSystemGalleryTypeView page=page/>
		 </div>	
		 <script type="text/javascript">
		 			function cleanForm(formId){
		 				if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				$(this).attr("value","");
				  			});
			  			}
		 			}
		 			function submit(formId,backDataContainerId){
		 				var request = new Object();
			  			if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  			});
			  			}
			  			
			  			
			  			var _select = formId+" select[data$='data']";
			  			if($(_select).length>0){
				  			$(_select).each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  			});
			  			}
			  			
						var action = $(formId).attr("action");
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
					function removeItem(id,action,backDataContainerId){
						if(id==''){
							alert("没有id");
							return;
						}
						alert("ddd");
						var request = {'id':id};
						
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
		 </script> 
</div>
</#macro>
<#--fileSystemGalleryUI 添加、修改页面 end-->

<#macro listFileSystemGalleryTypeView page>
<#if page?? && page.content??>
		<#assign fileSystemGalleryTypeList = page.content />
		 <table class="table">
		 <tr>
		 <th>
			 Id
			</th>
		 	<th>
			 名称
			</th>
			 <th>
			 描述
			 </th>
			 <th>
			 类型
			 </th>
			 <th>
			 排序
			 </th>
			 <th>
			 操作
			 </th>
		</tr>
		<#list fileSystemGalleryTypeList as fileSystemGallery>  
			 <tr>
			 <td>
				 ${fileSystemGallery.id}
				</td>
				<td>
				 ${fileSystemGallery.name}
				</td>
				 <td>
				 ${fileSystemGallery.description}
				 </td>
				 <td>
				 ${fileSystemGallery.type}
				 </td>
				 <td>
				  ${fileSystemGallery.sequence}
				 </td>
				 <td>
					 <ul class="list-inline"> 
						<li>
					 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${fileSystemGallery.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_ADD_UI_MAPPING}')">编辑</a>
					  	</li>
					    <li>
					    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${fileSystemGallery.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_TYPE_REMOVE_MAPPING}','#fileSystemGallery_content_div')">删除</a>
					    </li>
					  </ul>
				  </td>
			 </tr>
		</#list>
			<tr>
			  <td colspan="6">
				  <ul class="list-inline"> 
					<li>
				 		<a class="btn btn-primary" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${fileSystemGallery.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING}')">保存列表</a>
				  	</li>
				    
				  </ul>
			  </td>
			 </tr>
		</table>
</#if>
<script type="text/javascript">

</script>
</#macro>