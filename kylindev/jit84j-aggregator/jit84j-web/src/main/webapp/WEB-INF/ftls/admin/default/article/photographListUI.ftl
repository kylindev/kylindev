<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>相片列表</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-9">
<p class="text-success">相片列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>标题</th>
      <th>描述</th>
      <th>拍摄时间</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "photographList" />  
  <@form.form commandName="photographList" role="form" action="/photographUI">
  <tbody>
  <#if (photographList??)>

	<#list photographList as photograph>  


	 <tr>
	 <td>${photograph.uniqueIdentifier}
	 </td>
	 <td>
	 <input type="text" name="photographList[${photograph_index}].title" value="${photograph.title}" class="form-control" id="title[${photograph_index}]" placeholder="输入标题"/>
	 </td>
	 <td>
	 <input type="text" name="photographList[${photograph_index}].description" value="${photograph.description}" class="form-control" id="description[${photograph_index}]" placeholder="输入描述"/>
	 </td>
	 <td>
	 <input type="text" name="photographList[${photograph_index}].snatTime" value="${photograph.snatTime}" class="form-control" id="snatTime[${photograph_index}]" placeholder="拍摄时间"/>
	 </td>
	 <td>
	 <a class="text-danger" href='<@spring.url relativeUrl="/admin/photograph/modifyUI/${photograph.uniqueIdentifier}"/>'><input type="button" value="编 辑" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/photograph/delete/${photograph.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
	  | <a class="text-danger" href='<@spring.url relativeUrl="/admin/photograph/modify/${photograph.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
	 </td>
	 </tr>
	</#list>
	<tr>
		<td>
		<input type="submit" value="保存列表" class="btn btn-primary"/>
		</td>
		<td>
		<a class="text-danger" href='<@spring.url relativeUrl="/admin/photograph/photographAddUI"/>'>
		<input type="button" value="添加分类" class="btn btn-primary"/>
		</a>
		</td>
		<td>
		<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
  </#if>
   </tbody>
  </table>
</div>
 </div>
 </@form.form>
  <div class="col-md-6">
  
  </div>
</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>