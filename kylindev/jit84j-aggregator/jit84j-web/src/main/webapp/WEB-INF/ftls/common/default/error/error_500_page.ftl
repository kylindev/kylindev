<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/assets/common/css/bootstrap/bootstrap_min.css" rel="stylesheet" media="screen">
<link rel="icon" href="/assets/data/image/site_info/icon3.png">
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/assets/common/js/jquery/jquery.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/assets/common/js/bootstrap/bootstrap.min.js"></script>
</head>
<body>


<div class="container" id="top_login">
<@languageBar/>
<@spring.message "label.menu"/>
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-12" style="height:300px">
			<span class="text-danger">${exception.message}
			出错啦，哈哈哈！！！！！！！！！！
			</span>
		</div>
	</div>
</div>

<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>