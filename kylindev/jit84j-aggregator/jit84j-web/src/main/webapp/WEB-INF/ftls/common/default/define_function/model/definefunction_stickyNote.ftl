<#macro stickyNoteCategorySpecialIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#stickyNoteCategorySpecial_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</#macro>

<#--stickyNoteEditUI 添加、修改页面 begin-->
<#macro stickyNoteEditUI stickyNote>
<form id="stickyNote_form" action="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_EDIT_MAPPING}" method="post">
<div class="row">
		  <div class="col-md-12">
 				<p class="text-danger">${msg}</p>
		  	    <p class="text-primary">添加/修改 帖子</p>
		  	    <div id="stickyNote_result_id">
		  	     <input type="hidden" data="data" id="id" name="id" value="<#if stickyNote??>${stickyNote.id}</#if>">
		  	    </div>
		  </div>
</div>	  	
<div class="panel panel-warning">
	<div class="panel-heading ">选择帖子分布类别</div>
	<div class="panel-body">   
			<div class="row">
				  <div class="col-md-2">
				  	
								<label>特殊区：
								<@specialAreaSearch specialArea=specialArea name="specialAreaId"/>
								<script type="text/javascript">
									$(document).ready(function(){
										$("select").change(function(){
											var areaId = $(this).val();
											var stickyNoteId = $("#id").val() ;
											var request={"id":stickyNoteId,"specialAreaId":areaId};
											var url = '${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CHECKED_CATEGORY_MAPPING}';
											getContent(url,request,"#specialCategory_id");
										});
									})
									function submitStickyNote(formId,containerId){
										var _specialCategory = $("#specialCategory_id");
										if(_specialCategory.length>0){
											var _input= _specialCategory.find("input[data$='data-check-value']:checked");
											if(_input.length>5){
									  			alert("不能超过5个分类");
									  			return;
											}
											if(_input.length<1){
									  			alert("至少选择1个分类");
									  			return;
											}
										}
										var _input= $(formId).find("[data$='data']");
										if(_input.length>0){
											var _required="true";
								  			_input.each(function(){
								  				var node=$(this).attr("required");
								  				if(node=="required"){
								  					if($(this).attr("value")==""){
								  						alert($(this).attr("placeholder"));
								  						_required="false";
								  						return false;
								  					}
								  				}
								  			});
								  			if(_required=="false"){
								  				return;
								  			}
										}
										
										submit(formId,containerId);
									}
								</script>
								</label>
				  </div>
				   <div class="col-md-10">
				   	类别：<span id="specialCategory_id">
							<@categorySpecialCheckboxUI  categorySpecialList=categorySpecialList categorySpecialCheckedSet=categorySpecialCheckedSet name="categorySpecialIdList"/>
						</span>
				  </div>
		    </div>
	</div>
</div>
<div class="row">
		  <div class="col-md-4">
		  	
		      <div class="form-group ">
			    <label class="control-label" for="title">名称: 
				</label>
			    <input type="text" data="data" name="title" value="<#if stickyNote??>${stickyNote.title}</#if>" class="form-control" id="title" placeholder="输入标题" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="author">作者: 
				</label>
			    <input type="text" data="data" name="author" value="<#if stickyNote??>${stickyNote.author}</#if>" class="form-control" id="author" placeholder="输入作者" required="true"/>
			  </div>
			  
			   <div class="form-group ">
			    <label class="control-label" for="musicPath">音乐: 
				</label>
			    <input type="text" data="data" name="musicPath" class="form-control" id="musicPath" placeholder="输入音乐路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if stickyNote??>${stickyNote.musicPath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if stickyNote??>${stickyNote.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" />
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="browseMaxCount">浏览次数限制: 
				</label>
			    <input type="text" data="data" name="browseMaxCount" value="<#if stickyNote??>${stickyNote.browseMaxCount}</#if>" class="form-control" id="browseMaxCount" placeholder="输入描述"/>
			  </div>
			  
			   <div class="checkbox">
			    <label class="checkbox-inline" >允许评论
			    <input type="checkbox" data="data" name="commentable" <#if stickyNote?? && stickyNote.commentable> checked </#if> value="${stickyNote.commentable}" id="commentable"/>
				</label>
			 </div>
			  <div class="checkbox">
			    <label class="checkbox-inline">锁帖 <input type="checkbox" data="data" name="locked" <#if stickyNote?? && stickyNote.locked> checked </#if> value="${stickyNote.locked}"  id="locked"/></label>
			  </div>
			  <div class="checkbox">
			    <label class="checkbox-inline" for="friendable">好友可见 
			    <input type="checkbox" data="data" name="friendable" <#if stickyNote?? && stickyNote.friendable> checked </#if> value="${stickyNote.friendable}"  id="friendable"/>
			  </label>
			  </div>
			  <div class="checkbox">
			    <label class="checkbox-inline" for="transpond">是否转载帖 
			    <input type="checkbox" data="data" name="transpond" <#if stickyNote?? && stickyNote.transpond> checked </#if> value="${stickyNote.transpond}"  id="transpond"/>
			 	 </label>
			 </div>
			 <div class="form-group ">
			    <label class="control-label" for="transpondUrl">转载URL: 
				</label>
			    <input type="text" data="data" name="transpondUrl" value="<#if stickyNote??>${stickyNote.transpondUrl}</#if>" class="form-control" id="transpondUrl" placeholder="转载URL"/>
			  </div>
			  
			 <div class="form-group ">
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submitStickyNote('#stickyNote_form','#stickyNote_result_id')" value="保存文章" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#stickyNote_form')" value='清空'/>
				</li>
		      </ul>
		      </div>
		</div>
		<div class="col-md-8">
			 
			  <div class="form-group ">
			    <label class="control-label" for="summary">概要（小于250字）: 
				</label>
				<textarea data="data" id="description" name="summary" value="<#if stickyNote??>${stickyNote.summary}</#if>" class="form-control" id="summary" placeholder="输入概要" required="true"><#if stickyNote??>${stickyNote.summary}</#if></textarea>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="content">内容（多于250字）: 
				</label>
			    <textarea type="text" rows="12" data="data" name="content" value="<#if stickyNote??>${stickyNote.content}</#if>" class="form-control" id="content" placeholder="输入内容" required="true"><#if stickyNote??>${stickyNote.content}</#if></textarea>
			  </div>
			   <div class="form-group ">
			    <label class="control-label" for="imagePath">图片一: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图片路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if stickyNote??>${stickyNote.imagePath}</#if>'/>
			    </div>
			  </div>
			   <div class="form-group ">
			    <label class="control-label" for="image2Path">图片二: 
				</label>
			    <input type="text" data="data" name="image2Path" class="form-control" id="image2Path" placeholder="输入图片路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if stickyNote??>${stickyNote.image2Path}</#if>'/>
			    </div>
			  </div>
			   <div class="form-group ">
			    <label class="control-label" for="image3Path">图片三: 
				</label>
			    <input type="text" data="data" name="image3Path" class="form-control" id="image3Path" placeholder="输入图片路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if stickyNote??>${stickyNote.image3Path}</#if>'/>
			    </div>
			  </div>
		</div>
		
</div>
</form>
</#macro>
<#--stickyNoteEditUI 添加、修改页面 end-->

<#macro listStickyNoteView page formId="stickyNot_list_form">
<#if page?? && page.content??>
		<#assign stickyNoteList = page.content />
		 <table class="table">
		 <tr>
		 <th>
			 Id
			</th>
		 	<th>
			 标题
			</th>
			 <th>
			 作者
			 </th>
			 <th>
			 用户Id
			 </th>
			 <th>
			 发布时间
			 </th>
			 <th>
			 等级
			 </th>
			 <th>
			  类别
			 </th>
			 
			 <th>
			 浏览次数
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		<#list stickyNoteList as stickyNote>  
			 <tr>
			 <td>
				 ${stickyNote.id}
				</td>
				<td>
				 ${stickyNote.title}
				</td>
				 <td>
				 ${stickyNote.author}
				 </td>
				 <td>
				 ${stickyNote.ownerId}
				 </td>
				 <td>
				 ${stickyNote.publishTime}
				 </td>
				 <td>
				 ${stickyNote.level}
				 </td>
				 <td>
				 <ul class="list-unstyled">
				 <#list stickyNote.stickyNoteCategorySpecialSet as category>
				 <li>${category.categorySpecial.name}</li>
				 </#list>
				 </ul>
				 </td>
				  <td>
				  ${stickyNote.browseMaxCount}
				 </td>
				 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_EDIT_UI_MAPPING}/${stickyNote.id}' id="left_navigation_${stickyNote.id}">编辑</a>
				  	</li>
				    <li>
				    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${stickyNote.id}','${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_DELERE_MAPPING}','#stickyNote_list_content_div_id')">删除</a>
				
				    </li>
				  </ul>
			 </td>
			 </tr>
		</#list>
		<tr>
		<td colspan="9">
		<@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_LIST_UI_MAPPING}" formId='${formId}'/>
		</td>
		</tr>
		</table>
</#if>
</#macro>