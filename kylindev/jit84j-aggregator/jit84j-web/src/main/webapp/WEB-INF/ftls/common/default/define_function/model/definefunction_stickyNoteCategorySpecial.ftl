<#macro stickyNoteCategorySpecialIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#stickyNoteCategorySpecial_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</#macro>

<#--stickyNoteCategorySpecialEditUI 添加、修改页面 begin-->
<#macro stickyNoteCategorySpecialEditUI stickyNoteCategorySpecial page>
<div class="row">
		  <div class="col-md-3">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 特殊区</p>
		  	  
		  	  <form id="specialArea_form" action="${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_MAPPING}" method="post">
		  	
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if specialArea??>${specialArea.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称: 
				</label>
			    <input type="text" data="data" name="name" value="<#if specialArea??>${specialArea.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="code">代码: 
				</label>
			    <input type="text" data="data" name="code" value="<#if specialArea??>${specialArea.code}</#if>" class="form-control" id="code" placeholder="输入代码，使用字母，必须唯一" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="imagePath">图标: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图标路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if specialArea??>${specialArea.imagePath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if specialArea??>${specialArea.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if specialArea??>${specialArea.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="openable">是否开放: 
				</label>
			    <input type="checkbox" data="data" name="openable" <#if specialArea?? && specialArea.openable> checked </#if> value="${specialArea.openable}" class="form-control" id="openable"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="writable">允许发布: 
				</label>
			    <input type="checkbox" data="data" name="writable" <#if specialArea?? && specialArea.writable> checked </#if> value="${specialArea.writable}" class="form-control" id="writable"/>
			  </div>
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#specialArea_form','#specialArea_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#specialArea_form')" value='清空'/>
				</li>
		      </ul>
		</div>
		<div class="col-md-9">
		
		<p class="text-primary">特殊区列表</p>
		<@listSpecialAreaView page=page/>
		</div> 
		<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</div>
</#macro>
<#--specialAreaEditUI 添加、修改页面 end-->

<#macro listStickyNoteCategorySpecialView page formId="stickyNotCategorySpecial_list_form">
<#if page?? && page.content??>
		<#assign stickyNoteCategorySpecialList = page.content />
		 <table class="table">
		 <tr>
		 <th>
			 Id
			</th>
		 	<th>
			 标题
			</th>
			 <th>
			 作者
			 </th>
			 <th>
			 用户Id
			 </th>
			 <th>
			 发布时间
			 </th>
			 <th>
			  类别
			 </th>
			 <th>
			 等级
			 </th>
			 <th>
			 浏览次数
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		<#list stickyNoteCategorySpecialList as stickyNoteCategorySpecial>  
			 <tr>
			 <td>
				 ${stickyNoteCategorySpecial.stickyNote.id}
				</td>
				<td>
				 ${stickyNoteCategorySpecial.stickyNote.title}
				</td>
				 <td>
				 ${stickyNoteCategorySpecial.stickyNote.author}
				 </td>
				 <td>
				 ${stickyNoteCategorySpecial.stickyNote.ownerId}
				 </td>
				 <td>
				 ${stickyNoteCategorySpecial.stickyNote.publishTime}
				 </td>
				
				 <td>
				 ${stickyNoteCategorySpecial.categorySpecial.name}
				 </td>
				  <td>
				 ${stickyNoteCategorySpecial.stickyNote.level}
				 </td>
				  <td>
				  ${stickyNoteCategorySpecial.stickyNote.browseMaxCount}
				 </td>
				 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${specialArea.id}" onclick="switchSetting('${specialArea.id}','${specialArea.id}','${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_UI_MAPPING}','#specialArea_content_div')">编辑</a>
				  	</li>
				    <li>
				    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${specialArea.id}','${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_REMOVE_MAPPING}','#specialArea_content_div')">删除</a>
				
				    </li>
				  </ul>
			 </td>
			 </tr>
		</#list>
		<tr>
		<td colspan="9">
		<@pageBar page=page url="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_MAPPING}" formId='${formId}'/>
		</td>
		</tr>
		</table>
</#if>
</#macro>