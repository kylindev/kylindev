<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_developer.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>URL列表</title>

	
</head>
<div class="container" id="top_login">
	   <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>
<body>
<div class="container body_id" id="body_content">

<div class="row">
<div class="col-md-12">
<link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/demo/demo.css">
    <script type="text/javascript" src="/assets/common/js/jquery/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/assets/common/js/jquery/jquery-easyui-1.3.5/plugins/jquery.edatagrid.js"></script>
	
    <table id="dg" title="URL定义类" style="width:1000;height:350px"
            toolbar="#toolbar" pagination="true" idField="id"
            rownumbers="true" fitColumns="true" singleSelect="true">
        <thead>
            <tr>
            <th field="id" width="40">Id</th>
                <th field="title" width="100" editor="{type:'validatebox',options:{required:true}}">标题</th>
                <th field="clazzFullName" width="500" editor="{type:'validatebox',options:{required:true}}">类</th>
                <th field="description" width="150" editor="text">描述</th>
                 <th field="developerUserId" width="90" >开发者ID</th>
                <th field="developerName" width="90" >开发者</th>
            </tr>
        </thead>
    </table>
    <div id="toolbar">
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-add" plain="true" onclick="javascript:$('#dg').edatagrid('addRow')">新建</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-remove" plain="true" onclick="javascript:$('#dg').edatagrid('destroyRow')">删除</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-save" plain="true" onclick="javascript:$('#dg').edatagrid('saveRow')">保存</a>
        <a href="javascript:void(0)" class="easyui-linkbutton" iconCls="icon-undo" plain="true" onclick="javascript:$('#dg').edatagrid('cancelRow')">取消</a>
    </div>
    <script type="text/javascript">
        $(function(){
            $('#dg').edatagrid({
                url: '${KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING}2',
                saveUrl: '${KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_ADD_MAPPING}',
                updateUrl: '${KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_UPDATE_MAPPING}',
                destroyUrl: '${KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_REMOVE_MAPPING}'
            });
        });
    </script>
    
</div>

</div>
</div>

</body>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>