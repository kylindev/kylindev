<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">
<#include "definefunction_travel.ftl">

<@editInstantShare instantShare=instantShare/>

<#if instantShare??>
<#assign imageList = instantShare.imageList />
 <#if imageList?? && imageList?size gt 0>
	 <div id="image_content_div_id" class="hide">
	 <#list imageList as image>
		<div class='row' id="row_item_id_${image.fileInfoId}_back">
			<div class='col-md-1'>
					<input data='data' name='imageList[${image_index}].sequence' value='${image.sequence}' placeholder='序号' class='form-control'/>
					<input data='data' type='hidden' name='imageList[${image_index}].fileInfoId' value='${image.fileInfoId}'/>
					<input data='data' type='hidden' name='imageList[${image_index}].id' value='${image.id}' id='id_${image.id}'/>
					<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${image.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_MAPPING}','#instantShare_content_div')">移除</a>
			</div>
			<div class='col-md-6'>
					<input data='data' name='imageList[${image_index}].title' value='${image.title}' placeholder='输入标题' class='form-control'/><br/>
					<textarea data='data' class='form-control' rows='5' name='imageList[${image_index}].description' placeholder='输入图片背景故事'>${image.description}</textarea>
			</div>
			<div class='col-md-5'>
					<img src='${image.thumbnailUrl}' width='360px'><p> ${image.title} </p>
					<input data='data' type='hidden' name='imageList[${image_index}].imageUrl' value='${image.imageUrl}'/>
					<input data='data' type='hidden' name='imageList[${image_index}].thumbnailUrl' value='${image.thumbnailUrl}'/>
			 </div>
		</div>
	</#list>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			copyHideImageToContentContainer();
		});
	</script>
	</#if>
</#if>

<script type="text/javascript">
$(document).ready(function(){
	var _batch = $("#batch").val();
	$("#fileInfo_batch_id").val(_batch);
});
</script>
