<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_developer.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>URL列表</title>

	
</head>
<div class="container" id="top_login">
	   <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>
<body>
<div class="container body_id" id="body_content">

<div class="row">
<div class="col-md-12">
<link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/themes/bootstrap/easyui.css">
    <link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/themes/icon.css">
    <link rel="stylesheet" type="text/css" href="/assets/common/js/jquery/jquery-easyui-1.3.5/demo/demo.css">
    <script type="text/javascript" src="/assets/common/js/jquery/jquery-easyui-1.3.5/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="/assets/common/js/jquery/jquery-easyui-1.3.5/locale/easyui-lang-zh_CN.js"></script>
	
    <table id="tt" class="easyui-datagrid" style="width:1000px;height:350px"
            url="${KylinboyDeveloperConstant.KYLINBOY_CONSTANT_MODEL_LIST_UI_MAPPING}2"
            title="URL定义类列表" iconCls="icon-save"
            loadMsg="load data...please waiting"
            singleSelect="true"
            rownumbers="true" pagination="true">
        <thead>
        <tr>
            <th field="id" width="40" >Id</th>
                <th field="title" width="100" >标题</th>
                <th field="clazzFullName" width="500" >类</th>
                <th field="description" width="150">描述</th>
                 <th field="developerUserId" width="90" >开发者ID</th>
                <th field="developerName" width="90" >开发者</th>
            </tr>
            
        </thead>
    </table>
    <div id="win_static_url_list" class="container"></div>
    <script type="text/javascript">
    	$('#tt').datagrid({
			toolbar: [{
				iconCls: 'icon-help',
				handler: function(){alert('选择一条，双击查看详细定义')}
			}],
			onDblClickRow: function(rowIndex, rowData){
				listStaticUrlMapping(rowData.title + rowData.clazzFullName,'${KylinboyDeveloperConstant.KYLINBOY_URL_MAPPING_AJAX_MAPPING}',rowData.clazzFullName);
			}
		});
		
		function listStaticUrlMapping(title,action,clazzFullName){
					$('#win_static_url_list').window({
					    width:1050,
					    height:500,
					    title:title,
					    modal:true
					});
					var _html = $('#win_static_url_list').html();
					if(_html == '' || _html.length<=0){
						getData(action,clazzFullName);
					}
					$('#win_static_url_list').widonw('open');
				}
				
				function getData(action,clazzFullName){
					$.ajax({
						url: action,
						data: {'clazzFullName':clazzFullName},
						type: "POST",
						dataType : "text",
						contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
						success: function(data) {
								$('#win_static_url_list').html("<div id='content'>" + data + "</div>");
								
						},
						error: function() {
							alert( "Sorry, there was a problem!" );
						},
						complete: function() {
							//alert('complete');
						}
					});
				}
    	
    </script>
</div>

</div>
</div>

</body>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>