<#-- 切换语言begin -->
<#macro languageBar>  
<ul class="list-inline">
	  <li>
	  	<a class="text-primary" href="javascript:void(0)" onclick="changeLanguage('en')">EN</a>
	  </li>
	  <li>
	    <a class="text-primary" href="javascript:void(0)" onclick="changeLanguage('zh')">中文</a>
	  </li>
</ul>
<script type="text/javascript">
function changeLanguage(language)
{
     $.ajax({
       type: "GET",
       url: "${KylinboyDeveloperConstant.KYLINBOY_CHANGE_LANGUAGE_MAPPING}/"+language,
       data: "lang="+language,
       dataType:"json",
       async: true,
       error: function(data, error) {alert(data.responseText+"change lang error!");},
       success: function(data)
       {
            window.location.reload();
       }
	}); 
}
</script>
</#macro> 
<#-- 切换语言end -->

<#-- 获取格式化时间 begin -->
<#-- currentTime 毫秒值 -->
<#macro timeFormat currentTime>
	<#if currentTime lt 1000>
		<span class="text-danger">1</span>秒前
	<#else>
		<#assign currentTimeTemp = currentTime>
		<!--1天＝86400秒-->
		<#if currentTimeTemp gt 86400000>
			<#assign days=CalendarUtils.getDays(currentTimeTemp)/>
			<span class="text-danger">${days}</span>天
			<#assign currentTimeTemp = currentTimeTemp%86400000>
		<#elseif currentTimeTemp gt 3600000>
		<!--1小时＝3600秒-->
			<#assign hours=CalendarUtils.getHours(currentTimeTemp)/>
			<span class="text-danger">${hours}</span>小时
			<#assign currentTimeTemp = currentTimeTemp%3600000>
		<!--1分钟＝60秒-->
		<#elseif currentTimeTemp gt 60000>
			<#assign minutes=CalendarUtils.getMinutes(currentTimeTemp)/>
			<#assign currentTimeTemp = currentTimeTemp%60000>
			<span class="text-danger">${minutes}</span>分钟
			<#assign currentTimeTemp = CalendarUtils.getSeconds(currentTimeTemp)>
			<span class="text-danger">${currentTimeTemp}</span>秒
		</#if>
		前
	</#if>
</#macro> 


<#-- 获取格式化时间 end -->

<#-- 截取字符串 begin -->
<#macro localSubstring str max=100>
<#if str?? && str?length gt max>${str?substring(0,max)}...<#else>${str}</#if>
</#macro> 
<#-- 截取字符串 end -->