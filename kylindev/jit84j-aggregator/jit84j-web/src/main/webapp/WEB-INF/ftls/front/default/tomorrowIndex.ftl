<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_specialArea.ftl">
<#include "df_framework.ftl">
<html lang="zh-CN" class=" js csstransforms csstransforms3d csstransitions">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="icon" href="/assets/image/site_info/icon3.png">

<title>旅行即时分享社区--独家记忆</title>

</head>
<body>

<div class="container" id="top_login">
	<div class="row">
		<div class="col-md-3"><@languageBar/></div>
		<div class="col-md-4">
			<embed src="http://www.xiami.com/widget/36809170_1771902817/singlePlayer.swf" type="application/x-shockwave-flash" width="257" height="33" wmode="transparent" autoPlay="true"></embed>
		</div>
		<div class="col-md-5 text-right"><a class="text-primary">进入我的管理区</a> <@loginAndLogout /></div>
	</div>
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
		<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/><#---->
</div>

<div class="container" id="body_content">
	<div class="row">	
		<div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-heading text-center"><span class="text-danger"><span class="glyphicon glyphicon glyphicon-fire"></span>长线活动召集公告</span></div>
		  <div class="panel-body">
		   		<ul class="list-inline">
		   		<li>
					<a href="#" class="text-primary">漫游西藏40天活动召集，出发时间七月七日，报名截止时间六月三十日<span class="badge">23</span></a>
					<br/>
					<a href="#" class="text-primary">厦门-土楼-武夷山-20天骑行活动召集[4-8人]，出发时间6月13日，报名截止时间6月13日<span class="badge">07</span></a>
				</li>
				<li>
					<a href="#" class="text-primary">开往欧洲秋季的旅行列车，到达时间十月十四日，欧洲小镇浪漫之旅<span class="badge">59</span></a>
					<br/>
					<a href="#" class="text-primary">雪山、峡谷、飞溪、古树、巨石...徒步雨崩-梅里雪山-西当村，出发时间9月4日<span class="badge">36</span></a>
				</li>
				</ul>
		  </div>
		</div>
			
		</div>
	</div>
	<div class="row">
		<div class="col-md-2">
			<button class="btn btn-warning">我要发布</button><h4 class="text-warning text-right"><strong>最热标签:</strong></h4>
		</div>
		<div class="col-md-10">
		<p class="text-left text-left" style="padding: 10px">
			<button class="btn"><label class="label label-default">好玩的</label></button>
			<button class="btn"><label class="label label-default">有趣的</label></button>
			<button ><label class="label label-default">奇葩的</label></button>
			<button class="btn"><label class="label label-success">风光的</label></button>
			<button><label class="label label-danger">深度的</label></button>
			<button><label class="label label-default">秀的</label></button>
			<button><label class="label label-default">美不胜收的</label></button>
			<button><label class="label label-warning">瞬时的</label> </button>
			<button><label class="label label-default">问路的</label> </button>
			<button><label class="label label-default">你懂的</label> </button>
			<button class="btn btn-primary"><label class="label label-primary">在哪的</label></button>
			<button><label class="label label-info">有什么的</label></button>
			<button><label class="label label-default">其实没什么的</label></button>
			<button><label class="label label-default">无聊的</label></button>
			
			<button><label class="label label-default">好玩的</label></button>
			<button><label class="label label-default">有趣的</label></button>
			<button><label class="label label-default">奇葩的</label></button>
			<button><label class="label label-success">风光的</label></button>
			<button><label class="label label-danger">深度的</label></button>
			<button><label class="label label-default">秀的</label></button>
			<button><label class="label label-default">美不胜收的</label></button>
			<button><label class="label label-warning">瞬时的</label> </button>
			<button><label class="label label-default">问路的</label> </button>
			<button><label class="label label-default">你懂的</label> </button>
			<button><label class="label label-default">在哪的</label></button>
			<button><label class="label label-info">有什么的</label></button>
			<button><label class="label label-default">其实没什么的</label></button>
			<button><label class="label label-default">无聊的</label></button>
		</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-1">
			<button type="button" class="btn btn-primary btn-lg btn-block">徒步</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">骑行</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">自驾</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">混合</button>
		</div>
		<div class="col-sm-6">
			<div class="container">
			<label>活动行程计划区</label>
				<div id="myCarousel" class="carousel slide" >
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				    <div class="active item">
					   		
					   		<div style="position:relative;width:530px;height:360px;border:1px solid red;float:left;">
								<img src="/assets/data/upload/files/1/image/photo/2014/05/19/33675570_1276082644PEm7.jpg" style="width:530px;">
								<div style="position:absolute;top:10px;left:10px; padding:10;background-color:rgba(40%,40%,40%,0.6)" class="text-">
									<h5 class="text-primary">
							   		已经记不清楚自己是从什么时候开始，喜欢上了登山。
									也许是出自天性中的不安分因子，
									也许是现实中的人们终须有一个自己认可的人生出口。
									也许根本不用也许
									很多人问过我同样的问题：“登山这么累这么苦，这么危险。为什么还要坚持？”
									怎么去回答呢？
									我想，对于这样的问题，答案一定是千千万万的。
									我只是觉得
									喜欢一个人，或者一件事情，是不需要理由的。如果不喜欢，一定会有千万个理由。
									</h5>
								</div>
							</div>
					   		
					   		
				    </div>
				    <div class="item">
				    	<div style="background:url(http://img01.taobaocdn.com/imgextra/i1/15833137/T2I57sXj4aXXXXXXXX_!!15833137.jpg); height:360px">
				    	</div>
				    </div>
				    <div class="item" >
				    	
				    	<div style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/1.jpeg);">
				    		<label >筹备中的行程：</label>
				    		<ul>
							    <li class="text-primary"><strong>大鹏之旅7月游 7月12日 by 深圳的孩子</strong></li>
							    <li class="text-primary"><strong>小狐厦门之行 8月15日 by 小狐</strong></li>
							    <li class="text-primary"><strong>欧洲浪漫七天 8月4日 by 枫叶的思念</strong></li>
							    <li class="text-primary"><strong>走向西川 07/09 by 孤寂西川</strong></li>
							    <li class="text-primary"><strong>新西兰随处是风景 2015/02/29 by 小菲</strong></li>
							    <li class="text-primary"><strong>坐上火车去拉萨 08/09 by 小白</strong></li>
							    <li class="text-primary"><strong>滇藏骑行 07/09 by 落客</strong></li>
							    <li class="text-primary"><strong>走近撒哈拉 08/27 by 骆驼的脚步</strong></li>
							    <li class="text-primary"><strong>去往悄江南 05/21 by 女儿红</strong></li>
							    <li class="text-primary"><strong>云南篇之暴走雨崩 07/15 by 驴子</strong></li>
							    <li class="text-primary"><strong>环骑台湾岛 06/09 by 四小分队</strong></li>
							    <li class="text-primary"><strong>内蒙大草原 品鲜马奶酒 7/23 by 小娅</strong></li>
							    <li class="text-primary"><strong>开路七娘山 06/09 by 天涯浪子</strong></li>
							</ul>
				    	</div>
					    
				    </div>
				    <div class="item">
				    	<div style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg); height:360px">
				    	</div>
				    
					</div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#myCarousel" data-slide="prev">‹</a>
				  <a class="carousel-control right" href="#myCarousel" data-slide="next">›</a>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
		<label>招募中的活动</label>
			<ul class="list-unstyled">
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>158</span></small> <a class="text-info" href="#">还是家乡美 </a> <span class="text-danger">by</span> <span class="text-primary">小盆地</span>  <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1345</span></small> <a class="text-info" href="#">没有花儿香，没有树高，我是一棵无人知道的小草 </a> <span class="text-danger">by</span> <span class="text-primary">小草</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1558</span></small> <a class="text-info" href="#">从我这看，月亮是蓝的 </a> <span class="text-danger">by</span> <span class="text-primary">蓝月亮</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1258</span></small> <a class="text-info" href="#">开往索马里的地铁开通了 </a> <span class="text-danger">by</span> <span class="text-primary">电子琴</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>158</span></small> <a class="text-info" href="#">赐予我力量吧，我是希瑞 </a> <span class="text-danger">by</span> <span class="text-primary">希瑞公主</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>18</span></small> <a class="text-info" href="#">要飞五百光年才能见到我 </a> <span class="text-danger">by</span> <span class="text-primary">大象</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>128</span></small> <a class="text-info" href="#">附近没有星门 </a> <span class="text-danger">by</span> <span class="text-primary">Elen</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1534</span></small> <a class="text-info" href="#">遇见你是我最大的幸福 </a> <span class="text-danger">by</span> <span class="text-primary">kylinboy</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>15676</span></small> <a class="text-info" href="#">保护你是我的使命 </a> <span class="text-danger">by</span> <span class="text-primary">angel</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1587</span></small> <a class="text-info" href="#">我是中国人 </a> <span class="text-danger">by</span> <span class="text-primary">Bruce Lee</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>1385</span></small> <a class="text-info" href="#">飞吧飞吧 </a> <span class="text-danger">by</span> <span class="text-primary">云</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>12</span></small> <a class="text-info" href="#">还是家乡美 </a> <span class="text-danger">by</span> <span class="text-primary">kylinboy</span> <a class="text-success" href="#"><b>Go</b></a></li>
				<li ><small><span class="badge"><a href="#" class="text-danger">赞</a>150</span></small> <a class="text-info" href="#">还是家乡美 </a> <span class="text-danger">by</span> <span class="text-primary">kylinboy</span> <a class="text-success" href="#"><b>Go</b></a></li>
			</ul>
		</div>
		<div class="col-sm-1">
			<button type="button" class="btn btn-primary btn-lg btn-block">队伍</button>
			<button type="button" class="btn btn-primary btn-lg btn-block">散兵</button>
		</div>
	</div>
<hr/>
</div>


<div class="container" id="body_bottom">
	<#--
	<div class="row">
		<div class="col-md-12">
			<div class="btn-group btn-group-justified">
			  <a herf="#tomorrow_depart" class="btn btn-default active"><label class="text-primary">“明天”就出发</label></a>
			  <a herf="#depart_plan" class="btn btn-default"><label class="text-primary">结伴出行计划</label></a>
			  <a herf="#" class="btn btn-default active"><label class="text-primary">摄影篇</label></a>
			  <a herf="#" class="btn btn-default"><label class="text-primary">视频篇</label></a>
			</div>
	-->
			<#--<@specialAreaListIndex specialAreaList=specialAreaList/>-->
	<#--		
		</div>
	</div>
	-->
	<div class="row">
		<div class="col-md-6">
			<ul class="nav nav-tabs nav-justified">
			  <li><a href="#tomorrow_depart" data-toggle="tab"><label class="text-primary">“明天”就出发</label></a></li>
			  <li><a href="#depart_plan" data-toggle="tab"><label class="text-primary">结伴出行计划</label></a></li>
			</ul>
			<div class="tab-content">
					<div class="tab-pane active" id="tomorrow_depart">
						<ul class="media-list" >
										 <li class="media">
										 	<a class="pull-left" href="#">
										      <span class="glyphicon glyphicon-flag"></span>
										      <span class="glyphicon glyphicon-flag"></span> 
										      <span class="glyphicon glyphicon-flag"></span>
										    </a>
										    <div class="media-body">
											      <div class="row">
												      <div class="col-md-8" >
													      <span class="text-primary text-center">即将开始的旅行</span> 
													   </div>
													   <div class="col-md-1" >
													       <span class="text-danger">赞</span> 
													   </div>
													   <div class="col-md-1" >
													       <span class="text-danger">看</span>
													   </div>
													   <div class="col-md-2" >
													       <span class="text-danger">评</span>
													   </div> 
											       </div>
										     </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">花满楼</span> <span class="text-danger">1天后</span> 西双版纳之约，08/15
												      </h5>
												       <span class="text-info">我跟西双版纳有个约会</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一个" href="#">158</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="看一下" href="#">209</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="评一下" href="#">56</a>
												    </div> 
												  </div>  
										    </div>
										  </li>
										   <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      	<div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">在天边上</span> <span class="text-danger">3天后</span> 前往佛罗伦萨 06/13
												      </h5>
												       <span class="text-info">只为心中的佛罗伦萨</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一个" href="#">15</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="看一个" href="#">20</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="评一个" href="#">5</a>
												    </div> 
												  </div> 
										      
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">西门吹雪</span> <span class="text-danger">4天后</span> 走断千山万水-威尼斯 06/13
												      </h5>
												       <span class="text-info">两条腿，踏过千山万水也不累，纵不回，也在他乡交汇</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一个" href="#">1532</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="看一个" href="#">2023</a>
												   </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="评一个" href="#">532</a>
												    </div> 
												  </div> 
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">陆小凤</span> <span class="text-danger">5天后</span> 大漠探宝 06/13
												      </h5>
												       <span class="text-info">鸟一对，在天空中相会，酒一杯，各自天南地北</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一个" href="#">156</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="看一个" href="#">18</a>
												   </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="评一个" href="#">35504</a>
												    </div> 
												  </div> 
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">神马都是浮云</span> <span class="text-danger">6天后</span> 美国加州 06/13
												      </h5>
												       <span class="text-info">加州也就一浮云</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一个" href="#">150</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="看一个" href="#">200</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="评一个" href="#">50</a>
												    </div> 
												 </div> 
										    </div>
										  </li>
										  <li class="text-center">
										  <a href="#">查看更多....</a>
										  </li>
						</ul>
					</div>
				
					<div class="tab-pane" id="depart_plan">
						<ul class="media-list">
										 <li class="media">
										 	<a class="pull-left" href="#">
										      <span class="glyphicon glyphicon-flag"></span>
										      <span class="glyphicon glyphicon-flag"></span> 
										      <span class="glyphicon glyphicon-flag"></span>
										    </a>
										    <div class="media-body">
											      <div class="row">
												      <div class="col-md-8" >
													      <span class="text-primary text-center">正在邀约的旅行</span> 
													   </div>
													   <div class="col-md-1" >
													       <span class="text-danger">赞</span> 
													   </div>
													   <div class="col-md-1" >
													       <span class="text-danger">邀</span>
													   </div>
													   <div class="col-md-2" >
													       <span class="text-danger">关注</span>
													   </div> 
											       </div>
										     </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">花满楼</span> <span class="text-danger">@</span> 西双版纳之约，08/15
												      </h5>
												       <span class="text-info">我跟西双版纳有个约会</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一下" href="#">158</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="邀请搭伴人数" href="#">209</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="关注行程" href="#">56</a>
												    </div> 
												  </div>  
										    </div>
										  </li>
										   <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      	<div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">在天边上</span> <span class="text-danger">@</span> 前往佛罗伦萨 06/13
												      </h5>
												       <span class="text-info">只为心中的佛罗伦萨</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一下" href="#">15</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="邀请搭伴人数" href="#">20</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="关注行程" href="#">5</a>
												    </div> 
												  </div> 
										      
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">西门吹雪</span> <span class="text-danger">@</span> 走断千山万水-威尼斯 06/13
												      </h5>
												       <span class="text-info">两条腿，踏过千山万水也不累，纵不回，也在他乡交汇</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一下" href="#">1532</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="邀请搭伴人数" href="#">2023</a>
												   </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="关注行程" href="#">532</a>
												    </div> 
												  </div> 
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">陆小凤</span> <span class="text-danger">@</span> 大漠探宝 06/13
												      </h5>
												       <span class="text-info">鸟一对，在天空中相会，酒一杯，各自天南地北</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一下" href="#">156</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="邀请搭伴人数" href="#">18</a>
												   </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="关注行程" href="#">35504</a>
												    </div> 
												  </div> 
										    </div>
										  </li>
										  <li class="media">
										    <a class="pull-left" href="#">
										      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
										    </a>
										    <div class="media-body">
										      <div class="row">
											      <div class="col-md-8" >
												      <h5 class="media-heading">
												      <span class="text-primary">神马都是浮云</span> <span class="text-danger">@</span> 美国加州 06/13
												      </h5>
												       <span class="text-info">加州也就一浮云</span>
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="赞一下" href="#">150</a> 
												   </div>
												   <div class="col-md-1" >
												       <a class="text-danger" title="邀请搭伴人数" href="#">200</a>
												        </div>
												   <div class="col-md-2" >
												       <a class="text-danger" title="关注行程" href="#">50</a>
												    </div> 
												 </div> 
										    </div>
										  </li>
										  <li class="text-center">
										  <a href="#">查看更多....</a>
										  </li>
						</ul>
					</div>
				</div>
			
		</div>
		<div class="col-md-6">
			<ul class="nav nav-tabs nav-justified">
			  <li><a href="#photo_graph" data-toggle="tab"><label class="text-primary">室内活动</label></a></li>
			  <li><a href="#vedio" data-toggle="tab"><label class="text-primary">户外活动</label></a></li>
			</ul>
			<div class="tab-content">
					<div class="tab-pane active" id="photo_graph">
						<a href="#">
					      <span class="glyphicon glyphicon-flag"></span>
					      <span class="glyphicon glyphicon-flag"></span> 
					      <span class="glyphicon glyphicon-flag"></span>
					      室内活动
					    </a>
						<div class="row">
						     <div class="col-md-8 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='365' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 陆小凤</div>
								    </a>
							  </div>
							  <div class="col-md-4 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='170' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 斗转星移</div>
								    </a>
							  </div>
					    </div>
					   
					    <div class="row">
						      <div class="col-md-4 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='170' height="125"  class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 神马都是浮云</div>
								    </a>
							  </div>
							  <div class="col-md-8 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='365' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 西川</div>
								    </a>
							  </div>
					    </div>
					    <div class="row">
						      <div class="col-md-12 text-center" >
						      		<a href="#">查看更多....</a>
						      </div>
					    </div>
					</div>
					<div class="tab-pane" id="vedio">
						<a href="#">
					      <span class="glyphicon glyphicon-flag"></span>
					      <span class="glyphicon glyphicon-flag"></span> 
					      <span class="glyphicon glyphicon-flag"></span>
					       户外活动
					    </a>
						<div class="row">
						     <div class="col-md-8 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='365' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 陆小凤</div>
								    </a>
							  </div>
							  <div class="col-md-4 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='170' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 斗转星移</div>
								    </a>
							  </div>
					    </div>
					   
					    <div class="row">
						      <div class="col-md-4 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='170' height="125"  class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 神马都是浮云</div>
								    </a>
							  </div>
							  <div class="col-md-8 text-center" >
							      	<a class="pull-left" href="#">
								    <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='365' height="125" class="media-object" title="落日长河 by 西川" alt="落日长河">
								    <div>落日长河 by 西川</div>
								    </a>
							  </div>
					    </div>
					    <div class="row">
						      <div class="col-md-12 text-center" >
						      		<a href="#">查看更多....</a>
						      </div>
					    </div>
					</div>
			 </div>
		</div>
	</div>
	
	<br/>
	<div class="row">
		<div class="col-md-6">
  		</div>
  		<div class="col-md-6">
  			
  		</div>
  		
	</div>
	
	<div class="row">
			
			
		     <div class="col-md-7">
		     		<div class="panel panel-default">
					 	  <div class="panel-heading"><strong>最新发布【按最近时间顺序发布的活动】</strong></div>
						  <div class="panel-body" >
						  		<table>
							  		<tr>
								  		<td>
									  		<ul class="media-list">
											  <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">江湖浪子</span> <span class="text-danger">10秒前</span></h5>
											       <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">小榕树</span> <span class="text-danger">39秒前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">我来了</span> <span class="text-danger">1分钟前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">谁知道</span> <span class="text-danger">5分钟前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											</ul>
											
										</td>
										<td>
											<ul class="media-list">
											  <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object " src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">神经病F5</span> <span class="text-danger">10秒前</span></h5>
											       <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">撒哈拉_^_</span> <span class="text-danger">39秒前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">谁也不在乎</span> <span class="text-danger">1分钟前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											      <h5 class="media-heading"><span class="text-primary">Bruce Lee</span> <span class="text-danger">5分钟前</span></h5>
											      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    </div>
											  </li>
											</ul>
										</td>
									</tr>
									<tr>
									<td colspan='2'><p><a href="#" class="text-danger">加个广告在这能影响什么呢？谁还说什么了</a></p></td>
									<tr>
								</table>
						   </div>
				     </div>
		     </div>
		     <div class="col-md-5">
					<div class="panel panel-default">
					 	  <div class="panel-heading"><strong>行程关注排行榜【他们都在关注】</strong></div>
						  <div class="panel-body">
								<ul class="media-list">
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span> 已被<span class="text-danger">1234</span>人次关注 最后关注 <span class="text-danger">by</span>
								      <span class="text-primary">在天边上</span> 
								       
								      </h5>
								    </div>
								  </li>
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
								       已被<span class="text-danger">1224</span>人次关注 最后关注 <span class="text-danger">by</span>
								      <span class="text-primary">神马都是浮云</span> 
								      </h5>
								    </div>
								  </li>
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">五月初五 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
								         已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
								      	 <span class="text-primary">小葡萄</span> 
								      </h5>
								    </div>
								  </li>
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">07/07 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
								       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
								      	 <span class="text-primary">@梦@</span> 
								      </h5>
								    </div>
								  </li>
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">06/18 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
								       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
								      	 <span class="text-primary">小榕树</span> 
								      </h5>
								    </div>
								  </li>
								  <li class="media">
								    <a class="pull-left" href="#">
								      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="...">
								    </a>
								    <div class="media-body">
								      <h5 class="media-heading"><span class="text-info">10/14 东莞松江湖骑行骑行骑行骑行骑行骑行骑行骑行</span>
								       已被<span class="text-danger">124</span>人次关注 最后关注 <span class="text-danger">by</span>
								      	 <span class="text-primary">江湖浪子</span> 
								      </h5>
								    </div>
								  </li>
								</ul>
						   </div>
				     </div>
					
				
		     </div>
	</div>
	
	<div class="row">
			
			<div class="col-md-7">
				
					<div class="panel panel-default">
					 	  <div class="panel-heading"><strong>旅行达人行程【下一站】</strong></div>
						  <div class="panel-body" >
						  
							  <div class="panel-group" id="accordion">
								  <div class="panel panel-default">
								    <div class="panel-heading">
								      <p class="panel-title">
								        <a data-toggle="collapse" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
								          <small>站在西川之尽头，迎着夕阳撒下的余辉，此时此景，我才真正感受到我这一路的辛苦没有白费 by 西川 23:55 【一路向西】</small>
								        </a>
								      </p>
								    </div>
								    <div id="collapseOne" class="panel-collapse collapse in">
								      <div class="panel-body">
								      	<ul class="list-inline">
									        	<li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											    <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
									    </ul>
								      </div>
								    </div>
								  </div>
								  <div class="panel panel-default">
								    <div class="panel-heading">
								      <p class="panel-title">
								        <a data-toggle="collapse" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
								          <small>站在西川之尽头，迎着夕阳撒下的余辉，此时此景，我才真正感受到我这一路的辛苦没有白费 by 西川 23:55 【一路向西】</small>
								        </a>
								      </p>
								    </div>
								    <div id="collapseFour" class="panel-collapse collapse">
								      <div class="panel-body">
								      	<ul class="list-inline">
									        	<li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											    <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
									    </ul>
								      </div>
								    </div>
								  </div>
								  <div class="panel panel-default">
								    <div class="panel-heading">
								      <p class="panel-title">
								        <a data-toggle="collapse" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
								          <small>站在西川之尽头，迎着夕阳撒下的余辉，此时此景，我才真正感受到我这一路的辛苦没有白费 by 西川 23:55 【一路向西】</small>
								        </a>
								      </p>
								    </div>
								    <div id="collapseTwo" class="panel-collapse collapse">
								      <div class="panel-body">
									      <ul class="list-inline">
										        <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											    <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
										    </ul>
								      </div>
								    </div>
								  </div>
								  
								  <div class="panel panel-default">
								    <div class="panel-heading">
								      <p class="panel-title">
								        <a data-toggle="collapse" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
								         <small> 站在西川之尽头，迎着夕阳撒下的余辉，此时此景，我才真正感受到我这一路的辛苦没有白费 by 西川 23:55 【一路向西】</small>
								        </a>
								      </p>
								    </div>
								    <div id="collapseThree" class="panel-collapse collapse">
								      <div class="panel-body">
								       		<ul class="list-inline">
										        <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											     <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
											    <li>
											      	<a href="#" >
												      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" alt="..." width="100px" height="75px" class="img-responsive">
												    </a>
											    </li>
										    </ul>
								      </div>
								    </div>
								  </div>
								</div>
						  
						  
						  
						  
						  </div> 
					</div> 
				
		     </div>
		     <div class="col-md-5">
		     		<div class="panel panel-default">
					 	  <div class="panel-heading"><strong>行程宣言【下一程】<!--【来自地区，昵称，行程标题，时间，宣言】--></strong></div>
						  <div class="panel-body">
						  	<ul class="list-unstyled">
						  	<li><b>【塔里木】<b> 的 <span class="text-primary">小盆地</span> <span class="text-danger">新疆的路上 06/19</span> 说：还是家乡美</li>
						  	<li><b>【江苏】</b> 的 <span class="text-primary">小草</span> <span class="text-danger">撒哈拉的骆驼 06/19</span> 说：没有花儿香，没有树高，我是一棵无人知道的小草</li>
						  	<li><b>【火星】</b> 的 <span class="text-primary">蓝月亮</span> <span class="text-danger">东海游记 06/19</span> 说：从我这看，月亮是蓝的</li>
						  	<li><b>【爪哇岛】</b> 的 <span class="text-primary">精灵斗士</span> <span class="text-danger">环骑台湾岛 06/20</span> 说：自由行</li>
						  	<li><b>【乌拉草原】</b> 的 <span class="text-primary">蓝月亮</span> <span class="text-danger">香港回忆 08/23</span> 说：香港是一部旧电影/li>
						  	<li><b>【索马里】</b> 的 <span class="text-primary">电子琴</span> <span class="text-danger">索马里的路途 09/21</span> 说：开往索马里的地铁开通了</li>
						  	<li><b>【天之端】</b> 的 <span class="text-primary">云</span> <span class="text-danger">第一天堂 09/21</span> 说：飞吧飞吧</li>
						  	<li><b>【England】</b> 的 <span class="text-primary">希瑞公主</span> <span class="text-danger">魔鬼的城堡冒险 06/30</span> 说：赐予我力量吧，我是希瑞</li>
						  	<li><b>【命运号】</b> 的 <span class="text-primary">Elen</span> <span class="text-danger">超光速的命运号 06/30</span> 说：附近没有星门</li>
						  	<li><b>【小天体】</b> 的 <span class="text-primary">没完没了</span> <span class="text-danger">超级玛丽王国历险记 06/30</span> 说：我要撞到地球了</li>
						  	<li><b>【室女座】</b> 的 <span class="text-primary">大象</span> <span class="text-danger">织女星的奇迹 07/07</span> 说：要飞五百光年才能见到我</li>
						  	<li><b>【阿拉加斯加】</b> 的 <span class="text-primary">angel</span> <span class="text-danger">南半球的国度 07/07</span> 说：保护你是我的使命</li>
						  	<li><b>【西雅图】</b> 的 <span class="text-primary">Bruce Lee</span> <span class="text-danger">周边转转 07/07</span> 说：我是中国人</li>
						  	</ul>
						  </div> 
					</div> 
		     </div>
	</div>
	
	
	<div class="row">
		<div class="col-md-12">
		<!--每日选 5-10篇旅行达人的精彩文章在这里展示，要配有背景至少两张图，轮播展示，背景图尺寸要满足系统要求-->
				<div id="travelDeepCarousel" class="carousel slide" >
				  <!-- Carousel items -->
				  <div class="carousel-inner">
				  	<div class="active item">
						<img alt="" src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" height="300px" class="img-responsive"/>
						<div class="carousel-caption">
							<h4>
								冲浪
							</h4>
							<p>
								冲浪是以海浪为动力，利用自身的高超技巧和平衡能力，搏击海浪的一项运动。运动员站立在冲浪板上，或利用腹板、跪板、充气的橡皮垫、划艇、皮艇等驾驭海浪的一项水上运动。
							</p>
						</div>
					</div>
					
				    <div class="item">
					   		<div class="jumbotron"style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg); height:380px">
							  <h1>西川的旅行</h1>
							  <p>在这里，随便挑一个地方都胜得过黄果树....</p>
							  <p><a class="btn btn-primary btn-lg" role="button">深入探索...</a></p>
							</div>
				    </div>
				    <div class="item">
				    	<div class="jumbotron" style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg); height:380px">
						  <h1>厦门爱情日记</h1>
						  <p>邂逅了一段美好的爱情，从此不再孤单....</p>
						  <p><a class="btn btn-primary btn-lg" role="button">仔细品味...</a></p>
						</div>
				    </div>
				    <div class="item" >
				    	
				    	<div class="jumbotron" style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/1.jpeg);">
				    		 <h1>雨中漫步大理</h1>
						  	 <p>安静是一种让人享受的环境....</p>
						  	 <p><a class="btn btn-primary btn-lg" role="button">仔细品味...</a></p>
				    	</div>
					    
				    </div>
				    <div class="item">
				    	<div style="background:url(/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg); height:350px">
				    	田
				    	</div>
				    
					</div>
				  </div>
				  <!-- Carousel nav -->
				  <a class="carousel-control left" href="#travelDeepCarousel" data-slide="prev">‹</a>
				  <a class="carousel-control right" href="#travelDeepCarousel" data-slide="next">›</a>
				</div>
			</div>
			
	</div>
	
	
	
	
	
	<div class="row">
	  <div class="col-md-6">
					<div class="panel panel-default">
					 	  <div class="panel-heading">【队伍】兵团协作行【计划】</div>
						  <div class="panel-body">
						  
						  				<ul class="media-list">
											  <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											    	<div class="row">
											    	<div class="col-md-10">
												      	<h5 class="media-heading"><span class="text-primary">摩登小队</span> <span class="text-danger">on 07/15</span> ZG:长江 摄影师:江小鱼 成员：13人 
												        </h5>
												        <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											       </div>
											       <div class="col-md-2">
											       		<!--队旗-->
											       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='60' height='45' class="media-object" alt="队旗">
											       </div>
											       </div>
											    </div>
											   
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											    	<div class="row">
											    	<div class="col-md-10">
											      		<h5 class="media-heading"><span class="text-primary">小榕树</span> <span class="text-danger">on 06/13</span>  ZG:小榕树 摄影师:小榕树 成员：5人 </h5>
											      		<span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    	</div>
											       <div class="col-md-2">
											       		<!--队旗-->
											       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='60' height='45' class="media-object" alt="队旗">
											       </div>
											       </div>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
												      <div class="row">
											    	  <div class="col-md-10">
														      <h5 class="media-heading"><span class="text-primary">三人行</span> <span class="text-danger">on 06/18</span> ZG:我来了 摄影师:小榕树 成员：3人</h5>
														      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
												      </div>
											          <div class="col-md-2">
												       		<!--队旗-->
												       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='60' height='45' class="media-object" alt="队旗">
												       </div>
												       </div>
											    
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											   		  <div class="row">
											    	  <div class="col-md-10">
													      <h5 class="media-heading"><span class="text-primary">小猫队</span> <span class="text-danger">at 06/22</span> ZG:谁知道 摄影师:小榕树 成员：3人</h5>
													      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
													      </div>
											          <div class="col-md-2">
												       		<!--队旗-->
												       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='60' height='45' class="media-object" alt="队旗">
												       </div>
												       </div>
											    </div>
											  </li>
										</ul>
						  </div> 
					</div> 
	  </div>
	  <div class="col-md-6">
					<div class="panel panel-default">
					 	  <div class="panel-heading">特种【散兵】自由行【计划】</div>
						  <div class="panel-body">
						  			<ul class="media-list">
											  <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											    	<div class="row">
											    	<div class="col-md-10">
												      	<h5 class="media-heading"><span class="text-primary">长江</span> <span class="text-danger">on 10/14 </span> 【火焰山的旅行】
												        </h5>
												        <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											       </div>
											       <div class="col-md-2">
											       		<!--队旗-->
											       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="头像">
											       </div>
											       </div>
											    </div>
											   
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" src="..." alt="...">
											    </a>
											    <div class="media-body">
											    	<div class="row">
											    	<div class="col-md-10">
											      		<h5 class="media-heading"><span class="text-primary">小榕树</span> <span class="text-danger">on 10/14</span> 【火焰山的旅行】</h5>
											      		<span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
											    	</div>
											       <div class="col-md-2">
											       		<!--队旗-->
											       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="头像">
											       </div>
											       </div>
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
												      <div class="row">
											    	  <div class="col-md-10">
														      <h5 class="media-heading"><span class="text-primary">我来了</span> <span class="text-danger">on 10/14</span> 【火焰山的旅行】</h5>
														      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
												      </div>
											          <div class="col-md-2">
												       		<!--队旗-->
												       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="头像">
												       </div>
												       </div>
											    
											    </div>
											  </li>
											   <li class="media">
											    <a class="pull-left" href="#">
											      <img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='80' height='60' class="media-object" alt="...">
											    </a>
											    <div class="media-body">
											   		  <div class="row">
											    	  <div class="col-md-10">
													      <h5 class="media-heading"><span class="text-primary">谁知道</span> <span class="text-danger">on 10/14</span> 【火焰山的旅行】</h5>
													      <span class="text-info"><small>看着这涌动的火山，心血跟着沸腾沸腾沸腾沸腾沸腾.... </small></span>
													      </div>
											          <div class="col-md-2">
												       		<!--队旗-->
												       		<img src="/assets/data/upload/files/1/image/photo/2014/05/19/2.jpg" width='40' height='40' class="media-object img-circle" alt="头像">
												       </div>
												       </div>
											    </div>
											  </li>
										</ul>
						  
						  </div> 
					</div> 
	  </div>
	  
	</div>
	
	

</div>







<#--
<div class="container" id="body_bottom">
	<div class="row">
		<div class="col-md-12">
			<span class="label label-default">Default</span>
			<span class="label label-primary">Primary</span>
			<span class="label label-success">Success</span>
			<span class="label label-info">Info</span>
			<span class="label label-warning">Warning</span>
			<span class="label label-danger">Danger</span>
		</div>
	</div>
</div>
-->
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</body>
</html>