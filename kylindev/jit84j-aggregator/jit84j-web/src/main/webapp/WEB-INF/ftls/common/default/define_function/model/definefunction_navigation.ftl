
<#macro asignChildrenNavigationCheckbox children currentNavigation checkboxName="childrenNavigation">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>- - </#list>
            <#if currentNavigation?? && currentNavigation.id!=child.id>
	            <input type="checkbox" value="${child.uniqueIdentifier}" name="${checkboxName}[${child_index}].id" id="navigation_${child.uniqueIdentifier}"
	            	<#if currentNavigation?? && child.parent?? && currentNavigation.id==child.parent.id> checked </#if>
	            />
            </#if>
            
            <#if currentNavigation?? && currentNavigation.id==child.id>
	             	<font color="red">
	         </#if>
             ${(child.name)?if_exists}
             <#if child.hideable>
             	☀︎
             	<#else>
             	☼
             </#if>
             <#if currentNavigation?? && currentNavigation.id==child.id>
	             	</font>
	         </#if>
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenNavigation?? && child.childrenNavigation?size gt 0>  
            <@asignChildrenNavigationCheckbox children = child.childrenNavigation currentNavigation=currentNavigation checkboxName=checkboxName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
        
    </#if>  	
</#macro> 


<#--保存类别-->
<#macro persistNavigation children current>

<form action="/admin/navigation/addChilrenNavigation" id="addChilrenNavigation" method="post">
<input type="hidden" name="currentNavigationId" value="current.id">为[${current.name}]分配子类:<br/>
<@buildNodeCheckbox children = children checkboxName="childrenNavigationIds"/>  
<input type="submit" value="保存">
<input type="reset" value="重置">
</form>
</#macro> 



<#--选择父类-->
<#macro selectParentNavigationRadio children currentNavigation radioName="parent.id">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>- - </#list>
            <#if currentNavigation?? && currentNavigation.id!=child.id>
            	<input type="Radio" value="${child.uniqueIdentifier}" name="${radioName}" id="select_parent_navigation_${child.uniqueIdentifier}" <#if currentNavigation?? && currentNavigation.parent?? && currentNavigation.parent.id == child.id> checked</#if>/>
            </#if>
	             <#if currentNavigation?? && currentNavigation.id==child.id>
	             	<font color="red">
	             </#if>
             ${(child.name)?if_exists} 
             <#if child.hideable>
             	☀︎
             	<#else>
             	☼
             </#if>
	             <#if currentNavigation?? && currentNavigation.id==child.id>
	             	</font>
	             </#if>
             
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenNavigation?? && child.childrenNavigation?size gt 0>  
            	<@selectParentNavigationRadio children = child.childrenNavigation currentNavigation=currentNavigation radioName=radioName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro>

<#macro adminNavigation navigationList currentNavigation='default'>
<nav class="navbar navbar-default" role="navigation" style="z-index:200">
	  <!-- Collect the nav links, forms, and other content for toggling -->
	  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2" style="z-index:200">
	    <ul class="nav navbar-nav">
	   <#if (navigationList??)>
	   	<#list navigationList as navigation> 
		   	<#if navigation?? && !navigation.hideable>
			   	  <#if !(navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0)>
			      	 <li ><#if !navigation.disable><a href="${navigation.url}"></#if>${navigation.name}<#if !navigation.disable></a></#if></li>
			      </#if>
			      <#if navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0>
			      	<li class="dropdown">
			      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">${navigation.name}
			      	<b class="caret"></b></a>
			      	<#assign depth = 0/>
			      	<@adminNavigationDropdown navigationList=navigation.childrenNavigation/>
			      	</li>
			      </#if>
			  </#if>
	     </#list>
	    </#if>
	    </ul>
	    <#--
	    <form class="navbar-form navbar-right" role="search">
	      <div class="form-group">
	        <input type="text" class="form-control" placeholder="Search">
	      </div>
	      <button type="submit" class="btn btn-default">Submit</button>
	    </form>
	    -->
	  </div>
	</nav>
	<#if (currentNavigation?? && currentNavigation !='default')>
		<@breadcrumbNavigation currentNavigation=currentNavigation/>
	</#if>
</#macro>

<#macro adminNavigationDropdown navigationList >
	<ul <#if depth = 0>class="dropdown-menu" </#if>>
	
	  	<#list navigationList as navigation> 
		  	<#if navigation?? && !navigation.hideable>
			  	<#assign depth = depth + 1 />
			  		<li><#if !navigation.disable><a href="${navigation.url}"></#if>${navigation.name}<#if !navigation.disable></a></#if></li>
			  		 <#if navigation.childrenNavigation?? && navigation.childrenNavigation?size gt 0>
			  		 	 <@adminNavigationDropdown navigationList=navigation.childrenNavigation/>
			  		 </#if>
			  	<#assign depth = depth - 1 />
			  		 <li class="divider"></li>
		  	</#if>
	  	</#list>
	  	
  	</ul>
</#macro>

<#macro breadcrumbNavigation currentNavigation>
<#if currentNavigation??>
	<ol class="breadcrumb-small">
	<li>位置:</li>
	<#if currentNavigation.parent??>
	  <@breadcrumbNavigationItem currentNavigation.parent/>
	</#if>
	  <li class="active">${currentNavigation.name}</li>
	</ol>
</#if>
</#macro>

<#macro breadcrumbNavigationItem parentNavigation>
<#if parentNavigation??>
	<#if parentNavigation.parent??>
		<@breadcrumbNavigationItem parentNavigation.parent/>
	</#if>
	<li><#if !parentNavigation.disable><a href="${parentNavigation.url}"></#if>${parentNavigation.name}<#if !parentNavigation.disable></a></#if></li>
</#if>
</#macro>

<#macro listNavigationTree navigationTopList currentNavigation>
<div class="table-responsive">
<table class="table table-bordered">
<tr>
<td>颜色标识：</td>
 <#assign trClass = '' />
<#list 1..4 as dep>
<@getClassColor dep/>
<td  ${trClass}>${dep}级菜单</td>
</#list>
</tr>
</table>
  <table class="table">
  <thead>
    <tr>
      <th width="2%">Id</th>
      <th width="5%">名称</th>
      <th width="8%">代码</th>
      <th width="8%">图标</th>
      <th width="15%">路径</th>
      <th width="3%">排序</th>
      <th width="5%">显示</th>
      <th width="5%">开发者ID</th>
      <th width="15%">操作</th>
    </tr>
  </thead>
  <@form.form commandName="navigationList" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_LIST_MODIFY_MAPPING}">
  <tbody>
  <#if (navigationTopList??)>
  	 <#assign depth = 0 />  
  	 <#assign countNum = 0 /> 
	<@listNavigationTreeItem navigationTopList=navigationTopList/>
  </#if>
	<tr>
		<td>
		<#if (navigationTopList??)>
			<input type="submit" value="保存列表" class="btn btn-primary"/>
		</#if>
		</td>
		<td>
			<a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_ADD_UI_MAPPING}'>
				<input type="button" value="添加导航" class="btn btn-primary"/>
			</a>
		</td>
		<td>
			<input type="button" value="添加一行(TODO)" class="btn btn-primary"/>
		</td>
	 </tr>
   </tbody>
   </@form.form>
  </table>
</div>
</#macro>

<#macro listNavigationTreeItem navigationTopList>
	<#if navigationTopList??>
		<#list navigationTopList as navigation>  
			 <#assign depth = depth + 1 />
			 
			 
			 
			 <#assign trClass = '' />
			 <@getClassColor depth/>
			 
			 <tr ${trClass}>
			 <td>${navigation.uniqueIdentifier} <#list 1..depth as i> > </#list>
			 <input type="hidden" name="navigationList[${countNum}].id" value="${navigation.uniqueIdentifier}"/>
			 <img src="${navigation.iconPath}"/>
			 </td>
			 <td>
			 	<input type="text" size="3" name="navigationList[${countNum}].name" value="${navigation.name}" class="form-control" id="code[${countNum}]" placeholder="输入代码"/>
			 </td>
			 <td>
			 	<input type="text" size="3" name="navigationList[${countNum}].code" value="${navigation.code}" class="form-control" id="name[${countNum}]" placeholder="输入名称"/>
			 </td>
			 <td>
			 	<input type="text" size="3" name="navigationList[${countNum}].iconPath" value="${navigation.iconPath}" class="form-control" id="iconPath[${countNum}]" placeholder="输入图标路径"/>
			 </td>
			  <td>
			 	<input type="text" size="3" name="navigationList[${countNum}].url" value="${navigation.url}" class="form-control" id="url[${countNum}]" placeholder="输入导航路径"/>
			 </td>
			 <td>
			 	<input type="text" size="2" name="navigationList[${countNum}].sequence" value="${navigation.sequence}" class="form-control" id="sequence[${countNum}]" placeholder="站点路径"/>
			 </td>
			  <td>
			 	<input type="checkbox" name="navigationList[${countNum}].hideable" <#if navigation.hideable> checked </#if> class="form-control" id="hideable[${countNum}]" />
			 </td>
			  <td>
			 	<input type="text" size="3" name="navigationList[${countNum}].developer.userId.userId" value="${navigation.developer.userId.userId}" class="form-control" id="userId[${countNum}]" />
			 </td>
			 <td>
			 <ul class="list-inline"> 
				 <#if navigation.code?? && navigation.code!='top_Navigation'>
					<li>
				 	<a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_NAVIGATION_MODIFY_UI_MAPPING}${navigation.code}'><input type="button" value="编 辑" class="btn btn-primary"/></a>
				  	</li>
				  </#if>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/delete/${navigation.uniqueIdentifier}"/>'><input type="button" value="删 除" class="btn btn-primary"/></a>
				    </li>
				    <li>
				    <a class="text-danger" href='<@spring.url relativeUrl="/admin/menu/navigation/modify/${navigation.uniqueIdentifier}"/>'><input type="button" value="保 存" class="btn btn-primary"/></a>
				    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign countNum = countNum + 1 />
			 <#if navigation.childrenNavigation??>
			 	<@listNavigationTreeItem navigationTopList= navigation.childrenNavigation/>
			 </#if>
			 <#assign depth = depth-1 />
		</#list>
	</#if>
		
</#macro>

<#macro getClassColor depth>
	  <#switch depth>
	  <#case 1> <#assign trClass = 'class="success"' /> <#break>
	  <#case 2> <#assign trClass = 'class="warning"' /> <#break>
	  <#case 3> <#assign trClass = 'class="danger"' /> <#break>
	  <#case 4> <#assign trClass = 'class="active"' /> <#break>
	  <#default> <#assign trClass = '' />
	  </#switch>
</#macro>


<#macro currentNavigationButton currentNavigation>
<div class="row">
	<div class="col-md-12">
		<#if currentNavigation??>
			<div class="panel panel-warning" >
							<div class="panel-heading">${currentNavigation.name}</div>
							<ul class="list-inline">
								<#assign childrenNavigation=currentNavigation.childrenNavigation/>
								<#if childrenNavigation??>
									<#list childrenNavigation as child>
										<li>
										<a href="${child.url}" class="btn btn-primary" role="button">${child.name}</a>
										</li>
									</#list>
								</#if>
							
							</ul>
							注:执行此操作比较耗时
			</div>
		</#if>
	</div>
</div>
</#macro>

<#--同级水平按钮一字排开，传入当前导航，取父导航，再展开-->
<#macro horizontalNavigationUI currentNavigation isAjax='true'>
<div id="treeData_left_navigation" class="list-group">
<#if currentNavigation??>
		<#assign parentNavigation = currentNavigation.parent/>
		<#if parentNavigation?? && parentNavigation.childrenNavigation?? && parentNavigation.childrenNavigation?size gt 0>
		 <#assign navigationList = parentNavigation.childrenNavigation />  
			 <ul class="list-inline">
			  	<#list navigationList as navigation> 
				  	<#if navigation?? >
				  	<li>
				  	<#if isAjax=='true'>
					  	<a href="javascript:void(0)" class="list-group-item <#if currentNavigation.id==navigation.id>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}')">
					 		${navigation.name}
					 	</a>
				 	<#else>
					 	<a href="${navigation.url}" class="list-group-item <#if currentNavigation.id==navigation.id>active</#if>" id="left_navigation_${navigation.id}" >
					 		${navigation.name}
					 	</a>
				 	</#if>
				 	</li>
				  	</#if>
			  	</#list>
			  </ul>
		 </#if>
</#if>
</div>
</#macro>