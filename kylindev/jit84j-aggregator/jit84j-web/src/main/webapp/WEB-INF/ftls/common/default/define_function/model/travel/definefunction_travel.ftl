
<#macro listInstantShare page >
	<#if page?? && page.content??>
	<#assign instantShareList = page.content />
	<#if msg??><div class="alert alert-success" role="alert">${msg}</div></#if>
	<form id="instantShare_list_form" role="form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING}">

	 <table class="table">
	 <tr>
	 <th>
		 Id
		</th>
	 	<th>
		 标题
		</th>
		 <th>
		 发布人
		 </th>
		 <th>
		 发布时间
		 </th>
		 <th>
		 发布地
		 </th>
		 <th>
		 图片数
		 </th>
		 <th>
		 操作
		 </th>
	</tr>
	<#list instantShareList as instantShare>  
		 <tr>
		 <td>
			 ${instantShare.id}
			</td>
			<td>
			<a target="_blank" href="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING}/${instantShare.id}">${instantShare.title}</a>
			</td>
			 <td>
			 ${instantShare.nickName}
			 </td>
			 <td>
			 ${instantShare.publishDate}
			 </td>
			 <td>
			  ${instantShare.place}
			 </td>
			  <td>
			  ${instantShare.imageCount}
			 </td>
			 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_button_edit_${instantShare.id}" onclick="switchSetting('${instantShare.id}','button_edit','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT3_UI_MAPPING}','#instantShare_content_div','second_navigation_container')">编辑</a>
				  	</li>
				    <li>
				    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${instantShare.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_DELERE_MAPPING}','#instantShare_content_div')">删除</a>
				    </li>
				  </ul>
			  </td>
		 </tr>
	</#list>
		<tr>
			<td colspan="7">
			<@pageBarAjax page=page url="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_LIST2_UI_MAPPING}" formId='instantShare_list_form' backDataContainerId="instantShare_content_div"/>
			</td>
		</tr>
	</table>
	</form>
	</#if>
</#macro>


<#macro editInstantShare instantShare=null>
	<div class="row">
		<div class="col-md-6">
		     		<button class="btn btn-primary" onClick="submitInstantShareForm('#instantShare_form','#instantShare_content_div')">发布</button>
		     		<button class="btn btn-warning" onClick="cleanForm('#instantShare_form')">重填</button>
					<button class="btn btn-primary" data-toggle="modal" data-target="#uploadModal">
					  	上传图片
					</button>
					<button class="btn btn-primary" data-toggle="modal" data-target="#uploadModal2">
					  	选择已有图片
					</button>
		</div>
	</div>
	<br/>
	<form id="instantShare_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_EDIT2_MAPPING}" method="post">
	<div class="row">
			<div class="col-md-12 <#if msg??> alert alert-success </#if>" id="msg">
				${msg}
			</div>
	</div>
			
	<div class="row">
			<div class="col-md-9">
					<div id="instantShare_content_div">
					 </div>
					<input data='data' type="hidden" id="id" name="id" value="<#if instantShare??>${instantShare.id}</#if>"/>
				    <input data='data' type="text" name="title" value="<#if instantShare??>${instantShare.title}</#if>" class="form-control" id="instantShareTitleId" placeholder="说一句" required="true"/>
		     		<input data='data' type="hidden" value="${instantShare.batch}" name="batch" id="batch"/>
					       
			       <div class="form-group ">
					    <label class="control-label" for="description"> 
						</label>
						<textarea data='data' rows="5" id="description" name="description" value="<#if instantShare??>${instantShare.description}</#if>" class="form-control" id="description" placeholder="多说几句" ><#if instantShare??>${instantShare.description}</#if></textarea>
				  </div>
		   </div>
		   <div class="col-md-3">
			   <div id="city_1"> 
					    所在地：
					    <input type="hidden" id="tmp_province" name="tmp_province" value="<#if instantShare??>${instantShare.province}</#if>"/>
					    <input type="hidden" id="tmp_city" name="tmp_city" value="<#if instantShare??>${instantShare.city}</#if>"/>
					    <input type="hidden" id="tmp_mydistrict" name="tmp_mydistrict" value="<#if instantShare??>${instantShare.mydistrict}</#if>"/>
					    <select data='data' name="province" class="prov"></select> 
					    <select data='data' name="city" class="city"></select>
					    <br/>
					    <p>自定义发布地：
					    <input data='data' type="text" name="place" value="<#if instantShare??>${instantShare.place}</#if>" class="form-control" id="instantSharePlaceId" placeholder="自定义发布地"/>
						</p>
				</div>
			</div>
	 </div>
	 
	 <br/>
	 <div class="row">
			<div class="col-md-12" id="image_content">
				<div class="container-fluid" id="image_content_contariner">
				  
				</div>
			</div>
	 </div>
	 
	 </form>
	 
	 
	 <script type="text/javascript">
	 	function fillInstantShareId(data){
	 		$("#id").val(data.result.id);
	 		//处理msg begin
	 		$("#msg").removeClass("alert alert-success");
	 		$("#msg").addClass("alert alert-success");
	 		$("#msg").html(data.success + " at " + data.result.updateDate);
	 		$("#msg").show();
	 		//处理msg end 
	 		//fillImageContentWithId
		 	fillImageContentWithId(data.result.imageList, '${KylinboyTravelDeveloperConstant.KYLINBOY_REMOVE_IMAGE_FROM_INSTANT_SHARE_MAPPING}' ,'#instantShare_content_div');
	 		
	 	}
	 	function submitInstantShareForm(formId,backDataContainerId){
	 		var instantShareTitleId = $("#instantShareTitleId");
	 		var instantShareTitleValue = instantShareTitleId.val();
	 		if(instantShareTitleValue==''){
	 			var instantShareTitlePlaceHolder = instantShareTitleId.attr('placeholder');
	 			alert("您得【" + instantShareTitlePlaceHolder + "】");
	 			return;
	 		}
	 		
	 		var batch = $("#batch");
	 		var batchValue = batch.val();
	 		
	 		if(batchValue==''){
	 			alert("非法提交！");
	 			return;
	 		}
			submitForm2(formId,backDataContainerId,fillInstantShareId);
		}
		
		
		$(document).ready(function(){
			var tmp_province=$("#tmp_province").val();
			var tmp_city=$("#tmp_city").val();
			var tmp_mydistrict=$("#tmp_mydistrict").val();
			
			//var remote_ip_info;
			
			var myprovince = tmp_province;
			if(myprovince == ''){
				if(remote_ip_info && remote_ip_info['province']){
					myprovince = remote_ip_info['province']; 
				}
			}
			var mycity = tmp_city;
			if(mycity == ''){
				if(remote_ip_info && remote_ip_info['city']){
					mycity = remote_ip_info['city']; 
				}
			}
			var mydistrict = tmp_mydistrict;
			if(mydistrict == ''){
				if(remote_ip_info && remote_ip_info['district']){
					mydistrict = remote_ip_info['district']; 
				}
			}
			
			$(function(){
				$("#city_1").citySelect({
					prov:myprovince,
					city:mycity
				});
			});
		});
		
	 </script>
</#macro>

<#macro listInstantShareImageCounter instantShareImageCounterList=instantShareImageCounterList>
<#if instantShareImageCounterList?? && instantShareImageCounterList?size gt 0>
	<div class="btn-group btn-group-xs">
	<#list instantShareImageCounterList as instantShareImageCounter>
		<button type="button" onclick="showContent('${instantShareImageCounter.countKey}',${instantShareImageCounter.count},'${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_UI_MAPPING}','content_${instantShareImageCounter.countKey}')" class="btn btn-xs  <#if instantShareImageCounter.count gt 0> btn-primary <#else> btn-default </#if>"> ${instantShareImageCounter.day} 
			  <br/>
			  <#if instantShareImageCounter.count gte 0>
			  	<span class="badge pull-top">${instantShareImageCounter.count}</span>
			  </#if>
		</button>
	</#list>
	</div>
	<hr/>
	<div class="tab-content" id="tab-content">
		<#list instantShareImageCounterList as instantShareImageCounter>
		  <div class="hidden tab-panel nopicture" id="content_${instantShareImageCounter.countKey}">
		  
			  <input type="hidden" value="${instantShareImageCounter.countKey}" data="data" name="yearMonthDay" id="yearMonthDay_${instantShareImageCounter.countKey}"/>
			  <#if instantShareImageCounter.count gt 0>
			  	图片加载中...
			  <#else> 
			  	${instantShareImageCounter.countKey} 木有上传图片
			  </#if>
		  </div>
		</#list>
		
	</div>
	
	<script type="text/javascript">
		function showContent(countKey,imageCount,url,containerId){
			var activeItem = $("#tab-content").find(".active");
			
			if(activeItem){
				activeItem.each(function(index,element){
	    			$(this).addClass("hidden");
	    			$(this).removeClass("active");
	    		});
			}
			
	    	$("#content_"+countKey).removeClass("hidden");
	    	$("#content_"+countKey).addClass("active");
	    	
	    	//var yearMonth = $("#mirror_yearMonth").val();
			var yearMonthDay = countKey;//yearMonth + ""+ day;
	    	//如果没有图片 并且 图片数>0，执行加载图片，图片加载完成后，remove classname nopicture
	    	var hasPicture = $("#content_"+countKey).hasClass("nopicture");
	    	if(hasPicture && imageCount>0 && yearMonthDay!=''){
	    		//alert('第一次加载图片:' + yearMonthDay);
	    		var request={"yearMonthDay" : yearMonthDay};
	    		getContentWithCallBack(url, request,containerId,
	    			function(data){
	    				$("#content_"+countKey).removeClass("nopicture");
	    			}
	    		);
	    	}
	    }
	</script>
	
</#if>
</#macro>

<#macro listInstantShareImage page=null url=null containerId="instantShareImage_content_1">
<#if page??>
			<#assign instantShareImageList=page.content/>
			<#if instantShareImageList??>
				<#assign itemContainerId = 'item_container_' + containerId />
				<div id="${itemContainerId}">
					<input type="hidden" value="${instantShareImage.yearMonthDay}" data="data" name="yearMonthDay" id="yearMonthDay_${instantShareImage.yearMonthDay}"/>
					<@listInstantShareImageItem page=page url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_MAPPING containerId="${itemContainerId}"/>
				</div>
				<@nextPage page=page containerId=itemContainerId url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST_ITEM_UI_MAPPING pageSize=page.size/>
			</#if>
</#if>
</#macro>


<#macro listInstantShareImageItem page url containerId="item_container_instantShareImage_content_1">
<#if page??>
			<#assign instantShareImageList=page.content/>
			<#if instantShareImageList??>
				<div class="row">
				<#list instantShareImageList as instantShareImage>
				  <#--<div>${instantShareImage_index} -- ${instantShareImage.fileInfoId} -- ${instantShareImage.imageUrl}</div>
				  <img width="400px" src="${instantShareImage.imageUrl}" />
				  -->
				  
				  <div class="col-sm-6 col-md-4" style="height:300px">
				    <div class="thumbnail">
				      <img data-src="holder.js/240x180" src="${instantShareImage.imageUrl}" alt="${instantShareImage.title}">
				      <div class="caption">
				        <h5>${instantShareImage.title}</h5>
				        <p>
				        <div class="btn-group btn-group-sm">
					        <button type="button" class="btn btn-warning cancel" onclick="removeExitFileFromBack('${instantShareImage.fileInfoId}')" data-container="body" data-toggle="popover" data-placement="left" data-content="已移除">
		                        <i class="glyphicon glyphicon-ban-circle"></i>
		                        <span>取消</span>
		                    </button>
		                    <button  type="button" class="btn btn-primary start" onclick="addEixtFileToBack('${instantShareImage.fileInfoId}','${instantShareImage.imageUrl}', '${instantShareImage.thumbnailUrl}', '${instantShareImage.title}','${instantShareImage.description}')" data-container="body" data-toggle="popover" data-placement="left" data-content="已移除">
		                        <i class="glyphicon glyphicon-plus"></i>
		                        <span>加入</span>
		                    </button>
	                    </div>
				      </div>
				    </div>
				  </div>
				</#list>
				</div>
				
			</#if>
</#if>
</#macro>


<#macro addCommentUI url modelIdProperty position='1' appendValue=0>
<!-- Modal -->
	<div class="modal fade" id="commentModal${position}" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel${position}" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="commentModalLabel${position}">评论(200字以内)</h4>
	      </div>
	      <div class="modal-body">
		      <form id="form_id_comment${position}" action="${url}" method="post">
		      	<input type="hidden" value="" name="${modelIdProperty}" id="commentModalId${position}" data="data"/>
		      	<input type="hidden" value="" name="depth" id="commentModalIdDepth${position}" data="data"/>
		      	<textarea name="comment" class="form-control" rows="5" placeholder="输入评论" data="data"> </textarea>
		      </form>
	      </div>
	      <div class="modal-footer">
	      	<input type="hidden" name="isAppend" id="isAppendId${position}" value="${appendValue}"/>
	        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
	        <button type="button" class="btn btn-primary" id="button_id_save_comment${position}" onClick="saveComment('#form_id_comment${position}','#span_comment_id', '${appendValue}')">保存评论</button>
	      </div>
	    </div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
<script src="/assets/default/js/site/jit84j_comment.js"></script>
</#macro>



<#macro listImageUIByPage page url containerId>
<#if page??>
	<#assign instantShareImageList=page.content/>
	<#if instantShareImageList?? && instantShareImageList?size gt 0 >
		<@listImageItemUI instantShareImageList=instantShareImageList/>
	</#if>
	<@nextPageButtonBar page=page containerId=containerId url=url pageSize=page.size/> 
</#if>
</#macro>

<#macro listImageItemUI instantShareImageList>
	<#if instantShareImageList?? && instantShareImageList?size gt 0>
		<#list instantShareImageList as image>
		    <div class="thumbnail">
		    	<h5>${image.title}</h5>
		      <img data-src="holder.js/400x300" alt="${image.title}" src="${image.imageUrl}">
		      <div class="caption">
		        <p>${image.description}</p>
		      </div>
		    </div>
		</#list>
	</#if>
</#macro>







<#macro listCommentUIByPage page url containerId>
<#if page??>
	<#assign instantShareCommentList=page.content/>
		
	<#if instantShareCommentList?? && instantShareCommentList?size gt 0 >
		<@listCommentItemUI instantShareCommentList=instantShareCommentList/>
	</#if>
	<#-- assign url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_MAPPING + "/" + instantShare.id/ -->
	<@nextPageButtonBar page=page containerId=containerId url=url pageSize=page.size/> 
</#if>
</#macro>

<#macro listCommentUIByInstantShare instantShare containerId>
	<#if instantShare??>
			<#assign commentPage=instantShare.commentPage/>
			<#if commentPage??>
				<#assign instantShareCommentList=commentPage.content/>
				
				<#if instantShareCommentList?? && instantShareCommentList?size gt 0 >
					<@listCommentItemUI instantShareCommentList=instantShareCommentList/>
				</#if>
				<#assign url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_LIST_UI_MAPPING + "/" + instantShare.id/>
				<@nextPageButtonBar page=commentPage containerId=containerId url=url pageSize=commentPage.size/> 
			</#if>
	</#if>
</#macro>

<#macro listCommentItemUI instantShareCommentList isReply=false>
	<#if !isReply>
	<#assign depth=0/>
	</#if>
	<#if instantShareCommentList?? && instantShareCommentList?size gt 0>
				<#list instantShareCommentList as instantShareComment>
					<div class="row" id="row_span_comment_id_${instantShareComment.id}">
							<div class="col-md-12" id="span_comment_id_${instantShareComment.id}">
								<input type="hidden" value="${depth}" name="depth" id="depth_comment_id_${instantShareComment.id}"/>
								<p <#if !isReply> class="lead" </#if> >
								<#if depth gt 0>
									<#list 1..depth as i>
									<span class="glyphicon glyphicon-eye-open"></span> 
									</#list>
								</#if>
								<span class="text-danger">${instantShareComment.nickName}</span> 
								at <small>${instantShareComment.publishDate}</small> <#if isReply> 【回复】 ${instantShareComment.parent.nickName} : <#else>【说】:</#if> 
								${instantShareComment.comment}
								<a href="javascript:void(0)" id="a_${instantShareComment.id}" onclick="addComment('${instantShareComment.id}','2')">回复</a>
								</p> 
								<#assign replyCommentList=instantShareComment.replyCommentList/>
								<#if replyCommentList??>
									<#assign depth=depth+1/>
									<@listCommentItemUI instantShareCommentList=replyCommentList isReply=true/>
									<#assign depth=depth-1/>
								</#if>
							</div>
					</div>
				</#list>
	</#if>

</#macro>