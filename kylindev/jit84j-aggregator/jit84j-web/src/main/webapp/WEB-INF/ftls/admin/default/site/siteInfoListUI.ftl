<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_login.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>站点列表</title>
</head>
<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <#--<@commonBanner/>-->
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>


<body>
<div class="container body_content" id="body_content">

<div class="row">
<div class="col-md-9">
<p class="text-success">站点列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
      <th>Id</th>
      <th>站点名称</th>
      <th>站长</th>
      <th>域名</th>
      <th>操作</th>
    </tr>
  </thead>
  <@spring.bind "siteInfoList" />  
  
  <tbody>
  <#if (siteInfoList??)>

	<#list siteInfoList as siteInfo>  

	
	 <tr>
	 <@form.form commandName="siteInfoList" id="siteInfo_form_${siteInfo.uniqueIdentifier}" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_SITEINFO_LIST_MAPPING}">
	 <td>${siteInfo.uniqueIdentifier}
	 <input type="hidden" name="id" value="${siteInfo.uniqueIdentifier}"/>
	 </td>
	 <td>
	 <input type="text" name="siteName" value="${siteInfo.siteName}" class="form-control" id="siteName[${siteInfo_index}]" placeholder="输入代码"/>
	 </td>
	 <td>
	 <input type="text" name="siteAdminName" value="${siteInfo.siteAdminName}" class="form-control" id="siteAdminName[${siteInfo_index}]" placeholder="输入名称"/>
	 </td>
	 <td>
	 <input type="text" name="siteUrl" value="${siteInfo.siteUrl}" class="form-control" id="siteUrl[${siteInfo_index}]" placeholder="站点路径"/>
	 </td>
	 </@form.form>
	 <td>
	 <button onClick="editSiteInfo('siteInfo_form_${siteInfo.uniqueIdentifier}','${KylinboyDeveloperConstant.KYLINBOY_SITEINFO_EDIT_UI_MAPPING}')"  class="btn btn-primary">编 辑</button>
	 <button onClick="editSiteInfo('siteInfo_form_${siteInfo.uniqueIdentifier}','/admin/siteInfo/modify/${siteInfo.uniqueIdentifier}')"  class="btn btn-primary">保 存</button>
	 </td>
	 </tr>
	</#list>
  </#if>
   </tbody>
  </table>
</div>
 </div>
 <script type="text/javascript">
 function editSiteInfo(formId,action){
 	var _formId = "#"+formId;
 	$(_formId).attr("action",action);
 	$(_formId).submit();
 }
 </script>

</div>
</div>
</body>
<div class="container" id="body_footer">
	<@commonFooter/> 
</div>
</html>