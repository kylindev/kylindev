<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_user.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>找回密码</title>
</head>


<body>
<div class="container" id="body_content">
<div class="row">
<@form.form commandName="user" id="reset_password_form_id" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FORGET_PASSWORD_UI_MAPPING}">
	<div class="col-md-12">
	<div class="panel panel-warning center-block" >
		<div class="panel-heading ">找回密码邮件发送</div>
		  <h4 class="text-danger text-center">${msg}</h4>
	      <div class="form-group ">
				<ul class="list-inline text-center">
			   <li>
			     <a  class="btn btn-primary" href='${KylinboyDeveloperConstant.KYLINBOY_FORGET_PASSWORD_UI_MAPPING}'>
			     点击重新发送找回密码邮件
			     </a>
			   </li>
			   <li> 
			    <a  class="btn btn-primary" href='${KylinboyDeveloperConstant.KYLINBOY_FRONT_INDEX_UI_MAPPING}'>
			     返回系统首页
			     </a>
			   </li>
			   </ul>
	       </div>
	  </div>
	</div>
</@form.form>
</div>
</div>
</body>

<div id="body_bottom">
</div>

</html>