<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_travel.ftl">
<#include "definefunction_itemButton.ftl">
<#include "definefunction_manageAreaItem.ftl">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>我的管理区-管理区项-按钮管理</title>
</head>

<body>
<div class="container body_id" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@itemButtonIndexNavigationUI currentNavigation=currentNavigation/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="container_content_div">
			<@itemButtonEditUI itemButton=itemButton page=page/>
		</div>
	</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>