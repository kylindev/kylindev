<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_footer.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户登录</title>
</head>

<div id="top_banner">
<@commonBanner/>
</div>

<div id="top_menu">
	<#--<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>-->
</div>

<body>

<div class="container" id="body_content">
<div class="row">
  <div class="col-md-4">
	<div class="panel panel-warning">
	<div class="panel-heading ">从这里登录</div>
	<h3 class="text-danger">${jit8LoginFailure}</h3>
	<@form.form commandName="user" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FRONT_LOGIN_MAPPING}">
						
		   <@spring.bind "user.email" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="control-label" for="email_1">Email address:
		    <#if (spring.status.errorMessages?size>0)>
			
			  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			
			</#if>
		    </label>
		    <@form.input path="email" class="form-control" id="email_1" placeholder="Enter email"/>
		  </div>
		  
		  <@spring.bind "user.password" /> 
		  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
		    <label class="control-label" for="password_1">Password:
		    <#if (spring.status.errorMessages?size>0)>
			
			  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			
			</#if>
		    </label>
		    <@form.password path="password" class="form-control" id="password_1" placeholder="输入密码"/>
		   </div> 
		    
		    <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
			    <label class="control-label" for="validateCode_1">验证码：
			    <#if (spring.status.errorMessages?size>0)>
				
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
				
				</#if>
			    </label>
			    <input type="text" name="validateCode" class="form-control" id="validateCode_1"/>&nbsp;&nbsp;<img id="validateCodeImg" src="/validateCode" />&nbsp;&nbsp;<a href="#" onclick="javascript:reloadValidateCode();">看不清？</a>
		   		<a class="text-danger" href="${KylinboyDeveloperConstant.KYLINBOY_FORGET_PASSWORD_UI_MAPPING}">忘记密码</a>
		   </div> 
		    
	       <ul class="list-inline">
			  <li><input type="submit" value="登  录" class="btn btn-primary"/></li>
			  <li><p class="text-info">已有帐号，点击登录 | <a class="text-danger" href='${KylinboyCoreDeveloperConstant.KYLINBOY_USER_REGISTER_UI_MAPPING}'>没有帐号，<strong>点此</strong>新注册一个</a></p></li>
		   </ul>
	  </div><#-- end panel -->
	  </div>
	      
  </@form.form>
	  	  <div class="col-md-8">
				<div class="panel panel-info" >
					<div class="panel-heading ">最新信息</div>
					  <div class="panel-body">
					   <img width="708" height="270" src="http://images2.baihe.com/images/baihe_new/images/skin_index/20131118index_focus_02.jpg" alt="">
					  </div>
				</div>
	      </div>
 
</div>

<script type="text/javascript">
<!--
function reloadValidateCode(){
    $("#validateCodeImg").attr("src","/validateCode?data=" + new Date() + Math.floor(Math.random()*24));
}
//-->
</script>

</div>

</body>
<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg">
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>