<#import "spring.ftl" as spring />
<#include "taglib.ftl">
	<div class="row" id="row_span_comment_id_${instantShareComment.id}">
		<div class="col-md-12" id="span_comment_id_${instantShareComment.id}">
		<#assign instantShare=instantShareComment.instantShare/>
		<#if instantShareComment.instantShare??>
			<input type="hidden" name="commentTotalNumber_${instantShareComment.instantShare.id}" id="commentTotalNumber_${instantShareComment.instantShare.id}" value="${instantShareComment.instantShare.commentCount}">
		</#if>
			<p <#if !instantShareComment.parent??> class="lead" </#if>>
			<#assign depth=depth+1>
			<#if depth gt 0>
				<#list 1..depth as i>
				<span class="glyphicon glyphicon-eye-open"></span> 
				</#list>
			</#if>
			<span class="text-danger">${instantShareComment.nickName}</span> 
			at <small>${instantShareComment.publishDate?datetime}</small> <#if instantShareComment.parent??>【回复】 ${instantShareComment.parent.nickName} : <#else> 【说】：</#if> 
			${instantShareComment.comment}
			<span class="text-success">${msg}</span>
			</p> 
		</div>
	</div>
 <#--
<#assign commentList=page.content/>
 <#if commentList?? && commentList?size gt 0 >
 	<input type="hidden" value="${page.totalElements}" id="commentTotalNumber_${manifestoComment.manifesto.id}"/>
	<ul id="scroll_ul_${manifestoComment.manifesto.id}" class="list-unstyled text-left" style="height:20px;overflow:hidden;">
 	<#list commentList as comment> 
     <li>
	     <a class="text-info" href="#" title="查看【${comment.nickName}】的宣言">${comment.comment}</a> 
	     	<span class="text-danger">commented by</span> <a class="text-primary" href="" title="了解【${comment.nickName}】">
	     	<span class="text-primary">${comment.nickName}</span>
	     </a>
     </li> 
    </#list>
    <script type="text/javascript">
	    $("#span_comment_total_${manifestoComment.manifesto.id}").html('${page.totalElements}');
	</script>
    </ul> 
  <#else>
  	<a href="javascript:void(0)" onClick="addComment('${comment.manifesto.id}')" class="text-danger">我来评一下先</a>
 </#if>
--> 

 
