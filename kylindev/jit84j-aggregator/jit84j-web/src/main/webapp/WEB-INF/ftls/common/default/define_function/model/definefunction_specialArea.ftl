<#macro specialAreaDropdown specialAreaList specialArea name="name">
<select name='${name}' data="data">
		<option value="">ALL</option>
		<#list specialAreaList as sa>
	  	<option value="${sa.id}" <#if specialArea?? && specialArea.id?? && specialArea.id == sa.id>selected</#if>>${sa.name}</option>
		</#list>
</select>
</#macro>

<#macro specialAreaSearch specialArea name="name">
<#assign specialAreaList=StaticDataDefineManager.getData(StaticDataDefineConstant.SPECIAL_AREA_DROPDOWN)/>
<@specialAreaDropdown specialAreaList=specialAreaList specialArea=specialArea name=name/>
</#macro>

<#macro specialAreaIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#specialArea_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</#macro>

<#--specialAreaEditUI 添加、修改页面 begin-->
<#macro specialAreaEditUI specialArea page>
<div class="row">
		  <div class="col-md-3">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 特殊区</p>
		  	  
		  	  <form id="specialArea_form" action="${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_MAPPING}" method="post">
		  	
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if specialArea??>${specialArea.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称: 
				</label>
			    <input type="text" data="data" name="name" value="<#if specialArea??>${specialArea.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="code">代码: 
				</label>
			    <input type="text" data="data" name="code" value="<#if specialArea??>${specialArea.code}</#if>" class="form-control" id="code" placeholder="输入代码，使用字母，必须唯一" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="imagePath">图标: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图标路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if specialArea??>${specialArea.imagePath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if specialArea??>${specialArea.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if specialArea??>${specialArea.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="openable">是否开放: 
				</label>
			    <input type="checkbox" data="data" name="openable" <#if specialArea?? && specialArea.openable> checked </#if> value="${specialArea.openable}" class="form-control" id="openable"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="writable">允许发布: 
				</label>
			    <input type="checkbox" data="data" name="writable" <#if specialArea?? && specialArea.writable> checked </#if> value="${specialArea.writable}" class="form-control" id="writable"/>
			  </div>
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#specialArea_form','#specialArea_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#specialArea_form')" value='清空'/>
				</li>
		      </ul>
		</div>
		<div class="col-md-9">
		
		<p class="text-primary">特殊区列表</p>
		<@listSpecialAreaView page=page/>
		</div> 
		<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</div>
</#macro>
<#--specialAreaEditUI 添加、修改页面 end-->

<#macro listSpecialAreaView page>
<#if page?? && page.content??>
		<#assign specialAreaList = page.content />
		 <table class="table">
		 <tr>
		 <th>
			 Id
			</th>
		 	<th>
			 名称
			</th>
			 <th>
			 代码
			 </th>
			 <th>
			 图标
			 </th>
			 <th>
			 开放
			 </th>
			 <th>
			 发布
			 </th>
			 <th>
			 排序
			 </th>
			 <th>
			 描述
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		<#list specialAreaList as specialArea>  
			 <tr>
			 <td>
				 ${specialArea.id}
				</td>
				<td>
				 ${specialArea.name}
				</td>
				 <td>
				 ${specialArea.code}
				 </td>
				 <td>
				 ${specialArea.imagePath}
				 </td>
				 <td>
			 	 <input type="checkbox" data="data" name="openable" <#if specialArea?? && specialArea.openable>checked</#if> class="form-control" id="openable${specialArea.uniqueIdentifier}" />
				 </td>
				 <td>
				  <input type="checkbox" data="data" name="writable" <#if specialArea?? && specialArea.writable> checked </#if> value="${specialArea.writable}" class="form-control" id="writable${specialArea.uniqueIdentifier}"/>
				 </td>
				 <td>
				  <input type="text" data="data" name="sequence" value="<#if specialArea?? >${specialArea.sequence}</#if>" class="form-control" id="sequence${specialArea.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
				 </td>
				  <td>
				  ${specialArea.description}
				 </td>
				 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${specialArea.id}" onclick="switchSetting('${specialArea.id}','${specialArea.id}','${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_EDIT_UI_MAPPING}','#specialArea_content_div')">编辑</a>
				  	</li>
				    <li>
				    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${specialArea.id}','${KylinboyDeveloperConstant.KYLINBOY_SPECIAL_AREA_REMOVE_MAPPING}','#specialArea_content_div')">删除</a>
				
				    </li>
				  </ul>
			 </td>
			 </tr>
		</#list>
		</table>
</#if>
</#macro>

<#macro specialAreaListIndex specialAreaList>
	<#if specialAreaList?? && specialAreaList?size gt 0>
		 <ul class="nav nav-tabs nav-justified">
		  	<#list specialAreaList as specialArea> 
			  	<#if specialArea?? >
			  	<li <#if 'today' = specialArea.code>class="active"</#if>>
			  	<a href="javascript:void(0)"  id="specialArea_${specialArea.id}" >
			 		${specialArea.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	</#if>
</#macro>