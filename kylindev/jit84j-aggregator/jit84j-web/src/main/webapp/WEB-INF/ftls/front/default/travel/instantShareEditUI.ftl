<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">

<div class="container" id="body_content_opration">
	<ul class="list-inline">
		   		<li><a href="#"><span class="label label-success">发布即时分享</span></a></li>
		   		<li class="active"><a href="#"><span class="label label-primary">发布行程计划</span> </a></li>
		   		<li><a href="#"> <span class="label label-primary">发布活动计划</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布活动回忆帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布行程攻略分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布装备分享帖</span></a></li>
		   		<li><a href="#"><span class="label label-primary">发布摄影攻略分享帖</span></a></li>
		   	</ul>
</div>
<div class="container" id="body_content_navigation">
		<ul class="list-unstyled">
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 今日宣言</h4></a></li>
			<li><a href="#"><h4 class="text-center text-warning"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我要发布</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 即时分享</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的关注</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 个人资料</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的行程</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的活动</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的队伍</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的装备</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的回忆</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的驴友</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 摄影攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 首页定制</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 消息留言</h4></a></li>
		</ul>
</div>
<div class="container" id="body_content_detail">
	<div class="row">
			<div class="col-md-12">
				  	  <div class="row">
							<div class="col-md-10">
							 <form id="manifesto_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_MAPPING}" method="post">
									<input type="hidden" data="data" id="id" name="id" value="<#if manifesto??>${manifesto.id}</#if>">
								    <input type="text" data="data" name="decription" value="<#if manifesto??>${manifesto.decription}</#if>" class="form-control" id="manifesto" placeholder="说一句" required="true"/>
						     </form>
						     <div id="manifesto_content_div">
							 </div>
						     </div>
						     <div class="col-md-2">
						     		<button class="btn btn-primary" onClick="submit('#manifesto_form','#manifesto_content_div')">发布</button>
						     </div>
				       </div>
				       
				       <div class="form-group ">
						    <label class="control-label" for="description"> 
							</label>
							<textarea data="data" rows="5" id="description" name="description" value="<#if journey??>${journey.description}</#if>" class="form-control" id="description" placeholder="多说几句" ><#if journey??>${journey.description}</#if></textarea>
					  </div>
			</div>
	 </div>
	 <hr/>
	 <div class="row">
			<div class="col-md-12">
					<div class="form-group ">
					    <label class="control-label" for="imagePath">上传一张图片: 
						</label>
					    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图片路径" />
					 	
					 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_MULTI_UPLOAD_UI_MAPPING}','iconPath')"/><br/>
					    <div id="iconPath_img">
					    	<img src='<#if journey??>${journey.imagePath}</#if>'/>
					    </div>
					  </div>
			</div>
	 </div>
			
			<#--${KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_MULTI_UPLOAD_UI_MAPPING}-->
			<iframe id="upload_iframe" src="${KylinboyCoreDeveloperConstant.KYLINBOY_FILEINFO_MULTI_UPLOAD_UI_MAPPING}" scrolling="no" frameborder="0" width="100%" height="100%"></iframe>
			<script src="/assets/common/js/jquery/iframe/iframeheight.min.js"></script>
			
			
			<#--<script src="/assets/common/js/jquery/jquery-iframe-auto-height/release/jquery.iframe-auto-height.plugin.1.9.5.js"></script>-->
			<script type="text/javascript">
			 <#--jQuery('iframe').iframeAutoHeight({debug: true, diagnostics: false});      
			
				$("#upload_iframe").load(function(){
				var mainheight = $(this).contents().find("body").height()+30;
				$(this).height(mainheight);
				}); 
				$("#upload_iframe").on("change", function(){ 
					var mainheight = $(this).contents().find("body").height()+30;
					$(this).height(mainheight); 
				});
				-->
				$('#upload_iframe').iframeHeight({
				    resizeMaxTry         : 2,  
				    resizeWaitTime       : 300,    
				    minimumHeight        : 100, 
				    defaultHeight        : 500,   
				    heightOffset         : 90,    
				    exceptPages          : "",  
				    debugMode            : false,
				    visibilitybeforeload : true,
				    blockCrossDomain     : true,
				    externalHeightName   : "bodyHeight",
				    onMessageFunctionName: "getHeight",
				    domainName           : "*",
				    watcher              : true,
				    watcherTime          : 400
				});
				
			</script>
			
			
<div id="filepathTextFieldId_hidden"></div>
<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
</div>

