<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>导航列表</title>
</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>


<body>
<div class="container" id="body_content">
	<div class="row" style="z-index:-10">
		<div class="col-md-12">
		<p class="text-success">前台导航预览:</p>
		<@navigatMenuTopbar navigatMenuList=navigatMenuTopList currentNavigatMenu=currentNavigatMenu/>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12">
			<div class="table-responsive">
			  <#assign depth = 0 />
			  <@listNavigatMenuTree currentNavigatMenu=currentNavigatMenu/>
			</div>
		 </div>
	</div>
</div>
</body>
</html>