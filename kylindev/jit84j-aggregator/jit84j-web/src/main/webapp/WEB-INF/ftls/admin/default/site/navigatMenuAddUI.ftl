<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>添加导航</title>
</head>
<body>
<div id="body_content">
	<form id="navigatMenu_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_NAVIGATMENU_ADD_MAPPING}">
	<div class="row" >

		  <div class="col-md-6">
		  	  <p class="text-success">添加导航</p>
			  <input type="hidden" value="<#if navigatMenu??>${navigatMenu.id}</#if>" name="id"/>
		      <div class="form-group ">
			    <label class="control-label" for="name">导航名称: 
				</label>
			    <input name="name" value="<#if navigatMenu??>${navigatMenu.name}</#if>" class="form-control" id="name" placeholder="输入导航名称" required="true"/>
			  </div>
			  <div class="form-group">
			    <label class="control-label" for="code">导航代码: 
				</label>
			    <input name="code" class="form-control" value="<#if navigatMenu??>${navigatMenu.code}</#if>" id="code" placeholder="输入导航代码，使用字母" required="true"/>
			  </div>
			  
			  <div class="form-group">
			    <label class="control-label" for="code">导航图标: 
					
				</label>
			    <input name="iconPath" value="<#if navigatMenu??>${navigatMenu.iconPath}</#if>"  class="form-control" id="iconPath" placeholder="输入导航图标" />
			 	<input type="button" value="点击上传" class="btn btn-primary" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='${navigatMenu.iconPath}'/>
			    </div>
			  </div>
			  <div class="form-group">
			    <label class="control-label" for="code">导航路径: 
				</label>
			    <input name="url" value="<#if navigatMenu??>${navigatMenu.url}</#if>" class="form-control" id="url" placeholder="输入导航路径" />
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
					
				</label>
			    <input name="sequence" value="<#if navigatMenu??>${navigatMenu.sequence}</#if>" class="form-control" id="sequence" placeholder="输入导航顺序，使用数字1,2,10,12" required="true"/>
			  </div>
		  </div>
		  <div class="col-md-3">
				  
				    <label class="control-label" for="parent.id"><input type="button" value="选择父类" id="parent_id_button" class="btn btn-primary"/>: 
					</label>
					<div>
					<#assign depth = 1 />  
				  	<@selectParentNavigatMenuRadio children=navigatMenuList currentNavigatMenu=navigatMenu />
				  	</div>
				 
		   </div>
		    <div class="col-md-3">  
				  
				    <label class="control-label" for="parent.id"><input type="button" value="分配子类" id="parent_id_button" class="btn btn-primary"/>: 
					</label>
					<div>
					<#assign depth = 1 />  
					<@asignChildrenNavigatMenuCheckbox children=navigatMenuList currentNavigatMenu=navigatMenu/>
					</div> 
				  
			 </div>
		  
	  </div>
	  <div class="row" >
		   <div class="col-md-12">
	       	<input type="submit" value="保存设置" class="btn btn-primary"/>
	       </div>
      </div>
 
	</form>
	<script src="/assets/ckeditor/adapters/jquery.js"></script>
	<div id="filepathTextFieldId_hidden"></div>
	<script src="/assets/common/js/jit8_fw/fileupload.js"></script>
	<script src="/assets/common/js/jquery/jquery.json-2.4.min.js"></script>
</div>
</body>
</html>