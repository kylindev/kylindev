
<#macro buildNode children>  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
            <option value="${child.uniqueIdentifier}" label="<#list 1..depth as i>&nbsp;&nbsp;&nbsp;&nbsp;</#list>${(child.name)?if_exists}">  
                <#list 1..depth as i>&nbsp;&nbsp;&nbsp;&nbsp;</#list>${(child.name)?if_exists}  
            </option>  
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            <@buildNode children = child.childrenCategory />  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro> 

<#macro buildNodeCheckbox children checkboxName="">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <input type="checkbox" value="${child.uniqueIdentifier}" name="${checkboxName}" id="category_${child.uniqueIdentifier}"
            <#if child?? && child.currentArticleAsigned> checked </#if>
            />${(child.name)?if_exists}
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            <@buildNodeCheckbox children = child.childrenCategory checkboxName=checkboxName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro> 



<#macro asignCategoryForArticleAddedCheckbox children checkboxName="articleCategoryList">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <input type="checkbox" value="${child.uniqueIdentifier}" name="${checkboxName}[${child.uniqueIdentifier}].category.id" id="articleCategoryList_${child.uniqueIdentifier}"
            <#if child?? && child.currentArticleAsigned> checked </#if>
            />${(child.name)?if_exists}
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            <@asignCategoryForArticleAddedCheckbox children = child.childrenCategory checkboxName=checkboxName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro> 



<#macro asignChildrenCategoryCheckbox children currentCategory checkboxName="childrenCategory">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <#if currentCategory?? && currentCategory.id!=child.id>
	            <input type="checkbox" value="${child.uniqueIdentifier}" name="${checkboxName}[${child_index}].id" id="category_${child.uniqueIdentifier}"
	            	<#if currentCategory?? && child.parent?? && currentCategory.id==child.parent.id> checked </#if>
	            />
            </#if>
            
            <#if currentCategory?? && currentCategory.id==child.id>
	             	<font color="red">
	         </#if>
             ${(child.name)?if_exists}
             <#if currentCategory?? && currentCategory.id==child.id>
	             	</font>
	         </#if>
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            <@asignChildrenCategoryCheckbox children = child.childrenCategory currentCategory=currentCategory checkboxName=checkboxName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
        
    </#if>  	
</#macro> 


<#--保存类别-->
<#macro persistCategory children current>

<form action="/admin/category/addChilrenCategory" id="addChilrenCategory" method="post">
<input type="hidden" name="currentCategoryId" value="current.id">为[${current.name}]分配子类:<br/>
<@buildNodeCheckbox children = children checkboxName="childrenCategoryIds"/>  
<input type="submit" value="保存">
<input type="reset" value="重置">
</form>
</#macro> 


<#--为文章分配类别-->
<#macro asignCategoryForArticle children currentArticleId>

<form action="/admin/article/asignCategory" id="asignCategoryForArticle" method="post">
<input type="hidden" name="currentArticleId" id="asignCategoryForArticle_currentArticleId" value="${currentArticleId}"/>
<@buildNodeCheckbox children = children checkboxName="childrenCategoryIds_article"/>
<input type="button" value="保存" id="asignCategoryForArticleButton">
<input type="reset" value="重置">
</form>
<script type="text/javascript">
$( document ).ready(function() {
	$("#asignCategoryForArticleButton").click(function(){
			var currentArticleId = $("#asignCategoryForArticle_currentArticleId").attr("value");
			var categoryIdList="";
			//var categoryIdList=new Array();
			$("input[name='childrenCategoryIds_article']:checked").each(function(index,value){
				categoryIdList +=value.value+",";
				//categoryIdList[index]=value.value;
			});
			if(categoryIdList.lastIndexOf(",")>-1){
				categoryIdList=categoryIdList.substring(0,categoryIdList.lastIndexOf(","));
			}
			//alert(categoryIdList);
			var action = $("#asignCategoryForArticle").attr("action");
			var request={
				"articleId":currentArticleId,
				"categoryIdList":categoryIdList
			};
			//var encoded = $.toJSON( request );
			//alert(action);
			$.ajax({
				url: action,
				data: request,
				type: "POST",
				dataType : "html",
				success: function( val ) {
					$( "#asignCategoryForArticle_div" ).html( val );
				},
				error: function( xhr, status ) {
					alert( "Sorry, there was a problem!" );
				},
				complete: function( xhr, status ) {
					//alert( "The request is complete!" );
				}
			});
			
	});
});
</script>
</#macro> 

<#macro buildNodeRadio children currentCategory>  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        <#list 1..depth as i>-</#list>
            <input type="Radio" value="${child.uniqueIdentifier}" name="name" id="category_${child.uniqueIdentifier}" <#if currentCategory?? && currentCategory.parent?? && currentCategory.parent.id == child.id> checked</#if>/>${(child.name)?if_exists}
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            <@buildNodeRadio children = child.childrenCategory currentCategory=currentCategory/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro>

<#--选择父类-->
<#macro selectParentCategoryRadio children currentCategory radioName="parent.id">  
    <#if children?? && children?size gt 0>  
        <#list children as child>  
        	<#list 1..depth as i>-</#list>
            <#if currentCategory?? && currentCategory.id!=child.id>
            	<input type="Radio" value="${child.uniqueIdentifier}" name="${radioName}" id="select_parent_category_${child.uniqueIdentifier}" <#if currentCategory?? && currentCategory.parent?? && currentCategory.parent.id == child.id> checked</#if>/>
            </#if>
	             <#if currentCategory?? && currentCategory.id==child.id>
	             	<font color="red">
	             </#if>
             ${(child.name)?if_exists}
	             <#if currentCategory?? && currentCategory.id==child.id>
	             	</font>
	             </#if>
             
            <br/>
            <#assign depth = depth + 1 />  
            <#if child.childrenCategory?? && child.childrenCategory?size gt 0>  
            	<@selectParentCategoryRadio children = child.childrenCategory currentCategory=currentCategory radioName=radioName/>  
            </#if>
            <#assign depth = depth - 1 />  
        </#list>  
    </#if>  	
</#macro>

<#macro listTag tags>  
    <#if tags?? && tags?size gt 0>  
        <#list tags as tag>  
            <input type="checkbox" value="${tag.uniqueIdentifier}" name="${tag.uniqueIdentifier}" id="${tag.uniqueIdentifier}"/>  
                ${(tag.name)?if_exists}  &nbsp;&nbsp;&nbsp;&nbsp;
        </#list>  
    </#if>  	
</#macro> 

