<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>上传文件</title>
</head>
<body>
<div class="container">
<div class="row">
  <div class="col-md-12">
  <p class="text-success">上传文件</p>
  
  <@form.form method="POST" commandName="fileInfo"
		enctype="multipart/form-data" action="/admin/fileInfo/upload">
 
		<@form.errors path="*" cssClass="errorblock" element="div" />
 
		Please select a file to upload : <input type="file" name="files" />
		<input type="submit" value="upload" />
		<span><@form.errors path="fileInfo" cssClass="error" />
		</span>
 
	</@form.form>
  
  </div>
</div>
</div>

</body>
</html>