<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户登录</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-6">
<p class="text-success">修改与站长关系:</p>
<@form.form commandName="ownRelation" role="form" action="/modifyOwnRelation">

<@spring.bind "ownRelation.uniqueIdentifier" />  
<@form.hidden path="uniqueIdentifier" class="form-control" id="uniqueIdentifier" />

	    <@spring.bind "ownRelation.code" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    
	    <label class="control-label" for="code">代码: 
		<#if (spring.status.errorMessages?size>0)>
		<#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
		
		</#if>
	    </label>
	    <@form.input path="code" class="form-control" id="code" placeholder="输入代码"/>
	  </div>
	  
	   <@spring.bind "ownRelation.name" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="email_1">名称:
	    <#if (spring.status.errorMessages?size>0)>
		
		  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
		
		</#if>
	    </label>
	    <@form.input path="name" class="form-control" id="name" placeholder="输入名称"/>
	  </div>
	  
	  <@spring.bind "ownRelation.sequence" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="sequence">序号:
	    <#if (spring.status.errorMessages?size>0)>
		
		  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
		
		</#if>
	    </label>
	    <@form.input path="sequence" class="form-control" id="sequence" placeholder="输入序号"/>
	  </div>
        <div class="col-md-3">
       <input type="submit" value="保 存" class="btn btn-primary"/>
       </div>
       <div class="col-md-7">
       <p class="text-info">点击保存 | <a class="text-danger" href='${KylinboyDeveloperConstant.KYLINBOY_OWNRELATION_LIST_UI_MAPPING}'>返回列表</a></p>
       </div>
  </@form.form>
 </div>
  <div class="col-md-6">
  
  </div>
</div>
</div>

</body>
</html>