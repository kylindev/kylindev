<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">
<#include "definefunction_travel.ftl">

<div id="body_content_main_screen_first"><!--主屏元素展示一区-->
	<div class="row">	
			<div class="col-md-5">
				上一篇: <#if previous??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING}/${previous.id}"> ${previous.title} </a> by ${previous.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-5">
				下一篇: <#if following??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING}/${following.id}"> ${following.title} </a> by ${following.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-2">
			
			</div>
	</div>
	<div class="row">	
		<div class="col-md-10">
			<div class="panel panel-default" style="margin-bottom:0px">
			<h3>${instantShare.title}<#if instantShare.imageCount gt 0 ><small>图[${instantShare.imageCount}]</small></#if></h3>
			<h4>
				<ul class="list-inline">
					<li><small> by </small> <a href="" title="查看 ${instantShare.nickName} 的主页"> ${instantShare.nickName} </a></li>
					<li><small> in </small> ${instantShare.place} </li>
					<li><small> at </small> ${instantShare.publishDate} </li>
					<li>
						<small> 
							<a href="javascript:void(0)" class="praise-instantShare-a" id="praise_a_${instantShare.id}" onclick="praiseInstantShareWithCallBack('${instantShare.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_PRAISE_MAPPING}','#praise_${instantShare.id}','#praise_a_${instantShare.id}')">赞<span class="text-danger"><span class="glyphicon glyphicon-heart"></span></span></a>
							(<span id="praise_${instantShare.id}" class="text-danger praise-instantShare"> ${instantShare.praiseCount} </span>) | 
							<a href="javascript:void(0)" class="text-info" onclick="addComment('${instantShare.id}','1')">评<span class="glyphicon glyphicon-pencil"></span></a>(<span class="text-danger span-comment-total-${instantShare.id}"> ${instantShare.commentCount} </span>)
						</small>
					</li>
				</ul>
			</h4>
				  <div class="panel-body">
				    <p class="text-left">${instantShare.description}</p>
				    <br/>
				    <div class="row">	
							<div class="col-md-12" id="image_container_id_${instantShare.id}">
				    			<#assign url= KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_IMAGE_LIST2_UI_MAPPING +"/"+ instantShare.id>
				    			<@listImageUIByPage page=instantShare.imagePage url=url containerId="image_container_id_"+instantShare.id/>
							</div>			
					</div>
					 <br/>
					<div class="row">	
							<div class="col-md-12">
								
								<div class="panel panel-warning">
								  <div class="panel-heading">
								  	<span class="glyphicon glyphicon-comment"></span> 评论区(<span class="span-comment-total-${instantShare.id}"> ${instantShare.commentCount} </span>) | 
								  	<a href="javascript:void(0)" class="text-info" onclick="addComment('${instantShare.id}','1')"><span class="glyphicon glyphicon-pencil"></span>点此添加评论</a> | 
								  	<a href="javascript:void(0)" class="praise-instantShare-a" id="praise_a_${instantShare.id}_2" onclick="praiseInstantShareWithCallBack('${instantShare.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_PRAISE_MAPPING}','#praise_${instantShare.id}_2','#praise_a_${instantShare.id}_2')"><span class="text-danger"><span class="glyphicon glyphicon-heart"></span>点赞(<span class="praise-instantShare" id="praise_${instantShare.id}_2"> ${instantShare.praiseCount?default(0)} </span>)</span></a>
								  </div>
								  <div class="panel-body text-left" id="span_comment_id_${instantShare.id}">
								  	<@listCommentUIByInstantShare instantShare=instantShare containerId="span_comment_id_"+instantShare.id/>
								  </div>
								</div>
							</div>
					</div>
				  </div>
			</div>
		</div>
		<div class="col-md-2">
			<!--详细页面侧边广告区 begin-->
			<img src="http://image.songtaste.com/images/usericon/s/17/3800417.JPG" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/11/3989011.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/56/446456.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/l/20/4523320.jpg" border="0" class="icon">
			<!--详细页面侧边广告区 end-->
		</div>
	</div>
	
	
	<div class="row">	
			<div class="col-md-5">
				上一篇: <#if previous??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING}/${previous.id}"> ${previous.title} </a> by ${previous.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-5">
				下一篇: <#if following??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_VIEW_DETAIL_UI_MAPPING}/${following.id}"> ${following.title} </a> by ${following.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-2">
			
			</div>
	</div>
	
	<@addCommentUI url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_ADD_MAPPING modelIdProperty='instantShare.id' position='1' appendValue=-1/>
	
	<@addCommentUI url=KylinboyTravelDeveloperConstant.KYLINBOY_INSTANT_SHARE_COMMENT_ADD_MAPPING modelIdProperty='parent.id' position='2' appendValue=1/>
		
	<script type="text/javascript">
		function praiseInstantShareWithCallBack(instantShareId,url,containerId,praiseButtonId){
			praiseWithCallBack(instantShareId,url,containerId,praiseButtonId,function(data){
				$(".praise-instantShare").html(data);
				$(".praise-instantShare-a").each(function(){
					this.onclick=null;
				});
			});
		}
	</script>
	
</div>
