<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_userManageAreaItem.ftl">

<div class="container" id="body_content_opration">

</div>
<div class="container" id="body_content_navigation">
		<@listUserManageAreaItem currentUserManageAreaItem=currentUserManageAreaItem/>
		<#--
		<ul class="list-unstyled">
			<li><a href="#"><h4 class="text-center text-warning"><span class="glyphicon glyphicon-circle-arrow-right"></span> 今日宣言</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我要发布</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 即时分享</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的关注</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 个人资料</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的行程</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的活动</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的队伍</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 行程攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的装备</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的回忆</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 我的驴友</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 摄影攻略</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 首页定制</h4></a></li>
			<li><a href="#"><h4 class="text-center text-info"><span class="glyphicon glyphicon-circle-arrow-right"></span> 消息留言</h4></a></li>
		</ul>
		-->
</div>
<div class="container" id="body_content_detail">
	<div class="row">
			<div class="col-md-12">
				  	  <div class="row">
				  	  		 <div class="col-md-2 text-right">
						     		<h5 class="text-primary">我的宣言：</h5>
						     </div>
							<div class="col-md-8">
							 <form id="manifesto_form" action="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_MAPPING}" method="post">
									<input type="hidden" data="data" id="id" name="id" value="<#if manifesto??>${manifesto.id}</#if>">
								    <input type="text" data="data" name="decription" value="<#if manifesto??>${manifesto.decription}</#if>" class="form-control" id="manifesto" placeholder="每天一条宣言，今天展示什么呢" required="true"/>
						     </form>
						     <div id="manifesto_content_div">
							 </div>
						     </div>
						     <div class="col-md-2">
						     		<button class="btn btn-primary" onClick="submit('#manifesto_form','#manifesto_content_div')">发布</button>
						     </div>
				       </div>
			       
			</div>
	 </div>
	 <hr/>
	 <div class="row">
			<div class="col-md-12">
			我最近一周的宣言：
			</div>
	 </div>
	 <div class="row">
			<div class="col-md-12">
			今日格言：
			</div>
	 </div>
	 <div class="row">
			<div class="col-md-12">
				<ol >
					<li>
							Do not , for one repulse , give up the purpose that you resolved to effect .(William Shakespeare , British dramatist)
							<br/>
							不要只因一次失败，就放弃你原来决心想达到的目的。(英国剧作家 莎士比亚.W.)
					</li>
					<li>
						　　Don't part with your illusions . When they are gone you may still exist , but you have ceased to live. (Mark Twain , American writer)
						　　<br/>
						   不要放弃你的幻想。当幻想没有了以后，你还可以生存，但是你虽生犹死。((美国作家 马克·吐温)
					</li>
					<li>
							I want to bring out the secrets of nature and apply them for the happiness of man . I don't know of any better service to offer for the short time we are in the world .(Thomas Edison , American inventor)
				　　			<br/>
							我想揭示大自然的秘密，用来造福人类。我认为，在我们的短暂一生中，最好的贡献莫过于此了。 (美国发明家 爱迪生. T.)
					</li>
					<li>　
							Ideal is the beacon. Without ideal , there is no secure direction ; without direction , there is no life .( Leo Tolstoy , Russian writer)
				　　			<br/>
							理想是指路明灯。没有理想，就没有坚定的方向;没有方向，就没有生活。(俄国作家 托尔斯泰. L .)
					</li>
					<li>　
							If winter comes , can spring be far behind ?( P. B. Shelley , British poet )
				　　			<br/>
							冬天来了，春天还会远吗?( 英国诗人, 雪莱. P. B.)
					</li>
				</ol>
			</div>
	 </div>
</div>
