<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_stickyNoteCategorySpecial.ftl">
<#include "definefunction_stickyNoteCategory.ftl">
<#include "definefunction_stickyNote.ftl">
<#include "definefunction_specialArea.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>帖子列表-随手帖管理</title>
</head>

<body>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@horizontalNavigationUI currentNavigation=currentNavigation isAjax='false'/>
		</div>
	</div>
	<form action="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_LIST_UI_MAPPING}" id="stickyNot_list_form" method="post">
	<div class="row">
		<div class="col-md-12" id="stickyNoteCategorySpecial_content_div">
				<div class="panel panel-warning" id="specialArea_list_div">
					<div class="panel-heading ">多条件查询</div>
					<div class="panel-body"> 
					<label>特殊区：
					<@specialAreaSearch specialArea=specialArea name="specialAreaId"/>
					</label>
					<label>类别：
					<@stickyNoteCategorySearch stickyNoteCategory=stickyNoteCategory name="categorySpecialIdList[0]"/>
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="title" value="<#if stickyNote??>${stickyNote.title}</#if>" class="form-control" id="title" placeholder="输入标题" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="author" value="<#if stickyNote??>${stickyNote.author}</#if>" class="form-control" id="author" placeholder="输入作者" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="summary" value="<#if stickyNote??>${stickyNote.summary}</#if>" class="form-control" id="summary" placeholder="输入概要" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="imagePath" value="<#if stickyNote??>${stickyNote.imagePath}</#if>" class="form-control" id="summary" placeholder="输入图片路径" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="categorySpecialName" value="<#if stickyNote??>${stickyNote.browseMaxCount}</#if>" class="form-control" id="browseMaxCount" placeholder="输入浏览次数" />
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="minLevel" value="<#if stickyNote??>${stickyNote.minLevel}</#if>" class="form-control" id="name" placeholder="最小级别，数字0,1,2,3..." />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="maxLevel" value="<#if stickyNote??>${stickyNote.maxLevel}</#if>" class="form-control" id="name" placeholder="最大级别，数字6,7,8,9..." />
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="minPublishTime" value="<#if stickyNote??>${stickyNote.minPublishTime}</#if>" class="form-control" id="minPublishTime" placeholder="起始发布时间" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="maxPublishTime" value="<#if stickyNote??>${stickyNote.maxPublishTime}</#if>" class="form-control" id="maxPublishTime" placeholder="终止发布时间" />
					</label>
					<button class="btn btn-primary" onClick="formSubmit(1)">搜索</button>
					</div>
				</div>
					
				<div class="panel panel-warning" id="stickyNot_list_div">
					<div class="panel-heading ">列表页面</div>
					<div class="panel-body" id="stickyNote_list_content_div_id"> 
						<@listStickyNoteView page=page formId="stickyNot_list_form"/>
					</div>
				</div>
		</div>
	</div>
	</form>
</div>

</body>
</html>