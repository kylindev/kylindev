<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>录入文章</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-2">

</div>
<div class="col-md-8">
  <p class="text-success">设置文章基本信息</p>
  <@form.form commandName="article" role="form" action="/admin/article/articleAdd">
  <@spring.bind "article.uniqueIdentifier" /> 
      <@spring.bind "article.title" />  
      <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="title">标题: 
			<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
		</label>
	    <@form.input path="title" class="form-control" id="title" placeholder="输入文章标题" required="true"/>
	  </div>
	  <@spring.bind "article.titleSecond" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="titleSecond">第二标题:
		    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small> </#list>
			</#if>
	    </label>
	    <@form.input path="titleSecond" class="form-control" id="titleSecond" placeholder="输入第二标题" required="true"/>
	  </div>
	  
	  <@spring.bind "article.keyWord" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="keyWord">文章关键字:
		    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="keyWord" class="form-control" id="keyWord" placeholder="输入文章关键字,以,分隔" required="true"/>
	  </div>
	  
	  <@spring.bind "article.description" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label for="description">文章描述：
	    	<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="description" class="form-control" id="description" placeholder="输入文章描述" required="true"/>
	  </div>
	  <@spring.bind "article.primaryDesc" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="primaryDesc">文章概要:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="primaryDesc" class="form-control" id="primaryDesc" placeholder="文章概要" />
	  </div>
	   <@spring.bind "article.authorName" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteTitle">文章作者:
	    	<#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="authorName" class="form-control" id="authorName" placeholder="输入作者" required="true"/>
	  </div>
	  <@spring.bind "article.penName" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteSecondTitle">笔名:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="penName" class="form-control" id="penName" placeholder="输入笔名" />
	  </div>
	   <@spring.bind "article.firstImagePath" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="siteKey">封面图片路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="firstImagePath" class="form-control" id="firstImagePath" placeholder="封面图片路径" />
	    <input type="file" name="files" />
	  </div>
	  <@spring.bind "article.firstImageAdPath" /> 
	   <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="description">封面图片缩略图:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="firstImageAdPath" class="form-control" id="firstImageAdPath" placeholder="封面图片缩略图" />
	  </div>
	  <@spring.bind "article.iconPath" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="iconPath">文章图标路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="iconPath" class="form-control" id="iconPath" placeholder="文章图标路径,默认/image/site_info/icon.png" />
	    <img src="${article.iconPath}" title="文章图标" alt="独家记忆" class="img-rounded">
	  </div>
	  <@spring.bind "article.linkedUrl" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="linkedUrl">文章转载路径:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.input path="linkedUrl" class="form-control" id="linkedUrl" placeholder="文章转载路径" />
	    <img src="${article.linkedUrl}" title="文章转载路径" alt="文章转载路径" class="img-rounded">
	  </div>
	   <@spring.bind "article.articleContent.content" /> 
	  <div class="form-group <#if (spring.status.errorMessages?size>0)> has-error </#if>">
	    <label class="control-label" for="articleContent.content">文章内容:
	    <#if (spring.status.errorMessages?size>0)>
				  <#list spring.status.errorMessages as error>  <small>${error}</small><br/> </#list>
			</#if>
	    </label>
	    <@form.textarea path="articleContent.content" class="form-control" id="articleContent_content" placeholder="文章内容" />
	  </div>
	  
	  
	  
	   <div class="col-md-3">
       	<input type="submit" value="保存设置" class="btn btn-primary"/>
       </div>
       <div class="col-md-9">
       <p class="text-info">没有帐号，点击注册新用户 | <a class="text-danger" href='<@spring.url relativeUrl="/loginUI"/>'>已有帐号，点此进入登录页</a></p>
       </div>

</div>
<div class="col-md-2">
    <hr/>
 选择标签：   
 <br/>
    <@listTag tags=tagList />
</div>
	<br/>
	<hr/>
	<input type="button" value="选择分类" class="btn btn-primary"/><br/>
	<#assign depth = 1 />  
	<@asignCategoryForArticleAddedCheckbox children=categoryList />
	<br/>
  </@form.form>
</div>
</div>
<script src="/assets/ckeditor/ckeditor.js"></script>
<script src="/assets/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
$( document ).ready( function() {
	$( 'textarea#articleContent_content' ).ckeditor();
} );

</script>
</body>
</html>