
<#macro stickyNoteCategoryDropdown stickyNoteCategoryList stickyNoteCategory name="name">
<select name=${name} data="data">
		<option value="">ALL</option>
		<#list stickyNoteCategoryList as sc>
	  	<option value="${sc.id}" <#if stickyNoteCategory?? && stickyNoteCategory.id?? && stickyNoteCategory.id == sc.id>selected</#if>>${sc.name}</option>
		</#list>
</select>
</#macro>

<#macro stickyNoteCategorySearch stickyNoteCategory name="name">
<#assign stickyNoteCategoryList=StaticDataDefineManager.getData(StaticDataDefineConstant.STICKYNOTE_CATEGORY_DROPDOWN)/>
<@stickyNoteCategoryDropdown stickyNoteCategoryList=stickyNoteCategoryList stickyNoteCategory=stickyNoteCategory name=name/>
</#macro>

<#macro stickyNoteCategoryIndexNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}','#stickyNoteCategory_content_div')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>
<script src="/assets/default/js/site/jit84j_core.js?t=8"></script>
</#macro>

<#--stickyNoteCategoryEditUI 添加、修改页面 begin-->
<#macro stickyNoteCategoryEditUI stickyNoteCategory page>
<div class="row">
		  <div class="col-md-3">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 随手帖分类类别</p>
		  	  
		  	  <form id="stickyNoteCategory_form" action="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_MAPPING}" method="post">
		  	
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if stickyNoteCategory??>${stickyNoteCategory.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称: 
				</label>
			    <input type="text" data="data" name="name" value="<#if stickyNoteCategory??>${stickyNoteCategory.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="code">代码: 
				</label>
			    <input type="text" data="data" name="code" value="<#if stickyNoteCategory??>${stickyNoteCategory.code}</#if>" class="form-control" id="code" placeholder="输入代码，使用字母，必须唯一" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="imagePath">图标: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图标路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if stickyNoteCategory??>${stickyNoteCategory.imagePath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if stickyNoteCategory??>${stickyNoteCategory.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if stickyNoteCategory??>${stickyNoteCategory.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="openable">是否开放: 
				</label>
			    <input type="checkbox" data="data" name="openable" <#if stickyNoteCategory?? && stickyNoteCategory.openable> checked </#if> value="${stickyNoteCategory.openable}" class="form-control" id="openable"/>
			  </div>
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#stickyNoteCategory_form','#stickyNoteCategory_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#stickyNoteCategory_form')" value='清空'/>
				</li>
		      </ul>
		</div>
		<div class="col-md-9">
		
		<p class="text-primary">随手帖分类类别列表</p>
		<@listStickyNoteCategoryView page=page/>
		</div> 
		<script src="/assets/default/js/site/jit84j_core.js"></script>
</div>
</#macro>
<#--stickyNoteCategoryEditUI 添加、修改页面 end-->

<#macro listStickyNoteCategoryView page>
<#if page?? && page.content??>
		<#assign stickyNoteCategoryList = page.content />
		 <table class="table">
		 <tr>
		 <th>
			 Id
			</th>
		 	<th>
			 名称
			</th>
			 <th>
			 代码
			 </th>
			 <th>
			 图标
			 </th>
			 <th>
			 开放
			 </th>
			 <th>
			 排序
			 </th>
			 <th>
			 描述
			 </th>
			 <th>
			操作
			 </th>
		</tr>
		<#list stickyNoteCategoryList as stickyNoteCategory>  
			 <tr>
			 <td>
				 ${stickyNoteCategory.id}
				</td>
				<td>
				 ${stickyNoteCategory.name}
				</td>
				 <td>
				 ${stickyNoteCategory.code}
				 </td>
				 <td>
				 ${stickyNoteCategory.imagePath}
				 </td>
				 <td>
			 	 <input type="checkbox" data="data" name="openable" <#if stickyNoteCategory?? && stickyNoteCategory.openable>checked</#if> class="form-control" id="openable${stickyNoteCategory.uniqueIdentifier}" />
				 </td>
				 <td>
				  <input type="text" data="data" name="sequence" value="<#if stickyNoteCategory??>${stickyNoteCategory.sequence}</#if>" class="form-control" id="sequence${stickyNoteCategory.uniqueIdentifier}" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
				 </td>
				  <td>
				  ${stickyNoteCategory.description}
				 </td>
				 <td>
				 <ul class="list-inline"> 
					<li>
				 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${stickyNoteCategory.id}" onclick="switchSetting('${stickyNoteCategory.id}','${stickyNoteCategory.id}','${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_EDIT_UI_MAPPING}','#stickyNoteCategory_content_div')">编辑</a>
				  	</li>
				    <li>
				    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${stickyNoteCategory.id}','${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_REMOVE_MAPPING}','#stickyNoteCategory_content_div')">删除</a>
				
				    </li>
				  </ul>
			 </td>
			 </tr>
		</#list>
		</table>
</#if>
</#macro>