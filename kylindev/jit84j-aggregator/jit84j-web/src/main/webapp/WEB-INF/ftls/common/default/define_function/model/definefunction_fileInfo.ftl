
<#-- 文件类型列表 -->
<#macro listFileInfo page editNavigation>
<p class="text-success">文件列表:</p>
 <form commandName="fileInfo" id="fileInfo_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_MAPPING}">
<table class="table">
<#assign systemGalleryList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN)/>
  <thead>

  <tr>
      <td width="3%"></td>
      <td width="20%"><input type="text" data="data" placeholder="路径" size="15" name="fileAccessFullPath" value="<#if fileInfo??>${fileInfo.fileAccessFullPath}</#if>"></td>
      <td width="10%"><input type="text" data="data" placeholder="MIME类型" size="8" name="contentType" value="<#if fileInfo??>${fileInfo.contentType}</#if>"></td>
      <td width="10%"><input type="text" data="data" placeholder="扩展名" size="8" name="fileSuffix" value="<#if fileInfo??>${fileInfo.fileSuffix}</#if>"></td>
      <td width="8%"> </td>
      <td width="6%"> <input type="text" data="data" placeholder="所有者id" size="10" name="owner" value="<#if fileInfo??>${fileInfo.owner}</#if>"></td>
      <td width="10%">
      	<@getOwnerSystemGalleryWithAllForFileInfo fileInfo=fileInfo systemGalleryList=systemGalleryList name="systemGallery.id"/>
      </td>
      <td width="10%"> </td>
      <td width="8%"><input type="button" value="搜索" class="btn btn-primary" onclick="formSubmitAjax(1)"/></td>
    </tr>
    <#-- -->
    <tr>
     	  <th>Id</th>
	      <th>文件路径</th>
	      <th>MIME类型</th>
	      <th>扩展名</th>
	      <th>文件</th>
	      <th>所有者</th>
	      <th>所属分类</th>
	      <th>自定义分类</th>
	      <th>操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page.content??)>
  	 <#assign depth = 1 />  
	<@listFileInfoItem page=page systemGalleryList=systemGalleryList/>
  </#if>
	
	<#--
	<tr>
		<td colspan="8">
			<a class="btn btn-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
			保存列表
			</a>
		</td>
		
	 </tr>
	-->
   </tbody>
 </table>
  </form>
</#macro>

<#macro listFileInfoItem page systemGalleryList>
	<#if page?? && page.content??>
	<#assign fileInfoList = page.content />
		<#list fileInfoList as fileInfo>  
			 <#assign depth = depth + 1 />
			 <tr>
				 <td>
				 ${fileInfo.uniqueIdentifier}
				 </td>
				 <td>${fileInfo.fileAccessFullPath}
				<#-- <input type="text" name="fileInfoList[${fileInfo_index}].fileAccessFullPath" value="${fileInfo.fileAccessFullPath}" class="form-control" id="fileAccessFullPath[${fileInfo_index}]" placeholder="文件路径" size="60"/>
				 -->
				 </td>
				 <td>
				 ${fileInfo.contentType}
				 <#--<input type="text" name="fileInfoList[${fileInfo_index}].contentType" value="${fileInfo.contentType}" class="form-control" id="contentType[${fileInfo_index}]" placeholder="文件类型" size="5"/>
				  --></td>
				  <td>
				 ${fileInfo.fileSuffix}
				 </td>
				 <td>
				 <img src="${fileInfo.fileAccessFullPath}" title="${fileInfo.fileName}" alt="${fileInfo.fileAccessFullPath}" class="img-rounded" width="120px">
				 </td>
				  <td>
				  ${fileInfo.owner}
				 </td>
				 <td>
				  <#if systemGalleryList??>
				  <@getOwnerSystemGalleryNoData fileInfo=fileInfo systemGalleryList=systemGalleryList name="systemGallery.id"/>
				  </#if>
				 </td>
				 <td>
				  <#if systemGalleryList??>
				  <@getOwnerSystemGalleryNoData fileInfo=fileInfo systemGalleryList=systemGalleryList name="systemGallery.id"/>
				  </#if>
				 </td>
				 <td>
				 	<ul class="list-inline"> 
						<li>
					 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${fileInfo.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_UI_MAPPING}')">编辑</a>
					  	</li>
					    <li>
					    	<a class="text-danger" href='javascript:void(0)' onclick="removeItem('${fileInfo.id}','${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_DELETE_MAPPING}','#fileSystemGallery_content_div')">删除</a>
					    </li>
					  </ul>
				 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<tr>
		<td colspan="8">
		<@pageBarAjax page=page url="${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_LIST_UI_MAPPING}" formId='fileInfo_list_form' backDataContainerId="fileSystemGallery_content_div"/>
		</td>
		</tr>
	</#if>
	
	 <script type="text/javascript">
		 			function cleanForm(formId){
		 				if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				if($(this).attr("type")=="checkbox"){
				  					$(this).attr("checked",false);
				  				}else{
				  					$(this).attr("value","");
				  				}
				  			});
			  			}
		 			}
		 			function submit(formId,backDataContainerId){
		 				var request = new Object();
			  			if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				
				  				if($(this).attr("type")=="checkbox"){
				  					if($(this).attr('checked')==undefined){
				  						request[$(this).attr("name")]='false';
				  					}else{
				  						request[$(this).attr("name")]='true';
				  					}
				  					alert($(this).attr("checked"));
				  				}else{
				  					request[$(this).attr("name")]=$(this).attr("value");
				  				}
				  			});
			  			}
			  			var _select = formId+" select[data$='data']";
			  			if($(_select).length>0){
				  			$(_select).each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  				//alert($(this).attr("value"));
				  			});
			  			}
			  			
						var action = $(formId).attr("action");
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
					function removeItem(id,action,backDataContainerId){
						if(id==''){
							alert("没有id");
							return;
						}
						var request = {'id':id};
						
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
					
		 </script> 
</#macro>


<#--fileMimeUI 添加、修改页面 begin-->
<#macro fileInfoAddUI fileMime>
<div class="row">
<#assign systemGalleryList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_DROPDOWN)/>
		  <div class="col-md-4">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 文件信息</p>
		  	  
		  	  <form id="fileMime_form" action="${KylinboyDeveloperConstant.KYLINBOY_FILEMIME_ADD_MAPPING}" method="post">
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if fileMime??>${fileMime.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称【不能重复】: 
				</label>
			    <input type="text" data="data" name="name" value="<#if fileMime??>${fileMime.name}</#if>" class="form-control" id="name" placeholder="输入名称，必须唯一" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="suffix">扩展名: 
				</label>
			  <input type="text" data="data" name="suffix" value="<#if fileMime??>${fileMime.suffix}</#if>" class="form-control" id="suffix" placeholder="输入扩展名"/>
			  </div>
			   <div class="form-group ">
			    <label class="control-label" for="mime">MIME: 
				</label>
			  <input type="text" data="data" name="mime" value="<#if fileMime??>${fileMime.mime}</#if>" class="form-control" id="mime" placeholder="输入MIME类型"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if fileMime??>${fileMime.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">所属分类: 
				</label>
				<@getOwnerSystemGallery fileMime=fileMime systemGalleryList=systemGalleryList name="systemGallery.id"/>
			  </div>
			  <div class="form-group">
			    <label class="control-label" for="allowable">是否允许上传: 
				</label>
			    <input type="checkbox" data="data" name="allowable" <#if fileMime?? && fileMime.allowable>checked</#if> class="form-control" id="allowable" />
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if fileMime??>${fileMime.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#fileMime_form','#fileSystemGallery_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      	<li>
				   <input type="button" class="btn btn-primary" onClick="cleanForm('#fileMime_form')" value='清空'/>
				</li>
		      </ul>
		</div>	 
		<div class="col-md-8">
		  	  <@listFileMime page=page editNavigation=editNavigation/>
		 </div>	
		
</div>
</#macro>
