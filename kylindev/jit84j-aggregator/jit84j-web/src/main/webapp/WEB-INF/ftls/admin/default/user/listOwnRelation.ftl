<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>用户登录</title>
</head>
<body>
<div class="container">
<div class="row">
<div class="col-md-9">
<p class="text-success">站长关系列表:</p>
<div class="table-responsive">
  <table class="table">
  <thead>
    <tr>
    <th width='10%'>
    <div><input type="checkbox" name="select_ownRelation_all" value="all" class="form-control" id="select_ownRelation_all" />全选
    </div>
    </th>
      <th>Id</th>
      <th>代码</th>
      <th>名称</th>
      <th>序号</th>
    </tr>
  </thead>
  <@spring.bind "ownRelationList" />  
  <@form.form commandName="ownRelationList" role="form" action="/modifyOwnRelationList">
  <tbody>
  <#if (ownRelationList??)>

	<#list ownRelationList as ownRelation>  


	 <tr>
	 <td><input type="checkbox" name="select_ownRelation_${ownRelation_index}" value="${ownRelation.uniqueIdentifier}" class="form-control" id="select_ownRelation_${ownRelation_index}" />
	 </td>
	 
	 <td><input type="text" readonly="true" name="ownRelationList[${ownRelation_index}].id" value="${ownRelation.uniqueIdentifier}" class="form-control" id="uniqueIdentifier[${ownRelation_index}]" />
	 </td>
	 <td>
	 <input type="text" name="ownRelationList[${ownRelation_index}].code" value="${ownRelation.code}" class="form-control" id="code[${ownRelation_index}]" placeholder="输入代码"/>
	 </td>
	 <td>
	 <input type="text" name="ownRelationList[${ownRelation_index}].name" value="${ownRelation.name}" class="form-control" id="name[${ownRelation_index}]" placeholder="输入名称"/>
	 </td>
	 <td>
	 <input type="text" name="ownRelationList[${ownRelation_index}].sequence" value="${ownRelation.sequence}" class="form-control" id="sequence[${ownRelation_index}]" placeholder="输入序号"/>
	 </td>
	 </tr>
	</#list>
	
  </#if>
   </tbody>
  </table>
</div>
       <div class="col-md-3">
       <input type="submit" value="保 存" class="btn btn-primary"/>
       </div>
        <div class="col-md-3">
       <input type="button" value="删 除" class="btn btn-primary"/>
       </div>
       <div class="col-md-3">
        <a class="text-danger" href='<@spring.url relativeUrl="/modifyOwnRelationUI"/>'><input type="button" value="添 加" class="btn btn-primary"/></a>
       </div>
 </div>
 </@form.form>
  <div class="col-md-6">
  
  </div>
</div>
</div>
<script type="text/javascript">
//alert("成功");
$(document).ready(function(){
var length = $("form:checkbox").length;
alert(length);
});
</script>
</body>
</html>