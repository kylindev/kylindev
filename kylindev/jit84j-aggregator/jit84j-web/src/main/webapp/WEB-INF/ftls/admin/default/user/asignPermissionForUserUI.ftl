<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_role.ftl">
<#include "definefunction_permission.ftl">
<#include "definefunction_user.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>为角色分配权限</title>

</head>

<div class="container" id="top_login">
	  <@loginAndLogout />
</div>

<div class="container" id="top_banner">
	  <@commonBanner/>
</div>

<div class="container" id="top_menu">
	  <@adminNavigation navigationList=navigationTopList currentNavigation=currentNavigation/>
</div>

<body>
<div class="container" id="body_content">
<script src="/assets/common/js/jquery/jquery-ui.custom.js" type="text/javascript"></script>
<script src="/assets/common/js/jquery/jquery.cookie.js" type="text/javascript"></script>

<link href="/assets/common/js/jquery/dynatree/skin/ui.dynatree.css" rel="stylesheet" type="text/css" id="skinSheet">
<script src="/assets/common/js/jquery/dynatree/jquery.dynatree.js" type="text/javascript"></script>
<div class="row">
<@form.form commandName="user" role="form" id="user_permission_form_id" action=KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_UI_MAPPING>
	<div class="col-md-5" >
	     <div class="panel panel-warning">
			 <div class="panel-heading ">第一步：选择用户</div>
			 <div id="selectedUserDiv"> 
			 </div>
		</div>
	</div>
	<div class="col-md-7">
			<div class="panel panel-warning" >
				<div class="panel-heading ">第二步：选择权限</div>
				<@permissionCategoryListIncludePermissionTreeForRole page=page  removeUrl=KylinboyDeveloperConstant.KYLINBOY_PERMISSION_CATEGORY_REMOVE_MAPPING/>
			 </div>
			 <script type="text/javascript">
				   var _permissionIdList = new Array();
				   function selectedAllNodes(flag,node){
				   		if(node != null){
				   			if(flag){
				   				$(node.span).addClass("dynatree-selected");
				   				node.bSelected=true;
				   			}else{
				   				$(node.span).removeClass("dynatree-selected");
				   				node.bSelected=false;
				   			}
					   		
					   		if(node.childList != null && node.childList.length>0){
				                	for(var i=0;i<node.childList.length;i++){
				                		selectedAllNodes(flag,node.childList[i]);
				                	}
				             }
			             }
			             
				   }
				    $("#tree_permission").dynatree({
			        	 title: "permission_category_asign", 
			        	 minExpandLevel: 2,
			        	 checkbox: true, // Show checkboxes.
			        	 classNames: {checkbox: "dynatree-checkbox"},
			  			 selectMode: 2, // 1:single, 2:multi, 3:multi-hier
			  			 autoFocus: true,
			             onActivate: function(node) {
				             if($("#permission_category_id").length>0){
				              	var _current_id = $(node.data.title).filter(":input").val();
				             	$("#permission_category_id").val(_current_id);
				             }
			            },
			            onClick: function(flag,node){
			            	//如果不是顶级节点
			                if(node.parent != null && node.parent.parent!=null){
			                	//alert(node.parent);
			                }else if(node.parent != null && node.parent.parent==null){
			                	//如果是顶级节点
			                	//alert("顶级节点");
			                	
			                	var spanList = node.span.childNodes;
			                }
			            },
			            onSelect: function(flag, node) {
				            selectedAllNodes(flag,node);
				        }
			        });
			        
			        
			       function initSelectedUser(){
					       $.ajax({
								url: '${KylinboyDeveloperConstant.KYLINBOY_SELECTED_USER_UI_MAPPING}',
								data: '',
								type: "POST",
								dataType : "text",
								contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
								success: function(data) {
											$("#selectedUserDiv").html(data);
										
								},
								error: function() {
									alert( "Sorry, there was a problem!" );
								},
								complete: function() {
									//alert('complete');
								}
							}); 
					}
					initSelectedUser();
			 </script>
	</div>
	
</@form.form>
	
</div>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-warning" >
						<div class="panel-heading ">第三步：确认提交</div>
						<button type="button" class="btn btn-primary" onclick='addPermission()'>添加权限</button>
						<button type="button" class="btn btn-primary" onclick='removePermission()'>移除所选权限</button>
						<button type="button" class="btn btn-primary" onclick='removeAllPermission()'>移除所有权限</button>
		
		<script type="text/javascript">
			function addPermission(){
				var url = '${KylinboyDeveloperConstant.KYLINBOY_USER_ASIGN_PERMISSION_MAPPING}';
				addOrRemovePermission(url);
			}
			
			function removePermission(){
				var url = '${KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_PERMISSION_MAPPING}';
				addOrRemovePermission(url);
			}
			
			function removeAllPermission(){
				var url = '${KylinboyDeveloperConstant.KYLINBOY_USER_REMOVE_ALL_PERMISSION_MAPPING}';
				var userRadio =$(":radio:checked");
				var userId = -1;
				if(userRadio.length>0){
					 userId = userRadio.val();
				}else{
					alert("没有选择用户");
					return;
				}
				if(confirm("确定要移除该用户的所有权限吗？")){
					var data={"id":userId};
					submitOpationForUser(data,url);
				}
			}
			
			function addOrRemovePermission(url){
				var selectedNodes = $("#tree_permission").dynatree("getTree").getSelectedNodes();
				if(selectedNodes != null && selectedNodes.length>0){
					var userRadio =$(":radio:checked");
					var userId = -1;
					if(userRadio.length>0){
						 userId = userRadio.val();
					}else{
						alert("没有选择用户");
						return;
					}
					var selectedKeys = $.map(selectedNodes, function(node){
						var _key = node.data.key;
		   		 		if(_key != null && _key !=''){
		   		 			if(_key.indexOf('permission_id_')>-1){
		   		 				var permissionId = $("#"+_key).val();
		   		 				return permissionId;
		   		 			}
		   		 		}
					});
					var permissionIds = selectedKeys.join(",");
					
					var data={
						"id":userId,
						"permissionIds":permissionIds
					}
					
					submitOpationForUser(data,url)
				}else{
					alert("没有选择权限");
					return;
				}
			}
			
			function submitOpationForUser(data,url){
			       $.ajax({
						url: url,
						data: data,
						type: "POST",
						dataType : "text",
						contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
						success: function(data) {
									alert(data);
									initSelectedUser();
						},
						error: function() {
							alert( "Sorry, there was a problem!" );
						},
						complete: function() {
							//alert('complete');
						}
					}); 
			}
			
			
		</script>
		</div>
	</div>
</div>
</div>

</body>

<div id="body_bottom">
<img width="980" height="90" border="0" src="http://news.baihe.com/images/20131012_980x90.jpg"/>
</div>
<div class="container" id="body_footer">
<@commonFooter/>
</div>
</html>