<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "df_framework.ftl">
<#include "definefunction_login.ftl">
<#include "definefunction_banner.ftl">
<#include "definefunction_menu.ftl">
<#include "definefunction_footer.ftl">
<#include "definefunction_page.ftl">
<#include "definefunction_upload.ftl">
<#include "definefunction_travel.ftl">
<#include "definefunction_manifesto.ftl">

<div id="body_content_main_screen_first"><!--主屏元素展示一区-->
	<div class="row">	
			<div class="col-md-5">
				上一篇: <#if previous??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING}/${previous.id}"> <@localSubstring str=previous.decription max=20/> </a> by ${previous.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-5">
				下一篇: <#if following??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING}/${following.id}"> <@localSubstring str=following.decription max=20/> </a> by ${following.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-2">
			
			</div>
	</div>
	<div class="row">	
		<div class="col-md-10">
			<div class="panel panel-default" style="margin-bottom:0px">
			
			<h4>
				<ul class="list-inline">
					<li><a href="" title="查看 ${current.nickName} 的主页"> ${current.nickName} </a></li>
					<#--<li><small> in </small> ${current.place} </li>-->
					<li><small> at </small> ${current.publishDate} </li>
					
				</ul>
			</h4>
			<h2 class="text-danger">
				宣 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;言
			</h2>	
				  <div class="panel-body">
					
					<div class="row">	
							<div class="col-md-12">
								
								<div class="panel panel-warning">
								  <div class="panel-heading">
								  	<h3 class="text-left">${current.decription}<#if current.praiseCount gt 0 ><small>赞[${current.praiseCount}]</small></#if></h3>
								  	<span class="glyphicon glyphicon-comment"></span> 评论区(<span class="span-comment-total-${current.id}"> ${current.commentCount} </span>) | 
								  	<a href="javascript:void(0)" class="text-info" onclick="addComment('${current.id}','1')"><span class="glyphicon glyphicon-pencil"></span>点此添加评论</a> | 
								  	<a href="javascript:void(0)" class="praise-instantShare-a" id="praise_a_${current.id}_2" onclick="praiseManifestoWithCallBack('${current.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING}','#praise_${current.id}_2','#praise_a_${current.id}_2')">
								  		<span class="text-danger"><span class="glyphicon glyphicon-heart"></span>点赞(<span class="praise-instantShare" id="praise_${current.id}_2"> ${current.praiseCount?default(0)} </span>)</span>
								  	</a> | 
								  	<a href="javascript:void(0)" class="witness-a" id="witness_a_${current.id}_2" onclick="witnessManifestoWithCallBack('${current.id}','${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_EDIT_PRAISE_MAPPING}','#witness_${current.id}_2','#witness_a_${current.id}_2')"><span class="text-danger"><span class="glyphicon glyphicon-heart"></span>见证(<span class="praise-witness" id="praise_${current.id}_2"> ${current.witnessCount?default(0)} </span>)</span>
								  	</a>
								  </div>
								  <div class="panel-body text-left" id="span_comment_id_${current.id}">
								  	<@listCommentUIByManifesto current=current containerId="span_comment_id_"+current.id/>
								  </div>
								</div>
							</div>
					</div>
				  </div>
			</div>
		</div>
		<div class="col-md-2">
			<!--详细页面侧边广告区 begin-->
			<img src="http://image.songtaste.com/images/usericon/s/17/3800417.JPG" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/11/3989011.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/s/56/446456.jpg" class="icon">
			<img src="http://image.songtaste.com/images/usericon/l/20/4523320.jpg" border="0" class="icon">
			<!--详细页面侧边广告区 end-->
		</div>
	</div>
	
	
	<div class="row">	
			<div class="col-md-5">
				上一篇: <#if previous??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING}/${previous.id}"> <@localSubstring str=previous.decription max=20/> </a> by ${previous.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-5">
				下一篇: <#if following??><a href="${KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_VIEW_DETAIL_MAPPING}/${following.id}"> <@localSubstring str=following.decription max=20/> </a> by ${following.nickName}<#else>没有了</#if>
			</div>
			<div class="col-md-2">
			
			</div>
	</div>
	
	<@addCommentUI url=KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD2_MAPPING modelIdProperty='manifesto.id' position='1' appendValue=-1/>
	
	<@addCommentUI url=KylinboyTravelDeveloperConstant.KYLINBOY_MANIFESTO_COMMENT_ADD2_MAPPING modelIdProperty='parent.id' position='2' appendValue=1/>
		
	<script type="text/javascript">
		function praiseManifestoWithCallBack(manifestoId,url,containerId,praiseButtonId){
			praiseWithCallBack(manifestoId,url,containerId,praiseButtonId,function(data){
				$(".praise-instantShare").html(data);
				$(".praise-instantShare-a").each(function(){
					this.onclick=null;
				});
			});
		}
	</script>
	
</div>
