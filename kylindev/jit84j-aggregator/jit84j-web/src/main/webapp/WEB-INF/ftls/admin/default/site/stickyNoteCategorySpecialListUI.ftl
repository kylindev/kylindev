<#import "spring.ftl" as spring />
<#include "taglib.ftl">
<#include "definefunction_stickyNoteCategorySpecial.ftl">
<#include "definefunction_stickyNoteCategory.ftl">
<#include "definefunction_specialArea.ftl">
<#include "definefunction_navigation.ftl">
<#include "definefunction_page.ftl">
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="zh-CN">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>随手贴帖子列表-随手帖管理</title>
</head>

<body>
<div class="container" id="body_content">
	<div class="row">
		<div class="col-md-12">
			<@horizontalNavigationUI currentNavigation=currentNavigation isAjax='false'/>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="stickyNoteCategorySpecial_content_div">
			<form action="${KylinboyDeveloperConstant.KYLINBOY_STICKY_NOTE_CATEGORY_SPECIAL_LIST_UI_MAPPING}" id="stickyNotCategorySpecial_list_form" method="post">
				<div class="panel panel-warning" id="specialArea_list_div">
					<div class="panel-heading ">多条件查询</div>
					<div class="panel-body"> 
					<label>特殊区：
					<@specialAreaSearch specialArea=specialArea name="categorySpecial.specialArea.id"/>
					</label>
					<label>类别：
					<@stickyNoteCategorySearch stickyNoteCategory=stickyNoteCategory name="categorySpecial.category.id"/>
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="stickyNote.title" value="<#if stickyNoteCategorySpecial?? && stickyNoteCategorySpecial.stickyNote??>${stickyNoteCategorySpecial.stickyNote.title}</#if>" class="form-control" id="title" placeholder="输入标题" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="stickyNote.author" value="<#if stickyNoteCategorySpecial?? && stickyNoteCategorySpecial.stickyNote??>${stickyNoteCategorySpecial.stickyNote.author}</#if>" class="form-control" id="author" placeholder="输入作者" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="stickyNote.summary" value="<#if stickyNoteCategorySpecial?? && stickyNoteCategorySpecial.stickyNote??>${stickyNoteCategorySpecial.stickyNote.summary}</#if>" class="form-control" id="summary" placeholder="输入概要" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="stickyNote.imagePath" value="<#if stickyNoteCategorySpecial?? && stickyNoteCategorySpecial.stickyNote??>${stickyNoteCategorySpecial.stickyNote.imagePath}</#if>" class="form-control" id="summary" placeholder="输入图片路径" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="categorySpecial.name" value="<#if stickyNoteCategorySpecial?? && stickyNoteCategorySpecial.categorySpecial??>${stickyNoteCategorySpecial.categorySpecial.name}</#if>" class="form-control" id="name" placeholder="输入类别别名" />
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="minLevel" value="<#if stickyNoteCategorySpecial??>${stickyNoteCategorySpecial.minLevel}</#if>" class="form-control" id="name" placeholder="最小级别，数字0,1,2,3..." />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="maxLevel" value="<#if stickyNoteCategorySpecial??>${stickyNoteCategorySpecial.maxLevel}</#if>" class="form-control" id="name" placeholder="最大级别，数字6,7,8,9..." />
					</label>
					<br/>
					<label class="control-label">
					<input type="text" data="data" name="minPublishTime" value="<#if stickyNoteCategorySpecial??>${stickyNoteCategorySpecial.minPublishTime}</#if>" class="form-control" id="minPublishTime" placeholder="起始发布时间" />
					</label>
					<label class="control-label">
					<input type="text" data="data" name="maxPublishTime" value="<#if stickyNoteCategorySpecial??>${stickyNoteCategorySpecial.maxPublishTime}</#if>" class="form-control" id="maxPublishTime" placeholder="终止发布时间" />
					</label>
					<button class="btn btn-primary" onClick="formSubmit(1)">搜索</button>
					</div>
				</div>
					
				<div class="panel panel-warning" id="specialArea_list_div">
					<div class="panel-heading ">列表页面</div>
					<div class="panel-body"> 
						<@listStickyNoteCategorySpecialView page=page formId="stickyNotCategorySpecial_list_form"/>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
</script>
</body>
</html>