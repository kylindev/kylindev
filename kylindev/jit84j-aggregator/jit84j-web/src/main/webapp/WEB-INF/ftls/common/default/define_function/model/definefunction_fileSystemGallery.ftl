<#macro getOwnerSystemGalleryWithAllForFileInfo fileInfo systemGalleryList name="systemGallery.id">
<select name=${name} data="data">
		<option value="">ALL</option>
		<#list systemGalleryList as gallery>
	  	<option value="${gallery.id}" <#if fileInfo?? && fileInfo.systemGallery?? && fileInfo.systemGallery.id == gallery.id>selected</#if>>${gallery.name}</option>
		</#list>
</select>
</#macro>
<#macro getOwnerSystemGalleryNoData fileInfo systemGalleryList name="systemGallery.id">
<select name=${name} >
		<#list systemGalleryList as gallery>
	  	<option value="${gallery.id}" <#if fileInfo?? && fileInfo.systemGallery?? && fileInfo.systemGallery.id == gallery.id>selected</#if>>${gallery.name}</option>
		</#list>
</select>
</#macro>

<#macro getOwnerSystemGalleryWithAll fileMime systemGalleryList name="systemGallery.id">
<select name=${name} data="data">
		<option value="">ALL</option>
		<#list systemGalleryList as gallery>
	  	<option value="${gallery.id}" <#if fileMime?? && fileMime.systemGallery?? && fileMime.systemGallery.id == gallery.id>selected</#if>>${gallery.name}</option>
		</#list>
</select>
</#macro>
<#macro getOwnerSystemGallery fileMime systemGalleryList name="systemGallery.id">
<select name=${name} data="data">
		<#list systemGalleryList as gallery>
	  	<option value="${gallery.id}" <#if fileMime?? && fileMime.systemGallery?? && fileMime.systemGallery.id == gallery.id>selected</#if>>${gallery.name}</option>
		</#list>
</select>
</#macro>

<#macro getSystemGalleryCodeDropdown systemGallery name="code">
<select name=${name} data="data">
		<#list FileSystemGallery.codeList as galleryCode>
	  	<option value="${galleryCode}" <#if systemGallery?? && systemGallery.code == galleryCode> selected </#if> > ${galleryCode}</option>
		</#list>
</select>
</#macro>

<#-- 文件系统分类列表 -->
<#macro listFileSystemGallery page editNavigation>
<#assign systemGalleryTypeList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_TYPE_DROPDOWN)/>

<p class="text-success">文件系统分类列表:</p>
  <@form.form commandName="fileSystemGallery" id="fileSystemGallery_list_form" role="form" action="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING}">
<table class="table">
  <thead>
  <#--
  <tr>
      
      <td width="3%"></td>
      <td width="10%"><input type="text" placeholder="名称" size="5" name="name" value="<#if fileSystemGallery??>${fileSystemGallery.name}</#if>"></td>
      <td width="15%"><input type="text" placeholder="代码" size="5" name="code" value="<#if fileSystemGallery??>${fileSystemGallery.code}</#if>"></td>
      <td width="15%">
      <@getFileSystemGalleryTypeDropdownWithAll fileSystemGallery=fileSystemGallery systemGalleryTypeList=systemGalleryTypeList/>
      <td width="15%">
      <input type="text" placeholder="存储路径" size="5" name="storedPath" value="<#if fileSystemGallery??>${fileSystemGallery.storedPath}</#if>">
      </td>
      <td width="15%"><input type="text" placeholder="图标路径" size="5" name="storedPath" value="<#if fileSystemGallery??>${fileSystemGallery.imagePath}</#if>"></td>
      <th width="8%">排序</th>
      <td width="15%"><input type="button" value="搜索" class="btn btn-primary" onclick="formSubmitAjax(1)"/></td>
    </tr>
    -->
    <tr>
      <th width="3%">Id</th>
      <th width="10%">名称</th>
      <th width="15%">代码</th>
      <th width="15%">类型</th>
      <th width="15%">存储路径</th>
      <th width="15%">图标路径</th>
      <th width="8%">排序</th>
      <th width="15%">操作</th>
    </tr>
  </thead>
  <tbody>
  
  <#if (page.content??)>
  	 <#assign depth = 1 />  
	<@listFileSystemGalleryItem page=page editNavigation=editNavigation/>
  </#if>
	<tr>
		<td colspan="8">
			<a class="btn btn-primary" href='<@spring.url relativeUrl="${KylinboyDeveloperConstant.KYLINBOY_ADD_USER_UI_MAPPING}"/>'>
				保存列表
			</a>
		</td>
		
	 </tr>
	
   </tbody>
 </table>
  </@form.form>
 <script type="text/javascript">
 function removeItem(id,action,backDataContainerId){
	if(id==''){
		alert("没有id");
		return;
	}
	var request = {'id':id};
	
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
 </script>
</#macro>

<#macro listFileSystemGalleryItem page editNavigation>
	<#if page?? && page.content??>
	<#assign fileSystemGalleryList = page.content />
		<#list fileSystemGalleryList as fileSystemGallery>  
			 <#assign depth = depth + 1 />
			 
			 <tr>
			 <td>
			 ${fileSystemGallery.uniqueIdentifier}
			 </td>
			 <td>
			 ${fileSystemGallery.name}
			 </td>
			 <td>
			 ${fileSystemGallery.code}
			 </td>
			 <td>
			 <#assign typeName = 'fileSystemGalleryList['+fileSystemGallery_index+'].type' />
			 <@getFileSystemGalleryTypeDropdown fileSystemGallery=fileSystemGallery systemGalleryTypeList=systemGalleryTypeList name=typeName/>
			 ${fileSystemGallery.type}
			 </td>
			  <td>
			  ${fileSystemGallery.storedPath}
			 </td>
			 <td>
			 ${fileSystemGallery.imagePath}
			 </td>
			 <td>
			 <input type="text" data="data" name="fileSystemGalleryList[${fileSystemGallery_index}].sequence" value="<#if fileSystemGallery??>${fileSystemGallery.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			 </td>
			 <td>
			 <ul class="list-inline"> 
				<li>
			 		<a class="text-danger" href='javascript:void(0)' id="left_navigation_${editNavigation.id}" onclick="switchSetting('${fileSystemGallery.id}','${editNavigation.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_UI_MAPPING}')">编辑</a>
			  	</li>
			    <li>
			    <a class="text-danger" href='javascript:void(0)' onclick="removeItem('${fileSystemGallery.id}','${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_REMOVE_MAPPING}','#fileSystemGallery_content_div')">删除</a>
			
			    </li>
			  </ul>
			 </td>
			 </tr>
			 <#assign depth = depth-1 />
		</#list>
		<tr>
		<td colspan="8">
		<@pageBarAjax page=page url="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_LIST_UI_MAPPING}" formId='fileSystemGallery_list_form' backDataContainerId="fileSystemGallery_content_div"/>
		</td>
		</tr>
	</#if>
</#macro>

<#--fileSystemGalleryUI 添加、修改页面 begin-->
<#macro fileSystemGalleryAddUI fileSystemGallery>
<#assign systemGalleryTypeList=StaticDataDefineManager.getData(StaticDataDefineConstant.FILE_SYSTEM_GALLERY_TYPE_DROPDOWN)/>
<div class="row">
		  <div class="col-md-6">
		 	  <p class="text-danger">${msg}</p>
		  	  <p class="text-primary">添加/修改 系统分类</p>
		  	  
		  	  <form id="fileSystemGallery_form" action="${KylinboyDeveloperConstant.KYLINBOY_FILE_SYSTEM_GALLERY_ADD_MAPPING}" method="post">
		  	  <input type="hidden" data="data" id="id" name="id" value="<#if fileSystemGallery??>${fileSystemGallery.id}</#if>">
		      <div class="form-group ">
			    <label class="control-label" for="name">名称: 
				</label>
			    <input type="text" data="data" name="name" value="<#if fileSystemGallery??>${fileSystemGallery.name}</#if>" class="form-control" id="name" placeholder="输入名称" required="true"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="code">代码【必须与系统预定义的相同】: 
				</label>
				<@getSystemGalleryCodeDropdown systemGallery=fileSystemGallery/>
			    <#--
			    <input type="text" data="data" name="code" value="<#if fileSystemGallery??>${fileSystemGallery.code}</#if>" class="form-control" id="code" placeholder="输入代码，使用字母，必须唯一" required="true"/>
			  	-->
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="imagePath">图标: 
				</label>
			    <input type="text" data="data" name="imagePath" class="form-control" id="imagePath" placeholder="输入图标路径" />
			 	
			 	<input type="button" value="点击上传" class="btn btn-info" onclick="openWindow('${KylinboyDeveloperConstant.KYLINBOY_FILEINFO_UPLOAD_UI_MAPPING}?type=${FileSystemGallery.CODE_IMAGE}','iconPath')"/><br/>
			    <div id="iconPath_img">
			    	<img src='<#if fileSystemGallery??>${fileSystemGallery.iconPath}</#if>'/>
			    </div>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="code">存储路径: 
				</label>
			    <input type="text" data="data" name="storedPath" value="<#if fileSystemGallery??>${fileSystemGallery.storedPath}</#if>" class="form-control" id="storedPath" placeholder="输入存储路径" />
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="sequence">顺序: 
				</label>
			    <input type="text" data="data" name="sequence" value="<#if fileSystemGallery??>${fileSystemGallery.sequence}</#if>" class="form-control" id="sequence" placeholder="输入顺序，使用数字1,2...10,12" required="true"/>
			  </div>
			  
			  <div class="form-group ">
			    <label class="control-label" for="description">描述: 
				</label>
			    <input type="text" data="data" name="description" value="<#if fileSystemGallery??>${fileSystemGallery.description}</#if>" class="form-control" id="description" placeholder="输入描述"/>
			  </div>
			  <div class="form-group ">
			    <label class="control-label" for="type">类型: 
				</label>
				<@getFileSystemGalleryTypeDropdown fileSystemGallery=fileSystemGallery systemGalleryTypeList=systemGalleryTypeList/>
				<#--
			   	<input type="text" data="data" name="type" value="<#if fileSystemGallery??>${fileSystemGallery.type}</#if>" class="form-control" id="type" placeholder="输入类型，必须为数字，且唯一"/>
			  	-->
			  </div>
			  
		      </form>
			 <ul class="list-inline">
		       	<li><input type="button" onClick="submit('#fileSystemGallery_form','#fileSystemGallery_content_div')" value="保存分类" class="btn btn-primary"/></li>
		      </ul>
		</div>	 
		<div class="col-md-6">
		  	  <p class="text-primary">已存在的系统分类列表</p> 
		  	  <@listFileSystemGalleryView page=page/>
		 </div>	
		 <script type="text/javascript">
		 			function submit(formId,backDataContainerId){
		 				var request = new Object();
			  			if($("input[data$='data']").length>0){
				  			$("input[data$='data']").each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  			});
			  			}
			  			var _select = formId+" select[data$='data']";
			  			if($(_select).length>0){
				  			$(_select).each(function(){
				  				request[$(this).attr("name")]=$(this).attr("value");
				  				//alert($(this).attr("value"));
				  			});
			  			}
						var action = $(formId).attr("action");
						$.ajax({
							url: action,
							data: request,
							type: "POST",
							dataType : "text",
							contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
							success: function(data) {
								if($(backDataContainerId).length>0){
										$(backDataContainerId).html(data);
								}
									
							},
							error: function() {
								alert( "Sorry, there was a problem!" );
							},
							complete: function() {
								//alert('complete');
							}
						});
					}
		 </script> 
</div>
</#macro>
<#--fileSystemGalleryUI 添加、修改页面 end-->

<#macro fileSystemGalleryNavigationUI currentNavigation>
<div id="treeData_left_navigation" class="list-group">
	<#if currentNavigation?? && currentNavigation.childrenNavigation?? && currentNavigation.childrenNavigation?size gt 0>
	 <#assign navigationList = currentNavigation.childrenNavigation />  
		 <ul class="list-inline">
		  	<#list navigationList as navigation> 
			  	<#if navigation?? >
			  	<li>
			  	<a href="javascript:void(0)" class="list-group-item <#if 0==navigation_index>active</#if>" id="left_navigation_${navigation.id}" onclick="switchSetting('','${navigation.id}','${navigation.url}')">
			 		${navigation.name}
			 	</a>
			 	</li>
			  	</#if>
		  	</#list>
		  </ul>
	 </#if>
</div>

<script type="text/javascript">
function switchSetting(id,navigationId,url){
	var listItem = $(".list-group-item");
	var _id="left_navigation_"+navigationId;
	if(listItem.length>0){
		$(listItem).each(function(){
			if(_id==$(this).attr("id")){
				$(this).addClass("active");
			}else{
				$(this).removeClass("active");
			}
		});
	}
	var request={'id':id};
	getContent(url,request);
}

function getContent(url,request){
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			$("#fileSystemGallery_content_div").html(data);
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
</script>
</#macro>

<#macro listFileSystemGalleryView page>
<#if page?? && page.content??>
		<#assign fileSystemGalleryList = page.content />
		 <table class="table">
		 <tr>
		 	<th>
			 名称
			</th>
			 <th>
			 代码
			 </th>
			 <th>
			 类型
			 </th>
			 <th>
			 存储路径
			 </th>
		</tr>
		<#list fileSystemGalleryList as fileSystemGallery>  
			 <tr>
				<td>
				 ${fileSystemGallery.name}
				</td>
				 <td>
				 ${fileSystemGallery.code}
				 </td>
				 <td>
				 
				 <@getFileSystemGalleryTypeDropdown fileSystemGallery=fileSystemGallery systemGalleryTypeList=systemGalleryTypeList/>
				 
				 </td>
				 <td>
				  ${fileSystemGallery.storedPath}
				 </td>
			 </tr>
		</#list>
		</table>
</#if>
</#macro>