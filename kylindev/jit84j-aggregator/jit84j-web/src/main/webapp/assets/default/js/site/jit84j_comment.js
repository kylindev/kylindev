function addComment(modelIdValue, position){
	if(null==modelIdValue || ''==modelIdValue){
		return;
	}
	var _commentModalId = '#commentModalId'+position;
	$(_commentModalId).val(modelIdValue);
	
	var _commentModalIdDepth = "#commentModalIdDepth"+position;
	$(_commentModalIdDepth).val(-1);//先初始化为-1
	var depthId = "#depth_comment_id_" + modelIdValue;
	if($(depthId).length>0){
		var depthValue = $(depthId).val();
		$(_commentModalIdDepth).val(depthValue);
	}
	
	//span_comment_id_6
	var backDataContainerId='#span_comment_id_'+ modelIdValue;
	var isAppendId = "#isAppendId" + position;
	var isAppendIdValue = $(isAppendId).val();
	if(isAppendIdValue==''){
		isAppendIdValue=false;
	}
	var clickFunction = "saveComment('#form_id_comment" + position + "','"+backDataContainerId+"','"+modelIdValue+"','" + position + "'," + isAppendIdValue + ")";
	$('#button_id_save_comment' + position).attr("onclick",clickFunction);
	$('#commentModal' + position).modal('toggle');
}
function saveComment(formId,backDataContainerId,id, position, isAppend){
	//alert(formId + ":"+backDataContainerId);
	submitWithCallBack(formId,backDataContainerId,function(){
		//这一段放在callback中
		var totalCommentSpanClass=".span-comment-total-"+id;
		var totalValueItemId = "#commentTotalNumber_"+id;
		var _totalValueItemId=$(totalValueItemId);
		var _totalValueItemFromContainer=$(backDataContainerId).find(totalValueItemId);
		//alert(totalValueItemId);
		if(_totalValueItemFromContainer.length>0){
			var total = _totalValueItemFromContainer.val();
			//_totalValueItemFromContainer.remove();
			var _totalCommentSpanClass=$(totalCommentSpanClass);
			if(_totalCommentSpanClass.length>0){
				_totalCommentSpanClass.each(function(){
					$(this).html(total);
				});
				
			}
			
		}
	},isAppend);
	
	$('#commentModal'+position).modal('hide');
}
