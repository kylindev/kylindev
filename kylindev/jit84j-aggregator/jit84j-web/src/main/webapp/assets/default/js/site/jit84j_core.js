//切换按钮，异步获取内容并填充
function switchSetting(id,navigationId,url,containerId,secondNavigationContainerId){
	var listItem ='';
	if(secondNavigationContainerId){
		var _secondNavigationContainerId=$('#'+secondNavigationContainerId);
		if(_secondNavigationContainerId.length>0){
			listItem=_secondNavigationContainerId.find("li");
		}
	}
	if(listItem ==''){
		listItem = $(".list-group-item");
	}
	
	var _id="left_navigation_"+navigationId;
	if(listItem.length>0){
		$(listItem).each(function(){
			var _span = $(this).find(".label");
			if(_id==$(this).attr("id")){
				$(this).addClass("active");
				if(_span.length>0){
					_span.removeClass("label-primary");
					_span.addClass("label-success");
				}
				
			}else{
				$(this).removeClass("active");
				if(_span.length>0){
					_span.removeClass("label-success");
					_span.addClass("label-primary");
				}
			}
		});
	}
	var request={'id':id};
	getContent(url,request,containerId);
}

//点一次就失效
function praise(id,url,containerId,aId){
	$(aId).removeAttr("onclick");
	submitUrl(id,url,containerId);
	
}

function praiseWithCallBack(id,url,containerId,aId,callback){
	$(aId).removeAttr("onclick");
	submitUrlWithCallBack(id,url,containerId,callback);
	
}

function submitUrlWithCallBack(id,url,containerId,callback){
	if(id==''){
		alert("非法提交");
		return;
	}
	var request={'id':id};
	getContentWithCallBack(url,request,containerId,callback);
}

function submitUrl(id,url,containerId){
	if(id==''){
		alert("非法提交");
		return;
	}
	var request={'id':id};
	getContent(url,request,containerId);
}

function getContent(url,request,containerId){
	getContentWithCallBack(url,request,containerId,null);
}

function checkContainerId(containerId){
	var _containerId=containerId;
	if(containerId !=''){
		if(containerId.indexOf("#")<0){
			_containerId = "#"+_containerId;
		}
	}
	return _containerId;
}

function getContentWithCallBack(url,request,containerId,callback,type){
	if(type==undefined || type==null || type==""){
		type="text";
	}
	var _containerId=checkContainerId(containerId);
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : type,
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			if($(_containerId).length>0){
				$(_containerId).html(data);
			}
			if (typeof callback === "function") {  
		        callback(data);  
		    }
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}

function getContentWithCallBackForJson(url,request,containerId,callback){
	getContentWithCallBack(url,request,containerId,callback,"json");
}

//append添加
function getContentAppend(url,request,containerId,callback){
	var _containerId=checkContainerId(containerId);
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			$(_containerId).append(data);
			if (typeof callback === "function") {  
		        callback(data);  
		    }
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
//异步提交表单，表单区域要提交的内容必须有data='data'属性，将返回结果页面填充入指定容器
function submitForm2(formId,backDataContainerId, callback){
	if (typeof callback === "function") {  
        //callback(data);  
    }else{
    	callback=resolveJsonData;
    }
	doSubmit(formId,backDataContainerId, "json",callback,false);
}

function submit(formId,backDataContainerId,isAppend){
	doSubmit(formId,backDataContainerId, "text",null,isAppend);
}
function submitWithCallBack(formId,backDataContainerId,callback,isAppend){
	doSubmit(formId,backDataContainerId, "text",callback,isAppend);
}

function doSubmit(formId,backDataContainerId, type, callback, appendValue){
	//alert("submit:" + formId);
		var request = new Object();
		var _fom = $(formId);
		//var _input = formId + " input[data$='data']";
		if(_fom.length>0){
			var _input= _fom.find("input[data$='data']");
			if(_input.length>0){
	  			_input.each(function(){
	  				
	  				if($(this).attr("type")=="checkbox" && $(this).attr("data")!='data-check-value'){
	  					if($(this).attr('checked')==undefined){
	  						request[$(this).attr("name")]='false';
	  					}else{
	  						request[$(this).attr("name")]='true';
	  					}
	  				}else{
	  					//request[$(this).attr("name")]=$(this).attr("value");
	  					request[$(this).attr("name")]=$(this).val();
	  				}
	  				
	  			});
			}
			
			var _input= _fom.find("input[data$='data-check-value']");
			if(_input.length>0){
	  			_input.each(function(){
	  				if($(this).attr("type")=="checkbox" && $(this).attr("data")!='data'){
	  					if($(this).attr('checked')!=undefined){
	  						request[$(this).attr("name")]=$(this).attr("value");
	  					}
	  				}
	  			});
			}
			
			
			var _select = formId+" select[data$='data']";
			if($(_select).length>0){
				$(_select).each(function(){
					request[$(this).attr("name")]=$(this).val();
				});
			}
			
			var _textarea = formId+" textarea[data$='data']";
			if($(_textarea).length>0){
				$(_textarea).each(function(){
					request[$(this).attr("name")]=$(this).val();
				});
			}
			var action = $(formId).attr("action");
			$.ajax({
				url: action,
				data: request,
				type: "POST",
				dataType : type,
				contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
				success: function(data) {
					if(type=='text'){
						if($(backDataContainerId).length>0){
								if(appendValue==1){
									$(backDataContainerId).append(data);
								}else if(appendValue==-1){
									$(backDataContainerId).prepend(data);
								}else{
									$(backDataContainerId).html(data);
								}
								if (typeof callback === "function") {  
							        callback(data);  
							    } 
						}
					}else if(type=='json'){
						if (typeof callback === "function") {  
					        callback(data);  
					    } 
					}
						
				},
				error: function() {
					alert( "Sorry, there was a problem!" );
				},
				complete: function() {
					//alert('complete');
				}
			});
		}
}

function resolveJsonData(data){
	
	//alert(data.result.result);
}

function cleanForm(formId) {
	//alert("clean:" + formId);
	var _input = formId + " input[data$='data']";
	if ($(_input).length > 0) {
		$(_input).each(function() {
			if ($(this).attr("type") == "checkbox") {
				$(this).attr("checked", false);
			} else {
				//针对批次字段特殊处理
				if($(this).attr("name") == "batch"){
					var _time = new Date();
					$(this).val(_time.getTime());
				}else if($(this).attr("name") == "sequence"){
					$(this).val(0);
				}else{
					$(this).val("");
				}
			}
		});
	}
	
	var _textarea = formId+" textarea[data$='data']";
	if($(_textarea).length>0){
		$(_textarea).each(function(){
			$(this).val("");
		});
	}
	
	var _msg = formId + " div[id$='msg']";
	if ($(_msg).length > 0) {
		$(_msg).each(function() {
			$(this).hide();
		});
	}
	
}

function removeItem(id,action,backDataContainerId){
	if(id==''){
		alert("没有id");
		return;
	}
	var request = {'id':id};
	$.ajax({
		url: action,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded; charset=UTF-8', 
		success: function(data) {
			//alert("移除成功");
			if($(backDataContainerId).length>0){
					$(backDataContainerId).html(data);
			}
				
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
			//alert('complete');
		}
	});
}
//全选操作
function selectAll(id,prefix){
	var _id = "#" + id;
	var _all = $(_id);
	var _input = "input[id ^='"+prefix+"']";
	if(_all.attr("checked")){ 
		$(_input).each(function(){this.checked=true;}); 
	}else{
		$(_input).each(function(){this.checked=false;}); 
	}
}

function joinCheckboxValue(prefix){
	
	var _input = "input[id ^='"+prefix+"']:checked";
	var _all = prefix+ "all";
	var requestData="";
	if($(_input).length>0){
		$(_input).each(function(){
		if(($(this).attr("id")).indexOf(_all)<0){
			requestData += $(this).val()+",";
		}
		
	}); 
	
	if(requestData.lastIndexOf(",")>0){
			requestData = requestData.trim();
			requestData = requestData.substring(0,requestData.lastIndexOf(","));
		}
	}
	if(""==requestData){
		return "";
	}
	return requestData;
}

function submitOpration(url,request,backDataContainerId){
	$.ajax({
		url: url,
		data: request,
		type: "POST",
		dataType : "text",
		contentType:'application/x-www-form-urlencoded;charset=UTF-8', 
		success: function(data) {
			if($(backDataContainerId).length>0){
				$(backDataContainerId).html(data);
		}
		},
		error: function() {
			alert( "Sorry, there was a problem!" );
		},
		complete: function() {
		}
	}); 
}

function submitForm(formId,checkBoxPrefix,checkBoxName){
  	var request = new Object();
	if($("input[data$='data']").length>0){
		$("input[data$='data']").each(function(){
			request[$(this).attr("name")]=$(this).attr("value");
		});
	}
	if(checkBoxPrefix !='' && checkBoxPrefix != undefined && checkBoxName != '' && checkBoxName != undefined){
		var checkboxValues = joinCheckboxValue(checkBoxPrefix);
		request[checkBoxName]=checkboxValues;
	}
	var _formId="#"+formId;
	var action = $(_formId).attr("action");
	$.ajax({
			url: action,
			data: request,
			type: "POST",
			dataType : "json",
			success: function(data) {
				alert(data.msg);
			},
			error: function() {
				alert( "Sorry, there was a problem!" );
			},
			complete: function() {
				//alert('complete');
			}
	});
}

function scroll_news(scrollItem){
	$(function(){
		if(scrollItem==''){
			scrollItem="#scroll_div li";
		}
		$(scrollItem).eq(0).fadeOut('slow',function(){
			//   alert($(this).clone().html());
			//克隆:不用克隆的话，remove()就没了。
			$(this).clone().appendTo($(this).parent()).fadeIn('slow');
			$(this).remove();
		});
	});
}

function nextPage(url,currentNumber,pageSize, containerId,nextPageId,totalPage){
	var request=new Object();
		request.page=currentNumber;
		request.size=pageSize;
	
	if(''!=containerId){
		var _containerId = checkContainerId(containerId);
		var __container = $(_containerId);
		if(__container.length>0){
			var _input= __container.find("input[data$='data']");
			if(_input.length>0){
	  			$(_input).each(function(){
	  				request[$(this).attr("name")]=$(this).attr("value");
	  			});
	  		}
		}
		var _nextPageId = checkContainerId(nextPageId);
		var _nextPageIdElement = $(_nextPageId);
		if(_nextPageIdElement.length>0){
			$(_nextPageId).remove();
		}
		
		getContentAppend(url,request,containerId,function(data){
			if(''!=nextPageId){
				var _nextPageId = checkContainerId(nextPageId);
				var _nextPageNum = parseInt(currentNumber)+1;
				var _totalPage =  parseInt(totalPage);
				if(_nextPageNum >=totalPage){
					//$(_nextPageId).addClass("hidden");
					$(_nextPageId).onclick=null;
					$(_nextPageId).find("a").onclick=null;
					$(_nextPageId).html('<div class="alert alert-success" role="alert">已加载所有数据</div>');
				}else{
					$(_nextPageId).find("a").attr("onclick","nextPage('"+url+"',"+ _nextPageNum +","+ pageSize +",'"+ containerId +"','"+ nextPageId +"',"+totalPage+")");
				}
			}
			
		});
	}
}