/*
 * jQuery File Upload Plugin Angular JS Example 1.2.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global window, angular */

var _filesBack = new Array();
function addFileBack(files){
	for(var j=0;j<files.length;j++){
    	var file = files[j];
    	if(!_filesBack.contains(file)){
    		_filesBack.push(file);
    	}
    }
}

function copyFileFromBack(element){
	var fileId = $(element).find(".hide").html();
	var _fileExist = _filesBack.getObjById(fileId);
	var _files = new Array();
	_files.push(_fileExist);
	fillImageContent(_files);
	
}

Array.prototype.contains = function(obj) { 
	var i = this.length; 
	while (i--) { 
		if (this[i] === obj) { 
		return true; 
		} 
	} 
	return false; 
};

Array.prototype.getObjById = function(id) { 
	var i = this.length; 
	while (i--) { 
		if (this[i].id == id) { 
			return this[i]; 
		} 
	} 
	return null; 
};


//图片显示区
function fillImageContent(files){
	addFileBack(files);
	if($("#image_content_contariner").length>0){
    	var imageContainer = $("#image_content_contariner");
    	var rowItems = imageContainer.find(".row");
    	
    	var rowCount = 0;
    	if(rowItems){
    		rowCount = rowItems.length;
    		rowItems.each(function(index,element){
    			$(this).find("input[name$='sequence']").attr("name","imageList[" + index +"].sequence");
    		});
    	}
    	//alert(rowCount);
    	for(var i=0;i<files.length;i++){
    		var file = files[i];
    		var _rowId = "#row_item_id_" + file.id;
    		if($(_rowId).length>0){
    			continue;
    		}
    		var fileIndex = rowCount + i;
    		imageContainer.append(
    				"<div class='row' id='row_item_id_"+ file.id +"'>" 
        				+ "<div class='col-md-1'>" +
        						"<input data='data' name='imageList[" + fileIndex + "].sequence' value='"+ fileIndex +"' placeholder='序号' class='form-control'/>" +
        						"<a class='text-danger' href='javascript:void(0)' onclick=\"removeImage('row_item_id_"+ file.id + "')\" id='remove_item_a_id_" + file.id + "'>移除</a>" +
        						"<input data='data' type='hidden' name='imageList[" + fileIndex + "].fileInfoId' value='"+ file.id +"'/>" +
        						"<input data='data' type='hidden' name='imageList[" + fileIndex + "].id' value='' id='id_" + file.id + "'/>" +
        				  "</div>"
        				+ "<div class='col-md-6'>" +
        						"<input data='data' name='imageList[" + fileIndex + "].title' value='" + file.title + "' placeholder='输入标题' class='form-control'/><br/>" +
        						"<textarea data='data' class='form-control' rows='5' name='imageList[" + fileIndex + "].description' placeholder='输入图片背景故事'>"+file.description+"</textarea>" +
        				  "</div>" 
        				+ "<div class='col-md-5'>" +
        						"<img src='" + file.thumbnailUrl + "' width='360px'><p>" + file.name + "</p>" +
        						"<input data='data' type='hidden' name='imageList[" + fileIndex + "].imageUrl' value='"+ file.url +"'/>" +
        						"<input data='data' type='hidden' name='imageList[" + fileIndex + "].thumbnailUrl' value='"+ file.thumbnailUrl +"'/>" +
        				   "</div>" 
    				+ "</div>"
    				);
    		
    	}
    }
}

function removeImage(imageId){
	var _imageId= "#"+imageId;
	var _imageRow = $("#"+imageId);
	var imageContainer = $("#image_content_contariner");
	if(_imageRow.length>0 && imageContainer.length>0){
		_imageRow.remove();
    	var rowItems = imageContainer.find(".row");
    	var rowCount = 0;
    	if(rowItems){
    		rowCount = rowItems.length;
    		rowItems.each(function(index,element){
    			$(this).find("input[name$='sequence']").attr("name","imageList[" + index +"].sequence");
    		});
    	}
	}
}

function fillImageContentWithId(imageList,removeUrl,backDataContainerId){
	if(imageList!=null){
		$.each(imageList,function(index,element) {
			var fileInfoId=element.fileInfoId;
			var _id = "#id_" + fileInfoId;
			$(_id).val(element.id);
			var _removeItemAId = "#remove_item_a_id_" + fileInfoId;
			$(_removeItemAId).attr("onclick", "removeItem('" + element.id + "','" + removeUrl + "','"+ backDataContainerId + "')");
		});
	}
}


function copyHideImageToContentContainer(){
	var image_content_div = $("#image_content_div_id").html();
	var _rowItems = $(image_content_div).filter(".row");
	if(_rowItems.length>0){
		_rowItems.each(function(index,element){
			var _ori_id = $(this).attr("id");
			var _back_index = _ori_id.lastIndexOf('_back');
			if(_back_index>0){
				$(this).attr("id",_ori_id.substring(0,_back_index));
			}
		});
	}
	$("#image_content_contariner").html(_rowItems);
	_rowItems=null;
	image_content_div=null;
}

//选择已有图片调用，加入前一页面
function addEixtFileToBack(fileInfoId,imageUrl, thumbnailUrl, title,description){
	var _fileExist = new Object();
	_fileExist.id = fileInfoId;
	_fileExist.title = title;
	_fileExist.name = title;
	_fileExist.description = description;
	_fileExist.url = imageUrl;
	_fileExist.thumbnailUrl = thumbnailUrl;
	var _files = new Array();
	_files.push(_fileExist);
	fillImageContent(_files);
	
}
//选择已有图片，删除时调用
function removeExitFileFromBack(fileInfoId){
	
	//先检查是否是从DB查询出来的
	var _id = "#id_" + fileInfoId;
	var _idValue = $(_id).val();
	//如果是，直接返回，不能通过此方法移除元素，必须从前一页面点移除链接
	if(_idValue != ''){
		return;
	}else{
		removeImage("row_item_id_"+ fileInfoId);
	}
}

(function () {
    'use strict';

    var isOnGitHub = window.location.hostname === 'blueimp.github.io',
        url = isOnGitHub ? '//jquery-file-upload.appspot.com/' : '/admin/blank/file/DO/basicAddo_fileInfo_upload_ou_100001_u';

    angular.module('demo', [
        'blueimp.fileupload'
    ])
        .config([
            '$httpProvider', 'fileUploadProvider',
            function ($httpProvider, fileUploadProvider) {
                delete $httpProvider.defaults.headers.common['X-Requested-With'];
                fileUploadProvider.defaults.redirect = window.location.href.replace(
                    /\/[^\/]*$/,
                    '/cors/result.html?%s'
                );
               // if (isOnGitHub) {
                    // Demo settings:
                    angular.extend(fileUploadProvider.defaults, {
                        // Enable image resizing, except for Android and Opera,
                        // which actually support image resizing, but fail to
                        // send Blob objects via XHR requests:
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        maxFileSize: 5000000,
                        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                        handleResponse: function (e, data) {
                        	//var success= data.result.success;
                            //alert(success);
                            
                           // var files = data.result && data.result.files;
                            var files = data.result && data.result.result;
                            if (files) {
                            	//填充image，将图片显示在页面上
                            	fillImageContent.call(this,files);
                            	//继续执行本层替换
                                data.scope.replace(data.files, files);
                            } else if (data.errorThrown ||
                                    data.textStatus === 'error') {
                                data.files[0].error = data.errorThrown ||
                                    data.textStatus;
                            }
                        }
                    });
               // }
            }
        ])

        .controller('DemoFileUploadController', [
            '$scope', '$http', '$filter', '$window',
            function ($scope, $http) {
                $scope.options = {
                    url: url
                };
               // if (!isOnGitHub) {
                    $scope.loadingFiles = true;
                    $http.get(url)
                        .then(
                            function (response) {
                                $scope.loadingFiles = false;
                                $scope.queue = response.data.files || [];
                            },
                            function () {
                                $scope.loadingFiles = false;
                            }
                        );
                //}
            }
        ])

        .controller('FileDestroyController', [
            '$scope', '$http',
            function ($scope, $http) {
                var file = $scope.file,
                    state;
                if (file.url) {
                    file.$state = function () {
                        return state;
                    };
                    file.$destroy = function () {
                        state = 'pending';
                        return $http({
                            url: file.deleteUrl,
                            method: file.deleteType
                        }).then(
                            function () {
                                state = 'resolved';
                                $scope.clear(file);
                            },
                            function () {
                                state = 'rejected';
                            }
                        );
                    };
                } else if (!file.$cancel && !file._index) {
                    file.$cancel = function () {
                        $scope.clear(file);
                    };
                }
            }
        ]);

}());
