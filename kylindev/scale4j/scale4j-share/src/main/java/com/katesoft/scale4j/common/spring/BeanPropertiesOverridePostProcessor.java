package com.katesoft.scale4j.common.spring;

import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * This class allows to override any bean properties you like for particular bean definition.
 * <p/>
 * You would need to specify target bean.
 * 
 * @author kate2007
 */
public class BeanPropertiesOverridePostProcessor extends
         AbstractBeanPropertiesOverridePostProcessor {
   @Override
   protected void doPostProcessing(
            @SuppressWarnings("unused") ConfigurableListableBeanFactory configurableListableBeanFactory) {
      overrideBeanProperties();
   }

   @Required
   @Override
   public void setTargetBean(String targetBean) {
      super.setTargetBean(targetBean);
   }
}
