package com.katesoft.scale4j.common.spring;

import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import static com.katesoft.scale4j.common.spring.IPostProcessingOrder.PROPERTY_PLACEHOLDER_RUNTIME_MODE_PRIORITY;

/**
 * It is highly recommended to use this class instead of {@link PropertyPlaceholderConfigurer}.
 * 
 * @author kate2007
 */
public class RuntimePropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {
   /**
    * default constructor which sets {@link this#setIgnoreUnresolvablePlaceholders(boolean)} to
    * true.
    * <p/>
    * system properties mode {@link this#setSystemPropertiesMode(int)} defaulted to
    * SYSTEM_PROPERTIES_MODE_OVERRIDE.
    */
   public RuntimePropertyPlaceholderConfigurer() {
      setIgnoreUnresolvablePlaceholders(true);
      setOrder(PROPERTY_PLACEHOLDER_RUNTIME_MODE_PRIORITY);
      setSystemPropertiesMode(SYSTEM_PROPERTIES_MODE_OVERRIDE);
      setSearchSystemEnvironment(true);
   }
}
