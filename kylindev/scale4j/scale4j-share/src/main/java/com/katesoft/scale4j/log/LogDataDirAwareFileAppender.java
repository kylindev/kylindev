package com.katesoft.scale4j.log;

import com.katesoft.scale4j.common.lang.RuntimeUtility;
import org.apache.log4j.FileAppender;

/**
 * This class is very simple extension of classical file appender which allows you to store files in
 * LOG_DATA_DIR(you would not need to specify full path to log4 data dir in this case).
 * 
 * @author kate2007
 * @see RuntimeUtility#LOG_DATA_DIR
 */
public class LogDataDirAwareFileAppender extends FileAppender {
   @Override
   public void activateOptions() {
      String path = RuntimeUtility.LOG_DATA_DIR + fileName;
      setFile(path);
      super.activateOptions();
   }
}
