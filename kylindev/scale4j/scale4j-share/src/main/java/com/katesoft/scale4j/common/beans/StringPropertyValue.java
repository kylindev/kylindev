package com.katesoft.scale4j.common.beans;

import com.katesoft.scale4j.common.annotation.ValueObject;

/**
 * @author kate2007
 */
@ValueObject
public class StringPropertyValue extends PropertyValue<String> {
   public StringPropertyValue(final String pProperty, final String pValue) {
      super(pProperty, pValue);
   }
}
