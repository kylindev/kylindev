package com.katesoft.scale4j.common.spring;

/**
 * This class contains default orders for application post processors.
 * 
 * @author kate2007
 * @see AbstractBeanPropertiesOverridePostProcessor
 */
public interface IPostProcessingOrder {
   int PROPERTY_PLACEHOLDER_RUNTIME_MODE_PRIORITY = 200;
   int PROPERTY_PLACEHOLDER_WEB_MODULE_PRIORITY = 300;
   int PROPERTY_PLACEHOLDER_RTTP_MODULE_PRIORITY = 350;
   int PROPERTY_PLACEHOLDER_PERSISTENT_MODULE_PRIORITY = 400;
   int DATASOURCE_POST_PROCESSOR_PRIORITY = 500;
   int TRANSACTION_POST_PROCESSOR_PRIORITY = 500;
   int HIBERNATE_SESSION_POST_PROCESSOR = 600;
   int RTTP_HIBERNATE_POST_PROCESSOR = 700;
}
