package com.katesoft.scale4j.common.model;

import java.util.Collection;
import java.util.Iterator;

import net.jcip.annotations.ThreadSafe;

import com.katesoft.scale4j.common.annotation.NullSafe;
import com.katesoft.scale4j.common.io.FileUtility;

/**
 * This interface defines methods for java objects.
 * 
 * @author kate2007
 */
public interface IPrettyPrintableObject {
   /**
    * @return representation in short internal format.
    */
   @Override
   String toString();

   /**
    * @return key value representation of each persistent field based on reflection.
    */
   String reflectionToString();

   @ThreadSafe
   final class Printer {
      @NullSafe
      public static String reflectionToString(
               Collection<? extends IPrettyPrintableObject> collection) {
         StringBuilder builder = new StringBuilder();
         if (collection != null) {
            for (Iterator<? extends IPrettyPrintableObject> iterator = collection.iterator(); iterator
                     .hasNext();) {
               builder.append(iterator.next().reflectionToString());
               if (iterator.hasNext()) {
                  builder.append(FileUtility.LINE_SEPARATOR);
               }
            }
         }
         return String.format("[%s]", builder.toString());
      }

      private Printer() {
      }
   }
}
