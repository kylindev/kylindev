package com.katesoft.scale4j.common.model;

/**
 * Marker interface - implementing means that object has unique identifier of long type.
 * 
 * @author kate2007
 */
public interface IHasUniqueIdentifier<T> {
   /**
    * unique identifier of entity.
    * 
    * @return identifier of revision.
    */
   T getUniqueIdentifier();
}
