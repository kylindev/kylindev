package com.katesoft.scale4j.common.beans;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.katesoft.scale4j.common.annotation.ValueObject;
import com.katesoft.scale4j.common.model.IPrettyPrintableObject;

/**
 * this is just wrapper for property-value pair. can be re-used effectively.
 * 
 * @author kaet2007
 */
@ValueObject
public class PropertyValue<V> implements IPrettyPrintableObject {
   private final String property;
   private final V value;

   public PropertyValue(final String p, final V v) {
      super();
      this.property = p;
      this.value = v;
   }

   public String getProperty() {
      return property;
   }

   public V getValue() {
      return value;
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.toString(this);
   }
}
