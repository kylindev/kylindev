package com.katesoft.scale4j.common.utils;

import java.util.Collection;
import java.util.List;

/**
 * @author Santhosh Kumar T
 * @author kate2007
 */
public final class CollectionUtility {
   /**
    * Adds objects in array to the given collection
    * 
    * @param c
    *           collection to which add each element of array.
    * @param array
    *           ~ items
    * @return the same collection which is passed as argument
    */
   @SuppressWarnings({})
   public static <E, T extends E> Collection<E> addAll(Collection<E> c, T... array) {
      for (T obj : array) {
         c.add(obj);
      }
      return c;
   }

   /**
    * Removes objects in array to the given collection
    * 
    * @param c
    *           collection from which remove each element of array.
    * @param array
    *           ~ items
    * @return the same collection which is passed as argument(but without element from array)
    */
   public static <E, T extends E> Collection<E> removeAll(Collection<E> c, T... array) {
      for (T obj : array) {
         c.remove(obj);
      }
      return c;
   }

   /**
    * Adds the given item to the list at specified <code>index</code>.
    * <p/>
    * if <code>index</code> is greater than list size, it simply appends to the list.
    * 
    * @param list
    *           collection of elements
    * @param index
    *           specific index
    * @param item
    *           object to add.
    */
   public static <E, T extends E> void add(List<E> list, int index, T item) {
      if (index < list.size()) {
         list.add(index, item);
      } else {
         list.add(item);
      }
   }

   //
   public static boolean[] toBooleanArray(Collection<Boolean> c) {
      boolean arr[] = new boolean[c.size()];
      int i = 0;
      for (Boolean item : c) {
         arr[i++] = item;
      }
      return arr;
   }

   public static int[] toIntArray(Collection<? extends Number> c) {
      int arr[] = new int[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.intValue();
      }
      return arr;
   }

   public static long[] toLongArray(Collection<? extends Number> c) {
      long arr[] = new long[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.longValue();
      }
      return arr;
   }

   public static float[] toFloatArray(Collection<? extends Number> c) {
      float arr[] = new float[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.floatValue();
      }
      return arr;
   }

   public static double[] toDoubleArray(Collection<? extends Number> c) {
      double arr[] = new double[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.doubleValue();
      }
      return arr;
   }

   public static byte[] toByteArray(Collection<? extends Number> c) {
      byte arr[] = new byte[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.byteValue();
      }
      return arr;
   }

   public static short[] toShortArray(Collection<? extends Number> c) {
      short arr[] = new short[c.size()];
      int i = 0;
      for (Number item : c) {
         arr[i++] = item.shortValue();
      }
      return arr;
   }

   private CollectionUtility() {
   }
}