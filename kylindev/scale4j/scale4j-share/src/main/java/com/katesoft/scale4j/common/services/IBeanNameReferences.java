package com.katesoft.scale4j.common.services;

/**
 * This class contains ids for corresponding internal spring bean definitions.
 * <p/>
 * Clients can use this class to fetch beans from spring ApplicationContext or use with @Qualifier
 * annotation for auto-wiring.
 * 
 * @author kate2007
 */
public interface IBeanNameReferences {
   String TRANSACTION_MANAGER = "jvmcluster.transactionManager";
   String DATASOURCE_TRANSACTION_MANAGER = "jvmcluster.jdbcTransactionManager";
   String ACTUAL_DATASOURCE = "jvmcluster.actual.datasource";
   String JDBC_TEMPLATE = "jvmcluster.jdbcTemplate";
   String HIBERNATE_TEMPLATE = "jvmcluster.hibernateTemplate";
   String HIBERNATE_DAO_SUPPORT = "jvmcluster.hibernate.dao.support";
   String SESSION_FACTORY = "jvmcluster.sessionFactory";
   String TRANSACTION_SERVICE = "jvmcluster.user.transaction.service";
   String TRANSACTION_TEMPLATE = "jvmcluster.transaction.template";
   String RTTP_CLIENT_JOBS_EXECUTOR = "rttp.client.jobs.executor";
   String RTTP_CLIENT_SCHEDULER = "rttp.client.scheduler";
   String RTTP_CLIENT_LOCAL_EXECUTOR = "rttp.scheduler.executor";
   String RTTP_BATCH_JOB_REGISTRY = "rttp.job.registry";
   String RTTP_BATCH_JOB_REPOSITORY = "rttp.job.repository";
   String RTTP_BATCH_JOB_LAUNCHER = "rttp.job.launcher";
   String RTTP_BATCH_JOB_OPERATOR = "rttp.job.operator";
   String RTTP_BATCH_JOB_EXPLORER = "rttp.job.explorer";
   String RTTP_SPRING_HAZELCAST_BRIDGE = "rttp.bridge";
}
