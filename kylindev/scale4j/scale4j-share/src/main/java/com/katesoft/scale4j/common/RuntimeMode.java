package com.katesoft.scale4j.common;

/**
 * this interface defines common runtime modes.
 * 
 * @author kate2007
 */
public enum RuntimeMode {
   DEV,
   TEST,
   UAT,
   PROD,
   JUNIT
}
