package com.katesoft.scale4j.common.spring;

import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.beans.StringPropertyValue;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import org.hamcrest.Matchers;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/spring/placeholder-junit.xml" })
@DirtiesContext
public class RuntimePropertyPlaceholderConfigurerTest {
   @Autowired
   @Qualifier(value = "bean1")
   private StringPropertyValue bean1;
   @Autowired
   @Qualifier(value = "bean2")
   private StringPropertyValue bean2;
   @Autowired
   @Qualifier(value = "bean3")
   private StringPropertyValue bean3;

   @BeforeClass
   public static void beforeClass() {
      System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.name().toLowerCase());
      System.setProperty(RuntimeUtility.VAR_SERVICE_ID, "service154");
   }

   @AfterClass
   public static void afterClass() {
      System.clearProperty(RuntimeUtility.VAR_RUNTIME_MODE);
      System.clearProperty(RuntimeUtility.VAR_SERVICE_ID);
   }

   @Test
   public void bothBeansResolved() {
      assertThat(bean1, is(Matchers.<Object> notNullValue()));
      assertThat(bean2, is(Matchers.<Object> notNullValue()));
      assertThat(bean3, is(Matchers.<Object> notNullValue()));
   }

   @Test
   public void placeHoldersResolved() {
      assertThat(bean1.getValue(), is("value1"));
      assertThat(bean2.getValue(), is("value2"));
   }

   @Test
   public void systemPlaceHoldersResolved() {
      assertThat(bean3.getValue(), is("service154"));
   }
}
