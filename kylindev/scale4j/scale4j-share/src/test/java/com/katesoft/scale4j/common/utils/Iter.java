package com.katesoft.scale4j.common.utils;

import org.junit.Ignore;

import java.util.Iterator;

/**
 * @author kate2007
 */
@Ignore
public class Iter<E> implements Iterator<E> {
   @Override
   public boolean hasNext() {
      return false;
   }

   @Override
   public E next() {
      return null;
   }

   @Override
   public void remove() {
   }
}
