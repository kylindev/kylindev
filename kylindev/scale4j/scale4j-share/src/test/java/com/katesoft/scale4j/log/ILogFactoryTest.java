package com.katesoft.scale4j.log;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

public class ILogFactoryTest {
   private Logger logger;

   @Before
   public void setUp() {
      logger = LogFactory.getLogger(ILogFactoryTest.class);
   }

   @Test
   public void testGetLoggerClassOfQ() {
      assertThat(logger, is(notNullValue()));
   }

   @Test
   public void logWith2ArgsWorks() {
      logger.debug("work with 2 args [%s, %s]", "1_debug", "2_debug");
      logger.info("work with 2 args [%s, %s]", "1_info", "2_info");
      logger.trace("work with 2 args [%s, %s]", "1_trace", "2_trace");
      logger.error("work with 2 args [%s, %s]", "1_error", "2_error");
      logger.warn("work with 2 args [%s, %s]", "1_warn", "2_warn");
   }

   @Test
   public void stringFormattingFailsWithWrongString() {
      logger.debug("I am wrong string %s %s %s", "s1", "s2");
   }

   @Test
   public void warningWithExceptionResolved() {
      try {
         this.wait();
      } catch (Exception e) {
         logger.warn("unable to wait on object %s because of %s", this, e);
      }
   }
}
