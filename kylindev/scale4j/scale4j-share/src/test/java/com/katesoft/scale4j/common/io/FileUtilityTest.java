package com.katesoft.scale4j.common.io;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static com.katesoft.scale4j.common.io.FileUtility.*;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class FileUtilityTest {
   private File resource;

   @Before
   public void setUp() throws IOException {
      resource = fromClasspathSource("META-INF/spring/jvmcluster$jmx-common.xml");
   }

   @Test
   public void fileIsReadable() throws Exception {
      ensureFileIsReadable(resource);
   }

   @Test
   public void staticVariablesInitialized() {
      assertThat(USER_HOME, is(Matchers.<Object> notNullValue()));
      assertThat(SEPARATOR, is(Matchers.<Object> notNullValue()));
      assertThat(JAVA_HOME, is(Matchers.<Object> notNullValue()));
      assertThat(LINE_SEPARATOR, is(Matchers.<Object> notNullValue()));
      assertThat(PATH_SEPARATOR, is(Matchers.<Object> notNullValue()));
      assertThat(TMP_DIR, is(Matchers.<Object> notNullValue()));
      assertThat(USER_DIR, is(Matchers.<Object> notNullValue()));
      assertThat(USER_HOME, is(Matchers.<Object> notNullValue()));
   }

   @Test
   public void pathConstructedCorrectly() {
      assertThat(path("db1", "db2"), is(format("db1%sdb2%s", SEPARATOR, SEPARATOR)));
      assertThat(path(format("db1%s", SEPARATOR), "db2"),
               is(format("db1%sdb2%s", SEPARATOR, SEPARATOR)));
      assertThat(path(format("db1%s", SEPARATOR), format("db2", SEPARATOR)),
               is(format("db1%sdb2%s", SEPARATOR, SEPARATOR)));
   }
}
