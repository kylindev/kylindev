package com.katesoft.scale4j;

import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.log.LogFactory;
import org.junit.Ignore;
import org.springframework.beans.factory.InitializingBean;

import java.util.Arrays;

@Ignore
public class SimpleApplication implements InitializingBean
{
    public static final String PROGRAM_OUTPUT = "$$OUTPUT$$";
    public static final String ADDITIONAL_SYSTEM_PARAM = "-junit.additional";

    public static void main(String[] args) throws InterruptedException
    {
        Logger logger = LogFactory.getLogger(SimpleApplication.class);
        logger.debug(PROGRAM_OUTPUT + (args != null ? Arrays.toString(args) : ""));
        logger.debug(System.getProperties().toString());
        RuntimeUtility.gcOnExit();
        //
        for (int i = 0; i <= 100; i++) {
            logger.debug("%s = %s", i, new Object().toString());
        }
        Thread.sleep(50);
    }

    @Override
    public void afterPropertiesSet() throws Exception
    {
        main(new String[]{});
    }
}
