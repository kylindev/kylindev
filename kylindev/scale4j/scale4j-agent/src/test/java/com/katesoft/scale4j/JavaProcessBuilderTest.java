package com.katesoft.scale4j;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.is;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import org.apache.commons.lang.SystemUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.katesoft.scale4j.agent.JavaProcessBuilder;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.io.ByteArrayOutputStream2;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

public class JavaProcessBuilderTest {
   private final Logger l = LogFactory.getLogger(getClass());

   @BeforeClass
   public static void beforeClass() {
      System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
   }

   @Test
   public void commandWithDefaultParametersWorks() throws Exception {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      String[] command = builder.command();
      l.debug(Arrays.toString(command));
      assertThat(command[0], containsString(SystemUtils.getJavaHome().getAbsolutePath()));
      assertThat(new File(command[0]).exists(), is(true));
      assertThat(builder.javaHome().exists(), is(true));
   }

   @Test
   public void javaProcessStartedWithDebugOptions() throws Exception {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      builder.debugPort(678);
      String[] command = builder.command();
      l.debug(Arrays.toString(command));
      assertThat(command, hasItemInArray("-Xdebug"));
      assertThat(command, hasItemInArray("-Xnoagent"));
   }

   @Test
   public void memoryOptionsWorks() throws IOException {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      builder.maxHeap(256);
      builder.initialHeap(64);
      String[] command = builder.command();
      l.debug(Arrays.toString(command));
      assertThat(command, hasItemInArray("-Xms64m"));
      assertThat(command, hasItemInArray("-Xmx256m"));
   }

   @Test
   public void clientServerWorks() throws IOException {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      builder.initialHeap(64);
      builder.server();
      String[] command = builder.command();
      l.debug(Arrays.toString(command));
      assertThat(command, hasItemInArray("-Xms64m"));
      assertThat(command, hasItemInArray("-server"));
      builder.client();
      command = builder.command();
      assertThat(command, hasItemInArray("-client"));
   }

   @Test
   public void simpleApplicationExecuted() throws IOException, InterruptedException {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      builder.initialHeap(64);
      builder.server();
      builder.classpath(System.getProperty("java.class.path"));
      builder.mainClass(SimpleApplication.class.getName());
      String[] command = builder.command();
      l.debug(Arrays.toString(command));
      Process process = builder.launch(System.out, System.err);
      process.waitFor();
   }

   @Test
   public void complexApplicationExecuted() throws IOException, InterruptedException {
      JavaProcessBuilder builder = new JavaProcessBuilder();
      builder.initialHeap(64).maxHeap(256)
               .systemProperty(SimpleApplication.ADDITIONAL_SYSTEM_PARAM, Boolean.TRUE.toString())
               .server().jvmArg("-XX:-UseParallelGC").jvmArg("-XX:-PrintGCDetails")
               .jvmArg("-XX:-PrintGCTimeStamps").jvmArg("-XX:-PrintGC")
               .jvmArg("-Dfile.encoding=UTF-8").debugPort(2476)
               .classpath(System.getProperty("java.class.path"))
               .mainClass(SimpleApplication.class.getName()).arg("junit");
      ByteArrayOutputStream2 outputStream2 = new ByteArrayOutputStream2();
      Process process = builder.launch(outputStream2, System.err);
      process.waitFor();
      assertThat(
               new String(outputStream2.toByteSequence().buffer()),
               containsString(SimpleApplication.PROGRAM_OUTPUT
                        + Arrays.toString(new String[] { "junit" })));
      assertThat(new String(outputStream2.toByteSequence().buffer()),
               containsString(SimpleApplication.ADDITIONAL_SYSTEM_PARAM + "=" + "true"));
      l.debug("*************************************************");
      l.debug(new String(outputStream2.toByteSequence().buffer()));
      l.debug("*************************************************");
   }
}
