package com.katesoft.scale4j.agent.jmx;

import com.katesoft.scale4j.agent.ApplicationConfigurationReader;
import com.katesoft.scale4j.agent.ServiceLauncher;
import com.katesoft.scale4j.agent.args.AgentOptions;
import com.katesoft.scale4j.agent.service.ServiceStatus;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.xml.jvmcluster.ApplicationConfigurationDocument.ApplicationConfiguration;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItemInArray;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;

/** @author kate2007 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/jvmcluster$oxm.xml"})
public class ServiceManagerJMXExporterTest
{
    @BeforeClass
    public static void beforeClass()
    {
        System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
    }

    @Autowired
    private ApplicationConfigurationReader applicationConfigurationReader;
    private ServiceManagerJMXExporter jmxExporter;
    private ServiceLauncher serviceLauncher;

    @Before
    public void setUp() throws IOException
    {
        jmxExporter = new ServiceManagerJMXExporter();
        jmxExporter.setContext(mock(ClassPathXmlApplicationContext.class));
        AgentOptions options = new AgentOptions();
        ApplicationConfiguration applicationConfiguration =
            applicationConfigurationReader.parseConfiguration(new ClassPathResource("xml/testSpringAppConfiguration.xml").getInputStream());
        serviceLauncher = new ServiceLauncher(applicationConfiguration, options);
        jmxExporter.setLauncher(serviceLauncher);
    }

    @After
    public void tearDown()
    {
        serviceLauncher.stopAllServices(true);
    }

    @Test(expected = IllegalArgumentException.class)
    public void exceptionIfServiceDoesNotExist()
    {
        jmxExporter.isServiceRunning("asdar324");
    }

    @Test
    public void startDateIsNotNullBeforeActualStart()
    {
        assertThat(jmxExporter.startedAt("service1"), is(Matchers.<Object>notNullValue()));
    }

    @Test
    public void serviceConfigurationIsNotEmptyBeforeActualStart()
    {
        assertThat(jmxExporter.serviceConfiguration("service1"), is(Matchers.<Object>notNullValue()));
    }

    @Test
    public void serviceIsStartingBeforeActualStart()
    {
        assertThat(ServiceStatus.STARTING.toString(), is(jmxExporter.serviceStatus("service1")));
    }

    @Test
    public void serviceIsNotRunningBeforeActualStart()
    {
        boolean serviceRunning = jmxExporter.isServiceRunning("service1");
        assertThat(serviceRunning, is(false));
    }

    @Test
    public void allServicesListContainsMyService()
    {
        String[] services = jmxExporter.allServices();
        assertThat(services, hasItemInArray("service1"));
    }

    @Test
    public void allServicesByStatusReturnsStartingStatusBeforeActualStart()
    {
        String[] statuses = jmxExporter.allServicesByStatus();
        assertThat(statuses[0], containsString(ServiceStatus.STARTING.toString()));
        assertThat(statuses[0], containsString("service1"));
    }

    @Test
    public void stopServiceWorksBeforeActualStart() throws Exception {
        jmxExporter.stopService("service1", false);
    }

    @Test
    public void stopAllServicesWorksBeforeActualStart()
    {
        serviceLauncher.stopAllServices(false);
    }

    @Test
    public void runningCountIsZeroBeforeActualRun()
    {
        assertThat(serviceLauncher.runningCount(), is(0));
        assertThat(serviceLauncher.isRunning(), is(false));
    }

    @Test
    public void startServiceWorks() throws InterruptedException
    {
        jmxExporter.startService("service1");
        Thread.sleep(1000);
        serviceLauncher.join();
        assertThat(serviceLauncher.runningCount(), is(0));
    }

    @Test
    public void doubleRunWorks() throws InterruptedException
    {
        jmxExporter.startService("service1");
        Thread.sleep(1000);
        serviceLauncher.join();
        jmxExporter.startService("service1");
        Thread.sleep(1000);
        serviceLauncher.join();
        assertThat(serviceLauncher.runningCount(), is(0));
    }

    @Test(expected = IllegalStateException.class)
    public void shouldGetExceptionForAttemptToRunAlreadyRunningService() throws InterruptedException
    {
        try {
            jmxExporter.startService("service1");
            Thread.sleep(200);
            while (!serviceLauncher.isRunning()) {
                Thread.sleep(10);
            }
            jmxExporter.startService("service1");
        }
        finally {
            serviceLauncher.join();
            assertThat(serviceLauncher.runningCount(), is(0));
        }
    }
}
