package com.katesoft.scale4j.agent.args;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AgentArgsParserTest
{
    @Test
    public void parseAndValidateForDefaultOptionsWorks() throws Exception
    {
        AgentOptions options = AgentArgsParser.parseAndValidateOptions(new String[]{});
        assertThat(options.appConfigurationLocation(), is(Matchers.<Object>nullValue()));
        assertThat(options.log4jConfigurationLocation(), is(Matchers.<Object>nullValue()));
    }

    @Test
    public void logConfigurationResolved() throws Exception
    {
        AgentOptions options = AgentArgsParser.parseAndValidateOptions(new String[]{"-l", "log4j.xml"});
        assertThat(options.appConfigurationLocation(), is(Matchers.<Object>nullValue()));
        assertThat(options.log4jConfigurationLocation(), is(Matchers.<Object>notNullValue()));
    }

    @Test
    public void appConfigurationResolved() throws Exception
    {
        AgentOptions options = AgentArgsParser.parseAndValidateOptions(new String[]{"-c", "xml/testAppConfiguration.xml"});
        assertThat(options.appConfigurationLocation(), is(Matchers.<Object>notNullValue()));
        assertThat(options.log4jConfigurationLocation(), is(Matchers.<Object>nullValue()));
    }

    @Test
    public void logAndAppConfigurationResolved() throws IOException
    {
        AgentOptions options = AgentArgsParser.parseAndValidateOptions(new String[]{"-l", "log4j.xml", "-c", "xml/testAppConfiguration.xml"});
        assertThat(options.appConfigurationLocation(), is(Matchers.<Object>notNullValue()));
        assertThat(options.log4jConfigurationLocation(), is(Matchers.<Object>notNullValue()));
        assertThat(options.appConfigurationLocation().exists(), is(true));
        assertThat(options.log4jConfigurationLocation().exists(), is(true));
    }

    @Test(expected = IllegalArgumentException.class)
    public void wrongOptionCausedException() throws IOException
    {
        AgentArgsParser.parseAndValidateOptions(new String[]{"-l1", "log4j.xml"});
    }

    @Test
    public void fullyQualifiedAppConfigurationResolved() throws Exception
    {
        AgentOptions options = AgentArgsParser
            .parseAndValidateOptions(new String[]{"-c", "file:///" + new ClassPathResource("xml/testAppConfiguration.xml").getFile().getAbsolutePath()});
        assertThat(options.appConfigurationLocation(), is(Matchers.<Object>notNullValue()));
        assertThat(options.log4jConfigurationLocation(), is(Matchers.<Object>nullValue()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void notExistingResourceCausesException() throws IOException
    {
        AgentArgsParser.parseAndValidateOptions(new String[]{"-c", "file:///not_existing_resource.xml"});
    }

    @Test
    public void timeOutParsed()
    {
        AgentOptions options = AgentArgsParser.parseAndValidateOptions(new String[]{"-t", "152"});
        assertThat(options.getTimeout(), is(152));
    }
}
