package com.katesoft.scale4j.agent.jmx;

import com.katesoft.scale4j.agent.ApplicationConfigurationReader;
import com.katesoft.scale4j.agent.ServiceLauncher;
import com.katesoft.scale4j.agent.args.AgentOptions;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.rttp.jmx.IRttpSupportMBean;
import com.katesoft.xml.jvmcluster.ApplicationConfigurationDocument.ApplicationConfiguration;
import com.katesoft.xml.jvmcluster.ServiceDocument.Service;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jmx.support.ObjectNameManager;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

/** @author kate2007 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/jvmcluster$oxm.xml"})
@DirtiesContext
public class KillAppTest
{
    @BeforeClass
    public static void beforeClass()
    {
        System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
    }

    @Autowired
    private ApplicationConfigurationReader applicationConfigurationReader;

    @Test
    public void ableToStopServiceSimpleService() throws Exception
    {
        AgentOptions options = new AgentOptions();
        ApplicationConfiguration applicationConfiguration =
            applicationConfigurationReader.parseConfiguration(new ClassPathResource("xml/testSpringAppConfiguration.xml").getInputStream());
        ServiceLauncher serviceLauncher = new ServiceLauncher(applicationConfiguration, options);
        try {
            serviceLauncher.startService("service1");
            Thread.sleep(1000);
            serviceLauncher.stopService("service1", false);
        }
        finally {
            serviceLauncher.stopAllServices(true);
        }
    }

    @Test
    public void mBeanConnectFailureExceptionShouldBeThrownIfJMXRemotePortIsNotConfiguredCorrectly() throws Exception
    {
        AgentOptions options = new AgentOptions();
        ApplicationConfiguration applicationConfiguration =
            applicationConfigurationReader.parseConfiguration(new ClassPathResource("xml/testSpringRttpAppConfiguration.xml").getInputStream());
        ServiceLauncher serviceLauncher = new ServiceLauncher(applicationConfiguration, options);
        try {
            serviceLauncher.startService("service1");
            Thread.sleep(1000);
            serviceLauncher.stopService("service1", false);
        }
        finally {
            serviceLauncher.stopAllServices(true);
        }
    }

    @Test
    public void ableToStopRttpService2() throws Exception
    {
        AgentOptions options = new AgentOptions();
        ApplicationConfiguration applicationConfiguration =
            applicationConfigurationReader.parseConfiguration(new ClassPathResource("xml/testSpringRttpAppConfiguration.xml").getInputStream());
        ServiceLauncher serviceLauncher = new ServiceLauncher(applicationConfiguration, options);
        try {
            serviceLauncher.startService("service2");
            waitForJmxExport(applicationConfiguration, "service2", 30);
            serviceLauncher.stopService("service2", false);
        }
        finally {
            serviceLauncher.stopAllServices(true);
        }
    }

    private void waitForJmxExport(ApplicationConfiguration applicationConfiguration,
                                  String serviceName,
                                  int timeout) throws InterruptedException
    {
        long start = System.currentTimeMillis();
        Service s = null;
        Service[] serviceArray = applicationConfiguration.getServiceArray();
        for (Service service : serviceArray) {
            if (service.getName().equalsIgnoreCase(serviceName)) {
                s = service;
                break;
            }
        }
        if (s != null) {
            boolean rttpExported = false;
            do {
                try {
                    Thread.sleep(100);
                    int jmxPort = s.getJmxPort().intValue();
                    String url = String.format("service:jmx:rmi:///jndi/rmi://localhost:%s/jmxrmi", jmxPort);
                    JMXServiceURL jmxServiceURL = new JMXServiceURL(url);
                    JMXConnector connector = JMXConnectorFactory.connect(jmxServiceURL);
                    if (connector.getMBeanServerConnection().isRegistered(ObjectNameManager.getInstance(IRttpSupportMBean.OBJ_RTTP_SUPPORT))) {
                        rttpExported = true;
                    }
                    connector.close();
                }
                catch (Exception e) {}
            }
            while (!rttpExported && (System.currentTimeMillis() - start) < (timeout * 1000));
        }
    }
}
