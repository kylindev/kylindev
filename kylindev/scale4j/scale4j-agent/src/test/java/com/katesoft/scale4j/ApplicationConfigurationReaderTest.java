package com.katesoft.scale4j;

import com.katesoft.scale4j.agent.ApplicationConfigurationReader;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.xml.jvmcluster.ApplicationConfigurationDocument.ApplicationConfiguration;
import com.katesoft.xml.jvmcluster.JvmConfigurationDocument.JvmConfiguration.VmType;
import com.katesoft.xml.jvmcluster.ServiceDocument.Service;
import org.apache.xmlbeans.XmlException;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.HashSet;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/jvmcluster$oxm.xml")
public class ApplicationConfigurationReaderTest
{
    @BeforeClass
    public static void beforeClass()
    {
        System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
    }

    @Autowired
    private ApplicationConfigurationReader reader;
    private ApplicationConfiguration configuration;

    @Before
    public void setUp() throws XmlException, IOException {
        configuration = reader.parseConfiguration("xml/testAppConfiguration.xml");
    }

    @Test
    public void containsFewServices() throws Exception
    {
        Service[] services = configuration.getServiceArray();
        assertThat(services.length, greaterThan(1));
    }

    @Test
    public void serviceNamesAreUnique() {
        Service[] services = configuration.getServiceArray();
        HashSet<String> set = new HashSet<String>();
        for (Service service : services) {
            set.add(service.getName());
        }
        assertThat(services.length, is(set.size()));
    }

    @Test
    public void serverClientOptionsResolved() {
        Service[] services = configuration.getServiceArray();
        assertThat(services[0].getJvmConfiguration().getVmType(), is(not(services[1].getJvmConfiguration().getVmType())));
        assertThat(services[0].getJvmConfiguration().getVmType(), is(VmType.SERVER));
        assertThat(services[1].getJvmConfiguration().getVmType(), is(VmType.CLIENT));
    }

    @Test
    public void maxAndInitialHeapSpecifiedForFirstService() {
        Service[] services = configuration.getServiceArray();
        assertThat(services[0].getJvmConfiguration().getMemoryConfiguration().getInitialHeap().intValue(), is(8));
        assertThat(services[0].getJvmConfiguration().getMemoryConfiguration().getMaxHeap().intValue(), is(16));
        assertThat(services[1].getJvmConfiguration().getMemoryConfiguration(), is(Matchers.<Object>nullValue()));
    }

    @Test
    public void debugConfigurationResolved() {
        Service[] services = configuration.getServiceArray();
        assertThat(services[0].getJvmConfiguration().getDebugConfiguration(), is(Matchers.<Object>nullValue()));
        assertThat(services[1].getJvmConfiguration().getDebugConfiguration().getPort(), is((short) 8366));
    }

    @Test
    public void mainClassResolved() throws ClassNotFoundException {
        Service[] services = configuration.getServiceArray();
        assertThat(Class.forName(services[0].getMainClass()).getName(), is(SimpleApplication.class.getName()));
        assertThat(Class.forName(services[1].getMainClass()).getName(), is(SimpleApplication.class.getName()));
    }

    @Test
    public void globalConfigurationParsed() {
        assertThat(configuration.getDescription(), is(Matchers.<Object>notNullValue()));
        assertThat(configuration.getGlobalConfiguration(), is(Matchers.<Object>notNullValue()));

        assertThat(configuration.getGlobalConfiguration().getJvmArgArray().length, greaterThan(0));
    }

    @Test
    public void jvmArgsPassedToServiceAreValid() {
        Service[] services = configuration.getServiceArray();
        assertThat(services[0].getJvmConfiguration().getJvmArgArray().length, greaterThan(0));
        assertThat(services[1].getJvmConfiguration().getJvmArgArray().length, greaterThan(0));
    }
}
