package com.katesoft.scale4j;

import com.katesoft.scale4j.agent.ApplicationConfigurationReader;
import com.katesoft.scale4j.agent.JavaProcessBuilder;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.io.ByteArrayOutputStream2;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.xml.jvmcluster.ApplicationConfigurationDocument.ApplicationConfiguration;
import org.apache.xmlbeans.XmlException;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/jvmcluster$oxm.xml")
public class JavaProcessFromApplicationConfigurationTest
{
    private static final Logger l = LogFactory.getLogger(JavaProcessFromApplicationConfigurationTest.class);

    @BeforeClass
    public static void beforeClass()
    {
        System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
    }

    //
    @Autowired
    private ApplicationConfigurationReader reader;
    private ApplicationConfiguration configuration;

    @Before
    public void setUp() throws XmlException, IOException
    {
        configuration = reader.parseConfiguration("xml/testAppConfiguration.xml");
    }

    @Test
    public void firstServiceCanBeExecuted() throws IOException, InterruptedException
    {
        JavaProcessBuilder builder = new JavaProcessBuilder(configuration, configuration.getServiceArray(0));
        String s = startService(builder);
        assertThat(s, containsString(Arrays.toString(new String[]{"arg1", "arg2", "arg3"})));
        assertThat(s, containsString("service1.property=1"));
    }

    @Test
    public void secondServiceCanBeExecuted() throws IOException, InterruptedException
    {
        JavaProcessBuilder builder = new JavaProcessBuilder(configuration, configuration.getServiceArray(1));
        String s = startService(builder);
        assertThat(s, containsString(Arrays.toString(new String[]{"arg4", "arg5", "arg6"})));
        assertThat(s, containsString("service2.property=2"));
    }

    public static String startService(JavaProcessBuilder builder) throws InterruptedException, IOException
    {
        builder.classpath(System.getProperty("java.class.path"));
        ByteArrayOutputStream2 outputStream2 = new ByteArrayOutputStream2();
        Process process = builder.launch(outputStream2, System.err);
        process.waitFor();
        String s = new String(outputStream2.toByteSequence().buffer());
        l.debug("*************************************************");
        l.debug(s);
        l.debug("*************************************************");
        if (process.exitValue() != 0) { throw new IOException("exitValue is " + process.exitValue()); }
        return s;
    }
}
