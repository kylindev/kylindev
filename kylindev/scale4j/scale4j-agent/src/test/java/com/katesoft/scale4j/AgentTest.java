package com.katesoft.scale4j;

import com.katesoft.scale4j.agent.AgentLauncher;
import com.katesoft.scale4j.agent.ServiceLauncher;
import com.katesoft.scale4j.agent.args.AgentOptions;
import com.katesoft.scale4j.agent.exceptions.ExecutionTimeoutException;
import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class AgentTest
{
    private AgentOptions options;
    private Logger logger = LogFactory.getLogger(getClass());

    @BeforeClass
    public static void beforeClass()
    {
        System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
    }

    @Before
    public void before()
    {
        options = new AgentOptions();
        options.setKill(false);
        options.setStop(false);
        options.setLog4jConfigurationLocation(null);
    }

    @Test
    public void simpleApplicationExecutionCompleted() throws Throwable
    {
        options.setAppConfigurationLocation("xml/testAppConfiguration.xml");
        AgentLauncher launcher = new AgentLauncher(options);
        ServiceLauncher serviceLauncher = launcher.call();
        try {
            launcher.waitFor();
            assertThat(serviceLauncher.isStartupFailed(), is(false));
        }
        catch (Throwable e) {
            logger.error(e);
            throw e;
        }
        finally {
            launcher.destroy();
        }
    }

    @Test
    public void springApplicationExecutionCompleted() throws Throwable
    {
        options.setAppConfigurationLocation("xml/testSpringAppConfiguration.xml");
        AgentLauncher launcher = new AgentLauncher(options);
        ServiceLauncher serviceLauncher = launcher.call();
        try {
            launcher.waitFor();
            assertThat(serviceLauncher.isStartupFailed(), is(false));
        }
        catch (Throwable e) {
            logger.error(e);
            throw e;
        }
        finally {
            launcher.destroy();
        }
    }

    @Test(expected = ExecutionTimeoutException.class)
    public void springRttpExecutionCompleted() throws Throwable
    {
        options.setTimeout(20);
        options.setAppConfigurationLocation("xml/testSpringRttpAppConfiguration.xml");
        AgentLauncher launcher = new AgentLauncher(options);
        ServiceLauncher serviceLauncher = launcher.call();
        try {
            launcher.waitFor();
            assertThat(serviceLauncher.isStartupFailed(), is(false));
        }
        catch (Throwable e) {
            logger.error(e);
            throw e;
        }
        finally {
            launcher.destroy();
        }
    }
}
