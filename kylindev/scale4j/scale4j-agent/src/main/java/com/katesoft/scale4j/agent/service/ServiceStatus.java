package com.katesoft.scale4j.agent.service;

/** @author kate2007 */
public enum ServiceStatus {
   STARTING, RUNNING, FAILED_TO_START, TERMINATED;

   public boolean isActive() {
      return this == STARTING || this == RUNNING;
   }

   public boolean isRunning() {
      return this == RUNNING;
   }

   public boolean isFailedToStart() {
      return this == FAILED_TO_START;
   }
}
