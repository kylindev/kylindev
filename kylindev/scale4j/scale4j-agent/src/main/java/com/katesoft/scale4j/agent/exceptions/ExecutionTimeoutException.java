package com.katesoft.scale4j.agent.exceptions;

/**
 * This exception indicates that execution of all services took more time than expected,
 * 
 * @author kate2007
 */
public class ExecutionTimeoutException extends RuntimeException {
   private static final long serialVersionUID = 8224513547379976004L;

   public ExecutionTimeoutException(String message) {
      super(message);
   }
}
