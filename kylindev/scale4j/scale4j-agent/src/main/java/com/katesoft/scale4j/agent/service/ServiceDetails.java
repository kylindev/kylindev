package com.katesoft.scale4j.agent.service;

import java.util.Date;

/** @author kate2007 */
public class ServiceDetails {
   private long launchedAt;
   private ServiceStatus status;
   private String configuration;

   public ServiceDetails(String configuration) {
      this.configuration = configuration;
      status = ServiceStatus.STARTING;
      launchedAt = System.currentTimeMillis();
   }

   public ServiceStatus getStatus() {
      return status;
   }

   public void setStatus(ServiceStatus status) {
      this.status = status;
   }

   public Date launchedAt() {
      return new Date(launchedAt);
   }

   public String serviceConfiguration() {
      return configuration;
   }
}
