package com.katesoft.scale4j.agent.jmx;

import com.katesoft.scale4j.common.jmx.JmxOperationInvoker;

/** @author kate2007 */
public class StopAgentOperationInvoker extends JmxOperationInvoker<IServiceManagerMBean> {
   private boolean forceKill;

   public StopAgentOperationInvoker(String url) {
      super(url, IServiceManagerMBean.OBJ_NAME, IServiceManagerMBean.class);
   }

   public void setForceKill(boolean forceKill) {
      this.forceKill = forceKill;
   }

   @Override
   protected void doInvoke(IServiceManagerMBean proxy) {
      logger.info("calling jmx operation agent.stop(%s)", forceKill);
      proxy.stop(forceKill);
   }
}
