package com.katesoft.scale4j.agent.exceptions;

/**
 * This exception indicates that agent was not able to start all services and aborted.
 * 
 * @author kate2007
 */
public class FailedToStarAllServicesException extends RuntimeException {
   private static final long serialVersionUID = 3451816537029356134L;

   public FailedToStarAllServicesException(Throwable cause) {
      super(cause);
   }
}
