package com.katesoft.scale4j.agent.jmx;

import java.util.Date;

/** @author kate2007 */
public interface IServiceManagerMBean {
   String OBJ_NAME = "scale4j.agent:name=serviceManager";

   boolean isServiceRunning(String name);

   String serviceStatus(String name);

   String serviceConfiguration(String name);

   Date startedAt(String name);

   String[] allServices();

   String[] allServicesByStatus();

   void stopService(String name, boolean forceKill) throws Exception;

   void stop(boolean forceKill);

   void startService(String name);
}
