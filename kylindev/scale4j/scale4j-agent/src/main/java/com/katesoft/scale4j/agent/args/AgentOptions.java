package com.katesoft.scale4j.agent.args;

import com.katesoft.scale4j.common.annotation.ValueObject;
import com.katesoft.scale4j.common.io.FileUtility;
import org.kohsuke.args4j.Option;
import org.springframework.core.io.Resource;

import java.io.IOException;

/**
 * POJO that defines available options for agent launcher.
 * 
 * @author kate2007
 */
@ValueObject
public class AgentOptions {
   @Option(name = "-l", aliases = "--log-cfg", usage = "log4j configuration location file")
   private String log4jConfigurationLocation;
   @Option(name = "-c", aliases = "--app-cfg", usage = "appConfiguration location file")
   private String appConfigurationLocation;
   @Option(
           name = "-t",
           aliases = "--timeout",
           usage = "max time (secs) for application to run(for JUnit testing only)")
   private int timeout;
   @Option(name = "-k", aliases = "--kill", usage = "kill all launched services")
   private boolean kill;
   @Option(name = "-s", aliases = "--stop", usage = "stop all launched services")
   private boolean stop;

   public void setLog4jConfigurationLocation(String log4jConfigurationLocation) {
      this.log4jConfigurationLocation = log4jConfigurationLocation;
   }

   public void setAppConfigurationLocation(String appConfigurationLocation) {
      this.appConfigurationLocation = appConfigurationLocation;
   }

   public void setTimeout(int timeout) {
      this.timeout = timeout;
   }

   public void setKill(boolean kill) {
      this.kill = kill;
   }

   public void setStop(boolean stop) {
      this.stop = stop;
   }

   public Resource log4jConfigurationLocation() throws IOException {
      return FileUtility.resolve(log4jConfigurationLocation);
   }

   public Resource appConfigurationLocation() throws IOException {
      return FileUtility.resolve(appConfigurationLocation);
   }

   public int getTimeout() {
      return timeout;
   }

   public boolean isKill() {
      return kill;
   }

   public boolean isStop() {
      return stop;
   }
}
