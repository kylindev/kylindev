package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import com.katesoft.scale4j.rttp.internal.IRttpHazelcastRefs;
import com.katesoft.scale4j.rttp.spring.RttpAnnotationSessionFactoryLock;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

/**
 * @author kate2007
 */
public abstract class AbstractRTTPTransactionalTest extends AbstractPersistentTest {
   @Autowired
   private SpringHazelcastBridge springHazelcastBridge;

   @Before
   @Override
   public void setUp() throws Exception {
      System.setProperty(RuntimeUtility.VAR_SERVICE_ID,
               String.format("%s_%s", "service", UUID.randomUUID().toString()));
      springHazelcastBridge.updateAtomicBoolean(
               RttpAnnotationSessionFactoryLock.RTTP_SCHEMA_DB_UPDATE_DONE, false);
      springHazelcastBridge.updateAtomicBoolean(IRttpHazelcastRefs.BATCH_DB_CREATION_LOCK, false);
      springHazelcastBridge.updateAtomicBoolean(
               IRttpHazelcastRefs.SCHEDULER_QUARTZ_DB_CREATION_LOCK, false);
      springHazelcastBridge.updateAtomicBoolean(IRttpHazelcastRefs.MESSAGE_STORE_CREATION_LOCK,
               false);
      super.setUp();
   }

   @After
   @Override
   public void tearDown() throws Exception {
      super.tearDown();
   }
}
