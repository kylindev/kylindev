package com.katesoft.scale4j.rttp.jmx;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import com.katesoft.scale4j.rttp.jobs.LocalSchedulerFactoryBean;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.quartz.core.jmx.QuartzSchedulerMBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.access.MBeanProxyFactoryBean;

import javax.management.MBeanServer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class QuartzAccessorTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private MBeanServer mBeanServer;
   private QuartzSchedulerMBean proxy;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      LocalSchedulerFactoryBean schedulerFactoryBean = (LocalSchedulerFactoryBean) applicationContext
               .getBean("&" + IBeanNameReferences.RTTP_CLIENT_SCHEDULER);
      MBeanProxyFactoryBean factoryBean = new MBeanProxyFactoryBean();
      factoryBean.setProxyInterface(QuartzSchedulerMBean.class);
      factoryBean.setObjectName(schedulerFactoryBean.getJmxExportName());
      factoryBean.setServer(mBeanServer);
      factoryBean.afterPropertiesSet();
      proxy = (QuartzSchedulerMBean) factoryBean.getObject();
   }

   @Test
   public void quartzBeanAvailable() throws InterruptedException {
      assertThat(proxy.isStarted(), is(true));
   }

   @Test
   public void versionAvailable() {
      assertThat(proxy.getVersion(), is(Matchers.<Object> notNullValue()));
   }
}
