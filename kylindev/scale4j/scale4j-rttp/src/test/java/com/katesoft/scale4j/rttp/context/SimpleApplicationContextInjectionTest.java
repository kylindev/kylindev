package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.persistent.model.InternalDomainEntity;
import com.katesoft.scale4j.rttp.jmx.RttpSupportService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author kate2007
 */
public class SimpleApplicationContextInjectionTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private RttpSupportService rttpSupportService;

   @Test
   public void hibernateTransactionManagerInjected() {
      assertThat(hibernateTransactionManager, is(notNullValue()));
   }

   @Test
   public void internalDomainObjectsInitialized() {
      assertThat(hibernateTemplate.loadAll(InternalDomainEntity.class).size(), is(hibernateTemplate
               .getSessionFactory().getAllClassMetadata().size()));
   }

   @Test
   public void ableToFindBeanOfType() throws ClassNotFoundException {
      assertThat(rttpSupportService.beanNamesForType(RttpSupportService.class.getName()).size(),
               is(1));
   }
}
