package com.katesoft.scale4j.rttp.client;

import com.katesoft.scale4j.persistent.client.AbstractLocalHibernateDaoSupportTest;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/jvmcluster-scheduler-junit-context.xml",
         "/META-INF/spring/jvmcluster.rttp$mysql-postprocessing.xml" })
@TransactionConfiguration
@Transactional(propagation = Propagation.NOT_SUPPORTED)
@DirtiesContext
public class MySQLLocalHibernateDaoSupportPortabilityTest extends
         AbstractLocalHibernateDaoSupportTest {
}
