package com.katesoft.scale4j.rttp.jmx;

import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import org.hibernate.jmx.StatisticsServiceMBean;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jmx.access.MBeanProxyFactoryBean;

import javax.management.MBeanServer;

import static com.katesoft.scale4j.rttp.jmx.IRttpSupportMBean.OBJ_RTTP_HIBERNATE_STATISTIC;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author kate2007
 */
public class HibernateAccessorTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private MBeanServer mBeanServer;
   private StatisticsServiceMBean proxy;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      MBeanProxyFactoryBean factoryBean = new MBeanProxyFactoryBean();
      factoryBean.setProxyInterface(StatisticsServiceMBean.class);
      factoryBean.setObjectName(OBJ_RTTP_HIBERNATE_STATISTIC);
      factoryBean.setServer(mBeanServer);
      factoryBean.afterPropertiesSet();
      proxy = (StatisticsServiceMBean) factoryBean.getObject();
   }

   @Test
   public void startTimeAvailable() {
      assertThat(proxy.getStartTime(), is(lessThanOrEqualTo(System.currentTimeMillis())));
   }

   @Test
   public void entitiesCountMoreThanOne() {
      assertThat(proxy.getEntityNames().length, is(greaterThan(0)));
   }
}
