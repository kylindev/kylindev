package com.katesoft.scale4j.rttp.messaging;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Queue;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.support.MessageBuilder;

import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;

/**
 * @author kate2007
 */
public class DistributedQueueMessageChannelTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   @Qualifier("rttp.junit.channel1")
   private QueueChannel queueChannel1;
   @Autowired
   @Qualifier("rttp.junit.channel2")
   private QueueChannel queueChannel2;
   @Autowired
   @Qualifier("rttp.junit.channel3")
   private QueueChannel queueChannel3;
   //
   private Queue<?> inMemoryQueue1, inMemoryQueue2;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      //
      inMemoryQueue1 = springHazelcastBridge.getHazelcastInstance()
               .getQueue("rttp.junit.si.queue1");
      inMemoryQueue2 = springHazelcastBridge.getHazelcastInstance().getQueue(
               "rttp.junit.si.queue2.im-memory");
      //
      inMemoryQueue1.clear();
      inMemoryQueue2.clear();
      jdbcTemplate.update("truncate table INT_MESSAGE_GROUP");
   }

   @Test
   public void ableToPublishMessagesToChannel1() {
      Message<String> message1 = MessageBuilder.withPayload("message1").build();
      Message<String> message2 = MessageBuilder.withPayload("message2").build();
      //
      MessagingTemplate messagingTemplate = new MessagingTemplate();
      messagingTemplate.send(queueChannel1, message1);
      messagingTemplate.send(queueChannel1, message2);
      assertThat(inMemoryQueue1.size(), is(2));
      assertThat(inMemoryQueue2.size(), is(0));
   }

   @Test
   public void ableToPublishMessagesToChannel2() {
      Message<String> message1 = MessageBuilder.withPayload("message1").build();
      Message<String> message2 = MessageBuilder.withPayload("message2").build();
      //
      MessagingTemplate messagingTemplate = new MessagingTemplate();
      messagingTemplate.send(queueChannel2, message1);
      messagingTemplate.send(queueChannel2, message2);
      assertThat(inMemoryQueue2.size(), is(2));
      assertThat(inMemoryQueue1.size(), is(0));
   }

   @Test
   public void ableToPublishMessagesToChannel3() throws InterruptedException {
      Message<String> message1 = MessageBuilder.withPayload("message1").build();
      Message<String> message2 = MessageBuilder.withPayload("message2").build();
      Message<String> message3 = MessageBuilder.withPayload("message3").build();
      //
      MessagingTemplate messagingTemplate = new MessagingTemplate();
      messagingTemplate.send(queueChannel3, message1);
      messagingTemplate.send(queueChannel3, message2);
      messagingTemplate.send(queueChannel3, message3);
      //
      assertThat(jdbcTemplate.queryForInt("select count(*) from INT_MESSAGE_GROUP"), is(3));
   }
}
