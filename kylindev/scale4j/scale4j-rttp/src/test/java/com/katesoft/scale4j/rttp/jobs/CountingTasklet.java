package com.katesoft.scale4j.rttp.jobs;

import java.io.Serializable;

import org.junit.Test;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

/**
 * @author kate2007
 */
public class CountingTasklet implements Tasklet, Serializable {
   private static final long serialVersionUID = 6164354413018435631L;
   private static long counter = 0;

   @SuppressWarnings("unused")
   @Override
   public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
      System.out.println(String.format(
               "executing simple counting task, current counter value = %s", counter));
      counter++;
      return RepeatStatus.FINISHED;
   }

   public static long getCounter() {
      return counter;
   }

   public static void reset() {
      counter = 0;
   }

   @Test
   public void works() {
      getCounter();
   }
}
