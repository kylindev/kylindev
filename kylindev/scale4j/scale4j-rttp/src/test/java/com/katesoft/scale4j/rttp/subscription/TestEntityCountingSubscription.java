package com.katesoft.scale4j.rttp.subscription;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;

import org.junit.Ignore;

import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.rttp.subscription.common.TruePredicate;

/**
 * @author kate2007
 */
@SuppressWarnings("serial")
@Ignore
public class TestEntityCountingSubscription implements ICloudSubscription<TestEntity>, Serializable {
   private final LinkedHashSet<IBusinessProcess<TestEntity>> processes;
   private static Collection<TestEntity> insertEntities = new LinkedHashSet<TestEntity>();
   private static Collection<TestEntity> updateEntities = new LinkedHashSet<TestEntity>();
   private static Collection<TestEntity> deleteEntities = new LinkedHashSet<TestEntity>();

   @Ignore
   private static class Insert implements IBusinessProcess<TestEntity>, Serializable {
      @Override
      public void process(SubscriptionEvent<TestEntity> testEntitySubscriptionEvent) {
         if (testEntitySubscriptionEvent.getType() == CrudType.CREATE) {
            System.out.println(String.format("adding [%s] to insertEntities collection",
                     testEntitySubscriptionEvent.getEntity()));
            insertEntities.add(testEntitySubscriptionEvent.getEntity());
         }
      }
   }

   @Ignore
   private static class Update implements IBusinessProcess<TestEntity>, Serializable {
      @Override
      public void process(SubscriptionEvent<TestEntity> testEntitySubscriptionEvent) {
         if (testEntitySubscriptionEvent.getType() == CrudType.UPDATE) {
            System.out.println(String.format("adding [%s] to updateEntities collection",
                     testEntitySubscriptionEvent.getEntity()));
            updateEntities.add(testEntitySubscriptionEvent.getEntity());
         }
      }
   }

   @Ignore
   private static class Delete implements IBusinessProcess<TestEntity>, Serializable {
      @Override
      public void process(SubscriptionEvent<TestEntity> testEntitySubscriptionEvent) {
         if (testEntitySubscriptionEvent.getType() == CrudType.DELETE) {
            System.out.println(String.format("adding [%s] to deleteEntities collection",
                     testEntitySubscriptionEvent.getEntity()));
            deleteEntities.add(testEntitySubscriptionEvent.getEntity());
         }
      }
   }

   @SuppressWarnings("unchecked")
   public TestEntityCountingSubscription() {
      processes = new LinkedHashSet<IBusinessProcess<TestEntity>>(Arrays.asList(new Insert(),
               new Update(), new Delete()));
   }

   public void reset() {
      insertEntities.clear();
      updateEntities.clear();
      deleteEntities.clear();
   }

   public int insertCount() {
      return insertEntities.size();
   }

   public int updateCount() {
      return updateEntities.size();
   }

   public int deleteCount() {
      return deleteEntities.size();
   }

   @Override
   public Class<TestEntity> getSubscriptionClass() {
      return TestEntity.class;
   }

   @Override
   public IUnaryPredicate<TestEntity> getPredicate() {
      return new TruePredicate<TestEntity>();
   }

   @Override
   public Collection<IBusinessProcess<TestEntity>> getProcesses() {
      return processes;
   }
}
