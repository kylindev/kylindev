package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kate2007
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/xa-rttpContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml",
         "/META-INF/spring/rttp-jobs-context.xml", "/META-INF/spring/rttp-batch-context.xml",
         "/META-INF/spring/jvmcluster-batch-junit.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
public abstract class AbstractXAContextRTTPTest extends AbstractRTTPTransactionalTest {
}
