package com.katesoft.scale4j.rttp.jobs.jdbc.init;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import com.katesoft.scale4j.rttp.mocks.SpringHazelcastBridgeMock;
import org.hibernate.dialect.H2Dialect;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ConnectionCallback;
import org.springframework.jdbc.datasource.init.DataSourceInitializer;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class RttpBatchSchemaCreatorIntegrationTest extends AbstractSimpleContextPersistentTest {
   private DataSourceInitializer dataSourceInitializer;
   private RttpBatchSchemaCreator rttpBatchSchemaCreator;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      dataSourceInitializer = new DataSourceInitializer();
      dataSourceInitializer.setDataSource(dataSource);
      dataSourceInitializer.setEnabled(true);
      SpringHazelcastBridge springHazelcastBridge = new SpringHazelcastBridgeMock();
      rttpBatchSchemaCreator = new RttpBatchSchemaCreator();
      rttpBatchSchemaCreator.setDialect(H2Dialect.class);
      rttpBatchSchemaCreator.setRttpHazelcastBridge(springHazelcastBridge);
      rttpBatchSchemaCreator.afterPropertiesSet();
      dataSourceInitializer.setDatabasePopulator(rttpBatchSchemaCreator);
   }

   @Test
   public void schemaCreated() throws Exception {
      final AtomicBoolean r = new AtomicBoolean(false);
      jdbcTemplate.execute(new ConnectionCallback<Object>() {
         @Override
         public Object doInConnection(Connection con) throws SQLException, DataAccessException {
            try {
               dataSourceInitializer.afterPropertiesSet();
               r.set(rttpBatchSchemaCreator.tablesCreated(con));
               assertThat(r.get(), is(true));
               dataSourceInitializer.afterPropertiesSet();
               dataSourceInitializer.afterPropertiesSet();
            } catch (Exception e) {
               throw new RuntimeException(e);
            }
            return null;
         }
      });
   }
}
