package com.katesoft.scale4j.rttp.jobs;

import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/jvmcluster-scheduler-junit-context.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
@TransactionConfiguration
@Transactional
@DirtiesContext
public class RttpComplexJobLauncherTest extends AbstractComplexJobLauncherTest {
}
