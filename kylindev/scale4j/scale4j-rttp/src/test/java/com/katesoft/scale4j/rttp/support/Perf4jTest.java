package com.katesoft.scale4j.rttp.support;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.test.annotation.Repeat;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
@Transactional
public abstract class Perf4jTest extends AbstractPersistentTest {
   @Test
   @Repeat(2)
   public void saveWorks() {
      for (int i = 0; i < 10; i++) {
         hibernateTemplate.save(TestEntity.newTestInstance());
         hibernateTemplate.flush();
      }
   }

   @Test
   @Repeat(2)
   public void deleteWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         hibernateTemplate.delete(entity);
         hibernateTemplate.flush();
      }
   }

   @Test
   @Repeat(2)
   public void loadAllWorks() {
      hibernateTemplate.deleteAll(TestEntity.class);
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         hibernateTemplate.loadAll(TestEntity.class);
         hibernateTemplate.clear();
         hibernateTemplate.loadAll(TestEntity.class, true);
      }
   }

   @Test
   @Repeat(2)
   public void saveOrUpdateWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.saveOrUpdate(entity);
         hibernateTemplate.flush();
         entity.setCrudType(CrudType.UPDATE);
         hibernateTemplate.saveOrUpdate(entity);
         hibernateTemplate.flush();
      }
   }

   @Test
   @Repeat(2)
   public void updateWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.saveOrUpdate(entity);
         hibernateTemplate.flush();
         entity.setCrudType(CrudType.UPDATE);
         hibernateTemplate.update(entity);
         hibernateTemplate.flush();
      }
   }

   @Test
   @Repeat(2)
   @Rollback(false)
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void deleteAllWorks() {
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            for (int i = 0; i < 10; i++) {
               session.save(TestEntity.newTestInstance());
               session.flush();
               hibernateTemplate.deleteAll(TestEntity.class);
            }
            return null;
         }
      });
   }

   @Test
   @Repeat(2)
   public void loadAllByGlobalUniqueIdentifierWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         hibernateTemplate.loadByGlobalUniqueIdentifier(entity.getClass(),
                  entity.getGlobalUniqueIdentifier(), true);
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedQueryAndParamWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedQueryAndNamedParam(TestEntity.QUERY_FIND_BY_UID,
                  "id", entity.getUniqueIdentifier()), is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedQueryAndOneValueWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(
                  hibernateTemplate.findByNamedQuery(TestEntity.QUERY_FIND_BY_UID1,
                           entity.getUniqueIdentifier()), is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedQueryWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedQuery(TestEntity.QUERY_FIND_ALL),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedQueryAndParamsWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedQueryAndNamedParam(
                  TestEntity.QUERY_FIND_BY_UID_AND_VERSION, new String[] { "id", "version" },
                  new Object[] { entity.getUniqueIdentifier(), entity.getVersion() }),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedParamWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedParam(
                  String.format("from TestEntity e where e.%s = :id", TestEntity.PROP_UID), "id",
                  entity.getUniqueIdentifier()), is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNamedParamsWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedParam(String.format(
                  "from TestEntity e where e.%s = :id and e.%s = :version", TestEntity.PROP_UID,
                  TestEntity.PROP_VERSION), new String[] { "id", "version" },
                  new Object[] { entity.getUniqueIdentifier(), entity.getVersion() }),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByValuedBean() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         TestEntity entity1 = (TestEntity) entity.clone();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         entity1.cleanBeanProperties();
         entity1.setUniqueIdentifier(entity.getUniqueIdentifier());
         assertThat(
                  hibernateTemplate.findByValueBean(
                           String.format("from TestEntity e where e.%s = :uniqueIdentifier",
                                    TestEntity.PROP_UID), entity1),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByValuedBeanAndNamedQuery() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         TestEntity entity1 = (TestEntity) entity.clone();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         entity1.cleanBeanProperties();
         entity1.setUniqueIdentifier(entity.getUniqueIdentifier());
         assertThat(hibernateTemplate.findByNamedQueryAndValueBean(TestEntity.QUERY_FIND_BY_UID2,
                  entity1), is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByNameQueryAndValuesWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(hibernateTemplate.findByNamedQuery(TestEntity.QUERY_FIND_BY_UID_AND_VERSION2,
                  entity.getUniqueIdentifier(), entity.getVersion()),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByDetachedCriteriaWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TestEntity.class);
         detachedCriteria.add(Restrictions.eq(TestEntity.PROP_UID, entity.getUniqueIdentifier()));
         hibernateTemplate.findByCriteria(detachedCriteria).iterator().next();
      }
   }

   @Test
   @Repeat(2)
   public void findByDetachedCriteriaWithSizeRestrictionsWorks() {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         DetachedCriteria detachedCriteria = DetachedCriteria.forClass(TestEntity.class);
         detachedCriteria.add(Restrictions.eq(TestEntity.PROP_UID, entity.getUniqueIdentifier()));
         hibernateTemplate.findByCriteria(detachedCriteria, -1, 10).iterator().next();
      }
   }

   @Test
   @Repeat(2)
   public void findByExampleWorks() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         TestEntity entity1 = (TestEntity) entity.clone();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         entity1.setUniqueIdentifier(entity.getUniqueIdentifier());
         assertThat(hibernateTemplate.findByExample(entity1).iterator().next(),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void findByExampleWithRestrictionsWorks() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         TestEntity entity1 = (TestEntity) entity.clone();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         entity1.setUniqueIdentifier(entity.getUniqueIdentifier());
         assertThat(hibernateTemplate.findByExample(entity1, -1, 100).iterator().next(),
                  is(Matchers.<Object> notNullValue()));
      }
   }

   @Test
   @Repeat(2)
   public void bulkUpdateWorks() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
      }
      hibernateTemplate.bulkUpdate(String.format("update TestEntity e set e.%s = %s",
               TestEntity.PROPERTY_ONE_FIELD, "'kate2007'"));
   }

   @Test
   @Repeat(2)
   public void bulkUpdateWithOneValueWorks() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
      }
      hibernateTemplate.bulkUpdate(
               String.format("update TestEntity e set e.%s = ?", TestEntity.PROPERTY_ONE_FIELD),
               "kate2007");
   }

   @Test
   @Repeat(2)
   public void bulkUpdateWithFewValuesWorks() throws CloneNotSupportedException {
      for (int i = 0; i < 10; i++) {
         TestEntity entity = TestEntity.newTestInstance();
         hibernateTemplate.save(entity);
         hibernateTemplate.flush();
      }
      hibernateTemplate.bulkUpdate(String.format("update TestEntity e set e.%s = ?, e.%s = ?",
               TestEntity.PROPERTY_ONE_FIELD, TestEntity.PROPERTY_CRUD_TYPE), "kate2007",
               CrudType.UPDATE);
   }
}
