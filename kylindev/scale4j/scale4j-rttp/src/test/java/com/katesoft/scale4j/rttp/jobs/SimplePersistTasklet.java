package com.katesoft.scale4j.rttp.jobs;

import static org.mockito.Mockito.mock;

import org.junit.Test;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
public class SimplePersistTasklet implements Tasklet {
   private final Logger logger = LogFactory.getLogger(getClass());
   private HibernateTemplate hibernateTemplate;

   @SuppressWarnings("unused")
   @Override
   public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext)
            throws Exception {
      logger.debug("executing simple persistent task");
      hibernateTemplate.save(TestEntity.newTestInstance());
      return RepeatStatus.FINISHED;
   }

   @Test
   public void works() throws Exception {
      SimplePersistTasklet tasklet = new SimplePersistTasklet();
      tasklet.hibernateTemplate = mock(HibernateTemplate.class);
      tasklet.execute(mock(StepContribution.class), mock(ChunkContext.class));
   }

   @Autowired
   @Qualifier(value = IBeanNameReferences.HIBERNATE_TEMPLATE)
   public void setHibernateTemplate(HibernateTemplate hibernateTemplate) {
      this.hibernateTemplate = hibernateTemplate;
   }
}
