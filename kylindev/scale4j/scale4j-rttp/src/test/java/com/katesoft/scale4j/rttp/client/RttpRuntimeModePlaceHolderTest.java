package com.katesoft.scale4j.rttp.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import java.util.Set;

import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Constants;

import com.katesoft.scale4j.common.spring.IPostProcessingOrder;
import com.katesoft.scale4j.common.spring.RuntimePropertyPlaceholderConfigurer;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;

/**
 * @author kate2007
 */
public class RttpRuntimeModePlaceHolderTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private RuntimePropertyPlaceholderConfigurer configurer;

   @Test
   public void orderOfRuntimeModePropertyPlaceholderConfigurerHigher() {
      Constants constants = new Constants(IPostProcessingOrder.class);
      Set<Object> values = constants.getValues(null);
      for (Object value : values) {
         assertThat(configurer.getOrder(), is(lessThanOrEqualTo((Integer) value)));
      }
   }

   @Test
   public void hibernateSchemaCreateUpdateDeletePropertiesResolved() {
      assertThat(sessionFactoryOrEntityManagerFactory.getUpdateScriptsConfiguration()
               .isGenerateSchemaCreateScript(), is(true));
      assertThat(sessionFactoryOrEntityManagerFactory.getUpdateScriptsConfiguration()
               .isGenerateSchemaDropScript(), is(true));
      assertThat(sessionFactoryOrEntityManagerFactory.getUpdateScriptsConfiguration()
               .isGenerateSchemaUpdateScript(), is(true));
   }

   @Test
   public void maxOpenConnectionsPropertyResolved() {
      BasicDataSource basicDataSource = ((BasicDataSource) dataSource);
      assertThat(basicDataSource.getMaxActive(), is(50));
   }
}
