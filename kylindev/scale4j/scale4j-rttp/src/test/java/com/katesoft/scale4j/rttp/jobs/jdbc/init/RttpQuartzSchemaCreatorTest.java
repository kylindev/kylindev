package com.katesoft.scale4j.rttp.jobs.jdbc.init;

import org.hibernate.dialect.DB2Dialect;
import org.hibernate.dialect.DerbyDialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgresPlusDialect;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.dialect.SybaseASE15Dialect;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class RttpQuartzSchemaCreatorTest {
   @Test
   public void oracleDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(Oracle10gDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_ORACLE), is(true));
   }

   @Test
   public void mysql5InnodbDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(MySQL5InnoDBDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_MYSQL_INNODB), is(true));
   }

   @Test
   public void mysqlDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(MySQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_MYSQL), is(true));
   }

   @Test
   public void postgresDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(PostgresPlusDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_POSTGRESQL), is(true));
   }

   @Test
   public void sqlServerDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(SQLServer2008Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_SQL_SERVER), is(true));
   }

   @Test
   public void hsqldbDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(HSQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_HSQLDB), is(true));
   }

   @Test
   public void sybaseDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(SybaseASE15Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_SYBASE), is(true));
   }

   @Test
   public void derbyDetected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(DerbyDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_DERBY), is(true));
   }

   @Test
   public void db2Detected() throws Exception {
      RttpQuartzSchemaCreator populator = new RttpQuartzSchemaCreator();
      populator.setDialect(DB2Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSchedulerCreationScript().getFile().getCanonicalPath()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_QUARTZ_DB2), is(true));
   }
}
