package com.katesoft.scale4j.rttp.jobs;

import com.katesoft.scale4j.rttp.jobs.jdbc.init.RttpBatchSchemaCreator;
import com.katesoft.scale4j.rttp.jobs.jdbc.init.RttpQuartzSchemaCreator;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * @author kate2007
 */
public abstract class AbstractComplexJobLauncherPortabilityTest extends
         AbstractComplexJobLauncherTest {
   @Autowired
   private RttpBatchSchemaCreator rttpBatchSchemaCreator;
   @Autowired
   private RttpQuartzSchemaCreator rttpQuartzSchemaCreator;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      super.validateExecutionCount = false;
   }

   @Test
   public void testUsedDialectWasNotH2Dialect() {
      Class<? extends Dialect> dialect1 = rttpQuartzSchemaCreator.getDialect();
      Class<? extends Dialect> dialect2 = rttpBatchSchemaCreator.getDialect();
      assertThat(dialect1.getName(), is(dialect2.getName()));
      assertThat(dialect1.getName(), is(not(H2Dialect.class.getName())));
      assertThat(dialect2.getName(), is(not(H2Dialect.class.getName())));
   }
}
