package com.katesoft.scale4j.rttp.jmx;

import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;

/**
 * @author kate2007
 */
public class JmxAccessorTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private MBeanServer mBeanServer;
   private IRttpSupportMBean supportBean;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      RttpSupportFactoryBean factoryBean = new RttpSupportFactoryBean();
      factoryBean.setServer(mBeanServer);
      supportBean = factoryBean.getObject();
   }

   @Test
   public void getBeanDefinitionCountWorks() throws MalformedObjectNameException,
            InterruptedException {
      assertThat(supportBean.getBeanDefinitionCount(), greaterThan(0));
   }

   @Test
   public void getStartupDateWorks() throws MalformedObjectNameException, InterruptedException {
      assertThat(supportBean.getStartUpDate().getTime(), lessThan(System.currentTimeMillis()));
   }
}
