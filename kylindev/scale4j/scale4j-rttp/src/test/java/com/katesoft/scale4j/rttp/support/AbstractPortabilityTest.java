package com.katesoft.scale4j.rttp.support;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.persistent.spring.ILocalHibernateSessionBuilder;
import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.H2Dialect;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/**
 * @author kate2007
 */
public abstract class AbstractPortabilityTest extends Perf4jTest {
   @Test
   public void hibernateDialectIsNotH2() {
      ILocalHibernateSessionBuilder bean = (ILocalHibernateSessionBuilder) applicationContext
               .getBean("&" + IBeanNameReferences.SESSION_FACTORY);
      Dialect dialect = Dialect.getDialect(bean.getConfiguration().getProperties());
      assertThat(dialect.getClass().getName(), is(not(H2Dialect.class.getName())));
   }
}
