package com.katesoft.scale4j.rttp.context;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.rttp.jobs.DistributedTaskExecutor;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/simple-rttpContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
@DirtiesContext
public class SimpleContextWithoutJobsTest extends AbstractRTTPTransactionalTest {
   @Autowired
   private DistributedTaskExecutor distributedTaskExecutor;

   @Test
   public void contextCreated() {
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateDaoSupport.getLocalHibernateTemplate().execute(
                     new HibernateCallback<Object>() {
                        @Override
                        public Object doInHibernate(@SuppressWarnings("unused") Session session)
                                 throws HibernateException, SQLException {
                           assertThat(distributedTaskExecutor.getInstance().getLifecycleService()
                                    .isRunning(), is(true));
                           return null;
                        }
                     });
         }
      });
   }
}
