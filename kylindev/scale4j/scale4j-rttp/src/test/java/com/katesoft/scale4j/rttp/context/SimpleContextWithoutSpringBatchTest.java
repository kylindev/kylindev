package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/simple-rttpContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml",
         "/META-INF/spring/rttp-jobs-context.xml" })
@DirtiesContext
public class SimpleContextWithoutSpringBatchTest extends AbstractRTTPTransactionalTest {
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_CLIENT_SCHEDULER)
   private Scheduler scheduler;

   @Test
   public void contextInitialized() throws SchedulerException {
      assertThat(scheduler.getSchedulerName(), is("rttp.client.scheduler"));
   }
}
