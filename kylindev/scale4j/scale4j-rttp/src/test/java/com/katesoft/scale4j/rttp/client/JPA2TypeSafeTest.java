package com.katesoft.scale4j.rttp.client;

import com.katesoft.scale4j.persistent.model.UserDatasourceEntity;
import com.katesoft.scale4j.persistent.model.UserDatasourceEntity_;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;
import org.springframework.orm.jpa.JpaCallback;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class JPA2TypeSafeTest extends AbstractSimpleContextRTTPTest {
   @Test
   public void ableToLoadEntityByUniqueIdentifierAttribute() {
      final UserDatasourceEntity entity = UserDatasourceEntity.from((BasicDataSource) dataSource);
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      UserDatasourceEntity execute = hibernateTemplate
               .execute(new JpaCallback<UserDatasourceEntity>() {
                  @Override
                  public UserDatasourceEntity doInJpa(EntityManager em) throws PersistenceException {
                     CriteriaBuilder cb = em.getCriteriaBuilder();
                     CriteriaQuery<UserDatasourceEntity> query = cb
                              .createQuery(UserDatasourceEntity.class);
                     Root<UserDatasourceEntity> root = query.from(UserDatasourceEntity.class);
                     query.where(
                              cb.equal(root.get(UserDatasourceEntity_.uniqueIdentifier),
                                       entity.getUniqueIdentifier()),
                              cb.equal(root.get(UserDatasourceEntity_.guid),
                                       entity.getGlobalUniqueIdentifier()));
                     return em.createQuery(query).getResultList().iterator().next();
                  }
               });
      assertThat(execute.getUniqueIdentifier(), is(entity.getUniqueIdentifier()));
   }
}
