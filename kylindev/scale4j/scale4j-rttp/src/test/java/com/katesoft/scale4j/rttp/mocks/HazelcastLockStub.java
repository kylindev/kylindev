package com.katesoft.scale4j.rttp.mocks;

import java.util.concurrent.locks.ReentrantLock;

import org.junit.Ignore;

import com.hazelcast.core.ILock;
import com.katesoft.scale4j.common.utils.AssertUtility;

@Ignore
public class HazelcastLockStub extends ReentrantLock implements ILock {
   private static final long serialVersionUID = -3775824008060196552L;

   private final Object lockObject;

   public HazelcastLockStub(Object lockObject) {
      AssertUtility.assertNotNull(lockObject, "[lockObject]");
      this.lockObject = lockObject;
   }

   @Override
   public InstanceType getInstanceType() {
      return InstanceType.LOCK;
   }

   @Override
   public void destroy() {
   }

   @Override
   public Object getId() {
      return lockObject.toString();
   }

   @Override
   public Object getLockObject() {
      return lockObject;
   }
}
