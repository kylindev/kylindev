package com.katesoft.scale4j.rttp.mocks;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

import org.junit.Ignore;

import com.hazelcast.core.HazelcastInstance;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;

/**
 * @author kate2007
 */
@Ignore
public class SpringHazelcastBridgeMock extends SpringHazelcastBridge {
   private final Map<String, AtomicBoolean> booleanMap = new HashMap<String, AtomicBoolean>();
   private HazelcastInstance hazelcastInstance;

   @Override
   public HazelcastInstance getHazelcastInstance() {
      if (hazelcastInstance == null) {
         hazelcastInstance = new HazelcastInstanceStub();
      }
      return hazelcastInstance;
   }

   @Override
   public AtomicBoolean getAtomicBoolean(String name) {
      AtomicBoolean atomicBoolean = new AtomicBoolean(false);
      booleanMap.put(name, atomicBoolean);
      return atomicBoolean;
   }

   @Override
   public void updateAtomicBoolean(String name, boolean newValue) {
      AtomicBoolean atomicBoolean = booleanMap.get(name);
      atomicBoolean.set(newValue);
   }
}
