package com.katesoft.scale4j.rttp.support;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:META-INF/spring/simple-rttpContext.xml",
         "classpath:META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
public class RttpPerf4jTest extends Perf4jTest {
}
