package com.katesoft.scale4j.rttp.jobs;

import java.io.Serializable;

import org.apache.commons.lang.mutable.MutableInt;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

/**
 * @author kate2007
 */
public class SimpleJob implements StatefulJob, Serializable {
   private static final long serialVersionUID = -5935697239739829557L;
   public static final String EXECUTION_COUNT = "execution_count";
   protected transient Logger logger = LogFactory.getLogger(getClass());
   private static MutableInt i = new MutableInt();

   @Override
   public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
      i.increment();
      if (jobExecutionContext.getJobDetail().getJobDataMap().containsKey(EXECUTION_COUNT)) {
         logger.debug("current %s=%s", EXECUTION_COUNT, jobExecutionContext.getJobDetail()
                  .getJobDataMap().getIntValue(EXECUTION_COUNT));
         jobExecutionContext.getJobDetail().getJobDataMap().remove(EXECUTION_COUNT);
      } else {
         i.setValue(0);
      }
      jobExecutionContext.getJobDetail().getJobDataMap().put(EXECUTION_COUNT, i.intValue());
      jobExecutionContext.setResult(i.getValue());
   }
}
