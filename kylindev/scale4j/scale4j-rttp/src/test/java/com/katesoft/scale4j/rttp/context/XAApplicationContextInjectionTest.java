package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.rttp.jmx.RttpSupportService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Constants;
import org.springframework.test.annotation.DirtiesContext;

import java.util.Date;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * @author kate2007
 */
@DirtiesContext
public class XAApplicationContextInjectionTest extends AbstractXAContextRTTPTest {
   @Autowired
   private RttpSupportService rttpSupportService;

   @Test
   public void testAllInjected() {
      logger.debug("datasource = " + dataSource);
      logger.debug("hibernateTemplate = " + hibernateTemplate);
      logger.debug("hibernateDaoSupport = " + hibernateDaoSupport);
      logger.debug("jdbcTemplate = " + jdbcTemplate);
      assertThat(super.dataSource, is(notNullValue()));
      assertThat(super.hibernateTemplate, is(notNullValue()));
      assertThat(super.hibernateDaoSupport, is(notNullValue()));
      assertThat(super.jdbcTemplate, is(notNullValue()));
   }

   @Test
   public void beanReferencesResolved() {
      Constants constants = new Constants(IBeanNameReferences.class);
      Set<Object> constantsValues = constants.getValues("");
      for (Object next : constantsValues) {
         applicationContext.getBean((String) next);
      }
   }

   @Test
   public void startUpDateValid() {
      assertThat(new Date().getTime(), greaterThan(rttpSupportService.getStartUpDate().getTime()));
   }

   @Test
   public void beanDefinitionCountResolved() {
      assertThat(rttpSupportService.getBeanDefinitionCount(), greaterThan(0));
   }
}
