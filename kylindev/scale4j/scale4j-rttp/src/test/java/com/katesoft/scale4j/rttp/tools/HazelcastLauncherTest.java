package com.katesoft.scale4j.rttp.tools;

import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.persistent.model.RevisionDomainEntity;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.rttp.hibernate.ClusteredIncrementGenerator;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class HazelcastLauncherTest {
   HazelcastLauncher hazelcastLauncher;
   ClusteredIncrementGenerator clusteredIncrementGenerator;

   @Before
   public void setUp() {
      Hazelcast.shutdownAll();
      hazelcastLauncher = new HazelcastLauncher();
      clusteredIncrementGenerator = new ClusteredIncrementGenerator();
      SpringHazelcastBridge bridge = new SpringHazelcastBridge();
      bridge.setHazelcastLauncher(hazelcastLauncher);
      clusteredIncrementGenerator.setRttpHazelcastBridge(bridge);
   }

   @After
   public void tearDown() {
      hazelcastLauncher.stop();
      Hazelcast.shutdownAll();
   }

   @Test
   public void hazelcastInstanceStartedWithoutConfigurationFile() {
      HazelcastInstance instance = hazelcastLauncher.loadInstance(null);
      validateInstance(instance);
      TestEntity entity = new TestEntity();
      long id = clusteredIncrementGenerator.generate(entity);
      for (int i = 0; i <= 100000; i++) {
         assertThat((String.format("failed on iteration %s", i)),
                  clusteredIncrementGenerator.generate(entity), greaterThanOrEqualTo(id + i));
      }
   }

   @Test
   public void hazelcastInstanceStartedFromConfigurationFile() {
      hazelcastLauncher.setConfigLocation(new ClassPathResource("hazelcast-test.xml"));
      hazelcastLauncher.loadInstance(null);
      HazelcastInstance instance = hazelcastLauncher.loadInstance(null);
      validateInstance(instance);
      RevisionDomainEntity entity = new RevisionDomainEntity();
      long id = clusteredIncrementGenerator.generate(entity);
      for (int i = 0; i <= 100000; i++) {
         assertThat((String.format("failed on iteration %s", i)),
                  clusteredIncrementGenerator.generate(entity), greaterThanOrEqualTo(id + i));
      }
   }

   @Test
   public void hazelcastInstanceStartedFromCommandLineArg() throws IOException {
      System.setProperty(RuntimeUtility.VAR_HAZELCAST_CONFIGURATION, new ClassPathResource(
               "hazelcast-test.xml").getURL().toExternalForm());
      hazelcastLauncher.loadInstance(null);
      assertThat(hazelcastLauncher.startedFromSystemParam(), is(true));
      long id = clusteredIncrementGenerator.generate(new RevisionDomainEntity());
      clusteredIncrementGenerator.generate(new TestEntity());
      clusteredIncrementGenerator.generate(new TestEntity());
      assertThat(clusteredIncrementGenerator.generate(new RevisionDomainEntity()), is(id + 1));
   }

   private static void validateInstance(HazelcastInstance instance) {
      assertThat(instance, is(notNullValue()));
      assertThat(instance.getExecutorService(), is(notNullValue()));
      assertThat(instance.getCluster().getMembers().size(), is(1));
      assertThat(instance.getCluster().getLocalMember().getInetSocketAddress().getAddress(),
               is(notNullValue()));
      assertThat(instance.getCluster().getLocalMember().getInetSocketAddress().getPort(),
               is(greaterThan(0)));
   }
}
