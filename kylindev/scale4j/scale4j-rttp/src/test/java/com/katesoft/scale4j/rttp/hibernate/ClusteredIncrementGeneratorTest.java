package com.katesoft.scale4j.rttp.hibernate;

import com.hazelcast.core.IMap;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.katesoft.scale4j.rttp.internal.IRttpHazelcastRefs.ID_GENERATOR_MAP;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

/**
 * @author kate2007
 */
public class ClusteredIncrementGeneratorTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private SpringHazelcastBridge bridge;

   @Test
   public void fewRecordsPersisted() throws Exception {
      for (int i = 0; i <= 50; i++) {
         hibernateTemplate.save(TestEntity.newTestInstance());
      }
      hibernateTemplate.flush();
   }

   @Test
   public void eachRecordHasIncrementedIdentifier() {
      Long id = null;
      for (int i = 0; i <= 10; i++) {
         Long id1 = (Long) hibernateTemplate.save(TestEntity.newTestInstance());
         if (id != null) {
            assertThat(id1, greaterThan(id));
         }
         id = id1;
      }
      hibernateTemplate.flush();
   }

   @Test
   public void newIdGeneratedCorrectly() throws InterruptedException {
      TestEntity entity = TestEntity.newTestInstance();
      final IMap<Object, Object> map = bridge.getHazelcastInstance().getMap(ID_GENERATOR_MAP);
      Long old = (Long) map.get(TestEntity.class.getName());
      try {
         map.replace(TestEntity.class.getName(), 198989L);
         Long id = (Long) hibernateTemplate.save(entity);
         hibernateTemplate.flush();
         assertThat(id, greaterThan(198989L));
      } finally {
         map.replace(TestEntity.class.getName(), old);
         hibernateTemplate.delete(entity);
      }
   }
}
