package com.katesoft.scale4j.rttp.hibernate;

import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import com.katesoft.scale4j.rttp.subscription.TestEntityCountingSubscription;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static com.katesoft.scale4j.rttp.internal.IRttpHazelcastRefs.SUBSCRIBER_EXECUTOR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class CloudSubscriberTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   private TestEntityCountingSubscription subscription;

   @Autowired
   private SpringHazelcastBridge bridge;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      subscription.reset();
   }

   @Override
   public void tearDown() throws Exception {
      subscription.reset();
      super.tearDown();
   }

   @Test
   public void insertTriggered() throws Exception {
      TestEntity entity = TestEntity.newTestInstance();
      hibernateDaoSupport.getLocalHibernateTemplate().save(entity);
      hibernateDaoSupport.getLocalHibernateTemplate().flush();
      bridge.getHazelcastInstance().getExecutorService(SUBSCRIBER_EXECUTOR);
      assertThat(subscription.insertCount(), is(1));
   }

   @Test
   public void testOnPostUpdate() throws Exception {
      TestEntity entity = TestEntity.newTestInstance();
      hibernateDaoSupport.getLocalHibernateTemplate().save(entity);
      hibernateDaoSupport.getLocalHibernateTemplate().flush();
      assertThat(subscription.insertCount(), is(1));
      entity.setCrudType(CrudType.UPDATE);
      hibernateDaoSupport.getLocalHibernateTemplate().update(entity);
      hibernateDaoSupport.getLocalHibernateTemplate().flush();
      assertThat(subscription.insertCount(), is(1));
      assertThat(subscription.updateCount(), is(1));
   }

   @Test
   public void testOnPostDelete() throws Exception {
      TestEntity entity = TestEntity.newTestInstance();
      hibernateDaoSupport.getLocalHibernateTemplate().save(entity);
      hibernateDaoSupport.getLocalHibernateTemplate().flush();
      assertThat(subscription.insertCount(), is(1));
      entity.setCrudType(CrudType.DELETE);
      hibernateDaoSupport.getLocalHibernateTemplate().delete(entity);
      hibernateDaoSupport.getLocalHibernateTemplate().flush();
      assertThat(subscription.insertCount(), is(1));
      assertThat(subscription.deleteCount(), is(1));
   }
}
