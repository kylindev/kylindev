package com.katesoft.scale4j.rttp.jobs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobInstance;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;

/**
 * @author kate2007
 */
public abstract class AbstractComplexJobLauncherTest extends AbstractPersistentTest {
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_CLIENT_SCHEDULER)
   Scheduler scheduler;
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_BATCH_JOB_EXPLORER)
   JobExplorer jobExplorer;
   protected boolean validateExecutionCount = true;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      CounterPerJobTasklet.reset();
   }

   @Test
   public void allJobsExecutedFewTimes() throws InterruptedException, SchedulerException {
      final long start = System.currentTimeMillis();
      Thread.sleep(10000);
      while (!scheduler.getCurrentlyExecutingJobs().isEmpty()) {
         Thread.sleep(1000);
      }
      assertThat(CounterPerJobTasklet.getJobExecutionTimes("job4"), greaterThan(0));
      assertThat(CounterPerJobTasklet.getJobExecutionTimes("job3"), greaterThan(0));
      assertThat(CounterPerJobTasklet.getJobExecutionTimes("job2"), greaterThan(0));
      assertThat(CounterPerJobTasklet.getJobExecutionTimes("job1"), greaterThan(0));
      while (!scheduler.getCurrentlyExecutingJobs().isEmpty()) {
         Thread.sleep(1000);
      }
      //
      new TransactionTemplate(dataSourceTransactionManager)
               .execute(new TransactionCallbackWithoutResult() {
                  @Override
                  protected void doInTransactionWithoutResult(
                           @SuppressWarnings("unused") TransactionStatus status) {
                     long now = System.currentTimeMillis();
                     List<JobInstance> job1Instances = jobExplorer.getJobInstances("job1", 0,
                              Integer.MAX_VALUE / 2);
                     List<JobInstance> job2Instances = jobExplorer.getJobInstances("job2", 0,
                              Integer.MAX_VALUE / 2);
                     List<JobInstance> job3Instances = jobExplorer.getJobInstances("job3", 0,
                              Integer.MAX_VALUE / 2);
                     List<JobInstance> job4Instances = jobExplorer.getJobInstances("job4", 0,
                              Integer.MAX_VALUE / 2);
                     logger.info("fetched job executions in %s milisecs",
                              (System.currentTimeMillis() - now));
                     //
                     assertNoFails(job1Instances, start, "job1");
                     assertNoFails(job2Instances, start, "job2");
                     assertNoFails(job3Instances, start, "job3");
                     assertNoFails(job4Instances, start, "job4");
                     //
                     if (validateExecutionCount) {
                        assertThat(job1Instances.size(),
                                 is(CounterPerJobTasklet.getJobExecutionTimes("job1")));
                        assertThat(job2Instances.size(),
                                 is(CounterPerJobTasklet.getJobExecutionTimes("job2")));
                        assertThat(job3Instances.size(),
                                 is(CounterPerJobTasklet.getJobExecutionTimes("job3")));
                        assertThat(job4Instances.size(),
                                 is(CounterPerJobTasklet.getJobExecutionTimes("job4")));
                     }
                  }
               });
   }

   private void assertNoFails(List<JobInstance> jobInstances, long start, String job) {
      long now = System.currentTimeMillis();
      for (JobInstance jobInstance : jobInstances) {
         List<JobExecution> jobExecutions = jobExplorer.getJobExecutions(jobInstance);
         for (JobExecution jobExecution : jobExecutions) {
            logger.debug("%s %s =[status=%s, exit_status=%s, end_time=%s]", jobInstance,
                     jobExecution, jobExecution.getStatus(), jobExecution.getExitStatus(),
                     jobExecution.getEndTime());
            if (jobExecution.getStartTime() != null
                     && jobExecution.getStartTime().getTime() >= start) {
               assertThat(jobExecution.getEndTime(), is(Matchers.<Object> notNullValue()));
               assertThat(jobExecution.getStatus(), is(BatchStatus.COMPLETED));
               assertThat(jobExecution.getExitStatus(), is(ExitStatus.COMPLETED));
            }
         }
      }
      logger.info("checked no fails for job %s in %s", job, (System.currentTimeMillis() - now));
   }
}
