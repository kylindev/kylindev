package com.katesoft.scale4j.rttp.messaging.jdbc.init;

import com.katesoft.scale4j.rttp.jobs.jdbc.init.RttpQuartzSchemaCreator;
import org.hibernate.dialect.DB2Dialect;
import org.hibernate.dialect.DerbyDialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgresPlusDialect;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.dialect.SybaseASE15Dialect;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class RttpMessageStoreSchemaCreatorTest {
   @Test
   public void oracleDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(Oracle10gDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_ORACLE),
               is(true));
   }

   @Test
   public void mysql5InnodbDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(MySQL5InnoDBDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_MYSQL),
               is(true));
   }

   @Test
   public void mysqlDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(MySQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_MYSQL),
               is(true));
   }

   @Test
   public void postgresDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(PostgresPlusDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_POSTGRESQL),
               is(true));
   }

   @Test
   public void sqlServerDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(SQLServer2008Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_SQL_SERVER),
               is(true));
   }

   @Test
   public void hsqldbDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(HSQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_HSQLDB),
               is(true));
   }

   @Test
   public void sybaseDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(SybaseASE15Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_SYBASE),
               is(true));
   }

   @Test
   public void derbyDetected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(DerbyDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_DERBY),
               is(true));
   }

   @Test
   public void db2Detected() throws Exception {
      RttpMessageStoreSchemaCreator populator = new RttpMessageStoreSchemaCreator();
      populator.setDialect(DB2Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getSpringIntegrationCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_SPRING_INTEGRATION_DB2), is(true));
   }
}
