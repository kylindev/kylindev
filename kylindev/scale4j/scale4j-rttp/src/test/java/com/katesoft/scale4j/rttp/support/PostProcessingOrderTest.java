package com.katesoft.scale4j.rttp.support;

import com.katesoft.scale4j.persistent.spring.postprocessor.HibernateSessionExtensionPostProcessor;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import com.katesoft.scale4j.rttp.spring.postprocessor.RttpHibernatePostProcessor;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

/**
 * @author kate2007
 */
public class PostProcessingOrderTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   @Qualifier("scale4j.rttp.properties.configurer")
   private PropertyPlaceholderConfigurer rttpPropertyPlaceholderConfigurer;
   @Autowired
   @Qualifier("scale4j.persistent.properties.configurer")
   private PropertyPlaceholderConfigurer persistentPropertyPlaceholderConfigurer;
   @Autowired
   @Qualifier("rttp.hibernate.session.processor")
   private RttpHibernatePostProcessor rttpHibernatePostProcessor;
   @Autowired
   @Qualifier("junit.test.entity.post.processor")
   private HibernateSessionExtensionPostProcessor hibernateSessionExtensionPostProcessor;

   @Test
   public void orderIsValid() {
      assertThat(persistentPropertyPlaceholderConfigurer.getOrder(),
               greaterThan(rttpPropertyPlaceholderConfigurer.getOrder()));
      assertThat(rttpHibernatePostProcessor.getOrder(),
               greaterThan(rttpPropertyPlaceholderConfigurer.getOrder()));
      assertThat(rttpHibernatePostProcessor.getOrder(),
               greaterThan(persistentPropertyPlaceholderConfigurer.getOrder()));
      assertThat(hibernateSessionExtensionPostProcessor.getOrder(),
               greaterThan(rttpPropertyPlaceholderConfigurer.getOrder()));
      assertThat(hibernateSessionExtensionPostProcessor.getOrder(),
               greaterThan(persistentPropertyPlaceholderConfigurer.getOrder()));
      assertThat(rttpHibernatePostProcessor.getOrder(),
               greaterThan(hibernateSessionExtensionPostProcessor.getOrder()));
   }
}
