package com.katesoft.scale4j.rttp.support;

import com.katesoft.scale4j.persistent.client.AbstractLocalHibernateDaoSupportTest;
import com.katesoft.scale4j.persistent.model.InternalDomainEntity;
import com.katesoft.scale4j.persistent.model.RevisionDomainEntity;
import com.katesoft.scale4j.persistent.model.TestEntity;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.envers.AuditReaderFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.jpa.JpaCallback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import java.sql.SQLException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:META-INF/spring/simple-rttpContext.xml",
         "classpath:META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
public class RTTPHibernateSupportTest extends AbstractLocalHibernateDaoSupportTest {
   @Test
   public void internalDomainEntitiesAreNotAuditable() {
      hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            assertThat(
                     AuditReaderFactory.get(session).isEntityClassAudited(
                              InternalDomainEntity.class), is(false));
            assertThat(
                     AuditReaderFactory.get(session).isEntityClassAudited(
                              RevisionDomainEntity.class), is(false));
            return null;
         }
      });
   }

   @Test
   public void profiledMethodDoesNotCauseExceptions() {
      hibernateTemplate.loadAll(TestEntity.class, true);
      hibernateTemplate.deleteAll(TestEntity.class);
   }

   @Test
   @Transactional
   public void executeJPAMethodsDoesNotCauseErrors() {
      final TestEntity entity = TestEntity.newTestInstance();
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      hibernateTemplate.find(TestEntity.class, entity.getUniqueIdentifier());
      hibernateTemplate.clear();
      hibernateTemplate.execute(new JpaCallback<Object>() {
         @Override
         public Object doInJpa(EntityManager em) throws PersistenceException {
            em.getReference(TestEntity.class, entity.getUniqueIdentifier()).getUniqueIdentifier();
            return null;
         }
      });
      hibernateTemplate.clear();
      hibernateTemplate.remove(hibernateDaoSupport.getLocalHibernateTemplate().find(
               TestEntity.class, entity.getUniqueIdentifier()));
   }
}
