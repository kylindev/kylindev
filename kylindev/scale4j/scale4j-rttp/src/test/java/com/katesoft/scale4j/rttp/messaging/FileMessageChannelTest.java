package com.katesoft.scale4j.rttp.messaging;

import static com.katesoft.scale4j.common.io.FileUtility.SEPARATOR;
import static com.katesoft.scale4j.common.io.FileUtility.TMP_DIR;
import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;
import java.util.Random;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.integration.Message;
import org.springframework.integration.channel.QueueChannel;

import com.katesoft.scale4j.common.io.FileUtility;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;

/**
 * @author kate2007
 */
public class FileMessageChannelTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   @Qualifier(value = "rttp.junit.file1")
   private QueueChannel channel;
   File tempF;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      tempF = new File(TMP_DIR + SEPARATOR + "junit" + SEPARATOR + "input" + SEPARATOR
               + format("%s.txt", new Random().nextInt(1000)));
      FileUtils.touch(tempF);
      channel.clear();
      BufferedWriter out = new BufferedWriter(new FileWriter(tempF));
      out.write(format("line1=line1%s", FileUtility.LINE_SEPARATOR));
      out.write(format("line2=line2%s", FileUtility.LINE_SEPARATOR));
      out.flush();
      out.close();
      logger.info("file %s created for polling with content %s", tempF.getCanonicalPath(),
               IOUtils.readLines(new FileInputStream(tempF)));
   }

   @Override
   public void tearDown() throws Exception {
      super.tearDown();
      if (tempF.exists()) {
         tempF.delete();
      }
   }

   @Test
   public void filePolled() throws IOException {
      Message<?> message = channel.receive(150);
      File content = (File) message.getPayload();
      Properties properties = new Properties();
      properties.load(new FileInputStream(content));
      assertThat(properties.getProperty("line1"), is("line1"));
      assertThat(properties.getProperty("line2"), is("line2"));
   }
}
