package com.katesoft.scale4j.rttp.messaging.lock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import org.junit.Before;
import org.junit.Test;
import org.springframework.integration.Message;
import org.springframework.integration.support.MessageBuilder;

public class AbstractInboundLockHeaderEnricherTest {
   private AbstractInboundLockHeaderEnricher lockHeaderEnricher;

   @Before
   public void setUp() {
      lockHeaderEnricher = new AbstractInboundLockHeaderEnricher() {

         @Override
         protected String deriveLockId(Message<?> message) {
            return message.getPayload().toString();
         }
      };
   }

   @SuppressWarnings("unchecked")
   @Test
   public void headerAdded() throws Exception {
      Message<String> message = MessageBuilder.withPayload("test1").build();
      message = (Message<String>) lockHeaderEnricher.doTransform(message);

      assertThat((String) message.getHeaders()
               .get(AbstractInboundLockHeaderEnricher.LOCK_ID_HEADER), is("test1"));
   }

   @SuppressWarnings("unchecked")
   @Test
   public void headerIsNotOverwritten() throws Exception {
      Message<String> message = MessageBuilder.withPayload("test1")
               .setHeaderIfAbsent(AbstractInboundLockHeaderEnricher.LOCK_ID_HEADER, "test2")
               .build();
      message = (Message<String>) lockHeaderEnricher.doTransform(message);

      assertThat((String) message.getHeaders()
               .get(AbstractInboundLockHeaderEnricher.LOCK_ID_HEADER), is("test2"));
   }
}
