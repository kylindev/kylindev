package com.katesoft.scale4j.rttp.jobs;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Date;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class JobsExecutionTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_CLIENT_JOBS_EXECUTOR)
   private DistributedTaskExecutor distributedTaskExecutor;
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_CLIENT_SCHEDULER)
   private Scheduler scheduler;

   @Test
   public void clusterInstanceAwarePostProcessingWorks() {
      assertThat(distributedTaskExecutor.getInstance(), is(Matchers.<Object> notNullValue()));
   }

   @Test
   public void simpleJobSubmittedAndExecuted() throws SchedulerException, InterruptedException {
      JobDetail jobDetail = new JobDetail("job1", // job name
               "group1", // job group (you can also specify 'null' to use the default group)
               SimpleJob.class); // the java class to execute
      jobDetail.setVolatility(false);
      Trigger trigger = TriggerUtils.makeImmediateTrigger(2, 10);
      trigger.setStartTime(new Date());
      trigger.setName("myTrigger");
      scheduler.scheduleJob(jobDetail, trigger);
      Thread.sleep(50);
      logger.debug("job_details_map = %s", jobDetail.getJobDataMap().getWrappedMap().values()
               .toString());
      logger.debug("trigger_map = %s", trigger.getJobDataMap().getWrappedMap().values().toString());
   }
}
