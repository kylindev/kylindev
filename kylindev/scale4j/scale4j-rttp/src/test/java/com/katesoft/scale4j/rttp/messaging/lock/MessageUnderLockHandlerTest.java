package com.katesoft.scale4j.rttp.messaging.lock;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.springframework.integration.Message;
import org.springframework.integration.MessagingException;
import org.springframework.integration.core.MessageHandler;
import org.springframework.integration.support.MessageBuilder;

import com.katesoft.scale4j.rttp.mocks.HazelcastInstanceStub;

public class MessageUnderLockHandlerTest {
   private MessageUnderLockHandler handler1, handler2;

   @Before
   public void setUp() {
      handler1 = new MessageUnderLockHandler();
      handler2 = new MessageUnderLockHandler();

      HazelcastInstanceStub stub = new HazelcastInstanceStub();

      handler1.setClusterInstance(stub);
      handler2.setClusterInstance(stub);
   }

   @Test(expected = IllegalArgumentException.class)
   public void lockHeaderMustBeProvided() {
      Message<String> message = MessageBuilder.withPayload("test").build();
      handler1.handleMessage(message);
   }

   @Test
   public void messageHandlingDelegated() {
      MessageHandler actualHandler = mock(MessageHandler.class);
      Message<String> message = MessageBuilder
               .withPayload("test")
               .setHeader(AbstractInboundLockHeaderEnricher.LOCK_ID_HEADER,
                        actualHandler.toString()).build();
      handler2.setActualHandler(actualHandler);
      handler2.handleMessage(message);
      verify(actualHandler).handleMessage(message);
   }

   @Test
   public void anotherThreadIsNotAbleToProcessSameMessage() throws InterruptedException {
      final Map<String, Boolean> m = new HashMap<String, Boolean>();
      MessageHandler actualHandler = new MessageHandler() {
         @Override
         public void handleMessage(@SuppressWarnings("unused") Message<?> message)
                  throws MessagingException {
            try {
               String name = Thread.currentThread().getName();
               m.put(name, true);
               Thread.sleep(50);
            } catch (InterruptedException e) {
               throw new RuntimeException(e);
            }
         }
      };
      final Message<String> message = MessageBuilder.withPayload("test")
               .setHeader(AbstractInboundLockHeaderEnricher.LOCK_ID_HEADER, 1).build();
      handler1.setActualHandler(actualHandler);
      handler2.setActualHandler(actualHandler);
      Thread thread1 = new Thread(new Runnable() {
         @Override
         public void run() {
            handler1.handleMessage(message);
         }
      });
      thread1.setName("thread1");
      thread1.setDaemon(true);
      thread1.start();

      Thread.sleep(10);
      new Thread(new Runnable() {
         @Override
         public void run() {
            handler2.handleMessage(message);
         }
      }).start();
      assertThat(m.get("thread1"), is(true));
      assertThat(m.get("thread2"), is(nullValue()));
   }
}
