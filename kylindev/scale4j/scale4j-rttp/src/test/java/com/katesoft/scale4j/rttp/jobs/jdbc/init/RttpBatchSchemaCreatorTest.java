package com.katesoft.scale4j.rttp.jobs.jdbc.init;

import org.hibernate.dialect.DB2Dialect;
import org.hibernate.dialect.DerbyDialect;
import org.hibernate.dialect.HSQLDialect;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.hibernate.dialect.MySQLDialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.PostgresPlusDialect;
import org.hibernate.dialect.SQLServer2008Dialect;
import org.hibernate.dialect.SybaseASE15Dialect;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class RttpBatchSchemaCreatorTest {
   @Test
   public void oracleDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(Oracle10gDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_ORACLE), is(true));
   }

   @Test
   public void mysql5InnodbDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(MySQL5InnoDBDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_MYSQL), is(true));
   }

   @Test
   public void mysqlDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(MySQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_MYSQL), is(true));
   }

   @Test
   public void postgresDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(PostgresPlusDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_POSTGRESQL), is(true));
   }

   @Test
   public void sqlServerDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(SQLServer2008Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_SQL_SERVER), is(true));
   }

   @Test
   public void hsqldbDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(HSQLDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_HSQLDB), is(true));
   }

   @Test
   public void sybaseDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(SybaseASE15Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_SYBASE), is(true));
   }

   @Test
   public void derbyDetected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(DerbyDialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_DERBY), is(true));
   }

   @Test
   public void db2Detected() throws Exception {
      RttpBatchSchemaCreator populator = new RttpBatchSchemaCreator();
      populator.setDialect(DB2Dialect.class);
      populator.afterPropertiesSet();
      assertThat(
               populator.getBatchCreationScript().getURL().toExternalForm()
                        .endsWith(RttpQuartzSchemaCreator.TABLES_BATCH_DB2), is(true));
   }
}
