package com.katesoft.scale4j.rttp.jobs;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.rttp.context.AbstractSimpleContextRTTPTest;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import com.katesoft.scale4j.rttp.jobs.tasklets.CacheCleanTasklet;

/**
 * @author kate2007
 */
@SuppressWarnings({ "unchecked" })
@Transactional
public class SpringBatchJobLauncherTest extends AbstractSimpleContextRTTPTest {
   @Autowired
   @Qualifier(value = IBeanNameReferences.RTTP_CLIENT_SCHEDULER)
   private Scheduler scheduler;
   @Autowired
   private SpringHazelcastBridge bridge;
   private Trigger trigger;
   private RttpJobDetailBean jobDetailBean;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      //
      trigger = TriggerUtils.makeImmediateTrigger(2, 250);
      String uuid = UUID.randomUUID().toString();
      //
      trigger.setName(String.format("rttp-junit-trigger-%s", uuid));
      jobDetailBean = new RttpJobDetailBean();
      jobDetailBean.setJobClass(SpringBatchJobLauncher.class);
      jobDetailBean.setGroup(String.format("rttp-junit-batch-%s", uuid));
      jobDetailBean.setName(String.format("rttp-junit-quartz-%s", uuid));
      //
      CountingTasklet.reset();
   }

   @Override
   public void tearDown() throws Exception {
      CountingTasklet.reset();
      super.tearDown();
   }

   @SuppressWarnings({ "rawtypes", "serial" })
   @Test
   public void launchingOfSingleJobWorks() throws SchedulerException, InterruptedException {
      jobDetailBean.setJobDataAsMap(new HashMap() {
         {
            put(JobsUtility.JOB_NAME, "junit-job1");
            put(SpringBatchJobLauncher.JOB_PARAMETERS_INCREMENTER,
                     "distributed.job.parameters.incrementer");
         }
      });
      jobDetailBean.afterPropertiesSet();
      scheduler.scheduleJob(jobDetailBean, trigger);
   }

   @SuppressWarnings({ "rawtypes", "serial" })
   @Test
   public void launchingWithoutProvidedJobParametersIncrementerWorks() throws SchedulerException,
            InterruptedException {
      jobDetailBean.setJobDataAsMap(new HashMap() {
         {
            put(JobsUtility.JOB_NAME, "junit-job2");
         }
      });
      jobDetailBean.afterPropertiesSet();
      scheduler.scheduleJob(jobDetailBean, trigger);
   }

   @SuppressWarnings({ "rawtypes", "serial" })
   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void cacheClearTaskletWorks() throws SchedulerException, InterruptedException {
      bridge.getHazelcastInstance().getMap(TestEntity.class.getName()).clear();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            TestEntity testEntity1 = TestEntity.newTestInstance();
            TestEntity testEntity2 = TestEntity.newTestInstance();
            TestEntity testEntity3 = TestEntity.newTestInstance();
            TestEntity testEntity4 = TestEntity.newTestInstance();

            session.save(testEntity1);
            session.save(testEntity2);
            session.save(testEntity3);
            session.save(testEntity4);

            session.flush();
            session.clear();

            session.get(TestEntity.class, testEntity1.getUniqueIdentifier());
            session.get(TestEntity.class, testEntity2.getUniqueIdentifier());
            session.get(TestEntity.class, testEntity3.getUniqueIdentifier());
            session.get(TestEntity.class, testEntity4.getUniqueIdentifier());

            return null;
         }
      });
      jobDetailBean.setJobDataAsMap(new HashMap() {
         {
            put(JobsUtility.JOB_NAME, "junit-job3");
            put(CacheCleanTasklet.ENTITY_CLASS, TestEntity.class.getName());
         }
      });
      jobDetailBean.afterPropertiesSet();
      scheduler.scheduleJob(jobDetailBean, trigger);
      Thread.sleep(1000);
      assertThat(bridge.getHazelcastInstance().getMap(TestEntity.class.getName()).size(), is(4));
   }
}
