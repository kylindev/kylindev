package com.katesoft.scale4j.rttp.context;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/simple-rttpContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml",
         "/META-INF/spring/rttp-jobs-context.xml", "/META-INF/spring/rttp-batch-context.xml",
         "/META-INF/spring/rttp-integration-context.xml",
         "/META-INF/spring/jvmcluster-batch-junit.xml", "/META-INF/spring/jvmcluster-si-junit.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
@DirtiesContext
public abstract class AbstractSimpleContextRTTPTest extends AbstractRTTPTransactionalTest {
   @Autowired
   @Qualifier(IBeanNameReferences.TRANSACTION_MANAGER)
   protected PlatformTransactionManager hibernateTransactionManager;
   @Autowired
   @Qualifier(IBeanNameReferences.RTTP_SPRING_HAZELCAST_BRIDGE)
   protected SpringHazelcastBridge springHazelcastBridge;
}
