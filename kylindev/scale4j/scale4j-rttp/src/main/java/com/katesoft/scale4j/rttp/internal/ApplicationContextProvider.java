package com.katesoft.scale4j.rttp.internal;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * @author kate2007
 */
public class ApplicationContextProvider implements ApplicationContextAware {
   public void setApplicationContext(ApplicationContext ctx) throws BeansException {
      AppContext.setApplicationContext(ctx);
   }
}