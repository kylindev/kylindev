package com.katesoft.scale4j.rttp.jobs;

import org.quartz.SchedulerException;

import static com.katesoft.scale4j.common.lang.RuntimeUtility.VAR_SERVICE_ID;
import static com.katesoft.scale4j.common.utils.StringUtility.isEmpty;
import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;

/**
 * internal class, used for scheduler instance name generation
 * 
 * @author kate2007
 */
public class RttpSchedulerInstanceIdGenerator extends org.quartz.simpl.SimpleInstanceIdGenerator {
   @Override
   public String generateInstanceId() throws SchedulerException {
      if (isEmpty(System.getProperty(VAR_SERVICE_ID))) {
         return super.generateInstanceId();
      }
      return format("%s_scheduler_%s", System.getProperty(VAR_SERVICE_ID), currentTimeMillis());
   }
}
