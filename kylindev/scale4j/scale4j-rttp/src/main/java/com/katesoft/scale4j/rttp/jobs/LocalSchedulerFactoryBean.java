package com.katesoft.scale4j.rttp.jobs;

import com.katesoft.scale4j.common.lang.RuntimeUtility;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Properties;

import static com.katesoft.scale4j.rttp.jmx.IRttpSupportMBean.OBJ_RTTP_QUARTZ_MANAGER;
import static java.lang.Boolean.valueOf;

/**
 * @author kate2007
 */
public class LocalSchedulerFactoryBean extends SchedulerFactoryBean {
   private String jmxExportName;

   @Override
   public void setQuartzProperties(Properties quartzProperties) {
      if (quartzProperties.containsKey("org.quartz.scheduler.jmx.export")
               && valueOf((String) quartzProperties.get("org.quartz.scheduler.jmx.export"))) {
         if (!quartzProperties.containsKey("org.quartz.scheduler.jmx.objectName")) {
            jmxExportName = OBJ_RTTP_QUARTZ_MANAGER + RuntimeUtility.getJmxNameExtension();
            quartzProperties.put("org.quartz.scheduler.jmx.objectName", jmxExportName);
         }
      }
      super.setQuartzProperties(quartzProperties);
   }

   /**
    * @return jmx object name that was used for quartz scheduler registration.
    */
   public String getJmxExportName() {
      return jmxExportName;
   }
}
