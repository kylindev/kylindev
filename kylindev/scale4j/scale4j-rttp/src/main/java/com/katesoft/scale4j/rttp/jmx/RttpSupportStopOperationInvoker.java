package com.katesoft.scale4j.rttp.jmx;

import com.katesoft.scale4j.common.jmx.JmxOperationInvoker;

/**
 * This class allows you to stop running service.
 * 
 * @author kate2007
 */
public class RttpSupportStopOperationInvoker extends JmxOperationInvoker<IRttpSupportMBean> {
   public RttpSupportStopOperationInvoker(String url) {
      super(url, IRttpSupportMBean.OBJ_RTTP_SUPPORT, IRttpSupportMBean.class);
   }

   @Override
   protected void doInvoke(IRttpSupportMBean proxy) {
      proxy.stopService();
   }
}
