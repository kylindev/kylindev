package com.katesoft.scale4j.rttp.jmx;

import java.net.MalformedURLException;

import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.jmx.access.MBeanProxyFactoryBean;

/**
 * @author kate2007
 */
public class RttpSupportFactoryBean implements FactoryBean<IRttpSupportMBean> {
   private IRttpSupportMBean proxy;
   private MBeanServer server;
   private String url;

   @Override
   public IRttpSupportMBean getObject() throws MalformedURLException, MalformedObjectNameException {
      if (proxy == null) {
         MBeanProxyFactoryBean factoryBean = new MBeanProxyFactoryBean();
         factoryBean.setManagementInterface(IRttpSupportMBean.class);
         factoryBean.setProxyInterface(IRttpSupportMBean.class);
         factoryBean.setObjectName(IRttpSupportMBean.OBJ_RTTP_SUPPORT);
         if (server != null) {
            factoryBean.setServer(server);
         }
         if (url != null) {
            factoryBean.setServiceUrl(url);
         }
         factoryBean.setRefreshOnConnectFailure(false);
         factoryBean.afterPropertiesSet();
         proxy = (IRttpSupportMBean) factoryBean.getObject();
      }
      return proxy;
   }

   @Override
   public Class<?> getObjectType() {
      return IRttpSupportMBean.class;
   }

   @Override
   public boolean isSingleton() {
      return true;
   }

   public void setServer(MBeanServer server) {
      this.server = server;
   }

   public void setUrl(String url) {
      this.url = url;
   }
}
