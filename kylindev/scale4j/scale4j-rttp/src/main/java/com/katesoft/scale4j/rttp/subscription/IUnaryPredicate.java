package com.katesoft.scale4j.rttp.subscription;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

/**
 * Unary predicate allows to evaluate if clients are interested for some event, related to business
 * entity.
 * 
 * @author kate2007
 */
public interface IUnaryPredicate<T extends AbstractPersistentEntity> {
   /**
    * @param entity
    *           domain entity
    * @return true if clients are interested for give entity.
    */
   boolean execute(T entity);
}
