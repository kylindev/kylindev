package com.katesoft.scale4j.rttp.jobs;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import net.jcip.annotations.ThreadSafe;

import org.springframework.core.task.TaskExecutor;

import com.katesoft.scale4j.rttp.client.IClusterInstanceAware.ClusterInstanceAwareBean;

/**
 * this class is responsible for executing distributed task in cluster env.
 * 
 * @author kate2007
 */
@ThreadSafe
public class DistributedTaskExecutor extends ClusterInstanceAwareBean implements TaskExecutor {

   @Override
   public void execute(Runnable task) {
      ExecutorService executorService = getInstance().getExecutorService();
      logger.debug("submitting distributed task %s to distributed task executor", task.toString());
      executorService.execute(task);
   }

   public Future<?> execute(Callable<?> callable) {
      ExecutorService executorService = getInstance().getExecutorService();
      logger.debug("submitting distributed task %s to distributed task executor",
               callable.toString());
      return executorService.submit(callable);
   }
}
