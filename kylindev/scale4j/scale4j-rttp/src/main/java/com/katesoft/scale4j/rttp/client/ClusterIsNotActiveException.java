package com.katesoft.scale4j.rttp.client;

import static com.katesoft.scale4j.common.utils.AssertUtility.assertTrue;
import static java.lang.String.format;

import com.hazelcast.core.HazelcastInstance;

/**
 * This exception is thrown when hazelcast instance is not active.
 * 
 * @author kate2007
 */
public class ClusterIsNotActiveException extends RuntimeException {
   private static final long serialVersionUID = 5631879974185778474L;

   public ClusterIsNotActiveException(HazelcastInstance hazelcastInstance) {
      super((format("hazelcast instance=%s is no longer active", hazelcastInstance)));
      assertTrue(!hazelcastInstance.getLifecycleService().isRunning(),
               format("instance=%s is active", hazelcastInstance));
   }
}
