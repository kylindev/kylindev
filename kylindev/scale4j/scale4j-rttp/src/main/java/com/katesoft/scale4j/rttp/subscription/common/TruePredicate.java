package com.katesoft.scale4j.rttp.subscription.common;

import java.io.Serializable;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;
import com.katesoft.scale4j.rttp.subscription.IUnaryPredicate;

/**
 * predicate that always return true(can be used for general purposes such as debug)
 * 
 * @author kate2007
 */
public class TruePredicate<T extends AbstractPersistentEntity> implements IUnaryPredicate<T>,
         Serializable {
   private static final long serialVersionUID = 181940155477743499L;

   @Override
   public boolean execute(@SuppressWarnings("unused") T entity) {
      return true;
   }
}
