package com.katesoft.scale4j.rttp.client;

import com.hazelcast.core.HazelcastInstance;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

/**
 * Clients may want to use Hazelcast directly.
 * <p/>
 * In this case they would need to implement this interface and jvmcluster-rttp post-processor will
 * inject reference to hazelcast instance.
 * <p/>
 * Client may find useful ClusterInstanceAwareBean class for extending instead of implementing this
 * interface.
 * 
 * @author kate2007
 */
public interface IClusterInstanceAware {
   void setClusterInstance(HazelcastInstance instance);

   /**
    * simple hazelcast instance holder.
    */
   class ClusterInstanceAwareBean implements IClusterInstanceAware {
      protected final Logger logger = LogFactory.getLogger(getClass());
      private HazelcastInstance instance;

      @Override
      public void setClusterInstance(HazelcastInstance instance) {
         this.instance = instance;
      }

      public HazelcastInstance getInstance() {
         return instance;
      }
   }
}
