package com.katesoft.scale4j.rttp.subscription;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

/**
 * Business process class should be used with {@link IUnaryPredicate}.
 * <p/>
 * If clients are interested in some additional logic that is related to domain entity handling,
 * they can implement this interface and subscribe.
 * <p/>
 * Note: this interface is not designed to be used inside transactions. It should be used in
 * resource less env.
 * 
 * @author kate2007
 */
public interface IBusinessProcess<T extends AbstractPersistentEntity> {
   /**
    * handle domain entity event.
    * 
    * @param event
    *           create, update, delete event
    */
   void process(SubscriptionEvent<T> event);
}
