package com.katesoft.scale4j.rttp.internal;

import com.katesoft.scale4j.common.model.IPrettyPrintableObject;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;

/**
 * holder of SpringHazelcastBridge reference.
 * 
 * @author kate2007
 */
public class RttpHazelcastBridgeAwareImpl implements IRttpHazelcastBridgeAware,
         IPrettyPrintableObject {
   protected Logger logger = LogFactory.getLogger(getClass());
   protected SpringHazelcastBridge bridge;

   @Override
   public void setRttpHazelcastBridge(SpringHazelcastBridge springHazelcastBridge) {
      this.bridge = springHazelcastBridge;
   }

   public SpringHazelcastBridge getBridge() {
      return bridge;
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.reflectionToString(this);
   }
}
