package com.katesoft.scale4j.rttp.subscription;

import com.katesoft.scale4j.common.annotation.ValueObject;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

import java.io.Serializable;

/**
 * this class is holder for domain entity and type of event(create, update, delete).
 * 
 * @author kate2007
 */
@ValueObject
public class SubscriptionEvent<T extends AbstractPersistentEntity> implements Serializable {
   private static final long serialVersionUID = -8302344907340733805L;

   private CrudType type;
   private T entity;

   public SubscriptionEvent(CrudType type, T entity) {
      this.type = type;
      this.entity = entity;
   }

   public CrudType getType() {
      return type;
   }

   public T getEntity() {
      return entity;
   }
}
