package com.katesoft.scale4j.rttp.internal;

import org.springframework.context.ApplicationContext;

/**
 * This class will hold static reference to spring application context.
 * <p/>
 * This is bad pattern, but it is necessary for building distributed applications, as application
 * context is not serializable class and more over it is does not make any sense to transfer context
 * through network.
 * <p/>
 * This is internal class, please do not use this class directly.
 * 
 * @author kate2007
 */
public final class AppContext {
   private static ApplicationContext ctx;

   /**
    * Injected from the class "ApplicationContextProvider" which is automatically loaded during
    * Spring-Initialization.
    * 
    * @param applicationContext
    *           actual context
    */
   public static void setApplicationContext(ApplicationContext applicationContext) {
      ctx = applicationContext;
   }

   /**
    * Get access to the Spring ApplicationContext from everywhere in your Application.
    * 
    * @return application context
    */
   public static ApplicationContext getApplicationContext() {
      return ctx;
   }

   private AppContext() {
   }
}
