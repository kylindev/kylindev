package com.katesoft.scale4j.rttp.internal;

import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;

/**
 * implementing this interface means that class depends on spring-hazelcast-bridge bean.
 * <p/>
 * This class created for internal usage.
 * 
 * @author kate2007
 */
public interface IRttpHazelcastBridgeAware {
   void setRttpHazelcastBridge(SpringHazelcastBridge springHazelcastBridge);
}
