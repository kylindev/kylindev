package com.katesoft.scale4j.rttp.jobs.jdbc.init;

import static com.katesoft.scale4j.rttp.internal.IRttpHazelcastRefs.BATCH_DB_CREATION_LOCK;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedHashSet;

import net.jcip.annotations.ThreadSafe;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.perf4j.StopWatch;
import org.springframework.batch.core.repository.dao.AbstractJdbcBatchMetadataDao;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ScriptStatementFailedException;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;

/**
 * This class will create spring batch schema during startup if necessary.
 * <p/>
 * This class is designed to be used within jvmcluster. first will obtain distributed lock and then
 * create schema if necessary.
 * 
 * @author kate2007
 */
@ThreadSafe
public class RttpBatchSchemaCreator extends AbstractSchemaCreator {
   public RttpBatchSchemaCreator() {
      tablesPrefix = AbstractJdbcBatchMetadataDao.DEFAULT_TABLE_PREFIX;
   }

   /**
    * will create batch schema if does not exists
    * 
    * @param connection
    *           database connection
    * @throws java.sql.SQLException
    *            re-throw any SQLExceptions
    */
   @Override
   public void populate(Connection connection) throws SQLException {
      if (getBatchCreationScript() != null) {
         StopWatch stopWatch = new StopWatch(BATCH_DB_CREATION_LOCK);
         HazelcastInstance instance = springHazelcastBridge.getRunningInstance();

         ILock lock = instance.getLock(BATCH_DB_CREATION_LOCK);
         logger.info("using %s and dialect %s for batch schema creation", lock.toString(),
                  dialect.toString());
         try {
            if (lock.tryLock()) {
               try {
                  if (!tablesCreated(connection)) {
                     if (!springHazelcastBridge.getAtomicBoolean(BATCH_DB_CREATION_LOCK).get()) {
                        logger.info("batch schema is not created, will create using connection %s",
                                 connection);
                        springHazelcastBridge.updateAtomicBoolean(BATCH_DB_CREATION_LOCK, true);
                        super.setScripts(new Resource[] { getBatchCreationScript() });
                        try {
                           super.populate(connection);
                        } catch (ScriptStatementFailedException e) {
                           logger.warn(
                                    "unable to create spring batch schema for dialect %s, exception = %s",
                                    dialect.getSimpleName(), ExceptionUtils.getFullStackTrace(e));
                        }
                     }
                  }
               } finally {
                  lock.unlock();
               }
            }
         } finally {
            stopWatch.stop();
            logger.debug("spring batch schema created in %s milisecs using %s",
                     stopWatch.getElapsedTime(), connection);
         }
      }
   }

   @Override
   public Collection<String> tables() {
      Collection<String> tables = new LinkedHashSet<String>();
      tables.add(tablesPrefix + "JOB_INSTANCE");
      tables.add(tablesPrefix + "JOB_EXECUTION");
      tables.add(tablesPrefix + "JOB_PARAMS");
      tables.add(tablesPrefix + "STEP_EXECUTION");
      tables.add(tablesPrefix + "STEP_EXECUTION_CONTEXT");
      tables.add(tablesPrefix + "JOB_EXECUTION_CONTEXT");
      return tables;
   }
}
