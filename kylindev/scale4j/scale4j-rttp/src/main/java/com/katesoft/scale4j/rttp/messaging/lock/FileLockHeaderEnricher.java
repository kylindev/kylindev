package com.katesoft.scale4j.rttp.messaging.lock;

import org.springframework.integration.Message;
import org.springframework.integration.MessageHeaders;
import org.springframework.integration.file.FileHeaders;

import com.katesoft.scale4j.common.utils.AssertUtility;

/**
 * this class uses original file as key for locking
 * 
 * @author kate2007
 */
public class FileLockHeaderEnricher extends AbstractInboundLockHeaderEnricher {

   @Override
   protected String deriveLockId(Message<?> message) {
      MessageHeaders headers = message.getHeaders();
      String originalFile = headers.get(FileHeaders.ORIGINAL_FILE, String.class);
      AssertUtility.assertNotNull(originalFile, "[originalFile]");
      return originalFile;
   }
}
