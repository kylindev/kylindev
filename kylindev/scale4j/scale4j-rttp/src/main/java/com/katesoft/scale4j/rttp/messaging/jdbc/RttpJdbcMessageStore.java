package com.katesoft.scale4j.rttp.messaging.jdbc;

import org.hibernate.dialect.Dialect;
import org.hibernate.dialect.Oracle8iDialect;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.integration.jdbc.JdbcMessageStore;
import org.springframework.jdbc.support.lob.OracleLobHandler;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

/**
 * Just a very simple extension of JdbcMessageStore which inject proper LobHandler for Oracle
 * database.
 * 
 * @author kate2007
 */
public class RttpJdbcMessageStore extends JdbcMessageStore {
   private final Logger logger = LogFactory.getLogger(getClass());
   private Class<? extends Dialect> dialect;

   @Required
   public void setDialect(Class<? extends Dialect> dialect) {
      this.dialect = dialect;
   }

   @Override
   public void afterPropertiesSet() throws Exception {
      super.afterPropertiesSet();
      if (Oracle8iDialect.class.isAssignableFrom(dialect)) {
         OracleLobHandler oracleLobHandler = new OracleLobHandler();
         logger.info("setting oracle lob handler = %s to be used within jdbc message store",
                  oracleLobHandler);
         setLobHandler(oracleLobHandler);
      }
   }
}
