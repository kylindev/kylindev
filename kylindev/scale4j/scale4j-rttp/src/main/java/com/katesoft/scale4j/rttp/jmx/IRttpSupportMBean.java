package com.katesoft.scale4j.rttp.jmx;

import java.util.Collection;
import java.util.Date;

/**
 * @author kate2007
 */
public interface IRttpSupportMBean {
   String OBJ_RTTP_HIBERNATE_STATISTIC = "rttp.hibernate:name=statistics";
   String OBJ_RTTP_ATOMIKOS_STATISTIC = "rttp.atomikos:name=statistics";
   String OBJ_RTTP_SUPPORT = "rttp.support:name=support";
   String OBJ_RTTP_QUARTZ_MANAGER = "rttp.quartz:name=manager";

   void stopService();

   Collection<String> beanNamesForType(String clazz) throws ClassNotFoundException;

   Date getStartUpDate();

   int getBeanDefinitionCount();
}
