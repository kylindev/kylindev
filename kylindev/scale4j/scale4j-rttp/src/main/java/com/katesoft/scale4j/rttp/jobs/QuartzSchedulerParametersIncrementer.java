package com.katesoft.scale4j.rttp.jobs;

import static com.katesoft.scale4j.common.utils.AssertUtility.assertNotNull;

import java.util.Date;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersIncrementer;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

/**
 * This class will add scheduler date(truncated to milisecs) as parameter.
 * 
 * @author kate2007
 */
public class QuartzSchedulerParametersIncrementer implements JobParametersIncrementer {
   public static final String FIRED_TIME = "rttp.quartz.scheduler.fired.time";
   private static final Logger LOGGER = LogFactory
            .getLogger(QuartzSchedulerParametersIncrementer.class);
   //
   private final Date fireDate;

   public QuartzSchedulerParametersIncrementer(Date fireDate) {
      assertNotNull(fireDate, "fire date can't be null");
      this.fireDate = new Date(fireDate.getTime());
   }

   @Override
   public JobParameters getNext(JobParameters parameters) {
      LOGGER.debug("adding fire_date=%s to job parameters map", fireDate);
      return new JobParametersBuilder(parameters).addDate(FIRED_TIME, fireDate).toJobParameters();
   }
}
