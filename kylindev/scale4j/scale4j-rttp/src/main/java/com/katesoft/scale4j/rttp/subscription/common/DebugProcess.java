package com.katesoft.scale4j.rttp.subscription.common;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;
import com.katesoft.scale4j.rttp.subscription.IBusinessProcess;
import com.katesoft.scale4j.rttp.subscription.SubscriptionEvent;
import net.jcip.annotations.ThreadSafe;

import java.io.Serializable;

/**
 * business processes that will debug all jvmcluster commitments.s
 * 
 * @author kate2007
 */
@ThreadSafe
public class DebugProcess implements IBusinessProcess<AbstractPersistentEntity>, Serializable {
   private static final long serialVersionUID = -727245257058567371L;
   private Logger logger = LogFactory.getLogger(DebugProcess.class);

   @Override
   public void process(SubscriptionEvent<AbstractPersistentEntity> event) {
      logger.debug("entity %s/%s was committed to cluster", event.getEntity().getClass()
               .getSimpleName(), event.getEntity().getGlobalUniqueIdentifier());
   }
}
