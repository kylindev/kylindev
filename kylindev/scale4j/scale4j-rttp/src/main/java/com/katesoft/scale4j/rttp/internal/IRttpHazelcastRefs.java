package com.katesoft.scale4j.rttp.internal;

/**
 * This class contains hardcoded references to internal hazelcast objects.
 * <p/>
 * This is internal interface.
 * 
 * @author kate2007
 */
public interface IRttpHazelcastRefs {
   String ID_GENERATOR_MAP = "rttp.id.generator.map";
   String JOB_PARAMS_INCREMENTER = "rttp.params.incrementer";
   String SUBSCRIBER_EXECUTOR = "rttp.cloud.subscription.executor";

   String SCHEDULER_QUARTZ_DB_CREATION_LOCK = "rttp.quartz.schema.creation.lock";
   String BATCH_DB_CREATION_LOCK = "rttp.batch.schema.creation.lock";
   String MESSAGE_STORE_CREATION_LOCK = "rttp.message.store.creation.lock";
}
