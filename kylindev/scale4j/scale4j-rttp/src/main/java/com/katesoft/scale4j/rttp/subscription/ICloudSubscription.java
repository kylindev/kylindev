package com.katesoft.scale4j.rttp.subscription;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

import java.util.Collection;

/**
 * defines predicate + business services required for subscription.
 * 
 * @author kate2007
 */
public interface ICloudSubscription<T extends AbstractPersistentEntity> {
   Class<T> getSubscriptionClass();

   IUnaryPredicate<T> getPredicate();

   Collection<IBusinessProcess<T>> getProcesses();
}
