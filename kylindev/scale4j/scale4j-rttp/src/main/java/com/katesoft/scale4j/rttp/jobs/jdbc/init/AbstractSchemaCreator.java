package com.katesoft.scale4j.rttp.jobs.jdbc.init;

import java.io.IOException;

import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;

import com.katesoft.scale4j.persistent.spring.EntityManagedOrNativeHibernateFactoryBean;
import com.katesoft.scale4j.rttp.hibernate.SpringHazelcastBridge;
import com.katesoft.scale4j.rttp.internal.AbstractJdbcSchemaCreator;

/**
 * @author kate2007
 */
abstract class AbstractSchemaCreator extends AbstractJdbcSchemaCreator {
   private Configuration configuration;

   @Override
   public void afterPropertiesSet() throws IOException {
      if (dialect == null) {
         dialect = Dialect.getDialect(configuration.getProperties()).getClass();
      }
      super.afterPropertiesSet();
   }

   @Required
   @Autowired
   public void setEntityManagedOrNativeHibernateFactoryBean(
            EntityManagedOrNativeHibernateFactoryBean bean) {
      this.configuration = bean.getConfiguration();
   }

   @Override
   @Required
   public void setRttpHazelcastBridge(SpringHazelcastBridge springHazelcastBridge) {
      this.springHazelcastBridge = springHazelcastBridge;
   }
}
