package com.katesoft.scale4j.web.services;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;
import java.util.UUID;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jmx.StatisticsService;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.katesoft.scale4j.web.LocalPersistentIntegrationTest;
import com.katesoft.scale4j.web.exceptions.LocaleNotFoundException;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.model.UserLocale;

/** @author kate2007 */
public class UserRoleBehaviourTest extends LocalPersistentIntegrationTest {
   private StatisticsService statisticsService;
   private UserRoleService userRoleService;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      //
      userRoleService = new UserRoleService();
      userRoleService.setHibernateTemplate(hibernateTemplate);
      userRoleService.setUserDetailsService(userDetailsService);
      //
      statisticsService = new StatisticsService();
      statisticsService.setSessionFactory(sessionFactory);
      statisticsService.clear();
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void saveUserExecutedCorrectly() {
      saveUser();
      assertThat(statisticsService.getEntityInsertCount(), is(4L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(0L));
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void updateUserExecutedCorrectly() {
      final User user = saveUser();
      statisticsService.clear();
      user.setPassword("another_password");
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.update(user);
            return null;
         }
      });
      assertThat(statisticsService.getEntityInsertCount(), is(2L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(1L));
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void updatingUserPreferencesWorksCorrectly() {
      final User user = saveUser();
      statisticsService.clear();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            UserLocale userLocale = new UserLocale();
            userLocale.setCode("RU");
            userLocale.setLocale("russian");
            try {
               userRoleService.loadLocale("RU");
            } catch (LocaleNotFoundException e) {
               session.saveOrUpdate(userLocale);
               session.flush();
            }
            user.getPerson().setFistName("user_first_name");
            user.getPerson().setSecondName("user_second_name");
            user.getPerson().setMiddleName("user_middle_name");
            user.getUserPreferences().setLocale(userLocale);
            userRoleService.saveOrUpdateUser(user);
            //
            return null;
         }
      });
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.clear();
            User copy = (User) session.get(User.class, user.getUniqueIdentifier());
            assertThat(copy.getUserPreferences().getLocale().getCode(), is("RU"));
            assertThat(copy.getPerson().getFistName(), is("user_first_name"));
            assertThat(copy.getPerson().getSecondName(), is("user_second_name"));
            assertThat(copy.getPerson().getMiddleName(), is("user_middle_name"));
            return null;
         }
      });
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void deleteUserExecutedCorrectly() {
      final User user = saveUser();
      statisticsService.clear();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.delete(session.get(User.class, user.getUniqueIdentifier()));
            return null;
         }
      });
      assertThat(statisticsService.getEntityInsertCount(), is(2L));
      assertThat(statisticsService.getEntityDeleteCount(), is(1L));
      assertThat(statisticsService.getEntityUpdateCount(), is(0L));
   }

   private User saveUser() {
      final User user = testInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(@SuppressWarnings("unused") Session session)
                  throws HibernateException, SQLException {
            userRoleService.saveOrUpdateUser(user);
            return null;
         }
      });
      return user;
   }

   public static User testInstance() {
      User user = new User();
      user.setLogin(UUID.randomUUID().toString());
      user.setPassword("09842323");
      user.setEmail("email" + UUID.randomUUID().toString());
      user.setOpenId("openid9007");
      user.getPerson();
      user.getAccount();
      return user;
   }
}
