package com.katesoft.scale4j.web.jetty;

import com.katesoft.scale4j.common.utils.Enumerator;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.log.LogFactory;
import org.apache.log4j.Appender;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.perf4j.log4j.AsyncCoalescingStatisticsAppender;

import static com.katesoft.scale4j.log.LogFactory.getAllRootAppenders;
import static com.katesoft.scale4j.persistent.utils.Perf4jLoggerUtility.allAppenders;
import static com.katesoft.scale4j.persistent.utils.Perf4jLoggerUtility.findAsyncCoalescingStatisticsAppender;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/** @author kate2007 */
public class LogFactoryTest {
   private static final Logger LOGGER = LogFactory.getLogger(LogFactoryTest.class);

   @Test
   public void appenderListIsNotEmpty() {
      boolean atLeastOne = false;
      Enumerator<Appender> allAppenders = getAllRootAppenders();
      while (allAppenders.hasNext()) {
         LOGGER.info(allAppenders.next().toString());
         atLeastOne = true;
      }
      assertThat(atLeastOne, is(true));
   }

   @Test
   public void perf4jAppendersListIsNotEmpty() {
      boolean atLeastOne = false;
      Enumerator<Appender> perf4jAppenders = allAppenders();
      while (perf4jAppenders.hasNext()) {
         LOGGER.info(perf4jAppenders.next().toString());
         atLeastOne = true;
      }
      assertThat(atLeastOne, is(true));
   }

   @Test
   public void asyncCoalescingStatisticsAppenderFound() {
      AsyncCoalescingStatisticsAppender statisticsAppender = findAsyncCoalescingStatisticsAppender();
      assertThat(statisticsAppender, is(Matchers.<Object> notNullValue()));
   }
}
