package com.katesoft.scale4j.web.jetty;

import static com.katesoft.scale4j.persistent.utils.Perf4jLoggerUtility.findGraphingStatisticsAppender;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;

import java.util.Collection;
import java.util.List;
import java.util.Random;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.perf4j.log4j.GraphingStatisticsAppender;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.persistent.client.LocalHibernateTemplate;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.web.webconsole.WebMonitor;

/** @author kate2007 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/simple-rttpContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
@DirtiesContext
public class WebconsoleTest extends AbstractPersistentTest {
   private WebMonitor webMonitor;
   private SelectChannelConnector connector;

   @Override
   public void tearDown() throws Exception {
      hibernateTemplate.deleteAll(TestEntity.class);
      webMonitor.stop();
      super.tearDown();
   }

   @Override
   public void setUp() throws Exception {
      super.setUp();
      webMonitor = new WebMonitor();
      webMonitor.setPerf4jPath("/perf4j");
      webMonitor.setLog4jPath("/log4j");
      webMonitor.setContextPath("/");
      webMonitor.setHazelcastPath("/hazelcast");
      connector = new SelectChannelConnector();
      connector.setPort(8956 + new Random().nextInt(2000));
      webMonitor.setConnectors(new Connector[] { connector });
      webMonitor.afterPropertiesSet();
      logger.info("jetty server launched at %s port", connector.getPort());
   }

   @Test
   public void hazelcastAvailable() throws InterruptedException {
      if (webMonitor.isHazelcastAvailable()) {
         WebDriver driver = new HtmlUnitDriver();
         driver.get(String.format("http://localhost:%s%s", connector.getPort(),
                  webMonitor.getHazelcastPath()));
         assertThat(driver.getTitle(), containsString("Hazelcast Monitoring Tool"));
      }
   }

   @Test
   public void log4jConfigurationAvailable() throws InterruptedException, ClassNotFoundException {
      boolean containsLogElementForInternalClass = false;
      WebDriver driver = new HtmlUnitDriver();
      driver.get(String.format("http://localhost:%s%s", connector.getPort(),
               webMonitor.getLog4jPath()));
      List<WebElement> input = driver.findElements(By.tagName("input"));
      for (WebElement element : input) {
         String value = element.getValue();
         if (value.contains(LocalHibernateTemplate.class.getPackage().getName())) {
            Class.forName(value);
         }
         containsLogElementForInternalClass = true;
      }
      assertThat(containsLogElementForInternalClass, is(true));
   }

   @Test
   public void perf4jPageRendersGraphs() throws InterruptedException {
      for (int i = 0; i <= 50; i++) {
         final TestEntity entity = TestEntity.newTestInstance();
         transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(
                     @SuppressWarnings("unused") TransactionStatus status) {
               hibernateTemplate.save(entity);
               hibernateTemplate.flush();
               hibernateTemplate.clear();
               hibernateTemplate.loadByGlobalUniqueIdentifier(TestEntity.class,
                        entity.getGlobalUniqueIdentifier(), true);
               hibernateTemplate.clear();
               hibernateTemplate.findEntityForUpdate(TestEntity.class,
                        entity.getUniqueIdentifier(), entity.getVersion());
               hibernateTemplate.clear();
            }
         });
         transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(
                     @SuppressWarnings("unused") TransactionStatus status) {
               hibernateTemplate.delete(hibernateTemplate.get(TestEntity.class,
                        entity.getUniqueIdentifier()));
            }
         });
      }
      WebDriver driver = new HtmlUnitDriver();
      driver.get(String.format("http://localhost:%s%s", connector.getPort(),
               webMonitor.getPerf4jPath()));
      Collection<GraphingStatisticsAppender> collection = findGraphingStatisticsAppender();
      List<WebElement> bElements = driver.findElements(By.tagName("b"));
      List<WebElement> imgElements = driver.findElements(By.tagName("img"));
      assertThat(bElements.size(), is(collection.size()));
      assertThat(imgElements.size(), is(collection.size()));
   }
}
