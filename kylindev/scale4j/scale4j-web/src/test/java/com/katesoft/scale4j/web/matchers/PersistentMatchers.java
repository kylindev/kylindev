package com.katesoft.scale4j.web.matchers;

import org.hamcrest.Factory;
import org.junit.Ignore;

/**
 * This class defines static factory methods for matchers.
 */
@Ignore
public class PersistentMatchers {
   @Factory
   public static HasRoleMatcher hasRole(String role) {
      return new HasRoleMatcher(role);
   }
}
