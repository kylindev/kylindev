package com.katesoft.scale4j.web;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.persistent.client.LocalHibernateTemplate;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.services.RDBSUserDetailsService;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/applicationContext.xml",
         "/META-INF/spring/jvmcluster.rttp$postprocessing.xml" })
@Transactional
@TransactionConfiguration
@DirtiesContext
public abstract class LocalPersistentIntegrationTest extends AbstractPersistentTest {
   protected RDBSUserDetailsService userDetailsService = new RDBSUserDetailsService();

   protected User loadAdminUser() {
      return userDetailsService.loadUserByUsername("admin");
   }

   @Autowired
   @Qualifier(value = IBeanNameReferences.HIBERNATE_TEMPLATE)
   @Override
   public void setHibernateTemplate(final LocalHibernateTemplate hibernateTemplate) {
      super.setHibernateTemplate(hibernateTemplate);
      userDetailsService.setHibernateTemplate(hibernateTemplate);
   }
}
