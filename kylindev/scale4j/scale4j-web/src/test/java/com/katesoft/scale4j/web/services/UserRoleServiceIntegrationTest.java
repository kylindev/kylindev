package com.katesoft.scale4j.web.services;

import static com.katesoft.scale4j.web.matchers.PersistentMatchers.hasRole;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Collection;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.annotation.Rollback;

import com.katesoft.scale4j.web.LocalPersistentIntegrationTest;
import com.katesoft.scale4j.web.model.Role;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.security.ISecurityRoles;

public class UserRoleServiceIntegrationTest extends LocalPersistentIntegrationTest {
   private UserRoleService userRoleService;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      userRoleService = new UserRoleService();
      userRoleService.setHibernateTemplate(hibernateTemplate);
      userRoleService.setUserDetailsService(userDetailsService);
   }

   @Test
   public void loadUserByAdminUsernameWillLoadAdminUser() {
      Assert.assertEquals(loadAdminUser().getLogin(), "admin");
   }

   @Test
   public void findAllRolesReturnCollectionWithSizeEqualsToSupportedRolesCollectionSize() {
      final Collection<Role> allRoles = userRoleService.findAllRoles();
      Assert.assertEquals(allRoles.size(), Role.supportedRoles().size());
   }

   @Test
   public void requestNewPasswordForValidUserReturnNotNullNewPassword() {
      final String newPassword = userRoleService.requestNewPassword(loadAdminUser().getLogin());
      Assert.assertNotNull(newPassword);
   }

   @Test(expected = UsernameNotFoundException.class)
   public void requestNewPasswordForNotExistingUserThrowsException() {
      final User user = mock(User.class);
      when(user.getLogin()).thenReturn("adfjkadkfnajkdfnjdanf3r9324i$#$@$");
      userRoleService.requestNewPassword(user.getLogin());
   }

   @Test
   @Rollback(true)
   public void updatePasswordStoresNewPasswordInDatabase() {
      userRoleService.updatePassword("12345", loadAdminUser().getUsername());
      Assert.assertEquals("12345", loadAdminUser().getPassword());
   }

   @Test
   public void searchByAdminQueryStringReturnsNotEmptyResults() {
      final Collection<User> users = userRoleService.search("admin");
      logger.debug("found %s for search string [%s]", users, "admin");
      Assert.assertTrue(users.contains(loadAdminUser()));
   }

   @Test(expected = UsernameNotFoundException.class)
   public void verifyDeleteRemovesRecord() {
      userRoleService.deleteUser(loadAdminUser());
      userRoleService.loadUserByUsername("admin");
   }

   @Test(expected = DataAccessException.class)
   public void savingUserWithoutLoginIsNotPermitted() {
      final User user = new User();
      userRoleService.saveOrUpdateUser(user);
   }

   @Test(expected = DataAccessException.class)
   public void savingUserWithoutPasswordIsNotPermitted() {
      final User user = new User();
      user.setLogin("123");
      userRoleService.saveOrUpdateUser(user);
   }

   @SuppressWarnings("unchecked")
   @Test
   public void savingWorksWithLoginAndPasswordOnly() {
      User user = new User();
      user.setLogin("123");
      user.setPassword("09842323");
      userRoleService.saveOrUpdateUser(user);
      assertThat(
               user,
               allOf(hasRole(ISecurityRoles.ROLE_USER), not(hasRole(ISecurityRoles.ROLE_ADMIN)),
                        not(hasRole(ISecurityRoles.ROLE_SYSTEM_ADMIN))));
      user.setOpenId("openid1");
      userRoleService.saveOrUpdateUser(user);
   }
}
