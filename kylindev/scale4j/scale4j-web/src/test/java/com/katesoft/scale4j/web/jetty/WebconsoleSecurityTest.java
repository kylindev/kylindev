package com.katesoft.scale4j.web.jetty;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
import com.katesoft.scale4j.web.model.Role;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.security.ISecurityRoles;
import com.katesoft.scale4j.web.webconsole.WebMonitor;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mortbay.jetty.Connector;
import org.mortbay.jetty.nio.SelectChannelConnector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collections;
import java.util.Random;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/** @author kate2007 */
public class WebconsoleSecurityTest {
   private WebMonitor webMonitor;
   private SelectChannelConnector connector;
   private WebDriver driver;

   @After
   public void tearDown() throws Exception {
      webMonitor.stop();
   }

   @Before
   public void setUp() throws Exception {
      ProviderManager providerManager = new ProviderManager();
      DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
      UserDetailsService userDetailsService = mock(UserDetailsService.class);
      final String login = "user123";
      final String pass = "user123-pass";
      User user = new User();
      user.setLogin(login);
      user.setPassword(pass);
      user.setRoles(Collections.singleton(new Role(ISecurityRoles.ROLE_ADMIN)));
      when(userDetailsService.loadUserByUsername("user123")).thenReturn(user);
      authenticationProvider.setUserDetailsService(userDetailsService);
      providerManager.setProviders(Collections.singletonList(authenticationProvider));
      webMonitor = new WebMonitor();
      connector = new SelectChannelConnector();
      connector.setPort(9956 + new Random().nextInt(2000));
      webMonitor.setConnectors(new Connector[] { connector });
      webMonitor.setAuthenticationManager(providerManager);
      webMonitor.afterPropertiesSet();
      driver = new HtmlUnitDriver() {
         @Override
         protected WebClient newWebClient(BrowserVersion browserVersion) {
            WebClient client = super.newWebClient(browserVersion);
            DefaultCredentialsProvider provider = new DefaultCredentialsProvider();
            provider.addCredentials(login, pass);
            client.setCredentialsProvider(provider);
            return client;
         }
      };
   }

   @Test
   public void ableToOpenHazelcastPageIfAuthenticated() throws InterruptedException {
      if (webMonitor.isHazelcastAvailable()) {
         driver.get(String.format("http://localhost:%s%s", connector.getPort(),
                  webMonitor.getHazelcastPath()));
         assertThat(driver.getTitle(), is("Hazelcast Monitoring Tool"));
      }
   }

   @Test
   public void notAbleToOpenHazelcastPageIfAuthenticated() throws InterruptedException {
      if (webMonitor.isHazelcastAvailable()) {
         driver = new HtmlUnitDriver() {
            @Override
            protected WebClient newWebClient(BrowserVersion browserVersion) {
               WebClient client = super.newWebClient(browserVersion);
               DefaultCredentialsProvider provider = new DefaultCredentialsProvider();
               provider.addCredentials("1", "12");
               client.setCredentialsProvider(provider);
               return client;
            }
         };
         driver.get(String.format("http://localhost:%s%s", connector.getPort(),
                  webMonitor.getHazelcastPath()));
         assertThat(driver.getTitle(), is(not("Hazelcast Monitoring Tool")));
      }
   }
}
