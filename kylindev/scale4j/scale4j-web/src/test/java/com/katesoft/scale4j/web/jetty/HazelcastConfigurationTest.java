package com.katesoft.scale4j.web.jetty;

import com.katesoft.scale4j.web.webconsole.WebMonitor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mortbay.jetty.Connector;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

/** @author kate2007 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/jetty-test-cfg.xml" })
@DirtiesContext
public class HazelcastConfigurationTest {
   @Autowired
   private WebMonitor webMonitor;
   private Connector connector;

   @Before
   public void setUp() throws Exception {
      connector = webMonitor.getConnectors()[0];
   }

   @Test
   public void hazelcastAvailable() {
      assertThat(webMonitor.isHazelcastAvailable(), is(true));
      WebDriver driver = new HtmlUnitDriver();
      driver.get(String.format("http://localhost:%s%s", connector.getPort(),
               webMonitor.getHazelcastPath()));
      assertThat(driver.getTitle(), containsString("Hazelcast Monitoring Tool"));
   }

   @Test
   public void log4jConfigurationAvailable() throws InterruptedException, ClassNotFoundException {
      WebDriver driver = new HtmlUnitDriver();
      driver.get(String.format("http://localhost:%s%s", connector.getPort(),
               webMonitor.getLog4jPath()));
      assertThat(driver.getTitle(), is(not(containsString("404"))));
   }

   @Test
   public void perf4jPageRendersGraphs() throws InterruptedException {
      WebDriver driver = new HtmlUnitDriver();
      driver.get(String.format("http://localhost:%s%s", connector.getPort(),
               webMonitor.getPerf4jPath()));
      assertThat(driver.getTitle(), is(not(containsString("404"))));
   }
}
