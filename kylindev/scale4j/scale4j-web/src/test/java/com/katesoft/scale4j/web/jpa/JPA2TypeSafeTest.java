package com.katesoft.scale4j.web.jpa;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.SetJoin;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.orm.jpa.JpaCallback;

import com.katesoft.scale4j.web.LocalPersistentIntegrationTest;
import com.katesoft.scale4j.web.model.Role;
import com.katesoft.scale4j.web.model.Role_;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.model.User_;
import com.katesoft.scale4j.web.model.embedded.Account_;
import com.katesoft.scale4j.web.model.embedded.Person_;
import com.katesoft.scale4j.web.services.UserRoleBehaviourTest;

/** @author kate2007 */
public class JPA2TypeSafeTest extends LocalPersistentIntegrationTest {
   @Test
   public void ableToLoadEntityByUniqueIdentifierAttribute() {
      final User entity = UserRoleBehaviourTest.testInstance();
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      User execute = hibernateTemplate.execute(new JpaCallback<User>() {
         @Override
         public User doInJpa(EntityManager em) throws PersistenceException {
            CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> query = cb.createQuery(User.class);
            Root<User> root = query.from(User.class);
            query.where(cb.equal(root.get(User_.uniqueIdentifier), entity.getUniqueIdentifier()),
                     cb.equal(root.get(User_.guid), entity.getGlobalUniqueIdentifier()));
            return em.createQuery(query).getResultList().iterator().next();
         }
      });
      assertThat(execute.getUniqueIdentifier(), is(entity.getUniqueIdentifier()));
   }

   @Test
   public void ableToLoadEntityByEmbeddedElements() {
      final User entity = UserRoleBehaviourTest.testInstance();
      //
      entity.getPerson().setFistName("first_name1");
      entity.getPerson().setSecondName("second_name1");
      entity.getPerson().setMiddleName("middle_name1");
      //
      entity.getAccount().setAccountExpired(false);
      entity.getAccount().setAccountLocked(true);
      entity.getAccount().setCredentialsExpired(false);
      //
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      hibernateTemplate.execute(new JpaCallback<User>() {
         @Override
         public User doInJpa(EntityManager em) throws PersistenceException {
            final CriteriaBuilder cb = em.getCriteriaBuilder();
            CriteriaQuery<User> query = cb.createQuery(User.class);
            final Root<User> root = query.from(User.class);
            List<Predicate> conditions = new LinkedList<Predicate>() {
               private static final long serialVersionUID = -7129037877630720121L;

               {
                  add(cb.equal(root.get(User_.uniqueIdentifier), entity.getUniqueIdentifier()));
                  add(cb.equal(root.get(User_.guid), entity.getGlobalUniqueIdentifier()));
                  add(cb.equal(root.get(User_.person).get(Person_.fistName), "first_name1"));
                  add(cb.equal(root.get(User_.person).get(Person_.secondName), "second_name1"));
                  add(cb.equal(root.get(User_.person).get(Person_.middleName), "middle_name1"));
                  add(cb.equal(root.get(User_.account).get(Account_.accountExpired), false));
                  add(cb.equal(root.get(User_.account).get(Account_.accountLocked), true));
               }
            };
            query.where(conditions.toArray(new Predicate[conditions.size()]));
            User user = em.createQuery(query).getResultList().iterator().next();
            assertThat(user.getUniqueIdentifier(), is(entity.getUniqueIdentifier()));
            conditions.add(cb.equal(root.get(User_.account).get(Account_.credentialsExpired), true));
            query.where(conditions.toArray(new Predicate[conditions.size()]));
            assertThat(em.createQuery(query).getResultList().iterator().hasNext(), is(false));
            return null;
         }
      });
   }

   @Test
   public void ableToJoinRolesAndQueryByIdentifier() {
      final User entity = UserRoleBehaviourTest.testInstance();
      hibernateTemplate.save(entity);
      Role role = new Role();
      role.setRole("role1");
      hibernateTemplate.save(role);
      hibernateTemplate.flush();
      entity.getRoles().add(role);
      hibernateTemplate.update(entity);
      hibernateTemplate.flush();
      hibernateTemplate.clear();
      User execute = hibernateTemplate.execute(new JpaCallback<User>() {
         @Override
         public User doInJpa(EntityManager em) throws PersistenceException {
            return queryUserByIdAndRole(em, entity, "role1");
         }
      });
      assertThat(execute.getUniqueIdentifier(), is(entity.getUniqueIdentifier()));
      execute = hibernateTemplate.execute(new JpaCallback<User>() {
         @Override
         public User doInJpa(EntityManager em) throws PersistenceException {
            return queryUserByIdAndRole(em, entity, "role2");
         }
      });
      assertThat(execute, is(Matchers.<Object> nullValue()));
   }

   public static User queryUserByIdAndRole(EntityManager em, User entity, String role) {
      CriteriaBuilder cb = em.getCriteriaBuilder();
      CriteriaQuery<User> query = cb.createQuery(User.class);
      Root<User> root = query.from(User.class);
      SetJoin<User, Role> rolesJoin = root.join(User_.roles);
      query.where(cb.equal(rolesJoin.get(Role_.role), role),
               cb.equal(root.get(User_.uniqueIdentifier), entity.getUniqueIdentifier()),
               cb.equal(root.get(User_.guid), entity.getGlobalUniqueIdentifier()));
      Iterator<User> iterator = em.createQuery(query).getResultList().iterator();
      return iterator.hasNext() ? iterator.next() : null;
   }
}
