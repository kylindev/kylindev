package com.katesoft.scale4j.web.model;

import com.katesoft.scale4j.web.security.ISecurityRoles;
import org.junit.Test;

import java.util.Collection;

import static org.junit.Assert.assertTrue;

public class RoleTest {
   @Test
   public void testSupportedRoles() {
      Collection<String> supportedRoles = Role.supportedRoles();
      assertTrue(supportedRoles.contains(ISecurityRoles.ROLE_ADMIN));
      assertTrue(supportedRoles.contains(ISecurityRoles.ROLE_SYSTEM_ADMIN));
   }
}
