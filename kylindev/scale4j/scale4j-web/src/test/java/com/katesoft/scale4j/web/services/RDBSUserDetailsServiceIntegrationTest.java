package com.katesoft.scale4j.web.services;

import com.katesoft.scale4j.web.LocalPersistentIntegrationTest;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.security.ISecurityRoles;
import junit.framework.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

public class RDBSUserDetailsServiceIntegrationTest extends LocalPersistentIntegrationTest {
   private RDBSUserDetailsService rdbsUserDetailsService;

   @Test
   public void testLoadUserByUsername() {
      User userDetails = rdbsUserDetailsService.loadUserByUsername("admin");
      Assert.assertNotNull(userDetails.getPassword());
      boolean containsAdminRole = false;
      Collection<GrantedAuthority> authorities = userDetails.getAuthorities();
      for (GrantedAuthority grantedAuthority : authorities) {
         if (grantedAuthority.getAuthority().equalsIgnoreCase(ISecurityRoles.ROLE_SYSTEM_ADMIN)) {
            containsAdminRole = true;
         }
      }
      Assert.assertTrue(containsAdminRole);
   }

   @Required
   @Autowired
   @Qualifier(value = "jvmcluster.rdbsUserDetailsService")
   public void setRdbsUserDetailsService(final RDBSUserDetailsService rdbsUserDetailsService) {
      this.rdbsUserDetailsService = rdbsUserDetailsService;
   }
}
