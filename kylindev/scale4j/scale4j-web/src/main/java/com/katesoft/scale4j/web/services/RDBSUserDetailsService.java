package com.katesoft.scale4j.web.services;

import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.support.WebappDaoSupport;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.annotation.Transactional;

/**
 * Very common relation database service. retrieve {@link User} from database using internal
 * hibernate implementation.
 * 
 * @author kate2007
 */
@Transactional(rollbackFor = Exception.class)
public class RDBSUserDetailsService extends WebappDaoSupport {
   @Override
   @Transactional(readOnly = true)
   public User loadUserByUsername(String username) throws UsernameNotFoundException,
            DataAccessException {
      return super.loadUserByUsername(username);
   }
}
