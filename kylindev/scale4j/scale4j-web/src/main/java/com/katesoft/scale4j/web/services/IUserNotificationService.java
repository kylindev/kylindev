package com.katesoft.scale4j.web.services;

import com.katesoft.scale4j.web.model.User;

/** @author kate2007 */
public interface IUserNotificationService {
   /**
    * triggers when user requested new password.
    * <p/>
    * implementation can for example send email to user.
    * 
    * @param newPassword
    *           generated password
    * @param user
    *           domain entity
    */
   void notifyNewPassword(String newPassword, User user);

   /**
    * triggers when user's password being updated.
    * <p/>
    * implementation can for example send email to user.
    * 
    * @param password
    *           new password
    * @param user
    *           domain entity
    */
   void notifyPasswordUpdated(String password, User user);

   /**
    * triggers when user's preferable locale being updated.
    * 
    * @param locale
    *           new locale
    * @param user
    *           domain entity
    */
   void notifyUserPreferableLocaleUpdated(String locale, User user);
}
