package com.katesoft.scale4j.web.support;

import com.katesoft.scale4j.web.exceptions.LocaleNotFoundException;
import com.katesoft.scale4j.web.exceptions.RoleNotFoundException;
import com.katesoft.scale4j.web.model.Role;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.model.UserLocale;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * This interface defines common method that can be found in typical web application.
 * 
 * @author kate2007
 */
public interface IWebappSupportOperations {
   /**
    * retrieve user bound to thread {@link SecurityContextHolder}
    * 
    * @return logged in user if available else <code>NULL</code>.
    */
   User getLoggedInUser();

   /**
    * load user from database based for given {@link UserDetails} parameter.
    * <p/>
    * If userDetails parameter is <code>NULL</code>, trying to retrieve user using
    * {@link IWebappSupportOperations#getLoggedInUser()} method.
    * <p/>
    * If still can't identify throw {@link UsernameNotFoundException}.
    * 
    * @param username
    *           login of user.
    * @return loaded user *
    * @throws UsernameNotFoundException
    *            if user details information is not provided by method parameter and if no
    *            information available from Thread.
    */
   User loadUser(String username) throws UsernameNotFoundException;

   /**
    * Load {@link UserLocale} domain entity for given code parameter.
    * 
    * @param code
    *           locale's code
    * @return {@link UserLocale} domain entity.
    * @throws LocaleNotFoundException
    *            if locale is not found.
    */
   UserLocale loadLocale(String code) throws LocaleNotFoundException;

   /**
    * Load {@link Role} domain entity for given role parameter.
    * 
    * @param role
    *           code of role.
    * @return {@link Role} domain entity.
    * @throws RoleNotFoundException
    *            if role is not found.
    */
   Role loadRoleByName(String role) throws RoleNotFoundException;
}