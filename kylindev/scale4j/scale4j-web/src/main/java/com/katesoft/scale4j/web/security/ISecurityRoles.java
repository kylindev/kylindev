package com.katesoft.scale4j.web.security;

/**
 * Contains definition of common roles.
 * <p/>
 * Minimal set included(admin, user) at the moment.
 * 
 * @author kate2007
 */
public interface ISecurityRoles {
   String IS_AUTHENTICATED_ANONYMOUSLY = "IS_AUTHENTICATED_ANONYMOUSLY";
   //
   String ROLE_SYSTEM_ADMIN = "ROLE_SYSTEM_ADMIN";
   String ROLE_ADMIN = "ROLE_ADMIN";
   String ROLE_USER = "ROLE_USER";
}
