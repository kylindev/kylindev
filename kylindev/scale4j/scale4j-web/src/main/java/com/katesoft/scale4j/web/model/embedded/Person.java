package com.katesoft.scale4j.web.model.embedded;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;

/**
 * Classical implementation of Person entity.
 * 
 * @author kate2007
 */
@Embeddable
@Audited
@Access(AccessType.FIELD)
public class Person implements Serializable {
   private static final long serialVersionUID = 5431555040666160361L;
   // ***********************************************************************
   // ************************ persistent fields ****************************
   // ***********************************************************************
   @Column(name = "first_name", length = 100)
   @Field(index = Index.TOKENIZED, store = Store.NO)
   private String fistName;
   @Column(name = "second_name", length = 100)
   @Field(index = Index.TOKENIZED, store = Store.NO)
   private String secondName;
   @Column(name = "middle_name", length = 100)
   @Field(index = Index.TOKENIZED, store = Store.NO)
   private String middleName;

   // ***********************************************************************
   // ************************* (getters+setters) ***************************
   // ***********************************************************************

   public String getFistName() {
      return fistName;
   }

   public void setFistName(final String fistName) {
      this.fistName = fistName;
   }

   public String getSecondName() {
      return secondName;
   }

   public void setSecondName(final String secondName) {
      this.secondName = secondName;
   }

   public String getMiddleName() {
      return middleName;
   }

   public void setMiddleName(final String middleName) {
      this.middleName = middleName;
   }

   @Override
   public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
   }

   @Override
   public boolean equals(final Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
   }
}
