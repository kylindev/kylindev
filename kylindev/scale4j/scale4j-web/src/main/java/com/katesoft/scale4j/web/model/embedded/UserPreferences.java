package com.katesoft.scale4j.web.model.embedded;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import org.hibernate.envers.Audited;

import com.katesoft.scale4j.web.model.UserLocale;

/**
 * This class can be used for storing user preferences.
 * 
 * @author kate2007
 */
@Embeddable
@Audited
@Access(AccessType.FIELD)
public class UserPreferences implements Serializable {
   private static final long serialVersionUID = 7635968798687496177L;
   // ***********************************************************************
   // ************************ persistent fields ****************************
   // ***********************************************************************
   @ManyToOne
   private UserLocale locale;

   // ***********************************************************************
   // ************************* (getters+setters) ***************************
   // ***********************************************************************

   public UserLocale getLocale() {
      return locale;
   }

   public void setLocale(final UserLocale locale) {
      this.locale = locale;
   }
}
