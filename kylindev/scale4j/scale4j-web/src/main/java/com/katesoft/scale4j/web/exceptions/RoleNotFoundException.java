package com.katesoft.scale4j.web.exceptions;

import org.springframework.security.core.AuthenticationException;

/** @author kate2007 */
public class RoleNotFoundException extends AuthenticationException {
   private static final long serialVersionUID = -2466521574208292351L;

   public RoleNotFoundException(final String role) {
      super(String.format("role = %s not found", role));
   }
}
