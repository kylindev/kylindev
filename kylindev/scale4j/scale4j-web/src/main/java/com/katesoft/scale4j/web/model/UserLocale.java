package com.katesoft.scale4j.web.model;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Immutable;
import org.hibernate.envers.Audited;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

/**
 * This class can be use for storing supported locales into DB.
 * 
 * @author kate2007
 */
@Entity
@Table(name = "t_locale")
@Immutable
@Audited
@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
@Access(AccessType.FIELD)
public class UserLocale extends AbstractPersistentEntity {
   private static final long serialVersionUID = 8477584014345610585L;
   // properties
   public static final String PROP_CODE = "code";
   public static final String PROP_LOCALE = "locale";
   // ***********************************************************************
   // ************************ persistent fields ****************************
   // ***********************************************************************
   @Column(name = "locale", length = 100, updatable = true, unique = true)
   private String locale;
   @Column(name = "code", length = 3, updatable = true, unique = true)
   private String code;

   // ***********************************************************************
   // ************************* (getters+setters) ***************************
   // ***********************************************************************
   public String getCode() {
      return code;
   }

   public void setCode(final String code) {
      this.code = code;
   }

   public String getLocale() {
      return locale;
   }

   public void setLocale(final String locale) {
      this.locale = locale;
   }
}
