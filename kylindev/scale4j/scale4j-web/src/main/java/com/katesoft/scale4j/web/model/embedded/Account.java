package com.katesoft.scale4j.web.model.embedded;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embeddable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.envers.Audited;

/**
 * Classical implementation of Account entity.
 * 
 * @author kate2007
 */
@Embeddable
@Audited
@Access(AccessType.FIELD)
public class Account implements Serializable {
   private static final long serialVersionUID = -4121699368779692827L;
   // ***********************************************************************
   // ************************ persistent fields ****************************
   // ***********************************************************************
   // core spring security properties.
   @Column(name = "account_enabled")
   private boolean enabled = true;
   @Column(name = "account_expired", nullable = false)
   private boolean accountExpired = false;
   @Column(name = "account_locked", nullable = false)
   private boolean accountLocked = false;
   @Column(name = "credentials_expired", nullable = false)
   private boolean credentialsExpired = false;

   // ***********************************************************************
   // ************************* (getters+setters) ***************************
   // ***********************************************************************

   public boolean isEnabled() {
      return enabled;
   }

   public boolean isAccountExpired() {
      return accountExpired;
   }

   public boolean isAccountLocked() {
      return accountLocked;
   }

   public boolean isCredentialsExpired() {
      return credentialsExpired;
   }

   public void setEnabled(final boolean enabled) {
      this.enabled = enabled;
   }

   public void setAccountExpired(final boolean accountExpired) {
      this.accountExpired = accountExpired;
   }

   public void setAccountLocked(final boolean accountLocked) {
      this.accountLocked = accountLocked;
   }

   public void setCredentialsExpired(final boolean credentialsExpired) {
      this.credentialsExpired = credentialsExpired;
   }

   @Override
   public int hashCode() {
      return HashCodeBuilder.reflectionHashCode(this);
   }

   @Override
   public boolean equals(final Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
   }
}
