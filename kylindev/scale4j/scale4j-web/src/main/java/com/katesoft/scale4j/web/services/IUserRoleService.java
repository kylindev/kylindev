package com.katesoft.scale4j.web.services;

import com.katesoft.scale4j.web.model.Role;
import com.katesoft.scale4j.web.model.User;
import com.katesoft.scale4j.web.model.UserLocale;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Collection;

/** @author kate2007 */
public interface IUserRoleService extends UserDetailsService {
   /**
    * generates a new password for user by given username.
    * 
    * @param userName
    *           user details.
    * @return new password
    */
   String requestNewPassword(String userName);

   /**
    * updates user account with given new password.
    * 
    * @param newPassword
    *           new generated password.
    * @param username
    *           login of user
    */
   void updatePassword(String newPassword, String username);

   /**
    * looking for user preferred locale.
    * 
    * @param username
    *           login of user.
    * @return locale for the given user.
    */
   UserLocale getUserPreferableLocale(String username);

   /**
    * updates user's preferable locale by locale name.
    * 
    * @param newLocale
    *           code of new locale.
    * @param username
    *           login of user
    */
   void updateUserPreferableLocale(String newLocale, String username);

   /** @return collection of locales, that are supported by system. */
   Collection<UserLocale> findAllLocales();

   /** @return collection of supported roles. */
   Collection<Role> findAllRoles();

   /**
    * looking for users by query string.
    * 
    * @param queryString
    *           search
    * @return collection of users or empty collection if nothing found.
    */
   Collection<User> search(String queryString);
}
