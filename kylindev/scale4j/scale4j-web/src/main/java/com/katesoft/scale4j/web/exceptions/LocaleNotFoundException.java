package com.katesoft.scale4j.web.exceptions;

/** @author kate2007 */
public class LocaleNotFoundException extends RuntimeException {
   private static final long serialVersionUID = -6099587474634812356L;

   public LocaleNotFoundException(String localeCode) {
      super(String.format("UserLocale = %s not found", localeCode));
   }
}
