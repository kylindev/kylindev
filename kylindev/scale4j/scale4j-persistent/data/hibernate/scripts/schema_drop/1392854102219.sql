alter table t_test_entity2 drop constraint FK2F8697ED2AB1DDC3
alter table t_test_entity2_aud drop constraint FK599B835EDF74E053
alter table t_test_entity_aud drop constraint FK9FDFB456DF74E053
drop table revinfo if exists
drop table t_datasource if exists
drop table t_internal_entity if exists
drop table t_test_entity if exists
drop table t_test_entity2 if exists
drop table t_test_entity2_aud if exists
drop table t_test_entity3 if exists
drop table t_test_entity_aud if exists