package com.katesoft.scale4j.persistent.config;

import java.util.Properties;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import com.atomikos.icatch.config.UserTransactionServiceImp;
import com.katesoft.scale4j.common.lang.RuntimeUtility;

/**
 * Add some local default configurations.(no additional logic)
 * 
 * @author kate2007
 */
public class UserTransactionService extends UserTransactionServiceImp implements InitializingBean,
         DisposableBean {
   private static final long serialVersionUID = 2408118023675919671L;

   /**
    * add output_dir location to DATA_DIR(this property can be injected via System parameters)
    * 
    * @param properties
    *           client properties
    */
   public UserTransactionService(final Properties properties) {
      super(new Properties() {
         private static final long serialVersionUID = -7414293374075674227L;

         {
            if (!properties.contains("com.atomikos.icatch.output_dir")) {
               put("com.atomikos.icatch.output_dir", RuntimeUtility.TM_DATA_DIR);
            }
            if (!properties.contains("com.atomikos.icatch.log_base_dir")) {
               put("com.atomikos.icatch.log_base_dir", RuntimeUtility.TM_DATA_DIR);
            }
            putAll(properties);
         }
      });
   }

   @Override
   public void afterPropertiesSet() {
      init();
   }

   @Override
   public void destroy() {
      super.shutdownForce();
   }
}
