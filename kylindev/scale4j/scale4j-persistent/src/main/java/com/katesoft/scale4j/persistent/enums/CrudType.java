package com.katesoft.scale4j.persistent.enums;

import org.hibernate.envers.RevisionType;

import com.katesoft.scale4j.persistent.exceptions.UnknownEnumValueException;

/**
 * Type of operation(CREATE, UPDATE, DELETE).
 * 
 * @author kate2007.
 */
public enum CrudType {
   CREATE,
   UPDATE,
   DELETE;

   public static CrudType from(RevisionType type) {
      if (type == null) {
         return null;
      } else if (type == RevisionType.ADD) {
         return CREATE;
      } else if (type == RevisionType.DEL) {
         return DELETE;
      } else if (type == RevisionType.MOD) {
         return UPDATE;
      }
      throw new UnknownEnumValueException(CrudType.class, type.name());
   }
}
