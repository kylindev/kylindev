package com.katesoft.scale4j.persistent.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.dbcp.BasicDataSource;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

/**
 * utility class, allows to store user's datasource connection details into database.
 * 
 * @author kate2007
 */
@Entity
@Table(name = "t_datasource")
public class UserDatasourceEntity extends AbstractPersistentEntity {
   private static final long serialVersionUID = 6311396678696207328L;
   // ***********************************************************************
   // ************************ persistent fields ****************************
   // ***********************************************************************
   @Column(name = "user_name", nullable = true)
   private String username;
   @Column(name = "user_password", nullable = true)
   private String password;
   @Column(name = "driver_classname", nullable = false)
   private String driverClassName;
   @Column(name = "url", nullable = false)
   private String url;
   @Column(name = "initial_size")
   private int initialSize;
   @Column(name = "max_active")
   private int maxActive;

   // ***********************************************************************
   // ************************* (getters+setters) ***************************
   // ***********************************************************************

   public String getUsername() {
      return username;
   }

   public void setUsername(final String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(final String password) {
      this.password = password;
   }

   public String getDriverClassName() {
      return driverClassName;
   }

   public void setDriverClassName(final String driverClassName) {
      this.driverClassName = driverClassName;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(final String url) {
      this.url = url;
   }

   public int getInitialSize() {
      return initialSize;
   }

   public void setInitialSize(final int initialSize) {
      this.initialSize = initialSize;
   }

   public int getMaxActive() {
      return maxActive;
   }

   public void setMaxActive(final int maxActive) {
      this.maxActive = maxActive;
   }

   public static UserDatasourceEntity from(final BasicDataSource datasource) {
      String username = datasource.getUsername();
      String password = datasource.getPassword();
      String driverClassName = datasource.getDriverClassName();
      String url = datasource.getUrl();
      int initialSize = datasource.getInitialSize();
      int maxActive = datasource.getMaxActive();
      //
      UserDatasourceEntity userDatasource = new UserDatasourceEntity();
      userDatasource.setUsername(username);
      userDatasource.setPassword(password);
      userDatasource.setDriverClassName(driverClassName);
      userDatasource.setUrl(url);
      userDatasource.setInitialSize(initialSize);
      userDatasource.setMaxActive(maxActive);
      //
      return userDatasource;
   }
}
