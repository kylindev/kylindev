package com.katesoft.scale4j.persistent.hibernate;

import java.util.List;
import java.util.Properties;

/**
 * Contains common methods(properties) which are needed for session factory.
 * 
 * @author kate2007
 */
public interface ICommonHibernateConfiguration {
   void setMappingResources(List<?> list);

   @SuppressWarnings("rawtypes")
   void setAnnotatedClasses(List<Class> list);

   void setConfigLocations(List<?> list);

   void setHibernateProperties(Properties properties);
}
