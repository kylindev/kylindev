package com.katesoft.scale4j.persistent.model;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;

import com.katesoft.scale4j.common.model.IPrettyPrintableObject;

/**
 * Internal class that used for meta data information storing.
 * 
 * @author kate2007
 */
@Entity
@Table(name = "t_internal_entity")
@Access(value = AccessType.FIELD)
public class InternalDomainEntity implements IPrettyPrintableObject, Serializable {
   private static final long serialVersionUID = 2505432001831984865L;
   @Id
   private String entityName;
   @Column(updatable = false)
   private String entityClass;
   @Column(updatable = false, nullable = false)
   private String tableName;

   public void setEntityName(String entityName) {
      this.entityName = entityName;
   }

   public void setEntityClass(String entityClass) {
      this.entityClass = entityClass;
   }

   public void setTableName(String tableName) {
      this.tableName = tableName;
   }

   public String getEntityName() {
      return entityName;
   }

   public String getEntityClass() {
      return entityClass;
   }

   public String getTableName() {
      return tableName;
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.toString(this);
   }
}
