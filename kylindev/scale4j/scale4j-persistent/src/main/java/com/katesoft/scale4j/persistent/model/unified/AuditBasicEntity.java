package com.katesoft.scale4j.persistent.model.unified;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Access(AccessType.FIELD)
public abstract class AuditBasicEntity extends BO {

	private static final long serialVersionUID = 2647558113380380989L;

	@Column(name = "createDate", nullable = true)
	private Date createDate;
	
	@Column(name = "createUser", nullable = true)
	private long createUser;
	
	@Column(name = "updateDate", nullable = true)
	private Date updateDate;
	
	@Column(name = "updateUser", nullable = true)
	private long updateUser;

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public long getCreateUser() {
		return createUser;
	}

	public void setCreateUser(long createUser) {
		this.createUser = createUser;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public long getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(long updateUser) {
		this.updateUser = updateUser;
	}
	
	

}
