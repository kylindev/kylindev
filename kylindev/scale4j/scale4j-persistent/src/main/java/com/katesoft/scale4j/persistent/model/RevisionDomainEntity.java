package com.katesoft.scale4j.persistent.model;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import com.katesoft.scale4j.common.model.IHasUniqueIdentifier;
import com.katesoft.scale4j.common.model.IPrettyPrintableObject;

/**
 * This is internal class created for hibernate envers integration.
 * 
 * @author kate2007
 */
@RevisionEntity
@Entity
@Table(name = "t_revision")
@Access(value = AccessType.PROPERTY)
public class RevisionDomainEntity implements IPrettyPrintableObject, IHasUniqueIdentifier<Long>,
         Serializable {
   private static final long serialVersionUID = -4207222329393887958L;
   private Long uniqueIdentifier;
   private long timestamp;

   @Override
   @Id
   @RevisionNumber
   @Column(name = "rev", unique = true, updatable = false)
   public Long getUniqueIdentifier() {
      return uniqueIdentifier;
   }

   public void setUniqueIdentifier(long uniqueIdentifier) {
      this.uniqueIdentifier = uniqueIdentifier;
   }

   @Column(name = "rev_timestamp")
   @RevisionTimestamp
   public long getTimestamp() {
      return timestamp;
   }

   public void setTimestamp(long timestamp) {
      this.timestamp = timestamp;
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.toString(this);
   }
}
