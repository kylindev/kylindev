package com.katesoft.scale4j.persistent.model.unified;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.hibernate.search.annotations.DocumentId;

/**
 * abstract parent for all domain classes defined in jvmcluster module.
 * <p/>
 * Clients should use this class instead of inheritance from BO.
 * 
 * @author kate2007
 */
@MappedSuperclass
@Access(AccessType.FIELD)
public abstract class AbstractPersistentEntity extends AuditBasicEntity {
   private static final long serialVersionUID = -3859469873970313518L;

   @DocumentId
   @Id
   @Column(name = "unique_identifier", nullable = false, updatable = false, unique = true)
   private Long uniqueIdentifier;

   /**
    * set unique identifier of entity. NO need to populate this field manually - there is already
    * distributed id generator that will populate this field.
    * 
    * @param uniqueIdentifier
    *           newValue
    */
   @Override
   public void setUniqueIdentifier(Long uniqueIdentifier) {
      this.uniqueIdentifier = uniqueIdentifier;
   }

   @Override
   public Long getUniqueIdentifier() {
      return uniqueIdentifier;
   }
}
