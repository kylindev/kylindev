package com.katesoft.scale4j.persistent.spring;

import java.util.Properties;

import org.hibernate.cache.CacheDataDescription;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.CollectionRegion;
import org.hibernate.cache.EntityRegion;
import org.hibernate.cache.QueryResultsRegion;
import org.hibernate.cache.RegionFactory;
import org.hibernate.cache.TimestampsRegion;
import org.hibernate.cache.access.AccessType;
import org.hibernate.cfg.Settings;

/**
 * @author kate2007
 */
public class CacheFactoryProxy implements RegionFactory {
   RegionFactory proxy;

   public CacheFactoryProxy() {
      ThreadLocal<Object> factoryHolder = LocalAnnotationEntityManagerFactory
               .getConfigTimeRegionFactoryHolder();
      proxy = (RegionFactory) factoryHolder.get();
   }

   /**
    * Properties constructor: not used by this class or formally required,.
    * <p/>
    * But enforced by Hibernate when reflectively instantiating a RegionFactory.
    */
   public CacheFactoryProxy(@SuppressWarnings("unused") Properties properties) {
      this();
   }

   @Override
   public void start(Settings settings, Properties properties) throws CacheException {
      proxy.start(settings, properties);
   }

   @Override
   public void stop() {
      proxy.stop();
   }

   @Override
   public boolean isMinimalPutsEnabledByDefault() {
      return proxy.isMinimalPutsEnabledByDefault();
   }

   @Override
   public AccessType getDefaultAccessType() {
      return proxy.getDefaultAccessType();
   }

   @Override
   public long nextTimestamp() {
      return proxy.nextTimestamp();
   }

   @Override
   public EntityRegion buildEntityRegion(String regionName, Properties properties,
            CacheDataDescription metadata) throws CacheException {
      return proxy.buildEntityRegion(regionName, properties, metadata);
   }

   @Override
   public CollectionRegion buildCollectionRegion(String regionName, Properties properties,
            CacheDataDescription metadata) throws CacheException {
      return proxy.buildCollectionRegion(regionName, properties, metadata);
   }

   @Override
   public QueryResultsRegion buildQueryResultsRegion(String regionName, Properties properties)
            throws CacheException {
      return proxy.buildQueryResultsRegion(regionName, properties);
   }

   @Override
   public TimestampsRegion buildTimestampsRegion(String regionName, Properties properties)
            throws CacheException {
      return proxy.buildTimestampsRegion(regionName, properties);
   }
}
