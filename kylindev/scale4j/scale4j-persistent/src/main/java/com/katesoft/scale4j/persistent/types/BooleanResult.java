package com.katesoft.scale4j.persistent.types;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.springframework.dao.DataAccessResourceFailureException;

import com.katesoft.scale4j.common.annotation.ValueObject;
import com.katesoft.scale4j.common.model.IPrettyPrintableObject;

/**
 * This is boolean wrapper class(for hibernate usage purposes).
 * <p/>
 * Hibernate does not provide Boolean Wrapper, so created this boolean container bean.
 * 
 * @author kate2007
 */
@ValueObject
public class BooleanResult implements IPrettyPrintableObject {
   private Boolean boolvalue;

   public boolean getBoolvalue() {
      if (boolvalue == null) {
         throw new DataAccessResourceFailureException("internal boolvalue is not populated yet");
      }
      return boolvalue;
   }

   public void setBoolvalue(final Boolean boolValue) {
      boolvalue = boolValue;
   }

   @Override
   public boolean equals(Object obj) {
      return EqualsBuilder.reflectionEquals(this, obj);
   }

   @Override
   public int hashCode() {
      return new HashCodeBuilder().append(boolvalue).toHashCode();
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.reflectionToString(this);
   }
}
