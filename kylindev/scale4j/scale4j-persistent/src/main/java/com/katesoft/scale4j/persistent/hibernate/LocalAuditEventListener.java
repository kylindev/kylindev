package com.katesoft.scale4j.persistent.hibernate;

import net.jcip.annotations.ThreadSafe;

import org.hibernate.envers.event.AuditEventListener;
import org.hibernate.event.PostDeleteEvent;
import org.hibernate.event.PostInsertEvent;
import org.hibernate.event.PostUpdateEvent;

import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;

/**
 * internal class, add some trace output.
 * 
 * @author kate2007
 */
@ThreadSafe
public class LocalAuditEventListener extends AuditEventListener {
   private static final long serialVersionUID = -6781563338786525253L;
   private final Logger logger = LogFactory.getLogger(getClass());

   @Override
   public void onPostInsert(PostInsertEvent event) {
      logger.trace("onPostInsert %s", event.getEntity());
      super.onPostInsert(event);
   }

   @Override
   public void onPostUpdate(PostUpdateEvent event) {
      logger.trace("onPostUpdate %s", event.getEntity());
      super.onPostUpdate(event);
   }

   @Override
   public void onPostDelete(PostDeleteEvent event) {
      logger.trace("onPostDelete %s", event.getEntity());
      super.onPostDelete(event);
   }
}
