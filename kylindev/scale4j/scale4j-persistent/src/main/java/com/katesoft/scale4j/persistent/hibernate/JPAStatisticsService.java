package com.katesoft.scale4j.persistent.hibernate;

import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.hibernate.jmx.StatisticsService;

/**
 * @author kate2007
 */
public class JPAStatisticsService extends StatisticsService {
   private HibernateEntityManagerFactory entityManagerFactory;

   /**
    * extract session factory object and pass session factory reference into super class.
    * 
    * @param object
    *           factory
    */
   public void setSessionFactoryHolder(Object object) {
      if (object instanceof HibernateEntityManagerFactory) {
         this.entityManagerFactory = (HibernateEntityManagerFactory) object;
         setSessionFactory(entityManagerFactory.getSessionFactory());
      } else {
         setSessionFactory((SessionFactory) object);
      }
   }

   /**
    * @return entity manager factory if this object was created using hibernate entity manager
    *         factory.
    *         <p/>
    *         Otherwise <code>null</code>
    */
   public HibernateEntityManagerFactory getEntityManagerFactory() {
      return entityManagerFactory;
   }
}
