package com.katesoft.scale4j.persistent.types;

import java.io.Serializable;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.envers.DefaultRevisionEntity;

import com.katesoft.scale4j.common.annotation.ValueObject;
import com.katesoft.scale4j.common.model.IPrettyPrintableObject;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.RevisionDomainEntity;

/**
 * Contains information about domain entity at specific revision(crud type, entity, revision).
 * 
 * @author kate2007
 */
@ValueObject
public class RevisionData<T> implements IPrettyPrintableObject, Serializable {
   private static final long serialVersionUID = -5933099718538205464L;

   private final T entityAtRevision;
   private final Object revisionEntity;
   private final CrudType revisionType;

   public RevisionData(final T entityAtRevision, final Object revisionEntity,
            final CrudType revisionType) {
      this.entityAtRevision = entityAtRevision;
      this.revisionEntity = revisionEntity;
      this.revisionType = revisionType;
   }

   public T getEntityAtRevision() {
      return entityAtRevision;
   }

   public long getRevisionNumber() {
      return (revisionEntity instanceof DefaultRevisionEntity) ? ((DefaultRevisionEntity) revisionEntity)
               .getId() : ((RevisionDomainEntity) revisionEntity).getUniqueIdentifier();
   }

   public long getRevisionTimestamp() {
      return (revisionEntity instanceof DefaultRevisionEntity) ? ((DefaultRevisionEntity) revisionEntity)
               .getTimestamp() : ((RevisionDomainEntity) revisionEntity).getTimestamp();
   }

   public CrudType getRevisionType() {
      return revisionType;
   }

   @Override
   public String reflectionToString() {
      return ReflectionToStringBuilder.reflectionToString(this);
   }
}
