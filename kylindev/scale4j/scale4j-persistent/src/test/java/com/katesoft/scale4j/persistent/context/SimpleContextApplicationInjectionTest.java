package com.katesoft.scale4j.persistent.context;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

/**
 * @author kate2007
 */
public class SimpleContextApplicationInjectionTest extends AbstractSimpleContextPersistentTest {
   @Test
   public void hibernateTransactionManagerInjected() {
      assertThat(hibernateTransactionManager, is(notNullValue()));
   }

   @Test
   public void dataSourceTransactionManagerInjected() {
      assertThat(dataSourceTransactionManager, is(notNullValue()));
   }
}
