package com.katesoft.scale4j.persistent.context;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:META-INF/spring/jvmcluster$xa-persistentContext.xml",
         "classpath*:META-INF/spring/jvmcluster.testing$postprocessing.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
@DirtiesContext
public abstract class AbstractXAContextPersistentTest extends AbstractPersistentTest {
}
