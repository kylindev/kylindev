package com.katesoft.scale4j.persistent.model.unified;

import com.katesoft.scale4j.persistent.model.TestEntity;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class BOTest {
   @Test
   public void cleanPropertiesCleaned() {
      TestEntity entity = new TestEntity();
      entity.setOneField("one_two");
      entity.cleanBeanProperties();
      assertThat(entity.getUniqueIdentifier(), is(Matchers.<Object> nullValue()));
      assertThat(entity.getVersion(), is(0L));
      assertThat(entity.getGlobalUniqueIdentifier(), is(Matchers.<Object> nullValue()));
      assertThat(entity.getOneField(), is(Matchers.<Object> nullValue()));
   }

   @Test
   public void entityCloned() throws CloneNotSupportedException {
      TestEntity entity = new TestEntity();
      entity.setOneField("clone_field");
      TestEntity entity2 = (TestEntity) entity.clone();
      assertThat(entity.getOneField(), is(entity2.getOneField()));
      assertThat(entity, is(entity2));
      assertThat(entity, is(equalTo(entity2)));
   }
}
