package com.katesoft.scale4j.persistent.context;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.orm.hibernate3.HibernateAccessor;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.katesoft.scale4j.common.RuntimeMode;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.log.LogFactory;
import com.katesoft.scale4j.log.Logger;
import com.katesoft.scale4j.persistent.client.LocalHibernateDaoSupport;
import com.katesoft.scale4j.persistent.client.LocalHibernateTemplate;
import com.katesoft.scale4j.persistent.spring.EntityManagedOrNativeHibernateFactoryBean;
import com.katesoft.scale4j.persistent.spring.ILocalHibernateSessionBuilder;

/**
 * This is parent class for spring integration junit testing.
 * <p/>
 * this class does not define spring config locations, but autowiring the most common beans.
 * 
 * @author kate2007
 */
public abstract class AbstractPersistentTest implements ApplicationContextAware {
   protected final Logger logger = LogFactory.getLogger(getClass());
   protected DataSource dataSource;
   protected JdbcTemplate jdbcTemplate;
   protected LocalHibernateTemplate hibernateTemplate;
   protected TransactionTemplate transactionTemplate;
   protected DataSourceTransactionManager dataSourceTransactionManager;
   protected LocalHibernateDaoSupport hibernateDaoSupport;
   protected SessionFactory sessionFactory;
   protected ApplicationContext applicationContext;
   protected ILocalHibernateSessionBuilder sessionFactoryOrEntityManagerFactory;

   @BeforeClass
   public static void beforeClass() {
      System.setProperty(RuntimeUtility.VAR_RUNTIME_MODE, RuntimeMode.JUNIT.toString());
   }

   @Before
   public void setUp() throws Exception {
      EntityManagedOrNativeHibernateFactoryBean bean = (EntityManagedOrNativeHibernateFactoryBean) applicationContext
               .getBean("&" + IBeanNameReferences.SESSION_FACTORY);
      if (bean.getObject() instanceof HibernateEntityManagerFactory) {
         sessionFactory = ((HibernateEntityManagerFactory) bean.getObject()).getSessionFactory();
      } else {
         sessionFactory = (SessionFactory) bean.getObject();
      }
      sessionFactoryOrEntityManagerFactory = bean.getActualFactoryBuilderBean();
   }

   @After
   public void tearDown() throws Exception {
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.ACTUAL_DATASOURCE)
   public void setDataSource(final DataSource dataSource) {
      this.dataSource = dataSource;
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.JDBC_TEMPLATE)
   public void setJdbcTemplate(final JdbcTemplate jdbcTemplate) {
      this.jdbcTemplate = jdbcTemplate;
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.TRANSACTION_TEMPLATE)
   public void setTransactionTemplate(TransactionTemplate transactionTemplate) {
      this.transactionTemplate = transactionTemplate;
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.HIBERNATE_TEMPLATE)
   public void setHibernateTemplate(final LocalHibernateTemplate hibernateTemplate) {
      this.hibernateTemplate = hibernateTemplate;
      this.hibernateTemplate.setFlushMode(HibernateAccessor.FLUSH_EAGER);
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.HIBERNATE_DAO_SUPPORT)
   public void setHibernateDaoSupport(LocalHibernateDaoSupport hibernateDaoSupport) {
      this.hibernateDaoSupport = hibernateDaoSupport;
   }

   @Autowired
   @Required
   @Qualifier(value = IBeanNameReferences.DATASOURCE_TRANSACTION_MANAGER)
   public void setDataSourceTransactionManager(
            DataSourceTransactionManager dataSourceTransactionManager) {
      this.dataSourceTransactionManager = dataSourceTransactionManager;
   }

   protected <T> void doInNewTransaction(final HibernateCallback<T> e) {
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.execute(e);
         }
      });
   }

   @Override
   public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
      this.applicationContext = applicationContext;
   }
}
