package com.katesoft.scale4j.persistent.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.math.BigDecimal;
import java.sql.SQLException;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
public class TransactionTemplateTransactionalTest extends AbstractSimpleContextPersistentTest {
   @Test
   public void transactionCommitted() {
      final TestEntity testEntity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.save(testEntity);
            return null;
         }
      });
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            ((TestEntity) session.load(TestEntity.class, testEntity.getUniqueIdentifier()))
                     .getCrudType();
            return null;
         }
      });
   }

   @Test
   public void transactionRolledBack() {
      final TestEntity testEntity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(@SuppressWarnings("unused") Session session)
                  throws HibernateException, SQLException {
            hibernateTemplate.deleteAll(TestEntity.class);
            return null;
         }
      });
      try {
         doInNewTransaction(new HibernateCallback<Object>() {
            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
               session.save(testEntity);
               session.save(new BigDecimal("123"));
               return null;
            }
         });
      } catch (Exception e) {
      }
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            assertThat(session.createCriteria(TestEntity.class).list().size(), is(0));
            return null;
         }
      });
   }
}
