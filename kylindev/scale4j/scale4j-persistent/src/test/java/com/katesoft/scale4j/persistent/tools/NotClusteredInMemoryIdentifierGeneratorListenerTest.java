package com.katesoft.scale4j.persistent.tools;

import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.model.unified.IBO;
import org.hamcrest.MatcherAssert;
import org.hamcrest.Matchers;
import org.hibernate.event.EventSource;
import org.hibernate.event.SaveOrUpdateEvent;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * @author kate2007
 */
public class NotClusteredInMemoryIdentifierGeneratorListenerTest {
   @Test
   public void GeneratedIdsAreValid() throws InterruptedException {
      NotClusteredInMemoryIdentifierGeneratorListener identifierGenerator1 = new NotClusteredInMemoryIdentifierGeneratorListener();
      Thread.sleep(10);
      NotClusteredInMemoryIdentifierGeneratorListener identifierGenerator2 = new NotClusteredInMemoryIdentifierGeneratorListener();
      SaveOrUpdateEvent saveOrUpdateEvent1 = new SaveOrUpdateEvent(new TestEntity(),
               Mockito.mock(EventSource.class));
      SaveOrUpdateEvent saveOrUpdateEvent2 = new SaveOrUpdateEvent(new TestEntity(),
               Mockito.mock(EventSource.class));
      identifierGenerator1.onSaveOrUpdate(saveOrUpdateEvent1);
      identifierGenerator2.onSaveOrUpdate(saveOrUpdateEvent2);
      MatcherAssert.assertThat(((IBO) saveOrUpdateEvent2.getObject()).getUniqueIdentifier(),
               Matchers.greaterThan(((IBO) saveOrUpdateEvent1.getObject()).getUniqueIdentifier()));
   }
}
