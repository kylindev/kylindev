package com.katesoft.scale4j.persistent.spring;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import com.katesoft.scale4j.persistent.client.LocalHibernateTemplate;
import com.katesoft.scale4j.persistent.model.UserDatasourceEntity;

/**
 * @author kate2007
 */
public class LocalEntityManagerHibernateTemplateTest extends AbstractEntityManagerTest {
   private LocalHibernateTemplate localHibernateTemplate;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      localHibernateTemplate = new LocalHibernateTemplate(
               (HibernateEntityManagerFactory) factoryBean.getObject());
   }

   @Test
   public void localHibernateTemplateInitialized() {
      localHibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.flush();
            return null;
         }
      });
   }

   @Test
   public void ableToSaveEntityAndDeleteLater() {
      TransactionTemplate transactionTemplate = new TransactionTemplate(jpaTransactionManager);
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            UserDatasourceEntity userDatasourceEntity = UserDatasourceEntity.from(dataSource);
            userDatasourceEntity.setUniqueIdentifier(1L);
            localHibernateTemplate.save(userDatasourceEntity);
            localHibernateTemplate.flush();
            localHibernateTemplate.clear();
            assertThat(localHibernateTemplate.get(UserDatasourceEntity.class, 1L),
                     is(Matchers.<Object> notNullValue()));
            localHibernateTemplate.deleteAll(UserDatasourceEntity.class);
         }
      });
   }
}
