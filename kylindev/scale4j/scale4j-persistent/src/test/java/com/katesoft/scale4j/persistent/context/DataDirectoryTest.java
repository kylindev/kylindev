package com.katesoft.scale4j.persistent.context;

import static com.katesoft.scale4j.common.io.FileUtility.SEPARATOR;
import static com.katesoft.scale4j.common.lang.RuntimeUtility.TM_DATA_DIR;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

import java.io.File;

import org.junit.Test;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.common.io.FileUtility;
import com.katesoft.scale4j.common.lang.RuntimeUtility;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
public class DataDirectoryTest extends AbstractXAContextPersistentTest {
   @Test
   public void hibernateCreateFileCreated() {
      File file = new File(RuntimeUtility.HIBERNATE_DATA_DIR + SEPARATOR + "scripts" + SEPARATOR
               + "schema_create");
      File[] files = file.listFiles();
      assertThat(files.length, greaterThan(0));
   }

   @Test
   public void hibernateUpdateFileCreated() {
      File file = new File(RuntimeUtility.HIBERNATE_DATA_DIR + SEPARATOR + "scripts" + SEPARATOR
               + "schema_update");
      File[] files = file.listFiles();
      assertThat(files.length, greaterThan(0));
   }

   @Test
   public void hibernateDropFileCreated() {
      File file = new File(RuntimeUtility.HIBERNATE_DATA_DIR + SEPARATOR + "scripts" + SEPARATOR
               + "schema_drop");
      File[] files = file.listFiles();
      assertThat(files.length, greaterThan(0));
   }

   @Test
   public void atomikosDirectoryCreated() {
      File file = new File((TM_DATA_DIR.endsWith(FileUtility.SEPARATOR) ? TM_DATA_DIR.substring(0,
               TM_DATA_DIR.length() - 1) : TM_DATA_DIR) + "-junit");
      System.out.println(file.getPath());
      File[] files = file.listFiles();
      assertThat(files.length, greaterThan(0));
   }

   @Test
   @Rollback(false)
   public void hibernateSearchDirectoryCreated() {
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            for (int i = 0; i <= 100; i++) {
               hibernateTemplate.save(TestEntity.newTestInstance());
            }
         }
      });
      File file = new File(RuntimeUtility.HIBERNATE_SEARCH_DATA_DIR);
      File[] files = file.listFiles();
      assertThat(files.length, greaterThan(0));
   }

   @Override
   public void tearDown() throws Exception {
      hibernateTemplate.deleteAll(TestEntity.class);
      super.tearDown();
   }
}
