package com.katesoft.scale4j.persistent.model;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import org.apache.commons.dbcp.BasicDataSource;
import org.junit.Test;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
@Transactional
public class UserDatasourceEntityTest extends AbstractSimpleContextPersistentTest {
   @Test
   public void store10RecordWorks() throws Exception {
      for (int i = 0; i <= 10; i++) {
         BasicDataSource actualDatasource = (BasicDataSource) super.dataSource;
         UserDatasourceEntity entity = UserDatasourceEntity.from(actualDatasource);
         entity.setUrl(entity.getUrl() + i);
         Serializable id = hibernateDaoSupport.getLocalHibernateTemplate().save(entity);
         hibernateDaoSupport.getLocalHibernateTemplate().flush();
         hibernateDaoSupport.getLocalHibernateTemplate().clear();
         UserDatasourceEntity e = hibernateDaoSupport.getLocalHibernateTemplate().get(
                  UserDatasourceEntity.class, id);
         assertThat(e.getInitialSize(), is(actualDatasource.getInitialSize()));
         assertThat(e.getMaxActive(), is(actualDatasource.getMaxActive()));
         assertThat(e.getUsername(), is(actualDatasource.getUsername()));
         assertThat(e.getDriverClassName(), is(actualDatasource.getDriverClassName()));
         assertThat(e.getPassword(), is(actualDatasource.getPassword()));
         assertThat(e.getUrl(), is(actualDatasource.getUrl() + i));
      }
   }
}
