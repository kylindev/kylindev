package com.katesoft.scale4j.persistent.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.types.RevisionData;

/**
 * @author kate2007
 */
public abstract class AbstractLocalHibernateDaoSupportTest extends AbstractPersistentTest {
   @SuppressWarnings({})
   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void entityRevisionUpdatedPerEntityCommit() throws Throwable {
      final TestEntity entity = new TestEntity();
      entity.setOneField("one_field_1");
      try {
         transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
               return hibernateTemplate.execute(new HibernateCallback<Object>() {
                  @Override
                  public Object doInHibernate(Session session) throws HibernateException,
                           SQLException {
                     session.save(entity);
                     session.flush();
                     //
                     return null;
                  }
               });
            }
         });
         List<Number> revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
                  entity.getUniqueIdentifier());
         assertThat(revisions.size(), is(1));
         TestEntity historical1 = hibernateDaoSupport.loadForRevision(TestEntity.class,
                  entity.getUniqueIdentifier(), revisions.iterator().next());
         hibernateDaoSupport.getLocalHibernateTemplate().clear();
         //
         transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
               return hibernateTemplate.execute(new HibernateCallback<Object>() {
                  @Override
                  public Object doInHibernate(Session session) throws HibernateException,
                           SQLException {
                     TestEntity e = ((TestEntity) session.get(TestEntity.class,
                              entity.getUniqueIdentifier()));
                     e.setOneField("one_field_2");
                     session.update(e);
                     session.flush();
                     //
                     return null;
                  }
               });
            }
         });
         assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
                  .getOneField(), is("one_field_2"));
         revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
                  entity.getUniqueIdentifier());
         assertThat(revisions.size(), is(2));
         TestEntity historical2 = hibernateDaoSupport.loadForRevision(TestEntity.class,
                  entity.getUniqueIdentifier(), revisions.iterator().next());
         logger.debug("loaded historical entity = %s", historical2.reflectionToString());
         assertThat("one_field_2", is(historical2.getOneField()));
         hibernateDaoSupport.getLocalHibernateTemplate().clear();
         //
         transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
               return hibernateTemplate.execute(new HibernateCallback<Object>() {
                  @Override
                  public Object doInHibernate(Session session) throws HibernateException,
                           SQLException {
                     TestEntity testEntity = (TestEntity) session.get(TestEntity.class,
                              entity.getUniqueIdentifier());
                     testEntity.setOneField("one_field_3");
                     session.update(testEntity);
                     session.flush();
                     //
                     return null;
                  }
               });
            }
         });
         revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
                  entity.getUniqueIdentifier());
         assertThat(revisions.size(), is(3));
         TestEntity historical3 = hibernateDaoSupport.loadForRevision(TestEntity.class,
                  entity.getUniqueIdentifier(), revisions.iterator().next());
         logger.debug("loaded historical entity = %s", historical3.reflectionToString());
         assertThat("one_field_3", is(historical3.getOneField()));
         hibernateDaoSupport.getLocalHibernateTemplate().clear();
         //
         List<RevisionData<TestEntity>> all = hibernateDaoSupport.loadAllRevisions(
                  TestEntity.class, entity.getUniqueIdentifier());
         logger.debug("revisions = %s", all);
         assertThat(all.size(), is(3));
         //
         RevisionData<TestEntity> r3 = all.get(0);
         RevisionData<TestEntity> r2 = all.get(1);
         RevisionData<TestEntity> r1 = all.get(2);
         //
         assertThat(r3.getRevisionType(), is(CrudType.UPDATE));
         assertThat(r2.getRevisionType(), is(CrudType.UPDATE));
         assertThat(r1.getRevisionType(), is(CrudType.CREATE));
         //
         assertThat(r3.getEntityAtRevision(), is(historical3));
         assertThat(r2.getEntityAtRevision(), is(historical2));
         assertThat(r1.getEntityAtRevision(), is(historical1));
         //
         assertThat(r3.getRevisionNumber(), is(revisions.get(0).longValue()));
         assertThat(r2.getRevisionNumber(), is(revisions.get(1).longValue()));
         assertThat(r1.getRevisionNumber(), is(revisions.get(2).longValue()));
         //
         assertThat(r3.getRevisionTimestamp(), lessThanOrEqualTo(System.currentTimeMillis()));
         assertThat(r2.getRevisionTimestamp(), lessThanOrEqualTo(System.currentTimeMillis()));
         assertThat(r1.getRevisionTimestamp(), lessThanOrEqualTo(System.currentTimeMillis()));
      } catch (Throwable e) {
         logger.error(e);
         throw e;
      } finally {
         hibernateDaoSupport.getLocalHibernateTemplate().delete(
                  hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier()));
      }
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void deletingCreatesHistoricalRow() {
      final TestEntity entity = new TestEntity();
      entity.setOneField("will_be_deleted_soon");
      //
      transactionTemplate.execute(new TransactionCallback<Object>() {
         @Override
         public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
            return hibernateTemplate.execute(new HibernateCallback<Object>() {
               @Override
               public Object doInHibernate(Session session) throws HibernateException, SQLException {
                  session.save(entity);
                  session.flush();
                  return null;
               }
            });
         }
      });
      transactionTemplate.execute(new TransactionCallback<Object>() {
         @Override
         public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
            return hibernateTemplate.execute(new HibernateCallback<Object>() {
               @Override
               public Object doInHibernate(Session session) throws HibernateException, SQLException {
                  session.delete(session.get(TestEntity.class, entity.getUniqueIdentifier()));
                  session.flush();
                  return null;
               }
            });
         }
      });
      List<Number> revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      assertThat(revisions.size(), is(2));
      List<RevisionData<TestEntity>> all = hibernateDaoSupport.loadAllRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      //
      RevisionData<TestEntity> r2 = all.get(0);
      RevisionData<TestEntity> r1 = all.get(1);
      //
      assertThat(r2.getRevisionType(), is(CrudType.DELETE));
      assertThat(r1.getRevisionType(), is(CrudType.CREATE));
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void latestVersionForDateFound() {
      final TestEntity entity = new TestEntity();
      final AtomicReference<Date> creationDate = new AtomicReference<Date>();
      try {
         transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(
                     @SuppressWarnings("unused") TransactionStatus status) {
               hibernateDaoSupport.getLocalHibernateTemplate().execute(
                        new HibernateCallback<Object>() {
                           @Override
                           public Object doInHibernate(Session session) throws HibernateException,
                                    SQLException {
                              session.save(entity);
                              session.flush();
                              return null;
                           }
                        });
            }
         });
         hibernateTemplate.execute(new HibernateCallback<Object>() {
            @Override
            public Object doInHibernate(Session session) throws HibernateException, SQLException {
               creationDate.set(entity.getCreationTimestamp(session));
               return null;
            }
         });
         RevisionData<TestEntity> revisionData = hibernateDaoSupport.latestRevisionForDate(
                  TestEntity.class, creationDate.get(), entity.getUniqueIdentifier(), null);
         assertThat(revisionData.getEntityAtRevision().getUniqueIdentifier(),
                  is(entity.getUniqueIdentifier()));
         revisionData = hibernateDaoSupport.latestRevisionForDate(TestEntity.class, new Date(
                  creationDate.get().getTime() + 10000), entity.getUniqueIdentifier(), null);
         assertThat(revisionData, is(Matchers.<Object> notNullValue()));
         revisionData = hibernateDaoSupport.latestRevisionForDate(TestEntity.class, new Date(
                  creationDate.get().getTime() - 10000), entity.getUniqueIdentifier(), null);
         assertThat(revisionData, is(Matchers.<Object> nullValue()));
      } finally {
         hibernateDaoSupport.getLocalHibernateTemplate().delete(
                  hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier()));
      }
   }

   @Test
   @Transactional(propagation = Propagation.NOT_SUPPORTED)
   public void entityHistorySaved() {
      final TestEntity entity = new TestEntity();
      entity.setOneField("one_field_1");
      try {
         logger.debug("starting transactional execution");
         transactionTemplate.execute(new TransactionCallback<Object>() {
            @Override
            public Object doInTransaction(@SuppressWarnings("unused") TransactionStatus status) {
               return hibernateDaoSupport.getLocalHibernateTemplate().execute(
                        new HibernateCallback<Object>() {
                           @Override
                           public Object doInHibernate(Session session) throws HibernateException,
                                    SQLException {
                              session.save(entity);
                              session.flush();
                              //
                              assertThat(entity.getVersion(), is(0L));
                              assertThat(entity.getUniqueIdentifier(),
                                       is(Matchers.<Object> notNullValue()));
                              assertThat(entity.getGlobalUniqueIdentifier(),
                                       is(Matchers.<Object> notNullValue()));
                              //
                              entity.setOneField("one_field_2");
                              session.update(entity);
                              session.flush();
                              //
                              assertThat(entity.getVersion(), is(1L));
                              //
                              entity.setOneField("one_field_3");
                              session.update(entity);
                              session.flush();
                              //
                              assertThat(entity.getVersion(), is(2L));
                              //
                              return null;
                           }
                        });
            }
         });
         Number latestVersion = hibernateDaoSupport.latestRevision(TestEntity.class,
                  entity.getUniqueIdentifier());
         assertThat(latestVersion, is(Matchers.<Object> notNullValue()));
         List<Number> revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
                  entity.getUniqueIdentifier());
         assertThat(revisions.isEmpty(), is(false));
         assertThat(revisions.size(), is(1));
         assertThat(revisions.iterator().next(), is(latestVersion));
      } finally {
         hibernateDaoSupport.getLocalHibernateTemplate().delete(
                  hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier()));
      }
   }
}
