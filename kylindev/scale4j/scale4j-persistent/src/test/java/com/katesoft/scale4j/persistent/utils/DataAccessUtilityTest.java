package com.katesoft.scale4j.persistent.utils;

import static com.katesoft.scale4j.persistent.utils.DataAccessUtility.fetchFirstRowOrNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Test;

import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
@SuppressWarnings({ "unchecked" })
public class DataAccessUtilityTest {
   @SuppressWarnings("rawtypes")
   @Test
   public void fetchFirstRowFromEmptyCollectionWillReturnNull() {
      final Collection users = new ArrayList();
      assertThat(fetchFirstRowOrNull(users), is(nullValue()));
   }

   @SuppressWarnings("rawtypes")
   @Test
   public void fetchFirstRowFromNotEmptyCollectionWillReturnFirstValue() {
      final Collection mocks = new ArrayList();
      mocks.add(mock(TestEntity.class));
      assertThat(fetchFirstRowOrNull(mocks), is(notNullValue()));
   }
}
