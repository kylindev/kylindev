package com.katesoft.scale4j.persistent.spring.postprocessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;

import java.util.List;

import javax.persistence.Table;

import org.hibernate.envers.DefaultRevisionEntity;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.katesoft.scale4j.common.model.IPrettyPrintableObject;
import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.model.InternalDomainEntity;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.model.TestEntity2;
import com.katesoft.scale4j.persistent.model.TestEntity3;
import com.katesoft.scale4j.persistent.model.UserDatasourceEntity;

/**
 * @author kate2007
 */
public class InternalDomainPostProcessorTest extends AbstractSimpleContextPersistentTest {
   @Autowired
   InternalDomainPostProcessor postProcessor;

   @SuppressWarnings("unchecked")
   @Test
   public void schemaMetaDataPersisted() throws ClassNotFoundException {
      List<InternalDomainEntity> entities = hibernateDaoSupport.getLocalHibernateTemplate()
               .loadAll(InternalDomainEntity.class);
      System.out.println("loaded = " + IPrettyPrintableObject.Printer.reflectionToString(entities));
      for (InternalDomainEntity entity : entities) {
         if (entity.getEntityClass() != null) {
            assertThat(
                     entity.getEntityClass(),
                     anyOf(is(InternalDomainEntity.class.getName()),
                              is(UserDatasourceEntity.class.getName()),
                              is(TestEntity.class.getName()), is(TestEntity2.class.getName()),
                              is(TestEntity3.class.getName()),
                              is(DefaultRevisionEntity.class.getName())));
            Table table = Class.forName(entity.getEntityClass()).getAnnotation(Table.class);
            if (table != null) {
               assertThat(entity.getTableName(), is(table.name()));
            }
         }
      }
   }

   @Test
   public void userDatasourceEntityMetaDataPersisted() {
      List<InternalDomainEntity> entities = hibernateDaoSupport.getLocalHibernateTemplate()
               .loadAll(InternalDomainEntity.class);
      boolean contains = false;
      for (InternalDomainEntity entity : entities) {
         if (entity.getEntityClass() != null) {
            if (entity.getEntityClass().equalsIgnoreCase(UserDatasourceEntity.class.getName())) {
               contains = true;
            }
         }
      }
      assertThat(contains, is(true));
   }

   @Test
   public void testEntityMetaDataPersisted() {
      List<InternalDomainEntity> entities = hibernateDaoSupport.getLocalHibernateTemplate()
               .loadAll(InternalDomainEntity.class);
      boolean contains = false;
      for (InternalDomainEntity entity : entities) {
         if (entity.getEntityClass() != null) {
            if (entity.getEntityClass().equalsIgnoreCase(TestEntity.class.getName())) {
               contains = true;
            }
         }
      }
      assertThat(contains, is(true));
   }

   @Test
   public void reprocessWorks() {
      postProcessor.postProcessAfterInitialization(hibernateTemplate.getSessionFactory(),
               IBeanNameReferences.SESSION_FACTORY);
      List<InternalDomainEntity> entities = hibernateDaoSupport.getLocalHibernateTemplate()
               .loadAll(InternalDomainEntity.class);
      assertThat(entities.isEmpty(), is(false));
   }
}
