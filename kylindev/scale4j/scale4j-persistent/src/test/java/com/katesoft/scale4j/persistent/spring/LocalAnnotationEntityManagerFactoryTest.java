package com.katesoft.scale4j.persistent.spring;

import com.katesoft.scale4j.persistent.model.UserDatasourceEntity;
import org.hibernate.ejb.HibernateEntityManagerFactory;
import org.hibernate.metadata.ClassMetadata;
import org.junit.Test;
import org.mockito.Matchers;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.verify;

/**
 * @author kate2007
 */
public class LocalAnnotationEntityManagerFactoryTest extends AbstractEntityManagerTest {
   @Test
   public void userDataSourceClassIsMapped() throws Exception {
      HibernateEntityManagerFactory factoryBeanObject = (HibernateEntityManagerFactory) factoryBean
               .getObject();
      Map<String, ClassMetadata> classMetadata = factoryBeanObject.getSessionFactory()
               .getAllClassMetadata();
      assertThat(classMetadata.isEmpty(), is(false));
      Set<String> classes = classMetadata.keySet();
      assertThat(classes, hasItem(UserDatasourceEntity.class.getName()));
   }

   @Test
   public void propertiesEnrichmentWorks() {
      LocalAnnotationEntityManagerFactory b = (LocalAnnotationEntityManagerFactory) factoryBean
               .getActualFactoryBuilderBean();
      assertThat(b.getJpaPropertyMap().containsKey("org.hibernate.envers.store_data_at_delete"),
               is(true));
   }

   @Test
   public void datasourcePopulatorExecuted() throws SQLException {
      verify(databasePopulator).populate(Matchers.<Connection> anyObject());
   }

   @Test
   public void gettersReturnExpectedValue() {
      LocalAnnotationEntityManagerFactory b = (LocalAnnotationEntityManagerFactory) factoryBean
               .getActualFactoryBuilderBean();
      assertThat(b.getConfiguration(), is(org.hamcrest.Matchers.<Object> notNullValue()));
      assertThat(b.getLockProvider(), is(org.hamcrest.Matchers.<Object> notNullValue()));
      assertThat(b.getDatabasePopulator(), is(org.hamcrest.Matchers.<Object> notNullValue()));
      assertThat(b.getConfigLocations().isEmpty(), is(true));
      assertThat(b.getResources().isEmpty(), is(true));
      assertThat(b.isSchemaUpdate(), is(true));
      assertThat(b.getAnnotatedClasses().isEmpty(), is(false));
      assertThat(b.getEventListeners().isEmpty(), is(false));
   }

   @Test
   public void lockProviderExecuted() {
      verify(distributedLockProvider).lockFor(anyString());
      verify(distributedLockProvider).markWorkDone();
      verify(distributedLockProvider).isWorkDone();
   }

   @Test
   public void getConfigurationActuallyReturnNotNull() {
      assertThat(factoryBean.getConfiguration(), is(org.hamcrest.Matchers.<Object> notNullValue()));
   }
}
