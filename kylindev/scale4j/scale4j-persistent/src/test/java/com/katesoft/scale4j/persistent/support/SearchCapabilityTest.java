package com.katesoft.scale4j.persistent.support;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

import org.junit.Test;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
public class SearchCapabilityTest extends AbstractSimpleContextPersistentTest {
   @Override
   public void setUp() throws Exception {
      hibernateTemplate.deleteAll(TestEntity.class);
      super.setUp();
   }

   @Test
   public void searchWorks() {
      for (int i = 0; i <= 100; i++) {
         final AtomicReference<TestEntity> reference = new AtomicReference<TestEntity>();
         transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(
                     @SuppressWarnings("unused") TransactionStatus status) {
               TestEntity entity = TestEntity.newTestInstance();
               hibernateTemplate.save(entity);
               hibernateTemplate.flush();
               reference.set(entity);
            }
         });
         hibernateTemplate.clear();
         List<TestEntity> search = hibernateDaoSupport.search(reference.get().getOneField(),
                  TestEntity.class, new String[] { TestEntity.PROPERTY_ONE_FIELD });
         assertThat(search.size(), is(1));
         assertThat(reference.get().getOneField(), is(search.iterator().next().getOneField()));
         hibernateTemplate.clear();
         search = hibernateDaoSupport.search(UUID.randomUUID().toString(), TestEntity.class,
                  new String[] { TestEntity.PROPERTY_ONE_FIELD });
         assertThat(search.size(), is(0));
         hibernateTemplate.clear();
      }
   }
}
