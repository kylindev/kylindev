package com.katesoft.scale4j.persistent.model;

import java.util.UUID;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Store;
import org.junit.Ignore;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

/**
 * @author kate2007
 */
@Entity
@Ignore
@Table(name = "t_test_entity2")
@Audited
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Access(AccessType.FIELD)
@org.hibernate.annotations.Entity(dynamicUpdate = true)
public class TestEntity2 extends AbstractPersistentEntity {
   private static final long serialVersionUID = -5272355577433457099L;
   @Column(length = 36)
   @Field(index = Index.TOKENIZED, store = Store.YES)
   private String oneField;
   @JoinColumn(name = "parent_id")
   @ManyToOne(optional = true)
   private TestEntity parent;

   public void setOneField(String oneField) {
      this.oneField = oneField;
   }

   public String getOneField() {
      return oneField;
   }

   public TestEntity getParent() {
      return parent;
   }

   public void setParent(TestEntity parent) {
      this.parent = parent;
   }

   @Override
   public void forceAttributesLoad() {
      super.forceAttributesLoad();
      initializeProxy(parent);
   }

   public static TestEntity2 newTestInstance() {
      TestEntity2 entity = new TestEntity2();
      entity.setOneField(UUID.randomUUID().toString());
      return entity;
   }
}
