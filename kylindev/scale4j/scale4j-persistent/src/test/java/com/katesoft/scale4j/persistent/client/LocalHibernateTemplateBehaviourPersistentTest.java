package com.katesoft.scale4j.persistent.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import java.sql.SQLException;
import java.util.Date;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jmx.StatisticsService;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;

/**
 * @author kate2007
 */
public class LocalHibernateTemplateBehaviourPersistentTest extends
         AbstractSimpleContextPersistentTest {
   private StatisticsService statisticsService;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      hibernateTemplate.deleteAll(TestEntity.class);
      statisticsService = new StatisticsService();
      statisticsService.setSessionFactory(sessionFactory);
      statisticsService.clear();
   }

   @Test
   public void saveExecutedCorrectly() {
      final TestEntity entity = TestEntity.newTestInstance();
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.save(entity);
         }
      });
      // one for test entity save, another for DefaultRevisionEntity, and the last one for audit
      assertThat(statisticsService.getEntityInsertCount(), is(3L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(0L));
   }

   @Test
   public void updateExecutedCorrectly() {
      final TestEntity entity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.save(entity);
            return null;
         }
      });
      assertThat(statisticsService.getEntityInsertCount(), is(3L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(0L));
      statisticsService.clear();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            TestEntity copy = (TestEntity) session.get(TestEntity.class,
                     entity.getUniqueIdentifier());
            copy.setOneField("another");
            session.update(copy);
            session.update(copy);
            session.update(copy);
            return null;
         }
      });
      assertThat(statisticsService.getEntityInsertCount(), is(2L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(1L));
   }

   @Test
   public void saveOrUpdateExecutedCorrectly() {
      final TestEntity entity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.save(entity);
            return null;
         }
      });
      statisticsService.clear();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            TestEntity copy = (TestEntity) session.get(TestEntity.class,
                     entity.getUniqueIdentifier());
            copy.setOneField("another");
            session.saveOrUpdate(copy);
            return null;
         }
      });
      assertThat(statisticsService.getEntityUpdateCount(), is(1L));
   }

   @Test
   public void creationDateIsValid() {
      final TestEntity testEntity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.saveOrUpdate(testEntity);
            session.flush();
            return null;
         }
      });
      hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            assertThat(testEntity.getCreationTimestamp(session),
                     is(Matchers.<Object> notNullValue()));
            assertThat(testEntity.getLastModifyTimestamp(session),
                     is(Matchers.<Object> nullValue()));
            return null;
         }
      });
   }

   @Test
   public void creationDateIsNullWhenTransactionIsNotCommitted() {
      final TestEntity testEntity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.saveOrUpdate(testEntity);
            session.flush();
            assertThat(testEntity.getCreationTimestamp(session), is(Matchers.<Object> nullValue()));
            assertThat(testEntity.getLastModifyTimestamp(session),
                     is(Matchers.<Object> nullValue()));
            return null;
         }
      });
   }

   @Test
   public void lastUpdateTimestampAvailable() {
      final TestEntity testEntity = TestEntity.newTestInstance();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.saveOrUpdate(testEntity);
            session.flush();
            return null;
         }
      });
      final long timestamp = System.currentTimeMillis();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            testEntity.setCrudType(CrudType.UPDATE);
            session.saveOrUpdate(testEntity);
            session.flush();
            assertThat(testEntity.getCreationTimestamp(session),
                     is(Matchers.<Object> notNullValue()));
            assertThat(testEntity.getLastModifyTimestamp(session),
                     is(Matchers.<Object> nullValue()));
            return null;
         }
      });
      hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            assertThat(testEntity.getCreationTimestamp(session),
                     is(Matchers.<Object> notNullValue()));
            Date lastModifyTimestamp = testEntity.getLastModifyTimestamp(session);
            assertThat(lastModifyTimestamp, is(Matchers.<Object> notNullValue()));
            assertThat(lastModifyTimestamp.getTime(), greaterThanOrEqualTo(timestamp));
            assertThat(lastModifyTimestamp.getTime(),
                     lessThanOrEqualTo(System.currentTimeMillis() + 1000));
            return null;
         }
      });
   }

   @Test(expected = IllegalStateException.class)
   public void callingJpaMethodThrowsException() {
      hibernateTemplate.find(TestEntity.class, 1L);
   }
}
