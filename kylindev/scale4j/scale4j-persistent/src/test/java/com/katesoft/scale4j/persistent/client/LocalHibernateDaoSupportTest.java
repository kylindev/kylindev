package com.katesoft.scale4j.persistent.client;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author kate2007
 */
@ContextConfiguration({ "classpath:META-INF/spring/jvmcluster$simple-persistentContext.xml",
         "classpath:META-INF/spring/jvmcluster.testing$hibernatepostprocessing.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class LocalHibernateDaoSupportTest extends AbstractLocalHibernateDaoSupportTest {
}
