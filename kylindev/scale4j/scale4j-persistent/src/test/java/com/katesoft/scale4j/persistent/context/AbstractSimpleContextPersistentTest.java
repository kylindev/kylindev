package com.katesoft.scale4j.persistent.context;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.PlatformTransactionManager;

import com.katesoft.scale4j.common.services.IBeanNameReferences;

/**
 * @author kate2007
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:META-INF/spring/jvmcluster$simple-persistentContext.xml",
         "classpath*:META-INF/spring/jvmcluster.testing$hibernatepostprocessing.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@DirtiesContext
public abstract class AbstractSimpleContextPersistentTest extends AbstractPersistentTest {
   @Autowired
   @Qualifier(IBeanNameReferences.TRANSACTION_MANAGER)
   protected PlatformTransactionManager hibernateTransactionManager;
}
