package com.katesoft.scale4j.persistent.utils;

import com.katesoft.scale4j.persistent.model.TestEntity;
import org.junit.Test;
import org.springframework.util.ReflectionUtils;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

/**
 * @author kate2007
 */
public class HibernateReflectionUtilityTest {
   @Test
   public void fieldIsPersistent() {
      assertThat(HibernateReflectionUtility.isPersistentField(ReflectionUtils.findField(
               TestEntity.class, "oneField")), is(true));
   }

   @Test
   public void fieldIsNotPersistent() {
      assertThat(HibernateReflectionUtility.isPersistentField(ReflectionUtils.findField(
               TestEntity.class, "secondField")), is(false));
   }
}
