package com.katesoft.scale4j.persistent.model.unified;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThan;

import java.sql.SQLException;
import java.util.UUID;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.katesoft.scale4j.persistent.context.AbstractXAContextPersistentTest;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.model.TestEntity2;
import com.katesoft.scale4j.persistent.model.TestEntity3;

/**
 * @author kate2007
 */
public class CommonPersistentEntityTest extends AbstractXAContextPersistentTest {
   TestEntity testEntity;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      hibernateTemplate.deleteAll(TestEntity.class);
      testEntity = TestEntity.newTestInstance();
      hibernateTemplate.save(testEntity);
      hibernateTemplate.flush();
   }

   @Test
   public void verifyForcingDirtyLoadLoadedEntity() {
      final long primaryKey = testEntity.getUniqueIdentifier();
      hibernateTemplate.evict(testEntity);
      hibernateTemplate.clear();
      final TestEntity e = hibernateTemplate.load(TestEntity.class, primaryKey);
      e.forceAttributesLoad();
      assertThat(e.attributesLoaded(), is(true));
   }

   @Test
   public void ableToFetchChildren() {
      TestEntity2 testEntity21 = TestEntity2.newTestInstance();
      TestEntity2 testEntity22 = TestEntity2.newTestInstance();
      TestEntity2 testEntity23 = TestEntity2.newTestInstance();

      testEntity.addChild(testEntity21);
      testEntity.addChild(testEntity22);
      testEntity.addChild(testEntity23);

      testEntity21.setParent(testEntity);
      testEntity22.setParent(testEntity);
      testEntity23.setParent(testEntity);

      hibernateTemplate.update(testEntity);
      hibernateTemplate.flush();
      hibernateTemplate.clear();

      hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            session.createCriteria(TestEntity2.class).list().size();
            return null;
         }
      });
   }

   @Test
   public void verifyVersionIncremented() throws CloneNotSupportedException {
      long initialVersion = testEntity.getVersion();
      assertThat(testEntity, is(testEntity.clone()));
      //
      testEntity.setOneField(UUID.randomUUID().toString());
      hibernateTemplate.update(testEntity);
      hibernateTemplate.flush();
      hibernateTemplate.clear();
      assertThat(testEntity.getVersion(), is(++initialVersion));
      //
      testEntity.setOneField(UUID.randomUUID().toString());
      hibernateTemplate.update(testEntity);
      hibernateTemplate.flush();
      hibernateTemplate.clear();
      assertThat(testEntity.getVersion(), is(++initialVersion));
   }

   @Test
   public void cloneEntityWorksAfterLoadingFromDatabase() throws CloneNotSupportedException {
      hibernateTemplate.clear();
      TestEntity entity1 = hibernateTemplate
               .get(TestEntity.class, testEntity.getUniqueIdentifier());
      entity1.forceAttributesLoad();
      TestEntity entity2 = (TestEntity) entity1.clone();
      assertThat(entity1.getOneField(), is(entity2.getOneField()));
      assertThat(entity1, is(entity2));
   }

   @Test
   public void enumValuePersisted() {
      assertThat(testEntity.getCrudType(), is(Matchers.<Object> notNullValue()));
   }

   @Test
   public void acceptNullsWorks() {
      testEntity.setCrudType(null);
      hibernateTemplate.update(testEntity);
      hibernateTemplate.clear();
      hibernateTemplate.get(TestEntity.class, testEntity.getUniqueIdentifier());
   }

   @Test
   public void ableToPersistTestsEntity3() {
      TestEntity3 testEntity3 = new TestEntity3();
      hibernateTemplate.save(testEntity3);
      hibernateTemplate.clear();
      assertThat(testEntity3.getUniqueIdentifier(), lessThan(System.currentTimeMillis() / 100));

      TestEntity2 testEntity2 = TestEntity2.newTestInstance();
      hibernateTemplate.save(testEntity2);
      hibernateTemplate.clear();
      assertThat(testEntity2.getUniqueIdentifier(), greaterThan(System.currentTimeMillis() / 100));
   }
}
