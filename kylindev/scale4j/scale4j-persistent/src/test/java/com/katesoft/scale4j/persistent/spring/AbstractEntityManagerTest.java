package com.katesoft.scale4j.persistent.spring;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.LinkedList;
import java.util.Properties;
import java.util.concurrent.locks.ReentrantLock;

import javax.persistence.EntityManagerFactory;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.cache.HashtableCacheProvider;
import org.hibernate.dialect.H2Dialect;
import org.junit.After;
import org.junit.Before;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.orm.jpa.JpaTransactionManager;

import com.katesoft.scale4j.common.lock.IDistributedLockProvider;
import com.katesoft.scale4j.persistent.hibernate.StoreUpdateScriptsConfiguration;
import com.katesoft.scale4j.persistent.model.UserDatasourceEntity;

/**
 * @author kate2007
 */
public abstract class AbstractEntityManagerTest {
   protected BasicDataSource dataSource;
   protected EntityManagedOrNativeHibernateFactoryBean factoryBean;
   protected DatabasePopulator databasePopulator;
   protected IDistributedLockProvider distributedLockProvider;
   protected JpaTransactionManager jpaTransactionManager;

   @SuppressWarnings("rawtypes")
   @Before
   public void setUp() throws Exception {
      databasePopulator = mock(DatabasePopulator.class);
      distributedLockProvider = mock(IDistributedLockProvider.class);
      when(distributedLockProvider.lockFor(anyString())).thenReturn(new ReentrantLock());
      Properties properties = new Properties();
      ClassPathResource classPathResource = new ClassPathResource(
               "META-INF/resources/scale4j$jdbc.properties");
      properties.load(classPathResource.getInputStream());
      dataSource = new BasicDataSource();
      dataSource.setUrl(properties.getProperty("jdbc.url"));
      dataSource.setUsername(properties.getProperty("jdbc.username"));
      dataSource.setPassword(properties.getProperty("jdbc.password"));
      dataSource.setInitialSize(Integer.parseInt(properties.getProperty("jdbc.initialSize")));
      dataSource.setMaxActive(Integer.parseInt(properties.getProperty("jdbc.maxActive")));
      dataSource.setDriverClassName(properties.getProperty("jdbc.driverClassName"));
      factoryBean = new EntityManagedOrNativeHibernateFactoryBean();
      factoryBean.setUseJpaInsteadOfNativeHibernate(true);
      factoryBean.setDataSource(dataSource);
      factoryBean.setLockProvider(distributedLockProvider);
      factoryBean.setSchemaUpdate(true);
      factoryBean.setUpdateScriptsConfiguration(new StoreUpdateScriptsConfiguration());
      factoryBean.setAnnotatedClasses(new LinkedList<Class>() {
         private static final long serialVersionUID = -407117902701848146L;

         {
            add(UserDatasourceEntity.class);
         }
      });
      Properties hibernateProperties = new Properties();
      hibernateProperties.put("hibernate.dialect", H2Dialect.class.getName());
      hibernateProperties.put("hibernate.cache.provider_class",
               HashtableCacheProvider.class.getName());
      factoryBean.setHibernateProperties(hibernateProperties);
      factoryBean.setDatabasePopulator(databasePopulator);
      factoryBean.afterPropertiesSet();
      jpaTransactionManager = new JpaTransactionManager(
               (EntityManagerFactory) factoryBean.getObject());
   }

   @After
   public void tearDown() throws Exception {
      dataSource.close();
      factoryBean.destroy();
   }
}
