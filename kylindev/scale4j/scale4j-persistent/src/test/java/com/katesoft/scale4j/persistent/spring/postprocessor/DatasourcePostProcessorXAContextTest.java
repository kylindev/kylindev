package com.katesoft.scale4j.persistent.spring.postprocessor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.is;

import org.hamcrest.Matchers;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.katesoft.scale4j.persistent.context.AbstractXAContextPersistentTest;

/**
 * @author kate2007
 */
public class DatasourcePostProcessorXAContextTest extends AbstractXAContextPersistentTest {
   @Autowired
   private com.atomikos.jdbc.nonxa.AtomikosNonXADataSourceBean dataSourceBean;

   @SuppressWarnings("unchecked")
   @Test
   public void propertiesOverrideWorks() {
      assertThat(dataSourceBean.getDriverClassName(), is("org.h2.Driver"));
      assertThat(dataSourceBean.getUser(), is("sa"));
      assertThat(dataSourceBean.getPassword(), anyOf(is(Matchers.<Object> nullValue()), is("")));
      assertThat(dataSourceBean.getUrl(), is("jdbc:h2:mem:scale4j-xa;MVCC=TRUE;DB_CLOSE_DELAY=-1"));
      assertThat(dataSourceBean.getUniqueResourceName(), is("just-junit-unique-resource-name"));
      assertThat(dataSourceBean.getMinPoolSize(), is(2));
      assertThat(dataSourceBean.getMaxPoolSize(), is(7));
   }
}
