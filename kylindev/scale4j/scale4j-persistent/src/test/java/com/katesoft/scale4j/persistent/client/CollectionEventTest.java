package com.katesoft.scale4j.persistent.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import java.sql.SQLException;
import java.util.List;

import org.hamcrest.Matchers;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jmx.StatisticsService;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.model.TestEntity2;
import com.katesoft.scale4j.persistent.types.RevisionData;

/**
 * @author kate2007
 */
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class CollectionEventTest extends AbstractSimpleContextPersistentTest {
   private StatisticsService statisticsService;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      hibernateTemplate.deleteAll(TestEntity.class);
      statisticsService = new StatisticsService();
      statisticsService.setSessionFactory(sessionFactory);
      statisticsService.clear();
   }

   @Test
   public void savingEntityWithOneChildWorks() {
      final TestEntity entity = TestEntity.newTestInstance();
      entity.addChild(TestEntity2.newTestInstance());
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.save(entity);
         }
      });
      assertThat(entity.getChildren().isEmpty(), is(false));
      assertThat(entity.getChildren().iterator().next().getOneField(),
               is(Matchers.<Object> notNullValue()));
      assertThat(statisticsService.getEntityInsertCount(), is(5L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(0L));
      hibernateTemplate.flush();
      hibernateTemplate.clear();
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().size(), is(1));
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().iterator().next().getParent(), is(entity));
   }

   @Test
   public void updatingEntityByAddingOneChildWorks() {
      final TestEntity entity = TestEntity.newTestInstance();
      entity.addChild(TestEntity2.newTestInstance());
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.save(entity);
         }
      });
      entity.addChild(TestEntity2.newTestInstance());
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.update(entity);
         }
      });
      assertThat(statisticsService.getEntityInsertCount(), is(10L));
      assertThat(statisticsService.getEntityDeleteCount(), is(0L));
      assertThat(statisticsService.getEntityUpdateCount(), is(2L));
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().size(), is(2));
      List<Number> revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      assertThat(revisions.size(), is(2));
      List<RevisionData<TestEntity>> list = hibernateDaoSupport.loadAllRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      assertThat(list.size(), is(2));
      assertThat(list.get(0).getRevisionTimestamp(), lessThanOrEqualTo(System.currentTimeMillis()));
      assertThat(list.get(1).getRevisionTimestamp(), lessThanOrEqualTo(System.currentTimeMillis()));
      assertThat(list.get(0).getRevisionType(), is(CrudType.UPDATE));
      assertThat(list.get(1).getRevisionType(), is(CrudType.CREATE));
   }

   @Test
   public void removingChildFromCollectionWorks() {
      final TestEntity entity = TestEntity.newTestInstance();
      entity.addChild(TestEntity2.newTestInstance());
      entity.addChild(TestEntity2.newTestInstance());
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.save(entity);
            hibernateTemplate.flush();
            hibernateTemplate.clear();
         }
      });
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().size(), is(2));
      List<Number> revisions = hibernateDaoSupport.allRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      assertThat(revisions.size(), is(1));
      List<RevisionData<TestEntity>> list = hibernateDaoSupport.loadAllRevisions(TestEntity.class,
               entity.getUniqueIdentifier());
      assertThat(list.size(), is(1));
      hibernateTemplate.clear();
      hibernateTemplate.flush();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            TestEntity copy = (TestEntity) session.get(TestEntity.class,
                     entity.getUniqueIdentifier());
            TestEntity2 testEntity2 = copy.getChildren().iterator().next();
            copy.getChildren().remove(testEntity2);
            session.delete(testEntity2);
            session.update(copy);
            session.flush();
            return null;
         }
      });
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().size(), is(1));
      revisions = hibernateDaoSupport.allRevisions(TestEntity.class, entity.getUniqueIdentifier());
      assertThat(revisions.size(), is(2));
      list = hibernateDaoSupport.loadAllRevisions(TestEntity.class, entity.getUniqueIdentifier());
      assertThat(list.size(), is(2));
   }

   @Test
   public void updateChildWorks() {
      final TestEntity entity = TestEntity.newTestInstance();
      entity.addChild(TestEntity2.newTestInstance());
      transactionTemplate.execute(new TransactionCallbackWithoutResult() {
         @Override
         protected void doInTransactionWithoutResult(
                  @SuppressWarnings("unused") TransactionStatus status) {
            hibernateTemplate.save(entity);
         }
      });
      hibernateTemplate.clear();
      hibernateTemplate.flush();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            TestEntity copy = (TestEntity) session.get(TestEntity.class,
                     entity.getUniqueIdentifier());
            TestEntity2 testEntity2 = copy.getChildren().iterator().next();
            testEntity2.setOneField("updated_value");
            session.update(copy);
            session.flush();
            return null;
         }
      });
      assertThat(hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
               .getChildren().size(), is(1));
      assertThat(hibernateDaoSupport.allRevisions(TestEntity.class, entity.getUniqueIdentifier())
               .size(), is(1));
      assertThat(
               hibernateTemplate.get(
                        TestEntity2.class,
                        hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
                                 .getChildren().iterator().next().getUniqueIdentifier())
                        .getOneField(), is("updated_value"));
      assertThat(
               hibernateDaoSupport.allRevisions(
                        TestEntity2.class,
                        hibernateTemplate.get(TestEntity.class, entity.getUniqueIdentifier())
                                 .getChildren().iterator().next().getUniqueIdentifier()).size(),
               is(2));
   }
}
