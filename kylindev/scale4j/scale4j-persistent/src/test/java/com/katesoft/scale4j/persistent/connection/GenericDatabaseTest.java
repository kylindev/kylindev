package com.katesoft.scale4j.persistent.connection;

import java.sql.Connection;
import java.sql.SQLException;

import junit.framework.Assert;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.katesoft.scale4j.persistent.context.AbstractXAContextPersistentTest;

/**
 * @author kate2007
 */
public class GenericDatabaseTest extends AbstractXAContextPersistentTest {
   @Test
   public void connectionEstablished() {
      Connection connection = null;
      try {
         connection = dataSource.getConnection();
      } catch (SQLException e) {
         Assert.fail("unable to open connection with configuration " + e);
      }
      if (connection != null) {
         logger.debug("connection " + connection + " established successfully");
         try {
            connection.close();
         } catch (SQLException e) {
            Assert.fail("unable to close connection " + e);
         }
      }
   }

   @Test
   public void hibernateTemplateCreated() {
      hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException {
            session.clear();
            session.getStatistics();
            return null;
         }
      });
   }
}
