package com.katesoft.scale4j.persistent.model;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.UUID;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.envers.Audited;
import org.hibernate.search.annotations.Field;
import org.hibernate.search.annotations.Index;
import org.hibernate.search.annotations.Indexed;
import org.hibernate.search.annotations.Store;
import org.junit.Ignore;

import com.katesoft.scale4j.persistent.enums.CrudType;
import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;
import com.katesoft.scale4j.persistent.spring.postprocessor.HibernateSessionExtensionPostProcessor;

/**
 * This is test entity to be used with {@link HibernateSessionExtensionPostProcessor}.
 * 
 * @author kate2007
 */
@Entity
@org.hibernate.annotations.Entity(
                                  dynamicUpdate = true,
                                  dynamicInsert = true,
                                  optimisticLock = OptimisticLockType.VERSION)
@Table(name = "t_test_entity")
@Audited
@Ignore
@Indexed
@Cacheable
@Access(AccessType.FIELD)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries(value = {
         @NamedQuery(name = TestEntity.QUERY_FIND_BY_UID, query = "from TestEntity as e where e."
                  + TestEntity.PROP_UID + "=:id"),
         @NamedQuery(name = TestEntity.QUERY_FIND_BY_UID1, query = "from TestEntity as e where e."
                  + TestEntity.PROP_UID + "= ?"),
         @NamedQuery(name = TestEntity.QUERY_FIND_BY_UID2, query = "from TestEntity as e where e."
                  + TestEntity.PROP_UID + "= :" + TestEntity.PROP_UID),
         @NamedQuery(
                     name = TestEntity.QUERY_FIND_BY_UID_AND_VERSION,
                     query = "from TestEntity as e where e." + TestEntity.PROP_UID + "=:id and e."
                              + TestEntity.PROP_VERSION + "=:version"),
         @NamedQuery(
                     name = TestEntity.QUERY_FIND_BY_UID_AND_VERSION2,
                     query = "from TestEntity as e where e." + TestEntity.PROP_UID + "=? and e."
                              + TestEntity.PROP_VERSION + "=?"),
         @NamedQuery(name = TestEntity.QUERY_FIND_ALL, query = "from TestEntity") })
public class TestEntity extends AbstractPersistentEntity {
   private static final long serialVersionUID = 7876172410318362793L;
   public static final String QUERY_FIND_BY_UID = "TestEntity.findByUniqueIdentifier";
   public static final String QUERY_FIND_BY_UID1 = "TestEntity.findByUniqueIdentifier1";
   public static final String QUERY_FIND_BY_UID2 = "TestEntity.findByUniqueIdentifier2";
   public static final String QUERY_FIND_ALL = "TestEntity.findAll";
   public static final String QUERY_FIND_BY_UID_AND_VERSION = "TestEntity.findByUniqueIdentifierAndVersion";
   public static final String QUERY_FIND_BY_UID_AND_VERSION2 = "TestEntity.findByUniqueIdentifierAndVersion2";
   //
   public static final String PROPERTY_ONE_FIELD = "oneField";
   public static final String PROPERTY_CRUD_TYPE = "crudType";
   //
   @Column(length = 36)
   @Field(index = Index.TOKENIZED, store = Store.YES)
   private String oneField;
   //
   @SuppressWarnings({ "unused" })
   @Column(length = 36)
   @Transient
   private String secondField;
   //
   @Column(name = "crud_type")
   @Enumerated(EnumType.STRING)
   private CrudType crudType = CrudType.CREATE;
   //
   @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
   @OneToMany(cascade = { CascadeType.ALL }, mappedBy = "parent")
   private Collection<TestEntity2> children = new LinkedHashSet<TestEntity2>();

   public void setOneField(String oneField) {
      this.oneField = oneField;
   }

   public String getOneField() {
      return oneField;
   }

   public Collection<TestEntity2> getChildren() {
      return children;
   }

   public void setChildren(Collection<TestEntity2> children) {
      this.children = children;
   }

   public void addChild(TestEntity2 child) {
      child.setParent(this);
      getChildren().add(child);
   }

   public static TestEntity newTestInstance() {
      TestEntity entity = new TestEntity();
      entity.setOneField(UUID.randomUUID().toString());
      entity.secondField = UUID.randomUUID().toString();
      return entity;
   }

   public void setCrudType(CrudType crudType) {
      this.crudType = crudType;
   }

   public CrudType getCrudType() {
      return crudType;
   }

   @Override
   public boolean isCleanable() {
      return false;
   }

   @Override
   public void forceAttributesLoad() {
      super.forceAttributesLoad();
      initializeProxy(children);
   }
}
