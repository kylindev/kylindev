package com.katesoft.scale4j.persistent.client;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

import java.sql.SQLException;

import org.hamcrest.Matchers;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateOptimisticLockingFailureException;
import org.springframework.transaction.annotation.Transactional;

import com.katesoft.scale4j.persistent.context.AbstractSimpleContextPersistentTest;
import com.katesoft.scale4j.persistent.model.TestEntity;
import com.katesoft.scale4j.persistent.model.TestEntity2;
import com.katesoft.scale4j.persistent.utils.DataAccessUtility;

/**
 * @author kate2007
 */
@Transactional
public class LocalHibernateTemplatePersistentTest extends AbstractSimpleContextPersistentTest {
   TestEntity entity;

   @Override
   public void setUp() throws Exception {
      super.setUp();
      doInNewTransaction(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(@SuppressWarnings("unused") Session session)
                  throws HibernateException, SQLException {
            hibernateTemplate.deleteAll(TestEntity2.class);
            hibernateTemplate.deleteAll(TestEntity.class);
            return null;
         }
      });
      entity = TestEntity.newTestInstance();
   }

   @Test
   public void loadByGuidWorks() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      TestEntity e = hibernateTemplate.loadByGlobalUniqueIdentifier(TestEntity.class,
               entity.getGlobalUniqueIdentifier(), true);
      assertThat(e.getGlobalUniqueIdentifier(), is(entity.getGlobalUniqueIdentifier()));
      entity = e;
   }

   @Test
   public void collectionOfChildrenPersisted() {
      TestEntity testEntity = TestEntity.newTestInstance();
      hibernateTemplate.save(testEntity);
      //
      TestEntity2 testEntity21 = TestEntity2.newTestInstance();
      TestEntity2 testEntity22 = TestEntity2.newTestInstance();
      TestEntity2 testEntity23 = TestEntity2.newTestInstance();
      //
      testEntity.addChild(testEntity21);
      testEntity.addChild(testEntity22);
      testEntity.addChild(testEntity23);
      testEntity21.setParent(testEntity);
      testEntity22.setParent(testEntity);
      testEntity23.setParent(testEntity);
      //
      hibernateTemplate.update(testEntity);
      hibernateTemplate.flush();
      hibernateTemplate.clear();
      //
      assertThat(testEntity.getUniqueIdentifier(), is(testEntity21.getParent()
               .getUniqueIdentifier()));
      assertThat(testEntity.getUniqueIdentifier(), is(testEntity22.getParent()
               .getUniqueIdentifier()));
      assertThat(testEntity.getUniqueIdentifier(), is(testEntity23.getParent()
               .getUniqueIdentifier()));
      //
      TestEntity copy = hibernateTemplate.get(TestEntity.class, testEntity.getUniqueIdentifier());
      assertThat(copy.getChildren().size(), is(3));
      assertThat(copy.getChildren()
               .containsAll(hibernateTemplate.loadAll(TestEntity2.class, false)), is(true));
   }

   @Test
   public void entityForUpdateWorks() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      assertThat(hibernateTemplate.findEntityForUpdate(TestEntity.class,
               entity.getUniqueIdentifier(), entity.getVersion()),
               is(Matchers.<Object> notNullValue()));
   }

   @Test(expected = HibernateOptimisticLockingFailureException.class)
   public void entityForUpdateThrowOptimisticLockException() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      assertThat(hibernateTemplate.findEntityForUpdate(TestEntity.class,
               entity.getUniqueIdentifier(), entity.getVersion() + 1),
               is(Matchers.<Object> notNullValue()));
   }

   @Test(expected = EmptyResultDataAccessException.class)
   public void entityForUpdateThrowIncorrectSizeExceptionWhenEntityNotFound() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      assertThat(hibernateTemplate.findEntityForUpdate(TestEntity.class,
               entity.getUniqueIdentifier() + 2, entity.getVersion() + 1),
               is(Matchers.<Object> notNullValue()));
   }

   @Test
   public void ableToFindByCriteria() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      hibernateTemplate.save(TestEntity.newTestInstance());
      hibernateTemplate.save(TestEntity.newTestInstance());
      hibernateTemplate.flush();
      assertThat((TestEntity) hibernateTemplate.execute(new HibernateCallback<Object>() {
         @Override
         public Object doInHibernate(Session session) throws HibernateException, SQLException {
            Criteria criteria = entity.entityForUpdateCriteria().getExecutableCriteria(session);
            return criteria.uniqueResult();
         }
      }), is(entity));
   }

   @SuppressWarnings({ "unchecked" })
   @Test
   public void ableToFindByExample() {
      hibernateTemplate.save(entity);
      hibernateTemplate.flush();
      hibernateTemplate.save(TestEntity.newTestInstance());
      hibernateTemplate.save(TestEntity.newTestInstance());
      hibernateTemplate.flush();
      assertThat((TestEntity) DataAccessUtility.fetchFirstRowOrNull(hibernateTemplate
               .findByExample(entity.entityForUpdateExample())), is(entity));
   }
}
