        scale4j-share module contains useful utility classes and some basic interfaces(most likely clients will not use this module directly).
        Also some common spring configuration is included such as jmx-context.xml.

        scale4j-persistent provides rich domain model and CRUD backend plus additional audit and search capabilities
        (clients will use well-known interfaces similar to HibernateTemplate and HibernateDaoSupport).
        There is nothing special required to enable audit and search stuff - just adding few annotations from hibernate envers and hibernate search sub-projects
        to your domain entities (@Indexed @Audited annotations etc) - no audit triggers, high level API for retrieving historical data.
        In fact client interfaces that are available in scale4j-persistent module are simply extensions of spring orm classes/interfaces, so clients
        would not need to spent time on learning internal API/reading additional documentation, but will get extended functionality instead.
        scale4j-persistent is minimal dependency, but please note that scale4j-persistent is not cluster-aware(not 'cluster-safe') itself.

        scale4j-rttp('real time transaction processor') is core module for clustering, data-distributing, scaling.
        This module contains all you need to handle hugh amount of requests(transactions) in real-time and provides some extended functionality for job scheduling.
        Hibernate second-level cache provider(RttpCacheRegionFactory) is great backend for in-memory domain object caching.
        Spring batch is integrated with quartz job scheduler and you can find this integration is powerful and simple to use.
        Also you can find useful Perf4j integration - no more System.currentMilisecs(), no more StopWatches - just @Profiled annotation.
        There are a few spring post-processors which allows you to customize spring configuration, so as result you would not need to modify any of pre-configured configs directly.


        scale4j-web is simple extension of rttp module. Provides classical implementation of User/Role domain entities ready to work with spring security.
        But more important thing that you can find here is WebMonitor class which is jetty server itself enriched with per4j runtime statistic browser, log4j configurator,
        and very powerful hazelcast web console.


        scale4j-agent module defines concept of 'service' and allows you to launch client services in separate/dedicated JVMs(JVM will be created for each client service).
        Powerful com.katesoft.scale4j.agent.JavaProcessBuilder and com.katesoft.scale4j.agent.SpringConfigurationRunner allows
        you to launch spring-based services easily as well as control lifecycle of your service through JMX console. You would not need to restart whole application,
        but instead you can restart just separate service. Moreover each service will be part of the scale4j cluster itself.