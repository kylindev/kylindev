package com.katesoft.scale4j.demo.services;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.demo.domain.Currency;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/currencies-app-context.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
public class AvailableCurrenciesTaskletTest extends AbstractPersistentTest {
   private AvailableCurrenciesTasklet availableCurrenciesTasklet;

   @Before
   public void before() {
      availableCurrenciesTasklet = new AvailableCurrenciesTasklet();
      availableCurrenciesTasklet.setHibernateTemplate(hibernateTemplate);
   }

   @Test
   public void currenciesPersisted() throws Exception {
      availableCurrenciesTasklet.execute(null, null);
      int size = hibernateTemplate.loadAll(Currency.class).size();
      assertThat(size, greaterThan(10));
      hibernateTemplate.clear();
      availableCurrenciesTasklet.execute(null, null);
      assertThat(hibernateTemplate.loadAll(Currency.class).size(), is(size));
   }
}
