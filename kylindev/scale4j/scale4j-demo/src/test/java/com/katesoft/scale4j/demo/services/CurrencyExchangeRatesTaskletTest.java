package com.katesoft.scale4j.demo.services;

import com.katesoft.scale4j.common.services.IBeanNameReferences;
import com.katesoft.scale4j.demo.domain.ExchangeRate;
import com.katesoft.scale4j.persistent.context.AbstractPersistentTest;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.scope.context.StepContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "/META-INF/spring/currencies-app-context.xml" })
@TransactionConfiguration(
                          defaultRollback = true,
                          transactionManager = IBeanNameReferences.TRANSACTION_MANAGER)
@Transactional
public class CurrencyExchangeRatesTaskletTest extends AbstractPersistentTest {
   private CurrencyExchangeRatesTasklet currencyExchangeRatesTasklet;
   private AvailableCurrenciesTasklet availableCurrenciesTasklet;
   private ChunkContext chunkContext;

   @Before
   public void before() {
      currencyExchangeRatesTasklet = new CurrencyExchangeRatesTasklet();
      currencyExchangeRatesTasklet.setHibernateTemplate(hibernateTemplate);
      availableCurrenciesTasklet = new AvailableCurrenciesTasklet();
      availableCurrenciesTasklet.setHibernateTemplate(hibernateTemplate);
      chunkContext = new ChunkContext(new StepContext(new StepExecution("step-1", new JobExecution(
               1L))));
      chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext()
               .put("date", "2005-01-01");
   }

   @Test
   public void ratesPersisted() throws Exception {
      availableCurrenciesTasklet.execute(null, chunkContext);
      currencyExchangeRatesTasklet.execute(null, chunkContext);
      assertThat(hibernateTemplate.loadAll(ExchangeRate.class).size(), greaterThan(0));
   }
}
