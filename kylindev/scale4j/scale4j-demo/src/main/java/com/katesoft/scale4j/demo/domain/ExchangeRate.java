package com.katesoft.scale4j.demo.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

@Entity
@Table(name = "t_exchange_rates")
@Audited
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries(value = { @NamedQuery(
                                    name = "exchange_rate.find_by_code",
                                    query = "from ExchangeRate as r where r.currency = ?") })
public class ExchangeRate extends AbstractPersistentEntity {
   private static final long serialVersionUID = -3988545174393662547L;
   @JoinColumn(nullable = false, unique = true)
   @OneToOne(fetch = FetchType.LAZY)
   private Currency currency;
   @Column(nullable = false)
   private BigDecimal rate;
   @Column(nullable = false)
   private Date actualDate;

   public ExchangeRate() {
   }

   public void setCurrency(Currency currency) {
      this.currency = currency;
   }

   public void setRate(BigDecimal rate) {
      this.rate = rate;
   }

   public Currency getCurrency() {
      return currency;
   }

   public BigDecimal getRate() {
      return rate;
   }

   public void setActualDate(Date actualDate) {
      this.actualDate = actualDate;
   }

   public Date getActualDate() {
      return actualDate;
   }

   @Override
   public void forceAttributesLoad() {
      super.forceAttributesLoad();
      initializeProxy(currency);
   }
}
