package com.katesoft.scale4j.demo.domain;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.envers.Audited;

import com.katesoft.scale4j.persistent.model.unified.AbstractPersistentEntity;

@Entity
@Table(name = "t_currency")
@Audited
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Currency extends AbstractPersistentEntity {
   private static final long serialVersionUID = -7414795033013482436L;
   private String code;

   public Currency(String code) {
      this.code = code;
   }

   public Currency() {
   }

   public String getCode() {
      return code;
   }

   public void setCode(String code) {
      this.code = code;
   }
}
